# The Haynes catalog

## Useful information

### Running locally

Haynes uses a very old version of Ruby (1.8.7) and also the corresponding libraries are old.
Locally, I use rvm to manage this, e.g. `rvm use ruby-1.8.7-p374@haynes`

### UML diagram

There is a UML diagram in `application/db`. This diagram might not be up-to-date, but the most important relationships should be correct.

### Data versioning

Haynes offers a kind of change log for each work and composer. The corresponding data can be found in the tables called `***_versions`. They should have the same structure as the main tables, plus a few columns.

The versioning is done by a plugin called `acts_as_versioned`, which is quite certainly not maintained anymore. I think the project moved to Github: https://github.com/technoweenie/acts_as_versioned/

### Synchronisation

It is possible to synchronize the data on a local deployment with the remote database using an API. This API is basically a generic CRUD API which is implemented in the `api_controller`. It is exactly the same for all entities and overwrites all data in the remote database. So, use it with care!

On the client side, i.e. the local installation, the logic is implemented in the `synchronize_controller`, which first reads a list of all ids and the timestamps they have been updated from the remote database using GET /api/...table name.../timestamps. Then each timestamp from the local records is compared with the remote timestamp and if the local data is newer, or a local record has been added or deleted, the corresponding CRUD operation of the API is called.

Most records are synchronised in the "outgoing" mode, i.e. local data is sent to the remote installation. For additions and users, the data is "incoming", i.e. the remote data is the leading one, for example if someone added a note (addition) to a work.

### Logging

Currently, all requests are logged, which creates a very large log file. This should be changed.

## Directory structure

`application`: the actual application using the standard rails layout (at the time when it was initially created with rails 1.???).

`gitlab-ci.yaml`: Configuration for the Gitlab CI/CD pipeline.

`docker-compose.yaml` and `docker-compose-local.yaml`: define the docker containers needed to run the application.

`Dockerfile`: Defines the docker image for the application.

`mfo_dump*.sql`: The last dumps I created (when moving to a different server in 2019).

## Build process

The project uses a very simple build pipeline, which simply creates a docker image and pushes it to the provided docker registry of the Gitlab project. As usual for Gitlab projects, the pipeline is defined in the file `.gitlab-ci.yml`.

There are no unit tests (embarassing...) or other build steps.

The docker image for haynes uses a special base image, which includes the correct ruby/rails versions. The project for the docker base image can be found here: https://gitlab.com/kanschik.jka.jens/docker-image-rails

## Deployment

The project uses docker (https://www.docker.com/) to deploy the application. There are three relevant images:
1.   The rails project itself
1.   The database (mariadb)
1.   adminer as a (very) simple UI to access the database for administration / fixes etc.

The whole stack with all three components is described in the file `docker-compose.yaml`.

The rails container and the adminer container are both part of a traefik network. Traefik (https://docs.traefik.io/) is used as a proxy to handle all network communication. The two containers register themselves in the traefik network by using docker labels, for example `"traefik.port=3000"` to tell traefik that the rails container can be reached on port 3000.
