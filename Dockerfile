FROM registry.gitlab.com/kanschik.jka.jens/docker-image-rails:latest

COPY ./application /application
RUN chmod +x ./application/script/server

ENTRYPOINT [ "/application/script/server", "start", "-e", "online_production" ]
