#!/usr/bin/env ruby


def self.add(xml, controller, object)
	return if object == nil
    xml.url do
      xml.loc url_for(:controller => controller, :id => object.id, :action => 'show')
      xml.lastmod((Date.today-5).strftime("%Y-%m-%d"))
#      xml.lastmod object.updated_at.xmlschema if object.updated_at
      xml.changefreq "weekly"
      xml.priority 0.5
    end
    
end

ENV['RAILS_ENV'] ||= "production"

Dir.chdir(File.expand_path(File.dirname(__FILE__) + "/..")) # Change current directory to RAILS_ROOT
require "config/environment" # Start up rails

# These two lines make life super easy... It allows you to call url_for/link_to outside of a controller or view
include ActionController::UrlWriter
default_url_options[:host] = 'haynes-catalog.net'

filename = "#{RAILS_ROOT}/public/sitemap.xml"

File.open(filename, "w") do |file|
  xml = Builder::XmlMarkup.new(:target => file, :indent => 2)

  # This
  xml.instruct!
  xml.urlset "xmlns" => "http://www.sitemaps.org/schemas/sitemap/0.9" do
    Library.find(:all).each {|entity| add xml, :libraries, entity}
    Publisher.find(:all).each {|entity| add xml, :publishers, entity}
  	Work.find(:all).each {|entity| add xml, :works, entity}
    Composer.find(:all).each {|entity| add xml, :composers, entity}
    ['how_to_use', 'about_this_catalog', 'about_peter', 'contact'].each do |action|
      xml.url do 
        xml.loc url_for(:controller => 'info', :action => action)
        xml.changefreq "monthly"
        xml.priority 0.5
      end
    end
  end

end
