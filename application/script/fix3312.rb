#!/usr/bin/env ruby

ENV['RAILS_ENV'] ||= "development"

Dir.chdir(File.expand_path(File.dirname(__FILE__) + "/..")) # Change current directory to RAILS_ROOT
require "config/environment" # Start up rails

w = Work.find 3312
w.version = nil
w.create_version!