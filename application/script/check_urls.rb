#!/usr/bin/env ruby

def do_check(controller, id)
  puts "Checking #{controller} #{id}..."
  response = Net::HTTP.get_response URI.parse(url_for(:controller => controller, :id => id, :action => 'show'))
  if response.code != "200"
    puts "There is a problem with #{controller} #{id}, response is:"
    puts response.read_body
  end
  
end
ENV['RAILS_ENV'] ||= "development"

Dir.chdir(File.expand_path(File.dirname(__FILE__) + "/..")) # Change current directory to RAILS_ROOT
require "config/environment" # Start up rails

# These two lines make life super easy... It allows you to call url_for/link_to outside of a controller or view
include ActionController::UrlWriter
default_url_options[:host] = 'haynes.beauty-of-code.de'

Library.find(:all).each { |object| do_check(:libraries, object.id) }
Publisher.find(:all).each { |object| do_check(:publishers, object.id) }
Composer.find(:all).each { |object| do_check(:composers, object.id) }
Work.find(:all).each { |object| do_check(:works, object.id) }
