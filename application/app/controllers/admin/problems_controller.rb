class Admin::ProblemsController < Admin::AdminController
  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def show
    @problem = Problem.find(params[:id])
  end

  def new
    @problem = Problem.new
  end

  def create
    @problem = Problem.new(params[:problem])
    if @problem.save
      redirect_to :action => 'show', :id => @problem
    else
      render :action => 'new'
    end
  end

  def edit
    @problem = Problem.find(params[:id])
  end

  def update
    @problem = Problem.find(params[:id])
    if params[:cancel]
      redirect_to :action => 'show', :id => @problem, :search_id => params[:search_id]
      return
    end
  
    if @problem.update_attributes(params[:publisher])
      redirect_to :action => 'show', :id => @problem, :search_id => params[:search_id]
    else
      render :action => 'edit'
    end
  end

end
