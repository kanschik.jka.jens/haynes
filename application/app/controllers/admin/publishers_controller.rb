class Admin::PublishersController < Admin::AdminController
  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def show
    @publisher = Publisher.find(params[:id])
    @publisher_works = PublishersWork.count(:include => [:work], :conditions => ["publisher_id = ? and state = 'default'", params[:id]])
    @successors = Publisher.count(:conditions => {:successor_id => params[:id]})
    @title = @publisher.label
  end

  def new
    @publisher = Publisher.new
  end

  def create
    @publisher = Publisher.new(params[:publisher])
    if @publisher.save
      redirect_to :action => 'show', :id => @publisher
    else
      render :action => 'new'
    end
  end

  def edit
    @publisher = Publisher.find(params[:id])
    @title = @publisher.label
  end

  def update
    @publisher = Publisher.find(params[:id])
    if params[:cancel]
      redirect_to :action => 'show', :id => @publisher, :search_id => params[:search_id]
      return
    end
  
    if @publisher.update_attributes(params[:publisher])
      redirect_to :action => 'show', :id => @publisher, :search_id => params[:search_id]
    else
      render :action => 'edit'
    end
  end

end
