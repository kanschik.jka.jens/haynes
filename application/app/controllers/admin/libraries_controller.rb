class Admin::LibrariesController < Admin::AdminController
  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def show
    @library = Library.find(params[:id])
    @library_works = LibrariesWork.count(:include => [:work], :conditions => ["library_id = ? and state = 'default'", params[:id]])
    @successors = Library.count(:conditions => {:successor_id => params[:id]})
    @title = @library.label
  end

  def new
    @library = Library.new
  end

  def create
    @library = Library.new(params[:library])
    if @library.save
      redirect_to :action => 'show', :id => @library
    else
      render :action => 'new'
    end
  end

  def edit
    @library = Library.find(params[:id])
  end

  def update
    @library = Library.find(params[:id])
    if params[:cancel]
      redirect_to :action => 'show', :id => @library, :search_id => params[:search_id]
      return
    end
  
    if @library.update_attributes(params[:library])
      redirect_to :action => 'show', :id => @library, :search_id => params[:search_id]
    else
      render :action => 'edit'
    end
  end

end
