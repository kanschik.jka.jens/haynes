class PublishersController < ApplicationController

  def show
    @publisher = Publisher.find(params[:id])
    @publisher_works = PublishersWork.count(:include => [:work], :conditions => ["publisher_id = ? and state = 'default'", params[:id]])
    @successors = Publisher.count(:conditions => {:successor_id => params[:id]})
    @title = @publisher.label
  end

end
