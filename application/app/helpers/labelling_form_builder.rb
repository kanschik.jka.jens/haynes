class LabellingFormBuilder < ActionView::Helpers::FormBuilder

  (field_helpers - %w(check_box radio_button hidden_field)).each do |selector|
    define_method(selector) do |field, options|
      object = @template.instance_variable_get("@#{@object_name}")
      error = object.errors.on(field)
      error_tag = error ?
        @template.content_tag(:label, "&nbsp;") + 
        @template.content_tag(:p, error, :class => "error") : ""
      
      version = @template.instance_variable_get("@version")
      version_tag = version ?
        @template.content_tag(:label, "&nbsp;") + 
        @template.content_tag(:p, version.send(field), :class => "version") : ""
      
      @template.content_tag(:div,
        @template.content_tag(
          :label,
          options[:label] || field.to_s.humanize + ":",
          :for => "#{@object_name}_#{field}") + 
        @template.send(selector + '_tag', 
          "#{@object_name}[#{field}]",
          object.send(field),
          :id => "#{@object_name}_#{field}") +
        error_tag + 
        version_tag,
        :class => "required",
        :id => "#{@object_name}_#{field}_div"
      )
    end
  end

#    def text_field(field, options = {})
#      object = instance_variable_get("@#{@object_name}")
#       @template.content_tag(
#        "label",
#        options[:label] || field.to_s.humanize + ":",
#        :for => "#{@object_name}_#{field}",
#        :class => 'desc') +
#       super
##         @template.content_tag(options[:tag] || "span", super)
#    end

  def error_msg(name)
    return "#{Work.errors}" unless @errors && @errors[name.to_sym]
    html = ""
    @errors[name.to_sym].each do |error|
      html += "<label>&nbsp;</label>"
      html += "<p class='error'>" + error + "</p>"
    end
    return html
  end

#  def text_field(label, method, options = {})
#    error_msg = methods
##    error_msg = error_msg(method)
#    if error_msg != ""
#      options[:class] = options[:class] ? options[:class] + " error" : "error"
#    end
#    html = <<-EOHTML
#      <div class="required">
#        <label for="#{method}">#{label}</label>
#        #{super(method, options)}
#        #{error_msg}
#      </div>
#    EOHTML
#    return html
#  end

end