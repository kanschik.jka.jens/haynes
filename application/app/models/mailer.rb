class Mailer < ActionMailer::Base
  
  def comment_added(addition)
    @subject = "Comment added to #{addition.work.title} by user #{addition.user.logname}"
    @body["addition"] = addition
#    @recipients = [ "jens_kanschik@freenet.de"]
    @recipients = [ "jens_kanschik@freenet.de", "Peter Wuttke <wuttke.essen@gmx.de>" ]
    @from = "mail@haynes-catalog.de"
    @sent_on = Time.now
    @headers = {}
  end
  
  def user_registered(user)
    @subject = "New user #{user.logname} registered"
    @body["user"] = user
#    @recipients = [ "jens_kanschik@freenet.de" ]
    @recipients = [ "jens_kanschik@freenet.de", "Peter Wuttke <wuttke.essen@gmx.de>" ]
    @from = "mail@haynes-catalog.de"
    @sent_on = Time.now
    @headers = {}
  end

end