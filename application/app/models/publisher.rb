class Publisher < ActiveRecord::Base
  
  has_many :publishers_works
  has_and_belongs_to_many :works
  belongs_to  :successor, :class_name => "Publisher", :foreign_key => "successor_id"
  has_many    :predecessors, :class_name => "Publisher", :foreign_key => "successor_id"
  belongs_to  :country

  has_many    :documents,             :through => :document_usages
  has_many    :document_usages,       :as => :usage

  validates_presence_of :name, :label, :message => "Missing field"
  validates_uniqueness_of :label

end
