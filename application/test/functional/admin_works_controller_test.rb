require File.dirname(__FILE__) + '/../test_helper'
require 'admin/works_controller'

# Re-raise errors caught by the controller.
class Admin::WorksController; 
  # no login for testing:
  skip_before_filter :login_required
  def rescue_action(e) raise e end; 
end

class AdminWorksControllerTest < Test::Unit::TestCase
  self.use_transactional_fixtures = false
#  self.pre_loaded_fixtures = false
  
  fixtures :sources
  fixtures :works
  fixtures :work_versions
  fixtures :instrumentations
  fixtures :instrumentations_works
  fixtures :composers
  fixtures :libraries
  fixtures :libraries_works

  def dump_session
    session.data.each { |key, value| puts key.to_s + " => " + (value || "nil").to_xml }
  end

  def setup
    @controller = Admin::WorksController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
    @request.env['HTTP_ACCEPT'] = 'text/html'
  end

  def test_show
    show 100
  end
  def test_edit
    edit 100
  end
  # Testet, ob ein Werk mit einer Instrumentation und ohne publisher/libraries gespeichert werden kann (Titel wird geändert).
  # Erwartet: 
  # Kein Fehler, "show" wird angezeigt, es gibt immer noch genau eine Instrumentierung.
  def test_save
    edit 100
    expected_attributes = save_success(100, {"title" => "new title"})
    work = Work.find 100
    real_attributes = work.attributes.dup

    real_attributes.delete "updated_at"
    expected_attributes.delete "updated_at"

    assert_equal "new title", work.title
    assert_equal expected_attributes, real_attributes
    
    assert_equal 1, work.instrumentations(true).size
  end

  # Testet, ob ein instrumentation zu einem Werk hinzugefügt werden kann
  # Erwartet: 
  # Kein Fehler, "show" wird angezeigt, 
  # es gibt jetzt zwei Instrumentierungen.
  def test_save_add_instrumentation
    edit 100
    # simuliert den Klick auf "add instrumentation", ändern der Daten und Klick auf "ok".
    row_id = add_to_session :instrumentations_works,  100
    save_in_session :instrumentations_works, row_id, {:instrumentation_id => 12}
    
    expected_attributes = save_success(100, {"title" => "new title"})
    
    work = Work.find 100
    real_attributes = work.attributes.dup

    real_attributes.delete "updated_at"
    expected_attributes.delete "updated_at"

    assert_equal "new title", work.title
    assert_equal expected_attributes, real_attributes
    
    assert_equal 2, work.instrumentations(true).size
    assert_equal 11, work.instrumentations[0].id
    assert_equal 12, work.instrumentations[1].id
  end

  # Testet, ob ein instrumentation in einem Werk gelöscht werden kann.
  # Erwartet: 
  # Kein Fehler, "show" wird angezeigt, 
  # es gibt eine Instrumentierung (und vorher zwei).
  def test_save_remove_instrumentation
    edit 100
    # fügt erst einmal eine Instrumentierung hinzu,
    # da Werk 100 nur eine Instrumentierung hat und ein Werk nie 0 Instrumentierungen haben darf.
    row_id = add_to_session :instrumentations_works, 100
    save_in_session :instrumentations_works, row_id, {:instrumentation_id => 12}
    save_success(100, {"title" => "new title"})

    # löscht die ursprüngliche Instrumentierung (Code 11)
    edit 100
    remove_from_session :instrumentations_works, 0
    expected_attributes = save_success(100, {"title" => "new title"})

    # prüft Ergebnisse
    work = Work.find 100
    real_attributes = work.attributes.dup

    real_attributes.delete "updated_at"
    expected_attributes.delete "updated_at"

    assert_equal "new title", work.title
    assert_equal expected_attributes, real_attributes
    
    assert_equal 1, work.instrumentations(true).size
    assert_equal 12, work.instrumentations[0].id
  end

  # Testet, ob ein instrumentation in einem Werk geändert werden kann, 
  # dabei wird der Eintrag in der Tabelle Instrumentations_works 
  # nicht gelöscht + neu angelegt (delete+insert), sondern geändert (update).
  # Erwartet: 
  # Kein Fehler, "show" wird angezeigt, 
  # es gibt eine Instrumentierung.
  def test_save_change_instrumentation
    edit 100
    save_in_session :instrumentations_works, 0, {:instrumentation_id => 12}
    expected_attributes = save_success(100, {"title" => "new title"})

    # prüft Ergebnisse
    work = Work.find 100
    real_attributes = work.attributes.dup

    real_attributes.delete "updated_at"
    expected_attributes.delete "updated_at"

    assert_equal "new title", work.title
    assert_equal expected_attributes, real_attributes
    
    assert_equal 1, work.instrumentations(true).size
    assert_equal 12, work.instrumentations[0].id
  end

  # Testet, ob das Löschen der einzigen Instrumentierung möglich ist.
  # Erwartet: 
  # Fehlermeldung wird ausgegeben, die "edit" Seite wird angezeigt
  # und der Eintrag in Instrumentations_works ist nicht entfernt worden.
  # => Test nur erfolgreich, wenn InnoDB benutzt wird, da Rollback notwendig!
  def test_save_without_instrumentation
    edit 100
    remove_from_session :instrumentations_works, 0
    save_failure(100)
    # TODO :check error message
    work = Work.find 100
    assert_equal 1, work.instrumentations(true).size
    assert_equal 11, work.instrumentations[0].id    
  end

  # Testet, ob ein neues Werk mit einer Instrumentierung erzeugt werden kann.
  # Erwartet: 
  # Kein Fehler, "show" wird angezeigt, 
  # es gibt eine Instrumentierung.
  def test_create_with_instrumentation
    new
    row_id = add_to_session :instrumentations_works, 100
    save_in_session :instrumentations_works, row_id, {:instrumentation_id => 12}
    create :title => "some new title", :composer_id => 1
  end

  def test_create_cancel
    new
    post :create, :cancel => 'Cancel'
    assert_response :redirect, :controller => "/", :action => :index
  end

private
  def create(attributes = {})
    post :create, :create => 'Create', :work => attributes
    assert_response :redirect, :action => :show
    follow_redirect
    assert_response :success
    return attributes
  end
  def new
    get :new
    assert_response :success
    assert_select "form[action=/admin/works/create]" do 
      assert_select "input.button[name=create]"
#      assert_select "input.button[name=cancel]"
    end
  end

  def save_success(id, changes = {})
    work = Work.find id
    attr = work.attributes.dup.merge changes
    post :update, :id => id, :save => 'Save', :work => attr
    assert_response :redirect, :action => :show
    follow_redirect
    assert_response :success
    return attr
  end
  def save_failure(id, changes = {})
    work = Work.find id
    attr = work.attributes.dup.merge changes
    post :update, :id => id, :save => 'Save', :work => attr
    assert_response :success, :action => :edit
    return attr
  end
  def edit(id)
    get :edit, :id => id
    assert_response :success
    assert_select "form[action=/admin/works/update/#{id}]" do 
      assert_select "input.button[name=save]"
      assert_select "input.button[name=version]"
      assert_select "input.button[name=cancel]"
    end
  end
  def show(id)
    get :show, :id => id
    assert_response :success
  end
  def remove_from_session(name, row_id)
    short_name = name.to_s.singularize
    old_session = session.dup
    assert_not_nil old_session[name]
    assert old_session[name].size > 0

    post "#{short_name}_destroy".to_sym, "#{short_name}_row_id".to_sym => row_id
    
    assert_not_nil session[name]
    assert_equal old_session[name].size, session[name].size
  end
  def add_to_session(name, work_id)
    short_name = name.to_s.singularize
    old_list = session[name] ? session[name].dup : []

    post "#{short_name}_add".to_sym, :work_id => work_id, :instrumentation_id => 11
    
    assert_not_nil session[name]
    assert_equal old_list.size + 1, session[name].size
    session[name].size - 1
  end
  def save_in_session(name, row_id, params)
    short_name = name.to_s.singularize
    post "#{short_name}_save".to_sym, 
         "#{short_name}_row_id".to_sym => row_id, 
          "#{short_name}_#{row_id}" => params
  end
end