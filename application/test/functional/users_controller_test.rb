require File.dirname(__FILE__) + '/../test_helper'
require 'users_controller'

# Re-raise errors caught by the controller.
class UsersController; def rescue_action(e) raise e end; end

class UsersControllerTest < Test::Unit::TestCase
  fixtures :users

  def setup
    @controller = UsersController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
    
    @request.env['HTTP_ACCEPT'] = 'text/html'
  end

  def test_get_login
    get :login
    assert_response :success
  end
  def test_successful_login
    jka = users(:jkanschik)
    post :login, :login => jka.logname, :password => 'password'
#    assert_response :success
#    assert_template '/'
  end
  def test_wrong_login
    jka = users(:jkanschik)
    post :login, :login => jka.logname, :password => 'wrong'
#    assert_response :success
#    assert_template '/'
  end
  def test_register_start
    # user clicks "register" on login page
    post :login, :commit => 'Register'
    # is redirected to register
    assert_redirected_to :action => :register
    get :register
    assert_response :success
    assert_template 'users/register'
  end
  def test_register_success
    get :register
    assert_response :success
    # user enters registration data and clicks "register"
    post :register, :user => {
      :name => 'test_user', :logname => 'test_login',
      :mail => 'asd@web.de', :orig_password => 'asdfgh',
      :orig_password_confirmation => 'asdfgh'
      }
    # all data passes validation and user is now requested to login
    # the logname input field shoud be filled with the login name
    assert_redirected_to :action => :login, :login => 'test_login'
    get :login, :login => 'test_login'
    assert_response :success
    assert_select "input#login[value=test_login]"
    user = User.find_by_logname 'test_login'
    assert_equal 'test_user', user.name
    # check whether password has been saved correctly
    post :login, :login => 'test_login', :password => 'asdfgh'
#    assert_response :success
#    assert_template '/'
  end
  def test_register_failure
    #logname already in use (see fixture)
    post :register, :user => {
      :name => 'test_user', :logname => 'jkanschik',
      :mail => 'asd@web.de', :orig_password => 'asdfgh',
      :orig_password_confirmation => 'asdfgh'
      }
    assert_response :success
    assert_template 'users/register'
    assert_select 'div#user_logname_div p[class=error]'
    assert_select 'div#user_name_div p[class=error]', 0

    assert_nil User.find_by_name('test_user')
  end
  
  def assert_input_tag(object, name, type = "text")
    id = "#{object}_#{name}"
    assert_select "div##{id}_div" do
      assert_select "label"
      assert_select "input##{id}[type=#{type}]"
    end
  end
  def test_register_template
    get :register
    assert_response :success
    assert_template 'users/register'
    assert_input_tag('user', 'name')
    assert_input_tag('user', 'logname')
    assert_input_tag('user', 'orig_password', 'password')
  end
end
