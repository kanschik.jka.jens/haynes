require File.dirname(__FILE__) + '/../test_helper'

class UserTest < Test::Unit::TestCase
  fixtures :users

  # tests whether users with an existing account (old mysql 4.0 password) can log in 
  def test_login
    assert_not_nil User.authenticate("jkanschik", "kanschik")
  end
  
  # tests whether a new user can be saved and authenticated
  def test_register
    user = User.new
    user.name = "test"
    user.logname = "test"
    user.mail = "asd@qwe.de"
    user.orig_password = "password"
    user.save

    assert_equal 0, user.errors.size
    assert_not_nil user.password    
    assert_not_nil User.authenticate("test", "password")
  end
  # tests whether email verification works.
  # save user if no mail is given. if there is a mail adress, check the format.
  def test_register_no_mail
    user = User.new
    user.name = "test1"
    user.logname = "test1"
    user.mail = ""
    user.orig_password = "password"
    user.save
    assert_equal 0, user.errors.size
  end
  def test_register_invalid_mail
    user = User.new
    user.name = "test2"
    user.logname = "test2"
    user.mail = "asd"
    user.orig_password = "password"
    user.save

    assert_equal 1, user.errors.size
    assert_not_nil user.errors.on(:mail)
  end
end
