require File.dirname(__FILE__) + '/../test_helper'

class WorkTest < Test::Unit::TestCase
  self.use_transactional_fixtures = false

  fixtures :composers
  fixtures :works
  fixtures :work_versions
  fixtures :libraries
  fixtures :libraries_works
  fixtures :libraries_work_versions
  fixtures :instrumentations
  fixtures :instrumentations_works
  fixtures :instrumentations_work_versions


  def test_save
    w = works(:plus_joli_mots)
    assert_nil w.opus
    assert_nil w.version

    w.opus = "1"
    w.save_without_revision!

    assert "1", w.opus
    assert_nil w.version
  end


  def test_create_version_libraries
    w = works(:plus_joli_mots)
    assert_equal 2, w.libraries.size
    assert_equal libraries(:i_mc), w.libraries[0]
    assert_equal libraries(:a_imf), w.libraries[1]

    # create version and check whether libraries_works has been versioned
    w.create_version
    w = Work.find w.id
    assert_equal 1, w.version
    assert_equal 1, w.libraries_works[0].version
    assert_equal 1, w.libraries_works[1].version
  end

  def test_compare_versions
    w = works(:plus_joli_mots)
    w.opus = "opus v1"
    w.tune = "tune v1"
    w.create_version
    assert_equal 1, w.version
        
    w.opus = "opus v2"
    w.tune = "tune v2"
    assert_equal 2, w.libraries_works.size
    lw = w.libraries_works[0]
    lw.comment = "comment version 2"
    w.create_version
    assert_equal "comment version 2", LibrariesWork.find(575).comment
    assert_equal 2, w.version
    assert_equal 2, w.libraries_works(true).size
    assert_equal [575, 5114], [w.libraries_works[0].id || "", w.libraries_works[1].id|| ""].sort!
    assert_equal "comment version 2", LibrariesWork.find(575).comment

    assert_equal ["asd", "comment version 2"], [w.libraries_works[0].comment || "", w.libraries_works[1].comment || ""].sort!
        
    w.tune = nil
    w.create_version
    assert_equal 3, w.version
        
    w.opus = "opus v4"
    w.tune = "tune v4"
    # remove a library in the same way as it is done in the controller
    old_lw = w.libraries_works.dup
    old_lw.delete_at 0
    LibrariesWork.without_locking do
      LibrariesWork.without_revision do
        old_lw.each {|t| t.save}
        w.libraries_works = old_lw
      end
    end

    w.create_version!
    assert_equal 1, w.libraries_works(true).size
    assert_equal 4, w.version
  
    diff = w.compare_versions(1, 2)
    assert_equal 3, diff.size
    assert_equal({:old => "opus v1", :new => "opus v2"}, diff["opus"])
    assert_equal({:old => "tune v1", :new => "tune v2"}, diff["tune"])
    assert_equal 1, diff["libraries"].size
    assert_equal nil, diff["libraries"][0][:old].comment
    assert_equal "comment version 2", diff["libraries"][0][:new].comment 

    diff = w.compare_versions(1, 3)
    assert_equal 3, diff.size
    assert_equal({:old => "opus v1", :new => "opus v2"}, diff["opus"])
    assert_equal({:old => "tune v1", :new => nil}, diff["tune"])
    assert_equal 1, diff["libraries"].size
    assert_equal nil, diff["libraries"][0][:old].comment
    assert_equal "comment version 2", diff["libraries"][0][:new].comment 
  
    diff = w.compare_versions(2, 3)
    assert_equal(
      {
       "tune" => {:old => "tune v2", :new => nil}
      }, diff)

    diff = w.compare_versions(3, 4)
    assert_equal({:old => "opus v2", :new => "opus v4"}, diff["opus"])
    assert_equal({:old => nil, :new => "tune v4"}, diff["tune"])
    assert_equal 1, diff["libraries"].size
    assert_equal "comment version 2", diff["libraries"][0][:old].comment
    assert_equal nil, diff["libraries"][0][:new]
  
  end  

  def test_compare_with_previous
    w = works(:plus_joli_mots)
    w.opus = "opus v1"
    w.tune = "tune v1"
    w.create_version
    assert_equal 1, w.version
        
    w.opus = "opus v2"
    w.tune = "tune v2"
    w.create_version
    assert_equal 2, w.version
        
    w.tune = nil
    w.create_version
    assert_equal 3, w.version
        
    w.opus = "opus v4"
    w.tune = "tune v4"
    w.create_version
    assert_equal 4, w.version
  
    diff = w.compare_with_previous(1)
    assert_equal(
      {
      }, diff)
  
    diff = w.compare_with_previous(2)
    assert_equal(
      {"opus" => {:old => "opus v1", :new => "opus v2"},
       "tune" => {:old => "tune v1", :new => "tune v2"}
      }, diff)

    diff = w.compare_with_previous(3)
    assert_equal(
      {
       "tune" => {:old => "tune v2", :new => nil}
      }, diff)

    diff = w.compare_with_previous(4)
    assert_equal(
      {"opus" => {:old => "opus v2", :new => "opus v4"},
       "tune" => {:old => nil, :new => "tune v4"}
      }, diff)
  
  end
  
  
  
  def test_country_array
    w = works(:plus_joli_mots)
    assert_equal('YNNYNNN', w.country)
    assert_equal(%w(Italy England), w.country_array)
    
    w = Work.new

    0.upto 2**Work::COUNTRIES.size do |i|
      array = []
      w.country = ""
      Work::COUNTRIES.each_index do |j|
        w.country += (i[j] == 1 ? 'Y' : 'N')
        array.push Work::COUNTRIES[j] if i[j] == 1
      end
    
      assert_equal array, w.country_array
    end
  end

  def test_update_country
    w = works(:plus_joli_mots)
    assert_equal('YNNYNNN', w.country)

    w.update_country(nil)
    assert_equal('NNNNNNN', w.country)

    w.update_country({"France" => 'Y'})
    assert_equal('NYNNNNN', w.country)

    w.update_country({})
    assert_equal('NNNNNNN', w.country)

    w.update_country({"Italy" => 'Y', "France" => 'Y', "England" => 'Y'})
    assert_equal('YYNYNNN', w.country)
    
    w.reload
    assert_equal('YNNYNNN', w.country)
  end
  
  
  def test_diff_arrays
    w1, w2, w3 = LibrariesWork.new, LibrariesWork.new, LibrariesWork.new
    w1.library_id, w2.library_id, w3.library_id = 1, 2, 10
    w1.work_id = w2.work_id = w3.work_id = 1
    w1.comment, w2.comment, w3.comment = "t1", "t2", "t3"
  
    assert_equal([], Work.diff_arrays([], []))

    old_array = [w1, w2, w3]
    new_array = [w1, w2, w3]
    assert_equal([], Work.diff_arrays(old_array, new_array))
  
    old_array = [w1, w2, w3]
    new_array = [w1,     w3]
    assert_equal(
      [{:old => w2, :new => nil}],
      Work.diff_arrays(old_array, new_array))
  
    old_array = [w2]
    new_array = []
    assert_equal(
      [{:old => w2, :new => nil}],
      Work.diff_arrays(old_array, new_array))
  
    old_array = []
    new_array = [w2]
    assert_equal(
      [{:old => nil, :new => w2}],
      Work.diff_arrays(old_array, new_array))

    old_array = [w1,     w3]
    new_array = [w1, w2, w3]
    assert_equal(
      [{:old => nil, :new => w2}],
      Work.diff_arrays(old_array, new_array))

    w3_new = LibrariesWork.new
    w3_new.library_id = 10
    w3_new.work_id = 1
    w3_new.comment = "t3_new"

    old_array = [w1, w2, w3]
    new_array = [w1, w2, w3_new]
    assert_equal(
      [{:old => w3, :new => w3_new}],
      Work.diff_arrays(old_array, new_array))

  end
  
end
