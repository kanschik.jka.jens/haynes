class CreateAddCollections < ActiveRecord::Migration
  def self.up
    create_table :collections, :force => true do |t|
      t.column :label,      :string
      t.column :name,      :string
      t.column :info,      :longtext
      t.column :created_at, :timestamp
      t.column :updated_at, :timestamp
    end

    create_table :collections_works, :force => true do |t|
      t.column :work_id,     :integer
      t.column :collection_id,     :integer
      t.column :created_at, :timestamp
      t.column :updated_at, :timestamp
    end

    create_table :collections_references, :force => true do |t|
      t.column :reference_id,     :integer
      t.column :collection_id,     :integer
      t.column :created_at, :timestamp
      t.column :updated_at, :timestamp
    end


  end

  def self.down
    drop_table :collections
    drop_table :collections_works
    drop_table :collections_references
  end
end

