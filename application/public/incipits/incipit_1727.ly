\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 4/4
  c'4. c8 b4. b8
  a4. d8 g, c16 b  c d c b
  a gis a b a c b a gis8 a16 gis a8[ b]
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro"
  \time 2/2
  a4. c8  d d e e
  c a e'2 dis4 e4. e8 f16 e d c b8 e
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Largo"
  \time 3/4
  \partial 4
  c'4
  fis, fis e dis b b'
  a8 g fis4. e8 e4 gis gis
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Vivace"
  \time 3/4
  a8 gis a b c d
  e a g f e d
  c4 e8 d c b
  c4 b8 c a4
}
