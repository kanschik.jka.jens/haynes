 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro con brio"
  \time 2/2
  \partial 8
  c8
  g'4. e8 c'4. g8
  f4. d8 d'4. f,8
  f e g4 r8 c, d e
  e d f4 r8
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Adagio cantabile"
  \time 4/4
  \partial 8
  c16 es
  g es c8 r16 c d es f d b8 r16 d es f
  f es d c c'8 fis, g g, r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Tempo di Menuetto"
  \time 4/4
  \partial 4
  c8 e
  g4. g8 a f
  \grace f8 e4. e8 f d
  c c' c4. b16 a a4 g8 g a e
  \grace g8 f4.
}
