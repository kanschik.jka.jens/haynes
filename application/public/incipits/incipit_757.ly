 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. "
  \time 4/4
  \partial 8
  a8 
  e'4 r8 cis32 b a16 fis'4 r8 fis32 gis a16
  e4 r8 cis32 b a16 fis'4 r8 fis32 gis a16
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. "
  \time 4/4
  \partial 8
  r8 
  \set Score.skipBars = ##t
   R1*13 
    r2 r4 r8 a
    e'4 r8 cis32 b a16 fis'4 r16 a,16 d fis
    e4 r8 cis32 b a16 d cis b cis d b e d
    d8 e fis d
}


\relative c'' {
  \clef treble
  \key e\major	
   \tempo "2. Adagio"
  \time 3/4
    e4 b cis
    gis2 a8. gis32 a
    b2 cis4
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "3. Allegro con moto"
  \time 4/4
  r8 a a a  b b4 cis16 d
  cis4 fis b, e
  a,8 b16 cis d cis b a gis8 e a4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}