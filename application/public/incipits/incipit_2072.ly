\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro Moderato"
  \time 4/4
  \partial 8
  d16. es32
  f8 bes,32 a bes c bes8 bes' a16 c f,4 c16 d
  es8 a32 g a bes c8 es,
}



\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegretto"
  \time 3/4
    d8 d4 d16 es32 d g8 g
    g4. d16 bes bes g g d
    es8 es8. es16 d es
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegretto"
  \time 3/8
    f8~ f16 d32 f \grace f16 es16 es32 d
    \grace c16 bes16. a32 bes4
    g'8. g16 bes g
    g8 f r
}


