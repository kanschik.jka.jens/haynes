\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  \partial 8
  d8
  d d d d  d e16 fis g8 g
  fis16 g e fis d e c d b a g8 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*18 
    r2 r4 r16 d e fis
    g8 d b d g,4 r16 d' e fis
     g fis g d a' g a d, b' a g fis g d e fis
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 3/4
  d4 g g, es'2.~ es4 d8 c bes a
  bes4 g bes'~ bes a g
  fis8 es d4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 2/4
  g'4 d e16 fis g4 g,8
  c16 d e4 d16 c
  b8 g b d
}