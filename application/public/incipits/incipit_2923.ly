\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Thema. Andante"
  \time 6/8
   \partial 8
   g'8
   e4 e8 d4 d8 
   c4. r4 g'8
   g4 c,8 a'4 a8
   d,4. r4
}

