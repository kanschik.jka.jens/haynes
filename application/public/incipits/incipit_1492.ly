\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 3/4
    d4 g4. e8
    d g,4 a16 b \grace d8 c8 b16 a
    b8 e \grace d8 c2
    b4 r r
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key g\major
                      \tempo "2. Fuga. Alla breve"
                      \time 2/2
                      R1 R
                      r4 g g g
                      fis g8 a  b a g fis
                      e4 a8 g a4 e
                      fis d g2~
                      g fis g r
                      % Voice 1
                  }
\new Staff { \clef "alto" 
                     \key g\major
                        % Voice 2
                      r2 d
                      g b,
                      e1~ e2 d
                      r c~ c b
                      a1
                      g4 b8 a b4 g
                  }
>>
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Vivace"
  \time 3/4
      r8 d g b \grace b16 a8 g16 fis
      g4 d e
      d a4. b16 c
      b8 g'16 fis g8 b16 a g8 fis16 e
      d4 \grace d4 c2
      b4
}

