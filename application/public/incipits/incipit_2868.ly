\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro non molto [Tutti]"
  \time 4/4
  f,2 f4~ f8. f16
  f2 f'
  e32 d c8.~ c4 r 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro non molto [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*16 
   f,4~ f16 g32 a bes c d e  f8 c bes16 a g f
   c4~ c16 c32 d e f g a  bes8 d c16 bes a g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andante [Tutti]"
  \time 3/4
  r8 g' g8. f32 g c,8 e
  d g, g8. f32 g g,8 f'
  e g' g8. f32 g c,8 g'
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andante [Solo]"
  \time 3/4
     \set Score.skipBars = ##t
   R2.*11 
   c8. b32 c c8. b32 c d8 g,
   c8. b32 c c8. b32 c f8 d
   e16 c b c  g' c, b c  g' c, b c
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro molto [Tutti]"
  \time 2/4
  f,16 g a bes c8 c, 
  d16 e f g a8 a,
  bes16 c d e f g a b
  c8 c, r4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro molto [Solo]"
  \time 2/4
     \set Score.skipBars = ##t
   R2*31 
   f,8 c' \grace c16 bes4
   a8 d \grace d16 c4
   bes8 g' \grace f16 e4
   f16 c bes a a'4
   g16 e d c bes4~
   bes16 e g f  e d c bes
   a g f8 r
      
}
