\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "6. Andante"
  \time 4/4
      \override TupletBracket #'stencil = ##f
    g'4 g,  \times 2/3 {r8 g a}   \times 2/3 {bes[ a g]}
    d'4 d,  \times 2/3 {r8 fis g}   \times 2/3 {a[ bes c]}
    bes4 \grace a8 g4  \times 2/3 {r8 g a}   \times 2/3 {bes[ c d]}
    es4 fis,  \times 2/3 {r8 fis g}   \times 2/3 {a[ bes c]}
    bes4 d

} 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "7. Vivace"
  \time 3/4
        bes'4 d, f
        bes, \grace a8 bes2
        c8 d es4 c
        d \grace c8 bes2
        f8 f' f, g a bes
        c d es c d es
        f4 g8 f es d c2.
 } 
