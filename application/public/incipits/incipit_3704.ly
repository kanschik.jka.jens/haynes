\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro assai"
  \time 4/4
    es,1 es'2 es8 bes \grace as16 g8 f16 es
    f4 g \grace bes8 as2
}
