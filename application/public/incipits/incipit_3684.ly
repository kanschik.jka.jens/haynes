 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 4
  c4
  f2 f
  f4 f16 e f g r2
}
