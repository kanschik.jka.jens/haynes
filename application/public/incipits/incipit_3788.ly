 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 4/4
  f8[ c f g] a g16 a f g a bes
  c bes a c  bes a g bes a8 f r4
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Adagio"
  \time 3/4
  a4 f' f
  f e r
  r e d cis d2
  e4 a, r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Vivace"
  \time 3/4
  c4 d e f g8 f e4
  f8 g g4. f16 g
  a4 g8 a f4
}

