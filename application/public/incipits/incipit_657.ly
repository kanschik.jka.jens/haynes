
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I."
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
bes8 bes bes bes f'4 f
g8 bes16 a bes8 g f bes16 a bes8 d,
es d16 f c8 f d[ c]
 
}



\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 4/4
f4 r8 f8 g f r8 d8
es d r8 f8 g f es d 
c4. d16 c bes4. c16 bes
a8 bes4 f'8 g16 f es d 
c8. bes16 bes8[ bes]

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/8
bes16 a bes c d es
f8 f c
d es16 d c bes 
c8 f c
d es16 d c bes 
a8 \grace bes8 bes4 \grace {a16[ bes]} c4 r8
 
}
 

 

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allegro"
  \time 2/4
e8[ b g e]
dis'4. e8 fis16 e dis cis b a g fis
g8[ e b' e] 
fis16 e dis cis b a g fis
g8 e



 
}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "2. Adagio"
  \time 3/4
b4 g8 a b e,
dis4. fis8 g a 
g4 \grace fis8 e4 c'
c fis, b8. b16
b4
e,  
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/8
b8 g16 a g a
b8 e,16 fis e fis
g8 a b
g4 fis8
g16 b a b fis b
g b a b fis b
g8 a16 b a g fis4
 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III."
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 4/4
a4 gis a8 gis16 fis e8 e
fis d b e cis b16 a cis8 b
cis e b e d16 cis b cis a8 b

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Andante e Pianissimo"
  \time 3/4
e4 cis d 
e cis fis
e d4. cis8 
b2 b4
e d8 e cis e
d4 cis8 d b d 
cis b a b a b 
cis 4 a8 b e,4
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Allegro"
  \time 3/8
e8 cis a
fis'16 e d cis b a 
gis8 a b
cis b16 cis a8
e' e, e'
e16 dis cis b a gis 
fis8 

}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV."
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
\partial 8
d8 a' a a a 
d cis16 b a8 a
d cis16 b a8 a
b g e a 
fis d4 e8
fis a4 g16 fis
e8


}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Adagio"
  \time 3/4
fis4 b4. cis8
ais2 b4
e,8 fis fis e d cis
d e e d cis b 
ais4 cis8 d e4 
e fis8 e d cis \grace cis8 d4 b

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 6/8
d4. a
d a'4 a8
b a b e, a g 
fis a a e a a
fis a a e a a 
fis g fis e fis d
 

}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
fis4 r8 b8
fis b b, cis
d16 cis b cis d8 cis
b16 cis b cis d8 cis
b cis d cis16 b 
ais8 fis' fis fis

 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio e piano"
  \time 3/4
fis2 e4
d8 e d e fis4
fis e d
cis2 cis8 d
e4 e8 d
cis4
d8 cis d e fis d
g4 

 
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 6/8
\partial 8
b8 b4 fis8 g fis e 
d4 cis8 b4 cis8
d4 e8 fis e d 
cis fis fis e fis e
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI."
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro "
  \time 2/4
\partial 8
b16 c
d8[ g, g a]
b[ g g a]
b16 a b c d e fis g
fis 8 d r8
 
}


\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Adagio et piano"
  \time 3/4
g4 d8 c d e
d4 b c
d4. e8 d4
c b4. c8 a2 a4
d d8 c b4 
c c8b a4 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/8
g8 d b
g16 fis g a b c 
d8 e fis 
g4 b32 a g a 
b16 g d g b g
a fis d fis a fis
g e cis e g e 
fis8 e16 fis d8

 
}

