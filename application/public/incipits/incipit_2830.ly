\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 4/4
  d4 a'8. cis,16 d8 d, r d'
  e16.[ d32 e8.] b'16 a g fis e d8 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  a8 d4~ d16 a' fis d e cis d8 r a b4 g'8 b, b a r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Siciliana"
  \time 12/8
  d8. e16 d8 d4 d8 d8. cis16 d8 r r d
  \grace f8 e4 bes'8 a4 g8 f8. e16 d8 r r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Minuet. Allegro"
  \time 3/4
  \grace {fis16[ g]} a4 g fis
  \grace fis8 e4 d8 cis d4
  b g' \grace fis8 e4
  \grace d4 cis2 d4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Andante"
  \time 4/4
  \partial 8
  e8
  \grace d16 c8 \grace b16 a8 a a \grace b16 a16 gis a8 r a
  b c16 d \grace d16 c8. b16 \grace d16 c8 b16 a r8
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro con Spiritto"
  \time 12/8
  \partial 8
  e8
  a e d  c b a  d e f  e d c
  b c d c b a  gis a b e,4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Adagio"
  \time 3/4
  \partial 4
  e4
  e d8 f e d
  c b a e' f c
  b a b f' e b
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Allegro"
  \time 3/4
  cis4 \grace e8 d4 \grace cis8 b4
  \grace a8 gis2 a4
  b cis d
  cis16 b a8 a2
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 3/4
  c2. d
  c8 e \grace e4 f2
  bes,4 g' bes,
  \grace c4 bes4 a r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 12/8
  \partial 8
  c8
  f e f a4 c,8 d c d f4 a,8
  bes g f e c' bes a g f r r
}
  
  \relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Andante"
  \time 4/4
  d1~ d
  \grace f8 e2. f8 g
  \grace g8 f8. e16 d2 f4
  \grace f8 e4 a,2
}
  
  \relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Minuet"
  \time 3/4
  c2. bes
  a4 \grace c8 bes4 \grace a8 g4
  a8 g f g a bes
  c2
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 4/4
  \partial 4
  d4 g g g fis16 e d8
  \grace { c16[ d]} e4 d r g
  g fis8. g16 a4 c,
  \grace d8 c4 b r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 16
  d,16
  g8. b16 \grace b8 a8. c16 \times 2/3 { b16[ c d]} \times 2/3 { e16[ fis g]} \grace a,8 g4
  a8. e'16 d c b a
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Minuet. Allegro ma non troppo"
  \time 3/4
  g4 b8 a g fis
  g d g b d g
  \grace { c,16[ d]} e4 d c
  \grace c8 b2 a4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio Cantabile"
  \time 4/4
  d2. d4
  e8 fis16 g \grace g8 fis8. e16 \grace g16 fis8 \grace e16 d8 a'4~
  a16 a g fis g4~ g16 g fis e fis4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro assai"
  \time 4/4
  d4 fis8. g32 a \grace a8 g4. fis8
  e d4 cis8 d a r4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Tempo di Minuet"
  \time 3/4
  d2. e4~ e8. fis32 g fis4
  b,8 e \grace d8 cis2
  d4~ d8. cis32 b a4
}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Siciliana"
  \time 12/8
  \partial 8
  g'8
  \grace f8 e8. d16 c8  c4 a8 g4 b8 c4.
  d8. e16 f8 f4 f8 \grace g8 f4 e8 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4
  c8 e16. f32 \times 2/3 { g16[ f e]} \times 2/3 { d16[ c b]} c8 c' r g,
  \times 2/3 { a16[ b c]}  \times 2/3 { d16[ e f]}
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Minuet"
  \time 3/4
  g4 c d
  \grace c8 b2 c4
  a g a8 b16 c
  \grace g8 f2 e4
}
