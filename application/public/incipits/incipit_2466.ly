\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Simphonie concertante " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro Moderato"
  \time 2/2
  e4 e2 f8 e
  e d c4. d8 e f
  g4 g2 a8 g
  g f e4
  

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 2/4
  \grace b8 c8 c4 f16 e
  e d c4 d16 c
  c bes a4 g8
  bes16 a d c c8
 

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo Moderato"
  \time 2/4
  \partial 4
  e8 e16 g
  g f e f d8 d16 f
  f8 e g g16 e
  a8 a16 g g f f e
  \grace e16 d8[ g,]

}


