\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "Aria	"
  \time 4/4
  r4 r8 d g d b g
  e'4. d16 c d8 g,
}
