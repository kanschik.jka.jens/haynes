 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Hautbois"
  \key g\major
   \tempo "o.Bez."
  \time 4/4
    r8 g16[ a b a b c] d8 g,16[ a b a b c]
    d8[ g,]
}



\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key g\major
   \tempo "o.Bez."
  \time 4/4
     \set Score.skipBars = ##t
     R1*4
     r2 d2~ d2. g,4
     r8 g16 a b g b c d[ b]
}