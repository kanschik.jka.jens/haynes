
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Les Dances Amuzantes "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. ohne Bez."
  \time 2/4
\partial 4
c16 d e8
c g' g f
e d e16 f g8
g4 f8 e d4 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Dances D'auvergne. IIe. Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. ohne Bez."
  \time 2/2
\partial 2
e4 c c e e f
e2 a4 g
f e d2 c2
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Vaudeville. IIIe. Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. ohne Bez."
  \time 2/2
\partial 2
c4 es d g g f
es d es d 
f es d es c2



}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "La Duranti. IVe. Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Tres lentem.t"
  \time 2/2
\partial 2
 e8 d c b
c4 e8 f g f e d 
e4 d f8 e d c 
d g f e d c b a g2

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Muzette La Villelume. Ve. Suite"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Tendrem.t et louré"
  \time 2/2
\partial 2
d4 g
fis4. e8 d c b a 
b4 \grace a8 g4 b8 c d e 
d4 c b4. c8 a2
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Muzette. VIe. Suite"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Gracieusement"
  \time 2/2
\partial 2
d8 c b c 
d4 g, c b
a g a8 b c d 
b c d e c4. b8 a2
 
}

