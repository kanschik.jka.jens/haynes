\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 2/2
  r2 g4. c8
  \grace c4 b4. a8 \grace a4 g4. f8
  \grace f4 e2  g'8 e d cis
  \grace cis4 d2  f8 d c b
  \grace b4 c2
}
