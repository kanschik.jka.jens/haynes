
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nouveau Recueil de Noels [37 Pieces]"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Ou s'en ront des guays berger"
  \time 2/2
\partial 2
e4 e 
f f e e 
d d e e 
f f e2
d
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Noel pour Lamour de marie"
  \time 3/4
g4 c d
e2.
g4 g f 
e2 d4 
f f f
e2. g4 f e d2.
 
}
