 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'Gönne nach den Tränengüssen'" 
    }
    \vspace #1.5
} 

 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Hautbois solo"
  \key c\minor
   \tempo "o.Bez."
  \time 4/4
  r4 g'8 as16 f g es c8 r16 g' as f
  g es c8 g' c as g f d
  b4 d8 es16 c b8 c d es16 c
}

\relative c'' {
  \clef bass
  \set Staff.instrumentName = #"Raymondo"
  \key c\minor
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
     R1*4
     r4 g,8 as16 f g[ es] d c g'8 as16[ f]
     g[ es] d c g'8 c as8. g16 f[ es d] es
     b4 r r2
}