 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 3/4
  \partial 8
  g8
  c4~ \times 2/3 { c8[ d e] } \times 2/3 { d8[ c b] }
  b4 c r8 g
  e'4~ \times 2/3 { e8[ f g] } \times 2/3 { f8[ e d] }
  e16 d c d c4 r8
}
