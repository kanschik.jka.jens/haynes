
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 4/4
  \partial 4
  fis8. g16 a4 a a4. b8
  \grace a8 g4 g r4 \grace a8 g8 fis16 e
  a4 fis8 r16 d16 g4 e8 r16 cis16
  d8. e32 fis e4 r4 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Menuetto non tanto Presto"
  \time 3/4
   fis2 g8. e16
  d8. a16 fis'8. e16 e4
}



