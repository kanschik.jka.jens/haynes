\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Maestoso"
  \time 4/4
  \partial 8
  g'16 fis
  g fis a g b a g fis \grace fis8 e4 d8 e16[ d]
  e d fis e g fis e d \grace d8 c4 b8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Menuetto"
  \time 3/4
  d2 \grace fis8 e d
  d4 a b
  c2 \grace e8 d c
  \grace d8 c4 b r
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Rondeau. Allegretto"
  \time 3/8
  \grace c8 b a b
  \grace d8 c4 b8
  \grace d8 c4 b8
  \grace b8 a4 g8
  \grace c8 b a b
  \grace d8 c4 b8
  \grace c8 b a b
  \grace b8 a g a
  g4 r8
  }

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Maestoso"
  \time 4/4
  d2 cis8 d e cis
  d2 cis8 d e cis \grace b'8 a4. g16 a b4 a
  \grace a8 g4 fis r2
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Minuetto Larghetto"
  \time 3/4
  \partial 8
  f8 f8. bes16 a4 g
  \grace a8 g8. f16 f4 r
  es8. g16 f4 es
  \grace f8 es8. d16 d4 r
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Rondeau. Allegro"
  \time 2/4
  a4 a a fis8 g
  a4 d8 b a4 fis
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro assai"
  \time 2/2
  c2 e4 g
  c4. b16 a g8 f e d
  c4 c e c g' g, g2
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Largo assai"
  \time 3/4
  d4. \grace f8 es16 d d8 d
  fis8. a16 c,4 r
  d4. \grace f8 es16 d d8 d
  g8. bes16 g4 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Minuetto"
  \time 3/4
  e4 f8. d16 g4
  e f8. d16 g4
  c c b 
  \grace b8 a2 g4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Rondeau. Allegretto"
  \time 2/4
  c4 e8 g
  b,4 d8 f
  \grace f8 e4 d8 c
  d4 g,
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Largo assai"
  \time 3/4
  \grace fis8 e2 \grace fis8 e8 d16 cis
  b4 a a16 cis e a
  a8 gis fis e d cis
  cis16 d dis e b4 r
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 4/4
    a4 cis e r
    e \grace a8 gis8. fis32 gis a4 r
}


\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Minuetto"
  \time 3/4
    a8. a16 a4 r
    a'8 r e r cis r
    a8. a16 a4 r
    e'8 r cis r a r
    a d d cis \grace g'8 fis e
}


\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Rondeau"
  \time 2/4
  e8 r e r
  e2
  e8 r e r
  e2
  a16 gis a b cis b a gis
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
  f8 bes
  f r r4 r4 f8 bes
  f r r4 r4 d8 d
  \grace f8 es d \grace f8 es d \grace f8 es d f16 g a bes
  f d c bes es c bes a bes4 r
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "2. Amoroso Expresione"
  \time 3/4
    f4 as g8. e16
    f4 as g8. e16
    f4 c bes
    bes2 as4
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Minuetto "
  \time 3/4
  f8. g16 a8. bes16 c8 r
  \grace f,8 e2 f4
  f8. g16 a8. bes16 c8 r
  \grace c8 bes2 a4
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Rondeau. Presto assai"
  \time 2/4
  bes8 bes4 d8 bes bes4 d8
  bes8 bes4 d8 bes bes4 d8
  c8 c4 c8 d8 d4 es8 d4 \grace d8 c8. bes32 c
  bes4 r
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro assai"
  \time 4/4
    f2 a4 c
    e, g bes r
    c,2 e4 g f a c r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Largo"
  \time 3/4
    a'8 f r d r f16 d
    e8 a, r cis r e16 g
    f8 a r f r f16 d
    e8. d32 e a,4 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Minuetto"
  \time 3/4
  c'4. b16 a g8 a
  g4 f e
  d'4. c16 b a8 g
  g4 f e
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Rondeau. All. assai"
  \time 2/4
    f8 r g r
    a r f r
    bes r a r
    \grace a8 g4 f
}
