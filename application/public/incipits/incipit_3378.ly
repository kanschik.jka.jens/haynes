 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 4/4
    es,8 es4 es es es8
    es4 r
    bes''4. g8
    es4 g g g as r r8 as, as as
}


\relative c'' {
  \clef treble
  \key as\major	
   \tempo "2. Adagio"
  \time 3/4
  es4. f16 g as8 as
  g4 r <g bes>8. <bes des>16
  <bes des>4 <as c>8 r bes8. as16
  \grace as8 g4
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Rondo. Allegretto"
  \time 2/4
  \partial 8
  bes'8
  bes4. es8
  g,4. c8
  f,4 bes
  as16 g f g es8
}

