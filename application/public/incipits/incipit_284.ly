\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
  bes4 d, f bes d f bes r8 bes,
  a a a16 bes c a bes8 bes bes16 c d bes
}
