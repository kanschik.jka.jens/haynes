\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 3/4
    c'4 r8 c a e
    r b' b e, r b'
    a}
      
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro"
  \time 4/4
    r4 a' gis b
    e, a2 gis4
    a8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Adagio"
  \time 4/4
  c'4. b16 a g8 e c'4~
  c8 b16 a g a g f e4
}
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Vivace"
  \time 3/4
    r8 e e e e16 d e f
    g8 c, c c c16 b c d
    e8 a, a
}
