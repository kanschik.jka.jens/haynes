\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Es kömmt ein Tag, so das Verborgne richtet [BWV 136/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key fis\minor
   \tempo "o. Bez."
   \time 4/4
   r4 r8 cis8 cis16 a fis d' cis b a gis
   a8 fis r8 e'8 e16 cis a fis' e d cis b
   cis8 a r8
   
   
   
   
  
   
   
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key fis\minor
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
   R1*10 r4 r8 cis8 fis8. gis16 a gis b a
   a4 r8
}



