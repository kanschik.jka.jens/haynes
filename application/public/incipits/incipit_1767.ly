\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 3/4
  e8 d e f g4
  a a2 g8 f g a g4
  f f2
}
