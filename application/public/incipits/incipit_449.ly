
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "La Matignon - Premier Gentillesse "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
  \tempo "1. Gayment"
  \time 2/4
  c4 d b4. c8 d d16 e f8 f f e4 g8 f e d g b, c d e f4 e
 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement"
  \time 2/2
 \partial 2
 r2 r2 g4 f8 g \grace f4 e4 \grace d4 c4 c4. b8 c4. d8 e4 d8 c b2
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gayment"
  \time 3/8
 c8 c16 d e8 d16 c b a g a b c d e f g e4
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "La Racine - Deuxieme Gentillesse "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Vivement"
  \time 3/4
 e8 c c c c c d c c c c c d c c c c c f4. g8 e4 
 
 
}
 
\relative c''' {
  \clef treble
  \key g\minor
   \tempo "2. Gracieusement"
  \time 3/8
 \set Score.skipBars = ##t
   R4.*1  g16 f es8 d c g c b4. bes16 a g a bes c d4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gayment"
  \time 2/4
 \partial 8
 g8 c16 b c d c d b c d c d e d8 f f e e g g 
 
 
}

 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "L'Arnaud - Troisiéme Gentillesse "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gayement"
  \time 2/4
 e8 e  e d c4 b d8 d d c b4 c e8 e e d c4 g
  
 
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Movement de Louvre"
  \time 3/4
\partial 4.
\set Score.skipBars = ##t
 r4 r8  R2.*1 r4 r8 c8 d4 e4. d8 c4 d4 g, g' g4. f8 e4   
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gayement"
  \time 3/8
 c16 d e f g8 g g g a g f e4 d8 \grace {d32[ e]} f4
 
 
 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "La Le Maitre - Quatrieme Gentillesse "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Gayement"
  \time 2/4
 \partial 8
 g8 g c e, d c16 g a b c d e f g8 c e, d
 
 
 
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Gratieusement"
  \time 2/2
 \partial 2
 c4 d e e e d c b c d g, c b4. c8 c2

 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gayement"
  \time 6/8
 \partial 8
 e8 c4 c8 c b c d4.~ d8 c d e f e e d c d c d g,4
  
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    " La Plainville - Cinquiéme Gentillesse "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Moderement"
  \time 3/4
 \partial 4
 d4 g fis8 g a fis g4 d e \grace d8 c4 \grace b8 a4 
 
 
 
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Gratieusement "
  \time 3/8
\set Score.skipBars = ##t
   R4.*2 d8 \grace c8 b8 e d c16 b a c b8 \grace a8 g8 g' g4 fis8
 
}
		

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Gayment"
  \time 2/2
  \partial 2
g4 fis8 e d4 c b a b g b c d e fis g fis d b c8 d e4
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "La Casa dolce - Sixiéme Gentillesse "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Gayement"
  \time 2/4
 \partial 8
d8 d g g fis g4. d8 e16 fis g8 c, c c8 b4 d8 d a a b b8 a4
 
 
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Gratieusement"
  \time 2/2
  \partial 2
 g4 c b8 c d e a, b c a b4 \grace a8 g4 g c b8 c d e a,4. g8 g2
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gayement"
  \time 3/8
g16 a b8 c d g4 g8 fis e d4.
 
}
