 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "o.Bez."
  \time 6/8
  r16 a c e a e  f a d,8. c32 d
  e16 a gis a f e
}
