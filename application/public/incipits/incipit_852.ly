\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo Tutti]"
  \time 3/4
    r4 b,8 c c8. b32 c
    d4 e8. fis32 g fis8. g32 a
    g4 e'2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*5
   r4 fis,8 g g8. fis32 g
   a4 b8 a b g
   a4 c2
   b4
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
    d4. b8 g4. g8
    a a a b16 c
    b8 d a b16 c
    b8 d a b16 c b8 d a b16 c
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Siciliana"
  \time 3/8
      e8. f16 e8
      d4 e16. f64 g
      f8. e16 f8
      e4
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 3/8
   \set Score.skipBars = ##t
    r8 g' d
    b4 c8 
    d c16 b a g fis8 e' d
    b4 a8
}