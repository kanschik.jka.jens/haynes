\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria with Trio [BWV 194a/11]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key bes\major
                      \tempo "o. Bez."
                      \time 3/8
                      % Voice 1
                   d16 es f8 f,
                   bes4 bes'8
                   g f es
                   d4 c16 d 
                   es f es d c bes
                   a8 g16 f bes8
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key bes\major
                     \set Score.skipBars = ##t
                        % Voice 2
                 bes16 c d es f8
                 g, f f'
                 es d c 
                 bes4 a8
                 bes4 es8
                 c8 bes16 a bes8
                       
                        
                
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 3"
                     \set Staff. shortInstrumentName = #"Ob 3"
                     \key bes\major
                     \set Score.skipBars = ##t
                        % Voice 3
               f8 bes c
               d4 bes8
               bes4 a8
               f4 f8
               bes a g
               f4 f8
                       
                        
                
                  }
>>
}


