 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Andante Lento"
  \time 6/8
  \partial 4.
    es,8 es es
    es4 r8 as8. bes16 as r
    g8. as16 g r es8 es es
    es4 r8
}


\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key es\major
                      \tempo "2. Allegro ma non Presto"
                      \time 2/4
                      \partial 8
                      % Voice 1
                      bes8
                      bes r r bes
                      bes r r bes
                      bes r r bes
                      bes r r bes
                      es16 d c bes bes8 bes
                      \grace bes16 as4 g
                      f16 g as g f g es f
                      \grace es16 d8 c16 d bes8 bes
                  }
\new Staff { \clef "tenor" 
                     \key es\major
                     % Voice 2
                     r8
                     es2
                     d4 bes
                     as2 g4 es
                     r2
                     r4 es'~
                     es f~
                     f r
                  }
>>
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Tempo di Minuetto"
  \time 3/4
  bes2 \grace d16 c8. bes16
  \grace bes16 as4 g f
  es es f
  \grace as16 g4 f r
}

