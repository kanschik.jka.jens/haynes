\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  f8 bes, a bes   f' bes, a bes
  g'16 es d es f d c d  es c bes c d8 bes
}

