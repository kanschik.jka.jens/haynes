\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key e\minor
    \time 3/2
    \tempo "1. o.Bez."
      r2 c4 b a b
      a4. a8 b4 a g fis
      g2 e'2. g4
      c, b c2. c4
      c b b2
}

 