\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio I " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Prelude"
  \time 4/4
r8 g8[ g8. f16] e8.[ e16 e8. d16]
c8.[ b16 c8. d16] b8.[ b16 b8. a16]
g8.[ e'16 e8. e16] e8.[ d16 d8. d16]
d4 c

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Sarab.de"
  \time 3/4
e4 e4. e8
e4 d8 c b a
g4 c4. d8
b2 c4
g8 g c4 cis8 cis
d2 

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Fantaisie"
  \time 3/4
r4 r8 g8 c4
b4. a8 g f
e[ f e8. d16] c4
g'4. f8 e4 
f8 e d4. c8
b a g4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Loure"
  \time 6/4
\partial 4.
e8 d4
c4. d8 c4 b8 c c4. b8
a4. b8 a4 b4 b4. a16 b
c4. d8 e4 e d c

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Labagatelle"
  \time 3/4
\partial 4
r4 r4 r4 c4
d2 e4
e d c
b2 e4
cis2 d4
d2 c4
c4. b8 c d
c2 


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "6. Gavotte"
  \time 2/2
\partial 2
c4 e
a,4. a8 a4 b
c d8 e e4 f8 g
c,4 d8 e b4 c8 d
b b c d g,4 e'

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "7. Rondeau"
  \time 2/2
\partial 4
e4 e d8 c d4 e8 d
c b c d c4 c8 d
e2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "8. Menuet"
  \time 3/4
e4 f f8 e
d4. d8 e d
c4 c8 b c d
b2 c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "9. Autre"
  \time 3/4
e2 e4 
e d8 c b4
c d c b2 c4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "10. Chaconne"
  \time 3/4
e4 e8 d c d
b4. b8 e4 a,4. a8 b4
c c b8. a32 b
c8 e e d c d 
b4. b8 e4

}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Prelude"
  \time 2/2
\partial 2
r2 r4 d4 a4. a8
bes4 bes b4. b8
c4 bes a4. g8
fis4 fis'8 g a4 d,

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Prelude"
  \time 2/2
\partial 2
r2 r4 a4 d4. e8
cis4. cis8 d4. e8
fis4. fis8 dis4. dis8
e4. e8 cis4. cis8
d4. a8 c4. d8
b4. b'8 b4. b8
g4.

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Prelude"
  \time 2/2
r4 d4 d4. es8
c4 c f4. es8
d4. d8 es4 g8 g
a4 bes a4. bes8
bes4 d,

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Prelude. Lentement"
  \time 2/2
r4 e4 b4. c8
a4. a8 a4 b
g g a4. b8
b2 a4. b8
b2

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio VI " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "1. Prelude. Lentement"
  \time 2/2
\partial 2
r2 r8 as8 g f es g f es
d4 g g as8 g
f g b,4 c8 es es f
g d d e f g f e
d g f es es4


}

