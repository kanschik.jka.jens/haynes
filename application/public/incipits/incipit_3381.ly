 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro moderato"
  \time 4/4
    d4 fis8 a \grace a8 g4 e8 cis
    d4 fis8 a \grace a8 g4 e8 cis
    d4. fis8 e8. g16 fis8. a16
    \grace { g16[ a]} b2 a8 g fis g16 e
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Rondo. Legerement et Moderement"
  \time 4/4
  r4 fis8. e32 fis a4 a
  fis fis8. e32 fis a4 a
  fis fis8. e32 fis a4 a
  b b \grace { b16[ cis]} d4 cis8 b
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Largo"
  \time 6/8
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}