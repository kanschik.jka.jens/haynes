\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andantino"
  \time 4/4
  bes4. c16 d es4 d
  c8 d16 c c8 f16 es d4 c8 d
  c f16 a, bes4. c16 d es8 d
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Poco Allegro"
  \time 4/4
 \partial 8
    \set Score.skipBars = ##t

 r8 R1*7
 r2 r4 r8 f
 f8. g16 f8 es d4 c
 bes16 bes c d
   }

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Menuetto"
  \time 3/4
    d4 bes c
    a bes f~
    f8 f' es d c bes
    c4. d16 es d4
}