\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Tröste mir, Jesu, mein Gemüte [BWV 135/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key c\major
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                      g4 a8 b c e
                      d b c2
                      g8 a16 b c8 d e g
                      f d e2~ e16
                     
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key c\major
                     \set Score.skipBars = ##t
                        % Voice 2
                     e,4 f8 d e g
                     f d e2
                     g4 a8 b c e
                     d b c2~ c8
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key c\major
   \tempo "o. Bez."
  \time 3/4
     \set Score.skipBars = ##t
     R2.*16 g4 a8 b c e
     d b c2
}