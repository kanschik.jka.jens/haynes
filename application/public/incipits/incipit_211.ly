\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key c\minor
    \time 3/4
    \tempo "1. Adagio"
      es4 d b
      c8 g' es8. f32 g  c,8. d32 es
      d8. es32 f b,8. c32 d g,8. a32 b
}

\relative c'' {
  \clef treble  
  \key c\minor
    \time 2/4
    \tempo "2. Allegro"
      es4 d
      c r8 d
      b c d es
      d4 g
      b,8 b16 d c8 c16 es
      d8 d16 f  es8 es16 g
}

\relative c'' {
  \clef treble  
  \key c\minor
    \time 4/4
    \tempo "3. Adagio"
      es8 es es16 es d c  fis4 r
      f8 f f16 d b g e'4 r
      es8 es es16 c a f  d'4 r
}
\relative c'' {
  \clef treble  
  \key c\minor
    \time 2/4
    \tempo "4. Presto"
      c16 b c8 g16 f g8
      es16 d es8 c4
      g'' a16 f g8 d16 c d8
      b16 a b8 g4
}
