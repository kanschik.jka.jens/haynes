\version "2.16.1"
         #(set-global-staff-size 14)

\relative c'' {
  \clef treble  
  \key f \major
    \time 2/2
    \tempo "1. Marche, Vivace"
    \set Timing.baseMoment = #(ly:make-moment 1/4)
    f8 f16 f f8 c f c f c f 
    
}