\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}





\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. [Allegro]"
  \time 4/4
  e4 r8 e16 e g4 r8 g16 g
  c8 g e g c,4 r8 g'16 g
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio"
  \time 4/4
  e4 r8 e f f16 e f e f e
  d4 r8 d e e16 d
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro"
  \time 3/8
  r8 a e a c b c b a
  b a gis a4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. [Menuetto]"
  \time 3/4
  c4 b2 c4 g' e
  c b2 c4 g' e
}

