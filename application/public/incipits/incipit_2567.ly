 
\version "2.16.1"
 
#(set-global-staff-size 14)
 


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "Modéré et simplement"
  \time 4/4
  bes4 d c es
  d2 g
  f4 d es c
  bes2 c
}