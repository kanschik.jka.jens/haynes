
\version "2.16.1"

#(set-global-staff-size 14)

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 4/4
 g2. e8 c a' g g g g4 r4
 g4. a16 b c8 g e c a' g g g g4 r8
  

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondo moderato"
  \time 2/2
   e4 c f4. e8
 \grace e4 d2 c4 r4
 g'4 e a4. g8
 \grace g4 f2 e4 r4
 d2. f8 b,
 c \grace {d16 [c b]} c4 d8 e f g e e d


}

