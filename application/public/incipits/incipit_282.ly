\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegretto"
  \time 2/2
  bes2 f'
  es16 d8. c16 bes8. a16 bes8. a16 bes8.
  \grace a8 g4 f  bes4. c16 d
  c8 a bes4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Largo e mesto"
  \time 3/4
  d4 es d d fis g
  f16 es8. d4 c
  bes32 a g8. g4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allego moderato"
  \time 2/4
  \partial 8
  f8
  f4 es8 d d \grace f16 es8 d4
  c8. d32 es d16 c bes a
  bes8 f r
}
