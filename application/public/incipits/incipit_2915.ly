 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 4/4
    es1~
    es4 d16 c bes as g8. as16 bes as g f
    es8. f16  g g as bes  c c d es f f g as
    bes,4
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
    es16 d
    es8 bes4 c16 d es8 g,4 as16 bes
    c8 as'16 g f8 es4 d16 c d8 es16 d es8
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Siciliano"
  \time 12/8
    es8. d16 c8 b4 c8 
    g4.~ g8. g'16 f8
    es4 d8 c16 b c8 as'
    g,4
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "4. Bouree"
  \time 4/4
  \partial 4
    bes4
    es2 d es4 d8 c bes4 c8 d
    es4 c bes as
    g as bes c8 d
    es4
}