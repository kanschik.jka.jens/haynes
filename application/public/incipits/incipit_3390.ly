\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  d8 a fis' d a' g16 fis e fis g a fis4 r r2
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Roundo"
  \time 3/2
  b2 d fis d2. cis4 b2
  g'2. fis4 e d
  cis1
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 4/4
  fis4 a fis8 e16 d e8 a,
  d4 e8. fis16 fis4 e fis4 a
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  a4 a8 a d d16 d d8 d
  e16 fis e fis g8 a16 g fis8. e32 fis d4
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 3/4
  r4 fis d
  e2 a4 d,  b'8. a16 g8. fis16
  e2 a,4 
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio"
  \time 3/4
  f4 d4. f8
  e4. e8 a,4
  d d4. e8 cis4. d8 e4
}
  
  \relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 3/4
fis8. e16 fis8. g16 e8. fis16
g8. fis16 g8. a16 fis8. g16
}
  
 


