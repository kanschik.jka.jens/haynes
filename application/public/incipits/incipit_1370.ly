 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez. [Tutti]"
  \time 4/4
  d8 g, r16 g a g  d'8 g, r16 g a g
  d' g, a g  d' g, a g  d'8 g, r g
  c16 b a b c8 d
}

 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez. [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*7
   d8 g, r g d' g, r g
   c16 b a b c8 d b8 a16 b g8 g
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Cantabile"
  \time 3/8
  r8 r d,
  g b a
  b d c
  d4 e8
  d8 c16 b a g
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Allegro"
  \time 2/4
  \partial 8
  g8
  d' d d d
  d e16 fis g8 d
  c b b a a b16 a b8 d
}
