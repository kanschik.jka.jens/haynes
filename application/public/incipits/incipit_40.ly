\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Marche des Bergers"
  \time 2/2
  \partial 2
  c4 g
  c d8. e16 d4 g
  e4. d8 c8. d16 e8. f16
  g4 g d8. e16 f8. g16
  e4. d8 c4 g
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite II " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Les Contre-feseures de Musette"
  \time 4/4
  g2 d b8 c b c  d c b a
  g a b c d e f d 
  g8. d16 g8. a16 b8. c16 d4
}


