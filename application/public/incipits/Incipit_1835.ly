
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. La Cavalcade ou le départ des Chasseurs"
  \time 2/4
\partial 8
r8 c4 g
\grace f8 e4 \grace d8 c4
g'' \grace f8 e4 \grace d8
c4 f
e8 fis g d
e c~ c b c4 a'8 g16 f
\grace f8 g4 r4
  

}

\relative c' {
  \clef treble
  \key c\major
   \tempo "2. La Quête "
  \time 6/8
\partial 8
r8 c8 c c c c c 
c4. c
c8 c c c c c 
c4. c4 g''8
\grace f8 e8 d c b a g
c4 c,8 c4 g'8 
a b g a4 b8
c c, c c4.   


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. La Chasse "
  \time 6/8
\partial 8
r8 r4. r4. r4. r4 c8
c g c c g c
c4. e4 g8
e d c e d c
b d e d b a 
b a b c b a

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Le Défaut "
  \time 2/4
\partial 4
c8 d16 e
d8 c e b
\grace b8 c8 g~ g16 g c b
r16 a16 b c r16 g16 c b
r16 a16 b c g8 e'
e16 d c b a8 b16 c
d8

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Fanfare "
  \time 6/8
\partial 8
g8 c4 d8 e4 f8
g4. d4 e8
c4 g8 e'4 d8
c b c d e c
d e f g f e d4.

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "6. Les Petits Doits ou la Croixmar "
  \time 3/4
c4 b c
d \grace f8 e4 \grace d8 c4
f f e 
d8 c b a g4
c d e 
d2.
}


