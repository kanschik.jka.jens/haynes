 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  g'8 e c e d g, r c
  d e f e d4 r8 g e f d g
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Largo"
  \time 3/2
  e2 e4 f e c
  c2 r r
  b e4 d c b
  c2
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Aria"
  \time 3/4
  \times 2/3 { e8[ d c] } \times 2/3 { c8[ b a] } \times 2/3 { a8[ b c] }
  gis4 e a
  b a b f' e d
  c4 \grace b8 a4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Rigodon"
  \time 2/2
  \partial 4
  a'4
  e f d e c2. e4
  e2. e4
  e2.
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "5. Gigue"
  \time 6/8
  c4. b8 a b 
  c b c d c d
  e d e f e f g4. c,4.
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "6. Menuet"
  \time 3/4
  g4 a b
  c8 b c d c4
  e8 f
  d4. c8
  b a b c b4
}