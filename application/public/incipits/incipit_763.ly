\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
    g'16 g, g g   g g g g  g8 a16 b c d e fis
    g g b b c c d d c b a g fis e d c
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Largo"
  \time 3/4
    g16 g' bes, g' g, g' bes, g'  g, g' bes, g'
    d, d' fis, d'  d, d' fis, d'  d, d' fis, d'
    g a bes a g a bes a g f es d
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 2/4
      g'4 r16 b a fis
      g4 r16 b a fis
      g8 d e c d b c a
}
