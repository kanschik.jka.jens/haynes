\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Largo [Tutti]"
  \time 4/4
  r8 a16 gis a8 a, r16 c' b a gis b e,8
  r a16 gis a gis a gis a8 d, e e
  a,4 r r2
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Largo [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*2 
    c4~ c16 d b c  d4~ d16 f e d
    c b c b c b c d  b8 e, r4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Vivace [Tutti]"
  \time 4/4
  a'8 e e d  c b16 c a8 f'
  e d16 c b8 c16 a gis8 e r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Vivace [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*16
   a8. b32 c b8. c32 d c8. d32 e d8. e32 f
   e8 f16 e d c b a gis8 e r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Adagio"
  \time 4/4
  g'4. a8 b, a16 g g'8. a16
  d,8 e f g16 f e4 e8 f16 e
  d4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Vivace"
  \time 6/8
  c8 b16 a e'8  e f e
  c b16 a f'8 f4.
  b,8 a16 g d'8 d e d
  b a16 g e'8 e4.
}