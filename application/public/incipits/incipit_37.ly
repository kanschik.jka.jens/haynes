 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Grave"
  \time 4/4
    r4 g' g g
    a g f4. f8 e4 a4. g16 fis g4~
    g8 a16 g fis4 g8 d b g
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Ouverture"
  \time 4/4
  g'2 g4. d8 e2 r4 f
  g a8 bes bes4. a8
  a4.
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Air"
  \time 2/2
  \partial 2
  c8 d e f
  g4 g d g
  e c g' e a g f4. e8 d4 d
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Allemande"
  \time 4/4
  \partial 8
    e8 e4. e8 f16 e d c b8. a16 a8. e'16 e8 f g a16 b a g f e d8
}