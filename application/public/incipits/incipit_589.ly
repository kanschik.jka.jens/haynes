 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
   \time 4/4
      d2 e
      fis8 d d  d \grace e8 d8 cis16 b a g fis e
}
