 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key des\major
   \tempo "1. Largo"
  \time 3/8
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
    as'16 as8 as16~ as32 bes ges es
    c8 des r16 f
    f8 es r32 bes' as ges
    ges8 f
}