\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 4
    c,4 f g a bes
    c f8 e f4 a
    c,2 bes
    a4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 3/4
       bes8 bes4 d8 \grace d16 c8 bes16 a
       bes8 d4 f8 \grace f16 es8 d16 c
       d4 bes16 d d f  f bes bes g
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 3/8
     f4 a16 f
     c4 a16bes
     c8 d c
     \grace c16 bes4 a8
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Moderato"
  \time 4/4
  \partial 8
    f,8 bes16 d32 c bes16 bes    bes bes bes bes  
    bes16 d32 c bes16 bes    bes bes bes bes  
    c16 es32 d c16 g'
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 3/8
    f32 e f g a8[ c,]
    d32 cis d e f8[ a,]
    bes32 a bes c d16[ bes g f]
  
 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro con les Variazioni"
  \time 2/4
   f8. g32 a bes8 es,
   es4 \grace d8 c4
   d8 f g16 es c bes
   a8 c bes4
}
