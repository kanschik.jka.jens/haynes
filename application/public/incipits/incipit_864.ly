\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro "
  \time 2/4
  d8 d, r16 d fis a d8 d, r16 a' d fis a8 fis g e fis d r16
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Andante"
  \time 3/4
  r2. a'4 gis fis e d cis
  e, \grace {fis16[ gis a b cis]} d8 d d d
  cis a' d, d d d cis4
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 3/4
      d16 d, d d   d d d d  d d d d
      d e fis g  a g a b  cis b cis a
      d d, d d    d d d d  d d d d
}

