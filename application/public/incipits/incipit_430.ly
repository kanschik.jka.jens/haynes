
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Suite "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
  \tempo "1. Ouverture "
  \time 2/2
  \partial 8 
  g8 e e e g c, c c c 
 f4. f8 f d e f 
e4 \grace d8 c4 
 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxiéme Suite "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Prelude. Gravement"
  \time 2/2
 \partial 4
 r8 g8
 d4. e8 d4. c8
 b4 \grace a8 g4 b4. c8
 d4. d8 d4 g
 e4. e8 e4 a
 fis \grace e8 d4
 
 
 
}



  
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisiéme Suite "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Allemande. Gravement"
  \time 4/4
 \partial 8
 r16 g16 g8. a16 g f e d e8 d16 e f e d c
 b8 \grace a8 g8 r8
 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatriéme Suite "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Prelude. Gravement"
  \time 3/4
 \partial 4
 g4 e4. f8 g4
 b,4. a8 g4
 c4. d8 e4
 f \grace e8 d4
 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquiéme Suite "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Allemande. Gayment"
  \time 4/4
 \partial 8
 r16 g16 g4 r16 b16 a b g8 b,16 c d c b a 
 b8 g d' c16 d b8 g' g8. fis16 g8
 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixiéme Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude"
  \time 2/2
 c4. c8 c4 d8 e
 d4. d8 e4 c
 f4. f8 f4. g8 
 e4 \grace d8 c4
 
 
}

