\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran und Bass): So du willt, Herr, Sünde zurechnen [BWV 131/2]" 
    }
    \vspace #1.5
}

\transpose g a

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key g\minor
   \tempo "Andante"
   \time 4/4
   \set Score.skipBars = ##t
  R1*2 r4 f8 g16 f bes,8 g'16 es c8 bes'16 a bes8 bes, r4
   
   
 } 
 
\transpose g a
 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key g\minor
   \tempo "Andante [Melodie: Herr Jesu Christ, du hoechstes Gut]"
   \time 4/4
   \set Score.skipBars = ##t
   R1*6 r2 d2 d cis 
   d es
   f es d r2
   
} 
  
   
}

\transpose g a

\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key g\minor
   \tempo "Andante"
   \time 4/4
   r4 g8 bes16 a a8 r8 a8 c16 bes bes8 r8 r8 es,8 r8 es8
   
}






