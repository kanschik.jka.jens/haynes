\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. [Allegro][tutti]"
  \time 2/2
  g8 es c es g es c g'
  as as as as   as g16 f g8 c,
}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. [Allegro][Oboe]"
  \time 2/2
     \set Score.skipBars = ##t
 R1*12
 c4 r8 g as g16 f g8[ c ]
 b4 r8 c d16 f, g8~ g16[ as' g f]
 es8[ c]
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio [tutti]"
  \time 2/2
  g16. bes32 es8 
  g,16. bes32 es8   
  as,16. c32 f8 as,16. c32 f8 
  bes,16. es32 g8  bes,16. es32 g8
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio [Oboe]"
  \time 2/2
       \set Score.skipBars = ##t
  R1*4
  r2 r4 bes~
  bes1
  bes8 c16 d es[ g f es] d8 bes f'4~
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro"
  \time 3/8
  \partial 8
  g8  c8 c,16 d e f
  g8 g g
  c16 c, d e f g8
}

