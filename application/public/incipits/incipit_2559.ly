\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 6/8
   \partial 8
   c8
   f8. e16 d8 \grace d8 c8. bes16 a8
   \grace { g16[ bes] } d8. bes16 g8
}
