 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro [Tutti]"
  \time 3/4
    r8 d d d es d16 c
    d8 d d d es d16 c 
    d8 c16 d es8 d c d
   
}

 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro [Solo]"
  \time 3/4
  \set Score.skipBars = ##t
  R2.*21
  r8 g bes d g16 fis g fis
  g8 g, bes d g16 fis g fis
  g8 d g bes a g
  fis16 g fis g a8
   
}

 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Pizzicato"
  \time 3/2
       r2 d4 e f d
       e2 a, a'~
       a g4 f e d
       cis b a2 r
}

 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Vivace [Tutti]"
  \time 2/4
      g'4 g
      g r16 g a bes
      a8 d, d a'
      bes g d bes
      g4 f es4.
}

 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Vivace [Solo]"
  \time 2/4
   \set Score.skipBars = ##t   
   R2*7
   d8 c16 d es8 d
   d8 c16 d es8 d
   d g d a' d, bes' a d,
   g4
}
