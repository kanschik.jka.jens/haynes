\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro assai [Tutti]"
  \time 4/4
    fis2 r r4 fis g e fis2 r
    r4 fis g e fis r fis r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro assai [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   fis2 r4 r8 a,
   d2 a4. fis8
   d e16 fis g a b cis  d e fis g  a b cis d
   a2 e4. cis8 a4 r r2
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante poco Adagio"
  \time 2/4
    g'8 g4 fis16 e e d d4 dis8
    e8 e8. d32 e fis[ e d c]
    c8 b r16
}



\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Rondo"
  \time 3/4
    \partial 4
    fis8 g
    \grace b8 a4 g8 fis e d
    d cis e cis d dis
    e fis g b, e d
    \grace d8 cis4 b8 a
}