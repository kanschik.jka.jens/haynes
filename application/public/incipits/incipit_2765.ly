\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 4/4
  c4 r16 f32 e f16 c d8 c r16 f32 e f16 c
  d8 c bes16 e g bes, bes8[ a ]
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 4/4
  c4 r16 d32 e f16 c d8 c r f
  \times 2/3 { g16[ a bes] } \times 2/3 { g16[ a bes] } c,8 bes'
  a16 g f e f8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Largo. Affettuoso"
  \time 12/8
  d8. cis16 d8 a'16 g f e d cis d8 cis d r4 d8
  e f g a, bes' a32 g f e f16 e d cis d8
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Presto"
  \time 2/4
  f4 c r8 f16 e f8 c d4 c
  r8 e16 f g8 c,~ c4 bes
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 3/4
  bes4 f' bes,~
  bes a2
  \times 2/3 { c8[ d es] } d4 c
  d c bes
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Vivace"
  \time 4/4
  \partial 8
  f8
  d c16 bes c8 bes16 a bes8 c16 d es f g a
  bes8 f4 bes8 g f r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Andante"
  \time 2/4
    d8 d \times 2/3 { d16[ c d] } \times 2/3 { es16[ d c] }
     d8 fis, g4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 3/4
    bes8 c d es f bes,
    g'4 f r
    \times 2/3 { es8[ d es] } d4 r
    \times 2/3 { c8[ bes c] } bes4 r
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  d4 r16 fis e d e a, a'8 r16 g fis e
  fis32[ e d cis] d8 r16
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  a'8 g16 fis b8 d, cis d r a
  d16 fis e g fis a g b a8. a,16 g'8. a,16
  fis'8 e r
}
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Largo"
  \time 3/4
  b8 fis d'4. b8
  cis eis fis2
  b,4 g'16 fis e d cis8 b
  ais4. g8 fis4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Presto"
  \time 2/4
  d4 fis
  e8[ d cis b]
  a e'16 fis g8 g
  fis e d4
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 3/4
  d8 e16 fis g4 g
  \times 2/3 { g8[ fis g] } r4 b
  \times 2/3 { c8[ b a] } d4 c
  b4. a8 g4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  g'4 r16 d g b a4 r16 d, a' c
  b8 a16 g c8 b16 a d8 c16 b a8 g
  fis e d4
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Largo"
  \time 2/4
    e8 b4 c8~
    c b4 e8
    \times 2/3 { fis16[ g a ] } \times 2/3 { dis,16[ e fis] } b,8 a'
    g fis e4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Presto"
  \time 12/8
    g8 b d g a fis g4. r4 g8
    a b c d,4 c'8 b4 a8 g4.
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Largo"
  \time 4/4
  es16 bes g' es bes'8 g f bes, r g
  as16 f c' as f'8 as, as g r 
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Vivace"
  \time 4/4
  es16 f g as bes8  bes bes16 bes4 as16 g f
  es d es8 r bes c c c16 d32 es d16 c
  c8 bes r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Andantino"
  \time 2/4
  g'8 d4 bes'8
  a d, r g16 d
  es8 c16 a f16 es' d c
  d8 bes r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Presto"
  \time 3/4
  es,4 bes' es~ es d r
  f bes, as'~ as g2
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Adagio"
  \time 4/4
    a8 cis16 e a8 e fis e r a
    \times 2/3 { gis16[ a b] } \times 2/3 { b16[ cis d]} e,8 d' cis b a4
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 3/4
    a'8 gis a a, cis e
    a gis a a, cis e
    b' a gis fis e d
    cis b a2
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Largo Staccato alla breve"
  \time 3/8
  a8 e' r32 cis b32. a64
  b8 e r16 b
  a8 \times 2/3 { fis'16[ e d] } \times 2/3 { cis16[ b a] }
  gis16 fis e8 r
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Allegro"
  \time 2/4
    a2 fis'4 r16 gis32 a gis16 fis
    \grace fis8 e4. cis8
    d cis4 b8 cis b a4
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 3/4
  r8 f, a c f a
  r e, g c e g
  r f, a c f a
  c8. bes32 c d16 c bes a g8 f
  e16
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 4/4
  f16 c bes a f' c bes a f'8 a4 g16 f
  g e d c g' e d c g'8 bes4 a16 g
  a
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio Cantabile"
  \time 3/4
  d8 f4 a8 bes d,
  cis16 e g4 bes8 a16 g f e
  d8 f4 a8 g16 a bes d,
  cis d e cis a4.
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Vivace"
  \time 3/4	
  f,8 a c f a4 g2.
  c,8 b c e g bes
  a4 g f
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  g'4 r16 f e d c b c8 r g
  a16 b c a g f32 e \grace f8 e4 r8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 3/8
    c16 b c e g e
    d c d f g d
    e c d b c8
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Largo"
  \time 3/2
    a2 e' a~
    a gis1 d1.
    c2 b a dis1. e
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Presto"
  \time 2/4
    c8 g'4 c,8 d g4 d8
    e b c4~
    c8 a' g f e16 d c b c4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Andante"
  \time 12/8
  a8. c16 e8 a4 f8 f4 e8 r4 a8
  b16 gis f e b'8 d8. c16 b8 c8. b16 a8
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  e8
  a e c a a'16 g f e d c b a
  a'8 e c a a'16 g f e d c b a
  b'8 gis e gis b d4
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Largo Cantabile"
  \time 2/4
  g'8 c,4 c8
  a' g4 g8
  c16 g a e f d e c
  g8 f' e d e d c4
}
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Allegro"
  \time 6/8
  a'8 c, b a16 c d e f gis
  a g a e a c b a b e, b' d
  c8 b a c8. b16 a8
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Largo"
  \time 3/4
  a'8 g16 fis b4 d, cis d2
  \times 2/3 { g8[ fis e]} 
  \grace g8 \times 2/3 { a8[ g fis]} 
  \grace a8 \times 2/3 { b8[ a g]}
  \grace a8 d16 cis b a g4 fis~
  fis e2
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 2/4
  a'8 fis a e
  a d,4 a'8
  b16 d, cis d a' d, cis d
  g4 fis~
  fis8 e r
}
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Grave"
  \time 3/4
  fis4 b, b'8. fis16
  g2 fis4
  e8. cis16 ais8 cis fis, e'
  d4 cis b
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Presto"
  \time 3/8
  \times 2/3 { a'16[ g fis] } b8 d, cis d r
  \times 2/3 { e16[ fis g] } fis8 r
  \times 2/3 { g16[ a b] } a8 r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4
  g'4. \times 2/3 { d16[ cis b] } 
  a'4. \times 2/3 { fis16[ e d] }
  b'32 a g fis g8 r d e16 fis g e \times 2/3 { d16[ cis b]} cis8
  b8.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  g'8 d c b g' c, b a
  b a16 g a8 g16 fis g8 a16 b c d e fis
  g8 g g8. fis32 g a8 a a8. g32 a b16 a g8 r
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Largo Affetuoso"
  \time 2/4
  b4 c16[ d] \times 2/3 { e16[ dis e]}
  b8. c16 b8 a g8. fis16 e8
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Presto"
  \time 2/4
  g'8 fis16 g g8 fis16 g
  g8 g, r g'
  a16 g fis e d c b a b8 g r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Grave"
  \time 3/4
  r16 e, gis b e gis b gis e b gis e
  r fis b dis fis a fis b, a dis a fis
}


\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Vivace Affettuoso"
  \time 3/8
  e8 b gis'~
  gis fis e fis b, a'~
  a gis fis gis fis e
}

\relative c'' {
  \clef treble
  \key cis\minor
   \tempo "3. Andante"
  \time 4/4
  cis8 cis cis a' gis16 bis, cis8 r cis
  \times 2/3 {dis16[ cis bis]} 
  \times 2/3 {a'16[ gis fis]} 
  gis,8 fis' e32[ dis cis bis] cis8 r
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "4. Allegro ma non Presto"
  \time 2/4
  e8 gis e b
  fis' a fis b,
  gis' fis e4
  gis fis8 e
  fis ais b b,
  dis cis16 b cis4 b2
}
