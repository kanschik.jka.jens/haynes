 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Largo"
  \time 3/4
      << {
      % Voice "1"
          R2.
          r8 es es es es16 d es f
          d8 as'16 g   f g f es  d es d c 
          b
         } \\ {
      % Voice "2"
          r4 g' es
          c8. b16 c4 r
          r d8. es16 f4~
          f
      } >>

}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Vivace"
  \time 6/8
  \partial 8
    << {
      % Voice "1"
        r4 r8 r g'16 f es d
        es8 g c4 b 
        c8
         } \\ {
      % Voice "2"
        r8 c,16 d es c d8 g, b
        c c16 d es c d8 g16 f es d
        es8
      } >>

}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Andante"
  \time 4/4
  es8 es~ es16 g f es f8 bes,~ bes16 d c bes
  es8 d16 c bes8 as as[ g]
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Allegro"
  \time 2/4
  c16 d es8 es16 f g8
  as g4 c8
  c,16 d es8 es16 f g8 as g4
}