 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "Variatio 1ma"
  \time 3/4
    es,8. d16 es4 bes
    \grace as4 g2 bes4
    es8 g g bes bes es
    \grace bes4 as2 g4
}

