 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 4/4
  d16 e d e  fis g fis g a2~
  a8 d,16 e  fis g fis g a2~
  a16[ a g fis]
}
 