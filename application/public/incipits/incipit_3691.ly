\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. o.Bez."
  \time 4/4
  c4 g es c
  g'8 a16 b c d es8
}
