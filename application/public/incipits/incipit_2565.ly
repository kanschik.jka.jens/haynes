 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
    d4~ d16 es f d  bes a bes8 r4
    g'4~ g16 a bes g es d es4 f8
    d c16 bes f'8 es16 d c4
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegro"
  \time 3/4
  r8 f bes, g' f es
  d c16 d bes c d bes  c d es c
  d8 c16 d bes c d bes c d es c
  d8  c16 d bes8 c d e
}

\relative c' {
  \clef bass
  \key g\minor	
   \tempo "3. Cantabile [Bass]"
  \time 6/8
  g8. a16 g8 g fis4
  bes8. c16 bes8 bes a4
  d8. es16 d8 d c16 bes a g
  fis4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Cantabile [Solo]"
  \time 6/8
r4. d4.~
d es4 d8 bes4 a8 g4.
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Vivace"
  \time 4/4
  f4 es16 d c bes c8 bes r g'
  f g16 a bes8 es, d c r f
  
  
}