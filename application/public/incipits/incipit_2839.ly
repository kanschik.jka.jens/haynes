 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo"
  \time 4/4
  \partial 8
  d8 g,8. bes16 a8. c16 bes8. d16 c8. es16
  d8 c16 bes a8 bes16 c
  fis,4.
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro"
  \time 2/2
  g4 es' fis, a'~
  a8 c,16 bes c8 a bes g g'4~
  g8 bes,16 a bes8 g fis4
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Largo"
  \time 4/4
  es8 g,16 as bes8 c   c bes  g' es
  es d as' f  f es  bes' g 
  g f bes,4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Allegro"
  \time 3/8
  r8 g16 a bes g
  d'8 d d
  g g, g'
  a a16 bes c a
  bes8
}