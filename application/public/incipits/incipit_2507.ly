 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Andante"
  \time 4/4
  r8 g c4~ c8 b d c16 b
  c8 g es'4~ es8 d f es16 d
  es8 bes g'4~ g8 f16 es d8 es16 c
  b4 r8 g c4 r
}


\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key c\minor
                      \tempo "2. Adagio"
                      \time 3/4
                      R2. R2. R2.
                      g4 c es
                      d g,8 as' g f es2 f4
                      g8 bes as g f es
                      d2 es4
                  }
\new Staff { \clef "bass" 
                     \key c\minor
                     r8 c, bes as g f
                     es g f es d c 
                     b c g'4 g, c r r
                  }
>>
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Vivace"
  \time 2/4
   \partial 4
   g'8 c,
   b8. c16 d8 es
   d4 g8 c, b8. c16 d8 es
   d4
}
