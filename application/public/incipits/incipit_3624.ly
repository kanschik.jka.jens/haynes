
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
  \tempo "1. o.Bez."
  \time 3/2
  r2 g bes a d1
  d2 c4 b c2 bes1. bes2 a d
}


\relative c'' {
  \clef treble
  \key g\minor
  \tempo "2. Allegro"
  \time 4/4
  d8 g, es' g,  d'16 c d g, es'8 g,
  d' g, es' d16 c  d8 g, r g'
  fis g c, g' fis g r
}

\relative c'' {
  \clef treble
  \key es\major
  \tempo "3. Adagio"
  \time 4/4
  r4 bes2 bes8 bes
  es8. es,16 es8 es' as4. as8
  as g g2 f8 es
  d4
}

\relative c'' {
  \clef treble
  \key g\minor
  \tempo "4. Allegro ma non troppo"
  \time 6/8
  d8 g f   es d c
  bes a g fis16 es fis es d8
  d' c bes a16 g a g fis8
}
