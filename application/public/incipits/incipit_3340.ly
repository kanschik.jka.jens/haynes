 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 4/4
  \partial 8
  c8
  f16. e32 f16. e32 f16 c d bes
  c8 bes a f
  f'16. e32 f16. e32  f16 c d bes  c8 f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/2
      r8 f, f16 g a bes  c4. f8
      e bes a d c e, f bes a c, d d' c d16 c bes8 a g4 r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Adagio"
  \time 4/4
     c2~ c4. g'8
     f16. g32 f16. g32 f16. as32 g16. f32 \grace f4 e2
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. A tempo die Minuetto"
  \time 3/8
    f8 f8. g32 a
    g8 f16 e d c
    f8 c d
    a c f e4
}

