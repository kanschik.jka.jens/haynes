\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Allegro "
  \time 2/2
  b'16[ b ais ais] b b ais ais b8 fis r4
  b16[ b ais ais] b8 fis16 e d cis b8 r16 fis' fis fis
  g[ fis g a] g b a g  fis[ e] d8
  }

  
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Adagio"
  \time 3/4
  r4 fis2~
  fis4 e8 fis g4~
  g fis2~
  fis8 e16 d  cis4.
}


  
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Allegro"
  \time 2/2
  r8 b b cis d cis16 d b[ cis d e]
  fis4 b~ b8 a16 b a[ g a fis]
  g8 g16 a g[ fis g e] fis8 d16 e d[ cis d b]
  cis4
}