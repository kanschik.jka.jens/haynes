\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro "
  \time 4/4
  g'2 e a2. g4
  \grace g8 f4 e8 d e4 f
  g2. f4 e r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Menuetto"
  \time 3/4 
  g'2 f8 e e4 e e 
  f f16 g f e f8 g a4 f r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "Trio"
  \time 3/4 
  f4 a f c'4. bes8 a g
  a4 f f e c c
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Romanza"
  \time 6/8
  d8. e16 d8 b'4 g8
  fis4 g8 a4 r8
  c4 a16 b c b a g fis e
  d4 c8 b4 r8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Rondo"
  \time 2/2
  g'4 c8 r g4 c8 r
  b a a a a4. g8
  f4 e \grace e8 d4 e8 c
  b4 c8. b32 c d8 e f fis
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/2
  g'2. b4
  a fis g8 b g e d2. c'4
  a b g r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Menuetto"
  \time 3/4
    d2 g4 g b d
    \grace d8 c4 c8 b c4 a r r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Menuetto"
  \time 3/4
  g'2 bes4 a d,, r
  d c''2 bes4 g r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Romanza"
  \time 2/4
     g'8. a32 g  e8 r16 e
     e8. f32 e c8 r16 c
     a' b c b c a g f e4 d16 e f fis
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Rondo"
  \time 6/8
    b'8. c16 b8 d4 fis,8
    g8. a16 g8 b4 dis,8
    e fis g a b c
    \grace c4 b4. a4 r8
}

\pageBreak

\markuplist {
    \fontsize #3
    "Duo III " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  d2 \grace g8 fis4 e8 d
  a'4. b8 a a b cis
  d4. b8 g4 g g fis 8 r
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Menuetto"
  \time 3/4
    a'2 b8 g fis4 r d8 fis
    e4 r cis8 e d8 r16
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "Trio"
  \time 3/4
  r8 d f a d d,
  bes'4 r r r8 c, e g c c, a'4 r r
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Romanza. Poco adagio"
  \time 2/2
    g'4. a16 g b4 e
    \grace d8 c4 b8 c a4. c8
    \grace c8 b4 a8 g fis e e' c b2 a8 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Rondo"
  \time 2/2
    fis4 a8 r fis4 a8 r
    fis4 d'2 cis4
    b8 r b a g r g fis
    e d e fis e4 a,
}
