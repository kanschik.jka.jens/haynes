 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro non tanto"
  \time 4/4
   \partial 4
   a4
   d d8. fis16 \grace fis8 e4. g,8
   g4 fis b2~
   b8 g' fis e  e d d c
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 2/2
  \partial 2
  g'4 d
  b8. c16 d4 r r8 g,
  a8. g32 a c,4. c'8 c c
  c4 b
} 

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 2/4
  \partial 8
  a8
  d4 d
  d8 e16 fis  \grace a16 g8 fis16 e
  d8 d4 d8
  d e16 fis \grace a16 g8 fis16 e
}
