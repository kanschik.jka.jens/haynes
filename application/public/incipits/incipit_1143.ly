 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Vivace"
  \time 6/4
  f2.~ f8 f f  f f f
  f2.~ f8 bes, c d es f
  g g es g g4 r8 f d f f4
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key g\minor
                      \tempo "2. Largo"
                      \time 4/4
                      % Voice 1
                      R1
                      r2 r8 g'8 bes a
                      d,4 r8 d d d e16 f32 g f g e16
                  }
\new Staff { \clef "treble" 
                     \key g\minor
                        % Voice 2
                        r8 d es d g,4 r8 g
                        a a a16 bes32 c bes c a16 bes8 g r4
                        
                  }
>>
}

 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 2/4
    d4 es16 d c d
    bes f d f bes d c bes
    f'4 g16 f es f
    d bes f bes d f es d
}
