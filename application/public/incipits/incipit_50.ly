\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key a\minor
                      \tempo "1. [Adagio]"
                      \time 4/4
                      R1 e4 r8. e16 f8. e16 d8. c16
                      % Voice 1
                  }
\new Staff { \clef "bass" 
                     \key a\minor
                        % Voice 2
                       a,8. g16 f8. e16 d8. c16 b8. a16
                  }
>>
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. [Allegro]"
  \time 6/8
   a'4. e
   b8 c d  c b a
   e' f e  a b a
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 3/2
  b1 c2 a1 b2 g1 a2 fis1.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 6/8
    b4. e8 fis g
    fis g e dis c b
    c d c c d c
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Menuet"
  \time 3/4
   g8 a bes4 c
   d bes g d' r es
   c d r
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Adagio"
  \time 2/2
  fis2 b,8 d cis fis
  d b g'4 g8 fis fis4
  fis8 e e4
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. [Allegro]"
  \time 2/2
   b4 cis d e
   fis8[ e16 d] cis d e cis d8[ b] g' fis16 e
   fis8[ e16 d] cis8
}

  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key g\minor
                      \tempo "1. [Adagio]"
                      \time 4/4
                      R1 d4 r8 d g, a bes c
                      % Voice 1
                  }
\new Staff { \clef "bass" 
                     \key g\minor
                        % Voice 2
                       g,8 a bes c  d c bes a
                  }
>>
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 6/8
   d8 es d  g, a g
   c d c  fis, g fis g a bes
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Menuet"
  \time 6/4
   bes4 c r8 bes16 c d4 g, r
   d' g fis g2 r4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagissimmo"
  \time 3/2
   g'2 f es
   d1 es2 d c b c d es
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. La Chamade de Landau"
  \time 2/2
   c2 g4 es'
   d2 g,4 es'
   d c d b
   c2 b
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  bes8 bes16 bes bes4. bes8
  c d es f d bes f'4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 4/4
   es2 g,4 a
   fis2. d'4
   a2 a4 d bes g d'2
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. o.Bez."
  \time 6/8
  bes8 f bes c f, c'
  d4 r8 c4 r8
  d es f d c bes a
}
