\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
  g'4 c, \times 2/3 { b16[ c d] } c8 r4
  d'4 f, \times 2/3 { e16[ f g] } f8 f,16. a32 c16. f32
  a2
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante"
  \time 3/4
  \partial 16*7
  \grace a'16 g32[ fis64 g] b16[ a] \grace a16 g8 fis16 e
  \grace {g,,32[ d']} d'2 c16 b a g
  g4 g g
  fis8. g32 a g4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegretto [Tutti]"
  \time 3/8
   c8.[ d16] \grace e16 d16 c32 d
   e4 r8
   e8.[ f16] \grace g16 f16 e32 f
   g4 r8
   g16. c,32 c'8.[ a16]
   g16. c,32 c'8.[ a16]
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegretto [Solo]"
  \time 3/8
  \set Score.skipBars = ##t
   R4.*38
   c8.[ d16] \grace e16 d16 c32 d
   e4 r8
   e8.[ f16] \grace g16 f16 e32 f
   g4 r8
   g16 e c g' a e
   \times 2/3 { f16[ g a] }  \times 2/3 { cis,16[ d e] } d8
}
