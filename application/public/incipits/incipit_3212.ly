\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): In Jesu Demut kann ich Trost [BWV 151/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key e\minor
   \tempo "Andante"
   \time 2/2
   r4 r8 b8 g c b a
   dis, a' g fis b, g' fis e
   d'4 r8
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key e\minor
   \tempo "Andante"
   \time 2/2
   \set Score.skipBars = ##t
    R4.*8 r4 r8 b8 a c b a
    dis, a' g fis b, g' fis e
    d'4 r8
}



