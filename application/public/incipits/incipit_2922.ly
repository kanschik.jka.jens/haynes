\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 2/2
  bes'2 a4 bes
  c4. a8 f4 r
  f2 g16 f e f  e[ f g e]
  cis4 d r2
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Allegro"
  \time 2/4
  \partial 8.
  f,16 bes c
  d8 r r16 f, bes c
  d8 r r16 f, bes c
  d8 d d f16 es
  d4 c16
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Menuetto"
  \time 3/4
  \partial 4
  f4
  es d es  d c d
  bes es d c r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 2/2
  d2 d4 d
  f4. es8 c2
  bes4 c d f8 es
  d2 c4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Rondo. Allegretto"
  \time 2/4
  \partial 8
  d16 es
  g f es d  c d es c
  bes8 bes'4 d,16 f
}