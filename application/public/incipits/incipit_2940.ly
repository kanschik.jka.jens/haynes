\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. [Allegro] "
  \time 2/2
  r2 r4 e
  c2 g4 e'
  c2 g4 e'
  f d \grace c8 b4 a8 g
  c2 e4 r
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Larghetto"
  \time 4/4
  c4. d32 c b c as'8[ g f es] 
  es4. d8 d4 r8 
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegretto"
  \time 2/4
  \partial 8
  g8
  \grace { c16[ d]} e4 d16 c b a
  g8. a16 b c d e
  f8 f16. g32 e8 e16. f32
  d8. e16 d8
}