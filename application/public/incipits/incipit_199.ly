\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 3/4
        g'8 d d d g fis16 g
        a8 d, d d a' g16 a
        bes d, d d bes' a16 g
        a8 d, r4 r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 2/2
        g8[ d'] bes a16 g d'16[ d d8] r16 d g d
        c8[ c16 c] c es d c bes8 \grace a8 g8 r 	
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Presto"
  \time 2/2
        d4 d d d 
        g8 d d c bes4 a
        bes8 g a d, bes' g a d,
        bes'16[ c bes a] g a bes c d4 d,
}
