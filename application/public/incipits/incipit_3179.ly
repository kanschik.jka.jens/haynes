\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Meine Seele, auf! erzähle [BWV 69/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\major
                      \tempo "o. Bez."
                      \time 9/8
                      % Voice 1
                      R8*9*1 g4 d8 g a b c a d
                      b d g b, d g cis, e g
                      a, cis g' a, cis fis
                     
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\major
                     \set Score.skipBars = ##t
                        % Voice 2
                    g,4 d8 g a b c a d
                    b d g b, d g a, c fis
                    g4. r8 r8 b8 g fis e
                    a4 a,8 r8 r8 
               
                      
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key g\major
   \tempo "o. Bez."
  \time 9/8
     \set Score.skipBars = ##t
    R8*9*12 g4 d8 g a b c a d
    \grace c8 b4.~ b4 a16 b a4 r8
   
    
    
    }