\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 3/2
  c2. d4 c d
  c2 f, c'4. bes16 c
  d2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 4/4
  f4 g a e
  f8 c f4~
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio"
  \time 3/2
  f2 e d  e d cis
  d
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 6/8
  f4. c
  a8 g f  g f e
}

