\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Schweig, aufgetürmtes Meer! [BWV 81/5]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key e\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                R1 b4 e~ e8 d16 c b a g b
                e,8 g c4~ c8 b16 a g fis e g
                 
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                R1 r2 fis4 b~
                b16 a g fis e d c e d8 e16 fis g4~
                ~g16 fis e d 
                      
                  }
                  
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine 1"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 2
r8 e16 fis g a g a b cis b cis dis cis dis b
e8 e,16 fis g a g a 
                      
                  }
                  
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                r8 e16 fis g a g a b cis b cis dis cis dis b
e8 r8 r4 
                      
                  }
                  
>>
}


\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key e\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
  R1*9 b4 e r8 e,16 fis g8 a
  b2 r2
  
   

}