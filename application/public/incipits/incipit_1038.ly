\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 3/4
  a'4 cis, d
  a2 a'4
  a cis, d a2
  }

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 4/4
    a4. d8~ d cis16 b cis cis d e
    a,4. a'8 a fis16 e d d cis d
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 2/4
  \partial 4
  r16. a'32 fis16. a32
  d,4 fis8 a
  a16 fis a8 r16. a32 fis16. d32
  cis8 d e d
  a4
}