\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 4.
    es,8 g bes
    es4 es, es es
    d r8 bes' a bes c bes
    f'4 as, as as g r r2
}
