\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Tutti]"
  \time 2/2
  g'8[ d] e d16 c d8 g, r4
  g'8[ d] e d16 c d8 g, r16 fis' g a
  b8[ b] a b16 c
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   R1*17
   b8. c16 c8. b32 c d8 g, r e'
   d16[ c b d] c b a c  b8 g r e'
   d16[ c b d] c b a c  b[ a g b] c b a c
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Largo"
  \time 6/8
  e4 g8 e8. d16 c8
  c4 b8 r r b'
  g8. fis16 e8 fis8. g16 a8
  dis,4 fis8 r r 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
  g'8. g16 d8. d16 c8. c16
  b4 r r
  R2. R2.
  g'8. g16 d8. d16 c8. c16
  b8. b16 e8. e16 d8. d16
}
