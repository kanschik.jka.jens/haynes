 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Introduction. Allegro"
  \time 4/4
    g'4 \grace b8 a g16 fis g4 r8 d
    e d g d c b r a'
    b a16 g d' g, fis g r 
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Andante"
  \time 2/4
  \partial 8
  g8
  d' d d c
  b16 g d' b a' c, b a
  g fis \grace fis8 g r
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Gigue. Allegro"
  \time 6/8
  \partial 8
  d8 g4 d8 b8. a16 g8
  e'4 d8 d4 g8
  fis8. g16 a8 c,8. b16 a8 b4 a8 g4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Menuett"
  \time 3/4
    \times 2/3 { g'8[ fis e] } d4 c
    \times 2/3 { b8[ c d] } d2
    \times 2/3 { b'8[ a g ] } d4 c
    \times 2/3 { b8[ a g] } g2
    
}
\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "5. 2.e Menuett"
  \time 3/4
    \times 2/3 { bes8[ c d] } d4 es
    \times 2/3 { a,8[  bes c] } c4 d
    \times 2/3 { c8[ d es ] } d8 c bes a
    g4 d2
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "6. Furie. Trés vite"
  \time 4/4
        g'16 fis e d c b a g g fis a g g fis e d
        a'' g fis e d c b a c b a g b a g fis
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "7. Rondeau. Gracieux"
  \time 2/4
  d8 g fis e
  d2
  a4 b8 c c b a g
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "8. Contredanse"
  \time 6/8
  \partial 4.
  g'8. a16 g8
  fis4 fis8 e4 e8
  d4. g,8. a16 g8
  c4 b8 a8. g16 a8
  b4 g8
}