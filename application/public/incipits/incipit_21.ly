\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. "
  \time 3/4
  f8 f, f f  f f 
  f4 r r
  g'8 g,   g g   g g
}

