 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "o. Bez."
   \set Staff.instrumentName = #"Oboe"
  \time 4/4
  r4 d2 g,4
  e' e8 d e d d c
  b g g'4. fis8 e d
  cis a d4. cis8 f4~
  f8 e e8. d16 d4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "o. Bez."
   \set Staff.instrumentName = #"Soprano"
  \time 4/4
     \set Score.skipBars = ##t
     R1*4
     r2 d2~ d4 g, e' e8 e
     e4 d r2
}