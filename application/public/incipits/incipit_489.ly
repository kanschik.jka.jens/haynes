\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4
  d4~ d16 d e fis g8 g, r g'16 fis
  e8 d c16 b c d b8 g r
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  g'4~ g16 d e fis g8 g g a16 g
  fis8 a d, c b g' a, fis'
  g
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Andante"
  \time 3/4
  \partial 4
  g'4
  a fis2 g4 d e8 fis c b c e d c 
  b a b d c b
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga. Allegro"
  \time 6/8
  g8 a b  c d c
  b c d  e fis e
  d e fis g a g
  fis4.~ fis4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo"
  \time 4/4
  d8 g, es'16 d es c d8 g, es'16 d es c
  d8 c16 bes  a bes c d
  bes8[ g]
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Corrente. Allegro"
  \time 3/4
  \partial 8
  g'8 g4. a8 fis a
  g4 a bes
  fis4. g8 a4
  d, c4. d8
  bes4 g
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Largo"
  \time 4/4
  d8 e16 fis g8. a16  fis8 g16 a d,8 es16 d
  c8 c c16 es d c bes8 g
  d'4~ d c d
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 4/4
  r4 d g, es'
  fis,16 d e fis  g a bes g  c8 c c bes16 a
  bes8[ g]
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Largo"
  \time 4/4
a'4. a16 g f8 e16 d g8 f16 e
f8 d r16 a f d bes'4~ bes16 d c bes a4.
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro"
  \time 4/4
  r8 a d2 cis4
  d4. f8 e16 d cis b a8 g'
  f d r f e16 d cis b a8 g' f d r
}
  
  \relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Largo"
  \time 3/4
  a'8 g f g a bes
  a g f g a bes
  a bes a bes a bes g f g4 r
  }
  
  \relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Giga. Allegro"
  \time 6/8
  \partial 8
  a'8
  f e d  cis b a
  d4. e
  f8 e d g f e f e d d,4
  }

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Andante"
  \time 3/4
  r4 c b
  a a' gis
  a2 e4
  f8 g f e f d
  e f e d e c
  d e d c d b c4 a
}
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Andante"
  \time 4/4
e4 f  e16 d c b  a b c d
e8 a, f' a, e'16 d c b a b c d
e8 a, f' a, e' a, f' a,
}
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Aria 1er. Andante"
  \time 3/4
  r4 c8 e d f
  e4 a, b c4. d8 e4
}
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Giga. Allegro"
  \time 6/8
  a'4 e8 c b a
  e'4.~ e8 fis gis
  a4 gis8 a b c
  gis fis gis e4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Largo"
  \time 3/2
   a2 cis d e d2. e4
   cis2 e d4 cis b a b cis d e
   cis2. b4 a2
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 4/4
   e4. cis16 d e8 e fis gis
   a gis16 fis e fis d e cis8 fis16 e d8. cis16
   b8
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Andante"
  \time 3/4
  e8 fis e d cis b
  a2 fis'4 gis,8 a b2
  e,4 e'2
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Presto"
  \time 2/4
  e4 a
  gis8 fis e d cis d e fis b, e b cis d4 d
  cis8
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 3/4
r4 a' d,
g g4. a8
fis4 fis8 e fis g
e2
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
r4 a' d, g~
g8 fis fis g16 fis e8 fis16 g a g fis e
d8 e16 fis g fis e d  cis8 a d4~
d cis d
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Largo"
  \time 4/4
a'4. a8 b g16 a b8 a16 g
a8 d, r d g e16 fis g8 fis16 e fis8 a d, c b d g4~
g 
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Andante"
  \time 3/8
  r8 fis d
b' e, a
fis d g fis e4
d8 fis16 e fis d
e8 a, a'~ a gis4 a8
}

