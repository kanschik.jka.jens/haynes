\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 2/4
  f8 f,32 g a bes  c8 bes
  a \times 2/3 { f'16 e d }  c8 \times 2/3 { d16 c bes }
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. o.Bez."
  \time 4/4
   a'8. g32 f e8 d cis d r4
   d'8. c32 bes
}

}

\pageBreak

\markuplist {
    \fontsize #3
    "Concerto  III " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  g4 r16 b a g  d'4 r16 fis e d
  g4 r16
}
