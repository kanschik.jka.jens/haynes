\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Grosser Gönner, dein Vergnügen [BWV 210/8]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key cis\minor
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                   e8. fis16 dis4. cis8
                   gis16 ais bis cis dis8 fis e dis
                   e8. fis16 dis4. cis8
                   gis16 ais bis cis dis8 fis e dis 

                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine 1"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key cis\minor
                     \set Score.skipBars = ##t
                        % Voice 2
             cis4 a fis gis bis r4
             cis a fis gis bis r4 
             
                       
                        
                
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine 2"
                     \set Staff. shortInstrumentName = #"Ob 3"
                     \key cis\minor
                     \set Score.skipBars = ##t
                        % Voice 3
               gis4 fis cis dis gis r4
               gis fis cis dis gis r4
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key cis\minor
   \tempo "o. Bez."
  \time 3/4
     \set Score.skipBars = ##t
    R2.*18 e8. fis16 \grace e8 dis4. cis8
    gis16 ais bis cis dis8 fis e dis
    e8. fis16 \grace e8 dis4. cis8
    gis16 ais bis cis dis8 fis e dis
   
    
    
    }