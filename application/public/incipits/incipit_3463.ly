 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro molto"
  \time 3/4
    d,16 d a a d d fis fis a a d, d
    cis cis a a cis cis e e a a e e
    fis fis d d fis fis a a d d fis, fis
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Adagio"
  \time 6/4
      << {
      % Voice "1"
      r4 a f d a' d
      cis a r r2 r4
      r a' f d a' d
         } \\ {
      % Voice "2"
      R1.
      r4 e, cis a e' a
      f d r r2 r4
      } >>

}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Allegro"
  \time 2/4
   r8 d,16 e fis8 e16 d
   a'8 d, d'4~
   d8 cis16 b a8 g fis d d'4~
   d8 cis16 b a8 g
   
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}