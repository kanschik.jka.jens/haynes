 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  e8
  \grace d8 cis8. d16 e a gis fis
  \grace e16 d16. cis32 d8 r16 d \grace fis16 e d
  b'8. d,16 cis8~ cis32 d e32. d64 \grace e16 d16 cis a'8
}


\relative c'' {
  \clef treble
  \key a\major	
   \tempo "2. Allegro"
  \time 3/4
  r4 e2~
  e4 d8 cis d4
  \grace e8 d4 cis e~
  e d8 cis d4
  \grace e8 d4 cis
}

\relative c'' {
  \clef treble
  \key e\major	
   \tempo "3. Andante"
  \time 4/4
  b4. e8 e4 dis
  r8 b4 gis'16 e e4 dis
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "4. Allegretto"
  \time 3/4
  r4 e d d cis b
  a fis' fis fis e r
  b'4 b,4. cis16 d cis8 a' a4. gis16 fis
  \grace e8 d4 d8 cis e cis
  cis4 b r
}