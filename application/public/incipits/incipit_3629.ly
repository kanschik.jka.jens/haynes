\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato"
  \time 2/4
    \override TupletBracket #'stencil = ##f
  \partial 8
  \times 2/3 {d16 es e}
  f8 r r \times 2/3 {fis16 g f}
  e8 r r \times 2/3 {b16 c d}
}
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio"
  \time 3/4
      es4 d r as' g r
      bes2 c4
      bes8 a as4 g
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Romanze. Gratioso"
  \time 2/2
      d2 f4 r8 r16 d
      c2 f4 r8 r16 c
      bes4 bes g' es8 c bes2 a4
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegretto"
  \time 6/8
      f8. es16 d8  d es f
      fis4 g8 g4 r8
}
