\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro ma non troppo [Tutti]"
  \time 4/4
  c,4. e8 g g g g
  a4 g2 f4 e gis a g
  g2 f4 e
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro ma non troppo [Solo]"
  \time 4/4
  \set Score.skipBars = ##t
  R1*105
  r2 g'8. e16 f8. d16
  c1 g e' c g' e,
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andante [Tutti]"
  \time 3/4
      c4.. g16 e8. g16
      c,4. r16 \times 2/3 {g32[ a b]} c8 r16 \times 2/3 {a32[ b c]}
      d4.
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andante [Solo]"
  \time 3/4
    \set Score.skipBars = ##t
  R2.*9
  r4 r g8 c
  c4 b8 c d e
  d8 e16 f e4 g8. c,16
  \grace c8 b4. \grace d8 c8 b a
  a4 b8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Vivace"
  \time 2/4
  \partial 8
  e16 f
  g8 g16 e f8 f16 a
  g e c8 c'16 b a g
  fis g d g f e d c
  c b a g
}