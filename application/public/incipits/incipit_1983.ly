\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 3/2
     \set Score.skipBars = ##t
   R1.*3 
   d2 g fis 
   g4 fis g a fis g
}

