\version "2.16.1"
         #(set-global-staff-size 14)

\relative c'' {
  \clef treble  
  \key bes \major
    \time 4/4
    \tempo "1.Allegro assai "
     bes4. f16 bes d4. bes16 d
     f8 f f f f f f f
     f d c es d bes 
}