\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Du machst, o Tod, mir nun nicht ferner bange [BWV 114/5]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key bes\major
   \tempo "o. Bez."
   \time 4/4 
   \set Score.skipBars = ##t
  r8 f8 d es f16 g as8~ as g16 f
  g8 a a16 bes c d es c d8~ d16 f g f
  c f bes, f' a, f' g f 
 
                                                                                   

}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key bes\major
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
  R1*8 
  r8 f8 d es f16 g as4 g16 f
  es8 a a16 bes c d es,8 bes r8
  
       
}



