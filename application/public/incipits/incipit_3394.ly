\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato [Tutti]"
  \time 2/2
  <d, bes'>2 r4 f'8 d
  bes4 f d' f,
  <d bes'>2 r4 f'8 d
  bes4 f d' f,
  bes4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   R1*35 	
   r2 r4 f4
   bes8 f f2 es4
   \grace es8 d4 d2 c4
   bes8 c d es f d c bes a4 bes r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio [Tutti]"
  \time 2/4
  \partial 8
  bes8
  es16[ bes] r bes g[ bes] r g'
  f[ as,] r as f[ as] r
  
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio [Solo]"
  \time 2/4
  \partial 8
  r8
  \set Score.skipBars = ##t
  R2*7
  r4 r8 bes
  es es4 es8
  es16. d32 es8 r32 g[ bes g] es es g es
  \grace g8 as4 as32[ g f g] as f c' as
  as8 g r
}	

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro non molto [Tutti]"
  \time 3/8
    bes'4. f es8 c f bes,4 d8
    bes f d' bes4 d8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro non molto [Solo]"
  \time 3/8
  \set Score.skipBars = ##t
  R4.*51
  bes'4. f
  es16 d es c f d
  bes4 f'16 d
  g es d c b c
  f d c bes a bes
}