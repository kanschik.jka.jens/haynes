\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Ihm hab ich mich ergeben [BWV 97/8]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key f\major
                      \tempo "o. Bez."
                      \time 2/4
                      \partial 8
                      % Voice 1
                      c8 d16 c d8 r16 d32 es f es d16
                      d8 c r8
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key f\major
                     \set Score.skipBars = ##t
                        % Voice 2
                        a8 bes f bes4~
                        ~bes8 a16 g a4~ a8
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key f\major
   \tempo "o. Bez."
  \time 2/4
  \partial 8
     \set Score.skipBars = ##t
     r8 R2*15 r4 r8 c8
     d16 c d8 r8 f16 d d8 c r8

}