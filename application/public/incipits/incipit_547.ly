 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Vivace"
  \time 2/2
  g'4 r8 e16 f   g8[ f16 e] d c g'8
  a16[ f g8] r g16 a   \grace g8 f[ f16 g] \grace f8 e[ d16 e]
  \grace e8 d4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Siciliano"
  \time 6/8
  \partial 8
  f,8
  es'8. d16 c8 as'4 c,8
  b8. c16 d8 g,4 f'8
  f8. g16 as8 \grace as8 g4.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 2/4
  r8 c e f
  g a4 g16 f
  g8 a4 g16 f
  g8 f16 e f8 e16 d
  e8 e c d
  e
}
