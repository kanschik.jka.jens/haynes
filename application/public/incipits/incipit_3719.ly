 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  r2 c16 d e f  g32 a g f g16 bes
  a2 a32 bes bes d g,8 g16 bes a f
  e g f a, g e f r
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 3/4		
  f,8 c'4 a8 bes16 a bes8
  a c bes d c es
  d4 c r8 e
  f a, g bes bes8. a32 g
  a8[ c g bes]
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 2/4
  r8 f4 a8
  g16 f e d  c bes a g
  a8 f r f'
  f16 e f32 e d16 e4
  d2 c4 r
}
