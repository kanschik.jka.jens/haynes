\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. All.o Espressivo [Tutti]"
  \time 2/2
    <g, e' c'>2 r8. c'16 e8. c16
    g4 r r8. e16 g8. e16
    c4 \grace { d16[ c b] } c8 d e f g a
    g2 f4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. All.o Espressivo [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   R1*92 
    c'2~ c16[ b a g]  fis g a g
    g2~ g16[ fis f e] d e f d
    c4 \grace { d16[ c b] } c8 d e f g a
    g4. fis8 f4 r
}
