\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Ouverture"
  \time 2/2
  c4. c8 e8 e g g
  c4 c, a'4. a8
  a4 d, g4. g8
  g4 c,
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude. Vivement"
  \time 2/2
  \partial 2.
  c8 c c4 g
  c e8 e e4 d
  e8 f e f g a g a
  e f e f g a g a g2.
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gravement"
  \time 4/4
    c8. d32 c g'16 f e d 
    c8. d32 c g'16 f e d 
    c8. d32 e d8. e32 f e16 g, a g a g f' e32 d
    e16
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Gratieusement"
  \time 3/4
  g'8 f es d c b
  \grace b8 c4. d8 es f
  es d c d es f
  es d c4 g'
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Ouverture. Ardiment"
  \time 4/4
  \partial 8.
  d16 e fis
  g8 b, a g
  g'4 g
  g4. fis16 e d4 c8 b e4~ e16 d c b a8 b c d
  b4 \grace a8 g4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Gravement"
  \time 4/4
  \partial 8
  b16 g d'8 d d g fis4. g8
  a16 b a b c8 a \grace a8 b4 \grace a8 g4
}
