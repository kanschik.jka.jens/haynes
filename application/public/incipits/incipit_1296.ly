 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Larghetto"
  \time 3/4
    c8. d16 c8. f16 e8. d16
    c8. d16 c8. f16 e8. d16
    c8. d16 \grace c4 bes2
    a8 a' g f e d
    c8. d16 \grace c4 bes2
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 4/4
    f,16 g a bes c8 f~ f e4 d8~
    d c4 bes a16 bes c4~
    c8 bes16 a g8 f e c f4~
    f e
}

\relative c'' {
  \clef treble
  \key f\minor	
   \tempo "3. Largo"
  \time 12/8
    c8. des16 c8  f4 f8 f4 e8 r r des
    c8. des16 bes8 as8. bes16 g8 as4. r8 r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Tempo di Menuetto"
  \time 3/8
      c4. bes a g
      f8 g16 a bes c d e e4
      f8 c a
      bes g4
}