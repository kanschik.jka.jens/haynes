 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 2/2
    d4. c8 bes4. a8
    g2 g
    es'4. d8 c4. bes8
    a4 bes8 c a4. g16 a
    bes4 g
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 2/2
  r8 d d d  g, g' g g
  g f16 es f8 f  bes, es es es
  es d16 c d8 d
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Presto [Vn]"
  \time 2/2
  bes,16[ c d es]  f g a f  bes c d es f[ g a f]
  bes[ bes, c bes] bes' bes, c bes a g f8 r16
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Presto [Ob]"
  \time 2/2
  d4 c bes r8 c f4 es8. d16 c4 r8 f,
  f' es d c bes4 r8 bes
  es d c bes a4 r8
}
}