\version "2.16.1"
         #(set-global-staff-size 14)
\header {
  poet =  "Aria, Christen  zeigt durch euer leben"
}
\new Staff \with {
  instrumentName = "Oboe 1"}   
  
         \relative c'' {
  \clef treble
  \key  c \major
   \tempo " Aria"
  \time 4/4
  \partial4 e4 c16 ( b c d) e2 c'4 a16 (gis a b) c2 a,4  f16 e f g  a2 f4 d'16 (cis d e) f2 d'8 (b)  
  }
\new Staff \with {
    instrumentName = "Oboe 2"}
     \relative c'' {
  \clef treble
  \key  c \major
    
   \partial4 c4 a16 (gis a b) c2 a4 c16 (b c d) e2 f,4  d16 (cis d e) f2 d4 f16 (e f g) a4 d16 cis d e f4~
     }
     \new Staff \with {
  instrumentName = Bass }     
         \relative c'' {
  \clef bass
  \key  c \major
  \time 4/4
  \compressFullBarRests
     R1*22 a,2 e \grace {d8} \trill c4. b8 a2 e'1 ~ e2. f8 (e) c'4 a \grace {g8} f2 e r4 r8 a, a'4 f 
         }
    