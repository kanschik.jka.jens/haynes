\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro Vivace [Tutti]"
  \time 4/4
  <f, a f'>4 r <g e' c'>4 r
  <a a'>4 d'2.
  c4 a8. f16 c4 <g bes e>4
  <f a f'>4 r r2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro Vivace [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*85 
  r2 r16 f, g a bes c d e
  f2 c
  a'1~ a8 g bes g \grace a8 g8 f \grace g8 f8 e
  f4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andantino [Tutti]"
  \time 6/8
  c,8 g e' c g e'
  d g, f' d g, f'
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andantino [Solo]"
  \time 6/8
   \set Score.skipBars = ##t
   R2.*2
   g'4. c gis a8 r d
   c b a g a b
   d4. c4 r8
}	

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo. Allegro Vivace"
  \time 2/4
  \partial 8
  c8 c4 d8 e f16 e f g f8 a
  c,4 d8 e f16 e f g f8 c'16 bes
}