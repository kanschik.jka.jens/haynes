\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "Rondeau"
  \time 3/4
    g'2 \grace f8 e d16 c
    g'8 f d2
    e8 g g c c e,
    \grace e8 d4 c8 b a g
}