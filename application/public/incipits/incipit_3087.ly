\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt + echo): Treues Echo dieser Orten [BWV 213/5]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key a\major
   \tempo "o. Bez."
   \time 6/8
   a4.~ a4 e8
   fis d e fis d e
   a4.~ a8 cis b
   cis a b cis a b
   e4.~ e4
    
   
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt (+ echo)"
  \key a\major
   \tempo "o. Bez."
   \time 6/8
   \set Score.skipBars = ##t
    R2.*18 a4.~ a4 e8 fis d fis r4 r8 
}



