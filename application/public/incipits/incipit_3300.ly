\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegretto [Tutti]"
  \time 2/4
  \partial 16
  c16
  g'8 \times 2/3 { g16[ g g] } g8 g
  \grace g8 a4 r8 r16
  f
  b8 \times 2/3 { b16[ b b] } b8 b
  c c, r4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegretto [Solo]"
  \time 2/4
  \partial 16
  r16
  \set Score.skipBars = ##t
  R2*54
  r4 r8 c
  g'8 \times 2/3 { g16[ g g] } g8 g
  c g r16 g \grace b8 a16 g
  c8 g r g
  a16 f e d g cis, d b
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio [Tutti]"
  \time 2/4
  \partial 8
  g'8
  g \times 2/3 { c,16[ g c] } c8 g'
  g f r f
  f \times 2/3 { b,16[ g b] } b8 f'
  f8 e r
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio [Solo]"
  \time 2/4
  \partial 8
  r8
  \set Score.skipBars = ##t
  R2*29	
  r4 r8 g'
  g \grace f8 es8 \grace d8 c8 g'
  g f r f
  f \grace es8 d8 \grace c8 b8 f'
  f e r
  
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto [Tutti]"
  \time 2/2
  \partial 4
  g'4
  c,2 c c e8 d c b
  c4 a g f
  e2 e'8 d c b c4 a g f e2 c'
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto [Solo]"
  \time 2/2
  \partial 4
  r4
  \set Score.skipBars = ##t
  R1*76
  g'1 a2 b,
  c4 c g b
  c c g b
  c c g b
  c2 \grace e8 d2 \grace f8 e2 d
}
