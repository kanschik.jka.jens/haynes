\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  <c, c'>2. f'16 e d c
  g'8. e16 c4 r c
  <a f'>8 <a f'>8 <a f'>4
  <a f'>8 e'16 f g f e d 
  d e b d c4 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Oboe Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*8 
  c2. \grace f16 e8 d16 c
  a'8 g g4. g8 a b
  c e, e2 fis4 a8 g g4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Soprano Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*43
  c2 c4 \grace f16 e8 d16 c
  g'8 e16 c4 r c
  f2. \grace e16 d8 c16 b
  d8 c c4 r2
}
