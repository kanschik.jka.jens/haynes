\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. o.Bez."
  \time 4/4
  c8 es16 f g8 b, c4 r8 d
  es c16 bes
}
