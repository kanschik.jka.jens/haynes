\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/4
  f8 f4 a16 g
  f8 f4 a16 c, d8 d4 bes'16 d,
  d8 c4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio staccato [Tutti]"
  \time 3/8
  bes4. f'8 d bes
  g f r
  f' c a f e r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio staccato [Solo]"
  \time 3/8
    \set Score.skipBars = ##t
   R4.*33
  f8 d bes
  \grace g8  f4 r8
  f'8 c f,
  es'4.
  bes'16 g f es d c
  \grace bes8 a4 r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 3/8
  f8 f f
  f16 a f c a f
  f'8 f f
  f16 a f c a f
  g'8 g g
  g16 bes g e c g
}
