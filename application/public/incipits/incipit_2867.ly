\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo [Tutti]"
  \time 3/4
  f,4 r f8. g16
  a4 f a8. bes16
  c4 f, r f r8 c'16 bes a g f e
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*7 	
   f4 c~ c16 bes a bes a8 g16 f d'4. d8
   d8. b16 c4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro [Tutti]"
  \time 2/2
  a'16[ c a c] a c a c  g[ c g c]  g c g c
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro [Solo]"
  \time 2/2
     \set Score.skipBars = ##t
   R1*5 	
   r2 f8 e16 d c[ bes a g]
   f4 r8 c' d16 f d f c[ f c f]
   
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Presto"
  \time 3/8
  f16 e f c d e
  f e f c e f
  g f g c, e f
  g f g c, f g
}


