\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/2
  \partial 8
  f8
  bes4. a16 g f8. g16 f es d es
  d4 bes r2
}
