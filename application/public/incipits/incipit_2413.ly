\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  bes4 f'8. es32 f g8 f r f
  bes a16 g f8 es es d r c
  d16 f, f f
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*8 
  bes8 bes16 c d bes c d es8 d r d~
  d f16 es d c bes c c a bes8 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  f8 f f f  f f e e
  a a a a  a a g g
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*3
   r2 c4. d16 bes
   c8 f, bes16 a bes c a8 f r4
   r8 g' f16 g e8 f8 c f16 e f g
   e8 e d16 e c8 f4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro [Tutti]"
  \time 3/4
  bes'8 bes,16 c bes8 bes bes bes
  f' f,16 g f8 f f f  bes4 r8
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro [Solo]"
  \time 3/4
    r4 r8 bes bes bes
    c16 bes a g f8 es' d c
    d d16 es f8 d c bes
    c d16 es f8 c bes a
    bes
}