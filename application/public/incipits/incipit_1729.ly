\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo"
  \time 3/4
  g4 g8 a a8. g32 a
  bes4 bes8 c c8. bes32 c
  d4 d es
  d g, d'
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 4/4
  g4. bes8 c c d d
  bes a16 g d'8 g  f f es es
  d4. d8 d[ g,]
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Alla breve"
  \time 2/2
  d1~ d2 c~ c bes
  a1 g2. bes4 a a d d
}
