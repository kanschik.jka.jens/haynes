 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Allegro ma non troppo"
  \time 2/4
  fis8 b,4 g'8
  fis b,4 g'8
  fis e16 d cis8 b
  ais16 b cis8 fis,4
}


\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "2. Adagio"
  \time 4/4
    d8 r16 cis
    b16. a32 g16. fis32
    b8 r16 a g16. fis32 e16. d32
    g8 a16 b a8 a d, a' a a
}

\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "3. Allegro"
  \time 3/8
  \partial 8
  fis8
  g fis e
  fis e16 d cis b
  e8 d cis
  b4
}
