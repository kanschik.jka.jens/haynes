 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "Cantabile [Org.]"
  \time 3/4
  \partial 4
    c4
    bes8 a g f d'8. e32 f
    g,2 e'8. f32 g
    g4 f
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "Cantabile [Oboe]"
  \time 3/4
  \partial 4
  r4
  \set Score.skipBars = ##t
   R2.*23 
   r4 r d
   \grace c8 b2 g4 d'2 d4 e2 r4

}
