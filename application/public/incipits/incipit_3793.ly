 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Ouverture"
  \time 2/2
  a4. a8 d4 fis
  e4. d8 e fis g e
  a4. g16 fis e8 fis g a
  fis4 \grace e8 d4 r8
}
 
\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. 1.er Rondeau. Legerement"
  \time 3/4
  r4 fis e
  d e8 fis g fis e4 a b8 a
  g fis e d cis b a4 fis' e
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. 2.e Rondeau"
  \time 3/4
  \partial 2
  f g
  a d,8 f e g f4 \grace e8 d4 a'
  g8 f e f d e cis4
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4. 1.ere Air"
  \time 6/8
  \partial 4.
    a4 d8
    a4 b8 a4 g8
    fis4 e8 fis4 g8
    a4
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "5. 2.e Air"
  \time 6/8
  \partial 4.
  f4 g8
  a4 cis,8 d4 e8
  \grace e8 f4 \grace e8 d8 g4 f8
  e4 d8
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "6. Sarabande"
  \time 3/4
  a4 d4. e8
  cis4. d8 e4 fis \grace e8 d4. g8
  g4. fis16 e e4
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "7. 1.er Rigaudon"
  \time 2/2
  \partial 4
  d4 d a' a g fis d e fis g2 fis
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "8. 1.er Fanfare"
  \time 6/8
  \partial 8
  a8 a4 d8 d4 a8
  fis4. d8 d' e fis g a e a g fis4.
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "9. 1.er Menuet"
  \time 3/4
  fis8 g fis4 e d e8 fis g4
  fis g8 fis e d
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "10. 1.ere Gavotte"
  \time 2/2
  \partial 2
  fis4 e g fis e4. d8
  cis4 \grace b8 a4
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "11. 1.er Tembourin"
  \time 2/4
  fis8 g16 fis e d e cis
  d8 a a a'
  a g g fis fis4 e
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "12. Chaconne"
  \time 3/4
  r fis d a' a4. g16 a
  b4 g8 a b4
  e, a8 b a g fis4 fis d
}