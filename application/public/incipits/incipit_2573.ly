 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c' {
  \clef bass
  \key g\minor
   \tempo "1. Largo [Bass]"
  \time 12/8
  \partial 8
  g8
  es'4 es8 es8. d16 es8 es4 d8 r r d,
  c'8. d16 es8 d4 c8 bes8. a16 g8 r r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo [Solo]"
  \time 12/8
  r8 r g d'2. c4 a8
  fis4. r8 r g c8. d16 es8 d4 c8
  bes8. a16 g8 g'4.~g4 
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Presto"
  \time 4/4
  \partial 4
  d4 g,8 a bes c d4 g
  fis g2 g8 a16 bes
  f4 d8 es16 f es4 c8 d16 es
  d8 c bes4 r
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Tempo giusto"
  \time 3/8
  g4 a8 b16 a g8 a
  b16 c d8 e
  c4 b8 e d r
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}