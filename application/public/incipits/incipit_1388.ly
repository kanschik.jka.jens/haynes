 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
    b16 c d c b4 r g'8 r
    b r g r d r b r
    \grace d16 c16 b c d c4 r
   }
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 4/4
     a'4 a~ a16 a \grace g16 fis \grace e16 d16
     d8 cis r b' \grace a16 g16 \grace fis16 e16
     e d d8
}

 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Rondo"
  \time 3/4
    d'4 b c
    d4. e8 d4
    g,8 b d b e d
    d4 c b
}
