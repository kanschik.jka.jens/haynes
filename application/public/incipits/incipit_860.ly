\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 2/4
  d4 d
  d16 c bes c d8 g
  d4 d d16 c bes c d8 g,
  es' es es es d16 e f4
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Largo [Tutti]"
  \time 4/4
    d4. bes'8 d,4. c'8
    d,4. g8 es8. d16 c bes a bes
    a8. g16 f
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Largo [Solo]"
  \time 4/4
  d1~ d2~ d4~ d16 c bes a
  bes es d es c f es f d8 g~ g16 f es d
}

\relative c'' {
  \clef treble
  \key g\minor
     \tempo "3. Allegro"
  \time 3/4
  g'8 g g g g g
  fis4 f r
  e es r
  d8 d d d d d
  cis4 c r
}
