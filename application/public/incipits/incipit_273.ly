\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Amusette " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. La Nonette"
  \time 3/8
  \partial 8
  g'8
  \grace g8 a8. g16 f8
  e d e16 f
  g8 g, c b4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxieme Amusette " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. La Bergere"
  \time 3/8
  \partial 8
  g'8
  c,4 d8 e d g
  f e d g4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisieme Amuzette" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gracieusement"
  \time 3/8
  \partial 8 c8
  \grace c8 d8. e16 f8
  e \grace d8 c e16 f
  g8 f e
  d4
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatrieme Amuzette " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Le Calotin"
  \time 2/4
  \partial 8
  g'8
  c, f e d
  c d16 e f8 e d g g f 
  e
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquieme Amusette " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. La Modeste"
  \time 3/8
  d4 g8
  fis e d
  e d c
  \grace c8 d4 \grace c8 b8
  \grace a8 g8 c4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixieme Amusette " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Naivement"
  \time 2/4
  b8[ a b c]
  d4. g8 fis[ e d c]
  b4 \grace a8 g4
}



