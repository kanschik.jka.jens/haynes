 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 4/4
    a'4 c~ c8 b16 a b8 c16 b
    a4. b8 c e, a4~
    a8 g g a16 b e,4 d'
}


\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "2. Allegro"
  \time 4/4
  r8 a16 b c d c d e8 a, a' a
  a g16 fis g a g a  f8 f16 e f g f g
  e d e f  e f e f   d c d e
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Affetuoso"
  \time 3/2
  g'4. c8 e,4. f8 g4. a8
  g4. c8 e,4. f8 g4. a8
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "4. Allegro"
  \time 3/8
  a'8 e f
  e16 d e f e8
  e f16 e d c
  b a b c b8
}