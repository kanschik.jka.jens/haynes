\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  e8
  a,16 a a a  b b b b  c c c c  d d d d
  e8 a e f
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Largo"
  \time 3/4
    g8 fis g a b a  g fis g a b g
    dis c dis g fis e
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. o.Bez."
  \time 3/8
    \grace { a16 b} c4.
    \grace { c16 d} e4.
    a8 a, a  a a' a
    a g16 f e8 g[ f16 e]
    
}

