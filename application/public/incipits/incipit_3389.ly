 \version "2.16.1"
   #(set-global-staff-size 14)

\relative c'' {
  \clef treble   
  \key d\minor
   \time 12/8
  r4 r8 s  a a d e f e f d cis b a d4
}