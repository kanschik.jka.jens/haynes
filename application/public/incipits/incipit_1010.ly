\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  bes8 f d bes bes' f d bes
  bes'16 d, es f g a bes c d8 bes f d
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*19 f4. g8 f4. g8
   f16 es d c bes8 c d16 c bes c d bes c d
   c bes a bes c a bes c   d8 bes8 d f g4
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Siciliano"
  \time 6/8
   g8. a16 g8 bes4 d8 es4. d
   es8. f16 es8 as4 es8
   d8. es16 d8 g4 d8
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/4
  bes'4 a8 g f es
  d bes a g f es
  d4 f8 es d c bes4 d
}
