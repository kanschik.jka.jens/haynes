\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef bass
   \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \key a\major
   \tempo "1. Introducione"
  \time 2/2
    e,,4. e8 a4. gis16 fis64 e d cis
    d4. b8 b'4. \times 2/3 { a16 gis fis}
    e8. d16 d8. cis32 d cis8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Tempo di Minuetto"
  \time 3/8
    a4. b cis16 a e' cis b a
    b e, e' d cis b
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Andante e piano"
  \time 2/4
  \partial 8
  e,8
  a8 a8. c16 b16. a32
  e'8 e4 f8
  e16 e d c d8 e
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Allegro"
  \time 2/4
      a'8 e4 f8
      e a \grace c8 b8 a16 gis
      a8 e4 f8 e a \grace c8 b8 a16 gis
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "5. Passepied"
  \time 3/8
    \partial 8
    e8
    a gis a
    e16 fis e d cis b
    a8 b gis
    a e a16 b
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "6. Larghetto"
  \time 4/4
    e4 a, a'4. g16 f
    e8[ \grace { b'16 c} d8 c b] c16 b a4 g16 f
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "7. Minuetto"
  \time 3/8
        cis8 d16 cis b a
        e'8 fis gis
        a4. a, d8 cis b cis b a a'4. a,
}

