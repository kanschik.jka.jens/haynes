\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Un poco andante"
  \time 2/4
     c8. d32 c  c8 c
     c16 f f4 r16 f
     f a a4 r16 c
     c8. a16 c bes a g
}
