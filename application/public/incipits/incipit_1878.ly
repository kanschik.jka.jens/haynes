\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro "
  \time 2/4
   \partial 16
   c16
   a'4 g f8 e16 d c4 b a r8
}

\relative c'' {
  \clef bass
  \key d\minor
   \tempo "2. Larghetto"
  \time 12/8
    r4. d,8. e16 d8 
    d4 d,8 f8. e16 d8
    a'4. a8. bes16 a8  a4 a,8 cis8. bes16
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Vivace"
  \time 3/8
    a'8 f e
    f a,16 bes c8
    a' f e f4 c8
    d16 e f g bes, g'
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 3/4
    g'8 e16 f g4 a g8 e g g a a
    g e16 f g4 a
    f4. e16 f
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Largo"
  \time 4/4
    c8 e b c16 d c8 a r e'
    f e r e d c r c
    bes a r a
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Vivace"
  \time 3/8
  f16 f f8 e d c16 b c8
    g'16 f e8 d
    g16 f e8 d
}
