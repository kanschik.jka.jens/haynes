 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 4/4
    bes4 g16 es g'32 f g16 as4~ as16 f d bes
    g' f g8~ g16 es bes g f' es  f8~ f16 d b g
    es' d es8~ es16 c g' c, b a b8
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Allegro"
  \time 2/2
  \partial 8
  es,16 f g8[ g16 as] bes8 bes16 c d8[ d16 bes] es8 es16 bes
  f'8[ f16 bes,]  as'8 as16 g f[ es d c] bes d f g
  as8 as as as
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Adagio"
  \time 4/4
  g'4~ g16 bes a g fis4~ fis16 d a'8
  bes16 a g8~ g16 f es d es d c es  a, c es f
  d c d8
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "4. Menuet"
  \time 3/4
   es,8 f g4 bes
   es es8 d es4
   bes f' as \grace as8 g4 f8 g es4
}