\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata d-Moll" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 4/4
    d8[ \grace { g16[ a] } bes8 a g]  f16 e d8 r cis
    d16 a f'8 e16 a, g'8 f a16 d, cis8. b32 a
    d16 a f'8 e16 a, g'8 f a g bes
    a g16 f e8 d \grace d8 cis4 r8
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro"
  \time 4/4
    d16 cis d8  a f'  e16 cis d4 f8
    e16 cis d f  e cis d f  e cis d4
    a8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro assai"
  \time 2/4
    d4 d bes4. a16 g  a8 g f e
    f d4 bes'8
    a g f e
    f16 d f a d f, a d
}



