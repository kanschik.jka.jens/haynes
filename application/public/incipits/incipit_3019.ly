
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Poco Allegretto piu Andante"
  \time 2/4
  a4 cis8. d16 \grace cis8 b4 d8. e16
  \grace d8 cis4 fis16 d gis e
 a e e e fis d gis e
 a e e e fis d gis e 
 a e a16. fis32 e16 d cis b a4 r4

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Presto"
  \time 2/4
 a4 cis 16 a e' cis a'4 r4
 a,4 d16 a fis' d a'4 r4
 a,4 cis 16 a e' cis a' e cis' a e' cis a fis e2~ e
   
}



