\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  c,8 e g e f d b g
  c e g e f d b g
  c16 c c c  c' c c c b b b b f' f f f
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*23
   g'4 \grace f16 e8 \grace d16 c8 \grace c8 b4 f'
   e8 \grace a16 g8 f16 e d c \grace c8 b4 \grace g'8 f4
   e16 c d e   f g a b c4 b8 a
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Largo [Tutti]"
  \time 4/4
  f,16 f8 f f f16 f e8 e e d16
  c c8 bes' bes bes16 bes bes8 a c e,16
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Solo]"
  \time 4/4
  \set Score.skipBars = ##t
  R1*6
  r2 c8 f4 \grace bes16 a16 \grace g16 f16
  f8 e4 d8 c \grace a'16 bes8 bes bes
  \grace { a16[ bes]} bes8. a32 bes c8. e,16
  e f r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/8
  \partial 8
  g8
  c4 \grace e8 d16 c32 d
  e8 f r
  c4 \grace e8 d16 c32 d
  e8 f r
}