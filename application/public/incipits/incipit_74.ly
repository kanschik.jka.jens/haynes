\relative c'' {
  \clef treble  
  \key d\major
    \time 4/4
    \tempo "1. Allegro Spirituoso"
    \compressFullBarRests
    \override MultiMeasureRest.expand-limit = #3
    d2 d d r2 R1*10 cis1 d1 cis1 d1  cis1 dis1 e4   
}  