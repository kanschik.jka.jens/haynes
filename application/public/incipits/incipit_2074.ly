 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro spirituoso"
  \time 2/4
  \partial 16
  g'16 <d, b' g'>4 d'32[ c b16] b32 a g16
  g8 g4 r16 g'
  <e, c' g'>4 e'32[ d c16] c32[ b c16]
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Adagio"
  \time 3/4
  c8 g'16 e \grace d8 c2~
  c8 b16. d32 f,16 f e f d' f e g
  f8. e32 f
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Allegretto"
  \time 3/4
g,4 g' g
\grace a8 g4 fis4. a8
\grace b8 a4 g4. b8
e,2
}
