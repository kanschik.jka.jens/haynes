 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
  c8. c16
  a4 f8. f16 c4 e8. e16
  f2. g8 a
  c4 bes8. bes16 bes8 c16 d c8 bes
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Cantabile"
   \time 4/4
   g4 fis8 g  a g fis e
  e4 g8. f16 f4 r
     \set Score.skipBars = ##t
   R1*6 
   g'4. a16 g
   f8 e d c
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro à la norvège"
  \time 2/4
  a4. a8 a4. a8 a4. a8 a4. a8
  d16 e f g a bes a g
}
