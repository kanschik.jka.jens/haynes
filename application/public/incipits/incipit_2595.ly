 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Presto"
  \time 2/2
  c4 r8 c g'4 r8 g
  c g e c g' g, r
}


\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "2. Largo"
  \time 3/2
  r2 c a e'1.~
  e2 d4. c8 b4. a8 gis2
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Allegro"
  \time 12/8
  c8 c c   c c c  d4 g,8 d'4 g,8
  e' d c e d c g'4 d8 g4 d8
}
