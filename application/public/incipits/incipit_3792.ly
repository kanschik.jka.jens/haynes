 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. "
  \time 4/4
        << {
      % Voice "1"
      d2 r4 d8. d16
      b4 d r d8. d16 b4 d r d8. d16
      a4 d r 
           } \\ {
      % Voice "2"
  r4 a8. a16 fis4 a
    r fis8. fis16 d4 fis
    r d'8. d16 a4 d
    r fis8. fis16 e4 fis
      } >>

  
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. "
  \time 4/4
  f4 f f r f f fis fis fis g~ g
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. "
  \time 3/8
  d8 d d a a a fis' fis fis
  e e e a a a
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}