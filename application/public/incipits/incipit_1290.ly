\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}




\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Andante [Tutti]"
  \time 3/4
  a8. b16 c8 b a gis a e e4 r
  f8 e d c b a e' e e4 r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Andnate [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*4
   e4 e4. e8 f4 d4. d8 e4 c4. c8 d4 b4. b8
   c8. b16 a4 b
}
  
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 3/2
  a'2 e4. f8 e d c b
  c e a4 e4. f8 e d c b
  c a e2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Adagio"
  \time 4/4
  \partial 4
  g4 c c \times 2/3 {c8[ g' f]} \times 2/3 {e8[ d c]}
  d4 d \times 2/3 {d8[ fis g]} \times 2/3 {d8[ c b]}
  c4 c
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Andante Allegro"
  \time 2/2

  e4 a,~ a8 c b a
  b4 e,~ e8 d' c b
  c4 d8 e f4 \grace e8 d4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 4/4
  f4 c8. d16 c4 bes8. c16
  a4 r r2
  f'4 c8. d16 c4 bes8. c16
  a4 r8
  
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. o.Bez."
  \time 2/4
  \partial 8
  f,16 g a4 g
  f16 c d e f g a bes
  c8 d bes4
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Lento"
  \time 3/4
   d4 d8 a f e
   f4 g a
   bes a8 g f e f4 r r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. March"
   \time 2/4
  \partial 8
  c,8
  f4 f8. f16
  f4 c16 d e f
  g8 g g g g4 r
}

