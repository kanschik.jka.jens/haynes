\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  c,4. g8  c c c c
  c4. b16 a g4 r
  c'4. g8 c, c c c
  c'4. b16 a g4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
    g'2 e8 c c' a
    g2 e8 c c' a
    g e c' a g e c' a
    g4. f8 e4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante"
  \time 3/4
  g8. g,16 b8. d16 g8. b16
  c8. a,16 c8. e16 a8. c16
  d8. b,16 d8. g16 b8. d16
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Solo]"
  \time 4/4
   \set Score.skipBars = ##t

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro moderato [Tutti]"
  \time 4/4
  c,8 d16 e f g a b  c8 c c c
  c16 g a b c d e f  g8 g g g
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro moderato [Solo]"
  \time 4/4
  c4 c8. e16 d4 g,
  d' d8. f16 e4 c
  \grace a'16 g4 e8 c
  \grace g'16 f4 e8 d c4
}