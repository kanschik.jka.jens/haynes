\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran + Bass): Lobe den Herren, der künstlich und fein dich bereitet [BWV 137/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key e\minor
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                      e8 b e g b fis
                      g4. fis8 g e fis gis gis4. fis16 gis
                      a2.~ a8 g c a e g~g
                 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\minor
                     \set Score.skipBars = ##t
                  R2. b,8 e, g b e cis 
                  d4. c8 d b
                  cis dis dis4. cis16 dis
                  e2~ e16 d c b
                        
                
                  }
>>
}


\relative c'' {
	<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Sopran"
                     \key e\minor
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                  \set Score.skipBars = ##t
                     R2.*10 b4 b e
                     d4. c8 e b
                     c4 c8 b c4~c
                      
                     
                  }
\new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Bass"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                 R2.*8 e,,4 e b'
                 g4. fis8 g e
                 fis4 gis8 a fis gis
                 a4 e4. a8
                 dis,4. e16 fis b,4
                        
                
                  }
>>
}

	
	
