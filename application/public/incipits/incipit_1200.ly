 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Larghetto"
  \time 4/4
    d4. bes'8 a bes16 g fis8. g16
    a8 d,~ d16 c bes16. a32 bes8 g'~ g16 f es d
    es8 d~ d16 c c bes a16. g32 f8 r
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro"
  \time 4/4
   r8 g bes g d'4 g
   f8 a, bes es~ es d16 c d8 fis, 
   g a16 bes c8 c d, c' bes a
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Adagio"
  \time 3/4
  r4 g es bes'2.
  r4 es as, as2 g4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Giga"
  \time 12/8
  \partial 8
    d8
    g a bes  a g fis  g f es  d c bes
    a bes c  bes a g  fis e d r r d'
    e, d' c  fis, d' c bes a g r
}