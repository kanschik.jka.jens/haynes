\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sinfonia (und Chor): Aus der Tiefen rufe ich, Herr, zu dir [BWV 131/1]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\minor
                      \tempo "Adagio"
                      \time 3/4
                      % Voice 1
               R2. r4 d4 g8 a16 fis fis8. e16 d4 r4
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\minor
                     \set Score.skipBars = ##t
                        % Voice 2
              r4 d4 g, fis8. e16 d4 r4 r4 a'4 d8 es16 c
              bes8. a16 g8 a bes c
              d4.
                      
 }              
 \new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Basso "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\minor
                     \set Score.skipBars = ##t
                        % Voice 3
             g,,,4 g' es
             d bes g
             d d' fis
             g g, es d
                      
 }        
 
>>
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Sopran"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\minor
                      \tempo "Adagio"
                      \time 3/4
                      % Voice 1
                      R2.*23 r4 d4 g, fis8. e16 d4 r4
               
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Alt "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                        R2.*23 r4 bes4 c
                        d8 a a4 r4
              
 }                     
               
            
>>
}


