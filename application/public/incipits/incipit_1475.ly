\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  bes4 a bes16 bes, c d es f g a bes f g a bes c d es f 
  bes, c d es f g a bes8 bes, r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*19
    r4 r8 f8 g f16 es d8 es
    c16 d es f g es d c f8 es16 d c8 d
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Siciliana [Tutti]"
  \time 12/8
  bes4 bes8 bes4 bes8 a4 a8 a4 a8
  g4 g8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Siciliana [Solo]"
  \time 12/8
  d1.~ d4. g fis8. e16 d8 r r fis
  g4 d8 f8. es16 d8 es8. d16 c8 es4.
  d2.
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro [Tutti]"
  \time 3/8
  f16 bes, bes bes g' bes,
  f'16 bes, bes bes g' bes,
  f' bes, es bes d bes c a f8 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro [Solo]"
  \time 3/8
   \set Score.skipBars = ##t
   R4.*21
   d8 bes es d bes es
   d c bes a16 c es8 r
}