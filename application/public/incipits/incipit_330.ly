\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c' {
  \clef alto
  \key d\major
   \tempo "1. Andante assai"
  \time 4/4
  \partial 4
  d4
  a'1 a16 d cis d  e d cis b \grace b16 a8 g16 fis a g fis e
  d4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
    d2 e8 d e fis
    d2 e8 d e fis
    g4 e b cis
    d a fis d
}


\relative c' {
  \clef alto
  \key d\major
   \tempo "3. o. Bez."
  \time 3/4
  d4. fis8 e4
  e4. g8 fis4
  d e fis g e r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  f4. a8 f4 r
  c4. f8 c4 r
  a4. bes8 c4 d
  d8 c c4~ c8 d16 c bes8 a
  g4 fis
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 4/4
    f4 d16 es d es f4 f
    \grace g16 f8 es16 d  d4~ d16 es d16 es \grace g16 f8 es16 d
    c8 bes4 d8 c bes4 f'8
    \grace f8 es4 d r8
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Tempo di Minuetto"
  \time 3/4
  a4 a a a4. g8 a bes
  c2 \times 2/3 { d8[ e f]}
  c2 \times 2/3 { d8[ e f]}
  c2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. [Allegro]"
  \time 4/4
  d2 es
  d4 d, g bes
  a bes \grace d8 c4 bes8 a
  bes4. a8 g d g bes d4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante"
  \time 4/4
    d4. b8 e4 d
    \grace d8 c4 b r d,
    g2 b8 g c a
    d4. b8 g b c a d4. b8 g16 a b c d e fis g
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Cantabile"
  \time 2/4
  d4 \grace fis16 e8 d16 c
  b8. c16 d8 d, g b a d
  \grace c8 b4 a
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/2
  d2~ d8 a' fis d
  a2~ a8 fis' d a
  fis g a d, fis e d cis
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio"
  \time 4/4
  d4~ d16 a' f d d cis cis8~ cis16 d e f
  g8 g,4 g8 g16 fis fis8
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Arioso"
  \time 2/4
  d4 cis8 b a4 d
  e8 fis16 g fis8 g
  g fis r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 2/2
  es8. bes16 bes4~ bes8 es d f
  es8. bes16 bes4~ bes8 es d f
  es4 g,2 c4 c8 bes bes4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 2/2
  c8 bes bes4~ bes8 d es c
  c8 bes bes4~ bes8 d es c
  bes g as f g es f d
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Siciliano"
  \time 6/4
  \partial 4
  bes4
  g2 es4 f4. es8 f4
  es2 c4 bes2 es4
  d2 es4 as2 g4 f8 es f g f g f2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 4
  d8. d16
  g4 d8. d16 b'4 g8. g16
  d'4. c8 b4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 4/4
  \partial 4
  b8. c16
  \grace e8 d4 d2 c16 b a g
  \grace fis8 e4 d2 g8 fis
  e d g fis e d e' d
  \grace d8 c4 b2
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegretto"
  \time 2/4
\partial 4
   g'8 e
   d b g b
   a4 g'8 e
   d b g b
   d4 g8 e d b g b e4
}

