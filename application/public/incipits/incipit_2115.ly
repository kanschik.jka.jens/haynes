\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  \partial 8
  bes8
  f' as4 g8 f bes4 g8
  f16 es d f  es d c es d8 bes r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*10 
    r2 r4 r8 d16 es
    f8 bes es,8. d32 es d8 bes16 a bes c d es
    f8 bes es,8. d32 es d8 bes r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 4/4
  g'2 fis8 g a bes
  fis g a bes c a bes g
  a d, es4. d16 e f4~
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/8
  bes16 a bes c d c
  bes a bes c d c
  bes8 g' f es4 d8
}