 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'Dolce riposo'" 
    }
    \vspace #1.5
} 

 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\minor
   \tempo "Largo"
  \time 4/4
  r2 r4 es~
  es4. f16 g
  d8 bes r4
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Medea"
  \key c\minor
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
     R1*2
     r4 bes~ bes~ bes16 as g as
     g f es4. r2
}
