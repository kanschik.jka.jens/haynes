\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Herr, was du willst, soll mir gefallen [BWV 156/4]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key bes\major
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                      r8 bes8 d, es f d'~ d16 c bes a
                      bes c d c bes d c es d es f d f es d c 
                      d8[ f,] f'4~ f16
                     
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violini"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key bes\major
                     \set Score.skipBars = ##t
                        % Voice 2
                    R1 r8 bes,8 d, es f d'~ d16 c bes a
                      bes c d c bes d c es 
               
                      
                       
     
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key bes\major
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    R1*8 r8 bes8 d, es f d' d16 c bes a
    bes4. a16 g f8 bes d, es f4 r4
   
    
    
    }