
\version "2.16.1"
 
#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
  \tempo "1. Adagio"
  \time 4/4
  \partial 4 
  g4 \grace {cis16 [d e]} d4 d2 d4
  d16 e b c c2 b8. c16
  \grace b8 a4 g8. d'16 \grace d8 c2
  c16 b e d d4 r16
 
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/2
  g4 g4. b8 d g
  \grace a8 g8 fis fis4 r16 c16 b c d c b a
  g8 fis fis4. a8 b c
  d16 e d c b4 r8
 
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 2/4
 c4~  c16 b a g
  \grace b8 a8 g8. c16 e g
  g a, a8 b16 d32 e \grace g8 f16 e32 d
  \grace e8 d16 [c] c8.
 
 
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Scherzando"
  \time 2/4
  c16 \grace f8 e32 d e16 f fis g g8
  c,16 \grace d8 c32 b c16 d dis8 e
  f16 \grace b8 a32 g a16 b c g a g
  \grace  a8 g16 f \grace g8 f16 e \grace e8 d8 \grace c8 b8
  
 
}
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
 f4. f8 \grace bes8 a4 g8 f
 f e e4 \grace c'8 bes4 a8 g
\grace a8 g8 f f4~ f8 e d c
 cis d f c \grace c8 bes2
 bes16 a d c c4 r16
 
 
 
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Tempo di Minuetto poco Moderato"
  \time 3/4
  \partial 4
  c8. c16
 f4~ \times 2/3  {f8 e d}  \times 2/3  {c8 bes a}
 c16 d a bes bes4
 
  
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante poco Adagio"
  \time 3/4
  \grace c8 c'4. b8 a g
  \grace b8 a8 g16 a g4. c8
  c16 d, d4 d8 e8. c'16 c8. a16 \grace g8 f4. e8
  d a' a g g f
  \grace f8 e8 d16 c c4 r4
 
 
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondeau poco Moderato"
  \time 2/4
  e8. d16 c8 c
  \grace e8 d8 f16 d \grace c8 b8 a16 g
  c8. \grace {e32 [d c d]} e16 d8. \grace {e32 [d c d]} g16
  \grace f8 e4 d8 r8
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/2
 b4 b4. c8 b a
 \grace a8 g4 g4. a8 g f
 \grace fis8 e4 e'4. d8 g e
 \grace d8 c2 b4 r4

 
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Cherzanto"
  \time 2/4
  b8 b16. c32 a8 a16. b32
  g8 g16. a32 fis4
  g8 g16. b32 a8 a16. c32
  \grace d8 b4 a8 r8
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 4
  c4 f8 c c4. a8 d c
 \grace d8 c8 bes bes4. a8 g f
 e f g a bes8. a32 bes c8 bes
 a16 bes c d c4 r8

 
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Tempo di minuetto"
  \time 3/4
  f4 a c~ c8. bes16 d8. bes16 g8. f16
  e4 g bes~ bes8 a c a f4
 
}

 


