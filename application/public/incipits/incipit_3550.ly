 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
    b8
    b a a gis
    gis16 a b4 cis8
    b e4 a,8
    gis16 a b4 cis8
    b e4 dis16 cis b8
}


\relative c'' {
  \clef treble
  \key e\major	
   \tempo "2. Largo"
  \time 3/4
  R2.
  e,8 b e fis16 gis fis8 gis16 a
  gis8 a16 b b2
  r8 e dis cis b a
  b e b4 a
  gis8 b b a a gis gis fis
}

\relative c'' {
  \clef treble
  \key e\major	
   \tempo "3. Allegro"
  \time 3/4
  e4 fis8 dis e4
  b2.
  cis8 b cis a e'4
  b2.
  cis8 e d4 cis
  b8 dis e4 b
  a8 cis b4 a gis8
}
