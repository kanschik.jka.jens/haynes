 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
    d4 b g r8 d'
    e16 g32 fis g16 d
    c8. b16 b4 r16
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 4/4
  g'4 r8 d a'4 r8 d,
  b'16 g fis g  d b' a g  a fis e fis d a' g fis
  g e d e a, g' fis e fis8 d r16
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Largo"
  \time 3/4
  r4 b4. a8
  g fis e4 e'8. fis16
  dis8 cis b4  e8 b
  c4. e8 d16 e c8 b4.
}



\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Giga"
  \time 6/8
  g4 a8 b a g
  d'4 e8 fis e d
  g g, a b a g
  a4 b8 c b a
  
}