 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro moderato"
  \time 2/4
  \partial 8
    a16 bes
    c8 a bes g
    f r r e16 f
    g8 a bes a g4 r8 f f4 r8 f f4
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Poco Adagio"
  \time 3/4
  f2.~ f2~ f8. g32 a
  bes4. c16 d es f g es
  d8. es32 d c8 r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Menuetto"
  \time 3/4
  \partial 16*5
    c16 \grace { d32[ c b] } c4
    d b c
    bes g e d2. c4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Finale. Vivace"
  \time 6/8
  \partial 8
    f,16 a c8 c c  c8. b16 c8
    c8. b16 c8 c8. b16 c8
    c4 f8 f4 a8
    a4 c8 c4
}