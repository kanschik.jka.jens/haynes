\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4 
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
    bes8 bes4 \times 2/3 { c16 d es}  f16 d bes8~ bes16. f'32 d16. bes32
    g'8 g4 \times 2/3 { bes16 a g} \grace a16 g8 f4
}



\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Siciliano"
  \time 12/8 
    es,8. f16 es8 es16 g bes es g bes,   \grace b8 c8. d16 c8 bes es c
    bes as g  as g f
}



\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/8
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
    bes4. bes'
    \times 2/3 {c,16 d es} d8[ c]
    \times 2/3 { d16 c bes} bes4
    \times 2/3 {c16 d es} d8[ c]
}


