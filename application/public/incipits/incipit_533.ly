 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
   \time 12/8  
      c4.~ c8. d16 b8  c4.~ c8. e16 d8
      e8. d16 c8 f4. e
}
