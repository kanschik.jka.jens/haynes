 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  g'2 b4. c8 d8. b16 g4 r2
  g4 g2 e8 c d b g4 r2
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Gartioso"
  \time 3/4
  c4 d e f4. d8 e4
  \times 2/3 { a,8[ f' d]} \grace d8 c4 \grace c8 b4
  \grace b8 c4.
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Presto"
  \time 2/4
   g4 b a d
   b8 g fis d
   g g' g a16 g fis8 d e fis
   g
}
