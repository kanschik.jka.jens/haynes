\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 4/4
   << {
      % Voice "1"
          r2 r8 f8~ f16 g f32 es d c
          \grace bes8 a4 r r8 d~
          d32 es e f  fis g as a
         } \\ {
      % Voice "2"
          bes4 bes,8. bes16 a4 r 
          c'4 es,8. es16 d4 r
      } >>
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Molto allegro"
  \time 4/4
  bes2 d
  \grace d16 c4 bes r2
  es2 g
  a,4 bes r2
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Menuetto"
  \time 3/4
  bes'2 a16 g f es
  d4 r es
  f r \times 2/3 { g8[ es c]}
  bes2 a4
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Adagio"
  \time 4/4
    es,4 g bes bes,
    es16 es r es es es r es bes' bes r bes bes bes r bes
    es es r es es es r g, f f r f as as r f
    bes'2~ bes4. as16 g f8 es~ es16 d32 es f es d c bes4 r
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Menuetto. Allegretto"
  \time 3/4
  \partial 4
  f4
  bes f d es c a
  bes c d8. c16 c2
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "5. Romanze. Adagio"
  \time 3/4
  R2.
  as'2 g4
  f8. g32 f as8 g f es
  g4 f8. es16 d8. bes16 es4 r r 
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "6. Andantino"
  \time 2/4
  \partial 8
  f8
  d bes bes' bes
  bes a es4~ es8 d c g'
  bes a bes4
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "7. Allegro molto"
  \time 2/4
  \partial 4
  f16 d f d
  bes8 bes f'16 d f d
  bes8 bes bes16 c d es
  d es f g  f g a f
  bes8 bes
}
