\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key es\major
    \time 3/2
    \tempo "1. Andnate"
     r2 bes4 g c bes
     bes2 r r
     r bes4 g c bes bes2 r r
     r c4 g es' d d2 r r
     r f4 d g f f d
}
