 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
    d4 d d a'16 g fis e
    d8 a fis d d'4 a'16 g fis e
    d8 a fis d b'8. g16 d'8. b16
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Andante"
  \time 4/4
  \partial 8
  d8
  b16 a b c d8 d e16. fis32 g4 \grace fis8 \times 2/3 { e16[ d c]}
  d8 \grace e16 \times 2/3 { d16[ c b]}
  c8 \grace d16 \times 2/3 { c16[ b a]} b8 g r
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Comodo"
  \time 3/8
  \partial 8
  a16. d32
  d8[ cis] a16. fis'32
  fis8[ e] fis32 g a16
  g8 fis r
  e16 b' g e b cis
}
