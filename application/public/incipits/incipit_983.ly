\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  bes8 bes, r bes'16 c d8 d d d
  d bes r d16 es f8 f f f 
  f d r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*12 
    r8 f d a bes c16 d c d es c
    d8 bes r4 r2
    r8 f' d a bes c16 d bes c d bes 
    d c d es  d f es d c8 f, r4
    
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. n.N."
  \time 3/2
    g2 bes d
    fis,1.
    g2 bes d
    fis,1.
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. [Allegro] [Tutti]"
  \time 3/4
      d,8 f16 es f8 bes16 a bes8 d
      f, bes16 a bes8 d16 c d8 f
      bes,8 d16 c d8 f16 es f8 bes
      a4 g r
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. [Allegro] [Solo]"
  \time 3/4
     \set Score.skipBars = ##t
	R2.*22
f8 d d bes bes d
c f,16 g a bes c d es g f es d4 r r
}