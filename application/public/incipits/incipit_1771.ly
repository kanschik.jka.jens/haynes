 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Marche"
  \time 3/4
    g4. a8 b4
    c4. d8 e4
    d4. d8 g4
    e4. fis8 g4
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 2/2
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Largo"
  \time 6/8
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}