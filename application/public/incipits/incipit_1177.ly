 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "o.Bez."
  \time 3/4
  r4 r4 r8 c
  f4. g8 a bes c4 f, r8 c'
  bes4. a8 bes8. g16
  a4
}
