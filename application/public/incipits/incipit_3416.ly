\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. [Allegro]"
  \time 3/4
  c2 d4 e2 f4
  g a g g f e
  a4. f e d
  g4. e8 d c
  d8. e16 f4 e
  e8 d c b a g
  }
