 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef alto
  \key es\major
   \tempo "1. Allegro"
  \time 4/4
    bes2. c8 as8
    g2. as8 f
    bes2. c8 as8
    g2. as8 f
    bes g es es es g f d es
}


\relative c'' {
  \clef alto
  \key es\major	
   \tempo "2. Musette"
  \time 6/8
  g4 as8 bes g es
  \grace d8 c4 bes8 \grace d8 c4 bes8 
  g'4 as8 bes g es
  d as' g \grace g8 f4 r8
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Allegro con Expressione"
  \time 2/4
  \partial 4
  g16. es32 as16. f32
  bes8. c16 f, as g bes 
  \grace bes16 as8 g bes16. g32 es16 es
  bes'16. g32 es16 es es d es g 
  \grace g8 f4
  
}
