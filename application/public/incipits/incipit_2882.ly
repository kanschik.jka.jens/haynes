\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  g4 g g r8 g
  bes g bes d g,4 g 
  g r8 g bes g bes d
  g, f es d
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*19
   d,4 d g8. a16 bes8. c16
   d4 g \times 2/3 { fis8[ a g] } \times 2/3 {fis8[ e d] }
   \times 2/3 {c8[ es d] } \times 2/3 {c8[ bes a] } bes8 g bes d
  
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Larghetto"
  \time 4/4
  a'8 g16 f e8 d bes'2 
  a8 g16 f e8 a  f32 g a g f e d cis d4
}



\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 3/4
  g8 g, g g g g
  g' g, g g g g
  g'4 bes d
  g, bes d
}