 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  f4. d8 d c g'8. a32 bes
  a16 a8 bes32 g f16 f8 g32 e f16 c c a' \grace g8 f4
  e16 f f g g a c, f f8 e r
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro moderato"
  \time 4/4
  \times 2/3 { a'16[ g f]} f4 f8 e8. f32 g f8 r16 c
  c8. d32 es d8 c bes a r4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro assai"
  \time 3/8
  f4. \grace f8 e4.\trill f8 c c c c c
  a'4. \grace a8 g4. a8 f f   f f f 
}

