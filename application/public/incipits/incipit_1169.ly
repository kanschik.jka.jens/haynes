 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 8
    e,8
    a a4 gis16 a b cis d4 cis16 b
    cis d e fis e8 a cis, b r
}


\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "2. Largo"
  \time 3/4
  e4 d8 c b d
  c a4 f'8 e d
  e a,4 f'8 e d 
  e a gis a b d,
  d c r
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "3. Allegro"
  \time 3/4
   a4 cis8. d32 e d4
   cis b a
   fis' d8. e32 fis fis4
   fis4 e
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}