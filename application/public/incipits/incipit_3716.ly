 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
    f4 f2 f8. c16
    a'4 a2 f8. c16
    d4 bes'2 g8. e16
    f8. a16 c,2
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Minuetto di Variatione"
  \time 3/4
  a2 g4 g8 f f4 r
  d'2 f8 d d c c4 r
}
