 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key es\major
                      \tempo "1. Moderato"
                      \time 2/2
                      R1
                      r4 es16 f g f es8 g es g
                      f4 r r2
                      r4 f16 g as g f8 as f as
                      g4 r r2
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key es\major
                        % Voice 2
                      es,2 g4. f8 es4 r r2
                      f2 as4. g8 f4 r r2
                      g2 bes4. bes8
                  }
>>
}


\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key as\major
                      \tempo "2. Largo"
                      \time 3/8
                      R4. R
                      \grace es16 des8 c bes
                      as8. g16 as8
                      es'16. c32 bes8 r
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key as\major
                     % Voice 2
                     es8 des c
                     \grace c16 bes8 as g
                     as4 f8
                     es des c c bes r
                  }
>>
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Tempo di Gavotta"
  \time 2/2
  \partial 2
  es,4 es
  bes'4. es,8 \grace g16 f8 es f g
  es2 \grace { f16[ g]} as4 g
  f es  \grace g8 f8 es f g
  f d c bes
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}