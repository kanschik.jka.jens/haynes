
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Suitte  deuc Musettes "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Prelude. Lentement"
  \time 2/2
\partial 4
g4 e4. f8 e d c b
c4. d8 e4. f8
e4. f8 d e f g 
e4 \grace d8 c4

 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "2e. Suitte "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude. Lentement"
  \time 3/4
e8 d c d e f 
g2 c,4
b8 c d e f d 
e4. f8 e f 
g4 \grace f8 e4
 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "3e. Suitte "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Musette. Tendrement"
  \time 2/2
\partial 2
e4. d8
c4 e8 f16 g f4. g8
e4 d e4. f8
d4 e8 c f4. e8 d2 

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "4e. Suitte "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Gratieusement"
  \time 3/4
\partial 4
 g4 e4. f8 g d 
e4 \grace d8 c4 e8 f 
g4 f e
d e c \grace {b16[ c]} 
d4 g,
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ve. Suitte "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Musette"
  \time 2/2
\partial 2.
g4 c e 
d g f8 e d c 
d4 g, c e 
d f8 e d c d b c4

 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VIe. Suitte "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude. Lentement"
  \time 2/4
\partial 8
 c8 g'8. f16 e g f e 
d8. c16 d e f d
e8 c e16 d e f d8 g
 
 
}



