 
\version "2.16.1"
 
#(set-global-staff-size 14)
 \layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt) - Wo zwei und drei versammelt sind [BWV 42/3]" 
    }
    \vspace #1.5
}
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe I"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key g\major
                      \tempo "Adagio"
                      \time 4/4
                      % Voice 1
                  r2 r8 d8 \grace b'8 c8. d16
                  b c b c d4~ d8 c b16 d c b
                  a b a b c e d c
                   
               
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe II"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key g\major
                        % Voice 2
                  r8 d,8 \grace fis8 g8. a16
                  fis g fis g a4~
                  a8 g f16 a g f e fis e fis g b a g
                  fis g fis g a c b a g4 r4
                  
                        
                  }
                  
              
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key g\major
   \tempo "Adagio"
   \time 4/4
   \set Score.skipBars = ##t
     R1*12 r8 g8 e' r16 a,16 d8 r16 g,16 c b c8
     b8 r16 
  
}

