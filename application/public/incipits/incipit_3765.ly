\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
      \partial 4
      g4
    e'2. f8 e
    d r c r d r e r
    f2. g8 f
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andante"
  \time 2/2
      \partial 4
      g'4
      c4. b16 a g4 g
      g8 fis f4 r d8. f16
      e4. c8 b a f' d
      c2 b4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Menuetto"
  \time 3/4
  e2 f8 e16 f
  g4 c8 b a g
  f4 f8 e f g
  f4 r r
  }
