
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I "
    }
    \vspace #1.5

}
 
\relative c'' {
  \clef treble
  \key d\major
  \tempo "1. Adagio"
  \time 3/4
  d8. cis32 d d,8 a'' b a
 gis8. e16 g4 r4
 fis8. g32 a g8 b a16 g fis e
 \grace d8 cis4 d 
 
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
 a8 d fis a b cis, d r8 b'8
 cis, d r8 d'~ d cis16 b a8 g
 \grace g8 fis4

 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Andantino Cantabile"
  \time 2/4
  g16 b d,4 g,8
 e'8 e \times 2/3 {e16 [fis g]} \times 2/3 {g16 fis e} 
 e8 d
 
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Tempo di Minuetto"
  \time 3/8
  d4. e
 fis8 g16 a b cis
 d8 cis16 b a g 
 fis8 e16 d cis b
 a8 g4 fis
 
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Grave"
  \time 4/4
 d4 g,16 b d g fis e d8 r8 d'8
 e, c'32 e, d c d8 b'32 d, c b c8 fis16. g32 a8 c,
 b16 d g,8
 
 
 
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 3/4
  g4 \times 2/3 {d'8 c b} \times 2/3 {a8 g fis}
  g4 \times 2/3 {d'8 c b} \times 2/3 {a8 g fis}
  g8 g, b d g [b]
  b4 cis, d
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Largo"
  \time 3/4
  d4 g8 d' c16 bes a g
  fis4 g r8 g,8
  es'4 d8 c bes a bes a g4
 
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro molto"
  \time 2/4
  g8 b16 d g8 fis
  b4 a 
 \grace {a16 [b]} c4. b8
 a g16 fis g4
 \grace {a,32 [b]} c4. b8 
 a g16 fis g4

 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allegro ma non molto"
  \time 4/4
 \partial 8
 b8 e c' b dis \grace dis8 e4 r8 c8
 \grace c16 b8 a16 g \grace b16 a8 g16 fis g fis e8
  
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Larghetto "
  \time 3/4
 g4 g'~ \times 2/3 {g16 [fis e]} \times 2/3 {d16 c b}
  c8. b16 c4 r4
 a4 a'~ \times 2/3 {a16 [g fis]} \times 2/3 {e16 d c}
  b8. a16 b4
 
 
 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Minuet "
  \time 3/4
  b8 g e b'' g e
 \grace e8 dis2 e4
 c8 a'4 c,8 d a
 b fis' \grace fis8 g2
 
 
 
 
}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "4. Giga "
  \time 6/8
 \partial 4
 b8 a g fis e b e g
 \grace g8 fis4. r8 c'8 b
 a g fis b, fis' a 
 g fis e fis 
 
 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key a\major
   \tempo "1. Grave e Cantabile"
  \time 4/4
 a8 e16 a, fis'8 a16 a, e'8 a16 a, d4
 cis d cis b8 e,
 a16 a' b cis \grace e,8 d4
 
  
 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 3/8
 e16 d32 e fis8 [e]
 d16 cis \grace cis8 d4
 d16 cis32 d e8 [d] 
 cis16 b \grace b8 cis4
 cis16 b32 cis d8 [cis] 
 cis16 b gis'8. [a16]
 cis,16 b gis'8. [a16]
 \grace d,8 cis16 [b b8]
 
  
 
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Larghetto"
  \time 2/4
 e8 a,4 a'8
 a16 gis gis8 r8 d'8 
 d16 c c8 r8 e,8 f f~ f16 e32 f a [f e d] 
 e8 e~ e16 d32 e a [e d c] d8
 
  
 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Allegro Moderato"
  \time 4/4
 e16 d cis b a8 gis \grace gis8 a4 r16 d16 e fis
 e d cis b a8 gis \grace gis8 a4
 
  
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Pastorale"
  \time 3/4
 g4 g g g8 c g e f d 
 e c' e, c d b c2.
 
  
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Non tanto Adagio"
  \time 4/4
 d8 g, g' g g fis r8 d8
 e e~ e32 e fis g a [b c e,] d16 fis g8 
 
  
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro assai"
  \time 2/2
 c2 c' c b4 a a2 g4 f f2 e4 d 
 e8 d c2.
 
  
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 6/8
 \partial 8
 d8 g8. a16 g8 d'16 b g8 d 
 \grace d8 e4 d8 g,8~ g32 a b c d e fis g 
 \times 2/3 {e16 d c} g'8 d 
 
  
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro "
  \time 3/4
  d4 g,16 b d g b8 d
 \grace g,8 fis2 g4
 \times 2/3 {e8 e' c,} \times 2/3  {d d' b,} \times 2/3  {c c' a,}
 \grace a8 b4. a8 g4
  
 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Non tanto Adagio ma Cantabile "
  \time 3/4
 b4. e8 dis4
 \times 2/3 {e8 fis g} \grace g8 fis2
 g16 e8. dis16 e8. fis16 g8.
 a16 fis8. e16 fis8. g16 a8.
 b4
 
  
 
}

\relative c' {
  \clef treble
  \key g\major
   \tempo "4. Arietta von Variazioni"
  \time 2/4
 \partial 8
 d8 g8. [b16 d8 d]
 \times 2/3 {e16 fis g} g8 r8 g8~
 g16 fis a g fis e d c \grace c8 b4
 
  
 
}

