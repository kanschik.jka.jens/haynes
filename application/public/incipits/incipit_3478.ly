 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
    bes'2 bes,4. c16 d
    es4 es es es
    es8 d c bes g'2
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio arioso affetuoso"
  \time 2/4
  \partial 4
    bes16 r bes r
    bes8 g r16 g as f
    \grace g16 f16 es d8 r g16 as
    bes8 g'16 f es d c bes
    bes8. as16 g8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Menuetto. Allegretto"
  \time 3/4
  \partial 4
  g'8 f
  d4 d f8 es 
  c4 c es8 d bes4 bes' g
  f2
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Rondo"
  \time 2/2
  \partial 2
  d4 es
  c c d8 c d es
  f2 g8 f g a
  bes4 f es8 c d bes
  c4 f,8 r
}

