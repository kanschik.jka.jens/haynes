 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro Moderato"
  \time 2/2
    f,2 f4. f8
    g a16 bes a2 g4 f r r2
    a2 a4. a8 bes c16 d c2 bes4
    a8[ f']
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Minuetto"
  \time 3/4
  f,2. \grace g'8 f16 e f g a8 r r4
  c,2. \grace a'8 g16 fis g a bes8 r r4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro non tanto"
  \time 6/8
  \partial 8
    f,8 f4 a8 a4 c8
    c4.~ c8 a'8 f c4.~ c8 a' f c4 c8 c bes a f4
}
