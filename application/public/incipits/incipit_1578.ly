 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Ouverture"
  \time 2/2
    d2~ d4 cis8 b
    a4. a8 g4. fis16 fis4. d'8 d4 e
    cis4. a8 a'4. a8
    a1
}
 