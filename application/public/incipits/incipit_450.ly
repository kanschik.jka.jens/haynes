\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Gentillesse" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gaiment"
  \time 2/4
  c4 b c4. d8 e f g a
  b,4. c8
  d d d g e4. d8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement"
  \time 2/4
  e8[ d e f d e]
  c2 g4
  g'8[ f g a f g]
  e2 d4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gaiment"
  \time 3/8
  c8 c c d16 e c8 c
  g'8 g f e4 d8
  g8 g f e4 d8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxième Gentillesse" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Modérément"
  \time 2/4
  es4. d8 c d es f
  g4. f8 es d f4~
  f8 es d c
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement"
  \time 2/2
  \partial 2
  e8 f g4 e8 f g4 f e
  d c e8 f g4
  e8 f g4 f e d2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gaiment"
  \time 6/8
  \partial 8
  c8 e4 e8 g4 g8
  e4 c8 c4 g'8
  a bes a g4 a8
  g4.
  c,4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisième Gentillesse" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gaiment"
  \time 3/4
  \partial 4
  g4 c8[ d e f g c,]
  d4 c b
  c8[ d e f g c,]
  d4 c b
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Gracieusement"
  \time 6/4
  \partial 1
  c4 es4. d8 c4
  c4. d8 es4 d4. es8 f4
  es2 d4 g4. c8 b4 c4
}
  
  \relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gaiment"
  \time 2/4
  \partial 8
  e16 f 
  g8 a g f
  e d16 e c8 e
  d c b a
  c16 b c d
  c8
}
  

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatrième Gentillesse" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gaiment"
  \time 2/4
  \partial 8
  g8 c c c16 d e f
  g8 a16 b c8 c,
  e4 d c~ c16 d e f
  g8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement"
  \time 2/2
  \partial 2
    c4 g'
    e8 d c d e4 d
    g,2 g'8 e f d
    e4 \grace d8 c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gaiment"
  \time 3/8
  e8. f16 d8 c c c
  g' g a f4 e8
  g4 b,8 c b16 c a8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquième Gentillesse" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Gaiment"
  \time 6/8
  \partial 8
  g8 d'4 d8 d e fis
  g4 g,8 g4 d'8 e d c c b a b a g g4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Gracieusement"
  \time 3/8
  b4 a8 g4 e'8
  d4 c8 b4 \grace a8 g8
  g'4 fis8 g4 d8 e c e d4.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gaiment"
  \time 2/4
  \partial 4
  g'8 d
  e fis g a
  d,4 e16 d c8 e16 d  c8 c c
  c[ b]
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Presto"
  \time 2/4

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixième Gentillesse" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Gaiment"
  \time 2/4
  \partial 8
  g8
  d' d d d
  e d4 e16 fis g8 g g b
  a d,4 g16 fis e8 d d c c b4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Gracieusement"
  \time 2/2
  \partial 2
  b4 a8 c
  b4 \grace a8 g4  g' fis
  g d e8 f d f
  e4 d8 c b4. c8 d2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gaiment"
  \time 3/8
  g'8 g g
  d4. e8 d c
  b4 a8
  b a16 b g8 c b16 c a8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Andante"
  \time 3/8

}

