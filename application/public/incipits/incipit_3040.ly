\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
             \set Staff.instrumentName = #"Oboe"
                     \key f\major
                      \tempo "o.Bez."
                      \time 4/4
                       f8 c a' f g c, bes'4
                       a,16 c f e f c a c 
                       bes d g f  g e c e 
                       f4 f,2 e8 c'
                      % Voice 1
                  }
\new Staff { \clef "treble" 
             \set Staff.instrumentName = #"Violine"
                     \key f\major
                        % Voice 2
                       R1 f8 c a' f g c, bes'4
                       a,16 c f e f c a c
                       bes d g f  g e c e
                  }
>>
}
