\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 16
  g'16 g4 \grace { f16[ g]} a16 g f e
  d16[ e32 f] e8 r16 e d c
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andantino"
  \time 3/4
  c8 c16 bes a8 a16 bes c8 c
  bes bes16 a g8 g16 a bes8 bes
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Menuetto con Variat."
  \time 3/4
  g'8. c16 g4 g4
  g8. e16 g4 r
  g8 f e f g f f8. d16 f4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/2
  g2~ g8[ c] \grace d16 c8 b b2~ b8 e e d
  d4 g8 fis fis e e d
  d4. b8 d4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro assai"
  \time 2/4
    \partial 8
    d'8
    d b r b b g r d e4 g16 e g e d8[ c b]
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 3/4
  \partial 4
  \grace bes8 a16 g a bes
  c4 r16 c f d c8 d16 bes
  bes a d c c4
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Rondo. Allegretto"
  \time 6/8
    c'8. d16 c8 c a c
    \grace c8 bes4 a8 \grace a8 g4 f8
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Andante"
  \time 3/4
  bes'2 as16 g f es es4 d r8 c
  bes es16 g  bes,4 as
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Menuetto"
  \time 3/4
  es,2. \grace g8 f8. es16 f8. g16 es4
  es' c as
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro"
  \time 6/8
  \partial 8
  es,8
  es bes g' g es bes'
  bes4 g8 r r es
  d bes f' f d as' as4 f8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegretto"
  \time 2/2
  e2 fis16 d8. a'16[ fis8.]
  \grace gis16 fis16 e8. e4
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Tempo di Menuetto"
  \time 3/4
  cis8. d16 e4 e
  a8. e16 e4 r
  b8. cis16 d4 d b'8. d,16 d4
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/2
  a'2. gis8 fis fis b b2 a4 \grace a4 gis2.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andantino"
  \time 6/8
    d8. e16 d8 d b g
    c8. d32 e e8 r g e
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro assai"
  \time 2/4
  \grace b'8 a16 g a b a8 d
  \grace b8 a16 g a b a8 d
  a8 b16 a g8 fis
}

