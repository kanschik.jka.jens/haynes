\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  f,4. g8 bes a c a
  bes8. a32 bes d8 bes f4 r
  g4. a8 c bes d f
  es8. d32 es g8 es c4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*34 
    r2 r4 d8 es
    g f f2 bes8 f
    f4. es8 d4 d8 f
    g,4 es'8 g
    bes,4 a8 c c bes bes4 r8
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio [Tutti]"
  \time 2/4
  g4 as8. g32 as
  bes8. c16 bes8 bes16. bes32
  bes16 as as16. as32 as16. g32 g16. g32
  f8 es r4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*14
     es4 as16 g g c
     bes8. c16 bes8 bes16. bes32
     bes16 as as16. as32 as16 g g16. g32
     g f es f es8 r16
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegretto"
  \time 2/4
  f8 f f8. es16
  d8 d \grace { c16[ d] } es8 d16 c
  bes f d f bes d f d
  c es g, c \grace bes8 a4
}