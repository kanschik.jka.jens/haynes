 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  f4 r r8 c d16[ f e g]
  f4 r r r8 f
  e[ f16 d] e8 f16 d e8[ f16 d] e8 f16 d
  e4 r
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Adagio"
  \time 3/4
  r8 d cis d a f'
  e4 a, a'
  a g8 fis g4
  g f8 e f4
  f8 g e4. d8
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 2/4
  f4. a8
  g4. bes8
  a4. g16 f
  e f d e
  c4
}

