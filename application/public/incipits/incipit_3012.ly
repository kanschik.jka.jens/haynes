 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\minor
                      \tempo "1. o.Bez. "
                      \time 4/4
                      R1
                      r2 d4. a8 f' e e4 d8 e16 f e bes c d
                      c8 a
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key d\minor
                        % Voice 2
                        a4. d,8 bes' a a4
                        g8 a16 bes a e f g f8 e d e 
                        r a, a'2 gis4 
                        a
                  }
>>
}
