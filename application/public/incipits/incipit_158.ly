\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 3/4
   bes4 bes bes
   b b b
   bes2 r4
   c2 r4
   a4 a a  a a a a2 r4 bes2 r4
}
