 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "o.Bez."
  \time 4/4
      c8. d16 g,4 c r8 d
      e d16 e c d e f d4 g,
      \times 2/3 { c8[ d c] } \times 2/3 { b8[ c b] }
      \times 2/3 { a8[ b a] } g16 a g f
      e8. fis16 fis8. e32 fis g4 r2
}
