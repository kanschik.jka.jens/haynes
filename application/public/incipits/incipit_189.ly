 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro"
  \time 2/2
  r8 c,16[ d] es f g8  c, c'4 \times 2/3 { d16 b d }
  c8 c,16 d e[ f g8] c,
}
 

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio"
  \time 2/2
  R1 r2 r4 r8 g,
  g1~ g4 g8 a16 b c b c8~ c16 a' f d
  b8 g f'4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro"
  \time 3/8
  c16 d es8 d
  c g4
  c16 d es8 d c g'4
}
