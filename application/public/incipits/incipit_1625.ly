 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\minor	
   \tempo "Adagio non molto"
  \time 4/4
  r4 f,2 e4
  f16 c f as  c f, as c  des c bes as g des' c bes
  as c, f g as
}
