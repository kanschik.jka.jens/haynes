\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  c8. d16 d8. c32 d es8 c16 b c d es f
  g c, b c as'8 c, b8. a16 g4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*15
   c8 es \grace es8 d c16 b c d es f g8 c,
   as' g16 f es8 d16 c b8. a16 g4
  
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio assai [Tutti]"
  \time 3/2
  c,1.
  <c d>1.
  <c d f>
  <c d f as>
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio assai [Solo]"
  \time 3/2
   \set Score.skipBars = ##t
   R1.*12
   r2 g c~ c b f'~ f es4 d es2
   d2 g1~
   g2 fis1
   g1.
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro"
  \time 2/4
  c16 b c d es8 d
  c d16 b c8 g
  c16 es d c g'8 c,
  b8. a16 g4
}