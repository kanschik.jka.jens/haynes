\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 3/2
    b2 c c
    c1 b2 e e4 d e fis 
    g d c2. b4 b1
}
