 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 4/4
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
  R1*23
  c8 c16. c32 c8c \times 2/3 { b16 d f} f4 e8
  \times 2/3 { b16 d f} f4 e8 \grace e16 d8 c g'8. cis,16
}