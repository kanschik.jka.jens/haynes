 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

 \relative c'' {
<<
\new Staff { \clef "treble"   
                     \key es\major
                      \tempo "1. All. moderato"
                      \time 4/4
                      % Voice 1
                      <g, es'>4 r r2
                      R1
                      es'8 f16 g as bes c d es f g as bes c d es
                      es8 d d d d4 r
                  }
\new Staff { \clef "treble" 
                     \key es\major
                        % Voice 2
                      es,4 r es8 r f r
                      g4 r g8 r as r
                      bes2 r8 g g g g as as as as4 r
                  }
>>
}

 

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Menuetto All."
  \time 3/4
  \partial 4
  es,8. es16
  es4 bes g'
  es bes' g
  es' bes g'
  as f r
}

\relative c'' {
  \clef treble
  \key as\major	
   \tempo "3. Adagio"
  \time 2/4
  \partial 8
  es,8
  as8. as16 g8 as
  bes8. c16 as8 r
  des c bes as g f es r 
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "4. Rondo"
  \time 3/4
   es8 es g4. f8
   f16 es d es bes8 bes bes4
   f'8 f as4. g8 g16 f e f bes,8 bes bes4
}