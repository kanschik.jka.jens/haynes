 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro ma cantabile"
  \time 2/4
  r8 d g4~ g8 d g a
  bes4 a g8 d es4~
  es8 c d a
  bes g
  
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Largo"
  \time 4/4
  r4 r8 bes' a bes16 c fis,4
  fis8 g r4 r2
  r8 bes bes4~ bes16 bes c d c8 bes
  a a a4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Allegro molto"
  \time 2/4
  \partial 8
    g'8
    fis f e es
    d16 c bes a g8 bes'
    a as g fis
    g16 fis g a g8
}
