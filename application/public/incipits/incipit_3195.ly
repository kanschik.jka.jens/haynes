\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran + Alt): Wenn des Kreuzes Bitterkeiten [BWV 99/5]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Traverso"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key b\minor
                      \tempo "Duett"
                      \time 4/4
                      % Voice 1
                      r4 fis16 d cis b g'8 g g g 
                      g fis fis fis fis eis16 fis eis fis eis fis
                      gis a b8~b16 gis a eis
                   
                 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key b\minor
                     \set Score.skipBars = ##t
                     R1 r4 cis16 ais gis fis d'8 d d d 
                     d cis cis cis c b b b 
                        
                
                  }
>>
}


\relative c'' {
	<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Sopran"
                     \key b\minor
                      \tempo "Duett"
                      \time 4/4
                      % Voice 1
                  \set Score.skipBars = ##t
                     R1*5 r4 cis16 ais gis fis
                     d'8 d d d
                     d cis cis cis c b b b
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Alt"
                     \key b\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                 R1*4 r4 fis16 d cis b g'8 g g g 
                 g fis fis fis fis eis eis16 fis eis fis
                        
                
                  }
>>
}

	
	
