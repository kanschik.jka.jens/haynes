 \version "2.16.1"
         #(set-global-staff-size 14)
         \relative c'' {
  \clef treble
  \key a \major
   \tempo "1. Allegro assai [Tutti]"
  \time 4/4
\chordmode  {a,:5.8} r \chordmode  {a,:5.8} r \chordmode  {a,:5.8} cis (b cis) d4.e8 \grace {e16} d4 cis8 b a4 r \chordmode  {a,:5.8} r
  }
  
  \relative c'' {
  \clef treble
  \key a \major
   \tempo "1. Allegro assai [Solo]"
  \time 4/4
   a'2. e8 cis \grace {b16} a2~ \tuplet 3/2 {a8 cis e} \tuplet 3/2 {a gis fis} e d d2 cis8 b \grace {b16} a2
  }
  
  \relative c'' {
  \clef treble
  \key d \major
   \tempo "2.Adagio ma non troppo [Tutti]"
  \time 4/4
  d16 (fis,) fis4 (g8) d'16 g, gis4 a8  b16 (g) e (b) a (cis) e (g)
  }
  
   \relative c'' {
  \clef treble
  \key d \major
   \tempo "2.Adagio ma non troppo [Solo]"
  \time 4/4
  d1~ d16 (dis) \tuplet 3/2 {e16 (g b)} \tuplet 3/2 {a g fis} \tuplet 3/2 {g fis e} fis32 e dis16 dis8 r16 dis fis dis d8 (cis8)
   }
  \relative c'' {
  \clef treble
  \key a \major
   \tempo "3. Allegro [Tutti]"
  \time 3/8  
  a16 cis e a e cis a cis e a e cis a8 r r a4.\p b gis 
  }
 \relative c'' {
  \clef treble
  \key a \major
   \tempo "3. Allegro [Solo]"
  \time 3/8   
 a' e4 cis8 \grace {b8} a4. a8 cis e d cis b \slashedGrace a gis4. a8 cis e 
  } 
  
  
   