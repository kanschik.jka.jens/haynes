\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sinfonie I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
  c16 bes a g f8 d' d c4 d16 e
  f8 e16 d
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sinfonie II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  e16 d
  cis8 a a' e fis e4 a8
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sinfonie III " 
    }
    \vspace #1.5
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  g'16 f e d  g f e d c8 g e c
  e'16 f e f
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sinfonie IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 4/4
  g'16 g g g   g g g g    g d b d    g d b d
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sinfonie V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  d8
  g f16 es d8 c   bes16 a g4  bes16 c
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sinfonie VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  e16 d
  e8 g g16 f e d  c b c4 g'8
}
\pageBreak
