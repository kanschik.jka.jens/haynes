\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8
  d8
  d8. e16 cis8. d16
  d4.d32 fis e64 d cis b 
  a8. a16 g8.[ fis32 g]
}
