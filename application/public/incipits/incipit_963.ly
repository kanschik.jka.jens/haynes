 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/4
  f16 c c8~ \times 2/3 { c16[ bes c]} \times 2/3 { d16[ c bes]}
  bes16 a a4 f16 a
  \grace bes16 a g g8~ g16 bes a c
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Larghetto"
  \time 3/8
  f,8 f f
  \times 2/3 { f16[ g f]} bes8 a
  a g f
  f16 d es8 r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Andante"
  \time 4/4
  \partial 8
  f,8
  c'8. d16 c8 a
  d8. e16 f8. d16
  c a8. g8. a16 f4.
}

