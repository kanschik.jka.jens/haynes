
\version "2.16.1"

#(set-global-staff-size 14)

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 2/2
 g4. bes,16 a a4. c16 bes
 bes8 d16 c c8 [es16 d] d8 g, g'4~
 g8. \times 2/3 {fis32 g a} fis 8. g16
 g8 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 2/2
   d4 d d r8 d8
 bes g d'g fis d r8 d
 bes g d' g es d r8 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 3/4
   r4 d4 d16 es f8
 bes,4. c8 d4
 c c d8 es d8. c16 bes4 r4
 r4 bes'2~ bes4 a4. g16 a bes4 

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 3/4
   g8 d4 fis16 g g8. fis32 g
 a8 d,4 g16 a a8. g32 a 
 bes8 d, a' d, g a
 fis4. d8 fis8. es32 fis 
 g8 d4 


}

