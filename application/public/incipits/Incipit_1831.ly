\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Prelude. Lentement"
  \time 2/2
r4 d4 \grace {d16[ e]} fis4. g8
e2 a4. d,16 cis
\grace cis8 d4. e16 fis e4. d8
cis2

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allemande"
  \time 4/4
\partial 8
d8 d r8 \grace {a32[ b cis]} cis8. b32 cis d8 fis e g
fis a4 g16 fis e8 g4 fis16 e d


}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Fantaisie. un peu gay"
  \time 3/4
 r8 d8 fis d fis d
a' a, cis a cis a
d d, fis d fis d
a' b cis d e cis
d a g fis g a
fis a d fis, e cis' d,

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Sarabande"
  \time 3/4
d4 \grace {e32[ d cis d]} e4. fis8
cis4. b8 a4
a'8 g16 fis e8 fis16 g fis8. e16 
e2

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Gigue"
  \time 3/8
\partial 8
d8 a'4 g8
fis e fis 
\grace fis8 g4 fis8 e4 d8 cis4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "6. Rondeau"
  \time 2/2
\partial 2
d4 a'
\grace g16 fis4 \grace e16 d4 e \grace d16 cis4
d a d e8 fis 
g fis e g fis4 e8 d
cis4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "7. Menuet"
  \time 3/4
d4 a d
e8 d e fis e4
fis8 g a g fis e
d cis d e d4 


}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    " Sonate 1ere " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Lentement"
  \time 4/4
\partial 16
d16 d4 r16 a'16 g bes a8 e g32 f16. e32 d16.
c8 e16 f g4~ g8 f16 e e8. d16
d4

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Gayement"
  \time 4/4
r8 d8 f d e16 d e f e f g e
f4 d2 cis4 \grace {b16[ cis]}
d8 

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Courante"
  \time 3/4
\partial 16
d16 d4. a'8 f d
a'4 a,8 b cis d
cis d e a, e g
f e d e f g
a d f e d c 
b d cis e d g, f d' e,4 cis'

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Sarabande"
  \time 3/4
d4 a d
e4. d8 e4 f g4. a8
f2

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "5. Gigue. Gayement"
  \time 6/8
d8 a f d f a
d f a f4 e8 
d e f e f d cis4.
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonate II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Gravement"
  \time 4/4
d8.[ e16 d8. c16 b8. a16 b8 g]
 fis4. d8 g4 \grace {g32[ fis g]} g8. fis32 g
a8 d, c'8. c16 c8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Courante. Gayement"
  \time 3/4
\partial 16
g16 g4 a8 b c a
b a g a b g
c b a b c a
b a g a b c
d c d e fis d

}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allemande. Gravement"
  \time 4/4
\partial 16
g16 g4 r16 g'16 fis e d8. c16 b8. a16
g8.[ fis16 g a b c] d8 e16 d c8. b16 a4

}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Gigue. Gayement "
  \time 3/8
  \partial 8
d8 g a fis
g4 d8
b' c a 
b4 a8 
g a fis g fis e 
d e c 
b[ d]

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonate III " 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \key f\major
   \tempo "1. Vivement"
  \time 4/4
r8 f8 a c f4 r4
c8 r8 d8 r8 c8 r8 bes8 r8
a4 f r8 f'8 a c 
bes g e bes' a c, f a
g e c g'


}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Affectueusement"
  \time 3/4
a4 \grace bes8 bes4. a16 bes
c2 c4 
d e f 
e4. d8 c4
f c es 
\grace es16 d2 c4

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allemande. Gayement et moderement"
  \time 4/4
\partial 16
f16 f4~ f16 a g f e8 c \times 2/3 {d16[ e f]} \times 2/3 {e16[ f g]}
f8 c16 bes a8 g16 f e f g a bes8. c16
a8

}

\relative c' {
  \clef treble
  \key f\major
   \tempo "4. Gigue"
  \time 6/8
\partial 8
f8 a4 f8 c'4 c8
c4.~ c4 c8
bes a g a bes c
bes a g f4

}


