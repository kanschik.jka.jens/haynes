\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. ohne SB"
  \time 2/2
  d4. a8 f'4 d
  cis4. r8 r4 r8 d
  a'8. a16 a8. c,16 bes8. a16 bes8. g16
  g'8. g16 g8. c,16
}
      
