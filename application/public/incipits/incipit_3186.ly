\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Mein Heiland lässt sich merken [BWV 186/5]" 
    }
    \vspace #1.5


}



\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe da Caccia"
  \key d\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
   r4 r8 d8 e f g a
   f e16 d a'4~ a16 gis a b e,8 c'
   \grace b8 a4 r16


}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key d\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    R1*5 r4 r8 d16 e32 f e8 g, g f16 e
    f g g f32 g a8 
   
    
    
    }