\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Endlich, endlich wird mein Joch [BWV 56/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key bes\major
   \tempo "o. Bez."
   \time 4/4
   bes8 a16 g f4 c'16 bes a g f8 c'
   d16 c es d c bes a bes c2

  
   
  
  
   
}

\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key bes\major
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
   R1*10  bes8 a16 g f4 c'16 bes a g f8 c
   d8 f16 bes es8 d c2~ c8
   
}



