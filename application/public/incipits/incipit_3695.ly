 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro moderato"
  \time 4/4
  \partial 4
  d8 b
  g4 g g4. b8
  d4. e8 d4 r
   }
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 2/4
     \partial 8
       \override TupletBracket #'stencil = ##f
    g16 a
    \times 2/3 { b16 d g} d4 g16 d
    c8. b'32 g \grace d8 c4
}

 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 2/4
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  d4. \times 2/3 { e32 d c} 
  b8 d g e
  d4. e16 d32 c
  b4 r
}
