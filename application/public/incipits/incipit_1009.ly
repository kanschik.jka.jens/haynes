\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro ma non tanto"
  \time 2/2
  \partial 4
  fis8 g
  b,2 b8 c c d
  d e b c c4 gis'8 a
  a,2 a8 b b c
} 


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 3/8
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

  \partial 8
  g32 c e g
  g16. \times 2/3 { a32 g f} e8[ \grace g8 f16 d]
  c8 \grace c8 b16. a64 b c32[ b c e]
} 


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Rondeo"
  \time 3/4
    b'8 c a2
    a8 b g2
    e4 e \grace g8 fis8. e32 fis
    g8 d e fis g a
} 