\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato"
  \time 2/2
  f2 g4 f8 es
  f d bes4 r2
  r4 bes'2 a4
  bes f r2
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Menuetto"
  \time 3/4
  f2 bes4
  d,2 f4
  bes,2 c4
  bes2 r4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 2/4	
  \partial 8
  r8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Menuetto"
  \time 3/4
  bes2. c
  d4 g f es2 d4
  e2. f d4 c4. bes8
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "5. Finale. Allegro"
  \time 2/4
  d4. es8 c4. d8
  bes4. c8
  \grace bes8 a4 r
  bes a c bes es d
}