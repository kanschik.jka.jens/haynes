
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "1e. Fantaisie"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key c\major
   \tempo "1. Legerement"
  \time 2/4
\partial 8
g8 c c c e16 f
g8 f16 e d e f d
e f e d  c8 c
c d16 c b c d b
c8 g'16 f g8 g
 
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement"
  \time 3/4
e8 d c4 e
g2 a4
g f8 e d f
e4 g, g
c e8 d c d 
e4 c

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Gaiment"
  \time 3/8
g8 e16 f g8
a g g 
a g g
b16 a f e d c
d4.
 
}
 

 

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "II. Fantaisie "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Fierment"
  \time 3/4
r8 g8 c d e f
g4 a b
c e,8 d c d
e4 f8 e d f
e d e f g a 
d, g g a f g
e g16 f e8


 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement"
  \time 6/8
\partial 8
g8 c c c c4 d8
e e e e4 f8
g f e d4 c8
d d d d4


}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Vivement"
  \time 2/4
g4 c
e,4. f8
g[ c c, a']
g[ c c, a']
 g f16 e d8 c
b[ a b c] 
d[ c f e] 
d2
 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "III. Fantaisie"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gayment"
  \time 2/4
c8 g g c16 d
e8 d4 e16 f
g8 f16 e d e f d
e8 d16 e c8 d 
e[ g e c]
d[ g d b]


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gavotte. Gracieusement"
  \time 2/2
\partial 2
c8 e d c
b c d b g g' f g
e4 d e f
g c,8 d b4 d
d2


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gracieusement sans lenteur"
  \time 9/8
\partial 8
e8 c d e c d e f4 f8
e d e c d e f4 f8
e d e c d e d e c 
d4 g,8 g4


}




\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IV. Fantaisie"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Legerement"
  \time 2/4
r8 c8 g' g 
g4 a
g8 f16 e d e f d 
e8 d16 e c8 g'
c[ b a g] 
fis g16 a b8 a 
g b,16 c d8 d g,4



}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "2. Gavotte. Tendrement"
  \time 2/2
\partial 2
c4 b
c g f es d es f g
es4 c
as' g
f4. es8 d es d c
b4 g




}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Legerement"
  \time 2/2
\partial 2
g4 c,
c d8 e f4 f
e d e f
g c, b c
d e8 f g4 c,
 

}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "V. Fantaisie "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Legerement"
  \time 2/4
d8 e16 fis g8 g
fis4. e8
d c16 b c8 d
b g r8 a8
b16 c d8 a d16 c
b c d8 a d16 c
b8

 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Gracieusement"
  \time 3/4
\partial 4
g4 b4. a8 g fis
g4 d8 e d e 
b4. a8 b c
e4 g,

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gayment"
  \time 6/8
\partial 8
d8 g fis e d4 c8
b4 a8 g4 d'8
g fis e d4 c8
b4 a8 g4 d'8
d b' d, d b' d,
d c d b c d c

 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VI. Fantaisie"
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Legerement "
  \time 2/2
\partial 2
g4 g 
g g g g 
g fis8 e d c b a
b g b d g fis g a
g4 fis8 e d c b a 
b4 d

 
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Tendrement"
  \time 2/2
\partial 2
d4 c8 b
a4. b8 a b  c d
b4 g g' g
g2 fis
g4 d8 f e d c e
d c b d c b a c
b4


}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gaiment"
  \time 3/4
\partial 8.
d16 e fis
g4 fis8 e d c
b a g d' e fis
g4 fis8 e d c
b a g4 d'
e8 d e fis g a 
fis4 d

 
}

