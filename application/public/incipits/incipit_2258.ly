 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
  es4. bes'16 g es4 es
  es2 es8 f g as
  g f f2 as8 d, es2
}
 
