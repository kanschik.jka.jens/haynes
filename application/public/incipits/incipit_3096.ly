\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): O du von Gott erhöhte Kreatur [BWV 121/2]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key b\minor
   \tempo "o. Bez."
   \time 3/4
   fis8 b,4 ais16 b cis b ais gis
   fis8 cis' d cis16 d e d cis b
   ais4.
   
   
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key b\minor
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
    R2.*12 fis8 b,4 ais16 b cis b ais gis
    fis8 cis' d cis16 d e d cis b
    ais4 r4
  
   
 
   
   
}



