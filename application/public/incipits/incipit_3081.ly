\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Ach, unaussprechlich ist die Not [BWV 116/2]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key fis\minor
   \tempo "o. Bez."
   \time 3/4
   fis8 gis16 a bis,8 dis fis a
   gis e cis4 r4
   
   
   
  
   
   
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key fis\minor
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
   R2.*12 fis8 gis16 a r4 r4 R2. gis8 a16 b r4 r4 
}



