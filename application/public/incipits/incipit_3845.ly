\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Largo"
  \time 4/4
  d8 f16 d e8 a16 g f8 d4 cis8
  d f16 d e8 a16 g f8 f4 e8
  f
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro"
  \time 2/2
  d4 r8 f16 d e4 r8 a16 e
  f8 d
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Largo"
  \time 3/4
  f4 a c
  g c,8 e g c
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro"
  \time 6/8
  f4 a8 f4 d8
  cis4. r4 a8
}


