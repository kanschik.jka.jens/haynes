\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 2/2
      c'4 r r2
      g4 r r2
      c, e d g
      c,4 c c r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro assai"
  \time 6/8
     e4. c4 g'8
     e4. c4 g'8
     f4. d4 g8
     b,4 d8 g4 r8
}
