\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 4/4
    d4 a16 g fis e fis8 d fis a
    d32 e fis8. e32 fis g8. fis32 g a8. r a,
    d8. cis32 b a8 g fis d r
}
