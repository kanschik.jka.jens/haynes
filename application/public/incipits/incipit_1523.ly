 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe solo"
  \key g\major
   \tempo "Andante"
  \time 6/8
  g'8 c,16 b a g d'4.~
  d16. e32 d16. e32 d16. e32 d4.~
  d16 g c, b a g f'4.~
  f16 a f d b g
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key g\major
   \tempo ""
  \time 6/8
     \set Score.skipBars = ##t
     R2.*6
       g'8 c,16 b a g d'4.~
       d16 g c, b a g
       e'4 fis8
       g4 a8
       fis8. e16 d8
     

}