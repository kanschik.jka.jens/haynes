 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro moderato"
  \time 2/2
  \partial 8
  bes8 d es es2 g4
  \grace as8 g8 f f4 r bes,
  \grace e8 f4 f2 as4
  \grace bes8 as8 g g4 r
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Andante"
  \time 2/2
  d2 d8 c c bes
  es2. d4
  \grace d8 c4 bes d8 c bes a
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Presto"
  \time 3/8
  bes8. c16 bes8
  bes bes bes
  bes f' d
  bes4.
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}