 
\version "2.16.1"


#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key f\major
                      \tempo "Vivace"
                      \time 3/4
                      R2. r4 a c
                      a f r
                      R2.
                      r4
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key f\major
                        % Voice 2
                       f'8 f16 f f8[ c a c]
                       f,4 r r
                       f'8 f16 f f8[ c a c]
                       f,16 f' e d  c d c bes  a bes a g
                       f4
                  }
>>
}

