 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "Tendrement"
  \time 2/2
  \partial 2
    a8 c b d
    c4 \grace b16 a4 c8 e d f
    e4 d c b
    \grace d16 c4 \grace b16 a4
}
