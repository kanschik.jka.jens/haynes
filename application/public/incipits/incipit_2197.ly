 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Tutti]"
  \time 2/2
  \partial 8
  c8
  f f f f   f16 g f e f8 c'
  f, f f f f16 g f e f8 c
  d e16 f bes,4 a8[ f]
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Solo]"
  \time 2/2
  \set Score.skipBars = ##t
   R1*11 r2 r4 r8 c
   f f f f  f16 g f e f8[ d]
   c f bes, a16 g a8 f r g
   a16[ g f a] g c bes c  a g f a g[ c bes c]
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Adagio"
  \time 4/4
  f8 d r d a'4. a8
  f d r d a'4. a8 f d r d bes'4 bes8 g e c r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 3/4
  f4 g e
  f4. e16 d c8 bes
  a4 bes g a8. g16 f4 r
}
 
