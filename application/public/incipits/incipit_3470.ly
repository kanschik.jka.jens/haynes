\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.bez."
  \time 3/4
  \partial 4
  bes4
  d8 es16 d d4. c8
  \grace c8 bes2 r4
  es4. d8 c bes
  \grace bes8 c4. a8 bes4~
  bes8 c \grace d8 c2
  bes
}
