\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/4
    \partial 8
    d8
    g4 d
    bes8. a16 g a bes c
    d8 es es e16 fis
    g fis g a bes8 a16 g
    a8 d,
}
