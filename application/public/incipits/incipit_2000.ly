\version "2.16.1"
         #(set-global-staff-size 14)

         
         \relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
    bes'4. a16 bes f4 f
    g8 f f2.
    bes,4. d8 c es d g
    f4. es8 d2
}
         
         \relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio"
  \time 2/4
      es16 d32 es  bes16[ bes] bes c c d
      es16 d32 es bes16[ bes] bes c c d
      es8 f16 g
}
         
         \relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro assai"
  \time 3/8
    bes4. f'4 es8
    d16 bes c8 a
    bes bes,16[ c32 d] es32 f g a
    bes4.
}