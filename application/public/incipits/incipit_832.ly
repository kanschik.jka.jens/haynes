 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 4/4
  e4~ e16 d c b c8 b r16 b e, b'
  c8 b r16 b e, b' c b a g fis8 b
  g e
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 2/4
  e8 b4 dis8
  e b4 dis8
  e fis g fis16 e
  dis8 e16 fis
  b,8 a g4 fis8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Affetuoso"
  \time 6/8
  d16 c b c d8 d e r
  d c b b a r
  b16 c d e fis g   cis, d e fis g a
  fis e g fis e d
   }

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 6/8
  e4 e,8 b'4 e8
  e d c c b a
  d4 d,8 a'4 d8 
  d c b b a g
  }
