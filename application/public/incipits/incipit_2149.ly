\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Affettuoso"
  \time 4/4
  a'4. b8 cis, d r fis
  e fis g a16 g fis8 d r fis
  e e a a, d4 r8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 2/2
  r8 a' d, cis d e16 fis e8 fis16 g fis8 a d, cis d
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Sarabande"
  \time 3/4
  e4 b4. b'8
  g4. e8 fis4 e e8 g fis e d4. c8 b4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Tempo di Gavotta"
  \time 2/2
  \partial 2
  d4 d
  d8 e fis g e fis g a fis4 e8 d a'4 a,
}

