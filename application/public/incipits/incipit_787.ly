\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  c8. d16 e8. f16 d4 g8 e
  \grace d8 c8 f d g e \grace d8 c8 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. 1er Aria"
  \time 6/8
 \partial 2
   g8 c4 d8 e d e c4 d8
   e d e c4

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. 2er Aria"
  \time 6/8
  \partial 2
  c8 g'4 as8
  g4 c,8 g' as b
  c4 c,8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Giga. Allegro"
  \time 6/8
  c8 e c g' b g
  c4 c,8 g'8 b g
  c b a g a f
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
 \partial 8
  e16 f
  g8 c, c c
  a'16 b c8 f, f f e r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. 1er Gavotte"
  \time 2/2
   \partial 2
   c'4 b
   c g8 a g f e d e4 d
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. 2e Gavotta"
  \time 2/2
  \partial 2
  g8 c b c d4 g, g8 d' c d
  e4 \grace d8 c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Minuetto"
  \time 3/8
  g16 c e d g, d'
  f e g, e' g f
  a g f e d c
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Moderato"
  \time 3/8
  c16 d d8. c32 d
  e16 f e d c8
  g'8 a4
  g16 f e d c8
  
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gracioso"
  \time 2/2
 \partial 2
  g4. c8
  b4. c8 d4 e8 f
  e4 d
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Gratioso"
  \time 2/2
  \partial 2
  g'4. c,8
  as' g f e f4. bes,8
  g' f es d es4.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Giga. Allegro"
  \time 6/8
   \partial 8
   g'8
   c c, d e f g c, d e f g a
   g4 f8 e4 d8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  d8. d16 e8 fis g8. fis16 g8 a
  b c16 b a b g a
  fis8 \grace e8 d8 g d
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Gavotta"
  \time 2/2
 \partial 2
  g8 a b c d4 g, e'2 \grace { fis16[ g] } d2
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Sarabande"
  \time 3/4
  g4 d'4. es8
  d4 g, c bes4. a8 g a
  a2 a4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 6/8
 \partial 2
  d8 b a g
  d'4 d8 e fis g
  fis4 d8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Andante"
  \time 4/4
  g8 d'~ d16 es c d es8 c~ c16 d bes c
  d8 bes~ bes16 c a bes c8 a~ a16 bes g a bes8[ g]
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 3/4
  g8 b d fis g b
  a fis d g a c
  b g d a' b d a4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gratioso"
  \time 2/2
 \partial 2
  b4 a8 g
  d'4. e8 d c b a
  c16 b8. a16 g8. b4 a8 g
  d'4. e8 d c b a g2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Vivace"
  \time 6/8
 \partial 8
  d8 g4 d8 g8. fis16 g8
  a4 d,8 a'8. g16 a8
  b8. c16 b8
  a4 g8
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  g'8 a16 b a8. g16 fis8. e16 d8 g16 fis 
  e d c b c8. d16 b8. a16 g8 d'
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Gratioso"
  \time 3/8
  b16 c d8 g, e'4 d8 e16 d e fis g a
  fis8. d16 d8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Gratioso"
  \time 3/8
  g16 a bes8 g
  a16 bes c8 a bes16 c d e fis g
  d4.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Presto"
  \time 6/8
 \partial 8
d8
g d g b g b d4.~ d4
}

