\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Mein Gott, ich liebe dich von Herzen [BWV 77/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key a\minor
                      \tempo "o. Bez."
                      \time 4/4
                      e8 d e f e2~ 
                      e8 d16 e f e f e d2~ d8
                      c16 d e d e d
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key a\minor
                     c8 b c e c2~
                     ~c8 b16 c d c d c b2~
                     b8 a16 b c b c b
                        % Voice 2
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key a\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
     R1*7 r2 r4 r8 e8
     c b16 c d c d b c4 r8

}