\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 2/2
  \partial 4
  g'4
  c a e fis
  g2 g8 f d b c4.
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 2/2
  \partial 4
  c4 f2. e8 f e d d4 r bes
  \grace bes8 bes'2 a8 g f e
  g4 f r 
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo"
  \time 2/4
  \partial 4
  e16 f d e
  c8 c d16 f d b
  c8 c r4
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo ""
  \time 2/2
  \partial 4.
  c8 d e
  f g a bes   c c c c
  c bes a bes g4. c8
  bes a g a f4. a8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Menuetto"
  \time 3/4
  \partial 4
    c8 f
    a2 bes4
    c8 c16 bes a8 g f e
    d c bes a g f
    e4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo"
  \time 2/4
  c'8[ c c] r
  a[ a a] r
  \grace a8 g[ f g e]
  f[ f f] r
}