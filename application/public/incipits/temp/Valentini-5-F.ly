 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 3/4
  d4 g, g'16 a bes8
  fis4 g2~
  g4 fis2
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro"
  \time 4/4
  g2 d'4. r16 d
  g8. bes16 a8. g16 fis4. r16
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Adagio"
  \time 3/4
  d4 g,4. g'8
  fis4 g4. d8
  es c a c f es
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Allegro"
  \time 6/8
  g4. d'
  bes8 c d g,4 g'8
  bes,4 g'8  a,4 fis'8
}