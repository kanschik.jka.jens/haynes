 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 3/2
  f1 g4 f
  f1 f2
  g4 f es2. d4
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegro"
  \time 3/8
  bes4. f'
  g8 f es
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Adagio"
  \time 3/2
  f2 bes, bes8 c d16[ es f g]
  a,1 f'16 es d c bes8[ a16 g]
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Allegro"
  \time 6/8
  bes4 bes8 d4 d8
  f4.~ f4 f8
  g4 g8 g f es
}