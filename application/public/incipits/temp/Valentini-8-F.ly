 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Graue"
  \time 4/4
  d4 g32 fis e d c b a g d'4 g32 fis e d c b a g
  e'4. d32 e c16 d8 g, c4
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 2/2
  g2 d'
  b4 a8 g a4 d
  b a8 g a4 d
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Adagio"
  \time 3/2
  d2 cis1 d2~ d8 e b4 cis2 d4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Allegro"
  \time 3/8
  b16 c d8 e
  d16 c b a g8 b16 c d8 e
}