 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 3/2
  c2. d4 c d
  c2 f, c4. bes16 c
  d2 g, a4 bes
  a2. g4 f2
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 4/4
  f4 g a e
  f8 c f4~ f8 d g f
  e c r f d16 c d e  f e f g
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Adagio"
  \time 3/2
  f2 e d
  a'1 e2
  f e d cis1.
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allegro"
  \time 6/8
  f4. c
  a8 g f  g f e
  f4. r8 r g
  a g f  c' d c
  c4. r8 r8
}