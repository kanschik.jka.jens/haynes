
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata in B "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key bes\major
  \tempo "1. Adagio"
  \time 3/8
 bes8 a as \grace as16 g8 f r16 d16
 \times 2/3 {es16 f g} c,8 [bes] a bes r8
  
 
 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 4/4
  f2 es16 d8. c16 bes8.
 f4 f2 bes16 d8.
 c16 a8. bes16 d8. c16 a8. bes16 f'8.
 es4 d r4
 
 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/8
  f4 \times 2/3 {bes16 a g}
 \grace f8 es4 d8
 c8~ \times 2/3 {c16 [d es]} \times 2/3 {es16 f g}
 a,8 bes r8
 
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata in C"
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
 g4. f32 e d c d8. e32 f e4
 c a'8 \times 2/3 {g16 c a} \grace g8 f4 e
 a,4. 
 
 
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4
  c2 g'4. f8 
 \grace f4 e2. d4
 c2 a'4 g
 \grace g4 f2 e4 r4
  
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Minuetto con Variatione"
  \time 3/4
  c4 g' g8. a16 
 g4 d g 
 r4 g4 g
 c8 a e f f4
  
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata in F "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 3/8
 c8 f a 
 g8. [a32 bes] a8
 g bes d
 \grace f,8 e8 f r8
 
 
 
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 4/4
  r4 f4 a, bes
 c c2 d4
 e, f g4. a16 bes
 a8 bes c2
  
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Tempo di Minuetto "
  \time 3/8
  f8 a bes
 c4 d8
 d c16 bes a g
 a bes c8 d
 d c16 bes a bes 
 a8 
  
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata in G "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 3/4
 r8 d8 b g fis g
 r8 g8 [fis g g'8. g,16]
 a e' c a \grace g8 fis8. a16 d fis a c
 \grace c8 b8. a16 g4
 
 
 
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
 g8 d d4 e16 g, fis a c4 b16 a
 b g g8 e'4 d16 g, g8 a16 fis d c' b g g8
  
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
  g8 a a2 
 b8 g' d4. c8
 b d4 g,8 g16 fis a c
 b d g b d,4.
  
 
}