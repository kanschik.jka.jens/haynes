 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Graue"
  \time 4/4
    c4. f32 g f e f4. c8
    d c32 f e32. f64  bes,8. a16 a4.
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 3/8
  \partial 4
  c8 c
  f16 e f g f g
  e d e f e f
  d c d e d e
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Adagio"
  \time 3/2
  r2 f,4 g g4. f16 g
  a2. bes4 c2
  f, f'2. g4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allegro"
  \time 3/8
    f,8 a c
    f,16 g a bes c8
    f, d' c
}