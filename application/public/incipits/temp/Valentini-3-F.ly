 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  d4. e16 d c8 b r e16 d
  c8 b r d
  e32[ d c b] a c b c d[ c b a] g b a b
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 2/4
  d4 e
  d8. c16 b8. c16
  d4 e
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Adagio"
  \time 3/4
  d4 e8 d e d
  c2 b4
  d e8 d c32 b a16 g16. b32
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Allegro"
  \time 2/4
    g4 b8. c16
    d4. r16 e
    d8. c16 b8. c16
}