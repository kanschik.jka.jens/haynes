 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  g'4. g64 a g f e d128 c b8 c r
  g'64 a g f e d128 c
  f16 g e32 f g32. a64 d,8. c16 c4 r
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 3/4
  \partial 4
  r8 c
  c4. g8 a8. b16
  c8. b16 c8. d16 e8. f16
  g4 a8. g16 f8. e16
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Adagio"
  \time 3/4
  c4. g8 c16 d c b
  c4. g8 c16 d c b
  c f e d d4. c8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Allegro"
  \time 2/4
  c4 g'
  e8 d16 e c8 g
  c4 g'
}