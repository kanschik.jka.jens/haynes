 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 4/4
  \partial 16
  d16
  d8. e16 e8. d32 e f4. r16 a,
  d e f32 g a bes
  e,8. d16 cis4. r16
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  a'8
  a g16 f e8 d
  cis4 r8 a'
  a g16 f e8 d
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio"
  \time 3/4
  d8 e f4. g8
  e4 a, d8 cis d16 e f8 e4. d8
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "4. Gauotta. Allegro"
  \time 2/2
  d4 a d4. e8
  f16 g a8 g8. f16 e4 a,
  d c8 bes a8. g16 f8. g16
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "5. Minuet"
  \time 3/4
  d8 e f4 g
  e a, a a' g8 f e d
}