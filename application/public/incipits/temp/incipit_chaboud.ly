\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4	
  g'4~ g16 b a g  fis e d8 r16 d e fis
  g4~ g16 e a, g' g8 fis r16 d c d
  e4. fis16 e d8 d g4~
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4	
  r8 d b g a d, d' a b d g2 fis4
  g8 b, e c a d16 c b8 a16 g
  a4
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
  \time 3/2	
  r2 b e
  dis1 b2
  e4 d c b a g
  fis1 e2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga"
  \time 6/8	
  g'4. d
  e8 b c  d a c
  b g b a d, fis
  g4 g'8 fis4 e8
  d4 d8 c4 b8
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Largo"
   \time 12/8
   b8. c16 b8  e4 fis8 dis4. r8 r fis
   g8. fis16 e8 d8. e16 c8 b8. c16 a8 b8. g'16 fis8
   g4
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
   \time 2/4
   r8 e fis dis
   e4 fis
   g8 fis16 g e8 g fis g fis e
   dis16 e dis cis b cis b a
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
   \time 3/4
   g8. a16 b8. c16 d8. e16
   fis,2 e'4
   d c4. d8
   b8. a16 b8. a16 g8. a16
   fis2 r4
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
   \time 2/4
   b4 e8. fis16
   dis4. fis8
   g c, b a 
   g e16 fis g a b c
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
   \time 6/8
   d8. e16 d8 g b, c
   d8. e16 d8 g b, c
   d8 g fis e8. fis16 d8
   c8 b8. c16
   a4 d8
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
   \time 2/4
   g4~ g8 b16 c
   d c d e  d8 d
   e d4 g8
   c,8 b4 g'8
   a, g4 a8
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
   \time 3/4
   d4 g,4. d'8
   e4 d c b4. d8 c b
   a4 d,2
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga"
   \time 12/8
   \partial 8
   d8
   g a g  fis e fis g4.~ g8 fis e
   d e d  c b c b4. r8
}
