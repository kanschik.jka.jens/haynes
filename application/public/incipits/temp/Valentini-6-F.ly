 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  c4. e32 f e d c4. c8
  d16 e e16. d32 e  f g a16 g16. f32 e16 d c8 r
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 2/2
  c2 g c2. d4
  e f g f8 e
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Adagio"
  \time 3/4
  c4 g c8 d
  e16 f e d c4 e16 f f e32. f64
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Giga. Allegro"
  \time 6/8
  c4. b
  c~ c4 d8
  e d c  f e d
}