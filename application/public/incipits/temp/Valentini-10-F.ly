 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Viuace"
  \time 4/4
  r4 a bes c
  f, f'2 e4
  f a g c,
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Adagio"
  \time 3/2
  \partial 2
  c2
  a1 f'8[ e d16 c b c]
  b2 a4 g c2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 4/4
  c4. f8 f16 e f g f g f g
  e4. e8 d16 c d e  d e d e
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Adagio"
  \time 3/4
  c4 f, 
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "5. Minuet"
  \time 3/4
  d8 e f4 g
  e a, a a' g8 f e d
}