 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  r8 f, c' c a f r f'
  d bes g g'16 f e8 c f4~
  f16 g f a g4~ g16 a g bes a4
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 4/4
  r8 f8 f c g'4. f16 e
  a8 a, f'4. g16 f e f d e
  f4 c4. b16 a b8 g
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Adagio"
  \time 3/2
      << {
      % Voice "1"
      a1.~ a~ a2 f bes~
      bes c4 bes a2~ a4 bes g2. a4
         } \\ {
      % Voice "2"
      f1 e2
      f2. e8 d cis2
      d1 f2
      e1 f4 e d2 e2. e4
      } >>
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allegro"
  \time 6/8
  f8 c f  f a16 g f8
  g c, g'  g bes16 a g8
  a16 g f g a bes  a g a f g a
  e4 e8
}