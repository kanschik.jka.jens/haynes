 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 3/4
  e4 a, f'16 g f e
  d2 e4
  d16 c b32 d c b b4. a8
}


\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "2. Allegro"
  \time 4/4
  e4. e8 e16 d e f e g f e
  d c d e d f e d c b c d c e d c
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "3. Adagio"
  \time 3/4
  \partial 4
  e,4
  b'4. c8 b16 c b a
  g a g f e4 b'
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "4. Allegro"
  \time 6/8
  e4. r8 r e
  e d e  e d e  
  c b a r r e
}