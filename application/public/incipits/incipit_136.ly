 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
  g'4 r8 g16 g a4 r8 a16 a
  bes8 bes bes bes a4 r8 a16 a
  bes8 a bes f g f g d
  es[ f16 g] f8 es16 d d8[ bes]
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 3/2
  bes1.~ bes~ bes2 bes2. as4
  g2 es
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 2/2
  g4 d' g r8 g16 a
  bes8 a bes d, a' g a d, g fis g a fis4
}

