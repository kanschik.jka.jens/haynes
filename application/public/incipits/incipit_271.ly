\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Concert" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude. Gravement"
  \time 4/4
  r4 r8 c g' d16 e f8. g16
  e8 g d e b b16 c d8 b
  c
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deusieme Concert" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Moderemt"
  \time 3/8
  \partial 8
  c8
  e16 d c d e f
  g4 g,16 a
  b c d e f g
  e8[ \grace d8 c]
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisieme Concert" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gracieusement"
  \time 3/8
  e8 \grace e8 f4
  d8. c16 d e
  f8 f f
  f e16 d e f
  g4
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatrieme Concert" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Legeremt sans vitesse"
  \time 3/8
  \partial 8
  c8 g' d16 e f8
  e16 d e f g8
  c, f16 g e f
  d4
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquieme Concert" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Legeremt sans vitesse"
  \time 3/4
  \partial 2
  d4 d
  g fis8 e d c
  b4. c8 b c
  d4 g8 fis g a
  b4 \grace a8 g4
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixieme Concert" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Gracieusement"
  \time 2/2
  \partial 4
  g'4
  d e8 d c4 b8 c
  b4 g' fis d
  g b a b8 c
  b4 \grace a8 g4
}

