\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\minor
   \tempo ""
  \time 2/4
    r8 es d f r es d f
    r d c es r d c es
    r c bes des r c bes des
}
