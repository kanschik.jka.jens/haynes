 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro con Spirito"
  \time 4/4
  \partial 8
    g8
    c4. e8 g4. c8
    g g g g d4 r
    
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Adagio"
  \time 2/4
  
  
   \partial 4
  
    f,8 c
    a'8. c16  bes g c e,
    f8 c a' f
    c'8. f16 c a d bes
    \grace a8 g4
}

\relative c'' {
<<
\new Staff { \clef "treble"   
             \set Staff.instrumentName = #"Cor anglais"
                     \key c\major
                      \tempo "Rondo. Presto"
                      \time 6/8
                      g8 r r c r r
                      e r r c r r
                      g2.~ g4 r8 r4
                      % Voice 1
                  }
\new Staff { \clef "treble" 
             \set Staff.instrumentName = #"Violin"
                     \key c\major
                        % Voice 2
                       r8 e g g e' c
                       r g, e' e e' c
                       d r r c r g16 e
                       d8 b b b4
                  }
>>
}
