\version "2.16.1"
    #(set-global-staff-size 14)

\relative c'' {
  \clef treble  
  \key d\major
    \time 2/4
    a,8. d16 fis8 fis16 d |
    e8 e16 a,g'8 [r16 e]|
    fis8 a b [r16 e]|
    d8 c16 b a8 
}