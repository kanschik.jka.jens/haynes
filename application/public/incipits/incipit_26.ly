\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1.  [Tutti]"
  \time 2/4
  \partial 8
  d8
  d e16 fis g8 d
  d e16 fis g8 d
  c a b g a d,4 d'8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 2/4
  \partial 8
  r8
   \set Score.skipBars = ##t
   R1*19 
  r4 r8 d8
  d e16 fis g8 d
  d e16 fis g8 d
  c16 b c a b a b g a8 d,4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 3/4
  g4 b d
  \grace c8 b4 \grace a4 g4 e'
  d g,8 b a g
  fis4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro [Tutti]"
  \time 2/4
  g'8 d d4 e8 d d4
  g8 d d4 e8 d d4
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*11
   d4 r8 b16 c d8 d e d16 c
   d4 r8 b16 c
   d8 c c b
   b a a b16 c
}