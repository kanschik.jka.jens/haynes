\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante molto [Tutti]"
  \time 2/4
  g4 g'
  fis16 a d,4 c8
  b16 d fis,8~ fis16 a g8~
  g16 b a8~ a16 c b8~
  b16 fis' a4 c,8
  b16 a g8 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante molto [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*23 
    g'8 g, d'4~
    d8 g16 a b8 g
    fis d c4~
    c8 fis16 g a8 fis g d r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Largo"
  \time 3/4
  r4 g e'
  e d2
  d16 e fis g a g fis e d c b a
  d4 fis a
}



\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro molto [Tutti]"
  \time 12/8
    g'8 g, g g'g, g  g4. e'
    d8 g, g g' g, g g4. e'
    d8 g b d, g b  c, fis a  c, fis a
    b, d g  b, d g a,4. r4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro molto [Solo]"
  \time 12/8
   \set Score.skipBars = ##t
   R1.*11 
   r4. r4 d8
   g fis g g fis g
   g4.~ g4 b8 a g fis e d c
   c4 b8 r4 e8 d4. c
   b16 a g8 r r4
}