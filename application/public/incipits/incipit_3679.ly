\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  g8
  c4. d8 e4. f8
  e d d c
}
  
