 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 4.
  r16 d,[ g8. b16]
  b16 g'8. fis16 a,8.
}
