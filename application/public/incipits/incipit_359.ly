\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
      \time 4/4	
    es,8. d32 es  es8. d32 es
    g16 es d es bes' es, d es
        es8. d32 es  es8. d32 es
        bes'16 es, d es  es' es, d es
        g'8 f16 g as8 g
        \grace g8 f4 r
}
