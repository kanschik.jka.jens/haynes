\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "Aria"
  \time 12/8
  r4. d8. e16 d8 cis4 fis8 b,8. d16 cis8
  ais4 fis8 b4 a8 g8. fis16 g8 e'4 g8
  g
}
