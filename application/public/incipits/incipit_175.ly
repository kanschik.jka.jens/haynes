\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 3/8
    d16 a a a a b32 cis
    d16 d, d d d d
    a'' fis32 g a16 fis32 g a16 g32 fis
    e8 a,4
}

