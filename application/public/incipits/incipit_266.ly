\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "N. I 'Te feguitai felice'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 2/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

  \partial 8
  d8 d8. \times 2/3 { g32[ fis e]} d8 d
  d8. b16 d8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "N. II 'Per nuovo amor delira'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 2/4
  \partial 8
  g8 c8. b32 c d16 g, b d e8 f r d
  e f16 d c8 d16 b c8 g r
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "N. III 'Quando fi trovano'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
  d4 a'16 g fis e d8 a fis d
  d'4 a'16 g fis e d8 a fis d'
  b g' r
  
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "N. IV 'Donne belle che piangete'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 6/8
  a4. b cis8 d e d cis b
  a a' a a gis a
  fis a a a gis a
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "N. V 'Caro Quallieto'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Andantino Grazioso"
  \time 2/4
  cis16 e32 cis a8 r16 cis b a
  e'16. gis32 \grace a16 gis16. fis64 gis a16.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "N. VI 'Prendi l'estremo addio'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Andantino esspressivo"
  \time 3/4
  a8 e cis' a e' cis cis32 b16. d32 b16. \grace a4 gis2
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "N. VII 'Nel duol che prova'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegretto"
  \time 2/4
  \times 2/3 { bes16[ c d] } g,4 g'8
  \times 2/3 { fis16[ e d] } d4.
  
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "N. VIII 'Se mai pretendi d'esser amante'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante"
  \time 3/8
  f8 f8. g16 \grace bes,8 a4 bes8
  f es'32 c16. d32 bes16.
  \grace bes8 a4 bes8
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "N. IX 'Se io ritorno Cittadina'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 2/4
  e8 e4 d8~
  d cis r8 \times 2/3 { fis16[ d b] }
  cis8 \times 2/3 { e16[ cis a] }
  b8 \times 2/3 { d16[ b gis] }
  a4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "N. X 'Nel fen mi giubila'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Brioso"
  \time 2/4
  c4 g'16 e f d
  c8 \grace d16 c32 b c d c16 c c c
  c g c e d b g f
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "N. XI 'Se gl'uomini sospirano'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
  g8 c c~ c16 g c e
  d8 g, r g a c b d
  e8. f16 g e c g
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "N. XII 'Se resto sul lido'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 8
  d,8 
  d1 fis2 a d fis
  a4 fis d a
}

