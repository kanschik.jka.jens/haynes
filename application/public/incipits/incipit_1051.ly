\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro con moto"
  \time 4/4	
    g'2~ g8 a16 g   f e d c g4 r r2
    g'2~ g16 a b c  d f, g f e4 r r2
} 


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 3/4
    g8. b32 g d8. g32 d  b16. d32 g,16. b32
    c8 r16 e32 c a4 r
} 

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio [Oboe]"
  \time 3/4 \set Score.skipBars = ##t
   R2.*4 
    d8.. e32 d8 r r16 b'32 a g fis e d
    d8.. e32 d8 r r16 b'32 a g fis e d
    dis16 e fis g
} 

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro moderato"
  \time 2/4
  g'8. a32 g  e16 e c e
  g8. f16 d8 r16 d
  e8. c16 f8. d16
  \grace d8 b8 a16 g g8 
} 


