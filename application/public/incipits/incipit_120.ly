\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. [Allegro]"
  \time 2/2
     \set Score.skipBars = ##t
     R1*4
     d8 d16 d d8[ d] e[ a,] r e'
     fis16[ g fis e] d e fis g a4 r8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Aria. Largo"
  \time 12/8
  r8 r e e4 e8 e d c b c d
  gis, fis e a b c d4.
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. "
  \time 2/4
  d4 e 
  fis16 e d8 r fis
  e fis e fis
  e16 d e fis e8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Aria"
  \time 2/2
  r4  d8 e fis16[ e fis e] d e fis g
  a8 d, r4 r4 d8 e
  fis16[ e fis e] d8 fis e[ fis,]

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Grave"
  \time 2/2

}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "6. "
  \time 2/2

}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "7. Adagio"
  \time 2/2

}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "8. "
  \time 2/2

}