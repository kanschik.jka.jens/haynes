\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Poco alegro"
  \time 4/4
  g4 e8 f g4 a
  g f16 e f g e8 g f e
  d d e f g16 f g a g8 f
  e e f g a16 g a b a8 g fis4 

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. non troppo alegro"
  \time 3/4
  g4 a8 g a f
  g4. f8 e4
  f d4. e8
  e4 c8 d e f 
  g4 g4. g8 
  f4

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. piu alegro"
  \time 4/4
 g8. a16 g8. f16 e4 g
 c, d e d
 g8. a16 g8. f16 e4. e8
 a8. b16 a8. g16 fis4. fis8
 g4.

}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Canzon"
  \time 4/4
  c4 c8 c d16 c d e f e f d
  e8 c e f g d g4~
  g fis g g16 a g a
  f4 

}
  

\relative c''' {
  \clef treble
  \key c\major
   \tempo "5. Presto"
  \time 3/4
  \partial 4
  g4 a8 g a b a4
  g2 g4
  f8 e f g f4
  e2 e4 d2 d4
  e8 d e f e4 d2 d4



}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4
  r8 c8  as g as4 f~
  ~f8 g es4 d4. c8
  c d es f g a bes c
  d4 c8 bes a d bes a
  bes4 

}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "2. Poco piu alegro"
  \time 3/4
  g8 f g as g as
  f4 f4. g8
  es d es f es f 
  d2 d4
  d2 d4
  d4 c2 c b4 c g r4

}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "3. Canzon"
  \time 4/4
  g4 g8 g c,4 c'
  bes16 as g as bes as bes g as g f g as g as f
  g8 as16 bes c2 b4 c2 r2

}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "4. Adagio"
  \time 4/4
  r4 g4 as bes
  c4. d8 b4. b8
  c bes as g as4. as8
  g f es d es4. es8
  d8 c bes c
  ais4. g8 g4

}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "5. Poco lento"
  \time 3/2
  g2 as2. as4
  as2 g2. g4
  g2 f4 es f2
  g d g
  g f f 
  f es4 f g as
  f es d4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "1. Poco alegro"
  \time 2/2
  r8 g8 g g as16 bes as g f8 bes
  g4 r4 f4 g
  r8 es8 es es f16 g f es d8 g
  es4 r4
  

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Alegro"
  \time 3/4
  r4 es4 es
  d d g
  f f g
  es es f d r4 r4

}
  
  \relative c''' {
  \clef treble
  \key c\minor
   \tempo "3. Adagio"
  \time 2/2
  r4 g4 f2
  f r4 f4 es2 r4 es4
  d2 d
  r4 d4 c2
  r4 c4 bes2 bes es d 
  
  
}
  
  \relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Allemanda "
  \time 2/2
  g4 g c4. c8
  d es f g es c g'4~
  ~g f g4. bes8
  as4. as8 g4. g8
  f2 es

}

  
  \relative c''' {
  \clef treble
  \key c\minor
   \tempo "5. Alegro "
  \time 3/8
  \partial 8
  g8 as g as
  f es f g f g 
  es f g 
  f g es 
  d4 

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Alegro"
  \time 4/4
  r4 r8 f8 g a f g
  e8. f16 e8 a g f e f16 g
  f e f g f8 a g f e f16 g
  f8 d bes'4~ bes8 e,8 a4~
  ~a8 

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Poco alegro"
  \time 3/4
  f8 e f g a bes
  g f e  f g a
  f4 d4. f8
  e4 a,4. cis8
  d4. e16 f e8. d16 cis2

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allemanda"
  \time 2/2
  d4 a'~a8 g f e
  f2 cis
  d4 d'~ d8 c bes a 
  bes a g8. f16 e8 f16 g a8. g16
  f8 g a4~ a8

}

\relative c''' {
  \clef treble
  \key d\minor
   \tempo "4. Alegro"
  \time 3/4
  a4 g4. a8
  f4. e8 d4
  g g4. f8
  e2 d4
  a' d4.~d8
  c4. bes8 f4

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Intrada. Poco adagio"
  \time 2/2
  f4. f8 e f g e
  f4. f8 e2
  a4. a8 g a bes g
  a4. a8 g2

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Affettuoso"
  \time 3/4
  d8 cis d e f g
  e d e f g a
  f e f g a bes
  g f g a bes c
  a g a bes c a
  d c bes a g f

}

\relative c''' {
  \clef treble
  \key d\minor
   \tempo "3. Allemanda"
  \time 2/2
  a4. a8 bes4 a
  g8 a f g e4 a~
  ~
  a8 g g a f4. f8
  g4 a d, g4~ g8

}

\relative c''' {
  \clef treble
  \key d\minor
   \tempo "4. Gicque"
  \time 3/8
  a8 g a
  bes a bes
  g4 g8
  a g a 
  f g a 
  g4 f8
  e4 e8

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. ohne Bez."
  \time 4/4
  r8 a16 g fis8 e fis4 a
  g8. a16 fis8. g16 e2
  r8 e8 fis g a8. b16 g8 fis
  e4. d8 d2

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. ohne Bez."
  \time 3/4
  d2 a'4 
  b8 a b g a b
  a4. g8 fis4
  e2 e4
  fis8 e fis d e fis 
  e4. d8 cis4 b2.

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Adagio"
  \time 4/4
  r4 fis8 gis ais4 cis
  ais4. fis8 fis4 fis
  fis e fis2
  r8 cis8 fis e d d d d 
  e4 fis e a~ a
  

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "4. Allemanda"
  \time 4/4
  a4 b a fis
  g8. fis16 g8. a16 fis8. a16 g8. fis16
  e8. d16 cis8. d16 e4 fis 
  e cis

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Poco alegro"
  \time 3/4
  d8 cis d e fis g
  a4 e a
  g2. fis e
  fis4 a a a2 gis4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "1. Poco alegro"
  \time 4/4
  r8 g8 g8. g16 fis8 fis fis8. fis16
  g8 b16 a g8 fis16 e dis4 e
  fis g fis8 b16 a g8 fis16 e d8 ais b4

}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "2. Poco alegro"
  \time 3/4
  r4 r8 g8 g4
  fis2.
  r4 r8 b8 b4
  a4. a8 d4
  b c a
  b8 c b a g fis
  e fis e d c b
  a2 b4
  
 

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Canzon"
  \time 4/4
  r8 b8 e fis g4 fis8. g16
  e8 a~a g~ g fis16 e fis4
  g8 d e fis g fis e8. d16 
  cis8 d cis4 b

}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "4. Poco alegro"
  \time 6/4
  r4 g8 fis g a b4 a4. b8
  g4 a4. g8 fis e fis g a fis
  g2. fis4 d8 cis d e
  fis4 e4. fis8 d4 e4. d8
  cis8 b cis d e cis d2.


}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key f\major
   \tempo "1. Poco alegro"
  \time 2/2
  r8 c16 bes a8 g a4. c8
  bes16 a bes c a g a bes g4. g8
  a bes8. c16 c, d e d e fis d8. e16
  e8 g16 fis e8 d e4. g8 
  

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Alegro"
  \time 3/4
  r4 r4 c4
  f4. f8 g4
  a f e
  f4. f8 g4
  a4. a8 bes4
  c8 d c bes a bes
  g4 g a 
  bes2 bes4
  
  

}

\relative c''' {
  \clef treble
  \key f\major
   \tempo "3. Presto"
  \time 2/2
  a16 g a bes a g a bes g f g a g f g a 
  bes a bes c bes a bes c
  

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. ohne Bez."
  \time 3/4
  c4 c d 
  c4. bes8 a4
  a' g g 
  a4. bes8 a4
  g g a g4. f8 e4
  e d d
  e4. f8 e4
  

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IX " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "Largo"
  \time 2/2
  bes4. bes8 a4 a
  g4. g8 f4 f
  es4. es8 d4 d
  g4. a8 fis2
  

}


\relative c''' {
  \clef treble
  \key g\minor
   \tempo "Passegaille"
  \time 3/4
  bes4 bes2
  a bes4
  c4. d8 bes4
  a2. 
  d,2 g4
  g4. a8 bes4 
  a g4. a8
  f2.
  

}


\relative c''' {
  \clef treble
  \key g\minor
   \tempo "Largo"
  \time 2/2
  bes4. bes8 a4 a
  g4. g8 f4 f
  es4. es8 d4 d
  g4. a8 fis2
  

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Poco largo"
  \time 2/2
  d2 r8 g8 d g16 fis
  e8 a e a16 g fis2
  g g4 fis
  e2 fis8 a b fis
  g g a e fis4 g
  a fis
   

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Poco piu alegro"
  \time 3/4
  d4 d e
  d4. c8 d4
  b c a 
  b4. c8 b4
  d e fis
  g fis4. g8
  e4 fis g a 

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Adagio"
  \time 4/4
  r4 b4 a g
  fis g fis2
  e r16 e16 fis g a g fis e
  fis2 r16

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "4. Allemanda"
  \time 4/4
  b4 a b4. a8
  g a b c a4 d
  c b a g
  a8 b c d b a g fis
  e fis g a fis e d c   

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. ohne Bez."
  \time 3/4
  d4 c4. d8
  b4. c8 d4
  c b4. c8
  a2. a4 b c
  d2. d4 e fis e fis4. g8
  e4
  
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "6. Adagio"
  \time 4/4
  r8 g8 g g g fis fis fis
  fis e a4 d, g
  g fis e2 fis2 r2
   

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Poco lento"
  \time 4/4
  e4. e8 e4 f
  d4. d8 d4 e
  c4. c8 d4 e
  f8 e d c b4. b8
  c4 d e4. e8 
  d c d e f4 e
  e4. d8 e2
  
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Piu alegro"
  \time 4/4
  r8 e8 f e d e c d
  e8. d16 c8 e d c b4
  a8 a' e f g4 g
  f f e e 
  d d 
  
}
\relative c''' {
  \clef treble
  \key a\minor
   \tempo "3. Poco alegro"
  \time 3/4
  c4 c4. c8
  b4 b b 
  a a4. a8
  g4 g g 
  f4 f4. f8
  e4 e d 
  e2 b4
  
}
\relative c''' {
  \clef treble
  \key a\minor
   \tempo "4. Canzon"
  \time 2/2
  a4 a8 a g4. g8
  f8 f f16 e f g e4 a~
  ~a gis a2
  
  
}
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "5. Alegro"
  \time 6/4
  c4 e2 d4. e8 c4
  d4. e8 c4 b4. c8 a4
  e' g4. g8 g4. a8 f4
  f4. g8 e4 a4. g8 f e 
  d4.g8 e4
  
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Poco adagio"
  \time 4/4
  d4. d8 e8. fis16 e8. d16
  cis4. cis8 d4. e8
  cis4. b8 b2
  fis'4. fis8 g8. a16 g8. fis16
  e4. e8 fis4. g8
  e4. d8 d2
 
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Affettuoso"
  \time 3/4
  fis4 fis4. g8
  e4. e8 fis4
  d a'8 fis e d 
  cis4. b8 b4
  fis'4. g8 a4
  d,2.
  
 
}

\relative c''' {
  \clef treble
  \key b\minor
   \tempo "3. Adagio/piu alegro"
  \time 2/2
  r4 d4. d8 cis4
  r4 b4. b8 a4
  r4 g4. g8 fis4~
  ~fis8 fis e4 e4. d8
  d4 fis4. fis8 e4
  
 
}
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Allemanda"
  \time 4/4
  d4 b fis'4. e16 fis
  d8 b g'2 fis4
  e4. fis8 d4. cis8
  cis cis d e fis g a b
  g fis e d cis
 
  
}
