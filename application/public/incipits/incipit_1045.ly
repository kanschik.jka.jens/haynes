\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  bes2 \grace es8 d4 c8 bes
  f'2. f4
  bes8 bes4 bes bes bes8
  bes4 bes,2 bes4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Larghetto"
  \time 3/4
  c2. c4 bes a
  g bes d
  e, f r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegretto"
  \time 2/2
  bes4 d8. bes16 f'4 f
  f8. d16 bes4 r bes
  c c r8 es c es
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  r8 c c c  r c c c
  r d d d  r d c f
  r bes, bes bes
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Larghetto"
  \time 3/4
  r8 d d d d d
  r es es es es es
  r d d d d d
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro molto"
  \time 4/4
  \partial 4
  r4
  r8 f, f f  r a a a
  r c c c  r c f f f e e4
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 2/2
  f4~ f16 es d c  d4~ d16 c bes a
  bes8. c16 d4 r r8 bes
  es4 es2 es4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro moderato"
  \time 2/2
  bes2 d f2. g8 f
  es2. f8 es
  d8. es16 f4 r bes,8. bes16
}
  
  \relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Poco Allegro"
  \time 3/4
  bes8. c16 d8 d \grace es16 d8 c16 b
  c8. d16 es8 es \grace f16 es8 d16 c
}
  


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro molto"
  \time 4/4
  \partial 8
  r8
  c2 c4. c8
  e8. d16 c4 r r8 c
  <a f'>2 a4 bes c e r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 2/4
  \partial 16
  r16
  c,16 f c a  c f e d
  d c c8 c16 f a c
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegretto"
  \time 3/4
  \partial 4
  e8. f16
  g8 g4 a16 g f e d c
  \grace e8 d4 c8 c d e
  f f4 f8 g a
}

