 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  d,8 a~ a32[ d cis d] fis d a' fis  fis8 e r16 e e fis
  g g,8 g'16~
  g32[ a, cis e] g e b' g g8 fis r16
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Allegro ma moderato"
  \time 4/4
  a'4 g16 fis e d d8 cis~ cis16. e32 d16. fis32
  \grace fis8 e8. fis16 \times 2/3 { g16[ fis e]}
  \times 2/3 { b'16[ a g]} \times 2/3 { fis16[ e d]} d8 r4
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Allegretto"
  \time 6/8
  a8. b16 a8 a d fis
  fis e r r r a,
  e'8. fis16 e8 e fis g
  g fis r
}

