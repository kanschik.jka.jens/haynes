\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Willkommen! will ich sagen [BWV 27/3]" 
    }
    \vspace #1.5


}

\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe da caccia"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key es\major
                      \tempo "o. Bez."
                      \time 4/4
                      \partial 8
                      % Voice 1
                       es8 bes' as16 g as8 bes c bes r8 as8
   g f16 es bes' c des8 des c~ c16 bes as g
   f es d es
                      
                     
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Organo obl."
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key es\major
                     \set Score.skipBars = ##t
                        % Voice 2
                      \times 2/3 {es'16 bes as} g16 es' f, es f es' g, es' as, es' g, f g bes d f
                      es bes g bes es g, as  f' bes, g' as es c g' f es
                      d c bes c
                        
                
                  }
>>


}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key es\major
   \tempo "o. Bez."
  \time 4/4
  \partial 8
     \set Score.skipBars = ##t
    r8 R1*15  r2 r4 r8 es8 bes' as16 g as8 bes c bes r8 
   
    
    
    }