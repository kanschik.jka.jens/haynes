 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4
    es4. es8 d d d d
    d16 b c4 g8 f es16 d  g f es d
    es4 r8
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Vivace"
  \time 6/4
    r8 c b c16 d  g,8 b c es d es16 f bes,8 d
    es g16 f g8 c, g' bes, a f'16 es f8 bes, f' as,
    g es'16 d es8
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Affetuoso"
  \time 6/8
    r8 d4~ d8 g, g'
    fis4 r8 r4 r8
    r8 d4~ d8 g, g'
    es16 d es4~ es8 f, f'
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Allegro"
  \time 4/4
    g'4 g g4. f16 es
    d8 c d b  c g c16 d es c
    d8 g, d'16 es f d es8 g, c16 d es c
    d g, a b c d es f
}