\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. [Vivace]"
  \time 2/2
  d4. d8 g4 g32 g f es d c bes a
  g8. g'16 g8. g16  fis4 d8 r
}



\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Gavotte "
  \time 2/2

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Bourree altern."
  \time 2/2

}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Sarabande"
  \time 2/2

}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "5. Rigaudon altern."
  \time 2/2

}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "6. Trio"
  \time 2/2

}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "7. Passacaille"
  \time 2/2

}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "8. Passepied altern."
  \time 2/2

}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "9. Trio"
  \time 2/2

}
