 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 3/4
  b8 g4 d b8
  g d''4 c b16 a
  b g fis g  d g fis g  b, g' fis g
  g,8 d''4 c b16 a
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. Adagio"
  \time 4/4
  e16 b g e e' b g' e  b' fis dis b fis' b, b' fis
  g b e, g  b, e g e dis32 c b8. b4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Allegro"
  \time 2/4
  \partial 8
  d8 b g' a, fis'
  g fis16 e d8 c
  b g' a, fis'
  g fis16 e d8 d
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}