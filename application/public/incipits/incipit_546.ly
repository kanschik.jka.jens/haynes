 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 2/2
  f2~ f8 g16. f32 f8 es16. d32 es
  d4
}

\relative c'' {
  \clef bass
  \key bes\major
   \tempo "2. Allegro"
  \time 2/2
  f,,8 d bes g' f bes4 a16 g
  f[ es d f] es d c es d8 bes'4 a16 g 
  f
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 3/4
  d4. r16 d d8. d16
  es4  r r
  e4. r16 e16 e8. e16
  f4
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Vivace"
  \time 3/8
  f,8 bes16. c32 c16. bes64 c
  d8 d16. es32 es16. d64 es
  f4 g8
  f16. es32 d8 es
  es16. d32 c8 d~
  d c
}
