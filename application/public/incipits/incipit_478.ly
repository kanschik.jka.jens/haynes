\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ire. Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Marche des Tures. Lourdement"
  \time 2/2
  a2 a
  
  a2. s16 s32.
  e64[ fis32 gis a b]
 
  cis2 cis
  cis2.
  s32.
  e,64[ fis32 gis a b cis d]
  e2 e
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Le Plaintis. tres lentement"
  \time 3/4
  \partial 2
  e4. d16 cis
  \grace cis8 b4 r4 r4
  r4 cis4. b16 a
  \grace a8 gis4 r4 r4
  

}

\relative c' {
  \clef treble
  \key a\major
   \tempo "3. La Villageoise. gayment"
  \time 6/8
  \partial 8
  e8 a4 a8 a8. b16 gis8
  a4 e8 e4 e8
  a4 a8 a8. b16 gis8
  a4 

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Ier. Tambourin. Vite"
  \time 2/4
  e4 e8 d
  cis4 cis8 e
  d[ cis b cis] 
  a4 a
  

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "5. Aria 1a."
  \time 3/8
 e4 d8
 cis b a
 \grace cis8 b8. a16 b8
 \grace b8 cis4 \grace b8 a8
 e'4 d8
 cis b a 
 b16 d cis8 b a4.

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "6. Gavotte"
  \time 2/2
  \partial 2
  cis8 d e fis
  e4 a gis8 fis e d
  cis4 b cis8 d e fis
  e a, gis a d4 cis 
  \grace cis8 b2

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "7. Ier. Air. gay"
  \time 6/8
  \partial 8
  e8 cis8. b16 a8 cis8. b16 a8
  e'4. a,4 a'8
  gis4 fis8 e4 d8
  cis4 b8 a4 

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe. Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Lentement"
  \time 3/4
  c4 g' f
  \grace f8 es4 \grace d8 c4 as'
  g c4. d8
  \grace c8 b4. as8 g4
  es g8 f es d
  c4

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIIe. Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. L'indifferente"
  \time 3/4
  e4 \grace d8 cis4 d
  e a, b
  \grace b8 cis4 \grace b8 a4 e'
  a b4. cis8
  fis,4 b4. cis8
  \grace a8 gis4. fis8 e4

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IVe. Suitte " 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \key a\major
   \tempo "1. Musette"
  \time 3/8
  \partial 8
  e8 a4.
  \grace a8 gis8. fis16 gis8
  a e e 
  cis'4. 
  \grace cis8 b8. a16 b8
  cis a e
  e'4 fis8
  e d cis 
  e fis16 e d cis
  \grace cis8 b4 e,8

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ve. Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Le Prudent. Moderement"
  \time 2/2
  \partial 2
  cis4 b
  a e fis8 a gis b
  a cis b d cis e d cis
  b cis b a gis a gis fis
  e2

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VIe. Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. La Delicate. Gracieusement"
  \time 3/4
  g4 b g
  d'8 c d e fis d
  g4 g, g'8 fis 
  e d e fis g e
  a4 a,

}


