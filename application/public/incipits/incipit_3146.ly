\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Nun mögt ihr stolzen Feinde schrecken [BWV 248vi/9]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key b\minor
                      \tempo "o. Bez."
                      \time 2/4
                      \partial 8
                      % Voice 1
                      fis8 b[ d cis ais]
                      b[ d cis ais]
                      b[ g' fis e] 
                      d16 cis d cis b8
                 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key b\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                    d,8 d[ fis e cis]
                    d[ fis e cis]
                    d[ e' d cis]
                    b16 ais b ais b8
                       
                        
                
                  }
>>
}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key b\minor
   \tempo "o. Bez."
  \time 2/4
  \partial 8
     \set Score.skipBars = ##t
    r8 R2*15 r4 r8 fis8
    b d cis ais
    b d cis ais
    b g' fis e 
    d16 cis d cis b8
    
    
    }