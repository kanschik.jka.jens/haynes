 
\version "2.16.1"
 
#(set-global-staff-size 14)
 


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "Gravement"
  \time 4/4
    d4. cis8 d4 a
    bes2 a
    g4 f e a
    f e8 f d4
}