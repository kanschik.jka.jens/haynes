\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. o.Bez."
  \time 12/8
\partial 8
e8
c4 b8 a4 gis8 a2.~
a~ a4. r8 r a'
f4 e8 d4 a8 b4 c8 c4 b8 c4.
}
