
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Marche"
  \time 3/4
c4 g c 
e \grace d8 c4 e 
g f8 e d c 
d4.
} 
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "2e Suite  "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prelude"
  \time 3/4
 \partial 4
d4 g4. a8 b4
a4. g8 a4
b a g 
fis \grace e8 d4
 
 
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "3e Suite  "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Fugue"
  \time 2/2
 r4 c4 c c 
d8 c d e d e f d 
e f e d c d e f 
g4 d g2~ g4 
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "4e Suite  "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Musette en Rondeau"
  \time 3/4
 \partial 4
e4 g2 f4 e2 d4 c g a g c b c 
 
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "5e Suite  "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Lentement"
  \time 2/2
 b4. c8 a4. g8 d'2. r8 d e4. e8 d4. c8 b2 \grace a8 g2 
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "6e Suite  "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Lentement"
  \time 4/4
 \partial 16
c16 c8. d16 e8. f16 d8. d16 g4~ g8. f16 g8. d16 c8. d16 d8. c16 b8. d16 f4.
 
 
 
}
 


