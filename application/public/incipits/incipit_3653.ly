\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Moderato"
  \time 4/4
  \partial 8
  f,8
  bes d4 c16 a  bes8 f'4 es16 c
  d8 c4 bes16 a d bes f d
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. o.Bez."
  \time 3/8
    g'8. f32 es d8
    r16 es d c bes a
    bes es d c bes a
    g
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. o.Bez."
  \time 3/8
      bes8 bes bes  g'4.
      f8 bes, bes g'4. f4 r8
      
}

