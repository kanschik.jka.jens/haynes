\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/2
    g'8 d g a  b g b c
    d e16 d d8 e16 d d8 b16 a b8 c
}
