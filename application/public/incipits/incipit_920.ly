\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro moderato"
  \time 2/4
    <g, es'>8. bes16 <g es'>8. bes16
    f'8 g bes16 as g f
    <g, es'>8. bes16 <g es'>8. bes16
    f'8 g bes16 as g f
    es8 es' \grace d8 c4
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio Cantabile [Tutti]"
  \time 4/4
    r16 <es, c'>16 <es c'>16 <es c'>16 r <es c'>16 <es c'>16 <es c'>16
    r16 <d bes'>16 <d bes'>16 <d bes'>16 r <d bes'>16 <d bes'>16 <d bes'>16
    
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio Cantabile[Solo]"
  \time 4/4
   \set Score.skipBars = ##t
    bes'4 f8. d16  es32 es f d es8 r4
    es32[ c d es] f g a bes c[ d es d] es c a es d d es c d8 r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro assai"
  \time 2/4
    es,2 g4 bes es2 c
    as4 f bes bes,
    es16 f g f es4
}