\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto Primo " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 3/4
  d4 c8 b a g
  e'8. g16 \grace g8 fis2
  g16 a b8 fis g fis g
  a,4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  d8 b16 g g'4 fis8 g16 d b'4 a8
  b16 d g,4 fis8 g16 d g d g d g d 
  e c e c e c e c 

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Aria 1a"
  \time 3/8
  d8 c16 b a g
  g'8 f4 e8 c, r8
  e'8 d16 cis b a
  a'8 g4 fis8 r16 d16 e fis
  g d g b g d
  e c e g e c
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto Secondo " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  g4~ g16 c, e g a4~ a16 fis fis d
  \grace c8 b8. c32 b c16 g e' g, f'8 f f8. e32 d
  e16 c b d c4. cis8 d4~ d4

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  g8 e c g, b' c e d g
  e c g, b' c c, d'8. c32 d
  e16 e f e f f g  f g g a g a8 b
  c e, d g e

}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Pio affettuoso"
  \time 3/4
  \grace {es16[ f]} g2.
  \grace {as16[ g]} f2.
  es4 d c 
  \grace c8 b2 c4
  \grace {d16[ es]} f4 es d 
  es d8 g, b g

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 3/8
  \partial 8
  g8 c, d16 c b d
  c8 g d'
  e f16 e d f
  e8 c g'
  a16 g a f g a 
  g f g e f g
  f e f d e f
  e8 e, r8

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto Terzo  " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio sostenuto"
  \time 4/4
  \partial 8
  d8 g4. a16 g a4. d8
  \grace c8 b4 c~ c8 bes16 a
  bes8 g
  a bes c bes16 a bes8 a16 g g4~ g4 

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  r32 d32 e fis
  g8 g g d es4. bes8
  c16 a c es d c bes a bes d g, bes a c bes a
  bes d g, bes a c bes a bes8[ g,]

}
  
  \relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 3/4
  es4 es4. f16 g
  f4 f4. g16 as
  g8. a16 bes4 es,~ 
  es d2 es4 g2 
  

}
  
  \relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Aria 1a "
  \time 3/8
  \partial 16
  d16 d8 g fis
  \grace fis8 g4 d8
  es c4
  d8 \grace c8 bes4
  c8
  \grace bes8 a4
  bes16 c d4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto Quarto " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  a8 d d d d d8. cis32 b cis16 a cis e
  fis8. e16 fis16 gis a b e,8. d16 e fis gis a
  d,8. cis16 d fis e d cis16. d32 e8

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 3/4
  \partial 4
  a4 fis e cis
  d8.[ cis16 d8 fis e g]
  fis8.[ e16 fis8 a] cis, e
  d8.[ cis16 d8 fis e g]
  fis4 g a

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Adagio"
  \time 12/8
  \partial 8
  fis8 fis d b b' g fis e8. dis16 e8~ e16 g fis e d cis
  d8 fis b g e d d4 cis8~ cis16 g' fis e d cis
  d8 fis b

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Allegro"
  \time 2/2
  \partial 2
  fis4 a
  d,2 fis4 a
  d,2 a'4 d
  b g e a 
  d,2 a'4 d
  b g e cis'
  d d, 

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto Quinto " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Affettuoso"
  \time 3/4
  \partial 4
  e4 \grace e8 dis2 e4
  b2 g'4
  \grace g8 fis2 g4
  \grace e8 dis2 e4
  fis g a 
  g fis e
  c'2.~ c4

}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  b16 a g8 b b, e \grace e8 dis4 r8 b'16 b,
  c16. b32 c16 a' a, c b a b16. a32 b16 g' g, b a g

}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "3. Adagio"
  \time 3/4
  g8 fis g a b g
  a g a b c b
  b4 d g,~ g4 fis2 g r2

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro, Giga"
  \time 12/8
  \partial 8
  b8 g b e b e g \grace g8 fis4. r8 b8 b,
  c fis c a a' a, b d b g g' g,
  a c a fis fis' fis, 
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto Sexto " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 3/4
  \partial 4
  f4 bes,8 a bes c d bes
  c4 f, f'~
  f es8 d es c
  d4 f bes~ 
  bes a8 g a f 
  g4 c,

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/2
  bes4 bes bes bes
  f'2. es8 f
  g4 f g d
  es2. d8 es
  f4 es f c
  d8 f es d g f es d

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Affettuoso"
  \time 3/8
  \partial 8
  d8 g a bes~
  bes a g 
  fis4 g8
  as g f
  es16. d32 c4~
  ~c16 es d c' bes a bes4.~
  ~bes16 d c bes a g
  

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 2/4
  f4 g 
  f es8 d
  f[ bes, g' bes,]
  f'4 es8 d
  c4 d8 es \grace bes8 a4. 

}

