\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 2/2
  \set Score.skipBars = ##t
  \partial 8
  a8
  d8. e16 f8[ g]  a f16. e32 d8[ a']
  bes a g f e4 r8 e
  f d16 cis d8[ f]
}
