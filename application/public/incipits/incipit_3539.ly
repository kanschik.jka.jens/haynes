 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro moderato"
  \time 4/4
  \partial 4
  d4
  g8 d d2 c8 b
  e c c2 b8 a
  b16 c d e d e fis g fis g fis e d c b a g4 r r
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Adagio"
  \time 2/2
  \partial 4
  c8 e
  g c c2 e,4
  e8 d d2 \times 2/3 { e8[ f g] } 
  \grace g8 f4 e2 d4
  e16 d c d c4 r8
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Menuetto I."
  \time 3/4
  \partial 4
    d,8 g g2 d8 b' b2 g8 d'
    d4. d8 e c
    c4 b8
}



\relative c'' {
  \clef "treble"   
    \key g\major
    \tempo "4. Allegro"
    \time 2/4
    \partial 8
    d8 g4. fis16 e
    d8 e b c
    d4. e16 c b4 r
}
