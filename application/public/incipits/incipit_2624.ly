\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Largo"
  \time 4/4
  a4. b16 cis d8 a r b
  a4. b16 cis d8 a r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Vivace"
  \time 2/4
  d4 e
  r8 d e e
  fis4 g
  r8
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Siciliano"
  \time 12/8  << 	
       {
         r2. bes8. a16 g8 e'4 bes8
        bes8. g16 a8 r4.
       }
      \new Staff {  
        \key d\minor
         f4 f8 f4 f8 e4. r8 r g
         g4 f8 f4 f8
                }
    >>

}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Vivace"
  \time 6/8
  r4. fis8 d16 e fis g
  a8 a a a g16 fis g a
  fis4 b8 e,4
}
