\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ve Suite" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \key f\major
   \tempo "1. Ouverture "
  \time 3/4
  f8. f16 a8. a16 c8. c16
  f2 e4
  a2 g4~
  ~ g8 c, f4. g8
  e4 

  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. 1er Rondeau "
  \time 3/4
  r4 f4 c
  a \grace g8 f4 a
  c g bes
  a d8 c bes a 
  g4

  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Ariette "
  \time 6/8
  \partial 4.
  a8 bes c
  d4. c8 bes a
  g4 f8 a4 bes8 
  c4 d8 \grace d8 e4 f8
  e4.

  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. 1er Rigaudon "
  \time 2/2
  \partial 4
  c4 d2 e
  f8 e f g f4 c
  f4. g8 g4. f16 g
  a4 g8 a f4

  
}

\relative c' {
  \clef treble
  \key f\major
   \tempo "7. Badinage. Gracieusement "
  \time 6/8
  r8 r f8 f a c
  e, g c bes a g
  a bes c f, c' f
  e c f bes4 a8
  g4

  
}

\relative c' {
  \clef treble
  \key f\major
   \tempo "8. 1er Rondeau. Moderem.t "
  \time 2/4
  r8 f8[ a c]
  d, bes'16 a g f e d 
  c8 c'16 d e f g e 
  f g e f  g a bes g
  a8 

  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "10. 1er Menuet "
  \time 3/4
  f8 c a' c, f c
  g' c, bes' c, g' c,
  a'4 bes8 a g f
  g f e d c bes

  
}


\relative c' {
  \clef treble
  \key f\major
   \tempo "11. 1er Gavotte "
  \time 2/2
  \partial 2
  f4 a
  g c bes a
  g f c' es 
  d g e4. f8 g2
  
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "13. 1er Tambourin "
  \time 2/4
  f4 a16 g f e
  f4 a,8 bes
  c bes16 a g8 c
  a[ f a c]
   
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "15. Chaconne "
  \time 3/4
  r4 f4 a
  g c,8 d e c
  f4 f, bes~
  ~bes8 a g a bes c
  a4
  
   
}

