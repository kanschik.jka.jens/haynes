\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 2/2
  c'2 fis,
  g4 g2 a4
  f4. e16 f g8. a16 g8. f16
  e4 c r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   R1*66 
  c2 c16 e d f  e[ g f a]
  a8 g g4. a16 g f8 e
  d \grace e8 d16 cis d8[ e] f d g f
  dis2 e4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondeau"
  \time 6/8
  \partial 4.
  e8 f fis
  g e c  \grace d8 c b c
  cis4 d8  g, a b
  c d e g f e
  e4 d8
}
