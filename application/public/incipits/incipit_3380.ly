\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/4
    g'4 d d16 c b c d4
    b8 a16 g a8 g16 fis g8 d b g
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Largo"
  \time 2/2
    e8 g16 a b8 g g fis r b16 g
    e8 fis16 g fis8. e16 dis8 b r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
    r8 g' g g g g
    g b16 a g8 d c b
    e e d d c c b d g4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Polonaise"
  \time 3/4
    d8. e32 fis g4 b
    a8 fis d2
    e8. d32 c d4. b8
    c b16 a b8 d b g
}