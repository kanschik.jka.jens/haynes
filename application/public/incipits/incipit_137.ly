\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Combattans"
  \time 2/2
  d,16[ d d d]  fis fis fis fis   a[ a a a] d d d d
  d,16[ d d d]  fis fis fis fis   a[ a a a] d d d d
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Aria"
  \time 6/8
  d8 a d d4.
  e8 a, e' e4.
  fis,16 a a a d a r4.
  gis16 b b b e b r4.
}
  
  \relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Menuett"
  \time 3/4
  a4 d d
  d d8 d d d
  a4 e' e
  e e8 e e e
  }

  \relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Hedelseck"
  \time 2/4
  d8 a a b16 cis d8 a a b
  a g fis e fis e16 fis d4
  }
