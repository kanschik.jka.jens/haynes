 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 4/4
  \partial 8
      g'8
      \times 2/3 {e16[ f g]} g4 g8
      \times 2/3 {c,16[ d e]} e4 e8
      \times 2/3 {a,16[ b c]} c4 c8
      f32 e16. d32 c16. b32 a16. g32 f'16.
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  c32 d e f
  g8 g g g
  g g16 e g8 g16 e
  g8 g g g
  g8 g16 e g8 g16 e
  g4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Allegro"
  \time 3/4
  \partial 4
  c4
  g'8.f32 g a4 b,
  \grace b8 c2.
  a8. c16 g8. c16 d8. e32 f
  e8 d c2
  
}

