 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro"
  \time 3/4
    a'4 gis a g f e
    f e dis e8. dis16 e4 r
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Larghetto"
  \time 2/4
  g16 c c c  \grace d16 c16 b32 c b16[ b]
  g16 d d d  \grace e16 d16 c32 b c8
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "3. Scherzo"
  \time 3/8
  e8. d16 c8
  d8. c16 b8
  c8. b32 c d32 c b a
  gis16 a b4
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Menuett"
  \time 3/4
  \partial 8
  
}