\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 2/4
  \partial 8
  g'8
  g g g g
  g4 r8 c16. g32
  g8 g g g
  g4 r8 c16. g32
  g8 f r a16 f f8 e r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 2/4
  \partial 8
  g8
  d'8. e32 d d8 d
  d16 g d g d8 r
  e8. fis32 e e8 e e16 g e g e8 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto [Tutti]"
  \time 3/8
  \partial 8
  g8 e' e e
  e16 f g8 r
  c,16 b a g a b
  c4 g8
}

