\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Moderato"
  \time 3/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

  \partial 4
  bes8 d
  f2.~ f2 f8 bes
  bes a a g f es
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio"
  \time 2/4
  \partial 8
    bes8
    g'4. f8
    f es as4~
    as8 g g16 f as f
    d8 es
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Menuetto"
  \time 3/4
      \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \partial 4
  f4
  bes2 \grace g16 bes8 a16 g
  f4. fis8 g es
  d4 f8 es c a
  bes4
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Finale. Allegro"
  \time 2/4
      \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \partial 8
    f8
    f d bes' a
    g f es d es d es d
    es4
}
