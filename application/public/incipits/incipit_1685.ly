\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegretto ma non troppo"
  \time 2/4
  e8 cis32 d cis d e16 e e e
  fis8.[  \grace { gis32[ fis e fis] } gis16] a16 a gis fis
  \grace fis8 e8 d16 cis b cis d e d4 cis8 r
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Tempo di Minuetto"
  \time 3/4	
  a2.~ a
  a8 r e'4 cis
  a8 \grace b'8 a16 gis a8 e d cis
  b r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro molto"
  \time 4/4
  \partial 4
  d,8. es16 f8 f r f bes f d' f,
  \grace g8 f8 es es2 c8. d16 es8 es r es
  c' a es' es,
  \grace f8 es8 d d2
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Rondeau. Allegretto"
  \time 2/4
  d4 \grace f16 es8 d16 c
  f4 r8 bes
  bes,4 \grace d16 c8 bes16 a
  bes8 r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 3/4
  es,2. f as g4 f es 
  c'8. as16 es'4 r8 c
  bes8. es16 g4 r8 bes
  bes,4 as g
  g2 \grace { f16[ g]} f4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 4/4
   es,2 g4. bes8 es r g4 bes8 r r4
   \grace bes8 as4. es8 c' c es c
   \grace c8 bes4. g8 es4 r8
}
  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  r2 fis,4. g8 a4 r fis4. g8
  a4 r c,2 b4 r
  g'8 \grace a16 g16 fis32 g a8 g
  fis32 g a8. r4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Minuetto Grazioso"
  \time 3/4
   a4 a4. b8
   a g g2
   fis4 fis4. g8
   fis4 \grace fis16 e8 d16 e d4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegretto"
  \time 4/4
  d2 \grace d8 c8 b \grace b8 a g 
  g4. \grace b8 a8 g4 r
  e'4 e8. e16 c'4. e,8
  d4. e16[ d c] b4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Minuetto"
  \time 3/4
   d4 b r8 c16 b32 c
   d4 g, r8 b16 a32 b
   a4 e' r g,16 fis32 g
   fis4 d r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Cantabile"
  \time 2/2
  \partial 8
  f,8
  \grace { b16[ c d] }c2 f8 e d c 
  \grace { b16[ c d] } c2 g'8 e c bes
  \grace bes8 a8. bes16 c4 r8 f f f
  es4 d8 r g4 f8 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Rondeau. Allegretto"
  \time 2/4
   c'4 a8 f f4 e8 f
   fis4 g8 bes,
   \grace c16 bes8 a16 bes a4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 4/4
  \partial 4
 \partial 8
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 4/4
  \partial 8

}
