 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Violin"
  \key d\minor
   \tempo "Allegro"
  \time 2/2
     \set Score.skipBars = ##t
     R1*2
     f8 a f a  f a f a
     d,16[ c bes c]  d c bes c
     d16[ c bes c]  d c bes c
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key d\minor
   \tempo "Allegro"
  \time 2/2
     \set Score.skipBars = ##t
     R1*13
     a4 d a r8 a
     f f g g a d,
}