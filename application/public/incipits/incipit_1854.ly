\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Andante"
  \time 2/4
  \partial 8
  d8
  bes a16 g a8 fis
  g4 r8 a
  g16 bes a g  a c bes a
  bes8 g r a
}
