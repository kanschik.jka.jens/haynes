\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro Concerto [Tutti]"
  \time 4/4
    f2 g4~ g8. a16
    bes4 bes, r16 bes a bes f bes d, f
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro Concerto [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*35 
   f2 g4~ g8. a16
   bes4 bes8. bes16 bes4 bes
   bes2 bes
   bes~ bes16. c32 d16 c bes a g fis
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Largo assai [Tutti]"
  \time 3/4
  <a, f'>4. f''8 f,16. g32 \grace a8 g8
  a8 f' r g r a
  \grace {bes16[ c] } d4 r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Largo assai [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.* 15
   f2. g
   a8. bes32 c c8 c c c
   c8. d16 c4 r8 r16 f,16
   g8. a32 bes
   a8 g f e 
   \grace g8 f8. e16 f4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro [Tutti]"
  \time 3/8
    \partial 8
    \times 2/3 { d16[ c bes] } bes4 bes16. bes32 bes4
    \times 2/3 { d16[ c bes] } bes4 bes16. bes32 bes4
    \times 2/3 { d16[ es f] } es8 r
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro [Solo]"
  \time 3/8
    \partial 8
      r8
         \set Score.skipBars = ##t
         R4.*54
         f4. bes
         \grace bes8 a16. g32 f8 es
         es d r
         g16 f32 es d16[ c b c]
         c'8 r16 es,16 f g
         
}