\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key b\minor
                      \tempo "Andante un poco"
                      \time 3/4
                      d,4 b8 cis d4
                      cis2 cis4
                      fis, fis' ais,
                      b8 cis16 d a8 b cis b16 cis
                      d8 e16 fis g8 fis e d
                      cis8. b16 cis4
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key b\minor
                        % Voice 2
                       r4 fis b
                       b8 ais ais4. gis16 ais
                       b8 cis16 d
                       cis4. b16 cis
                       d8 e16 fis e4. d16 e
                       fis4 e8. d16 cis8. b16
                       ais8. gis16 fis4 
                  }
>>
}
