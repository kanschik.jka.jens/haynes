\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/4
  g,16 g' fis g  g, b' a b
  g, g' fis g  g, d'' c d
}
