\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Ouverture"
  \time 2/2
   \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
d4~ \times 4/6 { d16[ a b cis d e] } fis4 d
a'4. a8 a4. g16 fis
e2
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Ouverture"
  \time 3/8
  r8 a'16 g fis e d cis d e fis d
  e fis e cis d e fis8.
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Marche des Epicuriens"
  \time 2/2
\partial 2
d4 a d cis8 b a g fis e
d4 e8 fis e4
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Marche des Vestales"
  \time 2/2
d2 f
a,4 a' d, cis8 d e4 e8 f g4 e f2 d
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Marche des Vestales"
  \time 2/2
\partial 2
a'8 g a4
d,8 e fis4 e8 d cis b
a2 fis'8 e fis4
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "5. Sarabande"
  \time 3/4
d4 d4. d8
f,4 f4. e16 d
a'4 d4. e8 f2 e4
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "6. Loure"
  \time 6/4
\partial 2.
a4. bes8 a4
d, bes'2 a4 d2
cis4 a d d cis2
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "7. Chaconne"
  \time 3/4
r4 fis a
cis, e8 d cis b
a4 d4. fis8
e4 e fis8 g
fis4
}