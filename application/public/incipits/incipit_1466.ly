
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiére Suitte"
    }
    \vspace #1.5
}
 
\relative c' {
\clef "treble"   
  \key d\major
   \tempo "1. Prelude. Lentement"
  \time 2/2
r4 r8 fis8 \grace {fis16[ g]} a4. a8
a4. d,8 b'4. b8
b4. a16 g a4. a8 a4 r16

 
}

\relative c'' {
\clef "treble"   
  \key d\major
   \tempo "2. Allemande. La Royale. Gracieusement"
  \time 2/2
  \partial 8
  a8 a2 r8 r16 a16 d8. cis16
  d4 e fis4. g8
  e4 fis g4. a8
  fis4. e8 d4

}

\relative c' {
\clef "treble"   
  \key d\major
   \tempo "3. Rondeau. Le Due d'Orleans. Gay"
  \time 3/4
  fis4 a d
  g, b e
  cis8 a b cis d e
  fis e d e fis g
  a4. a8
  e4
  

}

\relative c'' {
\clef "treble"   
  \key d\major
   \tempo "4. Sarabande. La d'Armagnac"
  \time 3/4
  fis4 \grace {fis16[ g]} a4. g8 
  fis4. e8 fis4
  g e4. fis8
  fis4. e8 d4
  fis4 fis \grace g8 g4 \grace {fis16[ g]} 
  a2.

}

\relative c''' {
\clef "treble"   
  \key d\major
   \tempo "5. Gavotte. La Meudon"
  \time 2/2
  \partial 2
  a8 g fis e
  d4 e8 fis e fis g e
  fis4 \grace e8 d4 fis g8 a
  b4 a8 b g4. fis8 
  e2

}
\relative c'' {
\clef "treble"   
  \key d\major
   \tempo "6. Menuet. Le Compte de Brione"
  \time 3/4
  fis4 e2
  fis4 a a,
  d e \grace d8 cis4
  d8 cis d e d e 
  fis4 
}

\relative c'' {
\clef "treble"   
  \key d\minor
   \tempo "7. IIe Menuet"
  \time 3/4
  f4 e d 
  cis2 a'4
  g f e 
  f8 e f g f4
  f e d 
  cis2

}

\relative c'' {
\clef "treble"   
  \key d\major
   \tempo "8. Gigue. La Folichon"
  \time 6/8
  \partial 8
  fis8
  a4 a,8 d e d
  cis4 b8 a b a
  g a g fis d d'
  e cis a 

}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxiéme Suitte"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prelude. Lentement"
  \time 2/2
\partial 2
b4. d8
\grace d8 g,4. g8 a4 b \grace {g16[ b]}
c2. c8 b16 c
b2 d4. d8
e2 e4 fis8 g
fis4. 

 
}
\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Allemande. L'Atalance. Vivement"
  \time 4/4
  \partial 2
  r16 g16 fis e d c b a
  g8 b16 c d8 d d c16 b c8 d16 c
  b4 r16 b16 a g a8 d g, a16 g
  fis8 
  
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Sarabande. La Fidelle"
  \time 3/4
  b4 r16 c16 b c d8. g,16
  fis4. d8 g4
  a \grace {a16[ b]} c4 b8 c
  b4. a8 g4
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Petit Air tendre"
  \time 2/2
  \partial 2
  b4 a8 b
  \grace a8 g4. a8 b4. c8
  a4 a b a8 b
  \grace a8 g4 a b4. c8 a2
  
 
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Gavotte en Rondeau. La Maillebois"
  \time 2/2
  \partial 2
  d4 c8 b
  c4 b8 a c b a g
  fis4 \grace e8 d4 g8 a fis g
  a b a[ b16 c] b4. c8 a2
  
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "6. IIe Gavotte. Mineur"
  \time 4/4
  \partial 2
  bes4 a8 g
  \grace bes8 a4 fis g a8 bes16 c
  bes4 a bes8 d c es
  d es a, bes a4. g8 g2
  
 
}
\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Rondeau. Le Baron"
  \time 3/4
  g4 a fis
  g d e
  c2 b4
  a d a
  b a8 b
  g4
  
 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisiéme Suitte "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Allemande. La Cascade de St. Cloud. Piqué"
  \time 4/4
\partial 8
r16 d16
d8 b g d g4 r16 g16 a b
e,4 r16 fis16 g a d, c' b a a8. g16
g8[ g] d b g4
 
}

 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Sarabande. La Guimon"
  \time 3/4
  g4 d'4. e8
  c4. b8 a4
  b4 c8 b a g
  fis4. e8 d4

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Courante. L'indifferente. Legerement"
  \time 3/4
  \partial 4
  r8 g8
  g4. b8 a g
  a4 d, g
  e fis4. g8 fis2.

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "4. Double"
  \time 3/4
  \partial 4
  r8 g8
  g fis g b a g
  a fis d fis g b
  e, d e a fis g
  fis2.

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Rondeau. Le plaintif. tendrement"
  \time 3/4
  r4 r8 b8 c8. d16
  a4. b8 c8. d16
  g,4. a8 b8. c16
  fis,4 \grace e8 d4

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "6. Menuet. Le Mignon. Un peu doucement"
  \time 3/4
  g8 fis g4 d
  b2 d4
  g,4 g a \grace {g16[ a]} 
  b8 c b a g4
  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "7. Gigue. L' Italienne"
  \time 12/8
  \partial 8
  r16 d16
  d4. r8 r8 d8 g fis g a g a 
  b4. r8 r8 a8 g a b e, fis g
  fis4.
  

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatriéme Suitte "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key e\minor
   \tempo "1. Prelude. Lentement"
  \time 3/4
\partial 4
e4 b'4. a8 b fis
g4 g b
e,4. g8 fis e dis2 r4
r4 fis4 b
a \grace fis8 e4

 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allemande. La Fontainebleau. Gravement"
  \time 4/4
  \partial 8
  r16 e16
  e4 r16 b16 g e b'8 e fis g
  dis4. e16 dis32 e
  fis8 b, a b16 a
  g8 e' fis, dis' e,
 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Sarabande. Le Départ. douloureusement"
  \time 3/4
  e4 \grace {e16[ fis]} g4. a8
  fis4 b4. b8
  b4 a4. g16 a
  b4 fis8 g a fis
  g4 fis e 
  dis2

 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Air. Le Fleury. Gayement"
  \time 3/4
\partial 4
b4 e e8 fis g4
fis4 fis8 g a4
g fis e 
b'2 fis4

 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "5. Gavotte. La Mitilde. Tendrement"
  \time 2/2
\partial 2
e4 fis8 g
dis4. e8 fis g a fis
g4 \grace fis8 e4 g a \grace {fis16[ a]}
b4 a8 g fis e dis cis b4

 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "6. Branle de Vilage. L'Auteiiil"
  \time 2/2
\partial 8*3
e8 e, e'
dis cis b a g fis e4
e' e8 fis g4 g8 a
b4 e, dis e fis2
 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "7. Menuet. Le Beaulieu"
  \time 3/4
  e4 fis dis
  e b e
  g fis2
  g8 fis g a g fis
  e4 fis dis 
  e b
 
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "8. IIe Menuet"
  \time 3/4
  e4 e fis
  gis fis8 gis e4
  b8 cis d cis b a
  gis a gis fis gis e


 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinqiéme Suitte "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allemande. La Chauvet"
  \time 4/4
\partial 8
r16 e16
e4. fis16 e32 fis g8 e b'8. b16
b4 r16 a16 g fis g8 b e, fis16 g
dis4. 

 
}

 
\relative c''' {
  \clef treble
  \key e\minor
   \tempo "2. Rondeau. Le Lutin"
  \time 2/2
  r4 b4 b8 a g fis
  e4 e e e
  c' c8 b a g fis e
  dis b b cis dis e dis e
  fis g fis g16 a
  g4. fis16 g
  fis4

}

 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Gigue. La Perousine"
  \time 12/8
  \partial 8*7
  b8 e4 b8 e4 fis8
  g4. g8 a b e, fis g fis g e
  dis4 e8 fis4

 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Pieces a deux Flutes [1]"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "Les delices ou le Fargis. Trés lentement"
  \time 2/2
d4. d16 e fis4 r16 fis16 e d
e4. e16 fis g4 r16 g16 fis e
fis4 \grace {fis16[ g]} a8. d,16 d2~ d 
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "Rondeau. Le Champetre. Nommé parle Roy les Ecos. Gay"
  \time 2/2
  r4 a8 g fis4 e
  fis fis8 e d4 cis
  d fis8 g a4 b8 a
  g4 fis e8 fis d e
  cis4

}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ecos.  Pour al Flute traversiere seule "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key g\major
   \tempo "1. ohne Bez."
  \time 4/4
\partial 16*9
d16 g8.[ b16 d8. d16]
b8.[ a16 g8. d16] g8.[ b16 d8. d16]
b8. a16 g8 r16 g16 b8.[ d16 g8. b16]
\grace {b16[ a]} g8. 

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. ohne Bez."
  \time 3/8
g8 a b
a b c
b16 c b a g8
g a b
a b c 
b16 c b a g8
d' e fis
e fis g
fis16 g fis e d8

 
}


