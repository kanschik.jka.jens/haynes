\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Sie bemüht in dieser Zeit [BWV 185/3]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key a\major
   \tempo "Adagio"
   \time 4/4 
   \set Score.skipBars = ##t
 r4 e16 a32 gis a16. b32 gis8. a32 b e,16 d'32 cis d16. e32
 cis8 b16 a cis d32 e cis16 d32 e
 b8. cis32 d
 
                                                                                   

}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key a\major
   \tempo "Adagio"
   \time 4/4
   \set Score.skipBars = ##t
  R1*8
  r4 e16 a32 gis a16. b32 gis8 fis16 e d cis d8
  d cis r4
  
  
       
}



