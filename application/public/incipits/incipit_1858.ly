\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 6/8
    g4. e8 f g
    g4. e8 f g
    g4 e8 g f e
    e4 d8 d4 r8
}
