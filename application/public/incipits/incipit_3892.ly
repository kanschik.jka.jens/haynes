 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Obois Solo"
  \key f\major
   \tempo "Adagio"
  \time 4/4
    r8 c f f  \grace f8 e4 r8 g
    \grace g8 f f e f16 a \grace a8 g a g a16 f
    \grace f8 e8 e16 f g32 f e d c16 bes a8
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key f\major
   \tempo "Adagio"
  \time 4/4
     \set Score.skipBars = ##t
     R1*4
     r8 c8 f f f16 e e8 r4
     R1*3
     r8 c f f f16 e e8 r e 
     f c d c bes4. a8 g
}