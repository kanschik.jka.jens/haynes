\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Adagio"
  \time 3/2
  r2 a' gis
  a1 e2 fis4 e d2. cis4 cis1
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 2/4
  a'8 a, cis e
  a4. b8 
  cis b16 a gis8 a
  b4. e,8
  a e e e fis e r
}

\relative c'' {
  \clef treble
  \key fis\minor
   \tempo "3. Adagio"
  \time 3/4	
  r4 cis fis 
  eis2 a4
  gis2 fis4 eis2 a4~
  a b8 a gis fis eis2
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Giga"
  \time 6/8
  \partial 8
  e8 cis b a e' fis gis
  a4 a,8 r r e'
   fis d fis e cis d
   b4 e,8 r r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
  f4. r16 f
  d8. es16 f8. g16
  c,4. r16 f
  bes, c d32 es f g c,8. bes16
  a4.
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 4/4
  f4. f8 g f16 es f8 bes,
  g' f16 es f8 bes, g'16 f es g f es d es
  c8 f f es16 d c8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 3/4
  r4 bes4. bes8
  f'4 f4. es8 d4 d4. d8
  es4 g8 f es d
  c4 f2~
  f8 d bes g es'4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Giga. Allegro"
  \time 6/8
  bes8 c d  c d es
  d es f es f g
  f4 bes,8 g4 es'8
  a,4 c8 f4 g8
  f es d c4. bes r8 r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 3/2
  d4. e8 fis2 d
  fis4. g8 a2 fis
  b a4. g8 fis4. e8 d4. cis8 d2 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  d4. d8 a' a a a
  fis fis fis e16 d e8 a, a' a
  a d, a' a a a, r
}
  
  \relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Adagio"
  \time 6/8
  d4.~ d4 a8
  d e fis e a g fis4.~ fis4 a8
  d, e fis b,16 cis d e fis g
  cis,4.~ cis4
}
  
  \relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Allegro"
  \time 3/8
  fis16 g a8 d,
  fis16 g a8 d,
  b' a16 g fis e
  e4 d8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Allegro"
  \time 2/4
  e4. fis8
  gis fis16 e fis8 b,
  e4. fis8 gis fis16 e fis8 b,
}

\relative c'' {
  \clef treble
  \key cis\minor
   \tempo "2. Adagio"
  \time 3/4
  cis4. dis16 e dis8 e16 fis
  e4. fis16 gis
  fis8 gis16 a
  gis4. fis8 e8. dis16
  e8 fis16 gis dis4. cis8 cis2.
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "3. Gavotta. Allegro"
  \time 2/4
   \partial 8
   r16 b
   e4 fis
   gis4. r16 dis
   e8. fis16 gis8. a16
   gis4.
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "4. Giga. Allegro"
  \time 6/8
  \partial 8
  b8
  e4 e8 fis4 fis8
  gis fis e dis cis b
  e fis gis fis e dis cis dis e dis cis b e4 r8 dis4 r8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Adagio"
  \time 4/4
  e4 a32 gis fis e d cis b a a4. cis16 e
  fis8 e32 fis gis a d,8. cis16 cis4.
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  e8
  a gis16 fis e8 d
  cis4. e8
  a gis16 fis e8 d cis4. e8 a a, r
}

\relative c'' {
  \clef treble
  \key fis\minor
   \tempo "3. Adagio"
  \time 4/4
  \partial 8
  cis8
  fis32[ gis fis eis] fis gis fis eis] fis32[ gis fis eis] fis gis fis eis]
  fis8 fis, r gis'
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Allegro"
  \time 6/8
  a'4. e
  cis8 d e cis b a
  e'4.~ e4 e8 
  fis e s e d cis d cis b cis b a
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
  f8 g16. f32 es8 d d es16. d32 c8 bes
  f' g16. f32 es8 d32 es f16 c4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 3/4
  bes8 bes bes bes bes d16 c
  bes8 bes bes bes bes bes
  d d d d d f16 es 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 3/2
  d1 g8 f es d
  es2 d g8 f es d
  es1 d2
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 2/4
  bes4 f'
  d8. c16 bes8. c16
  d8. c16 bes8. c16
  f4 f, c'8. f16 e8. f16
  a,4 r
}

