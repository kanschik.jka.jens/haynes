\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/2
     \set Score.skipBars = ##t

     r4 r8 g  g[ a16 b] c8 d16 e
     f e f8 f[ e16 d] e8 f e4
}
