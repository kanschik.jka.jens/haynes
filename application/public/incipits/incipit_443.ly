\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "1.ere Serenade" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Ouverture"
  \time 2/2
  g'4. d8 bes g es' es
  c4. c8 d c bes a
  bes4 \grace a8 g4
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo ""
  \time 2/4
  a'4 r8 d,
  e e e d16 e
  fis8 fis fis e16 fis g8 d g4~ g fis g8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "2.eme Serenade " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Ouverture"
  \time 2/2
  r4 r8 e8 e4. b'8
  \grace a8 g4 \grace fis8 e4 a4. a8 a4 \grace g8 fis4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo " "
  \time 6/4
  b'2. b2 a4
  g fis e dis e fis
  b,2. fis'4 g a
  a b a a g fis
}

