 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "Andante"
  \time 3/4
  r4 r16 a b c
  b d f8 e gis,
  a4 r16 d e f e g c8 b a
}
