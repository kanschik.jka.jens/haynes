
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Amusement de Bellone "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Les Plaisirs des Mars. Fierement"
  \time 2/4
\partial 4
r16 c16 b a
g4 r16 a16 g f 
e4 r16 g16 f e 
f a g f e d c b c8 g
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Amusement Militaire "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Le Retour Du Rhim. Moderem.t"
  \time 3/4
\partial 4
d4 g8 fis g a b c
d4 d, e
d c4. d8
b4. a8 g4 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Amusement Palatin "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Le Retour Des Mars. Prelude"
  \time 3/4
\partial 4
c4 g'4~ g8.[ a16 g32 f e f g f e d] 
e4. d8 a4
c4. f32[ e f g c b a g f e d c] 
b4. 

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Amusement pour deux Muzettes "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Les Plaisirs De l'Isle Adam. Muzette. Gracieusem.t"
  \time 2/2
\partial 2
 e4 d8 c
g4 c' b8 a g f 
e4 d e8 f g a 
g f e d c b c f
d2 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Amusement pour une Muzette ou Vielle "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. La Feste Champetre. Marche"
  \time 2/2
\partial 2
g4 d8 b
g4 e'8 c a4 d8 c
b4 g d' g8 d
e4 e 

 
}

