\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Gott, dem der Erden Kreis zu klein [BWV 91/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key a\minor
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                      e4 a,8. b16 c8. d16
                      e8. a,16 g'2
                      f4 e8. d16 c8. b16
                      e8. c'16 a4. 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key a\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                    c,4 e, a8. b16
                    c8. d16 e2
                    a,8. d16 c8. b16 a8. gis16
                    a8. b16 c4.
                       
                        
                
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 3"
                     \set Staff. shortInstrumentName = #"Ob 3"
                     \key a\minor
                     \set Score.skipBars = ##t
                        % Voice 3
                 a4 c,8. d16 e4
                 a8. b16 cis2
                 d4 f, e~
                 ~e e4.
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key a\minor
   \tempo "o. Bez."
  \time 3/4
     \set Score.skipBars = ##t
    R2.*8 e4 a,8. b16 c8. d16
    e8. a,16 g'2
   
    
    
    }