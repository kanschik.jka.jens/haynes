\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Concerto"
  \time 2/4
  f8 c16 c c8 bes
  a f c' f
  g c, g' bes
  a g16 a f4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Aria. Adagio"
  \time 3/4
  d4 f8 e d cis
  d2 a4
  bes8 a g f' e f16 g
  cis,2
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. March"
  \time 2/2
  \partial 4
  a8 bes
  c4 c8 c c4 d8 e16 f
  e4 d8 e c4 c
  c2.
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Menuett altern"
  \time 3/4
    a4 bes8 c16 d
    g,8 f16 g
    a4 bes8 a g f
    d'4 e8 f16 g
    e4
}
