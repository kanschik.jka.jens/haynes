 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
    g4. a16 b c b c4 g'8
    a16 f e4 d8 e16 d c8 r
}
 