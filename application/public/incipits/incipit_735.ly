 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. [Allegro]"
  \time 2/2
    d8[ c16 bes] a g f e d4. a'16 a
    d8[ d16 d] d8 d  c8[ c16 c] f8 f16 f
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. o.Bez."
  \time 3/4
  a'4 f4. d8
  e4 a4. a,8
  d4 g e cis2 cis4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. o.Bez."
  \time 12/8
  \partial 8*7
    a8 d4 d8 d8 e d
    cis4 a8 f'4.~ f8 e d e d cis
    d4 g,8 g'4.~ g8 f e f e d cis4

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}