 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegretto grazioso"
  \time 4/4
  b2 b8 c c d
d4. c16 b a4 r
c2 c8 d e fis
g4. fis16 e d4 \grace e16 d8 c16 d
e4 d c\grace c16 b8 a16 b
b2 a4 r
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegretto con gusto"
  \time 2/4
  e2 g8 f e d
  \grace d16 c8 b c e
  g f e d
  \grace d16 c8 b c4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Menuetto grazioso"
  \time 3/4
  d2 \grace fis16 e8 d
  d4 fis g
  d2 \grace fis16 e8 d
  d4 fis g
}
