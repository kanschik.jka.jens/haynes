\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key a\minor
                      \tempo "Eglé Danse "
                      \time 3/4
                      \partial 4.
                      c8[ d8. e16]
                      e4 d8. c16 d8. b16
                      \grace b8 c4 \grace b8 a4 e'
                      g8.[ f16 e8. c16 d8. b16]
                      c4 r8 r16 e16[ a8. c16]
                      c4 b8. a16
                      b8. gis16 
                      \grace g8 a4 e8 r8 r4
                      % Voice 1
                  }
\new Staff { \clef "tremble" 
                     \key a\minor
                     \partial 4.
                     r8 r8 r8
                     \set Score.skipBars = ##t
                     R2.*3
                     r4 r8 c8 d e
                     e4 d8 c d b
                     \grace b8 c4 \grace b8 a4
                        % Voice 2
                  }
>>
}
