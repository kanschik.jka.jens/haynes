\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  c,8. c16 e8. c16 g'4 r16 e' d c
  b8 a g f f e r16 e' d c
  b8 a g f f e r4
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 4/4
    f4~ f16. c32 f16. a32 \grace a8 g16. f32 f16 g r4
    g4~ g16 e c bes' bes8 a r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Tempo di Menuet"
  \time 3/4
  c2 \times 2/3 { e8[ d c] } g4 f' e
  \times 2/3 { d8[ e f] } a,4 b
  \grace b4 c2.
}