\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Adagio"
  \time 3/2
  r2 a' gis a1 e2 fis4 e d2. cis4
  cis1
}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 2/4
  a8[ a, cis e]
  a4. b8
  cis b16 a gis8 a
  b4. e,8
  a[ e e e] 
  fis e r8
  
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Adagio"
  \time 3/4
  r4 cis4 fis
  e2 a4
  gis2 fis4
  e2 a4~
  a b8 a gis fis
  e2
  
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Giga. Allegro"
  \time 6/8
  \partial 8
  e8 cis b a e' fis gis
  a4 a,8 r4 e'8
  fis d fis e cis d 
  b4 e,8 r8
  
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  a'4. r16 a fis8. g16 a8. b16
  e,4. r16 a d, e fis32 g a b
  e,8. d16 cis4.
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  a4. a8 b8 a16 g
  a8 d,
  b' a16 g a8 d, b'16 a g b a g fis g
  e8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Adagio"
  \time 3/4
  r4 d4 d
  a' a4. g8
  fis4 fis4. fis8
  g4 b8 a g fis 
  e4 a2~
  ~
  a8[ fis d b] 
  
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Giga. Allegro"
  \time 6/8
  d8 e fis e fis g
  fis g a g a b
  a4 d,8 b4 g'8
  cis,4 
  
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 3/2
  d4 e fis2 d fis4 g a2 fis
  b a4 g fis e d cis d2.
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  d4. d8 a' a a a 
  fis fis fis e16 d e8 a, a' a
  a d, a' a a a, r8
  
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Adagio"
  \time 6/8
  d4.~d4 a8
  d e fis e a g
  fis4.~ fis4 a8
  d, e fis b,16 cis d e fis g
  cis,4.~ cis4
  
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Allegro"
  \time 3/8
  fis16 g a8 d,
  b' a16 g fis e
  e4 d8
  fis16 g a8 d,
  fis16 g a8 d,
  
  
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 3/2
  e2 b'2. a4 g2 gis1
  a4 e a2. g4
  fis1 
}

\relative c' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 2/4
  e4 e8. fis16
  g8.[ fis16 g8. a16]
  b4 e dis2
  e8.[ fis16 g8. fis16] e4. r16 
   
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Adagio"
  \time 3/4
  b8 a b4 a
  g8 fis g4 fis
  e e' dis e4. e8 fis4
  g8 fis g fis g fis
  e d e d e d 
  
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Giga. Allegro"
  \time 6/8
  \partial 8
  b8 g fis e e' fis g
  fis4. b,4 e8
  c b c a4 d8
  b a b g4 
  
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
  g'4. a8 b a16 g a8 d, g4. a8 b a16 g a8 d, e fis g b, c4 d g,4.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 3/4
  e4. fis16 g fis8 g16 a
  g4. a16 b a8 b16 c
  b4. a8 g8. fis16
  g8 a16 b fis4. e8
  e2.
  
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gavotta. Allegro"
  \time 2/4
  \partial 8
  r16 d16
  g4 a 
  b4. r16 fis16
  g8.[ a16 b8. c16]
  b4. r16
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga. Allegro"
  \time 6/8
  \partial 8
  d8 g4 g8 a4 a8
  b a g fis e d 
  g a b a g fis
  e fis g fis e d
  g4 r8
  
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Grave"
  \time 4/4
  a'4. g16 fis e4 r8 a
  b16 a g fis e8. d16 d4 r
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 2/4
  a4 g8. fis16
  e2
  a4 g8. fis16
  e2
  a8.[ g16 fis8. g16]
  a8.[ a,16 d8. g16] e4. r8
  
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Adagio"
  \time 3/4
  d4 fis a
  b2.
  d,4 fis a8 d,
  cis2 b8 a 
  b[ d d g e8. d16]
  cis2
  
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "4. Allegro"
  \time 3/8
  a8 d, a'
  a b16 a g fis
  e4.
  a8 d, a'
  a b16 a g fis
  e4.
  a8 g a
  b16 a g a b8
  e,16 fis g d fis g
  a4.
  
  
 
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 2.
  bes'32 a g f es d c bes bes4. d16 f
g8 f16 g32 f es8. d16 d4.
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  f8
  bes a16 g f8 es
  d4. f8 
  bes a16 g f8 es
  d4. f8
  bes bes, r8
  
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 4/4
  \partial 8
  d8 g32 a g fis g a g fis 
  g a g fis g a g fis
  g8 g, r8 a8
  bes'32 c bes a bes c bes a
  
}

\relative c''' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 6/8
  bes4. f
  d8 es f d c bes
  f'4. ~ f4 f8
  g f es f es d
  es d c d c bes
  
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  a'8 b16 a g8 fis fis g16 fis e8 d
  a' b16 a g8 fis32 g a16 e4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 3/4
  d8[ d d d] d fis16 e
  d8 d d d d d 
  fis[ fis fis fis] fis a16 g
  fis8 fis fis fis fis fis
  
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Adagio"
  \time 3/2
  fis1 b8 a g fis
  g2 fis b8 a g fis
  g1 fis2~
  ~fis
  
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Allegro"
  \time 2/4
  d4 a'
  fis8.[ e16 d8. e16]
  fis8.[ e16 fis8. g16]
  a4 a,
  
  
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Allegro"
  \time 3/8
  d16 fis a fis d fis
  e a cis, e a, cis
  d fis a fis d fis
  e8 a, a'
 
}


