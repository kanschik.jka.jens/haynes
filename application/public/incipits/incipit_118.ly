\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. [Allegro]"
  \time 2/2
  bes8[ f16 f] f8 f d'8[ bes16 bes] bes8 bes
  f'8[ es16 d] c8 d16 es  a,8 g16 a f4
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Aria. Andante"
  \time 3/4
  g4 bes8 g d'4~
  d4 a8 es' d c
  bes4 g
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Bourree"
  \time 2/2
 \partial 4
 bes4 bes'4 bes,4 bes'4 bes,4
 bes'4 bes8 bes bes4  bes,8 c
 d c bes c d4 d
 f f8 f f4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Menuet altern."
  \time 3/4
  bes4 d f bes2 r4
  bes,8 bes d d f f bes2 f4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "5. Trio"
  \time 3/4
  bes2.
  bes4 d8 c bes4 c2.~
  c4 es8 d c4
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "6. Gavotte"
  \time 4/8
  bes8. f16 f4
  d'8. bes16 bes4
  f'8 es16 d c8 d16 es
  a,8 g16 a  f4
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "7. Gigue"
  \time 6/8
   \partial 8
   f,8 bes f bes  bes f bes
   bes f bes bes4 bes
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "8. Passepiet"
  \time 3/8
 \partial 8
 f16 es
 d c d es d es
 c bes c d c es
}