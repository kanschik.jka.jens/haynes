 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
   \time 12/8
      r4 r8 r4 e8   f e f  d c d
      e4. a,
}
