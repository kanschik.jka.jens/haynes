\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 2/2
    \partial 4
    e8. e16
    e8 r  d r  cis r b r
    a8. cis16 b8. e16  a,4 a'8. gis16
    fis8 fis r e d[ d]
}


\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Minuetto. Allegro"
  \time 3/4 	

}


\relative c'' {
  \clef treble
  \key e\major
   \tempo "3. Romance"
  \time 4/4 	

}


\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Rondo"
  \time 4/4 	

}
