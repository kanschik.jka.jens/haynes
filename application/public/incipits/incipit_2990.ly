\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Aria"
  \time 4/4
  r4 fis8 g a b e, a
  fis d r4 r2
  r4 fis8 g a b e, a
  fis d16 d g8 fis b a r
}
