\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  \partial 8
  a8
  \times 2/3 {c16[ b a]} \times 2/3 {e'16[ d c]} a'8 e f e r a,
  \times 2/3 {c16[ b a]} \times 2/3 {e'16[ d c]} f8 a, gis a r
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \partial 8
     \set Score.skipBars = ##t
    r8
   R1*11
   r2 r4 r8 e
  \times 2/3 {c16[ b a]} \times 2/3 {e'16[ d c]} a'8 e 
  \times 2/3 {f16[ a g]} \times 2/3 {f16[ e d]}
  \times 2/3 {f16[ a g]} \times 2/3 {f16[ e d]} 
  \times 2/3 {e16[ g f]} \times 2/3 {e16[ d c]} 
  \times 2/3 {e16[ g f]} \times 2/3 {e16[ d c]} 
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Tutti]"
  \time 3/4
  c8 g g c c e
  r d d b b f'
  r e e c c bes'
  r a a f f c'
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Solo]"
  \time 3/4
  \set Score.skipBars = ##t
   R2.*9
   r4 r8 g'16 f e4
   d r8 f16 e d4
   c r8 e16 d c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Solo]"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro [Tutti]"
  \time 4/4
  a16 b c b a8 r  c16 d e d c8 r
  a' c, a a' gis32 a b8. d,32 e f8.
  b,32 c d8. gis,32 a b8. c8 e a4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro [Solo]"
  \time 4/4
  \set Score.skipBars = ##t
   R1*11
   a2.~ a16 c b a
   b2.~ b16 d c b
   c4~ c16 e d c  d4~ d16 f e d
   e8 a, r
}