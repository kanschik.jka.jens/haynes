 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef bass
  \key bes\major
   \tempo "1. Andante [Basso]"
  \time 3/2
    bes,2 bes,4 d es f
    g2 g,4 bes c d
    es2 es,4 d' c bes
    f'2 f, r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante [Oboe]"
  \time 3/2
   \set Score.skipBars = ##t
   R1.*7 
  r2 bes1
  bes1. bes bes~
  bes2 a c c1.~ c
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegro"
  \time 4/4
    r8 bes'16 a bes8 f g g16 f g8 d
    es c f f, bes bes16 c d es f g
    a,8 a16 bes c d e f g,4 e'
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Adagio"
  \time 4/4
     r4 r8 g'
     fis g c,16 es d c
     bes a g8 r bes' a d, g c,
     fis16 g a8 r
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Vivace"
  \time 3/4
      f8[ es d es f8. g16]
      f4 es8 d es4
      es8[ d c d es8. f16]
      es4 d8 c d4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "5. Presto"
  \time 4/4
  \partial 8
  bes8 a
  bes4 f c' f,
  f d' r d8 c
  d4 f, es' f,
  f f' r
}