\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Adagio [Tutti]"
  \time 3/4
    fis4 r8 fis fis8. g16
    \grace d8 cis4 r8 cis cis8. d16
    \grace d8 e4 r8
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Adagio [Oboe]"
  \time 2/4
   \set Score.skipBars = ##t
   R2.*4 
      b'2.~ b~
      b8. cis32 b  ais16 b cis ais \grace g8 fis8. e16
      d4.
}
