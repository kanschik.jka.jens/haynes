\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Balet " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
    \key c\major
   \tempo "1. Gaiment. Musette ou Viele"
  \time 2/4
 \partial 4
 e8 f g[ c, c c] 
 d c f4
 e8 d d4
 c16 g a b c d e f 
 g8[ c, c c] 
 d c f4

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement"
  \time 3/4
  g4 c,8. d16 d8. c32 d
  e4 e8. f16 f8. e32 f
  g4 f e 
  d8 c d2
  e4 \grace d8 c4 f
  \grace e8 d4 g \grace f8 e4
  a d,4. c8 c2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Vivement"
  \time 2/4
  c2 d 
  e4. f16 e
  d8[ d g g]
  g[ c, f f] f[ e d g] e[ c e f16 e]
  d8 g,

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Modérément"
  \time 2/2
  \partial 2
 es4  d8 f
 es4 \grace d8 c4 g' as
 g2 c4 b
 c g as8 g f es
 d4 c es d8 f
 es4 \grace d8 c4 

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Mouvement de chaconne"
  \time 3/4
  \partial 2
  c8 d e f16 e
  d4 g4. g8 g4 f e
  d8 c d e f d
  e4 c8 d e f16 e
  d4 g4. g8
  g4 f e

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe. Balet " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gaiment"
  \time 3/8
  c8 c16 d e f
  g4.
  f8 e16 f d8
  e g,4
  e'16 f e8 e
  e8 g,4
  f'16 g f8 f
  f e4

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Pesament"
  \time 2/2
  g2 g
  g4 e8 f g4 c
  g a f a
  g8 f e d c d e f
  
  

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Vite"
  \time 2/4
  c4 e
  d8[ d d d] 
  d4 f
  e8[ e e e]
  e4 g
  f8[ e d c]
  f e d16 e f d
  e8 g16 f e f e d
 

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Lentement"
  \time 3/4
  es8 d es c d b
  c4 g g'
  es d r4
  r4 r4 g4
  as8 g f es d c
  b4. a8 g4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Gaiment"
  \time 6/8
 \partial 4.
 c8. d16 c8
 g'4 g8 g4 g8
 g4. f4 f8
 f4 e8 d8. c16 d8
 e4 c8 

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIIe. Balet " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gaiment"
  \time 2/2
  \partial 2
  g4 c
  g c b c
  d2 e4 d
  e d g f8 e
  d4 g, 
  

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Modérément"
  \time 6/4
  c2. g4. f8 e4
  b2 c4 d2 e4
  f4. g8 e4 c' f, a
  d, b' r4

}
  
  \relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gaiment"
  \time 2/4
  e4 e
  d16 c d e d c d e
  f4 f
  e16 d e f e d e f
  g4 g
  f16 e f g f e f g
  e8[ g f e]
  d4 g,

}
  
  \relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Doucement "
  \time 2/2
  \partial 2
  es4 d8 es
  c4 d8 es f es d es
  c d es f g4 f8 g
  es4 f8 g as g f g
  es2

}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Gaiment "
  \time 3/8
  c4.
  e16 f g d e c
  d8 c4
  d16 e f a g f 
  e8 d16 e c8
  e16 f g d e c
  d8 c4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IV.e Balet " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Rondement"
  \time 3/4
  \partial 4
  g4 b a8 g fis a
  g4 d e
  d c8 b a c
  b4 \grace a8 g4 g'
  b a8 g fis a
  g4 d e
  d c4. b16 c
  b2

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Gaiment"
  \time 2/4
  g4. d8
  d[ g d g]
  a16 g a b a b g a
  b8 b b a16 g
  a8 d,4 

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Légerement"
  \time 6/8
  g4. b4 b8
  a g a d4 g,8
  g4. d'4 g,8
  g c b a g a b4 g8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Doucement"
  \time 3/2
  b4. c8 b4. b8 c4. b16 c
  d2. g4 fis4. e8
  d4. f8 e4. d8 c4. e8
  d2. c4 b2

}



\relative c''' {
  \clef treble
  \key g\major
   \tempo "5. Mouvement de chaconne"
  \time 2/2
 g4 b,8 a g4 g
 d' d e e8 fis16 g
 fis4 d'2 c4~ c b a4. g16 a
 b4

}


