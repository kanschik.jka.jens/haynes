\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. "
  \time 4/4

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. "
  \time 4/4

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. "
  \time 3/4
  \partial 4

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. "
  \time 6/8

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Largo"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Corrente. Allegro"
  \time 3/4
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Largo"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 4/4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4

}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
  \time 3/4

}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "4. "
  \time 6/8
  \partial 8

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Andante"
  \time 3/4

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Andante"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Andante"
  \time 3/4

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Allegro"
  \time 6/8

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Largo"
  \time 3/2

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Andante"
  \time 3/4

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Presto"
  \time 2/4

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante"
  \time 3/4

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Largo"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Andante"
  \time 3/8

}

