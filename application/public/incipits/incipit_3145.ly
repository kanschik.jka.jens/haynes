\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Recitativ (Tenor): Wie können wir, grossmächtigster August [BWV 215/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key b\minor
                      \tempo "Recitativ"
                      \time 4/4
                      % Voice 1
                      r16 fis16 fis32 e fis16 d8 r8 r2
                      r2 r16 fis16 fis32 e fis16 dis8 r8
                
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key b\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                    r16 d16 d32 cis d16 b8 r8 r2
                      r2 r16 dis16 dis32 cis dis16 b8 r8
                       
                        
                
                  
                  
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Tenor"
                     \key b\minor
                     \set Score.skipBars = ##t
                        % Voice 3
                    r4 r8 fis8 b b b cis
                    d d fis d b4 r8
                       
                        
                
                  }
>>
}


