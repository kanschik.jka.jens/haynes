\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "Allegro"
  \time 3/4
  c4 g2  c d4
  e f g  e f8 e d4
}
