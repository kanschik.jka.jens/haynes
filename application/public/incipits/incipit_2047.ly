 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Prelùde. Lentement"
  \time 3/4
    c4 d e
    f2. g4 a bes
    g4. f8 e4
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allemande"
  \time 2/2
  c4 f4. e16 d e4
  f r c4. bes8 a4 a'~ a8 g16 f g8 f16 e
  f4 f4. e16 d e8 g
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Air. Lentement"
  \time 2/2
  f2. e4 a2 g f e
  f8. g16 g4 a2
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Menuett"
  \time 3/4
    c4 d e f e8 d c4
    f e8 d c16 d e8
    f2.
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "5. Trio"
  \time 2/2
   c4 r8 c d c d bes
   c4 r8 c d c d bes
   c4. bes16 a g4 r8
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "6. Lentement - Gay"
  \time 2/2
  r4 f f f
  e e8 d c4 bes
  a f f f
}