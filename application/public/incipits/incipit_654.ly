
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Duo pour la flute, Hautbois e Clarinette"
  \time 2/2
  \partial 8.
g16 a b c2 e
g4 f e d c2 g2
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Si des Galans de la ville, Ariettes"
  \time 2/2
   \partial 2
b4 e g fis  e dis 
e b c b 
a g fis e 
b'2 

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. La Furstemberg"
  \time 2/2
   \partial 4
bes8 c d4 d d d 
es2 c
d4 c8 bes a4 d
\grace c8 bes4 a8 bes g a bes c 
d4 


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. ah que l'amour est chose Jolie"
  \time 2/2
   \partial 2
f4 e8 f
d4 d  c8 d c b 
b4 a

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Marche des Gardes Francoises"
  \time 2/2
   \partial 4
a4 d4. e8 d4 a
d a8. fis16 d'4 fis
e4. fis8 e4 a,
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "6.  Ariette Tous les Hommes sont bons"
  \time 2/2
   \partial 2
g4. g8
bes2 bes4 a
g2 \grace es8 d4. d8
d2 \grace es8 d4 c8 bes a2

}

