 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 4/4
  \partial 8
  \times 2/3 { fis16[ g a]}
  \times 2/3 { a16[ cis, d]} d4 \times 2/3 { fis16[ g a]}
  \times 2/3 { b16[ cis, d]} d4 \times 2/3 { g16[ a b]}
  a16 fis \times 2/3 { e16[ fis g]}
    fis16[ d]

  
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Presto"
  \time 2/2
  a'1 g
  \grace g8 fis2. \grace { g16[ a]} b4
  a d, \grace fis8 e4 d8 cis
  d2
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Menuetto"
  \time 3/4
  fis8 g a4 a a2.
  b8 g fis4 e
  d8 cis d2
}

