 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 3/8
    d4.~ d8 c b
    a d fis, g a b
    c4.
    b8 c d
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 2/2
  \partial 4
  d4 g,8 a b c d e fis g
  \grace fis8 e4 d r e8 c
  b4 c8 a g4 a8 fis
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Tempo di Menuetto"
  \time 3/4
  g'2. d
  \times 2/3 { e8[ d c]} g'4 e
  e4 d r
  c2. b a g2 r4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}