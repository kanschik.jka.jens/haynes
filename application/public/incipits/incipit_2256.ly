\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
       \override TupletBracket #'stencil = ##f

  d,8
  g g g g  \times 2/3 { b16 a g} g4 \times 2/3 { g'16 fis e}
  d8 b d16 c b a b8. c16 d8
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Andante quasi Adagio"
  \time 3/4
       \override TupletBracket #'stencil = ##f
    fis,2 g4
    a a \times 2/3 { d8 cis b}
    a4 fis g 
    a2 \times 2/3 { d8 cis b}
    a4 b cis
    
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
     \override TupletBracket #'stencil = ##f

  \time 6/8
      \partial 8
      d,8
      g g16 a b c  d8. e16 d8
      e8 e16 g fis a g4
}