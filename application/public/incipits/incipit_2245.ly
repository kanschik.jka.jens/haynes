\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  c8 g'2 \grace { a32 g fis g}  a8 g f e
  d c c c c 4 r8 r
  e d  c b
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Romance"
  \time 2/2
    f,2 g4 a
    c8 bes bes bes bes4 \grace { c32 bes a bes} c8 bes
    a c c c  c c d16 c bes a
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Allegro"
  \time 6/8
  \partial 8
    g'8
    g f e  e d c
    g' g g g4 c8
    c b a  a g fis
}