\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
  es,4 es8. es16 es4 es
  es4. g8 es4 r8 es
  f4 f8. f16 f4 f
  f4. as8 f4 r8 as
}
