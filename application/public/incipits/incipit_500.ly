\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Largo"
  \time 4/4
  es4 f r8 r16 f d4
  es4 r8 r16 es c4 d8. es16
  d8. c16 b2
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio"
  \time 4/4
  es4~ es16. d64 es f16 d   es bes' es,8~  es16. d64 es f16 d
  es bes c es
  \grace bes8 as4 g r8 g'
  as32 g f16 as8~ as16 c bes d, es d es4 es8
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro"
  \time 3/8
  g'8 es c
  d32 c b16 c4
  as8 g f'
  es16 c d b c8
  as g f'
  es d16 es c8
}



