 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Affettuoso"
  \time 4/4
    bes8 c d es f2~
    f8 g16. f32 es8 f16. es32 d4~ d8. c16
    c4 r r2
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegro"
  \time 4/4
    r8 f, bes d c4 es
    d8 c16 bes g'8 f es d c bes
    a4 f r2
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Affetuoso"
  \time 3/4
    d4 g a
    bes2 a4
    g4. bes8 a g
    fis4 d r
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Vivace"
  \time 2/4
      r8 f bes, c d4. es8
      f4. g8 f16 es f4 g8
      f16 es d f  es d c es
      d es f4 g8
}