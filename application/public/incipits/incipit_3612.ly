\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Largo"
  \time 4/4
  es4 d es r
  c bes8. as16 g4 r
  g' f8. es16 d4 es8. c16
  b4 c d r8
}



\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro [Tutti]"
  \time 3/4
  c16 d es f g8 c, as' b,
  c16 d es f g8 c, as' b,
  c16 d es f g8 c, as' c,
  b a g f es d
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*29 
  c8 g g as as g
  c g g as as g
  g' c, c es d c b4. a8 g4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Adagio"
  \time 4/4
  bes16 g bes g bes g bes g  c as c as c as c as
  bes g bes g  es' bes g es bes' g es8
}
  
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. 1me Menuet"
  \time 3/4
  es8 f g4 g g2.
  g,4 c d es d8 es c4
}


  