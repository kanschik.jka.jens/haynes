 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "o.Bez."
  \time 12/8
  r4. bes4 c8 d es f bes,4 c8 d es f  
}

