 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio di molto"
  \time 4/4
  \partial 8
    d16. cis32
    d8 a4 d8 \grace cis8 b b~ b16 b d b
    b8 a16. g32 a4
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Allegretto"
  \time 4/4
    d8. cis32 b a8 b \grace a4 gis4. a8
    fis8. g32 a g8 fis \grace g16 fis8 e
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Allegretto"
  \time 3/4
    d4. a8 fis' e
    e4. a,4 g' fis
    fis8. a16 g4 \grace fis8 e4
    \grace d4 cis2 d4
}
