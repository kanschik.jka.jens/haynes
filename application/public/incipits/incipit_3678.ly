\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allergo"
  \time 3/4
  f,8 f r a r c
  r bes r g e'8. d32 e
  f8 f r a, r  c
}
  
