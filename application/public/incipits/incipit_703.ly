
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\minor
  \tempo "1. Largo"
  \time 4/4
  g4 g g~g16 g a bes 
  a8. a16 a8 bes16 c bes8[ g]
 }

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allemande Allegro"
  \time 4/4
  \partial 8
 d16 d bes8 g d'8. d16 d4~ d16 d e fis g8 a16 bes a8. g16 fis8 d4 g16 d es d c bes a4 g
 
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Sarabande"
  \time 3/4
  d4 g4. a8 fis2 g4 es d4. c8 bes4. a8 g4 
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Giga Allegro"
  \time 6/8
  g4 bes8 d4 g8
 fis4 d8 fis e fis g4 d8 a4 bes8 c d c bes 4 
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
 g4. a16 g f8 g16 a g8. f16 e4. f16 e d8 e16 f e8. d16 c4~c16 e d c [b8 g]
 
 
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4
  r8 c8 c c g'4. g8 e e e f16 e d8 g, g'4~g8 c, f2 e4 d2 
 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Sarabande Largo"
  \time 3/4
  g4 e4. a8 g4. f8 e4 g a8 g f e d2 
 
 
}

\relative c' {
  \clef treble
  \key c\major
   \tempo "4. Aria Allegro"
  \time 3/4
  e8  g c e g e b d g d b d c g' d e d c
 
  
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IX "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Largo"
  \time 4/4
 a4~ a16 c b a f'8. f16 e d c b [c8 a]
 
 
 
}
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allemana Allegro"
  \time 4/4
  \partial 8
 e8 a b c b a gis a e c d e b [c a] 
 
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Aria Largo "
  \time 3/4
  e4 a4. g8 f2 e4 d c4. b8 cis4. b8 a4
 
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Giga Allegro"
  \time 6/8
  \partial 8
 e8 a b a gis fis gis a gis fis e4.~ e4 a8 gis fis gis a gis fis e4.~ e4
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata X "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key f\major
   \tempo "1. Adagio e Cantabile"
  \time 4/4
 \partial 8
 c8 f g16 a g8 a16 bes a8 g16 f e8 f16 g f8 g16 a g8 a16 bes a8 g16 f bes8 a16 g c4. bes16 a g8
 
 
 
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro "
  \time 4/4
  \set Score.skipBars = ##t
   R1*3  r8 f8 f f e e f16 e d c d8 g, g'4. c,8 f4~f8 e e d16 c d4.
 
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Sarabande "
  \time 3/4
 a4. bes8 c4 f, f'4. e8 d4. e8 f4 e f g a4. g8 a [bes] g2
 
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Gavotta Allegro "
  \time 2/4
  c4 f d4. d8 c8. d16 bes8. c16 a4 f a'4. bes8 g4. a8 f8. e16 f8. g16 e4 c 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XI "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Grave"
  \time 4/4
 d4. d8 e4. e8 d4. e8 c2 b4. b8 cis4 d~d cis d a' b4. b8 a4. b8 g2
 
 
 
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Allemande Allegro "
  \time 4/4
  \partial 16
 g16 g4~g16 d e f e d c b a b c d b8 e a, d g, c fis, b e, fis g2
 
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Ciaccona Vivace "
  \time 3/4
 r4 d4. d8 d4. d8 e [fis] g4 g2
a4 a2 
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Gavotta Allegro "
  \time 2/4
  c4 f d4. d8 c8. d16 bes8. c16 a4 f a'4. bes8 g4. a8 f8. e16 f8. g16 e4 c 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XII "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. Largo"
  \time 4/4
 a4 gis8 fis e4. d8 cis d cis b a2~a~a4. b8 cis a d b cis a fis' d e cis fis gis a2~a4
 
 
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allemande Allegro "
  \time 4/4
  a4. r16 a16 b4. r16 b16 cis8. cis16 b8. a16 gis8. e16 e'4~e dis e8. b16 e4~e8. cis16 cis8. cis16 d4.
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Aria Adagio "
  \time 3/4
 r4 e4 a gis2 a4 fis b,4. e8 cis4. b8 a4
 
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "4. Gavotta "
  \time 2/4
  a4 a gis e fis e8 d cis4. d8 e cis d e fis g fis e d e d cis b4 a
 
}


