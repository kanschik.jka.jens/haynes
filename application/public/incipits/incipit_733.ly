\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andantino"
  \time 4/4
  \partial 4
  g4
  c4. g8 e g c e
  \grace e8 d4. b8 g b d f e4. c8 g c e g f4.
}

