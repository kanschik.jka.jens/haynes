\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Choralbearbeitung (Tenor): Der du bist dem Vater gleich [BWV 36/6]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key b\minor
                      \tempo "Choral. Allegro molto"
                      \time 3/4
                      % Voice 1
                  b8 fis fis gis16 ais b ais b cis
                  d cis d e d cis b cis d cis d e
                  fis8[ b,]
                     
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key b\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                 R2. b8 fis fis gis16 ais b ais b cis
                  d cis d e d fis e d e d cis b
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key b\minor
   \tempo "Choral. Allegro molto"
  \time 3/4
     \set Score.skipBars = ##t
     R2.*8 b2. b a d cis2 b4 cis2. b
}