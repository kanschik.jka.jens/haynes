 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #""
                     \key d\major
                      \tempo "o.Bez."
                      \time 3/4
                      % Voice 1
                      R2. R R
                      r4 b b b2 cis8 b16 cis
                      d4 cis8. d16 b4
                      cis2 b8. cis16
                      a4 cis8 d e4 fis r r
}
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #""
                     \key d\major
                      % Voice 2
                      R2. R r4 a, a
                      a2.~ a4 gis2 a2 a4~ a gis2 a4 a8 b cis4
                      a r r
                  }
>>
}

