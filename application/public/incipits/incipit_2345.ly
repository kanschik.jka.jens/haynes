\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 4
  a4
  d16 e fis4. e16 fis g4.
  \grace g8 fis4 e8 fis
}
