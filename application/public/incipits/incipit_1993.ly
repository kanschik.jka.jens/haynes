 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

 

 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 3/4
  r4 d2~
  d2.~
  d4 g8 f es d
  es f es d c bes
  a4 c2~ c4
}



\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/2
  g16[ a bes c] d e fis g a[ g fis e] d c bes a
  bes8 g d' g fis d r 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Adagio"
  \time 2/2
  r4 bes2~ bes8 c16 d
  es4. d16 c d8 bes r4
  r8 g8. f16 es d c[ as'8.] g16 f es
  d4 f
  }


\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 2/2
  \partial 4
  g8 a
  bes4 g bes d
  g2. f4
  bes, es2 d4
  g, c2 bes4 es, es'8 d c bes a g 
  fis4. e8 d4
}
