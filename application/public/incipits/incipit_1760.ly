\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Aria o.Bez."
  \time 3/4
  r4 r f,
  bes d8 c bes4
  bes d8 c bes4
  c d es
  d r
}
