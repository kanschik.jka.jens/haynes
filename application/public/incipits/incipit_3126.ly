\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Von den Strikken meiner Sünden [BWV 245/11]" 
    }
    \vspace #1.5
}
 
 
\relative c''' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key d\minor
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                       \set Score.skipBars = ##t
     R2.*1 r4 bes4 e,~
     ~e d8 f a4~
     a8 g bes g e d
     cis 
                    
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key d\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                r4 a'4 d,~
                ~d cis8 e g4~
                ~g8 f a f d c
                bes4 d g~
                ~g8 
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key d\minor
   \tempo "o. Bez."
  \time 3/4
     \set Score.skipBars = ##t
     R2.*8 r4 a4 g16 f e32 f d16
     d4 cis8 e g4~
     ~
     g8 a16 bes a8 f d c
     bes d e g16 f g4~g8
}