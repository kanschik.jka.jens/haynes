 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

 

 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
   \override TupletBracket #'stencil = ##f
 \partial 8
 g8
 d'8 d~ \times 2/3 { d16[ b c]} \times 2/3 { d16[ e fis]}
 g8 g, g' g,
 \grace { c16 d}  e8 d 
  \times 2/3 { d16[ b c]} \times 2/3 { d16[ e fis]}
 g8 g, g' g,
 
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \override TupletBracket #'stencil = ##f
  \partial 8
  r8
  \set Score.skipBars = ##t
    R1*21
    r2 r4 r8 g
    d'4 d \grace {b16 c} d4~
    \times 2/3 { d16[ b c]} \times 2/3 { d16[ e fis]}
     g8 g g g \grace {fis16 g} a8 g
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  \partial 8
  g8 
  c c g'32 e16. f32 d16. c16 e \grace e16 d c c16 e \grace e16 d c 
  g8 g d'32 b16. c32 a16.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Solo]"
  \time 4/4
  \partial 8
  r8
    \set Score.skipBars = ##t
    R1*8
    r4 r8 g c4 \grace a'16 g16[ f32 e] \grace g16 f16 e32 d
    c16 e \grace e16 d c c16 e \grace e16 d c 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro [Tutti]"
  \time 3/4
    g8 d'16 b g8 g g g
    g e' d c b a
    g8 d'16 b g8 g g g
    g g fis e d c
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*37
  r4 r d
  g8 b16 a g8 g g g
  g\grace { a16 b} c8 b a g
  g
}
 


