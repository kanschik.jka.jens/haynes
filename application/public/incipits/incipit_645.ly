\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 2/2
\partial 4
  bes8 bes
  c4. c8 c4 d8 es
  d4. c8 bes c d es
  f4. es16 d
  c4 f d es c
}
