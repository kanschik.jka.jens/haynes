\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro assai [Tutti]"
  \time 4/4
  \partial 16
  f,16
  bes8 bes bes4. d16 c es d c bes
  a8 a a2 a16 bes c d
  es8 es es4. g16 f f es d c
  bes8 bes bes2
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro assai [Solo]"
  \time 4/4
  \partial 16
  r16
   \set Score.skipBars = ##t
   R1*58 
   f1~ f~ f~ f2. f8 fis
   g g g2 bes8 g
   f d bes2 bes8 d d c bes2 c8 a bes4 r 
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 2/4
  f16 c c8 r16 c c c
  f d d8 r16 d d d
  c a' f d c8 bes
  bes32 a d c c8
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Rondo. Allegretto"
  \time 2/4
  \partial 8
  d16 es
  f8. es16 d es f g
  \grace f16 es8 d16 c c8 r16 f
  bes8. d16 c bes a g
  \grace g16 f8 f f
}