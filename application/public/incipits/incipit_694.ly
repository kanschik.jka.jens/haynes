\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Eerste Suite [Libro I]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. De Nieuwe Studente Mars"
  \time 2/2
  \partial 4
  \times 2/3 { a8[ b cis] }
  d2 fis8 e d cis d cis b a g fis e d
  fis'2

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Tweede Suite [Libro I]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Menuetto Primo"
  \time 3/4
  b2 c8 d
  \grace b4 a4 b8 c
  \grace g8 fis2 \times 2/3 { g8[ a b] }
  a8 d, e fis g a b2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Derde Suite [Libro I]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Mars"
  \time 2/2
  \partial 4
  c4
  f g8 e f a g e f4. e16 d
  c4 a bes g,
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "4de Suite [Libro II]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Menuet"
  \time 3/4
  a2 a8 b16 cis
  \grace cis8 b2 b8 cis16 d
  \grace d8 cis2 d4 \grace {cis16[ d]}
  e4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "5de Suite [Libro II] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo  "1. De Folies van Holland"
  \time 3/4
  g4 bes d
  fis, a c es d c \grace c8 bes4. a8 g4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "6de Suite [Libro II] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Mars"
  \time 2/2
    \partial 4
    \times 2/3 { bes8[ c d] }
    es4 g8 es d f bes, d
    es4 g,8 es d f bes, d es4
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "7de Suite [Libro III] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Menuet"
  \time 3/4
  g8 a b c d g,
  \grace b8 a4 g8 fis g4
  a8 b c d e d
  \grace d8 c2 b4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "8de Suite [Libro III] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Het Liere Deuntje"
  \time 6/8
  \partial 2
  a8 d4 d8
  d e fis fis e d
  e4 a,8 e'4 e8
  e fis g g fis e fis4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite 9 [Libro III] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. "
  \time 2/2
  \partial 4
  c4
  f f8 f f4 e 
  f f,8 f f4 f'
  \times 2/3 { e8[ f e]}
  \times 2/3 { d8[ e d]}
  \times 2/3 { c8[ d c]}
  \times 2/3 { bes8[ c bes]}
}

