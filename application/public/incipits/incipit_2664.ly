 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Grave"
  \time 2/2
  d1 f2 f8 f es8. d16 es2 es8 es d8. c16
  a'2 a8 bes, g'8. f16
  es d d c  c[ bes bes a]
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Allegro"
  \time 2/2
  g'8 c, r4 d8 g, r4
  es'8 c16 d es8 d16 es f8 bes, r4
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Andante [Tutti]"
  \time 3/2
  r2 bes4 g c bes
  bes2 r r
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Andante [Solo]"
  \time 3/2
  R1. r2 es bes g es r r es' g4 f
  f1. f2 bes f d bes r
}
\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Vivace"
  \time 3/4
  r8 g c d16 es f8 es16 d
  es8 c g'4 f g8[ g,]
}