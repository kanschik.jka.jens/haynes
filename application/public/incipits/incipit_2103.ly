\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  bes2 bes bes2. f8 d'
  d c c4 \grace d8 c8 bes c d
  c2 bes4
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Rondo. Allegro moderato"
  \time 2/4
  bes16 bes' a g   g f es d
  \grace f8 es16 d es f d8
  d16 c d es d c c bes
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro assai"
  \time 3/8
  bes8 bes bes \grace bes8 d4 c8
  bes bes bes \grace d8 f4 es8
  d d d \grace bes8 bes'4
}
  
