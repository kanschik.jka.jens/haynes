 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  e,8
  a cis~ cis16 b a gis  a gis a4 e'8
  fis16 d e a, d8. e16
  \grace d8 cis4 r8
}


\relative c'' {
  \clef treble
  \key a\major	
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  a8
  cis a gis a
  e b'4 cis16 d
  cis d e8 e cis cis b b r
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "3. Largo"
  \time 3/4
  c4 b a
  b8 d c b a gis
  a4 r r
  R2.
  c4 d e d8 f f e e d
  e4 d c
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "4. Allegro"
  \time 3/4
    a8 b cis4 cis
    cis4 b a
    gis8 a b4 b b2.
    a4 a8 gis a b cis4 fis2
    e8 d e cis d4 cis d2
}