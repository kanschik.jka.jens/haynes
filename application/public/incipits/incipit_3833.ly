 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro non tanto"
  \time 4/4
  f8 f4 es d bes'8~
  bes bes,4 bes8 bes a c4
  d16 f bes, d f, bes d, f bes d f bes d,4
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Adagio"
  \time 4/4
  e8. e16 f8. g32 a a4 g8 f
  e8. f32 g f8 d e4 r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 3/4
  f,8 a16 g f8 a c f
  e4 e2
  f16 e f g a8 f c a bes4 a r
}
