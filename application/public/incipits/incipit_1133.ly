\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/2
    g4 g g, r16 d'' e fis
    g8 d b g d' d, r16 d e fis
    g8 c e g fis d r d,
} 



\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 2/2
    d,2 d8[ fis] e16 fis d8
    g2 g8[ b] a16 b g8
fis2 fis8[ a] c16 b c8
b2
} 


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 2/4
      g4 d' g8 fis16 e  d c b a
      g8 d' e d
      c4 b
} 