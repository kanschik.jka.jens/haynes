 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. Presto"
  \time 2/4
    f16 g as g f8 c
    des c bes as
    f'16 g as g f8 c des c
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Siciliana"
  \time 12/8
  g'4. g8. c16 b8 c4 g8 f4 g8
  es8. d16 c8 r r es d8. b16 g8 g8. b16 d8
  e4. e8. f16 g8 as8. bes16 c8 as8. g16 f e
}

\relative c'' {
  \clef treble
  \key f\minor	
   \tempo "3. Tempo di menuetto"
  \time 3/8
  r8 c f
  e e16 f g8
  f16 g as8 f
  g16 f e d c8
  e f c g' c, g'
  f16 g a8 g16 f
  e4 r8
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Menuett"
  \time 3/4
  \partial 8
  
}