 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 3/4
  \partial 4.
    a8 b cis
    d16 cis d fis d cis d a' d, cis d b'
    d, cis d fis d cis d a' d d, cis d e8 fis g b a g fis e d4. 
}


\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "2. Largo"
  \time 4/4
  fis16. e32 d16. cis32 b8 b b8. ais16 b8 b
  cis16
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Allegro"
  \time 3/4
  fis,8 d' a' g a fis, g d' b'2
fis,8 d' a' g a fis, e cis' g'2

}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4. Menuett"
  \time 3/4
    d8 fis e d g e d fis e d b' a g4 a8 g fis e fis4 e8 fis d4
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "5. Allegro"
  \time 6/8
  a'4 fis16 g a8 d, d' cis b4 a8 g4 fis8
}