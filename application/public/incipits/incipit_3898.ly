\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Moderato"
  \time 2/4
    f8. a32 g f16 c f c
    f8. a32 g f8 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Rondo Amoroso"
  \time 3/4
    a'8 c  d c bes a
    g4. a16 bes a4
    \grace c8 bes4 bes8 g a f
    \grace a8 g4 f8 e d c
}
