\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
  d8
  g4 g8 g
  \grace a16 g8 fis16 e fis g a b
  c4 c8 c
  \grace d16 c8 b16 c d8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Larghetto"
  \time 3/4
  d4 g f~ f es d
  d8. c32 d es4 d
  \grace d4 c2 bes4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 2/2
  g2 c b e
  d4 c b a g c b e
  d c b a g
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro assai"
  \time 4/4
  r8 e c a gis b e, gis
  a2 b c8 e a, e' gis, b e, gis
  a2 b
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Andantino"
  \time 2/4
  \partial 8
  e,8
  e'8. fis16 e8 d
  \grace d8 cis4. fis16 gis
  a8 e4 d8
  \grace d8 cis4.
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Presto"
  \time 3/8
  a16 gis a b c d
  e8 e e
  c e a,
  gis b e,
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Allegro"
  \time 4/4
  b'4 fis d16 fis b, fis' e g cis, e
  d fis b, d  e g cis, e  d fis b, d cis e ais, cis
  b cis d e fis8 fis fis2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Larghetto alla Siciliano"
  \time 6/8
  g'8. fis16 e8 d8. e16 c8
  \grace c8 b4.~ b4 a8
  g4 e'8 d8. e16 c8
  \grace c8 b4.~ b4
}
  
  \relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. In Tempo di Minuetto ma Grazioso"
  \time 3/4
  fis8. e32 fis g4 fis
  e e8. d32 e fis4
  d d8. cis32 d e4
  \grace d4 cis2 b4
}
  


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. /Allegro"
  \time 4/4
  c'4 a8 f e16 c d e f g a b
  c8. g16 a8 f e16 c d e f g a b c8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Affettuoso"
  \time 3/4
  f4 \grace f8 e4. r16 f
  \grace e8 d4 c r
  c e f \grace c8 bes4 a r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto"
  \time 2/4
  e4~ \times 2/3 { e8[ f g]}
  \times 2/3 { f8[ e f]} \times 2/3 { d8[ e f]}
  \times 2/3 { e8[ d e]} \times 2/3 { c8[ d e]}
  \times 2/3 { d8[ c d]} \times 2/3 { b8[ c d]}
  c4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  d4~ d16. a32 a8 e'4~ e16. a,32 a8
  fis'16 a g b a4 a, g'
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Mesto"
  \time 3/8
  c8 d e e16. d64 e f8 r
  r16 f e gis a c
  \grace e,16 d8 c r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro di moto"
  \time 2/4
  r8 d' b d
  a d g, e'
  fis, d d d d8. cis32 d e8 a, 
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Grave"
  \time 2/2
  e4. fis32 g a b c8.[ c16] b8. a16
  g8. b16 e,4 r8 r16 e dis8. fis16
  b,4.
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Alla breve ma Presto"
  \time 2/2
  b1 b2 b4 b g8 b c b dis, b' c b
  e, b' c b fis b c b
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Cantabile"
  \time 4/4
  c8. b32 a g8 g a g r c16. e32
  d16. f32 e16. g32 \grace g8 f4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Presto"
  \time 3/8
  e8 c' b a g fis
  g dis e a, b4
  e,
}

