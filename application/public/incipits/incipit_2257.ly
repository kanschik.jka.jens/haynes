\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro maestoso"
  \time 4/4
    g'2~ g16 a g fis g8 a
    b g d b g4 r
    d2 c'' 
    b16 g8. a16 fis8.
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 2/2
    g'2. a8 g
    \grace { f16 g a} g8 f f4 r8 f g g
    \grace { e16 f g} f8 e e4 r8 g g b,
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Rondeau. Allegro moderato"
     \override TupletBracket #'stencil = ##f

  \time 2/4
    g'8 g \grace g8 fis8 e16 d
    g8 g a4
    g8 g \grace a8 g8 fis16 g
    \grace g8 fis8 e16 d d8 d
}