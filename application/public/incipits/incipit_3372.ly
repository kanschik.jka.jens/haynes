 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante"
  \time 6/8
    f,8 g f  f bes f
    f g f \grace g8 f4.
    bes8 c bes  bes c bes
    bes d16 c bes c \grace bes8 a4.
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Fantasia"
  \time 3/4
   bes8. d16 f4 bes
   g16 f es8   \grace a,8 bes2
   bes8. d16 f4 bes
   g16 f es8   \grace a,8 bes2
   
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Gavotta"
  \time 2/2
  \partial 2
  d8 es f a,
  \grace a8 bes4. c8 c d f a,
  \grace a8 bes4. f'8 g f f es
  es d d c c d es g,
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Aria"
  \time 2/2
  \partial 8
  r8
  r d c bes r c bes a
  d c16 bes c a bes c bes4 r8 c
  es16 c c8 es16 a, a8 c16 es, es8
}