\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/2
  es4. f16 g32 es
  d8.[ es16] es8 bes
  c[ bes']  \times 2/3 { as8[ g f]  }  \times 2/3 { g8[ f es]  } r8 bes
}
