\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Maestoso con fuoco [Tutti]"
  \time 4/4
  c4 c, b8 b' r4
  a4 a, g8 g' r4
  f8 a16. c32 f16. e64 f a16. f32
  \times 4/6 { g16[ f e d c b]  } c4~
  c16 b d e f8 f, f e r16
  
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*18
    c2 g'4 g,16.[ g'32 \times 2/3 { b16 a g] }
    c2 b4 e,,16.[ e'32 \times 2/3 { g16 f e] }
    a4 g16 f e d g8 b, \grace b8 c4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio [Tutti]"
  \time 3/4
    g2 \grace c8 b8 a16 g
    g'4 fis8 r r8. b,16
    e4 d8 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*16
   g4. b16 a \grace c8 b8 a16 g
   g'4 fis8 b,16 c  \grace e8 d8 c16 b
   e8. fis32 g
   d8 g,16 a  \grace c8 b8 a16 a g4 fis16
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Tutti]"
  \time 2/4
  c4 g'8. e16 c4 c
  \grace d8 c32 b c8. c'4
  r8. c,16 c'8. e,16
  d4 d'
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Tutti]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*45
  c4 g'8. e16 c4 c
  \grace d8 c32 b c8. c'4~
  \times 2/3 { c8[ b a] } \times 2/3 { g8[ f e] }
  e8 d d4
}