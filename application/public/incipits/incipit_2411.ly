\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  <e, g c>4 r <g b d> r
  <g c e> r r16 c16 e g f e d c
  b8. b16 b8. a32 b c8. c16 c8. b32 c
  d4 r r2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andantino"
  \time 3/4
    a2 \times 2/3 { c8[ bes g] }
    f2 g4
    a c8. a16 \times 2/3 { g8[ bes d] }
    f,4. g16 f e4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Vivace"
  \time 2/4
  e8 e16. e32 e8. d16
  d c c c c8 r
  \grace { d16[ e]} f8 f16. f32 f8. e16
  e d d d d8 r
}
