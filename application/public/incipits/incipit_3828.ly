\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Molto Adagio [Tutti]"
  \time 4/4
  c,4 e gis a
  fis g b, r
  c cis d8 a' g f
  e2 f16 e d4 a'8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro Spirituoso [Solo]"
  \time 4/4
  \partial 4.
  g c d
  e2~ e8[ c \grace {d16[ c] } b8 c]
  cis4 d r8 g, \grace b16 a8 g
  f'2~ f8 d c d
  dis4 e r8
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio assai [Tutti]"
  \time 4/4
    a4 c,8. b32 a e'8 <e b' e>8 r c16. d32
    e8.[ g16 e8 d] c16.[ d64 e] b8 r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio assai [Solo]"
  \time 4/4
  \set Score.skipBars = ##t
  R1*3 
  r2 r4 r8 c16. d32
  e8.[ f16 e8 d] \grace { c16[ d] } e8[ a, a8. b16]
  c8.[ d16 c8 a] d4.
  
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo con spirito"
  \time 6/8
  \partial 8
  c32 d e f
  g8 g16 fis g8  g8 g16 fis g8  
  g8 g16 fis g8  g e c'
  g4 e8 c4 e8 d4 b8 g
}