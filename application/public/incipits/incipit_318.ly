 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\major
                      \tempo "1. [Allegro]"
                      \time 2/2
                      % Voice 1
                      R1
                      r2 d,8 d16 d d d d d
                      d2 r2
                  }
\new Staff { \clef "bass" 
                     \key d\major
                         % Voice 2
                        d,8 fis fis16[ e d8] a'8[ cis] cis16 b a8
                        d[ fis,16 g] a8 a, d2
                  }
>>
}


\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\minor
                     \tempo "2."
                     \time 3/4
                      % Voice 1
                      R2. R R
                      r4 a'2~ a4 g bes~ bes 
                      a8 g f g16 e f2 r4
       }
                  
\new Staff { \clef "treble" 
                     \key d\minor
                    
                        % Voice 2
                        a,4 a bes bes a4. a8
                        d d4. e8 cis2 r4
                  }
>>
}


\relative c'' {
  \clef treble
  \key d\major	
  \tempo "3."
  \time 6/8
   a'4 a8 fis a fis
   d e fis e fis g
   fis g a d,4 d8
}

