\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 2/2
     es2. d4
     c bes c d
     \grace f16 es8 d16 c bes4 r bes8 g'
     \grace g4 f2 \times 2/3 {f8 es d} \times 2/3 {c8 bes as}
     g8. as16 bes4
}

