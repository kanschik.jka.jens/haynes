\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria I " 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key f\major
                      \tempo "1. o.Bez."
                      \time 4/4
                        f,4. g8 a g a bes
                        c4. d8 c d16 a  bes8 c16 g
                        a8 f r4 r2
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key f\major
                        % Voice 2
                     r2 f4. g8
                     a g a bes g f f e
                     f4 r4 r2
                  }
>>
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria II " 
    }
    \vspace #1.5
}
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key c\major
                      \tempo "1. o.Bez."
                      \time 12/8
                      \partial 8
                      g8
                      c g c  b a b  c4 g8 g4 d'8
                      e f e  d c d  e f e d4
                      
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key c\major
                        % Voice 2
                       r8
                       r2. r4. r8 r g,
                       c g c  b a b  c4 g8 g4
                  }
>>
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria III " 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key f\major
                      \tempo "1. o.Bez."
                      \time 2/2
                      r2 r8 f, e e
                      f16[ a g f]  g bes a g a4 r r1
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key f\major
                        % Voice 2
                       r1 r8 f e e f16[ a g f] g bes a g
                       a4 r r2
                  }
>>
}

