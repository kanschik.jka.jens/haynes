 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegretto"
  \time 2/4
  \partial 8
  g8 d' d4 e32 d e fis
  g16 fis fis4 g16 e
  d8 b'16 b, b8 c
  d8. e16 d8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Rondeau. Allegro"
  \time 3/8
  g8 b d
  d8. e16 d8 b d g
  g8. a16 g8 b4 a8
  g8 d c
  b16 a32 b c8 b
  \grace b8 a4 r8
}