 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 3/4
    g'4 d b g r8 b' b b
    b8. c16 a8 c c c
    c8. d16
    b8 d d d
    d4. c8 b a
}
 