\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Marsch I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "o.Bez. "
  \time 4/4
    \partial 8
    f8
    bes bes, d f bes bes, d4~
    d8 c16 d16 es8 es es d r
   
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Marsch II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "o.Bez."
  \time 4/4
   es,4 es8. es16 bes'8. bes16 c8. d16
   es2 \grace f8 es4 \grace d8 c4

}



\pageBreak

\markuplist {
    \fontsize #3
    "Marsch III " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "o.Bez."
  \time 4/4
  \partial 8
  f,8
  bes d d16 c bes a bes8 d16 f f8 es
  es16 d c bes d c bes a bes8 f
  
}


