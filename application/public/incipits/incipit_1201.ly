
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante"
  \time 4/4
 bes   4. g'16 f f8 d16. c32 bes8 g'
 f8 d16. c32 bes8 es d bes g a 
 bes16 a bes g es' d es c d4 r4

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/2
   f2 bes,4 r8 bes8 c c c8. bes32 c d8 d, r8 d'8
  es es es8. d32 es f8 f, r8 

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Larghetto"
  \time 3/4
   r4 d4 es8 d d2 r4 R2.
 r8 es8 d c bes a bes8. a16 g4 r4 

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 2/2
   r4 r8 d16 es f8 bes, bes bes c bes bes bes c bes bes bes
  es16. f32 g8 f4 r8 c8 d bes 
 c16. d32 es8 d4. 

}

