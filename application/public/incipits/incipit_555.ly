
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
  \tempo "1. Andantino"
  \time 2/4
  \partial 8 
  c8 g'16 f e4 f8 e16 d d4 e8
 \grace g16 f8 e16 d a c b d c8. d32 e d8
 
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Presto"
  \time 6/8
  g8 e f g a b c d e g,4 r8
 c'8 b a g f e d f e d4 r8
 
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
 c4 c8. c16 \grace a'8 g4 f8 e 
 d c c2 cis4 
 d4 d8. d16 \grace g8 f4 e8d c b b2.
 
 
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro Gratioso"
  \time 4/4
  \partial 4
 g4~ g8 c b a g4 g g8 e' e2 d8 c c b b a a g g f f e e2  
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto III "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 4/4
 \partial 4
e4 d8 cis cis cis cis a cis d 
 cis2 b4 cis
 b8 a gis fis eis fis a cis a2
 
 
 
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Tempo di Minuetto"
  \time 3/4
a2 \grace fis'16 e8 d16 cis
 b2 d16 b d b 
gis8 r8 a8 r8 b8 r8
 \grace d16 cis2 b4
  
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto IV "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante con moto"
  \time 2/4
 g16 b d4 \grace e8 d16 c32 b
 dis16 e e e e8 r8
 
 
 
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Presto"
  \time 2/4
  g8 r8 a8 r8 
 b4. c16 b
 a8 r8 g8 r8 
fis4 r4  
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto V "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
 c4 c2 \times 2/3 {d8 e f}
 c4 c2 \times 2/3 {d8 e f}
 \grace f8 e4 d8 c c4 c
 c4. a8 c4 r4
 
 
 
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Tempo di Minuetto"
  \time 3/4
  c8 [d bes g] a r8
 c8 [d bes g] a r8
 f'8 r8 e8 r8 d8 r8 c8 
 
}
