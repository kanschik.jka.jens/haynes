\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                    \set Staff.instrumentName = #"Flauto"
                     \key g\minor
                      \tempo "1. o.Bez."
                      \time 4/4
                      r4 bes'8 a a2~
                      a32 c bes a d16 a32 fis g4~ g32 f e f g a bes c f,8. g16
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                    \set Staff.instrumentName = #"Hautbois"
                     \key g\minor
                        % Voice 2
                       r4 g es'4. d16 es
                       d4 r8 g16 es c4~ c32 es d c bes16 c32 a
                  }
\new Staff { \clef "french" 
                    \set Staff.instrumentName = #"Viola d'Amour"
                     \key g\minor
                        % Voice 2
                       r4 a16 d cis d g,8~ g32 f es f g bes a g f16 g32 es
                       f4~ f32 es d es f16 g32 es es4. c16 d
                  }                  
>>

}

