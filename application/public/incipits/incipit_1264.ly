 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key g\major
   \tempo ""
  \time 12/8
  g'4. g8. a16 fis8 g4.  d8[ b]
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Dafne"
  \key g\major
   \tempo ""
  \time 12/8
     \set Score.skipBars = ##t
     R1.*9
     d4.~ d4 e16 c d4.~ d4 g8
     g4 e8 c4 c8 c4. b
}