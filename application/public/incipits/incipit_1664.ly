\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Trio in c] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Prelude. Grave"
  \time 2/2
  c4 d es f
  g4. f8 f4. es16 d
  es2 g4. g8
  g4
  
  

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Sarabande"
  \time 3/4
  c4 es4. f8
  d4 d es
  c d f
  b,2 b4
  c es4. f8
  d4. es8 f8 d
  es f f4. g8 g2.
  

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Rigaudon"
  \time 2/2
  es2 f \grace {es16[ f]}
  g2. as4
  g f es d
  es4. f8 es4 d
  
  

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Gigue"
  \time 6/8
  \partial 16*3
  c16 d8
  es8. es16 f8 g4 g8
  g8. f16 es8 d8. d16 es8
  f4 f8 f8. es16 d8
  es4 c8
  

}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "5. Gavotte"
  \time 2/2
  \partial 2
  g8 f es d
  c4. c8 f es d c
  b4 g es'8 d c bes
  a4 bes a4. g8 g2.
  
 

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "6. Plainte. Grave"
  \time 2/2
  es4 c as'4. as8
  d,4. d8 g2
  c, f4. f8
  f4 g8 f es4. d8
  es4. f16 g
  f4. es8
  d2
  
  
  

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "7. Rondeau. Gay"
  \time 3/4
 r4 c4 d
 es f g
 f4. g8 es4
 d2 d4 es4. d8
 c4 f d 
 es d4. c8 c2 r4
  
  
  

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "8. Passacaille"
  \time 3/4
  es8 f es d c4
  g'2 g8 g
  g as f4. es16 f
  g8 as g f es d
  
  
  
  

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Trio in G] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prelude. Gay"
  \time 2/2
  \partial 2
  r8 g8 g a
  b b b c d d e fis
  g1~g8 g g a fis fis fis fis
  g g g g a fis g a
  d, d d g e fis e8. d16 d2 r2
  
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Trio in d] "
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Prelude"
  \time 2/2
  r4 d4 a4. bes8
  g4. g8 g4 f8 e
  f4 f' f4. g8
  e4. e8 e4 d8 cis 
  d2. d4 d2 cis4. d8
  cis2 
  


}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Trio in D] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Prelude"
  \time 3/4
  d8 a d e fis g
  fis e fis g a b
  a4. g8 fis e
  fis cis d fis e d
  cis b cis d e cis
  fis4 d8 d e fis
  b,4
  
  

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Trio in g] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Prelude"
  \time 3/4
  d4 es4. a8
  bes4 bes4. a8
  a4 a bes
  g g8 g a a 
  fis4. g8 a4
  d, g4. d8 
  es4. f8 g4 c,
  

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Trio in C] " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Prelude"
  \time 2/2
  g1 f e d 
  f e 
  r4 g4 f e 
  d e d4. c8
  c1
  

}



