\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro con spirito [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*49 c2 e4. f8 g4~g16 f e d c4 r4 c4. f16 g a8 a a  a c4. a8 g4 r8
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Solo]"
  \time 2/4
  \partial 16
   c'16 c4 a8 r16 c16 c4 g8 r16 c16 \grace g8 f16. e32 f4 fis8 g4~ g32[ f e d]
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo [Solo]"
  \time 2/4
  g'8~g32 a g f e8 r16 g16 f8~f32 g f e d8 r16 f16 \grace f16 e8 d16 c a8. d16 \grace c16 b8. a16 g8 r8
}
