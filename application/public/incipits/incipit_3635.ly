\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Affetuoso"
  \time 6/8
  d8 f, e f4 g8
  a bes a a4 g8
  c f, e f4 g8 a bes a g4.
}
