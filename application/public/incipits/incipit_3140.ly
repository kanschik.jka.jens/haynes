\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Verbirgt mein Hirte sich zu lange [BWV 104/3]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key b\minor
                      \tempo "o. Bez."
                      \time 4/4
                      \partial 8
                      % Voice 1
                  fis8 
                  b, cis cis d d e e fis
                  fis gis16 ais b cis d b cis b ais gis fis8 cis'
                  d16 e fis cis
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key b\minor
                     \set Score.skipBars = ##t
                        % Voice 2
            r8 r4 r8 fis,8 b, cis cis d
            d16 cis b cis d e fis gis ais b cis8 cis fis,
            fis2~ fis8
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key b\minor
   \tempo "o. Bez."
  \time 4/4
  \partial 8
     \set Score.skipBars = ##t
    r8 R1*5 r2 r4 r8 fis8
    b, cis cis d d e e fis 
    fis g16 fis e d cis d32 b ais8 gis16 ais fis8 r8
}