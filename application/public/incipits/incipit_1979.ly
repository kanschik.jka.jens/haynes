 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 4/4
  \partial 4
      r4
      r4 a8. a16 a2~
      a4. e'8 f e f cis
      d4. a'8 bes a bes fis
      g4. f8 es8 d es d
      cis[ a]
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Allegro"
  \time 2/2
  r4 a' e g f bes4. e,8 a4~
  a8 d, g4. f8 e a
  f e e8. d16 d
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Grave"
  \time 4/4
  r4 f d bes'8 bes,
  es4~
  es16 g f es d8 bes g'4~ g8 c16 e, f4. bes16 d, e8. f16 f1
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "4. Allgero"
  \time 6/8
    d8 e f g a bes
    a4. g
    f8 e d e d cis
    a' g f g f e
}