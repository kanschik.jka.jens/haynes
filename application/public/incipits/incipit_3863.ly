\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata 2-VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Affetuoso"
  \time 12/8	
  \partial 8
  g8 d'4 d8 d4 d8 d8. e16 d8 d4 fis8
  \grace fis8 g4.~ g8. fis16 e8 d4 c8 b4 a8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4	
  g4 b8. c16 d4 r8 d
  g g g g fis e16 fis d8 g
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Grave"
  \time 4/4		
   e4~ e16. c32 b16. e32 fis4~ fis16. c32 b16. fis'32
   g16. fis32 e8 r16
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 3/8
  g16 a b8 a g d' fis, g16 a b8 a g d' fis,
}

