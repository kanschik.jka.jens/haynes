\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Ia " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Intrada.Risoluto e moderato"
  \time 3/4
  d8 cis16 b a8 g fis e
  d8. d16 fis8. fis16 a8. a16
  d8. e16 e4. d16 e
  fis4 \grace e8 d4 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 2/4
   d8 a d16 fis e d e8 a, e'16 g fis e
   fis8 e16 fis g fis e d
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Andante"
  \time 6/4
  \partial 2.
   d4. e8 d4
   d fis a d cis8 b a g
   fis4. e8 d4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Allegro"
  \time 3/8
  d16 e e8. d32 e
  fis8 fis16 g a8
  d,16 e e8. d32 e
  fis16 e fis e d8
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Seconda " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 4/4
  \partial 16
  g'16
  g4~ \times 2/3 {g16 d c}  \times 2/3 {b16 a g} e'8 e e e
  e8 d~ \times 2/3 {d16 g, a}  \times 2/3 {b16 a g} c8 c c c
  c[ b]
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
   g16 a b c d g, e'8
   e d4 g8 \grace { fis16[ g]}
   a16 g fis g a8 c,
   b a16 b g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gracioso"
  \time 3/8
   b16 c c8. b32 c
   d8 g, e'
   d c4
   b16 g a fis g8
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 6/8
  \partial 8
  d8
  g b g d g d
  b16 c d c b a
  g8 b d
  g d g g4.
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata terza " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Andante"
  \time 2/4
  \partial 8
  f16 g a8 a a a
  a g4 e16 f
  g8 g g g
  g f4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro"
  \time 3/8
   d16 e f g a bes
   a8 d d,
   d16 bes' a g f e
   \grace g8 f8 e16 f d8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio"
  \time 3/4
   a'4 a4. g16 a
   bes8 a bes a bes a
   g16 e f g g4. f16 g
   a8 g a g a g
   f16 d e f f4.
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Vivace"
  \time 2/4
  \partial 16
  a16
  d8. f16 e8. g16
  f8. e16 d8. e16
  f8. a16 g8. bes16
  a8.
}
  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata quarta " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 6/8
  f8 c16 bes a g f8 f' a
  g bes,16 a g f e8 g' bes
  a16 g a bes c8 a f f,
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro moderato"
  \time 2/4
   f4 c a8. g16 f4
   f'8 c c f g c, c g'
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "3. Sarabande"
  \time 3/4
   f4 g as
   g \grace c,8 c4 as'8 g
   f g16 as g4. f8 e2.
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Tempo di minuetto"
  \time 3/8
  f8 a16 g f f
  f8 a16 g f f
  g8 bes16 a g g
  a8 g16 a f8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata quinta" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Gracioso"
  \time 3/4
  e4 g fis e b' a
  \times 2/3 { g8[ a b]}
  \times 2/3 { c8[ b a]}
  \times 2/3 { g8[ fis e]}
  dis4 e r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 4/4
 \partial 2
   e16 fis e dis  e fis e dis
   e8 b g e  g'16 a g fis g a g fis
   g a b8 g8 e
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "3. Sarabande"
  \time 3/4
  gis'4 a b
  e,4. fis8 gis4
  a \grace gis8 fis4 b
  gis4. fis8 e4
 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Minuetto I"
  \time 3/4
  e8 b cis dis e fis
  g dis e fis g a
  b fis g a b c a2 g4
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata sesta " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gracioso"
  \time 3/4
  \partial 4
  g4
  c8 d d4. c16 d
  e8 f f4. e16 f
  g4 c g
}	

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 2/4
   g'16 f e d c8 a'
   a g4 e8
   f16 d b d g,8 f'
   e8. d16 c8 d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gracioso"
  \time 3/8
  e16 f f8. e32 f
  g8 c, a' a g f
  g c, a' a g f
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Giga. Allegro"
  \time 6/8
  \partial 8
  g8
  c d c  c d c
  c d c  c e g
  c b a g a f
  e4 d8 c4
 
}