\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Grazioso"
  \time 2/4
  g'4 f
  e16 c' c b b a a g 
  \grace a16 g8 f16 e \grace g16 f8 e16 d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Minuet"
  \time 3/8
  g'4 e16 f32 g
  g16 c c b b a g4 e16 f32 g 
  g16 f f e e d
  c4 d8 c4 d8
  
}

