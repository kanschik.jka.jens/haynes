\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 3/4
  \set Score.skipBars = ##t
  r4 d2~ d2 \grace c8 bes4
  a g'2
  g4 a8[ g f e]
  f[ a a g g f]
  
}
