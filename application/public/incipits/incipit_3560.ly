\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "Aria 3"
  \time 3/4
    r4 r d
    g8. fis16 g8. bes16 a8 g
    fis e d4 fis g r e f r
}
