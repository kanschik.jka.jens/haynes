 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4
      b8. c32 d  c16 b a g fis8. e16 d8 b'16 c
      d8 b16 c d8 e a,4 r
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 4/4
    d8 b16 a g8 g' fis d r4
    d8 b16 a g8 g' fis d r fis
    g16 a fis a  g a fis a  g8 g, r
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Largo"
  \time 3/4
     d2.~ d
     a4 c8 b a g a4 c8 b a g
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Allegro"
  \time 6/8
    g8 g' g fis16 g fis e d8
    R2.
    b8 e e e4.
    cis8 fis fis fis4.
    d8 g g g4.
}