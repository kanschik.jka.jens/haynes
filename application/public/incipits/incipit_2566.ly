 
\version "2.16.1"
 
#(set-global-staff-size 14)
 


\relative c'' {
  \clef treble
  \key e\major	
   \tempo "Trainé"
  \time 4/4
  \partial 4
    e8 dis
    e dis e dis e dis e dis
    e2 b4 cis
    b gis a fis b2 e,4
}