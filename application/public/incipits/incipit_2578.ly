 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Lentement"
  \time 2/2
    c4. g'8 f4 \grace es8 d4
    es4. es8 d4 \grace c8 b4
    c4 g
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Vite"
  \time 4/4
  c2 b4 c d es f d
  es f8 es d4 es
  f g as f g
  
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Lentement avec douceur"
  \time 2/2
  g2 c es
  \grace d8 d2 d4. es8 f4. d8
  es2 es4. d8 c4. bes8 a2 b2. c4
  b2
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Gai"
  \time 6/4
    r4 r4 c es d c
    g'2 g4 g d es
    f g f f es d
    es2 es4 es f g
    b,2 b4 b2 c4 g2. g
}