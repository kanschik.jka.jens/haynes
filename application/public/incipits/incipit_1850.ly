\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
    c4~ c16 c d e
    d4~ d16 d e f
    e4 c g'2
    r4 g, c8. d16 c8. d16
    e8. f16 e8. f16 g8 e16 f g8 f16 e
}
