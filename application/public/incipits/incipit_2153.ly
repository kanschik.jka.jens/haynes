\version "2.16.1"

#(set-global-staff-size 14)

  \relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Affetuoso"
  \time 4/4
  d4. g16 f es8 g, c4.
  bes16 a bes4. es16 d c bes a g
  fis8 g32 a bes16
}

  \relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Presto"
  \time 4/4
   r4 d g a
   bes2 a2.
   g8 fis g4 es
   d2. d4
   es c8 d es4 d8 c d4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Larghetto"
  \time 6/8
  d8. es16 f8 es8. d16 c8
  d4 f,8 bes4.~ bes a bes4 f8 bes4.~ bes a bes
}

  \relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Vivace"
  \time 3/8
    g16 bes a g fis g
    a c bes a g a
    bes d c bes a bes
    c es d c bes c
    d8[ g,]
}

