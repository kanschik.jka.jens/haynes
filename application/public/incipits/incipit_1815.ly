\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro maestoso"
  \time 4/4
    c,2. e4
    g4. e8 c4 r
    f4. e8 d4. c8
    b4 a g r
}
