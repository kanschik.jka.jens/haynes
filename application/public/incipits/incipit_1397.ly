\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. [Allegro]"
  \time 2/2
  es4 es2 g16 f es d
  es4 bes as g
  es'4 es2 g16 f es d
  es4 bes as g
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Largo"
  \time 3/4
    d4 c8 bes a g
    \grace g8 fis2 g4
    a8. bes16 c4 bes
    \grace bes8 a2 g4
    es'2.
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro"
  \time 6/8
     es,4. bes' g es'
     d8 f es d c bes
     \grace { f'16 g} as2.
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allegretto"
  \time 2/2
  \partial 4
    b4 g' g2 fis4
    \grace fis8 e8 dis e4 r b~
    \times 2/3 { b8[ c d]} \times 2/3 { c8[ b a]} \grace g4 fis2
    g4 e' b2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Largo"
  \time 3/4
     g4 d4. f8
     e4 d c b8 d g b a c~
     c b4 a g8
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Allegro ma non troppo"
  \time 3/4
     g8. fis16 g8. a16 b4
     c b2~ b4 \grace b8 a8 g a4
     \grace b8 a4 g r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. [Allegro]"
  \time 2/2
    f,4 a8 f  c' a f' c
    a' c, c2 bes4
    a8. f16 f8. e32 f c'8 a f' c
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "2. Largo"
  \time 3/4
   c4 f g
   as8. g32 f e4. f16 g
   f4 es des
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro Assai"
  \time 2/4
  \partial 8
     c8 f4. c8
     g'4. c,8
     a'4. c,8
     bes'4. a16 bes
     c8 c4 c8
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegretto"
  \time 2/2
    f,16 a c8 c2 bes16 c d8 d2 c4 bes16 c d8
    c4 g16 a bes8 a4 e16 f g8 f4 c r2
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "2. Largo"
  \time 3/4
     f,16 c' bes c  as c g bes  f as e g
     f as g as  f as es ges des f c es
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 3/4
     f8. e32 f c4 r8 c
     \grace e8 d2 c4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. [Allegro]"
  \time 2/2
  g4. f16 es d8. c16 bes8. a16
  g8 g g g  g16 a bes a g8 g'
  a, a a a   a16 bes c bes a8 g'
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Molto adagio"
  \time 4/4
   bes4 bes'2 g4
   \grace a8 g4 f r8. f,16 bes8. d16
   d4 c8. d16 es8. d32 es f8[ es]
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 3/4
     g'4 d bes' a \grace g8 fis2
     g8. f32 es d4 bes'
     a \grace g8 fis2 g4
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegretto"
  \time 2/2
  b4 b2 c8 d c4 c2 d8 e
  d4. c16 d e4 g,
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Largo"
  \time 3/4
   r8 g' fis e dis e
   \grace d8 c4 b r
   fis'4 fis4. g16 a
   g fis e8 e4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro assai"
  \time 2/4
     g'4 d d e8 d d g r d
     d4 e8 d d4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. [Allegro] [Tutti]"
  \time 2/2
   \partial 4
   g,8. a16 bes4 d g a bes8. d16 g2 d4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. [Allegro] [Solo]"
  \time 2/2
  \set Score.skipBars = ##t
    \partial 4
    r4
    R1*42
      d2 g4 g, \grace bes8 a4 g8 fis g4 d'
      \grace d8 es2. f4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Arioso con affetto"
  \time 3/4
     bes4 g as 
     bes es \grace d8 c4
     bes4 \grace bes4 as2
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Presto [Tutti]"
  \time 2/4
     r8 g'g16 fis g8 es d4 c bes8 g16 fis g8 es
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Presto [Solo]"
  \time 2/4
  \set Score.skipBars = ##t
   R2*42
   d2 bes' a8 g fis e
   \grace d4 c2
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 2/2
   \override TupletNumber #'stencil = ##f
  c,4. \times 4/6 {d32[ e f g a b]  } c8. c16 c8. c16
  c16 b c d e4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 2/2
  \set Score.skipBars = ##t
  R1*42
  c4. d16 e d4. e16 f
  e f g8 g2 a8 g
  f e d c
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Affetuoso"
  \time 3/4
     g'2.~ g4 f es
     d8. es32 f es8 d c b
     \grace b16 c8. g16 g4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro assai"
  \time 2/4
     c,4 e8 c g' c, e g
     c c e c
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro [Tutti]"
  \time 2/2
    c,16 d es f g8[ g] g g g g
    c g g g g f es d 
    c4 as'
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro [Solo]"
  \time 2/2
  \set Score.skipBars = ##t
  R1*41
  g'2 f
  \grace f16 es8 d es f g4 as
  g2 f
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio poco andante"
  \time 4/4
     g'2 f16 e8. d16 c8.
     b8. c32 d c4 bes2
     a8[ a']
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro assai"
  \time 3/4
  c8. c16 c8. c16 c8. c16 
  c8. d16 d4. c16 d
  e8. e16 e8. e16 e8. e16 
 
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro assai"
  \time 3/4
  \set Score.skipBars = ##t
  R2.*37
  g'2. f
  es8 g f es d c
  d f es d c b
  \grace d8 c4 b c~ c
 
}