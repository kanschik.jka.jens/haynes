\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
    <g, e' c' g'>4 r r2
    <g e' c' g'>4 r r2
    <g g' d' f>4 r r2
    <g g' d' f>4 r r2
    e''4 f16 e d c g'4
}

