\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro [Tutti]"
  \time 2/2
  \partial 16
  d16
  a'4. \times 2/3 { g16 f e}  d8 d d d
  d cis cis4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   \partial 16
   r16
   R1*52
   r2 r4 r8. d16
   a'4. r16 bes \grace bes8 a4. r16 bes
   bes8 a a4 r
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Tutti]"
  \time 2/4
    f8. e32 d c8 c
    c2
    f8. e32 d c8 c
    c2
    f4 f16 f e f
    e d d8 r4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
    R2*12
    f8. e32 d  c8 c
    c4~ c16  c' c e,
    f8. e32 d c8 c
    c4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Rondo. Allegro"
  \time 6/8
    d4 f8 f e d
    a' bes bes bes4 r8
    a4 d,8 f e d d cis a a4 r8
}