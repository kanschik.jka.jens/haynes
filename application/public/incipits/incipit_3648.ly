 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  f8
  d c16 d bes8 g' a,4 bes8 f'
  a,8. bes16 bes8. a32 bes c4. f8
  f, es' f, d' f, c' f, f
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Adagio"
  \time 3/4
  d8 g, es' d es d16 c d8 g, es' d es d16 c d4 g, bes'
  bes8 a4 g a8
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Allegro"
  \time 2/4
     bes4 f'
     \grace es8 d4. c8
     bes16 c d es f8 g
     a,8. g16 f4
}
