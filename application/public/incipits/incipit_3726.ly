 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key as\major
   \tempo "1. Adagio"
  \time 3/4
    c8. d32 es d4 c
    bes as r
    as'8 as4 g16 f es8 des
    des4 c r8
}


\relative c'' {
  \clef treble
  \key as\major	
   \tempo "2. Allegro"
  \time 2/2
  as'2~ as8 g16 f es8 des
  des2 c8 f es16 f f8
  f4 es r8 as16 g as8 as
  g4 c, r8
}

\relative c'' {
  \clef treble
  \key as\major	
   \tempo "3. Allegretto"
  \time 3/8
  es4 des8
  \grace des4 c4.
  as'8 g f \grace es8 des4 c8
  \grace g'16 f16 e e4~
  e16 f f as \grace bes32 as16 g32 f
  f16 es es4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}