\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 4/4
  d4 r16 a b cis  d e d a  d e d a
  fis g fis d fis g fis d d' a d fis e d cis d
  e8 a, r16
}
