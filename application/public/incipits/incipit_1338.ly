\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Largo"
  \time 4/4
  g'8 es16. d32 c8 g'  as f16. es32 d8 f
  g es16. d32 c16
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Vivace"
  \time 2/2
   g'8 c, r16 es[ d c] d8[ g,] r16 g'as g
   f[ f g as]
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Adagio"
  \time 3/4
     r4 c f
     es c as'~ as g c, d2
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Allegro"
  \time 6/8
  c8 d c b g b c4. d
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 4/4
  d4 r16 f e d  e8 a, a'16 g f e
  f e d e
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 3/4
  \partial 4
     d8 d4 r8 a d f
     e8 cis16 d  e8 g f e
     f d16 e
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio"
  \time 3/2
   r2 a d e a, e'
   f2. e4 d2
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro"
  \time 6/8
  d4 e8 f e d
  a'4 a,8 a'4 a,8
  a'4 a,8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 2/2
  g,1~ g4. c8 b4. a16 g
  c4~ c16 d e8
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro"
  \time 2/2
     c16[ d es f]  g as f g  es[ g f g]  c, es d c
     b d c d
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Adagio"
  \time 2/2
   r2 es4. es8
   d4. c16 bes a4. a8 b4. a16 g
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Allegro"
  \time 6/8
   \set Score.skipBars = ##t
   R2.*3 
   r8 r g'16 g
   g8 g, g'16 g
   es8 c es  d es16 d c d
   b8
}

  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Largo"
  \time 4/4
    a4 e8 a c4 a8 c
    e4. a,8 b c d c16 b
    c8[ a]
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro"
  \time 4/4
   e4 d16 c b a gis4 a
   d c8 b c4 a
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Largo"
  \time 3/2
     c4. d8 e4  d g,4. g' a4 g2
     f4 e2
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Allegro"
  \time 3/4
    a8 c16 b c8 e16 d e4
    a,8 e' a gis16 fis gis8. a16 a8
}
