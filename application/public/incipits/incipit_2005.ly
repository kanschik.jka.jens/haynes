\version "2.16.1"
         #(set-global-staff-size 14)

         
         \relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 3/2
    r2 g g
    g bes bes
    bes as g
    c c d
    b g es as f2. es4
}
         