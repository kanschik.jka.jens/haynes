 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro maestoso"
  \time 4/4
    c'4. b16 a g4 e8 g
    \grace g8 f4 e r2
    e4. g16 f e4 c8 e
    \grace e8 d4 c r2
   }
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Menuett"
  \time 3/4
     \partial 4
     g'4
     c r g8 e
     f4 r b8 f
     d2 e8 f
     \grace f8 e4.
}

 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Adagio"
  \time 4/4
  \partial 8
  f16 a
  c8. d32 bes
  \grace bes16 a8. d16
  c a f8 f16 a a, bes
}

 \relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 2/4
    g'4. c16 a
    g4. c16 a
    \grace g4 f2
    e4 r
   }
