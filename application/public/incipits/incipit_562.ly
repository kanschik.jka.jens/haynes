 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. All. [Tutti]"
  \time 3/4
    bes2 \grace g'8 f8 es16 d
    c8 r d r es r
    a, r bes r c r
    \grace es8 d2 c4
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. All. [Solo]"
  \time 3/4
     \set Score.skipBars = ##t
     R2.*87 
     f2 \grace es8 d16 c d es
     f4. g8 a bes
     bes a g f es d
     c4. d16 es d4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Adagio cantabile [Tutti]"
  \time 3/4
    \times 2/3 { d8[ es d]} \times 2/3 { cis8[ d d]} \times 2/3 { d8[ d d]}
    \times 2/3 { d8[ es d]} \times 2/3 { cis8[ d d]} \times 2/3 { d8[ d d]}
    \times 2/3 { bes'8[ a g]} \times 2/3 { g8[ f es]} \times 2/3 { es8[ d c]}
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Adagio cantabile [Solo]"
  \time 3/4
      \set Score.skipBars = ##t
     R2.*40
     g'2 \grace c8 bes8 a16 g
     f8 es es2
     es16 d cis d a'4. c,8
     c16 d a bes bes2
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Allegro con gratioso"
  \time 6/8
  \partial 8
  f,8
  f f f  f f f
  bes bes16 d c es d8 c bes
  a g f
}
