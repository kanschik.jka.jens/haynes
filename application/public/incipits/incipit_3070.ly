\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Kreuz und Krone sind verbunden [BWV 12/4]" 
    }
    \vspace #1.5
}
\transpose c d{
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\minor
   \tempo "o. Bez."
   \time 4/4
   r4 c16 d32 es c16 d32 es f,8. d'16 d es32 f g,16 f'
   es f32 d c8 
   
  
}  
}
\transpose c d{
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key c\minor
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
    R1*6 r4 g16 as32 bes as16 g b8. c16 f, g32 as g16 f
    es8
}
}


