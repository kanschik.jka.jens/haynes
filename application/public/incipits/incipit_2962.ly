 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 4
  \grace c16 b16 a b c
  d4 b c d
  e \grace f16 e d e f e4 e16 d c b
  a4 a b c c b r
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Adagio"
  \time 3/4
  r4 g4 g8. bes32 a
  g4 fis fis
  r f8 d g f
  \grace f8 e4 e
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Presto"
  \time 6/8
  \partial 8
  d8
  d e d d c b
  c4 r8  r r c
  c d c c b a
  b4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}