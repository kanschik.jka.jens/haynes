\version "2.16.1"
         #(set-global-staff-size 14)

         
         \relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 3/2
    r2 g2. g4 a2 d2. d4
    g,2 c1~ c4 d bes a bes c
    a g a bes c d
    bes2 d g~ g fis1
}
         