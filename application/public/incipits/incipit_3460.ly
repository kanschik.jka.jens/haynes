\version "2.16.1"
         #(set-global-staff-size 14)


\relative c'' {
  \clef treble  
  \key f \major
    \time 4/4
    \tempo "1. Allegro "
    f8 c g' c, a' f r16 g a bes
    a8 f r16 g a bes a8 f r16

}

\relative c'' {
  \clef treble  
  \key f \major
    \time 3/4
    \tempo "2. Adagio "
    f4 e8 (f) 
    d4 a'8 e c e a, c f (g) e f d4 a'2 a,4 
}

\relative c'' {
  \clef treble  
  \key f \major
    \time 2/4
    \tempo "3. Presto  "
   f4 c f g a g f c f g a8 bes c4 bes8 (a) g f e4 c 
}