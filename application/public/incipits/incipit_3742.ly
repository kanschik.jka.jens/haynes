 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. Largo"
  \time 3/4
    f4 c des
    des c as'
    as g8 des' bes g
    \grace f8 es8. d16 c4
}
 
 
\relative c'' {
  \clef treble
  \key f\minor
   \tempo "2. Allegretto"
  \time 4/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      c8 as'4 g16 f f4 e8 \times 2/3 { e16 f g}
      c,8 c'4 bes16 as as8 g
}


\relative c'' {
  \clef treble
  \key des\major
   \tempo "3. Adagio ma non troppo"
  \time 4/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      r16 as des16.[ es64 f] es16. as,32 as8
}


\relative c'' {
  \clef treble
  \key f\minor
   \tempo "4. Vivace"
  \time 3/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      c4 des
      e,
      \grace e8 f2 f'4
      \grace es8 des2.
      \grace des8 c2 des4
      \grace c8 bes2.
      as4 r
}
