 \version "2.16.1"
    #(set-global-staff-size 14)

\relative c'' {
  \clef treble  
  \key c\major
  \compressFullBarRests
   \time 3/4
   \tempo "1. Moderato"
   R2.*12 c'4 g f e e e f f f e2 r4
}