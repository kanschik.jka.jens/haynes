\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Ich halte meinen Jesum feste [BWV 157/2]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key fis\minor
   \tempo "o. Bez."
   \time 3/8
   \partial 8
   cis8 cis4.~ cis8. fis16 d8
   cis4.~ cis8. eis16 gis b
   a8. gis16 fis16[ gis32 a]
   d,8. e16 cis8 b
   
   
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key fis\minor
   \tempo "o. Bez."
   \time 3/8
   \partial 8
   \set Score.skipBars = ##t
    r8 R4.*31 r8 r8 cis8 cis4.~cis4.~cis8 fis16 eis fis8~ 
    fis16 dis eis gis b a a8. 
}



