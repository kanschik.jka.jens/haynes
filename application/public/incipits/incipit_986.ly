\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
    bes4 r8 bes16 a bes8 d4 c16 bes
    c8 es4 d16 c d8 f4 es16 d
    c4 r8 bes16 a bes8 d4
} 