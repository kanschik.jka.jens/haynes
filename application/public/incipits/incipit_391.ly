\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 4/4
      r8 g b d   b g a d,
      g4 a b16 a g a  b a b cis
      d4 r8 e a, a' a a
      a a a cis, d4. d8
      cis4 cis d
}
