\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro maestoso"
  \time 4/4
    e,4. f8 g4 g
    g4. a8 g4 r
    a16 b a gis a8. b16 d8 c b a
}
