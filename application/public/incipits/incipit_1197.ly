\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "March I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "o.Bez."
  \time 4/4
  \partial 4
    g8 a b2 b8 c d4
    \grace c4 b2. d4
    g g8 g g4 b
    e,2. a4 d,8 c b c d4
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "March II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "o.Bez."
  \time 4/4
    g'8 fis g a g4 b
    a g8 fis g2
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "March III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
      \tempo "o.Bez."
  \time 2/2
   g'2 d g2. g,4
   a8 g a b a4 d
   b2 a4 d b a8 g a4 d b
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "March IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
     \tempo "o.Bez."
  \time 2/2
    f8[ f16 f] f8 a    f4 c
    f8[ f16 f] f8 a    f4 c
    d8[ d16 d] c8. bes16 c[ bes a g] f8 c'
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "March V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
      \tempo "o.Bez."
  \time 4/4
  \partial 4
    g'8 a g4 g,8 g c4 d
    b4. a8 g d' e f
    e4 d8 c b4. c8 c2.
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "March VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
     \tempo "o.Bez."
  \time 4/4
    c,2 e4 g c2. d4
    e d8 e f4 e e d r
}
