\version "2.16.1"
         #(set-global-staff-size 14)

         
         \relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/2
    g'2~ g8 fis16 g a g fis g d2~
}
                 
         \relative c'' {
  \clef treble
  \key d\major
   \tempo "2. o.Bez."
  \time 3/8
  \partial 8
  a16. a32
  d8. fis16~ fis32 e d cis
  e d cis d a8
}
         
                 
         \relative c'' {
  \clef treble
  \key g\major
   \tempo "3. o.Bez."
  \time 2/4
  \partial 8
  d8 g g r fis
  g g r d
  cis16 d cis d  e d c b
  a8 a
}
         