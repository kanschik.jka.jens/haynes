\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Mein Jesus ist erstanden [BWV 67/2]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key e\major
   \tempo "o. Bez."
   \time 4/4
   \partial 8
   b8
   e b b b b16 cis dis e fis8 gis16 a
   gis8 r16 e16 dis8 r16 e16 gis8 fis r8
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key e\major
   \tempo "o. Bez."
   \time 4/4
   \partial 8
   \set Score.skipBars = ##t
   r8 R1*5 r2 r4 r8 
    b8
   e b b b b16 cis dis e fis8 gis16 a
   gis8 r16 e16 dis8 r16 e16 fis4 r8
   
   
}


