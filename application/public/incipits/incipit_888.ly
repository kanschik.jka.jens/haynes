\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 4/4
  cis4 d e8 a4 gis16 fis
  e8 fis \grace e8 d4 cis8
}
