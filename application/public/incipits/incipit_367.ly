 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Andante"
  \time 3/8
  \partial 8
  bes8
  es8. d16 es8~
  es \times 2/3 { es16[ f g]} \times 2/3 { as16[ g f]}
  es8. d16 es8~
  es \times 2/3 { es16[ f g]} \times 2/3 { as16[ g f]}
  
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  es,8
  bes' bes16 bes bes8 g16 as
  bes8 es16 bes bes8 g16 as
  bes8. c16 bes as g f
  es d es8 r
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Allegro"
  \time 3/4
  R2.
  bes2.
  es,4 es8 g f as
  g4 g8 bes as c
  bes4 es, es'
  des2.
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}