\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key f\major
    \time 2/2
    \tempo "1. Allegro"
       a'4 r8 bes16 a  g8 g g a16 g
       f16[ e f g] f a g f e4~ e16 e f e
       d4~ d16 d e d c4~ c16 c d c
       bes4~ bes16 bes c a g4
}

\relative c'' {
  \clef treble  
  \key f\major
    \time 2/2
    \tempo "2. March"
    \partial 4
    c4
    c c8 c c4 c
    c c c c
    c d c c c2.
}

\relative c'' {
  \clef treble  
  \key f\major
    \time 2/2
    \tempo "3. Aria. Adagio"
    \partial 4
      c4. f8 e c r4
      r2 g4. c8 a f f' e d c bes4
}
\relative c'' {
  \clef treble  
  \key f\major
    \time 3/4
    \tempo "4. Aria"
       f4 r f
       f r f
       f e f
       f2.
       f4 a8 g a f
       g f e f g e
}
