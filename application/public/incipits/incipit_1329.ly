 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro (molto)"
  \time 2/4
  \partial 8
  g8
  c r e r
  g4 r8 a
  c,4 b c r8
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Menuet"
  \time 3/4
    g4. e8 a f
    g4. c8 b a
    g4. e8 a f g4 r

}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Aria"
  \time 4/4
   c8 f,16. f32 f8. g32 a \grace a32 g16 f32 e f8 bes4~
   bes16 bes' g f e d c bes \grace c32 bes16 a32 bes a8 r4

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  g4 
  c r8. b16 c4
  d4 r g,
  e' r8. d16 c4 b r
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "5. Andante"
  \time 2/4
    g16 e' e4 d16 c
    \grace c16 b8 a16 g c4
    g16 f' f4 e8
    \grace e16 d16. c32 d16. e32 d4
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "6. Presto"
  \time 6/8
    c8 g e d' b g
    e' c g f' d g,
    g'4. c8 g e
    a f d f d b
    c4 r8
}
