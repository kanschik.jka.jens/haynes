 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Adagio"
  \time 3/4
    bes4 d2~
    d8. c16 es2
    bes8 a f'4.. es16
    \grace es8 d4 c r
    bes'2. a16 g fis g g4 r
}
