\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 3/4
  d2. d
  d8 c c4 b8. c16
  a4 g r
}

\relative c'' {
  \clef alto
  \key c\major
   \tempo "2. Allegro Moderato"
  \time 2/2
  \partial 4
    d,4~
    d g2 b4
    \times 2/3 { b8 a g } g4 r d
    d a'2 c4
    \times 2/3 { c8 b a } a4 r
  }

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
  b2 d4
  g,2 e'4
  fis,2 g4 a4. c8 b4
  }