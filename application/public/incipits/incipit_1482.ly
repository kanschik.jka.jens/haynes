 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Un pocoAdagio"
  \time 4/4
  \partial 8
    d8
    g g~ g16 a32 b a16 b32 g  \grace g16 fis8 \grace e16 d8 r16 b' g d
    d16 dis8 e32 c c16 b8 a16
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  d8
  g~ g32 a b a g16 d b g e'16. d32 e8 r16 g32 fis g16 e
  d b' fis g a,8. b32 c
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Larghetto e Cantabile"
  \time 3/4
  g'4 fis r8. g16
  \grace fis4 e2 d4
  b' \grace a16 g4 \grace fis16 e4
  \grace d4 c2 b4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}