\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Was des Höchsten Glanz erfüllt [BWV 194/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key bes\major
   \tempo "o. Bez."
   \time 12/8
   r4. d8 c d d c d es16 f g8 es
   c4. a8 bes16 a g f f'8 es d c d es
   a,8 g16 f g a bes c d8 d 
  
  
  
   
   
   
}

\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key bes\major
   \tempo "o. Bez."
   \time 12/8
   \set Score.skipBars = ##t
R1.*4 r4. d8 c d d c d es16 f g8 es
c4. a8 bes16 a g f f'8 es d c d es
f,2.~ f4. r4.

   
   
}


