\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Meine Seufzer, meine Tr√§nen [BWV 13/1]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Recorder 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key d\minor
                      \tempo "o. Bez."
                      \time 12/8
                      % Voice 1
                  r4 r8 f4 f8 f e f g4 g8
                  g f g a4 a8 a g a bes4
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Recorder 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \set Score.skipBars = ##t
                     \key d\minor
                        % Voice 2
                  r4 r8 d,4 d8 d cis d e4 e8
                  e d e f4 f8 f e f g4
                       
                
                  }
                  
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe da Caccia"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \set Score.skipBars = ##t
                     \key d\minor
                        % Voice 3
                  a,1.~ a4.~ a8 g16 f e d bes'4.~ bes16 a g f e d
                  
                   
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key d\minor
   \tempo "o. Bez."
  \time 12/8
     \set Score.skipBars = ##t
    R1.*8 r4 r8 d4 d8 d cis d e4 e8
    e d e f4 f8 f e f g16 f e d cis d cis4 r8
                   
   
    
    
    }