
\version "2.16.1"

#(set-global-staff-size 14)

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
 g1 d8 d d2 \grace c16 b8 a16 g
 d'1 a8 a a2 \grace g16 fis8 e16 d g4 r4
  
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 3/4
 g4 e \grace g16 fis8 e16 fis
 g4. fis8 e c'
 \grace b16 a8 g16 fis e4 d d2 c4
   
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Rondeau"
  \time 6/8
 g4 g8 \grace a16 g8 fis g
 e e e e fis gis
 a4 a8 a b g 
 fis fis fis fis4 d8
   
}


