 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major	
   \tempo "1. Affettuoso"
  \time 4/4
  g4~ g16 bes a g a8 d, r d'
  c d16 es d8. c16 bes8 g r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 4/4
  g'8 r d r   bes16 d c d  a d c d
  bes16 d c d  a d c d   bes8 g r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Arioso"
  \time 3/4
  d4 g, fis   g bes a
  fis c'8 es d c
  bes4 a8 bes g4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Gigue"
  \time 12/8
  g8 a g  g bes d g4 g,8 g4 d'8
  g a g  a bes a  bes4 g8
}
