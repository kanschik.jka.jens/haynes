\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Was willst du dich, mein Geist, entsetzen [BWV 8/2]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key cis\minor
   \tempo "o. Bez."
   \time 3/4
   \partial 4
   gis4 \grace cis8 e4. dis8 cis4
   r8 bis8 \grace fis'8 a4 gis8 fis
   gis e  cis16 gis cis dis e dis e cis
   
   
   
   
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key cis\minor
   \tempo "o. Bez."
   \time 3/4
   \partial 4
   \set Score.skipBars = ##t
    r4 R2.*11 r4 r4 gis4 \grace {cis16[ dis]} e4. dis8 cis4 
    r8 bis8 \grace {fis'16[ gis]} a4 gis8 fis
    gis e \grace dis8 cis4 r4
  
   
 
   
   
}



