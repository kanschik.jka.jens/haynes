\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Lentement"
  \time 2/2
    a2~ a4. a8
    b2~ b4. b8
    cis2~ cis4. cis8
    dis2~ dis4. dis8
    e2 e
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxiéme Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Lentement"
  \time 3/2
  c2 f2. a4
  g2 c, r4 c'
  f,2 bes2. bes4
  bes2 a2.
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisiéme Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Gay"
  \time 3/4
    r4 r f,
    bes4. a8 bes c
    c4. bes8 c4
    d bes d c2 c4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatriéme Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Lentement"
  \time 2/2
  r2 b
  g' b~
  b a4. g8
  fis2. e4
  dis2. e4
  b2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquiéme Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Gravement"
  \time 2/2
   r4 r8 fis a4. a8
   d,2. cis4
   b4. b8 cis4 d
   cis2.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sisiéme Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Gratieusement"
  \time 3/2
  r2 g'1
  r2 a~ a4. a8
  bes1 g2
  es1 d4. es8
  d1.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Septiéme Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Lentement"
  \time 3/2
   \set Score.skipBars = ##t
   R1.*3 
   es2 \grace { es16[ f] } g2
   d2 \grace { d16[ es] } f2
   c2 \grace { c16[ d] } es2
   b2 \grace { b16[ c] } d2
   g, r1
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Huitiéme Duo" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Lentement"
  \time 2/2
  g2 fis  bes a~
  a4. bes8 g2~
  g4. a8 fis4. g8
  g2. r4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Neuviéme Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Gravement"
  \time 2/2
  \partial 8
  a'8
  a2 a16[ g f e d c b a]
  bes2~ bes4. a8
  a2. r8 d
  d2. e16 d c d
  e2~ e8 f e f g2
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Dixiéme Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Grave"
  \time 2/2
  a2~ a4. a8
  e'2~ e4. d16 e
  fis1~ fis4. e8 g2~
  g4. fis8
  a2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Onziéme Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Gay"
  \time 4/4
  r8 bes bes bes f2
  r8 f' f f  bes, c16 d es d c bes
  a8 bes16 c d c bes a
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Douziéme Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Lentement"
  \time 3/2
  r2 r c,
  as'2. g4 f2
  g c, c'
  d2. es4 f2
  b, g
}

