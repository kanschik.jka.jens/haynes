
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key bes\major
                      \tempo "1. Largo"
                      \time 4/4
                      % Voice 1
                      r4 f, bes f
                      r f d' bes
                      r f' bes f 
                      d
                  }
\new Staff { \clef "bass" 
                        % Voice 2
                        d,,1 bes2 r
                        r2 r4 f'
                        bes16. f32 bes16. f32 d16. bes32 f'16. d32 g2
                  }
>>
}


\relative c'' {
  \clef treble
  \key g\minor
  \tempo "2. Andante"
  \time 3/4
    f,16 f f f   bes f f f    d' f, f f 
    bes8 bes, r4 r
    d16 d d d   g d d d
}


\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key bes\major
                      \tempo "3. Soave"
                      \time 12/8
                      % Voice 1
                        r4.  r8 r bes a d16 cis d8 a d16 cis d8 
                        r r  g, bes16 fis g a bes8 r r a c16 g a bes c8
}
\new Staff { \clef "alto" 
                     \key bes\major
                        % Voice 2
                         d4. d8 c16 bes a g fis8 d r r r a'
                         bes16 fis g a  bes8 r r g c g a bes c8 r r
}
>>
}



\relative c'' {
  \clef treble
  \key bes\major
  \tempo "4. Allegro"
  \time 2/4
    f8 r f r
    \grace es8 d c16 d bes8 g'
    f a, bes c 
    d c r g'
    f a, bes c
}
