\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato"
  \time 2/2
  \partial 4
    d,8 es
    f4 f2 bes8 g
    g f f2 d'8 f
    f es es4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 3/8
 
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Presto"
  \time 3/8
 
}
