 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #""
                     \key f\major
                      \tempo ""
                      \time 4/4
                      % Voice 1
                      f8 c d bes a f r4
                      r2 r4 c'8 f 
                      e d e c f4 f16 e f d 
                      e4~ e16 d e c d4~ d16 c d bes
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #""
                     \key f\major
                        % Voice 2
                        r2 f'8 c d bes
                        a f g c a g a f
                        c'2~ c16 bes c a bes4~
                        bes16 a bes g a4~ a16 g a f bes4
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soparno"
  \key f\major
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
     R1*4
     r2 f8 c d bes
     a f r4
}