 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 4/4
    es,2~ es8. es16 es8. d32 es
    g2. es4
    bes2. b4
    c2. d8 es
    f g as bes c4 c
    es, d2 es4
    f as2 d,4
    es r8
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  es4. es8 bes8. bes16 g8. g16
  c4. c8
  as8. as16 f8. f16
  as4. as8
  f8. f16 d8. d16
}


\relative c'' {
  \clef bass
  \key es\major	
   \tempo "2. Adagio [Solo]"
  \time 4/4
    \set Score.skipBars = ##t
   R1*4
   es,2 bes4 g f1
   \clef treble
   as''2 f4 d
   f es r8 es es es
   es as as2 c4
   bes8. g16 es4 r2
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Andantino con Variazioni"
  \time 2/4
  \partial 8
  bes8
  bes es es bes
  bes f' f bes, 
  g' g as as f16 es d c bes8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}