\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. [Vivace]"
  \time 2/2
  es,8 r g r bes r bes r
  es es es d16 c d4 r8 g,8
  c c c bes16 a bes4
}



\relative c'' {
  \clef treble
  \key es\major
   \tempo "2.Siciliana "
  \time 6/8
  bes4 d8 g4 f8  d4 bes8 f4 c'8
  d4 bes8 d4 g8 c,4.
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro"
  \time 3/8
es8 d16 c d bes
es8 d16 c d bes
es d es f g as
bes8 as16 g f8
}
