\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 2/2
  \set Score.skipBars = ##t
  \partial 8
  r8
  R1*2
  r4 r8 c  f[ f] f16 a g f
  g4 r8 c,
}
