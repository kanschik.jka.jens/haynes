 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Commodo"
  \time 4/4
  f8. e32 f c8 f a8. g32 a f8 c'
  c16. a32 f16. a32 bes16. g32 e16. g32 f4 r
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Adagio"
  \time 2/2
  g8 e g4 g8 e g4
  c8 g c4 c8 g c4
  e8 c g' e
  \times 4/6 { c'8[ b a g f e]}
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 3/8
   f16 c r f32 g a16 a 
   a f r a32 bes c16 c 
   c a r a32 bes c16 c
   c a e f c es
}
