 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Presto"
  \time 6/8
    g'4 c8 g f f
    f e e e4 r8
    d4 a'8
    f d b c g f e4 r8
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Andante moderato con sordini"
  \time 2/4
  c'4 r8 bes16 g
  f8 a g c, 
  c'4 r8 bes16 g a4 r8 g16 e
  f8 g c, e f4 r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Menuetto"
  \time 3/4
  \partial 4
    g'4 g2 f4
    \grace f8 e d c4 c f2 d8 b c4 r g'8 e
    f2 d8 b c4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Moderato"
  \time 2/4
    c4 b8 a g f' e c a8. d16 c8 b c4 r
}