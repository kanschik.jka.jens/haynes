\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 2/4
    d4 fis16 e d cis
    d4 a a a'
    g fis e d
    cis d r a
    cis32 d e8. e32 fis g8.
    g8 fis r4
}
