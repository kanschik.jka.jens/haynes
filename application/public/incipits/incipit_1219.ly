\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. "
  \time 2/2
  d4. a8 f4 e8. d16
  e'2 e8 e f cis
  d4. d8 d8. d16 e8. f16
  cis4.
}



\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. "
  \time 3/4
  r4 a bes c4. bes8 a4
  d e f8 g
  e4. d8 c4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. "
  \time 2/2
  \partial 4
  d4
  e2 d8 e8 f
  e2 d8 e f4
  e d e f e a,
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Menuett"
  \time 3/4
  a'4 d, cis
  d8 e f4 e
  a d, cis d8 e f4 e
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "5.  [Allegro] [Violin]"
  \time 2/2
    d,8 d' cis a  d bes g e'
    f, d' e, cis' d, bes'16 g a[ a,]
    d4
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "5.  [Allegro] [Oboe]"
  \time 2/2
    \set Score.skipBars = ##t
    R1*4
    a'4. f16 e d4 r8 a'
    bes g f8.[ e16] e[ f f g] g a a bes]
}
