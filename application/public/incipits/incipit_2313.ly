\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Tutti]"
  \time 2/4
  g'8 b,16 d g4
  g8 b,16 d g4
  g16 d a' d, b'4
  c,8 d,16 fis c'4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*20
   r4 r16 a'16 g a
   fis g e fis d e c d
   b4 r16 b' a b
   g a fis g e fis d e
   c4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Largo"
  \time 4/4
  r4 b e4~ e8 fis16 e dis8 b
  c4. d16 b b4 e a~
  a8 g f4~ f8 e a g
  fis[ g] r16
}



\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro [Tutti]"
  \time 3/8
    g'8~ \times 2/3 { g16[ d c]} \times 2/3 { b16[ g b]}
    \times 2/3 { d16[ b d]} e8 e
    e d r
 }

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro [Tutti]"
  \time 3/8
     \set Score.skipBars = ##t
   R4.*28

    d8~ \times 2/3 { d16[ c d]} \times 2/3 { e16[ d c]}
    d8~ \times 2/3 { d16[ c d]} \times 2/3 { e16[ d c]}
    \times 2/3 { d16[ e fis]} 
    \times 2/3 { g16[ b a]}
    \times 2/3 { g16[ fis e]}
}