 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 4/4
    c4 r c r
    c16 f e f c8 r c16 a' g a c,8 r
    r16 e a g  a fis g a r d, g f g e f g
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Adagio"
  \time 3/4
    a4 f d
    d' cis2
    d4 e8 d c b
    c2 r4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Vivace"
  \time 3/4
  c2 f4
  bes,8 g' f g a,4
  d8 c c bes bes a
  a4 g8 a f4
}
