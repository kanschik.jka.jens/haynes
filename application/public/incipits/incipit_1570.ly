 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegretto"
  \time 4/4
  c4 c16 d c d e8. f32  e64 d c8 r
  e4 e16 f e f g8. a32 g64 f e8 r
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Adagio"
  \time 3/8
  c16 r d r es r
  g8 f es
  as16 g f es d c b4. c
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Rondeon. All. assai"
  \time 2/4
  g'4. e8 f4. d8 e4. c8 d2
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Menuett"
  \time 3/4
  \partial 8
  
}