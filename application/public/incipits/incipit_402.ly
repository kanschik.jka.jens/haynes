\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. All. [tutti]"
  \time 2/2
  \partial 8
  d,8
  d2 r4 a'16  g fis e
  d4 r4 fis8 a4 r8 d
  cis2
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. All. [solo]"
  \time 2/2
  r2 \grace e8 d32 cis d8. d8[ d]
  \grace e8 d32 cis d8. d16[ e fis g] a8 a a a
  \grace a8 b8 a \grace a8 g fis \grace fis8 e d r4
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Largo staccato"
  \time 3/4
  b,8 cis d e fis g
  ais,4 fis' r
  a,8 b cis d e fis g,4 e' r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio [solo]"
  \time 4/4
  d8 b16 a g8 g  \times 2/3 { c16[ d e]} d8 g4~
  g8 fis16 g \times 2/3 { a16[ g fis]} \times 2/3 { e16[ d c]} b4 g'
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. All. con spirito"
  \time 3/4
  \partial 16
  d16
  d4 r8 a d fis
  e4 r8 a, cis e
  d4 r r
}

