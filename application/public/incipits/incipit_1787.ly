 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 2/2
    d4~ d16 d e fis
    g[ e fis g] g8 b
    e,4~ e16 e d c b8[ d] g8. a16
}
