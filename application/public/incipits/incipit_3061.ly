\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Qui tollis [BWV 233/4]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key g\minor
   \tempo "Adagio"
   \time 4/4
   r4 g4 es'2~ es16 d d c fis4~ fis8 g g, a
   bes16 es cis d a bes fis g d f a c a'4~ a8
   
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key g\minor
   \tempo "Adagio"
   \time 4/4
   \set Score.skipBars = ##t
     R1*9 r8 d8 es4~ es16 d c b c8 r8 
     r8 c8 fis,4~ fis8 g~ g es'16 d
     bes8 a a4 r4
}



