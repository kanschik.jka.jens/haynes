 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 2/4
  c4 r16 c d e
  f8 f f f
  e16 d e f e d e f
  d c d es  d c d es
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Largo"
  \time 3/4
  c8 bes a g f g16 e
  d2.
  g'8 f e d c d16 bes
  a2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Fantasia."
  \time 2/4
  c16 e, f8 f d'
  a4 r8 bes
  c16 e, f8 f d'
  }

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Gigue."
  \time 6/8
  r4 c8 f4.
  e8 d c e4 f8
  g a f g a f
  g f e f4
  }

\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. Menuett."
  \time 3/4
  c8 f, f f f c'
  d f, f f f d' c4 f8 e f g
  e4. d8 c4
  }
