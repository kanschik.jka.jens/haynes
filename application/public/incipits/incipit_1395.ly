 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8.
    d,16 fis a
    d8 d16 d d8 d d4 fis
    d8 fis d a fis d r a'
    d4 e fis g
    a16 g fis8 g b b4 a
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Cantabile"
  \time 3/4
   r8 f f e e d
   \grace d8 cis2.
   d8 f e g f e
   \grace d8 cis4 d r
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Plaisanterie"
  \time 2/4
  d8 d16 d d8 d
  d8 d16 d d8 d
  d4 e e r
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4. Menuett"
  \time 3/4
      a'8 fis d4 d8 d
      d4 a' b
      a a8 a a a
      a4 a8 a fis4
      a, a8 a a a
      a4 e' fis g g8 g fis fis
      \grace fis8 e2 r4
}