\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro [Tutti]"
  \time 3/4
  \partial 16
  d16 d4 r8 a a8. g16 fis4 r16 d e fis g a b cis
  d8. e16 fis8. g16 a8. d,16
  cis4. b8 a4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*30 
    r4 r8 a b cis
    d fis4 a d,8
    cis16 d e8 r a, cis d
    e g4 a, g'8
    fis16 g a8 r
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  fis16 fis fis fis fis fis fis fis  g g g g   g g g g
  fis fis fis fis  fis fis fis fis fis fis, fis fis fis fis fis fis fis
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Adagio [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*2
   r4 \times 2/3 { fis8[ e fis]} \times 2/3 { d8[ cis d]} \times 2/3 { b8[ ais b]}
   g16 a g8 r4 r e'4 cis' e, d r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 2/2
  d1 e fis2 g~ g fis e1 d4 e fis gis
  a
}