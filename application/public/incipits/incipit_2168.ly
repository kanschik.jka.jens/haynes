\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
    d4. f8 a,8. a16 a8 a16 bes
    bes8 bes r bes es8. es16 es8 d16 es
    c4. c8 bes8. bes16 bes8 a16 bes
}
