 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Bergerie"
  \time 3/4
  \partial 4
    b8 c
    d4 e8 d c e
    d4 g, b8 c
    d4 e8 d c e
    d4 g,
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Borée, Suite de la Bergerie"
  \time 2/2
  \partial 4
  d4 g g fis e
  d2 g4 fis8 e
  d4 c8 b c4 b8 a
  b4 a8 b g4
}

