\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
  bes4~ bes16. c32 d16. a32 bes4~ bes16. c32 d16. a32 
  bes8. c32 d es8 d16 c d16. c32 d16. bes32
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/4	
   r4 r8 d16 es f8 f f f
   bes,4 r8 bes c d es d16 c
   d c c d  d e e f f8 c f4~
   f8 g16 f e f d e f4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Largo"
  \time 4/4
      << {
      % Voice "1"
      d2~ d16. c32 b16. a32 g16. f32 es16. d32
      es4 es16. g32 c16. es32 d4 g
         } \\ {
      % Voice "2"
      as16. g32 f16. es32 d16. c32 bes16. as32 g4 d'
      d8 c16 b c4 c8 bes16 a bes8 a16 g es'4
      } >>
 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 2/4
  \partial 8
  f8
  bes a16 g f es d c
  d8 c16 bes c8 a
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 4/4
  r4 a8 d16. cis32 d4~ d8 f16. e32 
  f4 r8 d16. cis32 d8 a'16. g32 a4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro"
  \time 12/8
     d8 a d  d cis d e a, e'  e d e
     f e d g4.~ g8 a f e4 d8
     d e f f e d
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Affetuoso"
  \time 3/4
   c4 f, f'~
   f e8 d e4 d g2~ g4 f8 e f4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro"
  \time 4/4
  \partial 8
  a'8
  a16 g f e  d8 cis d4 r8 a'
  a16 g f e d8 cis d a
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 4/4
  bes4~ bes16. g32 es16. c'32 bes4~ bes16. g32 es16. c'32 
  bes8 es16 g, as8 bes g16. bes32 es16. d32 es8 as,
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Alla breve"
  \time 2/2
   es2 d4. c16 d es4 bes g f8 es
   c'4 as as g8 f
   bes4 g g f8 es f2.
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Andante"
  \time 6/4
   r4 c8 d es b  c g es' f g d
   es2.~ es8. d32 es f8 d es c
   f2.
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Allegro"
  \time 6/8
  \partial 8
  bes8 es es,16 f g as g4 bes8
  bes8 es es,16 f g as g4 
}

  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  r4 c8 d16 es d8. c16 d8 es16 f
  d8 c r4 r2
  r4 c8 d16 es d8. c16 d8 es16 f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/4
   r8 c a f
   d'4 c bes8 a16 g a8 g16 f g4.
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Largo"
  \time 3/2
   a2 f4 e f d
   d'1.~ d2 e4 d cis b
   cis1.
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 3/8
  r8 f, f f g16 f e f
  g8 g g g a16 g f g
  a g a f f'8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  r4 r8 g d' b16 g fis8 d'16 c
  b a g8 r4 r2
  r8 g'16 d e d e8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 6/8
   r8 d b g e'16 d c b a8 fis' d b g'16 fis e d
   c8 d16 c b a c8 d16 c b a 
   b8 g d'
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Grave"
  \time 4/4
   g8 e' cis b16 a d2~
   d32 g fis e d cis b a g16 cis cis16. b64 cis d4
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 4/4
  g'4. fis16 e d8 e16 d c e d c
  b8 a16 g d'8 g16 g a,8 g'16 g a,8 fis'16 fis
  g4.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  a8 d4~ d16 e32 fis e16. d32 d4~ d16 e32 fis e16. d32 
  e4~ e16 fis32 g fis16. e32
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
   r4 a b8 b b a16 g
   a8 d a g16 fis g8 d' g, fis16 e
   fis8 d' fis, e16 d e4.
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Affetuoso"
  \time 3/2
   b4 cis d cis b2~
   b ais1
   d4 e fis e d2~
   d cis1
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Vivace"
  \time 3/4
      << {
      % Voice "1"
      d4 r8 a d16 e fis e
      e4 r8
         } \\ {
      % Voice "2"
      d8 cis16 b  a g fis e fis8 d
      a'16 a' g fis e d a b cis8 a

      } >>
  
  
}
