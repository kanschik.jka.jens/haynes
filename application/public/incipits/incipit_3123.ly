\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Ich sehe schon im Geist [BWV 43/9]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key a\minor
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                     r4 r8 e8 f16 g f d
                     e8 c~ c b16 c d e d b
                     c8 a~a16
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key a\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                  r4 r8 c8 d16 e d b
                  c8 a~a8 gis16 a b c b gis 
                  a8 e r8
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key a\minor
   \tempo "o. Bez."
  \time 3/4
     \set Score.skipBars = ##t
     R2.*12 r4 r8 a8 b16 a gis a
     b8 e,4 fis16 gis a b c d
     c b a4
}