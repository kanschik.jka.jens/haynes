 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. A tempo giusto [Obs]"
  \time 3/8
  r8
  << {
      % Voice "1"
      b8 c
      d16 c b c d8
               } \\ {
      % Voice "2"
      g,8 a
      b16 a g a b8
      } >>
      r8
      << {
      % Voice "1"
       a8 b c16 b a b c8
         } \\ {
      % Voice "2"
     fis,8 g
      a16 g fis g a8
      } >>
      
}