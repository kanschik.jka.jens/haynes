 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef bass
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
    bes,1
    fis8 g g2 g8 es'
    es d c bes bes4 bes
    b8 c c2
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Adagio"
  \time 2/4
  es,8 as16. g32 g8 f
  es c'16. bes32 bes8 as
  g es'16. d32 \grace d16 c16. bes32 as16. g32 as8 g r4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Presto"
  \time 4/4
  <d, bes'>4 c8 bes d c es d
  c bes c bes  d c es d
  c bes d c es d bes' e,
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}