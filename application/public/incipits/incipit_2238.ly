 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro moderato"
  \time 2/4
    a8 d4 cis16 b
    a8 d,4 e16 fis
    g a b8. g16 fis e
    fis g a8~ a8. b32 cis
    d8. e32 fis g8 e
    fis16 e d8 r
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante e Lento [Tutti]"
  \time 3/8
  g8. b16 a8 g4 r8
  b8. d16 c8 b4 r8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante e Lento [Solo]"
  \time 3/8
   \set Score.skipBars = ##t
   R4.*22
   g8. b16 a8 b8. d16 c8
   d16 g, g'8 e
   \grace d8 c4 b8
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Allegro assai"
  \time 3/8
  d4. fis
  e8 d cis b a g fis d a'
  a a a b g d' d d d
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}