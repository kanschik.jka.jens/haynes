 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 3/2
    r2 g g g1.
    r2 es'4 c b2
    c es4 c b2
    c
}
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro"
  \time 4/4
  \partial 8*5
  g8 c b c d
  es d es b c g' f16 es d c
  b8 c16 d g,8
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Adagio"
  \time 4/4
  es4 d cis d~
  d c d8 fis fis fis
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Allegro"
  \time 6/8
  c8 es g b, d g,
  c es g b,4 r8
  c8 es g b, d g,
  c es g as,4 r8
}
