 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Largo [Adagio]"
  \time 4/4
    c2~ c4. es16 d d4. f16 es es4~ es16 f g as
    d,4~ d16 es f g c,4~ c16 d es f
    b,8 g r4 r2
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Allegro"
  \time 4/4
  r8 c c c  b g bes e,
  a f as d, g es16 f g8 g'
  a, f' b, g' es c as' as
  
}

\relative c'' {
  <<
\new Staff { \clef "treble"   
                     \key bes\major
                      \tempo "3. Adagio"
                      \time 3/2
                      R1. bes2 c d
                      es1 d2 
                      c f es d bes r
                      % Voice 1
                  }
\new Staff { \clef "bass" 
                     \key bes\major
                     es,,2 g as
                     bes1 as2 g c bes a f r
                     bes c d
                        % Voice 2
                  }
>>
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Bourée angloise"
  \time 4/4
  \partial 4
  g'4
  f8 es d c  es d c b
  c2 g4 b
  c d es f d2
}