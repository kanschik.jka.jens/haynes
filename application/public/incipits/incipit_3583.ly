\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro molto [Tutti]"
  \time 3/4
  c4 c c
  c8 c, e g c e
  d d4 e16 f e8 d
  c8. e16 g,8. f16 e8. d16
  c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro molto [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*32
   \grace a'8 g4 f16 e8. d16 c8.
   \grace gis'4 a2.
   c,8 f a c c32 b16. a32 g16.
   a2 g4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante [Tutti]"
  \time 3/4
  f8 c r c a f
  \times 2/3 { e16[ f g]} f8 r f f f
  \times 2/3 { e16[ f g]} f8 r f a c
  es2.
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante [Solo]"
  \time 3/4
  \set Score.skipBars = ##t
  R2.*12
   \grace {e16[ f g]} f8 c4 c8 \grace bes16 a8 \grace g16 f8
   e8. f32 g f8 f~ f16 f' \grace g8 f16 e32 d
   d4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto assai"
  \time 2/4
  c8 e g c
  c, c' b, b'
  a, c f a
  a, a' g, g'
  f, a d f a f e d c4 b
}

