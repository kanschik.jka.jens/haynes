\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 2/2
  \partial 8
    d8 g g g g  g32 bes g8. g32 bes g8.
    g16 bes a g fis[ g fis g]
}


