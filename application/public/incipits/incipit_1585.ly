\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Amoroso andante"
  \time 3/8
  c8 f8. g16
  \grace f8 e4 d8~
  d16 b c e g bes,
  a16. bes32 c8 r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Tantino Allegro"
  \time 2/4
  \partial 8
    c8 f f4 a8
    a4 g8 c,
    g' g4 bes8 bes4 a8
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Menuett"
  \time 3/4
    c4 c4. bes16 c
    d8. d16 d4. c16 d
    e8 bes' a g f e
    \grace e4 f2.
}
