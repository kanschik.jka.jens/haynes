 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\minor
   \tempo "Adagio"
  \time 3/4
  r4 f, g
  as bes c
  des8 des4 des8~ des16 f es des
  c8 as'4 g as8 f2.
}
