\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 12/8	
    r4 r8 bes4 c8  d c bes f'4 es8
    d c bes f'4 es8  d c bes a4 bes
    c4.
}
