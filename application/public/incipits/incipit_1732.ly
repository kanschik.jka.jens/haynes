\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 3/4
    r4 bes bes
    es2.
    r4 c c f2 es4
    d4. c8 bes4
}
