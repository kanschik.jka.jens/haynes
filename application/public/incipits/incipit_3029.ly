 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Gravement"
  \time 6/4
  \partial 4.
    d,8 d'4
    c4. bes8 a4 bes2.
    a4. a8 bes4 g c2
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. En Menuet"
  \time 3/4
    d,4 g2 fis g4
    a bes2 a bes4
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Lentement"
  \time 4/4
    r2 r4 bes
    a r r bes
    g r r a
    fis
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "4. Gayement"
  \time 2/2
    g4 d' d c 
    bes a8 g fis4 e8 d
    g4. a8
}