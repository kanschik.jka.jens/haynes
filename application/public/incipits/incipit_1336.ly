 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Marche. Adagio"
  \time 4/4
  bes8. c16 bes8. c16 bes8 es bes r
  bes as as as as4 g
}

