\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Piece in D] " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. Prelude. Gravement"
  \time 2/2
  d4~ d16 d cis b a8. b16 g8. a16
  fis4~ fis16 a g fis e8. fis16 d8. e16 cis 4.
 

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Allemande. L'Angelique"
  \time 4/4
  \partial 16
  a16 a4~a16 a fis d g8. fis16 e fis g a 
  fis4 d16 fis g a d,8. cis16 b cis d e
  cis8

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Le Badin"
  \time 6/4
  r4 r4 d4 fis g a
  g fis e fis e d
  g2. fis4 g a
  d, cis d e cis d
  cis2 a4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. L'Espagnol"
  \time 3/4
  r4 d4 d
  e d e 
  fis e fis
  g fis e
  fis cis d
  e e4. d8 
  d4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. La Chevry. Gavotte"
  \time 4/4
  \partial 2
  d4 a
  d e8 fis e4 a
  fis d fis e8 d
  g4 fis8 g a4 g8 fis
  e2
 

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "6. La Coquette"
  \time 6/8
  \partial 4.
  d4 e8
  fis4 e8 d4 cis8
  d4 a8 d4 e8
  fis e fis g a fis
  e4 
  
  

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "7. Allemande. La Magdelon"
  \time 2/2
  \partial 8
  r16 d16 d4~ d16 fis e d a'8 e16 fis g8 fis16 e
  fis8 \grace {e32[ fis]} g16 fis e8 a d,8 fis e d
  cis16[ d cis d] e d cis b a8 
  
  

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "8. Langloise. Gigue"
  \time 9/8
  a4 g8 fis d fis e fis d
  cis b cis d fis d g a fis
  e4. 
  
  

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Piece in G] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prelude"
  \time 4/4
  b2~b8. b16 a8. g16
  d'2 g,8. a64[ g fis g] a8. b16
  fis4. d'8 d8.~ d64[ c b c] c8. b16
  b4.

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Piece in e] "
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Prelude. Grave"
  \time 2/2
  r2 e4 b
  g'2 fis4. e8
  dis2 e~
  e d4. d8
  d2 \grace c8 c4. c8
  c2 \grace b8 b2~ b


}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Piece in g] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Prelude"
  \time 2/2
  r4 r8 g8 bes4. g8
  d'4. d8 g4 fis8 g
  fis4. fis8 fis4. g16 a
  d,4. d8 g4~ g16 fis e d
  es4. es8 d4.

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Piece in d] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Prelude"
  \time 2/2
  d2 a4. a8
  bes4. bes8 bes4. c8
  a2 f'4. f8
  d4. d8 d4 cis8 d
  cis2 c~c bes~bes

}


