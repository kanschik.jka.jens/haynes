\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. o.Bez."
  \time 4/4
    a'8 \grace g8 f16. e32 d8 d d2
    cis4 r8 cis d4
}
