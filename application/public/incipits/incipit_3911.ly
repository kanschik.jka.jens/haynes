\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Con l'arpa Sonora" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Corno Inglese obl."
  \key g\major
   \tempo "Andantino"
   \time 3/4
   \partial 8
   r16 d16
   d8. g16 g4. b8
   \grace b8 a4 g4 \times 2/3 g8 fis f
   e8. c'16 c4. e,8
   e d d4.
  
  
 
 
                                                                                   

}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key c\major
   \tempo "Andantino"
   \time 3/4
   \set Score.skipBars = ##t
   \partial 8
 r8 R2.*5 r2 r8 b8
 b8. e16 e4. g8
 \grace g8 f4 e r8 e8
 \grace d8 c8. a'16 a4. c,8
 c b4.
       
}




