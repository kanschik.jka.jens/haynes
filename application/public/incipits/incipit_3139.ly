\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Choral + Recitativ (Tenor): Die Welt sucht Ehr und Ruhm [BWV 94/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\major
                      \tempo "Arioso"
                      \time 3/8
                      % Voice 1
                  d8 c16 a b g
                  g'8 fis g
                  a g16 e fis d
                  c8 a' c,
                     
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\major
                     \set Score.skipBars = ##t
                        % Voice 2
             R4. d8 c16 a b g fis4 r8
             a8 g16 e fis d
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key g\major
   \tempo "Arioso (Choral)"
  \time 3/8
     \set Score.skipBars = ##t
     R4.*7 r8 r8 d8
     b a g
     d' e16 d c d 
     e4.~e4 
}