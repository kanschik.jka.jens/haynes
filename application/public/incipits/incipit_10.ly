\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Tutti]"
  \time 2/4
  bes8 f' bes, a
  bes8 f' bes, a
  bes d16 es f8 bes, 
  c bes r bes
  a es' d c16 bes
  a8. g16 f4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*26
   r4 r16 bes a bes
   c bes a bes  f' bes, a bes
   \grace bes8 a4. g8
   f16 g a bes c8 es,
   es d r
  
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2.Adagio [Tutti]"
  \time 4/4
    r16 g' d bes   r d bes g
    r g' es c  r es c g
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*4
   g'4. d8 es16. d32 es8 r es
   es d r g g fis r fis
   g g, r f' f es r es
   

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/8
    bes8 d,16 es f8
    bes, bes' a
    bes8 d,16 es f8
    bes, bes' a
    bes16 f f f f f 
    g8 f r
}