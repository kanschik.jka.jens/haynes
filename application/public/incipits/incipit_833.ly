\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
  r8 d c f16 a, bes8 bes c d16 es
  d8[ d]
}
