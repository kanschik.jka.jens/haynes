\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. allegro"
  \time 2/2
    \partial 8
    r8
    r4 r8 c g' g g g
    g f16 e f8 f bes bes bes bes
    bes[ a16 g]
}
