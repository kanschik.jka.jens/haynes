\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo [Oboe]"
  \time 12/8
  bes8. c16 bes8  bes4 bes8 c bes c r r c
  d8. es16 d8 e f d d4 c8 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo [Soprano]"
  \time 12/8
   \set Score.skipBars = ##t
   R1.*7 
   r2. bes8. c16 bes8 bes4 bes8
   c bes c r4 r8 d8. es16 d8 e8 f d
}
