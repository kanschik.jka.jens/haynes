\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Schläfert allen Sorgenkummer [BWV 197/3]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key a\major
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
 e2 cis'4
 b a8 gis a4
 fis e a d,2 cis4
 r4
 
                                                                                   

}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key a\major
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
 R2.*23 r4 e2~
 ~
 e8 gis b d
 cis4
 b a8 gis a4
 fis e8 gis a gis
 \grace e8 d2 cis4 r4
  
  
       
}



