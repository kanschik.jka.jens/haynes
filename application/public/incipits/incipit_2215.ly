\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4 
    f8 c c c  c32 b c8. c32 b c8.
        f8 bes, bes bes  bes32 a bes8. bes32 a bes8.
}


