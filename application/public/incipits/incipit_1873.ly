\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 4
  c4
  f, f' \times 2/3 {f8[ es d]} \times 2/3 {es8[ d c]} 
  d4 d \times 2/3 {d8[ c bes]} \times 2/3 {c8[ bes a]} 
  bes4 g8
}
      
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Largo"
  \time 6/8
  d4 a8 f'8. e16 d8
  bes4 a8 r r d
  e8. d16 e8 a,16 g' f e d cis
  f8. e16 d8 d4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Vivace"
  \time 6/8
    \partial 8
    c8
    f,4. g4. a8 c f a, c f
    d c bes g' a f e d c c4
}
