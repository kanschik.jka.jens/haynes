\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  g4 g g r8 d'16. g,32
  fis8 g c b  e d r d16. g,32
  fis8 g c b a g r d
  es e f fis g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*26 
  r2 r4 r8. d16
  g4 g g4. g32 a b16
  fis8 g e d  c b4 g'32 a b16
  fis8 g e d c b
  b16 g' b g
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  \partial 8
  a'8
  fis32 e d16 d[ d] d d d d b'8 a b,4
  a16 cis d a' g4 fis32 e d16 d8 r16
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio [Solo]"
  \time 4/4
  \partial 8
  r8
   \set Score.skipBars = ##t
   R1*11
   r4 r8 a'8
   fis32 e d16 d[ d] d d d   b'32 g d' b a8 r a
   g32[ fis16. e32 d16.] cis32[ b16. a32 g'16.]
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto [Tutti]"
  \time 2/4
  g'4 d b8 b' b, b'
  b, b' c,4
  b8 b' b, b'
  b, b'
  c,4 b4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*52
   g'4 d b16 a g e' e d d c b a g e' e d d c
   b8 b' a, c' b, d' a, c'
   b g d4
}