\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "1re. Fête " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Légerement"
  \time 4/8
  \partial 8
  e16 d
  c8 g e' f16 e 
  d4~ d16 e d c
  b8[ c d b]
  c e e16 f e f
  d e d e d e d e
  

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement"
  \time 6/8
  \partial 4.
  g4 a8
  g4 g'16 f e8 d8. c16
  b4 r8 g4 a8
  g4 e'8 d16 c b8. c16 c4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gaiement"
  \time 4/8
  e8 d16 c f8 e
  d g, r8 e'8
  d16 e d c b c b a
  g8[ e' e e]  
  d[ d d d]
  c[ c c c]
 

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe. Fête " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Légerement"
  \time 4/8
  \partial 2
  g4 g
  c4. b16 a
  g8[ a g f]
  e d16 e c d e f
  g8 f16 e d8 c b
  

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Lentement"
  \time 3/8
  es4 f8
  es4 d8
  es d b 
  c g b
  c g16 c b d
  c8 g16 c b d
  g,8 c4 b4.
  

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gaiment"
  \time 3/8
  c8 d b
  c16 b c e e8
  c e b
  c16 g a b c d
  e8 d c b4.

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    " IIIe. Fête " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Modérément"
  \time 3/8
  \partial 8
  g8
  c,16 b c d e c
  d c d e f d
  e8 f16 e d c
  d8 g, g' 
  

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Lentement"
  \time 3/8
 c8 g c~
 c b4
 c8 es es
 d d es16 d
 c8 d16 es f es
 d8 b16 c d b

}
  
  \relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Gaiment"
  \time 4/8
  g8[ c, c a']
  g[ c, c a']
  e f16 e d8 c
  b g' g16 f e d
  c8 e e16 d c b 
  a8 

}
 

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IVe. Fête " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Vivement"
  \time 3/4
  c8 c, c c c' b
  a a, a a a' g 
  f f, f f f' e
  d d d d g f
  e e e e e e 

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement"
  \time 2/2
  \partial 2
  e8 f d f
  e4 c d8 e c d
  b4 c e8 f d f
  e4 c d8 e c d
  b2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Légerement"
  \time 4/8
  e16 d e f e f e d
  c8 g g c16 d
  e d e f e f e d
  c8 g g c16 d
  e8 d16 c
  f8 e d4

}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    " Ve. Fête " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Gayment"
  \time 3/4
  g4 fis e
  d c b 
  e d c
  b a g
  b8 a b c d c
  b a b c d c
  b a b d c b a2.

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Gracieusement"
  \time 6/8
  \partial 4.
  bes16 a bes c bes c
  a4 a8 bes a fis
  g8. a16 fis a g fis g bes a c
  bes8 a16 bes c8 bes c16 bes a g
  fis4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Légerement"
  \time 4/8
  b8[ c d c]
  b[ c d e]
  a,[ a a a] 
  b[ b a fis]
  g[ d' c a]
  b g16 a b c b a
  g8[ b a g]
  fis g16 a b8 a

}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    " VIe. Fête " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Gaiment"
  \time 3/8
  g8 d16 e fis d
  g8 d16 e fis d
  g8 g, g
  g4.
  e'16 d c b a g
  e' d c b a g

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Gracieusement"
  \time 6/8
  \partial 4.
  b4 c8
  b8. a16 b c b a b c a b
  g8. fis16 g a b4 c8
  b8. c16 b8 a4 g8
  fis4.

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Gaiment"
  \time 4/8
  g4 d8 b
  g4 r8 e'8
  d[ c b a] 
  b g16 a b c d e
  d8 e16 d c8 b a4

}



