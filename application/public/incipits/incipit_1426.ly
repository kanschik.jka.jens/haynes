 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
    \partial 4
    bes'4
    es4. g,8 g4 g
    g2 bes4 r
    es, es d d es4. f16 es d4 r
}
 