\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8
  d8
  \grace a'16 g16 fis32 g d16[ r32 d]
  \grace d16 c16. b32 \grace b16 a16. g32
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 16
  a'8 g g fis fis4
  r8 r16 fis
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 4/4
    d1 fis \grace b16 a16 gis a b a4 r2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8
  g8
  g16 bes bes8 r bes
  bes16 d d8 r d
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/4
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
    \times 2/3 { d16 e d} g4 d8
    \times 2/3 { e16 fis e} g4 e8
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  e8
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

  \grace d16 \times 2/3 { c16 b a } a8 r a'
  gis16. a32 b8
}
