\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Mein Jesu, ziehe mich nach dir [BWV 22/2]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\minor
   \tempo "o. Bez."
   \time 9/8
   r4 r8 r4 g8 c16 d es8 d
   d fis, g~ g b c d16 es f8 es8
   es fis, g~ g
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key c\minor
   \tempo "o. Bez."
   \time 9/8
   \set Score.skipBars = ##t
    R2.*12 r4 r8 r4 g8 c16 d es8 d
    d4.~ d8. es16 f g as8 g f es4 r8
}



