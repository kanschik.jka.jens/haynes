\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Moderato"
  \time 4/4	
    b2 gis8 a c a
    g2 d8 d g b
    d2 \grace { e16[ d cis d]}  fis8 e d c
    b2 a8
}
