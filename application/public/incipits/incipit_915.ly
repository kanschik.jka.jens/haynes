\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 3/4
    <g, es' es'>4 r8 bes''16 g
    es8 g16 es
    bes8 es16 bes g8 bes16 g es8 g16 es
    d4 d d
    d r r
}

