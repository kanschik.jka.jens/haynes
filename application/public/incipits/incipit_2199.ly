 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Intrada"
  \time 4/4
    c4. f,8 d'2
    c8 f, f'2 e4
    d2 c
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Presto"
  \time 2/4
    r8 c a f   c' bes16 c  d8 c16 bes
    c8 e c g
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Menuet"
  \time 3/4
  f4 a8 f g4
  c,8 a bes g a4
  c4 f8 c, g' a
  e2 f4
}
\relative c'' {
  \clef treble
  \key f\major	
   \tempo "Trio"
  \time 3/4
    c4 a d
    c2 f4
    e f g a2.
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Bouree"
  \time 2/2
  \partial 4
  a4
  f' a d, g
  c, bes8 c a4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "5. Gigue"
  \time 6/8
  \partial 8
   a8
   a4 a8 f'4 e8
   d8 bes' a g4 f8
   g4
}
  

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "6. Air"
  \time 2/2
     a'2 cis,
     d4 a f'2
     g4 c, g'4. f16 g
     a2
}
 
\relative c'' {
  \clef treble
  \key f\major	
   \tempo "7. Menuet"
  \time 3/4
  \partial 4
     f,4
     f f a8 bes
     c4 f, f'
     c a8 f d'4
     c2
}
 