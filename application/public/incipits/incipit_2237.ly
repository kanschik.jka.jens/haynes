\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Tutti]"
  \time 2/2
  \partial 8
  r32 bes, c d
  es8 es es es  es8. d32 c d8 r32 d es f
  g8 g g g g8. f32 es f8 r32 bes, d f
  bes8 bes bes r32 a bes d
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Solo]"
  \time 2/2
  \partial 8
  r8
   \set Score.skipBars = ##t
   R1*7 
    \grace c8 bes16[ a bes8] \grace c8 bes16 a bes8 
    c32 d es16 d8 r4
    \grace c8 bes16[ a bes8] \grace c8 bes16 a bes8 
    es32 f g16 f8 r4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 4/4
  \partial 8
  f8
  \grace es8 d16. c32 bes16 bes[ bes] bes[ d f16. bes,32]
  \grace c8 bes32[ a bes16] a8 r c
  \grace bes8 a16. g32 f16 f f
  f[ a c16. es32] es32[ c16.] \grace c8 d8 
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/8
  f4. d8 c bes
  \grace d8 c8 a8. g32 a bes4.
  bes' a8 g f
  g e8. d32 e
  f4.
}