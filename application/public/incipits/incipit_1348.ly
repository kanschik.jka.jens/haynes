 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4
  c4~ c16 b c d32 es
  d4~ d16 es f es
  d4~ d16 as' g f
  es8 c r
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Andante"
  \time 3/4
  c4 r8 es d16 c d b
  c4 r8 es d16 c d b
  c4 r8 g' g8. f32 g
  as8 c, f, f' f8. es32 f
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Affetuoso"
  \time 6/4
  bes4. as8 g4 \grace f8 es2 c'4
  bes2.~ bes2 es4
  as,2 f'4 g,2 es'4
  f,2. r4 r
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. ALlegro"
  \time 3/8
  c8 g4
  es'8 d16 c d b
  c8 g4
  g'8 f16 es f d
  es8 d16 c d b
  c8 d4
}