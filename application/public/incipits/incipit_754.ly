 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Spirituoso"
  \time 2/2
  f4. c8 a4. c8
  f, g a bes c d e f
  g4. c,8 c4. c8
  c d e f g a bes c
  a4. f8 f4. f8
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Adagio"
  \time 4/4
  f4. g16 e f4. g16 e
  f8 f,16 g a8 bes c4. d16 bes
  c4. d16 bes c8 d16 e f8 g16 a
  g16 f e8 r4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 6/8
  r8 r c c4 c8
  a f a  c a bes16 c
  a8 f c' f4 f8
}
