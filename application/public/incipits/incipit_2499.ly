\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro e molto Vivace [Tutti]"
  \time 4/4
    c4 c8. c16 e4 e8. e16
    g8 g g2 f16 e d c
    b4 c f e
    d c
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro e molto Vivace [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*35 
  r2 r4 r8
  g
  c b16 c d c b c  e8 d16 e f e d e
  g8 g g4. a16 g f e d c b4 r4 r2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  r16 c f a  c c c c  bes bes, es g bes bes bes bes
  a c, f a c c c c   bes bes, es g bes bes bes bes
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*314
   f16 f f8. a32[ g] a16 g32 f e16[ d] \grace cis8 d4 d8
   d32 c b c g'8. e16[ c bes]  gis[ a] r8 r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro molto [Tutti]"
  \time 4/4
    <g e' c'>4 r
    <d b' g'> r
    <g c e> r8 g c d e f
    g4 e f d
    e32 g c8. r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro molto [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*44 
   r2 r4 r8 g
   c4 e8. f16 g8 g a b
   c a g f f4 e
   a8 f e d  g e d c
   b f' f e d b a g
   c4
}
