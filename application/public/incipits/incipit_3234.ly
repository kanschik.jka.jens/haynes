\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Lasset dem Höchsten ein Danklied erschallen [BWV 66/3]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key d\major
                      \tempo "o. Bez."
                      \time 3/8
                      % Voice 1
                      d8 d16 e fis g
                      a8 b cis
                      d16 cis  d8 b'
                      cis,16 b cis8 a'
                      b,16 d g e fis g
                      a, cis fis d e fis
                 
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key d\major
                     \set Score.skipBars = ##t
                        % Voice 2
                    d,8 d16 e fis g
                      a8 b cis
                      d16 cis  d8 b'
                      cis,16 b cis8 a'
                      b,16 d g e fis g
                      a, cis fis d e fis
                      
                  }
>>
}


\relative c {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key d\major
   \tempo "o. Bez."
  \time 3/8
     \set Score.skipBars = ##t
   R4.*32 d8 d16 e fis g
   a8 b cis
   d fis, g 
   g16 fis fis8 r8
  
   

}