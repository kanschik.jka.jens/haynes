\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro maestoso"
  \time 4/4
    \override TupletBracket #'stencil = ##f
      c2 \grace { d32 c b c} f8 e d c
      \times 2/3 { c8 e g} g2 f8 e
      \grace gis8 a8 g16 f e d c b
}
