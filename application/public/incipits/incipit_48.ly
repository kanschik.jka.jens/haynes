 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. "
  \time 2/2
  \partial 8
  a8
  d2. g,4 fis r b4. g8
  e4 r8 e e2 a g g fis4
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Allegro"
  \time 2/2
  \partial 8
  a8 d2. cis8 d e4~ e16 fis d e fis8 e16 fis d8 a'
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Adagio"
  \time 3/4
  a'4 fis d b'4. cis8 a d
  g,4. g8 fis g fis4 fis g
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4. Menuett"
  \time 3/4
    d4 cis d e d8 cis b a
    b4 g' fis8 g
}