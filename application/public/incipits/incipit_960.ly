 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 4/4
  bes4 bes2 bes4
  \grace as8 g4. as8 bes4 bes
  \times 2/3 {c8[ d es]} es2 d8 c
  \grace c16 \times 2/3 { bes8[ as g]} g4 r2
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 3/4
  g8.[ \grace {a32[ g fis] } g16] bes8 g d' bes
  \grace a16 g8. fis16 \grace fis8 g4 bes
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Rondo. Allegro"
  \time 6/8
  bes8. as16 g8 g as bes
  bes c as f4 as8
  g8. f16 es8 es d es f4. bes,4
}

