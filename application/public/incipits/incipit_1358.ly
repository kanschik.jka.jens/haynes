\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Vivace"
  \time 4/4
  c4. es8 d16 es f4 d8
  es c g' f16 g as g g as   as g g f
  f8 bes, f' es16 f g f f g   g f f es
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Largo"
  \time 3/4
  r4 c8. es16 d8. es32 f
  es8. d16 es8. g16 f8. es16
  d4 f8. es16 d8. es32 f
  es8. f16 d4. c8
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Presto"
  \time 2/4
  r8 c8 g' g
  g d g g 
  g es g g as f16 g as8 g16 f
}
