 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 2/2
    bes8 c16 d c8 bes bes a r c16 d
    es8 es es es es4 d
    d8 es16 f es8 d d c r
  
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegro"
  \time 2/2
    f8 bes,16 a bes8 f' g16 f g8~ g16[ g f es]
    f f es d   es[ es d c]  d c bes a bes4~
    bes1~
    bes4. a8 bes4 r8
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Siciliano"
  \time 6/8
    d8. c16 bes8   bes8. g'16 bes,8
    bes4 a8  a4 bes8
    c4 c8  c8. bes16 a8
    bes8. a16 g8  g4 r8
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Allegro"
  \time 3/8
      f8 d bes
      f' d bes
      g' f4
      f8 es4
      es8 c a
      es' c a
      f' es4
      es8 d4
}