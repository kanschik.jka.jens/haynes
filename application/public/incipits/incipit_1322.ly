 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Grazioso"
  \time 4/4
    es,8 es4 g16. bes32 es16 es32 f g16 f es8 r
    g,8 g4 bes16. es32 g16 g32 as bes16 as g8 r
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Largetto"
  \time 2/4
  es8 es4 f16 es es d d4 g,8
  c16 f8 es16 d' c8 bes16
  es,8 f16 d es8 r
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Menuetto"
  \time 3/4
  es,2 d16 es f g as4 f8 d es4
  bes'' as g
  f4. g16 as g4
}

}