 \version "2.16.1"
         #(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \time 3/4
   g8 a8 g4 g g8 g b g d'4 b8 g d' b g'4 g8 d b' g d'4
}
