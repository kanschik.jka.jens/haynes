\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Choralbearbeitung (Tenor): Sie stellen uns wie Ketzern nach [BWV 178/4]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key b\minor
                      \tempo "Choral"
                      \time 4/4
                      % Voice 1
                      r16 fis16 e fis b,8 cis d4 r16 d16 e d
                      cis8 fis~ fis e~ e 
                
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key b\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                    r2 r16 b16 a b fis8 gis
                    a4 r16 g16 a g fis8 b r16
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key b\minor
   \tempo "Choral"
  \time 4/4
     \set Score.skipBars = ##t
    R1*2 r2 r4 d4
    cis b8 cis d e fis4 e e d r4
    
    
    }