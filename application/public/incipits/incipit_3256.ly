\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key g\minor
                      \tempo "o.Bez."
                      \time 4/4
                         d4~ d16 g, fis g es'8 es es16 f32 g f16 es
                       es8 d~ d16 bes a g c8[ c]
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key g\minor
                        % Voice 2
                       r2 g4~ g16 c, b c
                       a'8 a~ a16 bes32 c d16 es
                       fis,8 es'~ es16 c a fis'
                  }
>>
}
