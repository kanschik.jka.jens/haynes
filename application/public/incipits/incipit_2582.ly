 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Largo"
  \time 6/4
      << {
      % Voice "1"
          R1.
          r4 d bes' a g f
          e8 bes' a g f e f
         } \\ {
      % Voice "2"
          r4 a, f' e d cis
          d1.~
          d4 cis4. b16 cis d4
      } >>

}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
    a8
    d e4 f8~ f e16 d e8 a,
    d e4 f8~ f8 e16 d
    e8
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Affettuoso"
  \time 6/8
  \partial 8
    d8
    g8. a16 bes8 g8. fis16 g8
    g4 fis8
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "4. Presto"
  \time 2/2
    a'2 d, bes'2. a8 g
    a4 g f a
    g2. f8 e
    f4
}