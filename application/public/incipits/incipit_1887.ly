\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Serenade ou Concert - 1. Airs de Fanfares " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Ouverture"
  \time 2/2
c2 r8 g8 c d
e4 c g'4. g8 g2 d4 e8 f
e4 g c4. g8
a4. a8 a4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Premier Menuet"
  \time 3/4
e4 c g
c4. d8 e4 f f4. e16 f
g8 f g a g4
a bes8 a g fis
g 

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Fantaisie"
  \time 4/8
  \partial 4
r4 r2 r4 g8 c 
c g c e
c g c c 
c2 c c4


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Menuet en Rondeau"
  \time 3/4
g4 c d
e4. f8 g4
e f8 e d c
d c b a g4 

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Premier Passepied"
  \time 3/8
e8 g e c g c
d d16 e f8
e c16 d e8
d e16 d c b 
c8 a16 b c8

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "6. Premier Courante"
  \time 3/2
\partial 8
c8 c4. g8 c4 d e4. f8
d4. e8 a,4 b c4. d8
b4. a8 g4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "7. Premier Menuet"
  \time 3/4
c8 d d2
c2 g'4
\grace f16 e4 \grace f16 f4. e16 f
g8 f g a g4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "8. Premier Rigaudon"
  \time 2/2
\partial 4
c4 g2 c
d2. e4
f e d c
d8 c b a g4 

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Serenade ou Concert - 1. Airs Tendres  " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Sommeil"
  \time 3/2
c4. b8 c4. d8 c4. d8
es4. d8 es4. f8 es4. f8 
g2 g,2. g4
c4. b8 c4. d16 es d4. c8
b2

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Corrente. Allegro"
  \time 3/4
  \partial 8

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Largo"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 4/4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Serenade ou Concert - 1. Airs de Champestres  " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Marche des Berges"
  \time 2/2
c2 e
d4 c b a
g2 a4 b
c d a b
c g c d 
e4. f8 f4. e8 e2 

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro"
  \time 4/4

}
  
  \relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Largo"
  \time 3/4

}
  
  \relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. "
  \time 6/8
  \partial 8

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Andante"
  \time 3/4

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Andante"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Andante"
  \time 3/4

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Allegro"
  \time 6/8

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Largo"
  \time 3/2

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Andante"
  \time 3/4

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Presto"
  \time 2/4

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 3/4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Largo"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Andante"
  \time 3/8

}

