 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Tempo largo"
  \time 4/4
    es4 es,8. g16 bes8 bes bes bes
    bes g16 es g bes es g bes,8 g16 es g bes es g
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Tempo giusto"
  \time 3/4
  es4 bes8 es g bes
  \times 2/3 { bes8[ a bes] } bes,2
  f'8. g32 as g4 f
  g8 es bes' bes, \grace c8 bes4
}
