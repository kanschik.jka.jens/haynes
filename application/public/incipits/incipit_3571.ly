\version "2.16.1"
         #(set-global-staff-size 14)
         
         \relative c'' {
  \clef treble
  \key  g\major
   \tempo " 1. Moderato [Tutti]"
  \time 4/4
  \partial8 d,8\p g8 b16. g32 a8 c16. a32 b16 (d) c (e) d8 g,\f fis-. e-. d-. c' b16\p (d) c (e)   
         }
         
 \relative c'' {
  \clef treble
  \key  g\major
  \tempo " 1. Moderato [Solo]"
  \time 4/4
  \partial8 d,8 g8 b16. g32 a8 c16. a32 b16 d c e d b' (a) g fis8 e d c 
  \override TupletBracket.bracket-visibility = ##f \tuplet 3/2 {b16 (c d)} \tuplet3/2 {c (e fis)}  
 }

 
         \relative c'' {
  \clef treble
  \key  g\major
   \tempo "2. Poco Adagio [Tutti]"
  \time3/4
  e,8 b e g' g16\p (fis) fis (e)
  e (dis) dis (c) c (b) b (a) a (g) g (fis) e\f
         
         }

 
         \relative c'' {
  \clef treble
  \key  g\major
  \tempo "2. Poco Adagio [Solo]"
 \time 3/4
  b2. b2. b'2. b2. c8 a \grace {g} fis4. a8 b g \grace {fis} e4. b8
         }
         
          
         \relative c'' {
  \clef treble
  \key  g\major
   \tempo "3. Allegro"
  \time 2/4
  d4 d d4. c8 b g a fis g a16 b c d e fis g4 g
         }