\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 4/4
  a'4. a8 bes4 a g4. a8 f4 d
  e8 a, a'4~ a8 d, g4~ g8 a f8. e16 e4 r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Vivace"
  \time 4/4
  r8 a a a f4. f8 e f16 g a4
  d,8 e16 f g a g f e4. e8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio"
  \time 3/2	f2 a d,
  cis2. d4 e2
  a, f' f
  e4. f8 e4. f8 g4. a8 f1 r2
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro"
  \time 4/4
  f4. f8 e4. e8 f4 g8. f16 e4 f
  
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Grave"
  \time 3/2
  f2. g4 f es
  d2 d d d4 c c2. d8 es
  d4 c d es d2
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 4/4
    bes'4 f4 g4. c8
    a f bes2 a4
    bes r r2

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Grave"
  \time 4/4
  c'4. bes8 a4 g
  a f d'4. d8 c4. bes16 a bes4. a16 g a8. bes16 a8. bes16 a8. c16 bes8. a16
  g4 r r2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 4/4
  r2 r8 f e8. e16 d4 c8. c16 d e f8 f e f4 r
}
  
