 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Ouverture"
  \time 4/4
  g'4~ g16 a fis g
  d8. g16 bes,8. d16 g,2 c4. c8
  
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Très vite"
  \time 4/4	
  \partial 4
  d4
  g, es' d c bes c8 bes a bes c a
  bes4 g
  
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Sans Souci"
  \time 3/8
  g'8 fis g
  d4. c8 bes a bes g d'
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Hornpipe"
  \time 3/4
  g'8 d4 es d16 c
  f8 bes,4 c bes16 a
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Gavotte"
  \time 4/4
  \partial 2
  d8 c d es
  d4 d es d
  g2 f
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "5. Passepied"
  \time 3/8
  \partial 8
  d8 g fis16 g a fis g4 a8 bes f es
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "6. Irlandoise - Vitement"
  \time 6/8
  \partial 8
    g'16 fis
    g8 d fis g d fis
    g4. d4 f16 e
}

