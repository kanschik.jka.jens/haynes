\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Bald zur Rechten, bald zur Linken [BWV 96/5]" 
    }
    \vspace #1.5
}
 
 
\relative c''' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key d\minor
                      \tempo "o. Bez."
                      \time 3/4
                      \partial 4
                      % Voice 1
                r4 r4 r4 g8. e16
                cis8. d32 e a,4 bes'8. a16
                f8. d16 fis4. g8 
                g2 g,8. bes16
                es8. d16 es4 r4
                 
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key d\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                  r4 r4 r4 d8. bes16
                  g8. a32 bes e,4 bes''8. a16
                  f8. d16 fis4. g8 g2 d,8. g16
                  bes8. a16 bes4 r4
                      
                  }
                  
                  \new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Continuo"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key d\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                 d,,4
                 d'8. cis16 d4 bes8. g16
                 e8. f32 g cis,4 cis'
                 d c2
                 bes
                      
                  }
                  
>>
}


\relative c {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key d\minor
   \tempo "o. Bez."
  \time 3/4
  \partial 4
     \set Score.skipBars = ##t
  r4 R2.*7 r4 r4 d8. f16
  bes8. a16 bes4 g8. e16
  cis8. d32 e a,4 bes'8. a16
  f8. d16 fis4. g8
  g2 r4
  
   

}