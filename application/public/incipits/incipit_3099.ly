\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Geliebter Jesu, du allein [BWV 16/5]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe da Caccia"
  \key f\major
   \tempo "o. Bez."
   \time 3/4
 r4 r8 c8 a16 f' d bes
 c e f8~ f16 c d bes a f' d bes
 c a g8~ g16
  
   
  
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key f\major
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
   R2.*16  r4 r8 c8 bes16 a32 g a16 f
   d'8 c4 bes16 a f'8 d16 c 
   c4 r4
 
}



