 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
   \time 3/8  
      e8 f16 e d c
      d8 e16 d c b
      c b a8 a'~
      a gis8. fis32 gis
      a8 e
}
