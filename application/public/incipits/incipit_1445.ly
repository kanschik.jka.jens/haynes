 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro moderato"
  \time 4/4
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
    c4 a'8. f16 \times 2/3 { e16 f g} f8 r4
}