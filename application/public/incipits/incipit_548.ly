 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

 

 
\relative c'' {
  \clef bass
  \key f\major
   \tempo "1. Adagio [Bassoon]"
  \time 2/2
  f,,8 c16. f32 c'8 bes16. c32 a8. g16 f16. g32 a16. bes32
  g8 c, c' c c16. bes32 bes16. d32 bes,8 bes'8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio [Oboe]"
  \time 2/2
  \set Score.skipBars = ##t
   R1*5 
   c4. f16 g \grace f8 e4. f8
  g bes, bes bes bes16. g32 a8
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/2
  \partial 8
  c8
  f16[ e f g] f8 c a f a c
  f16[ e f c] g f g c, a'8 g 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Adagio"
  \time 3/4
  a'8. g16 a8. g16 f8. e16
  f8. e16 f8. e16 d8. c16
  d8. cis16 d8. cis16 d8. e16
  cis4 a
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 2/4
  \partial 8
  c8
  c16 bes a bes c8 d
  d c f4~ f8 e16 f g8 bes,
  bes a
}
