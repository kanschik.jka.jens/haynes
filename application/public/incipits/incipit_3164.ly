\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Gott ist gerecht in seinen Werken [BWV 20/5]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key bes\major
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                      r2 r8 d8 es16 d c bes
                      f'2~ f8 f g16 f es d 
                      \grace es8 g4 f16 es d c  \grace d8 f4
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key bes\major
                     \set Score.skipBars = ##t
                        % Voice 2
                    R1 r8 c8 bes16 a g f
                    bes8 bes a bes
                    bes4 c8 g a4
                       
                        
                
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 3"
                     \set Staff. shortInstrumentName = #"Ob 3"
                     \key bes\major
                     \set Score.skipBars = ##t
                        % Voice 3
                 R1 r8 a8 g16 f es f
                 d8 f es f
                 es4 g8 es f4 
                       
                        
                
                  }
>>
}


\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key bes\major
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    R1*9 r8 bes8 d, f bes,4 r8 bes'8
    \grace bes8 a4 r4 r8 d,8 es16 d c bes 
    f'2 r4
   
    
    
    }