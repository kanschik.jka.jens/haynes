 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro Moderato"
  \time 4/4
  \partial 8
  d8
  a' a a a  a4. g8
  fis8. d16 g e d cis d8 a'16 b a8. g16
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  g8
  d'4~ d16 d e d d4 b'16 d, e c
  d8 b' b d16 b a4 a16 f a f
}
 

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 3/4
      fis4 a a
      a fis d
      b4. cis8 \grace e8 d8 cis16 b 
      a4 g fis g
}
 
