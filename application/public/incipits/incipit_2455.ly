 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  f,4 c a'4. g8
  f4 c c'4. bes8 a4 f f'4. e8
  \grace e8 d4 c r2
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Adagio"
   \time 3/4
   g'4 f8 e c' a
   g4. f8 e g
   a,8. f'16 e4 d
   d2 c4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Rondo"
  \time 2/4
  a'8 a16 f bes8 bes16 g
  c4 d16 c bes a
  g8 g16 e a8 a16 f
  bes4 c16 bes a g
}