\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/4 
    g4 b8 c d d, r4
    b'4 b a r8 d
     a a a a
}


