\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
    f,1 a8 g f e f2
    a8 g f e f a a c
    c4 bes a r
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Larghetto [Tutti]"
  \time 4/4
  f2 bes16 f8. es16 d8.
  \grace d8 c16 bes  c d es2 f16 es d c
  f8 d bes bes bes4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Larghetto [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*19
   f2 bes16 f8. es16 d8.
   c8. d16 es2 \grace f8 es8 d16 c
   f8 d bes2 \grace d8 c8 bes16 a
   \grace c8 bes8. a16 bes4 r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Tempo di minuetto"
  \time 3/4
  c2. d d8 c bes a g a
  a4 g r
  c8 d e f g a
  c,4 bes a
}