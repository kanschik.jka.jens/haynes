\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  c2~ c8 b16 c d c b c
  b8 d g4~ g8 f e d
  c4
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio"
  \time 3/4
  c,4 es r8. c16
  b4 as' r8. b,16
  c4 d es
  f8. es16
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Allegro"
  \time 2/4
  \partial 4
  c16 b c d
  e8 e d d f4. e8
  d d \grace d16 c8 b16 c
  b8 r
}
  
