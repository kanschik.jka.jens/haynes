 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef alto
  \key d\major
   \tempo "1. [Allegro]"
  \time 3/4
  a2 \grace b16 a8 g16 fis
  e fis cis d d4 r8 d
  e8. fis16 g8 g g g
  \grace a8 g4 fis r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Rondeau. Allegro assai"
  \time 2/4
  d,4 \grace g16 fis16 e fis g
  a8 a a a
  \grace b16 a8 g16 fis d'4~
  d cis
  d8 a
}