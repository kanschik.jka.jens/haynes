\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Fromme Musen! meine Glieder [BWV 214/5]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe 1u2 unisono"
  \key b\minor
   \tempo "o.Bez."
   \time 3/8
 fis8 d'16 cis b ais
 b fis g e fis8~
 ~fis fis'16 e d cis
 d b cis ais b8~
 ~b16 g' fis e fis d
 b e d cis d b
 a g fis e cis'8
 ais4 r8

   
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key b\minor
   \tempo "o. Bez."
   \time 3/8
   \set Score.skipBars = ##t
  R4.*16 fis8 d'16 cis b ais
  b8 fis g
  fis16 b ais b fis e
  e8 cis16 d b8
     
     
}



