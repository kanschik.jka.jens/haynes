\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro Moderato"
  \time 2/4
  d8 d d16. e32 fis16. g32
  a16. g32 a4 d16. fis,32
  \grace fis16 e8. b'16 a g fis e
  fis e d8 r4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Largo Affettuoso"
  \time 3/4
  fis4 e8 fis8 g4
  g fis r
  e8 g fis e d cis
  d4 \grace cis8 b4 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Minuetto"
  \time 3/4
  a'8 fis16 g a8 a d a
  a8 fis16 g a8 a d a
  g4 fis e
  fis16 g fis g a4 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Giga"
  \time 6/8

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Andante"
  \time 2/4
  \partial 8
  cis16. d32
  e16 cis b a e' cis a' fis
  fis8 e r a16 cis cis b b8~ b16 gis e d
  cis16. d32 e8 r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Affettuoso"
  \time 2/4
  \partial 8
  e8
  e a16 e e8 a16 e
  \grace e16 d8 d32 c d e d8d
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Minuetto"
  \time 3/4
  a4 a'2
  g16 a b8 \grace fis4 e2
  
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 4/4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro Moderato"
  \time 2/4
  \partial 8
  a8
  d fis \times 2/3 { fis16[ g a]}
  \times 2/3 { cis,16[ d e]} d8 fis
  \times 2/3 { fis16[ g a]}
  \times 2/3 { cis,16[ d e]} d16 d' cis b a8 g \grace g8 fis4 r8 
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Affettuoso"
  \time 2/4
  a'32 f16. d'4 a8
  bes32 g16. e'4 g,8~
  g8 a16 bes a g f e
  \grace e8 d4 r
}
  
  \relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Minuetto"
  \time 3/4
 \partial 8
 a'8
 a4 d r8 a
 a4 d a8 fis
 fis4 e8. a16 g8 fis16 e
 \grace e4 d2
}
  
  \relative c'' {
  \clef treble
  \key es\major
   \tempo "4. "
  \time 6/8
  \partial 8

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Maestoso"
  \time 2/4
  g'8 d4 c8
  b16 g d' b g' d b' g
  d8 d4 c8 \times 2/3 { b16[ a g]} g8 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Siciliano"
  \time 6/8
  g8. a16 g8 g'8. fis16 e8
  e4 d8 d4 c8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Minuetto"
  \time 3/4
  g4 b c8 d16 e
  d8 b' a g fis g
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Allegro"
  \time 6/8

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Sostenuto"
  \time 4/4
  \partial 4
  g4
  c e8. f16 g4 c8. g16
  a4 g r g8. b,16
  d8. e16 f4 r d8. b16 c8. d16 e4 r
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Andnate Affettuoso"
  \time 3/4
  c'4 g \grace bes16 as8. g16 
  g4 f8 es d c d f es d c b
  \grace b4 c2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegretto"
  \time 3/8
  c8. d32 e d16[ e32 f]
  e8 c d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Presto"
  \time 2/4

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante Grazioso"
  \time 2/4
  d8 \grace a'16 \times 2/3 {g16[ fis e]} d8 d
d8 \grace a'16 \times 2/3 {g16[ fis e]} d16 g b d
e,8 \grace d'16 \times 2/3 {cis16[ b a]} g8 fis
\grace fis8 g4 r16 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Amoroso"
  \time 2/4
  \partial 8
  g8
  c c32 b c d
  e8 e32 d e f
  g8 g4 f8 e8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Minuetto"
  \time 3/4
  d8 b r g b d d b r g b d e, c' b a g fis g2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Andante"
  \time 3/8

}

