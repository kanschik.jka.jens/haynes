\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Maestoso [Tutti]"
  \time 4/4
  g4 b8. g16 d'4 d,
  g r r8 f f f
  f16 e dis e e4 r8 e16 c g8 e'
  e16 d cis d d4 r8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Maestoso [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*24
   g4 b8. c16 d4 e8. fis16
   g1~ g
   g4 fis16 g a g g2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  g'8 c16 g \grace g16 f8 e16 d c8 f16 e e8 a16 g
  g8 f16 e \grace e16 d8 c16 b c8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Solo]"
  \time 4/4
  \set Score.skipBars = ##t
  R1*8
  \grace {fis16[ g a]} g8 c,32 e g c 
  \grace a8 g16 f32 e
  \grace g8 f16[ e32 d]
  d32 c b c c8~ c4~
  c16 g' f e \grace e16 d8 c16 b \grace d16 c8[ g]
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 2/4
  g4 c8. b16 \grace b8 a4 g
  g e'8. d16
  \grace d8 c4 b
}