\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro moderato"
  \time 2/4
  \partial 8
  d8
  g4 fis 32 g fis g fis16 e
  e d d4 c16 b
}


\relative c'' {
  \clef alto
  \key c\major
   \tempo "2. Adagio siciliano"
  \time 6/8
  g8. a16 g8 c8. b 16 a8
  g8. a16 f8 e r r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro moderato"
  \time 2/4
  g'8 fis g fis
  e d c b
  b a gis a
  a2
}
