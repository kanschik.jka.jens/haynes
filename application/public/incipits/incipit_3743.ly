 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 8.
  f,16 g a
  bes f es f c' a bes c d8 r r16 f, g a
  bes f es f c' a bes c d4 r
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Largo"
  \time 3/2
  bes1. bes4 c bes c bes c
  a bes a bes a bes f g f g f g es1.
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Scherzo"
  \time 2/4
  d8 bes r4 d8 bes r4
  \times 2/3 { f8[ g f]} \grace f16
  \times 2/3 { es8[ d es]} 
  \grace es8 d4 c
}

