\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude"
  \time 2/2
  r8 c' b a  g f e d
   e g f e d c b a
   g a b c d e f d
  
}
