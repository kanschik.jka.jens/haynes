\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/4
    bes'4 f bes, r8 bes
    a bes es c
    \grace c8 d4 r8 bes
    a bes es c
    \grace c8 d4 r8
    
}
