\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Sonate in g] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Ouverture"
  \time 4/4
  d4. d8 g4. g8
  g2 f4. g16 d
  es4. es8 es4. d8
  d2 c4. d8
  \grace c8 bes4. c16 bes bes4. a8
  a4 

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. le Ballet"
  \time 2/2
  bes4 g d'4. g8
  fis4 a d,8. c16 bes8. a16
  bes4 g8. g16 c8. bes16 bes8. a16
  a4. 
  
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Bourée"
  \time 2/2
  \partial 4
  g4 g d g fis8 e 
  d e d c b4 d
  c b a g8 fis
  g4. a8 b4 c
  d g fis4. g8
  a2 r8
  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Sarabande"
  \time 3/4
  d4 g4. a8
  fis4. e8 d4
  e c4. b8
  b4 a8. g16 g4
  b \grace b8 c2

  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Gavote"
  \time 2/2
  \partial 2
  b4 d
  g,2 c4 e
  a, b c \grace b8 a4
  \grace c8 b4 \grace a8 g4 b d
  g,2

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "6. Les Forgerons"
  \time 12/8
  r4 r8 r8 d8 c b a g d' g fis
  g a b a b c b g b
  e,4.
  r8 c8 e a, a' e fis g a e fis d
  cis e cis a d a

}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Sonate in e]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allemande"
  \time 4/4
  r4 r8 r16 e16 e4~ e16 fis g a
  b8 a16 g fis8 b g e b8. cis16
  dis8 e4 dis8 e4~ e16 e c b
  c4 c8. b16 b4

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Courante"
  \time 3/2
  \partial 4
  r8 b8
  b4. a8 g4 e c'4. d8
  b2. b4 e4. fis8
  dis4. e8 fis4 g g4. fis8 fis2.

}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "3. Sarabande"
  \time 3/4
  g4 fis4. b8
  g4. fis8 e4
  e dis4. cis16 dis
  e4. b8 b4
  c c4. b8
  a4

}

\relative c' {
  \clef treble
  \key e\minor
   \tempo "4. Fugue"
  \time 4/4
  r4 e8 fis g a b fis
  g e g a b e4 dis8
  e g fis a g b4 a8
  b16 b a b fis b a b gis8 a b gis

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Sonate in a] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allemande"
  \time 2/2
  e4. e8 c8.[ d16] e8 d16 c
  b8 a a e g b c d
  e4. e8 \grace d8 c8.[ d16] e8 d16 c
  b8[ e] fis[ gis] a[ e] e a

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Sarabande"
  \time 3/4
  a4 f'4. g8
  e2 \grace d8 d4
  e c4. b8
  b2 e,4 a f'4. g8
  \grace f8 e2 d8. c16
  c8. d16 d4. c16 d
  e2.

}
  
  \relative c' {
  \clef treble
  \key a\minor
   \tempo "3. Gigue I"
  \time 6/8
  \partial 4.
  r8 e8 a
  gis4 a8 b c d
  c b a c4.
  b8 c a gis4 a8
  b4.

}
  
  \relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Le Papillon "
  \time 4/4
  \partial 4
  a8 e
  a16 b gis a b cis a b cis d cis d e8 a
  gis fis16 gis e8 a gis fis e d
  cis e d8. cis16 b a gis b

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonate pour la Flute à bec" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. lentement"
  \time 2/2
  r4 a4 d2
  d2. \grace e8 d8 e16 cis
  d2 \grace a8 a4 r4 
  r4 a4 f'2~
  f2. \grace g8 f8 g16 e
  \grace e8 f4 g a g8. f16 f2.

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Fugue"
  \time 6/8
  r4. r4.
  r8 d8 f e a e
  f d a'4 g
  a8 a, c b e b
  cis d4. cis4
  d8 f c d c8. bes16 a4

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Courante"
  \time 3/4
  \partial 4
  r8 a8
  a4. a8 d f
  e g f a d[ cis] d2 a4
  bes g4. f8
  f g a g f[ a]

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Fugue"
  \time 4/8
  r8 d8[ f g]
  a4 f
  g16 a g f e f g e
  f g f e d8 f
  e8 d4 cis8
  d8 d4 d8

}



