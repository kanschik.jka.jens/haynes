 \version "2.16.1"
         #(set-global-staff-size 14)
         
     \relative c'' {
  \clef treble
  \key  c\minor
 \tempo "1. Allegro"
  \time 2/4
    c8 c c c
    as'8. g16 f es d c
    g'8. f16 es d c b'
     }

     \relative c'' {
  <<
\new Staff { \clef "treble"   
                     \key es\major
                      \tempo "2. Largo"
                      \time 3/4
                        r2 es4 bes8 c
                        g f16 es
                        es'2~ es8 d16 es
                        f8 es d c16 d
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key es\major
                        % Voice 2
                       es4 bes8 c g f16 es es'4~
                       es4. d8 c bes a c
                       bes2
                  }
>>
     }

     \relative c'' {
  \clef treble
  \key  c\minor
 \tempo "3. Allegro"
  \time 3/8
         c8 c c
         c es16 d c8
         c g' c, c es16 d c8
         c c' bes as g f es d c
     }
