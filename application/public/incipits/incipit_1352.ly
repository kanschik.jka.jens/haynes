
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
  \tempo "1. Larghetto"
  \time 4/4
  g4 c8 d b4 r
  r8 c g'8. f32 g as8  \grace g8 f r4
  r8 g bes as16 g as8. g16  f e f g
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 2/4
     r8 g8 es'4
     d8 g16 f  g8 c,
     b g es'4
     d r
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Largo"
  \time 3/4
    c2. c4 g es'
    d g8 f es d 
    es4 c r
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 2/4
  g'4 c,
  b4. d8 
  g, f' es d 
  es4 d
}

