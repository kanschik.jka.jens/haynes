\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8
  d8
  g, a16. fis32 b8 c16. a32
  d8. c16 b8 r
  r4 r8 d16. b32
  b8. c32 a g8 a
  b8. c32 a g8 a
  g4
}
