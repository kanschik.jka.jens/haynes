\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef bass
  \key bes\major
   \tempo "1. Adagio"
  \time 3/4
  f,,4 bes c16 d es8
  \grace bes8 a2 bes4
  g f es16 d es8 d4.
}

\relative c'' {
  \clef bass
  \key bes\major
   \tempo "2. Allegro"
    \time 2/2
    r8 bes, bes bes   bes bes bes bes
    bes[ a16 g] f es d c  d[ c bes c]  d es f c
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Siciliana"
  \time 6/8
  d8. es16 d8 g f es16 d
  es8. f16 es8 d4 fis16 g
  c,4 fis16 g bes,8. c16 d8~
  d c bes bes[ a]
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
    \time 3/4
    bes4 d c
    d f es
    d bes' a8 g
    f4 es d 
}

