\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4	
       bes'2 f4. d'4
       bes4 r8 d16 f  es d c bes a bes c a
       f4 f f4. g16 a
       bes4 r8
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Larghetto Cantabile"
  \time 2/2
  \partial 4
    bes8. bes16
    es4. f8 g as bes c
    bes f f2 as8 g   g16 f e f  g[ f as f e4]
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Rondo. Allegretto Grazioso"
  \time 4/4
  \partial 4
      f,8. f16
      bes8 c a bes \grace d8 c8 bes
      es2 d8 r16 d
      c8 es c a bes8. c32 d
      c2
}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro Expressivo"
  \time 4/4	
  \partial 8
     a8
     a4 \grace c8 bes8 a a4 a
     a8 d d2 c4
     c8 bes a bes bes4. \grace { c32 bes a bes} e8
     e4. a,8 a4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Larghetto sostenuto ma con moto"
  \time 8/8
     f8. g16 f8 f4 f8
     f4 d8 bes4 d8
     c4 c8 d16 c bes a bes c
     d4. bes8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Presto ma non tanto"
  \time 2/4
  \partial 8
    a'8
    a a a16 bes a gis
    a8 a a16 bes a gis
    a8 a g g
    \grace g8 f4 e8
}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro Maestoso"
  \time 4/4	
  \partial 8
     c8
     f2 c4. c8
     a2 f4. f8
     g2 f8 e d c f4 a8. a16 f4 r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Larghetto sostenuto"
  \time 4/4
    r4 r4 r8. a16
    d8. f16 a8 gis16 a  bes a f d
    cis8. \grace { d cis b cis} d16 e4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegretto con Brio"
  \time 2/4
  \partial 4
      f,8 g a bes32 a g a bes8 g
      f f g g \grace bes8 a8 g16 a  bes a  g f
      c8 c f
}

