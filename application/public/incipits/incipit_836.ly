\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 4/4
  r8 a16. bes32 a8 a16 bes c8 a bes16 bes a g
  a8 d16. e32 d8 d16 e f8 d e16 e d cis 
  d8 a
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\minor
                      \tempo "2. Allegro"
                      \time 2/4
                      % Voice 1
                      R2*3
                      r16 a'16 e a e a d, a'
                      cis, a' e a e a  d, a'
                      cis,8 a
                  }
\new Staff { \clef "bass" 
                     \key d\minor
                        % Voice 2
                        d,,16 cis d e f e f g
                        a g a b c b c a
                        d cis d e   d f e d
                        cis8 a
                  }
>>
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Cantabile"
  \time 3/4
  \partial 4
  f8. g16
  f4 c f8. g16
  f4 c f8. es16
  d4 c bes
  bes a r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 3/8
  d16 a b cis d cis
  d a d a d a
  e' a, e' a, e' a, 
  f'8 r r
}
