\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble	
  \key c\major
   \tempo "1. Largo"
  \time 4/4
    \partial 8
    c8
    g' g g g g2~ g g8 g16 a g a g a
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4
    e4 f e d e8. f32 e f8. g32 f e8. f32 e d8 g,
    c c c g' d d d e16 d
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Affetuoso"
  \time 3/4
    \partial 4
    c8. d16
    e4 e e8. f16 g4 g
  
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 2/4
  \partial 8
    g8
    g e16 f g8 g g d g g'
    f e d c
    b a16 b g4
}
