
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Actéon Cantate"
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Air"
  \time 2/4
\partial 4
r16 g16 f g
\grace f8 e8 \grace d8 c8 d \grace c8 b8
c g r16 d'16 e f 
e8. f16 g8 f16 e
d4 r16
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Air. Cor de Chasse"
  \time 6/8
c4 g8 c4 c8
c4.~ c4 d8
e c d e f e 
d4 g,8 d'4 d8
d4 e8 e4 d8 d4
 
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "6. Air "
  \time 6/4
\partial 2.
a4. b8 gis4
a2 c4 b2 d4
c2 \grace b4 a4 e'2 e4
e2 f4 d4. c8 d4
e2 e,4 e'2 e4 e d c d2 d4
d c b  
}

