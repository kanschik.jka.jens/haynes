 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Aria. Adagio"
  \time 4/4
    r8 f16 es d c c bes f8 a bes a
    a f'16 es d c c bes f8 bes c bes
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Aria"
  \time 4/4
  r4 r8 d
  bes c16 d g, bes d g
  fis8 g16 a
  d,8 d es16. d32 es16. d32 es16 g c c,
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Aria"
  \time 4/4
  r8 f, bes16 d c es  d8 d d16 f es g
  f8 f f16 d es f g8. as16 g16 f es d
  c4 r4 r2
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Menuett"
  \time 3/4
  \partial 8
  
}