 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  f,8
  bes a16 g f8 d' c a16 bes bes8 f
  f'16 d c bes  bes' f es d d8 c r
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Siciliano"
  \time 6/8
   \partial 8
   d,8
   g8. a16 bes8
   a8. bes16 g8
   g4 fis8 r r es'
   d8. es16 c8 a'8. g16 fis8
   fis4 g8 r r
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 6/8
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
    f,8 bes c  \times 2/3 {d16 c bes} f'8 r
    d8 es c  \times 2/3 {d16 c bes} bes'8 r
    
}
 