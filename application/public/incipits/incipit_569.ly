 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4		
  \times 2/3 { c8[ d bes]}  \times 2/3 { a8[ bes g]}  f4 r 
  \times 2/3 { c'8[ d bes]}  \times 2/3 { a8[ bes g]}  f4 r 
  e8 g4 bes8 f a4 c8
}

 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Solo]"
  \time 4/4		
     \set Score.skipBars = ##t
   R1*65
   r2 r8 c f a
   b c c2 a8 f
   \grace g8 f8 e e4~ e8 c e g
   a bes a g f e d c
   f \grace g8 f16 e f8 g a4 r
}


\relative c'' {
  \clef bass
  \key f\major	
   \tempo "2. Rondeau. Allegretto"
  \time 2/4
  f,,16 \grace g8 f32 e f16 g a bes c cis
  d8. bes16 f8 r
  d'16 \grace e8 d32 cis d16 e \grace g8 f8 e16 d
  c8. a16 f8 r
}
