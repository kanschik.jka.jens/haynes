 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Spirituoso [Tutti]"
  \time 4/4
  \partial 8
  g'8
  g4 r16 d b d g4 r16 d b d 
  g8 g, d' d, g[ g,]
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Spirituoso [Solo]"
  \time 4/4
  \partial 8
  r8
   \set Score.skipBars = ##t
   R1*17 
     g4 r8 d' d4 c16 b a g
     fis8 g r g a b c d16 c
     b a g8 r
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. Adagio [Tutti]"
  \time 6/8
  e,8 c' c c d, d
  d b' b b b, b
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. Adagio [Tutti]"
  \time 6/8
     \set Score.skipBars = ##t
   R2.*11 
   e,8 c' c c b r
   e, e' e e dis r
   b g' g fis d b
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Largo"
  \time 3/4
  <d, b' g'>4 d'2 b8 e d c b a
  <g, d' b'>4 a'2
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}