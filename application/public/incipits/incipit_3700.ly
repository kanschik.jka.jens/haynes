 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4  
  \partial 16
      bes16
      d,2 es
      g8 f f4. bes8 bes bes
      d,2 es
      g8 f f2 r4
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "2.Andante"
  \time 3/4  
    g'2 f8. d16 
      \override TupletBracket #'stencil = ##f
      es2 \times 2/3 {bes8 es g}
      bes2 as8. f16
      g2 \times 2/3 { bes8 g es}
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Rondo. Allegro"
  \time 6/8 
  \partial 8
    \override TupletBracket #'stencil = ##f
    \times 2/3 { f16 g a}
    bes8 bes bes   bes bes bes
    bes a g  f g a
    bes4 bes8 c4 c8
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Adagio"
  \time 3/4
    e4. bes'8 a16 g f e
    f8. g16  gis8 a8. bes32 a g32[ f e d]
}
