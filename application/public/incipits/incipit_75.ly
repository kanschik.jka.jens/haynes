\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. [Allegro]"
  \time 2/2
  r4 f,  a c 
  f c a f 
  c'2 \times 2/3 { bes4 c d }
  c2 f,4 c'
  f e f8 a g f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Cantabile"
  \time 3/4
  c4 d8 f e g16 f
  f2 g4
  a bes8 g a f e4.
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Aria Allegro"
  \time 2/2
  \partial 4
  c4
  f c r8 c d e 
  f4 c r c g' c, r c16 d e f 
  g4 c,
}

