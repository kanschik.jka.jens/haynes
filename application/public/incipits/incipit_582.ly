\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "Doux"
  \time 3/4
\partial 4.
r8 r4
r4 r8 d8 a4
d2 fis4
e4. d8 cis4
d2
}
