\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 2/4
    g4 d16 bes' a g  a4 fis g
}
