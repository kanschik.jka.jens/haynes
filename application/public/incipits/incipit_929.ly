 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "1. [Andante}"
  \time 4/4
  f,4 bes8 bes bes4 a8 es'
  d8. c16 bes4 d f8. f16
  es4. a,8 bes8. c16 d4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. [Allegro]"
  \time 2/2
  f4. es16 f d4 bes es g f8 es d c
  d c d4 c f~ f es2 d4 c2 bes4
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. [Allegro]"
  \time 3/4
  r4 r d8 c
  c2 f8 es
  d2 f,8 f g2 a4 bes4.
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. [Allegro]"
  \time 2/2
   f2 g
   f4 es8 d es4 f bes,8 r16 f' d8 bes
   f'8 bes g f e4 f2 e4 f4
}
