 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
    g'2 g8 c e, g
    g4. e8 c4 r8 c
    cis d d d  d4. e16 f
    f2
}

 \relative c'' {
  \clef treble
  \key f\major
   \tempo "2. o.Bez."
  \time 3/4
    f,2. g8 f bes a d c
    bes4 a
}
 
 \relative c'' {
  \clef treble
  \key c\major
   \tempo "3. o.Bez."
  \time 2/2
    g'4. c8  e,4 g
    f a d,2
    e4. c8 a4 d
    c8 b d b  g a16 b c d e f
}
 