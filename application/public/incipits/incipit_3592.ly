
\version "2.16.1"

#(set-global-staff-size 14)

\relative c' {

\clef "treble"   
  \key f\major
    \tempo "1.Grave"
      \time 4/4
      f8 c a f' e8. g32 f e8 r8
      bes'8 g e bes a8. c32 bes a8 r8
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegretto"
  \time 4/4
 c4. bes8 bes4 a
 r8 c8 c bes bes4 a8 r8
 bes'16 g e c bes g e bes a c f a c f a f
 d g bes a g d g f f4 e8 r8
  

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Rondo Allegretto ma moderato"
  \time 6/8
  c8. d16 c8 a d c
  c4. bes4 bes8
  bes8. c16 bes8 g c bes
  bes4. a4 f8
  g es f g es f bes4 r4 



}

