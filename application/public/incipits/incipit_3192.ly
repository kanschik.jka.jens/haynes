\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Das Unglück schlägt auf allen Seiten [BWV 139/4]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Ob. d'amore 1+2"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key fis\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                      \override TupletBracket #'stencil = ##f
                      \override TupletNumber #'stencil = ##f
                 r4 fis4~ fis8.[ \times 2/3 {eis32 fis gis]} b,4~
                 ~b8.[ \times 2/3 {ais32 gis fis]} e'4~ e8.
                 
               
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \set Score.skipBars = ##t
                     \key fis\minor
                        % Voice 2
                 r16 fis,16 a cis fis cis fis a gis gis, cis eis gis b gis eis
                 cis fis cis ais fis cis fis ais 
                       
                
                  }
                  
                 
>>
}


\relative c {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key fis\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    R1.*12 r8 r16 fis16 a8. fis16 cis'4 r4
    r8 r16 fis,16 ais8. fis16 b4 b
     r8
                   
   
    
    
    }