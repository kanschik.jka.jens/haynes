\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Bereite dich, Zion, mit zärtlichen Trieben [BWV 248i/4]" 
    }
    \vspace #1.5


}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Ob. d'amore + Vn.1"
  \key a\minor
   \tempo "o. Bez."
  \time 3/8
  \partial 8
     \set Score.skipBars = ##t
   e8 a c b
   c gis a
   g16 f e d g8
   e \grace d8 c c' b c16 b a gis a8 b c
   d c16 b c8
   \grace c8 b8.
   
    
    
    }
}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key a\minor
   \tempo "o. Bez."
  \time 3/8
     \set Score.skipBars = ##t
    R4.*15 r8 r8  e8 a c b
   c gis a
   g16 f e d g8
   e c c' b c16 b a gis a8 b c
   d c16 b c8
   \grace c8 b8.
   
    
    
    }