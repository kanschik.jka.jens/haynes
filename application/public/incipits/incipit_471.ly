\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'Io vi chiedo' " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "Andante"
  \time 3/4
    c4 d4. c16 d
    e8 f e f g4
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'Sol con dolce mormorio' " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "Spirituoso affetuoso"
  \time 4/4
  r4 a8 bes c16 bes a8 d16 c d e
  f8 f, d'8. c16  bes a bes8 r c16 bes
  a16[ a32 bes c16 d]
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'Rendetemi il mio ben'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "Allegro"
  \time 12/8
    << {
      % Voice "1"
      r4 r8 r r g c4 d8 e4 f8
      d4. r8 r e c4 b8 c4 d8
      b4 c8
         } \\ {
      % Voice "2"
      g'4. c, r4 r8 r4 c8
      b4 a8 b4 g8 a4 g8 fis4 a8
      g4.
      } >>
}

