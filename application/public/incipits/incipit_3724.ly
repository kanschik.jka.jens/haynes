 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\minor
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  bes8
  es16. ges32 ges16. bes32 bes8 ges16 es 
    es8 d16. g32 \grace g16 as4
  as8 ges f16. c'32 c16 c r2
}
