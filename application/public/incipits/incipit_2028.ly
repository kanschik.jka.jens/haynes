 \version "2.16.1"
         #(set-global-staff-size 14)
         
     \relative c'' {
  \clef treble
  \key  d\major
 \tempo "1. o.bez."
  \time 2/4
    \override TupletNumber #'stencil = ##f
  \override TupletBracket #'stencil = ##f
    d8. cis32 b a8 b~
    b a4 d8
    cis16 d e4 g,8
     }
