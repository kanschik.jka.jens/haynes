\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Andante e Spiccato [Tutti]"
  \time 4/4
  d8 d, r d e a, a' e
  f d r bes' c, a' bes, g'
  a,4 r16 a b cis d e f g a8 a, d4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Andante e SPiccato [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*3 
   d4 r16 d e f  e8 a, a'16 g a e
   f8[ d]
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio [Tutti]"
  \time 3/4
  d8 d d d d d
  e e e e e e
  e e e e e e
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*3 
    d8 f f a a bes
    bes4 r r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Presto"
  \time 3/8
  d16 a d e f d
  e a, e' f g e
  f e d e f g 
  a4 a,8
}