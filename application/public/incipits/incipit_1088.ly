\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante [Oboe]"
  \time 2/2
  \partial 8
  f,8
  a4. bes8 c d e f
  f4 e2 r8 d
  c4. d8 e f g a16 bes
  bes4 a2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante [Sopran]"
  \time 2/2
  \partial 4
  f,4
  a2 c4 c8 f
  f4 e2 bes4
  e4. f8 g4. bes,8
  bes4 a
}