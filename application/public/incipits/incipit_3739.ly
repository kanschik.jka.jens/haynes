 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  f16. d'32
  d8. c32 bes \grace bes8 a8. bes32 c
  bes8. a32 g f16 d' a bes
  a32 g fis g g8
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegretto"
  \time 4/4
  d8. es32 f \times 2/3 { es16[ d c]} \times 2/3 { d16[ c bes]}
  \grace bes8 a4 bes8 bes'~
  bes16 c, c es d16. es64 f es8
  \times 2/3 { d16[ c bes]} bes8 r16
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Allegro ma non Troppo"
  \time 3/4
  f2 \times 2/3 { d8[ es f]}
  \grace f4 es2 d4
  g4 g8 f f es
  \grace es4 d2 c4
}
