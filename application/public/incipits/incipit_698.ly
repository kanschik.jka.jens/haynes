 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "o. Bez."
  \time 4/4
  r2 r4 r8 a'
  a16 g fis e d8 a e'8. fis16 e fis g8
  fis8. g16 fis g fis g a8 b16 a g8 fis e4 r8 
}

 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "o. Bez."
  \time 4/4
r4 r8 a'
a16 g fis8 d a
e'8. e16 e fis g8 fis16 e d8 r
}
