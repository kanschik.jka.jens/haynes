
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Largo"
  \time 3/4
 es2.~ es~ es4 d8 c d4 es2 r4 bes2.~ bes~ bes4 a8 g a4 bes2 r4
 f'8 es d c bes as g2 r4
 as'8 g f es d c bes2.~ bes4
  
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Vivace"
  \time 2/4
 \partial 8
 bes8 es bes8 bes c16 d
 es8 es,4 c'8
 bes as16 g as8 g16 f
 g8 f16 g es f g as 
 g8 f16 g f8 as 
 g c bes es d es f es16 d es4 r8
   
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Mesto"
  \time 3/8
  << {
      % Voice "1"
            R4.
            r8 fis8 g
            R4. r8 b, c
         } \\ {
      % Voice "2"
            g'16. f32 es16. d32 c8 c4 r8
            d16. es64 f es8 d
            es16. d32 c8 r
      } >>
  
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Vivace"
  \time 4/4
     g'8 f g f  es bes es f
     es16 f g8 f16 g as8 g f16 es f8 bes
   
}

