\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
        r1 r8 a a16 g g fis  fis g a b \grace a8 g4
        fis r r16 fis e d e a e' fis32 g
} 


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  a8 fis16 g a8 b  b a a d
  cis4 b b8 a r
} 

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Largo"
  \time 3/4
    b2. b8 d d cis cis b
    b2. b8 d d cis cis d
} 

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Menuet"
  \time 3/4
    fis,8 g a4 g
    fis fis e
    d8 e fis4 g
    fis e r
} 
