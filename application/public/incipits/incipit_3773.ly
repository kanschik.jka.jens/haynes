\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Prima " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  r16 a16
  d8.[ d16 d8. d16] d8.[ a16 fis8. d16]
  fis'8.[ fis16 fis8. fis16] fis8.[ d16 a8. fis16]
  a'8.[ a16 a8. a16] a8.[ fis16 d8. a'16]
  b8.[ a16 g8. fis16]
  e8.[ fis16 e8. fis16]

}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Gratioso"
  \time 3/4
  e4 \grace d8 cis4 \grace b8 a4
  fis'2 e4 fis8 e d4. cis8
  cis2 r4
  a'4 cis a, gis b' gis, fis a' fis, e
  
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Giga. Allegro"
  \time 12/8
  \partial 4
  d8 e
  fis e d a'4 a,8 d cis d r8 e8 fis
  g a b a4 g8 fis e d a'4 cis,8
  b a b g'4 g,8 a g a fis'4 fis,8

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Secunda " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 3/4
  g8.[ g16 b8. b16 d8. d16]
  fis,8.[ fis16 a8. a16 d8. d16]
  g,8.[ g16 b8. b16 d8. d16]
  a4 d, r4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante"
  \time 4/4
  \partial 8
  d8 b g d' g fis d r8 g
  e c a d b g d' g
  fis d a' d b g b d
  a d, fis a g e cis a'
  fis d a fis

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Adagio"
  \time 4/4
  r8 d8 fis g a4 g
  fis8 e a2 gis4
  a8 a g a16 g fis8 fis' d fis16 d
  b8[ c]
  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga. Allegro"
  \time 12/8
  \partial 8
  d8 g4 d8 g4 d8 a'4 d,8 a'4 d,8
  d' e d a4 g8 a4. d,
 b'4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Terza " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  a4 r8 a8 d16 d d d d d d d 
  d d d d fis, fis fis fis g g g g a a a a
  d,8 d, r8

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio Estacato"
  \time 4/4
  b4. r16 b16 d8.[ cis16 d8. b16]
  fis'4. r16 fis16 ais8.[ gis16 ais8. fis16]
  b4. r16 b16 d8.[ cis16 d8. b16]
  ais8.[ g16 fis8. fis16]

}
  
  \relative c''' {
  \clef treble
  \key d\major
   \tempo "3. Presto"
  \time 2/4
  a4. r16 b16 
  a4. r16 b16
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \times 2/3 {a8 g fis} \times 2/3 {b8 a g}
  a4 d,8. d16
  e8. fis16 \times 2/3 {g8 a g}
  fis4.

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Quarta " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 3/2
  g2 b g
  d'2. e4 d c
  b4. a8 g2 g'4 d
  e c a fis d d'
  b2. a4 g2

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  g4 r8 d8 g fis16 e d8 d
  g fis16 e d c b a g8 g' g b
  a[ d,]

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Adagio"
  \time 4/4
  e4. fis16 g fis8 b, fis' b16 a
  g fis e8 r16 g16 a b a8 d, a' b16 a
  g fis e8 r16

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 3/4
  d2 g,4
  e'4 f8[ e d c]
  d4 e8 d c b
  c4 d8 c b a 
  b4. a8 g4

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Quinta " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Largo"
  \time 3/2
  r2 cis2 \grace b8 a2
  e'2. fis4 e d 
  cis b a2 r2
  r2 e' b 
  cis1 b2
  cis4 b a2. gis4 gis1 r2
  

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 4/4
  a8 cis e a gis e r8 a8
  fis d16 cis b8 e cis a r8 e'8
  cis b16 a b8 e cis b16 a b8 e
  cis b16 a b8 e cis a r8

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Andante"
  \time 3/4
  r4 a8 b c d
  e4 a, a'
  gis a2 b4 e, r4
  r4 e4 d 
  c c' b a b8 c b a 
  gis4. fis8 e4

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Presto"
  \time 2/4
  a8[ gis a e]
  cis'[ b cis a]
  e'4 d8 cis
  b4 e, 
  a'8[ gis gis fis]
  fis[ e e d ]
  d[ cis cis b]
  b [a a gis]

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Sexta " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  d8.[ d16 d8. d16] d16 a b cis d e fis g
  a8 a a a fis d a b16 cis
  d8[ a]

}

\relative c' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 3/8
  \partial 16
  d16
  d8. e32 fis g a b cis
  d8 cis b
  a8. g16 fis8
  g e a
  fis8. e16 d8
  a''8 fis d
  b'4 a8
  g8 a16 g fis8
  e4 a,8

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 2/4
  d4 a'
  fis8 e16 d b'8 b
  b a r8 d,8
  g16 fis g a a,8 g'
  g fis r8

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Settima " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  g8 g, g g' g g, g b16 c
  d8 d e16 d c b a g fis e d4
  d'8 a a d d[ d,] d 



}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 4/4
  g4 fis g r8 d8 
  e d16 e c8. b16 b4 r4
  d'4 cis d r8 a8
  b a16 b g8. fis16 fis4 r4


}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
  g4 a8 g fis4
  g fis8 e d4
  b' c8 b a4
  b a8 b g4
  d' c8 b a g 
  fis4. 

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Ottava " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 3/2
  r2 bes4 c d es
  f2 bes, f'
  g f2. es4
  d2. c4 bes2
  r2 d4 es f g
  c,2 c4 d es f
  bes,2
  

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 4/4
 f4 r8 f8 bes a bes a
 bes4 d,8 f g d es f
 bes,4 r8 c8 d c d c 
 d4 c8 f d a bes c
 f,4 r8
 
}


\relative c''' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 4/4
  \partial 8*7
 g4 fis8 g[ g,] r8 d'
 g a16 bes a8 g fis16 es d8 r8 g16 fis
 es8 es es f16 es 
 d8 d d es16 d
 c8[ c]

}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 3/4
  f4 bes8 a bes4
  f4 g8 f g a 
  f4 bes8 a bes4
  f g8 f g a 
  f es d c bes4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Nona " 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  r16 f16 f f f f f f f a a a a a a a
  a c c c c c c c c f f f f f f f 
  f8 f, f a'16 bes c8 f, f a



}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 3/4
  f8 a c a f a 
  e g c g e g
  f a c a f a 
  g f e d c4


}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Giga. Allegro"
  \time 12/8
  f4. r8 r8 c8 f g e f g e
  f c a f4 f'8 a bes g a bes g 
  a f c a4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Decima " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
 d4. d8 e4 e8 d16 e
 fis8 d a'2 gis8. a16
 a4 r4 



}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
 d8 fis16 g a8 a, d d, r8 d'16 e
 fis8 e fis g16 fis e8 a, a'4~
 a8 b16 a gis8. a16 a4 r4


}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Adagio"
  \time 4/4
 fis4 e8. fis16 d4. d8
 cis4 a' a4. gis8 a1

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Giga. Allegro"
  \time 12/8
  \partial 8
  a8 d cis d e d e fis4. e4 a8
  fis4 e8 fis g fis e4. a,4 a8
  d cis d e d e fis4.
 

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Undecima " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 4/4
 a4 r8 cis16 d e8 e e d16 e
 fis8 fis fis gis16 fis e8 e e fis16 e
 d8 d d e16 d cis4 r8



}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 3/4
 a8 gis a e fis gis
 a4 a, e'
 fis8 e d cis b e
 cis4. b8 a4


}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Giga. Allegro"
  \time 12/8
 a8 b cis d e d cis b a e'4 a,8
 fis' gis fis e fis d cis b cis a4 e'8
 a b a a b a gis fis gis e4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Duodecima " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Baletto. Allegro"
  \time 4/4
 c8.[ c16 e8. c16] g'4. r16 g16
 e8.[ g16 c,8. e16] d4 g,8. g'16
 e8.[ c16 c8. e16] d8.[ b16 g8. b16] c8.[ a16 fis8. a16] b4


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 3/4
 c4 e8 d c4 
 c e8 d c4
 d g, g'
 e8 d e d c4
 e g8 fis g4
 d g8 fis g4
 c, g'8 fis g4


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Adagio"
  \time 4/4
d4. e16 d c8 c c d16 c
b c d e a,8. g16 g2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Presto"
  \time 2/4
 c8[ g g c]
 d[ g, g g']
 e[ c c e] 
 d16 c b a g8 g
 c[ g g c]
 d[ g, g g']
 e[ c c e] d4
 

}


