\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 2/2
  \partial 4
  bes8. es,16
  \grace d'8 c4 bes bes8.[ es,16]
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
   g4 f8[ \grace { g32[ f es] } f16. g32] 
   \grace f8 es4~ es16 es f g
}
