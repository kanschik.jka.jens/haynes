 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "Romance"
  \time 2/2
  r4 g' a fis
  g b c a
  b g a fis g b, c d
  g, d' g2~ g4 fis a2
}
