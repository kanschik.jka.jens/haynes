\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Anthem I for Coronations" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Vivace"
  \time 3/4
  r4 r g8. b16
  b8. c16 b8 a g4
  d'8. e16 d8 c b16 c d b
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Anthem III for Ascension Day" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Sym."
  \time 2/2
  f4 f8. g16 a bes a g f4
  a4 a8. bes16 c d c bes a4
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Anthem V A Thankgiving" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Non troppo presto"
  \time 3/4
  g4 a b8 cis 
  d8. e16 d8 c b4
  e d8 c b a
  b8. a16 g4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Anthem VI ST. Matthew Ch.XI. Ver.28" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Sym."
  \time 3/4
    f4 e8 d c bes
    \grace bes4 a2 \grace {bes16[ c] } d4
    c16 f e d c4 bes
    a8 g a4
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Anthem VII St. John Ch.14" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Non troppo Presto"
  \time 3/4
  d4 g, a bes8. c16 d8 c bes a
  bes8. a16 g4 d'8 d
  es d c bes a g d'2.
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Anthem VIII Psalm CXXVIII" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegretto"
  \time 2/2
  d2 a4 g
  fis2 e4 d
  a'2 d8 cis d e
  fis2
}



