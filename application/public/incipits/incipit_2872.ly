 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro"
  \time 2/2
    e4 a, a' r8 g16 f
    e8 d c b a[ g16 a] e d c b
    a8 c e a gis16 e fis gis
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 2/2
  g'8 c, r a' g c, r a'
  g[ a16 g] f e f g e4 r8 
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "3. Allegro"
  \time 2/2
   a,8 a' a, a' a, a' e16[ d c b]
   a8 a' a, a' a, a' e16[ d c b]
   
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Menuett"
  \time 3/4
  \partial 8
  
}