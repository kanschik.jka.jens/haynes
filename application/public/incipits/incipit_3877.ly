\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro Moderato"
  \time 2/2
  \partial 4
  c4 f8.[ c16 a8] r a'8.[ f16 c8] r
  c'4 c2 bes16 a g f
  e8 d bes' g g f f e
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 2/4
  \partial 8
  c8
  g'4 c16 g e c
  \grace e8 d8 c c'16 g e c
  \grace g'8 f8 e  r e
  \grace e8 d8 d8. a'16 f d
  c8[ b]
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro [Tutti]"
  \time 3/8
  \partial 8
    c8 f f, f'~
    f e f g c, g'~
    g f g
    a16 c bes a g f e g f e d c
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro [Solo]"
  \time 3/8
  \partial 8
  r8
   \set Score.skipBars = ##t
   R4.*41
   r8 r c
   f f, f'~ f e16 g f a
   g8 c, g'~
   g f16 a g bes
   a c bes a g f e g f e d c
}