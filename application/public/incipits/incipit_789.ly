\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Legerement et Gracieusement"
  \time 2/4
  a4 e'
  cis4. d8
  e b d cis b a r 
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Legerement"
  \time 2/4
  \partial 4
  g'8 c, es d c b c es es es 
  d f f f es d g c, es d c b
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
  a'8
  e4 a
  cis,4. fis8
  e a d, cis b a r cis b e r a, gis cis r
}

  

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Un peu Gracieusement"
  \time 2/2
  \partial 2
  cis4 d
  cis8 e b d cis b a gis
  a4 e e' fis
  e8 a e d cis d b cis cis4 b
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Gravement"
  \time 3/4
  g'4. f8 es d
  c2.
  c'8 g f es d es16 c
  b2.
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Un peu rondememt"
  \time 3/8
\partial 8
e,8
a b gis a e'16 fis e d
cis d b cis a d
cis8 b e,
}
