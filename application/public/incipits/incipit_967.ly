 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  g'4 g8. g16 g4 r16 g, g8
  a g c b \grace b16 a8 g r16 b b8
  c b e d \grace d16 c8 b r
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 4/4
  d,16 e fis g  a32 fis16. e32 d16. d8 cis r a~
  a16 a'8 b32 c b16 g e cis d e32 fis g4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Rondeau"
  \time 3/4
  g'2 \grace e16 d8 c16 b
  a4 a a
  b8. c32 d c8 b a g
  fis8. g32 a g8 d b g
}

