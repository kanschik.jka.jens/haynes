\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro assai moderato [tutti]"
  \time 4/4
  c1 d e f g
  b2 d32 c b16 b[ b] f32 e d16 d[ d]
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro assai moderato [solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*32 
   << {
      % Voice "1"
        r2 r4 r8 c'16 a
        g8. f16 e8 d c4 r8 c'16 a
        g8. f16 e8 d c16 b c d e f g e
         } \\ {
      % Voice "2"
        c4 c c32 d e d c8 r4
        e8. d16 c8 b c32 d e d c8 r4
        e8. d16 c8 b c4 c16 d e d
      } >>
}

