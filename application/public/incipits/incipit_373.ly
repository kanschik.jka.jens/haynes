\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. All. [tutti]"
  \time 4/4
  d4 d,8. d16 d4 fis8. a16
  d8. d,16 fis8. a16 d8. d16 fis8. a16
  d4 d,8.d16 d4 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. All. [solo]"
  \time 4/4
  d4 d8 d \grace { e16[ d cis] } d4 a'8 fis
  d4 d8 d \grace { e16[ d cis] } d4 b'8 g
  d4 d8 d \grace { e16[ d cis] } d4 a'8 fis
  d4 d8 d \grace { e16[ d cis] } d2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio [tutti]"
  \time 4/4
  d8 b16 a g8 g g16 g' fis e d c b a
  g g' fis g  r g fis g  \times 2/3 { a,16[ b c]} \times 2/3 { c16[ d e]} \times 2/3 { e16[ d c]} \times 2/3 { c16[ b a]} 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio [solo]"
  \time 4/4
  d8 b16 a g8 g  \times 2/3 { c16[ d e]} d8 g4~
  g8 fis16 g \times 2/3 { a16[ g fis]} \times 2/3 { e16[ d c]} b4 g'
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. All. [tutti]"
  \time 3/8
  d,8 fis a d a fis
  d fis a d, fis a
  d,4 r8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. All. [solo]"
  \time 3/8
  d8. cis16 d e d8 d d
  d8. cis16 d e d8 d d
  a'8. gis16 a b a8 a a
}
