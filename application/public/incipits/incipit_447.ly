
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I."
    }
    \vspace #1.5
}
 
\relative c''' {
\clef "treble"   
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
a4 r8 fis16 g a8 d, d b'
b a r8 fis16 g a8 d, d d'
g, d16 cis d8 d' e,8 d16 cis d8 e16 fis
g4.


}



\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 3/8
d16 e fis e d8
a'8 e a
fis16 e d e fis8
e8 a, a'
a g fis e fis g a4.~ a8 g16 fis e8
fis e4 d r8


}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Affettuoso"
  \time 3/4
f4 g8 f e f
d2 bes'4 a2 g4
f e bes' a2 d4
g, f e
a f4. e16 f e2.
 
}
 
\relative c''' {
  \clef treble
  \key d\major
   \tempo "4. Presto"
  \time 2/4
a2 d,4~ d16 fis e d 
e4~ e16 g fis e
fis4~ fis16 a g fis 
g a g fis e fis g a 
fis8 d r8

 
}
 

 

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Andante"
  \time 4/4
\partial 4
e8 b'
b e, r4 r4 e8 c'
c e, r4 r4 e8 c'
c b b a a16 b g a fis g e fis 
dis8 \grace cis8 b8 r8 


 
}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 4/4
\partial 8
b8 b b, e16 b fis' b, g'8 e r8 b'8
b b, e16 b fis' b, g'4 r8 b8
b a16 b c8 b a g16 a b8 a 


}


\relative c''' {
  \clef treble
  \key e\minor
   \tempo "3. Adagio"
  \time 3/4
 \set Score.skipBars = ##t
   R2.*1 b8 a b a g fis
g a a4. g8 
fis2 b,4
e8 fis g a a g16 a
b2.~b4 a4. a8 a4 
 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 3/8
e4 fis8 g a b
e, fis4
dis8. cis16 b8



 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III."
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Vivace"
  \time 3/4
\partial 2
b8 c d4
g,2 r4
r4 b8 c d4
g, g'8 a b4
e, fis g 
fis4. e8 d4 e d c b4. c8 d4 c b2


}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 3/8
d4 e8 d16 c b c d8
d4 e8
d16 c b c d8
d4 e8
d e16 fis g8
c,8 b4


}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Allemanda. Moderato"
  \time 4/4
\partial 8
r16 g16 g8. d16 d b b d e c c a a d d c 
b8 g g' g g4 fis
g8 b, b e e a, a d


}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Presto "
  \time 2/4
\partial 8
d8 b c16 d e4
d8 d g b,
c b a4
g8 b16 c d4~ d cis d4. g,8 
c e a,


}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV."
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key d\major
   \tempo "1. Sicigliana"
  \time 6/8
\partial 4.
d8. e16 d8
a'8. b16 a8 b8. cis16 d8
cis8. b16 a8 a'8. g16 a8
fis8. e16 fis8 g16 b, cis8. b32 cis
d4


}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Gavotta"
  \time 2/2
\partial 2
d8 e fis g
a4 e e fis8 g
fis4 d a' a 
a d, g g 
g2 fis4 fis
fis b, e e e2



}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Andante"
  \time 3/4
\partial 2
b8 cis d4
r4 cis8 d e4
r4 d8 e fis4~ fis8 g e4. d16 e fis4
 

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Minoetto"
  \time 3/4
d4 d d 
a b2
fis8 g a g fis e 
d4 d' b e d8 cis b a 
b4 gis a b b 4. a8 a2. 

}



