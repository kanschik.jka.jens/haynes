 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
    f4 c' a f e g bes r
    e,8 f g f e d c bes bes a a a a4 r
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Menuetto"
  \time 3/4
  \partial 4
  f4 f e f e e c
  c8 d e f g c
  a4 f r8
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro piu giusto"
  \time 2/4
  \partial 8
  e8
  \times 2/3 { e8[ f e]}   \times 2/3 { e8[ f e]}
    \times 2/3 { g8[ g g]} g8 g 
      \times 2/3 { g8[ f e]}
        \times 2/3 { g8[ f e]}
          \times 2/3 { d8[ d d]} d8
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Finale. [Allegro]"
  \time 2/4
  \partial 8
    c8
    c f f f f e e c
    a' a c16 bes a g a8 f f c
}