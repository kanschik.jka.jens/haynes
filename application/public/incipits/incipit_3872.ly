\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro Moderato [Tutti]"
  \time 4/4
    <g, e' c'>2 c'4 c
    c8 c,16 d e8 f e d e c
    <g d' b' g'>2 g''4 g
    g8 g,,16 a b8 c b a b g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro Moderato [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*38
   r2 r4 r8 r16 g'
   c2 c4 c c2 c16 b a g f e d c
   c'2 c4 c c2 c16 b a g f e d c c'4 c,
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Lento"
  \time 3/4
  \partial 16
  c16
  f2 \times 2/3 {a8[ g f]} f4 e r8 r16 c
  g2 \times 2/3 {bes8[ a g]} \grace a8 g4 f4
  
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegretto"
  \time 3/8
  \partial 16
  c16
  g'4
  f32 e d c
  c8 c c
  g'4 f32 e d c
  c8 c c
  c'4. a g8 f e
  \grace e8 d8 c16 b a g
}
