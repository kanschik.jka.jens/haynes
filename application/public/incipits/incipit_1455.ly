 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 3/4
  es4 es8. es16 es8. es16
  es8. g16
  es8 r r4
  as4 as8. as16 g8. f16
  f d d es es8 r r4
}
