 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
    r8. e16
    e1
    c4 r r2
    r2 r4 e8. f16
    g4 g g e c c d e
    g4. f8 e4 r
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Adagio"
  \time 3/4
  c2 d8 c
  c2 d8. c16
  c4 bes8 a g f
  a4 g8 a bes b
  c4 cis8 r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Menuetto. Allegro"
  \time 3/4
  \partial 4
  g,4 c e g c2 b4
  b a g
  g f e
}
\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Finale. Presto"
  \time 2/4
  \partial 8
  g'8
  a g a f
  g4 r8 e f e f d
  e4 r8 g,
  a b c d e f g e
  f4 e d r8
}
