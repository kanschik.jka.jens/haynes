 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 3/2
    c4. d8 e4. f8 g4. a8
    b,1 c2
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 4/4
  c8 g'4 e8 d g4 d8
  c g'4 c,8 b8. a16 g8 c
  d16 c d e  f e f g
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "3. Largo e cantabile"
  \time 4/4
  a8 c16 b a8 b16 c  b gis e gis  b c d e
  c b a8 r4 r2
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Allegro"
  \time 3/8
    g'8 a16 g a f g8 r g
    g a16 g f e e4 d8
}