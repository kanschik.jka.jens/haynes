 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  r2 f,8 a16 bes c8 d
  c16 bes c d c8 bes16 a bes a bes c bes8 a16 g
  a8 f a c
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 4/4
  f,16 g a bes c8 d4 e8 f c
  f,16 g a bes c8 d4 e8 f c
  d16 c d es
  c bes c d bes a bes c  a g a bes
  g8 c,
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Largo"
  \time 3/4
  a'4 \grace g8 f4 \grace e8 d4
  bes'2 a4
  g8 bes a g f e f4. e8
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allegro"
  \time 2/4
  f,8 a16 bes c d e c
  f8 e f c
  d es16 d c8 bes a bes c
}