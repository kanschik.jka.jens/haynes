\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Andantino"
  \time 2/2
  \times 2/3 { a8[ c e] } e2 a8. a16
  \times 2/3 { gis8[ f e] } e4 r2
  \times 2/3  { e8[ gis b] } b2  d8. d16
  \times 2/3  { c8[ b a] } a4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro molto"
  \time 3/4
  e2 \grace e8 d8 c16 d
  \grace d8 c4 r8 a' a a
  \grace a8 gis4. f8 e d
  \grace d8 c4 r8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Andantino Gratioso"
  \time 2/4
  g8 e16 f g8 g
  g8.[ \times 2/3 { a32 b c] } g8 r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Affetuoso"
  \time 3/8
  c'16. d32 c8 r
  bes16. c32 bes8 r
  r16 f f f f8~
  f16 bes32 c64 d c16[ bes a g]
  f4.
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. All. ma moderato"
  \time 4/4
  f8. g16 f8. g16 f g a bes c8 r
  d16 bes f4 d'16 bes c a f4.
  g8 a4 bes8 \grace b8 c4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante assai moderato"
  \time 2/4
  \partial 8
  bes16. f'32
  f8. es16 d8 d16. d32
  d8. c16 bes8 r16
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. All. non troppo"
  \time 4/4
  bes'4 f8. f16 \times 2/3 { d16[ c bes } bes8] r4
  r2 r4 r8 bes8
  c d \grace f8 es8 d16 c d4
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 3/4
  g'4 r r
   \set Score.skipBars = ##t
   R2.*2 
   r8 g b d \grace d8 c b16 a
   g2.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Non Tanto Allegro"
  \time 2/4
    g2
    a4. b16 c
    b4 \grace d8 c8 b16 c d8 g16 d \grace d8 c4
    b
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Un poco Adagio"
  \time 4/4
  g'4 a16 g f e d8 c4 b8
  \grace b8 c4~ c32[ d e f] g16 g \grace gis8 a4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. All. moderato"
  \time 4/4
    c1 d2 d4. e16 f
    e32 d c8. c4 c8 e g c a32 b c8. c8 c c a4 f8 \grace fis8 g4. e8 c4 r
}



