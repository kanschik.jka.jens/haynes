 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 3/4
    bes4 f'16 bes,8. g'4
    \grace bes,4 a2 bes4
    c8.[ d32 es]
}
