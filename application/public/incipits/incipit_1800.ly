\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
      f4 f16 e f g f4 f16 e f g
      f f f f  c c c c a a a a  c c c c
      f,
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 4/4
  \partial 4
  d8. es16
  f4. g8  f d c bes
  c8. bes16 c8 d  g,4. bes8
  f8. g16 f8 d f4
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo ""
  \time 6/8
    \partial 8
    c8
    f4 a8 g f e
    f e d c4.
    d8 e f bes,4 a8 g f' e f4 r8
}

