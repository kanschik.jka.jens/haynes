\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Moderato [Tutti]"
  \time 4/4
  \partial 8
  e16 cis
  a8 a' a a a16 fis d4 cis8
  fis16 d b4 a8 d16 b gis4 fis8
  e
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Moderato [Solo]"
  \time 4/4
  \partial 8
  r8
   \set Score.skipBars = ##t
   R1*17
   r2 r4 r8 \times 2/3 { e,16[ fis gis]}
   a2~ a8 d4 cis8
   fis16 d b4 a8
   d16 b gis4 fis8
   e
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  \partial 8
  a32 gis a b
  a16 fis'8 d a fis16 g32 fis g a g8 r16 g32 fis g16 g
  g g'8 e cis g16 fis32 g a b a8 r16
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio [Solo]"
  \time 4/4
  \partial 8
  r8
  \set Score.skipBars = ##t
  R1*9
  r2 r4 r8 a16. b32
  a8 fis' e32 d16. cis32 b16. \grace b8 a16 gis gis4
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Presto"
  \time 2/2
  e2 cis4 e
  e2 d4 cis8 b
  cis4 e a e
  e2 d4
}