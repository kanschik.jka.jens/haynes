\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Tutti]"
  \time 2/2
  f4 c d16 e f4 e16 d c8 a' bes,4 a16 g f8 f'4~
  f8 e16 f g8 bes, bes a r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   R1*15 
   f8. e32 d c8 c d16 e f4 e16 d
   c8 a' bes,4 a16 f a c f4~
   f16 a g f e[ d c bes] bes8 a r
   }

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Largo"
  \time 3/4
  a8. f'16 e4 d
  d cis2
  bes16 a8. d16 cis8. g'16 e8.
  f16 e8. \grace e8 d2
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Presto"
  \time 3/4
  f2. g
  a4 e f d c f
  bes, a r
  d c bes a g r
}

