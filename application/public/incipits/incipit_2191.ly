\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro "
  \time 4/4
  bes,2 f'
  d r8 bes c d
  es4 g c, es
  a, <a f'>8. <a f'>16 <a f'>4 r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio"
  \time 3/4
  es4 bes' d,
  \grace f16 es8 d16 c bes8 es f g
  as8. f16 d8 f g as
  bes8. g16 es8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 2/4
  d8. es16 f f f f
  bes8. c16 d8 r
  g,16 bes f f  es f d d
  c c d bes a f' e es
}