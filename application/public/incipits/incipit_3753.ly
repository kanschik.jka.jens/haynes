\version "2.16.1"

#(set-global-staff-size 14)

\relative c' {
  \clef treble
  \key bes\major
   \tempo "1. Furioso "
  \time 2/2
  bes8 d f d bes d f d
  bes8 d f d bes d f d
  d f bes f d f bes f
}
