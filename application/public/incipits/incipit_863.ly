\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Un poco Allegro"
  \time 2/4
  \partial 8
  bes8 f' f f f
  \grace g8 f4 es16 d c bes 
  d32 c bes16 f' f f
  f16 as g f es d c bes
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Air. Andante"
  \time 3/8
  bes'4 \grace a8 g8
   \grace f8 es4  \grace d8 c8
    \grace bes8 a4.
    d4  \grace c8 bes8
     \grace a8 g4
}


\relative c'' {
  \clef treble
  \key bes\major
     \tempo "3. Bouree"
  \time 2/2
  \partial 4
  bes4
  f' f f d8 bes
  bes'4 bes bes bes,
  f8 g a bes c d es f
  g4 g2 f8 es
}

\relative c'' {
  \clef treble
  \key bes\major
     \tempo "4. Passepied"
  \time 3/8
  \partial 8
  bes16 d
  f8 f f
  f bes d,16 es
  f8 bes d,16 es
  f8 bes bes,16 d
}

