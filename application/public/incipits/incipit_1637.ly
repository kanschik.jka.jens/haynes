 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
    f4. f8 f f f f 
    f r f r f r f r
    f4. f8 f f f f 
    f r f r f r f r
    e4. e8 e e e e
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Romanza"
  \time 4/4
  \partial 2
  d8 es e f
  bes,8 r bes b c r c cis d4 r
  d8 es e f g r f r es r d r
  c4 r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Menuetto. Allegretto"
  \time 3/4
  \partial 4
  f8 a c4 c c
  c c c a f c'
  a f 
}
\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Rondo"
  \time 2/4
  \partial 8
    a'16 bes
    c8 c16 bes  a8 a16 g
    f4. g16 a 
    bes8 bes16 a g8 g16 f
    e4.
}
