\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 2/4
    d4 d d16 e fis g a8 d,
    e fis g8. fis32 g
    fis16 e fis g a8 d,
    e fis g8. fis32 g
}
