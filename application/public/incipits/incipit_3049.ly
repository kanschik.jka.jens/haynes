\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro"
  \time 4/4
  g8 c es d  g, c16 d es d es d
  c8 d16 es f g as8 as d, as' d,
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio"
  \time 12/8
  bes4.~ bes16 c bes as bes c d,4.~ d16 c d es f g
  as c bes as f'8~ f16 es d c bes as g8
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro"
  \time 2/4
  \partial 8
     c8 g c f, c'
     es,16 d es d c8 es' 
     d g4 c,8 b16 a b a g8
}