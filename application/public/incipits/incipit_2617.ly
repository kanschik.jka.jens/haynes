\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 12/8
    d4.~ d8. e16 d8 d8. e32 fis g8 d8. e16 c8
    d4.
   \tempo "Allegro"
  \time 4/4
    r8 a a a b cis16 d e8 d
    cis a d2 c4~
    c4
}
      
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Vivace"
  \time 2/4
  d8 d d e16 fis
  g8 g g a16 b
  c8 c c b16 a
  b a g8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Grave"
  \time 3/2
  <a c fis>2 r r
  <e g b>2 r r
  <e' g ais>2 r r
  b' r4 e, d g
  fis
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Vivace"
  \time 6/8
  \partial 4.
    g'8 a g
    fis g fis e fis e
    d4 d,8 e' f e
    d e d c d c
    b4 g8
}
