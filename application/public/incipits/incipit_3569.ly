\version "2.16.1"
         #(set-global-staff-size 14)
         
         \relative c'' {
  \clef treble
  \key  f \major
   \tempo " 1. Allegro [Tutti]"
  \time 4/4
  \partial4 r8. f,16 f8.a16 a8. c16 c8. f16 f8. a16 a4 (c8) a-. f4 (a8) f-. c8-. f-. c-. a-.
         }
  \relative c'' {
  \clef treble
  \key  f \major
  \tempo " 1. Allegro [Solo]"
     \time 4/4        
\partial 8 c16 f f4 e32 (d16.) c32 (bes16.) a16 bes c8 r bes16 (d) d4 c32 (bes16.) a32 (g16.) f16 g a8
  }
  
   \relative c'' {
  \clef treble
  \key  bes \major
   \tempo " 2. Andante [Tutti]"
  \time 3/4
  
  bes,8 bes4 c16 d es8 d \grace {d16} c8 bes16 c bes8 r r4 bes'8 bes4 c16 d es8 d\slashedGrace {f} es
   }
     \relative c'' {
  \clef treble
  \key  bes \major
   \tempo " 2. Andante [Solo]"
    \time 3/4
    bes8 bes4 c16 d \grace {f8} es8 d \slashedGrace {d} c8 bes16 c bes4 r d8 d4 es16 f g8 f \slashedGrace {f} es
     }
   \relative c'' {
  \clef treble
  \key  f \major
   \tempo " 3, Menué [Tutti]"
  \time 3/4    
  
  c'4 (a) f-. a (f) c-. d16 (bes8.) a16 (g8.) f16 e8. \grace {e4} f2  \tuplet 3/2{a'8 (g f)}   
   }
   
     \relative c'' {
  \clef treble
  \key  f \major
  \tempo " 3, Menué [Solo]"
 \time 3/4
    c8-. f-. f (e) e (d) d (c) c (bes) bes (a)  \tuplet 3/2{g( a bes)} f4 e
    
     }
 
 