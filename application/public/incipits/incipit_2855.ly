 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
  d4 r8 c d4 f
  d r8 c d4 f
  d8 es f4 d8 es f g
  c,4 r4 r2
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio [Tutti]"
  \time 2/2
  r8 d d d bes bes bes bes
  bes bes bes bes  a a d d
  d d c c d2
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio [SOlo]"
  \time 2/2
   \set Score.skipBars = ##t
   R1*2 
   r2 r4 r8 d16 c
   bes a g8 r d'16 c bes a g bes~ bes[ d d g]
   fis4 r8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 12/8
  \partial 8
  f,8
  bes c d  c4 f,8 c' d es d4 f,8
   bes c d c4 f,8 c' d es d4 f,8
   es' f es   es f es  d es d c[ f,]
}
