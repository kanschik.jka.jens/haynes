 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  g2 a8 b c d
  c2 b4 c
  cis8 d e d c b a g
  f2 e4
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Rondeau. Allegretto"
  \time 2/2
  \partial 8

}
