\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Largo"
  \time 3/4
  d4. a'8 f d
  e e4 e8 e16 g f e
  f4 e a
  a gis2 a8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro"
  \time 4/4
  d2 cis8 b16 a d8 e
  f16 e f8 a f e a4.
  a2. r16 f e d
  d8 cis a e' e d r4
}
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\minor
                      \tempo "3. Allegro"
                      \time 3/8
                      R4. R4.
                      d32 f a16 a8[ g]
                      f16 d e f g a
                      bes8 a r
                  }
\new Staff { \clef "treble" 
                     \key d\minor
                      d,32f a16 a8[ g]
                      f e4 d8 cis4 d8 r r 
                  }
>>
}

