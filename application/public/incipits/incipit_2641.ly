\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  r8 f bes4 r8 f bes4
  r8 f bes4 g8 as16 g f8 es
  d
}
      
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Largo"
  \time 3/2
  c2 d es
  f g b,
  c
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 12/8
              bes 4. bes bes bes
      bes bes bes r8 r bes
      c d es c d es d es f d es f
}