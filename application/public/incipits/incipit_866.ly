\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro ma non presto"
  \time 2/4
  \partial 8
  g8
  b c d e
  a, d4 fis32 e d16 g,8 d'4 fis32 e d16
  fis,8 d'4
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante [Tutti]"
  \time 3/4
  g4 fis g
  a g fis
  g fis fis8. g32 a
  g4 fis r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante [Solo]"
  \time 3/4
  b2. b b b 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
      b4 c a b c d
      e d r
      fis8. g32 a g4 r
}

