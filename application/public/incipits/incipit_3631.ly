\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  d8
  g4 g, r8 \grace a'8 g16 fis g8 a
  b4 g, r8 \grace c'8 b16 a
}
