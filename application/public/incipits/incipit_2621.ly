\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Affetuoso"
  \time 4/4
      << {
      % Voice "1"
      r2 r8 d'16 a bes a bes fis
      g d d' a bes a bes fis g fis
         } \\ {
      % Voice "2"
      r8 g d es16 d d2~
      d2 d16 a bes a
      } >>
}
      
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Vivace"
  \time 3/4
  r8 d g, d' c bes16 a
  bes4 r8 bes a g16 fis
  g8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Adagio"
  \time 4/4
  bes'8 f d c16 bes c4~ c16 es d c
  bes4
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro"
  \time 4/4
      << {
      % Voice "1"
      r8 d d g, r d' d g,
      r d' g d
         } \\ {
      % Voice "2"
        r4 r8 d d g, r d'
      d g, r g
      } >>
}
