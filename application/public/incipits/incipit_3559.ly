\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  \partial 8
  d8
  \grace c8 b8. a32 g e'8 fis g g, e' fis
  g g, e' fis g g, r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
  
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Andante [Tutti]"
  \time 4/4
  e,8 \times 2/3 { e'16[ fis g] } b,8 a g \times 2/3 { e'16[ fis g] } g,8 fis
  e e' c'4 b8 e, a4
  g8 b, c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Solo]"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 2/4
  g2~ g8 b a fis
  b2~ b8 d c a
  b4 d g2
}