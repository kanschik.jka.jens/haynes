 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Andante"
  \time 3/2
        << {
      % Voice "1"
        R1. fis1.~ <fis a>1.
        <fis a>2 <e gis>4.
         } \\ {
      % Voice "2"
        <gis, b e>4 <gis b e>4 <gis b e>4 <gis b e>4 <gis b e>4 <gis b e>4
        <fis a cis>4 <fis a cis>4 <fis a cis>4 <fis a cis>4 <fis a cis>4 <fis a cis>4
        <fis a b>4 <fis a b>4 <fis a b>4 <fis a b>4 <fis a b>4 <fis a b>4
        <e a b>4 <e a b>4 <e gis b>4
      } >>

}


\relative c'' {
  \clef treble
  \key e\major	
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  b8
  cis16 dis e8 e16 dis cis8
  cis b4 a8 gis16 a b gis
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Siciliano"
  \time 6/8
  \partial 8
  b8
  e8. g16 fis8 e8. b16 dis8
  e4. e8. fis16 g8
  fis8.
}

\relative c'' {
  \clef treble
  \key e\major	
   \tempo "4. Vivace"
  \time 3/8
    b4. cis cis8 b4
    e8 a,4
}