\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. o.Bez."
  \time 3/4
  \partial 8
    b8
    e4. fis8 g fis16 e
    fis4. g8 a g16 fis 
    g8 a16 b b4 a16 g fis e
    e4 dis8
}
