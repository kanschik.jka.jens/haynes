\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Ach, lege das Sodom der sündlichen Welt [BWV 48/4]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key es\major
   \tempo "o. Bez."
   \time 3/8
   \partial 8
   bes8 g8. c16 bes8
   d, es16 g bes c
   des8 c16 bes c32 des es16
   c8 bes16 as bes8
   c8. f16 d8 bes8 as16[ g]
  
    
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key es\major
   \tempo "o. Bez."
   \time 3/8
   \partial 8
   \set Score.skipBars = ##t
     r8 R4.*15 r8 r8 bes8
     g8. c16 bes8
     d,8 es16 g c d
     des8 c16 bes c32 des es16
     c8 bes16 as bes8
     c8. f16 d8 bes8 as16[ g]
     
}



