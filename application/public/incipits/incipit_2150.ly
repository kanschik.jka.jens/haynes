
\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. ohne Satzbezeichnung"
  \time 4/4
  e4. c'16 b b8 dis, e c'16 b b8 a16 g fis8 e dis b r8 e16 b
 c b c8 c d16 a b a b8~ b c16 b a8
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 2/2
   e8 b'16 g e8 [dis] e16 g b g e8 [fis] 
 g16 [fis g a] fis [b a b] g  [fis g a] fis b a b
 g e c' b a [g fis e] dis e fis dis b4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. ohne Satzbezeichnung"
  \time 3/2
    r2 b2 g'2~ g fis e dis b e 
 fis4. g8 g2. fis8 g a2*3~ a2 b4 a g fis g2 b, e d
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. ohne Satzbezeichnung"
  \time 3/8
   e8 b e fis16 g a8 fis g e16 fis g a b8 e, fis dis b r8
}