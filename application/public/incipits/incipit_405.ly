\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}




\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4
   c16. es32 d16. c32 g'8 g, c16 b c8 r g'16 c,
   d8 es16 f es8. d16 es8 c r
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro"
  \time 2/2
     c16[ d es f]  g c, d es d[ c d es] d es c d
     es d es f
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Adagio"
  \time 3/2
    c4. d8 es4. f8 g4. c,8
    b4. c8 a4. b8 c4. es8
    d4.
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 12/8
  c4. d e8 d c  f e d
  g f e  a g f  g f e f e d
  e4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4
  g'8. f16 es8 d16 c b4. c8
  d es16 f g f g d es4 r8
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Vivace"
  \time 3/2
      g2 g g g2. f4 g2
      c c4 es  d c
      b2. a4 g2
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Adagio"
  \time 4/4
    b2 r4 d4
    f8 es f g  es d es16 f
    d8 g, b8. b16 c4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Allegro assai"
  \time 4/4
    es4 es8. d16 d4 r8 d
    g4 g8. f16 f4. g8 es d c d
}
