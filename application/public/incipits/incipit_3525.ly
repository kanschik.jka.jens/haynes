\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 3/4
  \partial 4
  bes8. c16
  d4 g, es'8. es16
  es4 d d8. d16
  g8. c,16 c4. c8
  c4 bes
}
