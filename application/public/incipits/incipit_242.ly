\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\minor
                      \tempo "1. o.Bez."
                      \time 2/2
              r8 d4 cis8 d4 r
              r8 a f' e  d cis r a'~
              a g f f e4r
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key d\minor
                        % Voice 2
                      r2 r8 d4 cis8
                      d4 r8 a f' e d cis
                      r4 r8 d4 c8 bes bes	
                  }
>>
}
