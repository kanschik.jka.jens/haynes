\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4 	
      c,8. d16 e8. f16  g8 a16 b c4
      c16 b d c  e d f e
}
