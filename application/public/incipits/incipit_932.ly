 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. [Allegro]"
  \time 4/4
   c8. d16 e8 d16 c d8. e16 f e f g
   e8 c g'4. a16 g f4~
   f8 g16 f e4
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Grave"
  \time 4/4
  es4. f8 g4 d8 es16 f
  es8. d16 c4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. "
  \time 6/8
  r8 g g c d e
  d e f e8. d16 c8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}