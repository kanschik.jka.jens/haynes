\version "2.16.1"
         #(set-global-staff-size 14)

         
         \relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
    r4 f2 e8 d
    c4 d e f~
    f e f2
    g4 c, d e f2 e4 g~ g f8 e d4 e
}
         