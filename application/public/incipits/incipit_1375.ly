 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Vivace"
  \time 2/4
    e4 b g e
    g16 fis e fis g a g a b4 r
   
}

 

 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 4/4
    g8 g g g  a a a a
    b b b b c g' a g
    r g a g r g a g
}

 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Allegro"
  \time 3/4
    e4 e, r8 b'
    e d c b a g 
    fis4 d r8 a'
    d c b a g f
    e4 c r8
}
