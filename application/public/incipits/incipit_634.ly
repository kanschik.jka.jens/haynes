 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
   \time 4/4
      bes4 bes'8. a32 g f4 g8. f32 es
      \grace es16 d8 c16 bes  \grace d16 c8 bes16 a
      bes8 f r4
}


\relative c'' {
  \clef treble
  \key g\minor
       \tempo "2. Adagio"
   \time 4/4
  \partial 8
  d8
  g8. bes32 a g8 d \grace d8 es8. g32 f es8 b
  \grace bes8 c8. es32 d
  c8 g a g r

}
\relative c'' {
  \clef treble
  \key bes\major

   \tempo "3. Spirituoso"
   \time 3/4
      <bes, f' bes>4 r8 es8 d c
      <bes f' bes>4 r8 es d c
      <bes f' bes>4 d' es
      f16 f f f  es es es es  d d d d
}
