\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.bez."
  \time 3/4
    r8 d d4. c16 bes
    bes2 r8 d
    f2 es8 d16 es
    d2
}
