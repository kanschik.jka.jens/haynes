\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro spirituoso"
  \time 4/4
  c2 e4. c8
  g'4 g2 e8. c16
  f4. e8 d c b a
  g4
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante Largo [Tutti]"
  \time 2/4
  \partial 4
  r16 c, f a
  c8 b c b
  c8. cis16 d f e d
  c8 c4 bes8
  bes4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante Largo[Solo]"
  \time 2/4
  \partial 4
  r4
   \set Score.skipBars = ##t
   R2*7
   d4 c32[ c d e] f g a bes
   c2~
   c4~ c16[ bes32 a] g f e d
   c16 c f a c8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo"
  \time 6/8
  e4 e8 \grace f16 e8 dis e
  g4 fis8 f4 d8
  c4 c8 \grace d16 c8 b c
  d g, a  b c d
}