
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Larghetto"
  \time 4/4
  a8 bes c d16 c c4. d8 e f g a e16 d c8 r16 f16 g16. a32
 d,8 e16. f32 bes,8. a16 a8 d16 c c4

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 3/8
 c8 f e f c r8 R4. c8 a bes c a c 
 f16 c f c g' c,
 a' f bes a g f  
 e c e a g a 
 d, d, d' g f g 
 e [c] f4 

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Adagio"
  \time 3/4
  r4 f4. e16 d32 cis 
 d4 a r4
 bes'4 a8. g32 f e8 f16 g
 f2 e4 
 r4 e8. f32 g f8. g16 e4. a8 d,4 


}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 2/2
  c2 d4. e16 d 
 c4 f bes,2 a4 a'~ a8 g g f 
 e c f a, bes d c bes 
 a f a bes c f, f'4~ f8 e f g a g a bes
 g g, c bes a [f]


}

