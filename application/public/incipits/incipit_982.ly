\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro ma non troppo"
  \time 4/4
  \partial 4
  f,4
  bes d f bes,
  \times 2/3 { a8[ bes c]} bes2 a8 g
  f4 bes es d
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 3/4
  g8. g'16 g4 d
  \grace c8 bes4 \grace a8 g4 r 
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro assai"
  \time 2/4
  bes4 bes bes4. d8 bes f' bes, d
  bes bes' bes, f'
}
