\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key bes\major
    \time 4/4
    \tempo "1. Allegro"
       bes2 bes4. bes8
       bes4 bes8. bes16 bes4 bes
       bes2 es
       d a
       bes8 f d'4. d8 es c16 a bes4 r r2
}

\relative c'' {
  \clef treble  
  \key es\major
    \time 3/4
    \tempo "2. Andante"
      bes8. c16 bes8 bes bes16 as g as
      as4 g16 g as g \grace c16 bes8 as16 g
      g8. as16 es4 d
      es8 r r4 r4
}

\relative c'' {
  \clef treble  
  \key bes\major
    \time 2/2
    \tempo "3. Rondo. Allegro"
       \grace {d16 es} f4 es8 d  \grace d8 c4 bes8 a
       bes d c a  bes4 a
}