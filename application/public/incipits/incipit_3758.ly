\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\minor
                      \tempo "Andante"
                      \time 3/4
                      % Voice 1
                      d4 a8. f'16 e8. d16 
                      cis8 d16 e a,4 r 
                      r r8 d g8. f16
                      e8 f16 g c,4 r
                  } 
\new Staff { \clef "treble" 
                       % Voice 2
                       r2.
                       r4 r8 e a8. g16
                       f8 g16 a
                       d,4 r
                  r r8 f f8. e16 d8 c16 d bes4
                  }
>>
}
