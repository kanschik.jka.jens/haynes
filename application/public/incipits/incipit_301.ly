 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Larghetto"
  \time 3/4
  \partial 4
    es,8. f16
    g8. as16 bes4 bes8. c16
    \grace c4 bes2 c8. d32 es
    c16 bes8. as16 g8. f16 d8. f2
}


