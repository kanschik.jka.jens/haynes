\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  bes 4 f' \grace es8 d4. c8
  bes16 c d es f8 g a, bes r d
  c16 es d f \grace f8 es4 \grace es8 d4. r8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*14
      bes 4 f' \grace es8 d4. c8
  bes16 c d es f8 g a, bes r d
  c16 bes a g f8 es\grace es8 d4 r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [Tutti]"
  \time 6/8
  g'8. a16 g8 g d,16 c bes a
  g8. a16 g8 g4 r8
  as''8. bes16 as8 as8. f16 d8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [Solo]"
  \time 6/8
   \set Score.skipBars = ##t
   R2.*10
   g8. a16 g8 fis16 g a bes c d
   es8. d16 c8 d,16 fis a es' d c
   \grace c8 bes8. a16 g
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Presto"
  \time 3/8
  \partial 8
  f,8
  bes4 bes8
  bes4 bes8
  \grace c8 bes8. a16 bes8
  f4 es8 d8. es16 f8
  bes4 f8
}