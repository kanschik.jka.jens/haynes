\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "[voce]"
  \time 3/4
  r4 d8 bes g es'
  d4 g, r
  bes8 a bes c d es
  d2 bes4
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "[Oboe]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*7 
   r2 r4 r8 g'
   es d c b c d es es
   d b c b r2
}
