 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Largo"
  \time 3/4
  r4 e2~ e8 fis dis2
  e r4 R2.
  r4 g2~ g8 e c2
  b r4
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. Allegro"
  \time 2/4
    e16 fis g8 g fis
    e dis e b
    g16 a b8 b a
}

\relative c'' {
  \clef treble
  \key e\major	
   \tempo "3. Dolce"
  \time 3/8
  gis'8 a16 gis a b
  gis4 r8
  cis, fis e
  dis16 cis b8 r
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "4. Vivace"
  \time 3/8
    gis'8 r r
    a r r
    b a16 g fis e
    dis8 e16 b e fis
}