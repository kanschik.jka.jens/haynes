 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
    c16. c32
    c8 a g f
    e32 f g e e8 r d'16. d32
    d8 bes a g
    f32 g a g f8 r4
}
 