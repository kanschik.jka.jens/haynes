\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4
  g'4. d8 e d16 e c fis32 g a16 c, c8 b r16 d g b
  a d, d32[ fis g a]
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/2
  g'16 fis g a g8 d e d4 e8 c b4 c8 a g r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Largo"
  \time 3/4
  e8 dis e b e g fis e fis b, fis' a
  g fis e b' c e, dis4. cis8 b4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 12/8
  g8 fis g  g fis g  d' cis d d cis d g fis g g fis g
  fis e d r r 
}	

