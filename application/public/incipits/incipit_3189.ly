\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Meine Seele, auf! erzähle [BWV 169a/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Recorder"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key c\major
                      \tempo "o. Bez."
                      \time 9/8
                      % Voice 1
                 c4 g8 c d e f d g
                 e g c e, g c d, f b
                c4. r8 r8
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe da Caccia"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \set Score.skipBars = ##t
                     \key c\major
                        % Voice 2
                   R4.*3*1 c,4 g8 c d e f d g
                 e g c e, g c fis, a c
                       
                
                  }
                  
               
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key c\major
   \tempo "o. Bez."
  \time 9/8
     \set Score.skipBars = ##t
    R4.*3*12 c4 g8 c d e f d g
    \grace f8 e4.~ e4 d16 e d4 r8
                   
   
    
    
    }