\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Gott, bei deinem starken Schützen [BWV 14/4]" 
    }
    \vspace #1.5
}
 
 
\relative c''' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\minor
                      \tempo "Vivace"
                      \time 4/4
                      \partial 2
                      % Voice 1
                      g8 a bes d,
                      es d c a'~ a c,16 bes c8 a'
                      bes, a g a bes a16 g a8 d8 ~ d
                 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                       r2 R1 r2 d8 e f a,
                       bes a g e'~ e g,16 f g8
                   
                       
                        
                
                  }
>>
}


\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key g\minor
   \tempo "Vivace"
  \time 4/4
  \partial 2
     \set Score.skipBars = ##t
    r2 R1*11 r2 g4 d'
    es4. d8 c bes a g
    a fis d4
    
    
    }