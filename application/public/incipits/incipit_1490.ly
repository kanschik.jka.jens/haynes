
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 2/2
  \partial 8
  bes8 f' d bes g' \grace f16 es16. d32 es8 r8 g8
  \grace g16 f8. c16 \grace c16 d8. f16 \grace f16 es 8 d r16
  

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/2
  \partial 8
  f16. g32 f8 bes a bes \grace a16 g8 f r8 d8
  c16. bes32 c4 f8 es4 d8 g
  f4 es d r4
   
}


\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key bes\major
                      \tempo "3. Vivace assai"
                      \time 3/4
                      % Voice 1
	    R2. r4 c2~ c4 bes8 a g f
                  es4 es'2~ es4 d8 c d4
                  c f2~ f4 e2 f2 r4  

                  }
\new Staff { \clef "bass"
                \key bes\major 
                        % Voice 2
                 bes,,,8 c d c d bes
                 es c f es f f,
                 bes4 bes' bes, c c' c,
                 d2
                  }
>>
}

