 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 3/4
  d8 es d c bes a
  bes4 g d' 
  g g4. a8
  fis2.
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro"
  \time 2/2
  \partial 4
  d4
  bes g bes d
  a2 r4 d
  bes g bes d
  a2 r4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Adagio"
  \time 2/2
  d8.[ es16] d16 es d es d8.[ c16] d c bes a
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Bourée"
  \time 2/2
  \partial 4
  d4
  bes g g  d'
  es2. d4
  c bes a d
  bes a8 bes g4 r
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "5. Allegro"
  \time 3/4
  g4 bes c
  d8 c bes a g4
  bes2 a4
  g2 r4
}
