\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Auf meinen Flügeln sollst du schweben [BWV 213/7]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key e\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                 e8 e,~ e16 fis g a b8 b b b 
                 c16 b c d c b a c b a b c
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                   R1*3 b'8 b,~ b16 cis d e fis8 fis fis fis
                   g16 fis g a
                       
                        
                
                  }
                
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor [Tugend]"
  \key e\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    R1*12 e8 e,~ e16 fis g a b8 b b b 
    c16 b c d c b a c b a b c
                      
   
    
    
    }