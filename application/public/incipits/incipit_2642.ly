 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Grave"
  \time 2/2
  << {
      % Voice "1"
  f'1 f8 c r a16. bes32 c16.[ a32 bes16. c32] d16. c32 bes16. a32 g4 r r2       } \\ {
      % Voice "2"
  a8 f a c a f a c
  a a r f16. g32 a16. f32 g16. a32 bes16. a32 g16. f32 e4 r r2    } >>
  
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Vivace"
  \time 3/2
  c'4 a a bes bes4. a16 bes
  c4 a a bes bes4. a16 bes c4 bes8 a bes4 a8 g a4 g8 f
  g4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Adagio"
  \time 4/4
  c'1 a4 bes c2 c b
  b4 a e'2 a, bes a
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allegro"
  \time 4/4
  c4 d r8 d c bes
  c a c4 r8 bes bes a
  a g c4
}