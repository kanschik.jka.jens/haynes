\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro assai"
  \time 4/4
  f4 r8 c16 f a4 r8 f16 a c4 c c r
  f,8 e d c c4 bes8 a g4 bes a
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Romabce poco Andante"
  \time 2/4
  \partial 8
  e16 f
  g4 e8 c'
  a8. g16 f e d c
  \grace c8
  b a16 g g'8 f
  f16 e d e c d e f
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo. Moderato"
  \time 4/4
\partial 4.
c8 d e
f4 f g2
a4 bes8 g f e d c
e d g f a g bes a
a2 g4
}
