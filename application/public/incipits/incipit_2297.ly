\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
    e2 d c4 r r c
    e2. d8 c
    f2. e8 d
    g1~ g
}

