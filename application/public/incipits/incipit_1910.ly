\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
   f2 e8 f g f
   bes4. a8 g r f r
   e4 f a g f r r2
}
  
