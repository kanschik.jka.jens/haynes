 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Aria"
  \time 2/2
  \partial 2
  r2
     \set Score.skipBars = ##t
   R1*11
   r2 d4 fis e g fis e8 d
   cis4 a r2

}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Aria"
  \time 3/4
  \partial 4
  r4
   \set Score.skipBars = ##t
   R2.*9
   r4 r4 fis,8 g
   a4 a d8 e
   cis4 a r
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Aria"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*10
   d4 f e d2 a4 bes a g
   f2 e4 f e d
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4. Aria"
  \time 3/4
  \partial 4
  r4
   \set Score.skipBars = ##t
   R2.*7 
   r4 r4 a
   d fis8 e d cis
   d4 a b g e a fis d r
}