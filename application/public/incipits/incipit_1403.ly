 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
  bes16 [c bes c] bes c a bes c [d c d] c d bes c
  d8[ es16 d] c bes a bes a4
}
 
\relative c'' {
  \clef alto
  \key bes\major
   \tempo "2. Allegro"
  \time 2/2
  r2 r4 r8 f,8
  bes[ a16 g ] f es d c bes[ a bes c] bes c a bes
  c[ bes c d] c d bes c d8 bes r16
}


\relative c'' {
  \clef alto
  \key bes\major
   \tempo "3. Adagio"
  \time 3/4
  g2~ g16 bes a g
  fis2 r4
  g2~ g16 f es d
  es2 r4
}

\relative c'' {
  \clef alto
  \key bes\major
   \tempo "3. Allegro"
  \time 6/8
  \partial 8
  f,8
  d es d c bes a c f c c f c d c bes d c bes
  a8. bes16 c8 f,4
}

