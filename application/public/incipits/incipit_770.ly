 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
    g8
    d'16 bes a g g'8 d \grace f8 es8 d g,4~
    g8 \times 2/3 { fis16[ g a] } d,8 c' c bes16 g g'4
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro moderato"
  \time 4/4
  \partial 8
    d8 d16 bes a g g'8 es d16 bes a g g' fis g es
    es8 d r
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Gratioso"
  \time 3/8
  \partial 8
    d8
    d g, a bes16 g d' bes g' d
    d8 g, a
    bes16 g d' bes g' d
    es8[ g,]
}
