\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. [Allegro]"
  \time 2/2
c16[ c c c] d d d d   es[ es es es]  f f f f
g8 f16 e d[ c b a] b8 g r
}



\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Menuet alternativem."
  \time 3/4
  c4 g' g
  es2 d4
  f as8 g f es d2 d4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Lauter"
  \time 3/4
  c,4 c8 d es f
  g f g a b g
  c b c es d c
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Gigue alternativement"
  \time 6/8
  \partial 8
  g8
  es f g as g as
  g f g c,4
}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "5. Lauter"
  \time 6/8
  \partial 8
  g8
  c d es es d c
  b c d g,4
}

