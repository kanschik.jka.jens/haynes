 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Grave"
  \time 4/4
  \partial 16
    bes16
    es8.. es32 g16.. es64 g16.. es64 bes8.. bes32 es16.. bes64 es16.. bes64
    g8 r 
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "Allegro, ma non troppo"
  \time 3/4
  \partial 4
  bes8 g'
  g2 f8 es
  \grace {d16[ es f]} es4. d8 c bes
  \grace bes8 as4 as as as r
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Andante cantabile"
  \time 2/4
  f8 es d c
  bes8. d32 c bes8. c32 d
  \grace f8 es8 d c bes
  a8 c32 bes a bes c8. cis16
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Rondo. Allegro, ma non troppo"
  \time 6/8
  \partial 8
    bes,8
    es4 es8 es4 es8
    g4 f8 es4.
    f as4 g8 f4 r8
}