 \version "2.16.1"
         #(set-global-staff-size 14)
         
     \relative c'' {
  \clef treble
  \key  bes\major
 \tempo "1. o.bez."
  \time 2/2
    \override TupletNumber #'stencil = ##f
  \override TupletBracket #'stencil = ##f
    <d, bes'>4 r8 f32 es d c
    bes8 d f bes
    <f c'>4 <f d'> r f'8 bes
    bes4. a16 bes c8 a es' es, es16 d g f f4
     }
