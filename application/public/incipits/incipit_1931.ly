\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 2/4
  g'4 e8 e32[ f g a]
  g4 e8 e32[ f g a]
  a16 g g f f e e d
  d4 e8 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Tempo di Minuetto"
  \time 3/4
  \partial 4
  g'4
  e2 f4
  d2 e4 c2 d4
  g,4. g8 g8. f32 g
  c2
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Moderato"
  \time 4/4
  d4~ d16 e fis g  dis e fis e e8 r
  fis16 g a b cis a g fis fis g a g d8 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Presto"
  \time 3/8
  d8 d d
  e32 fis g8. e8
  d d16 e d e
  d4 r8
  cis cis a b4 g8 a a16 b a b a4 r8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante con moto"
  \time 3/4
  r8 a' fis d a fis
  e8. fis16 g4 r
  r8 g' e cis g e
  fis8. g16 a4 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegretto"
  \time 2/4
  fis8. g16 a8 a
  d,8. e16 fis8 r
  b,4 a'16 g fis e
  d cis b a b cis d e
  fis8. g16 a8 a
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 4/4
  f2. e16 f g f
  e8. d16 d2 cis16 d e d
  c8. bes16 bes2 a16 bes d bes
  \grace c8 bes4 a r2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Tempo di Minuetto"
  \time 3/4
  a'2 g4
  g f e16 f g a
  bes2 a4
  a g f16 g a bes
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Andante"
  \time 6/8
  r8 es, g bes es g
  g4 f8 es4 r8
  r g, bes es g bes
  bes4 as8 g4 r8
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Grazioso"
  \time 2/4
  \partial 4
  bes8 es
  es16 d c bes bes8 g'
  g16 f es d bes8 bes'
  bes16 as as g g f f es
  es d f8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 3/4
  f,16 a c f
  f,4 r
  bes8 r a r g r
  f16 a c f
  d'8 r c r bes r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andante dell'Ipocondriaco"
  \time 2/4
  g'4 f16 e a g
  g4 e8 f
  d e c d
  c16 b d c  b d e f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Presto"
  \time 2/4
  \partial 8
  c8
  \times 2/3 { f8[ a c] }  \times 2/3 { bes8[ g e] } 
  \times 2/3 { f8[ a g] }  \times 2/3 { f8[ e d] }
  \times 2/3 { c8[ bes a] } \times 2/3 { g8[ a bes] } 
}
