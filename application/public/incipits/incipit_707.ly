 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegretto"
  \time 4/4
    g'8 a g a g a
    g bes es d r4
    fis,8 g fis g fis g
    fis a cis d r4
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Andante"
  \time 6/8
  es,2.~ es4.~ es8 f g as g as
  g bes as g4. f4 r8
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Menuetto. Allegretto"
  \time 3/4
   g2.~ g4 a fis
   g bes d
   a2 r4
   bes2.~ bes4 c a
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Allegretto"
  \time 2/4
  g'8 cis d4~
  d8 es16 d cis8 d
  bes fis g4~
  g8 a16 g fis8 g
  c8 d es4
}