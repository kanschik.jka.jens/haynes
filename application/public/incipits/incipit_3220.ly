\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Und wenn der harte Todesschlag [BWV 124/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key fis\minor
   \tempo "o. Bez."
   \time 3/4
   r4 r8 cis8 fis16 gis a fis
   \grace cis16 bis4~ bis16 fis e fis a8. gis16
   \grace fis16 e4~ e16
   
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key fis\minor
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
  R2.*8 r4 r8 cis8 fis8. a16
  bis,4. fis8 a8. gis16
  e2.~e2 r4
   
   
}


