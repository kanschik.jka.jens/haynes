\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto [III] " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Allegro Moderato  C-Dur  3/8"
  \time 3/8
    g4. e8 f g
    a4 b8 c r8 r8
    f,16 g f e f8
    e16 f e d e8
    d16 e d c b a g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Menuetto  C-Dur  3/4"
  \time 3/4
  c4 r8 c8 b c 
  d e f g a b c4 g g
  g
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Andante poco Adagio  F-Dur  2/4"
  \time 2/4
  c2 \grace c8 c'2
  c8. a16 bes g f e g f f8 r8 f8
  e16 d d8 d32 e f e g f e d 
  d16 c c8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Rondo. Allegro  C-Dur  2/4"
  \time 2/4
  \partial 8
  e16 f
  g8 r8 r g16 e
  g8 r r e16 f
  g8 r r c16 a
  g4 r8 g
  c c c c
  c16 b a g f e d c b c d e f d e c
  d4
}

