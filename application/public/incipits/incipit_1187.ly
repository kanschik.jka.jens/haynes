\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro moderato"
  \time 2/4
  \partial 8
  a8
  d d4 e16 cis
  d8 d4 e16 cis
  d cis d8 r e
  fis fis4 g16 e fis8 fis4 g16 e fis e fis8 r
  
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Andantino"
  \time 2/4
  \partial 8
  f16 g
  a8 d, \grace f8 e8 d16 cis
  d8 a4 f'16 g
  a8 d, g f
  \grace g8 f8 e r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegretto"
  \time 3/8
  \times 2/3 { fis,16[ g a]} a4
  \times 2/3 { b16[ a g]} b8 cis
  \grace cis8 d8. a'16 d, fis
  \grace fis8 e8. a16 e g
  \grace g8 fis8.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato"
  \time 2/4
  f4~ f16 bes a g
  f4~ f16 bes a g
  \times 4/6 { f16[ g a bes a g] } f8 es
  d16 c bes8 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Siciliana"
  \time 6/8
  f8. a16 f8 c d e
  f8. a16 f8 c4 bes8
  a16 bes c8 a g16 a bes8 g
  a16 bes c8 a g16 a bes8 g
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/8
  bes8 d f
  \grace c'8 \times 2/3 { bes16[ a g]} f8 bes
  g f es
  \times 2/3 { d16[ c bes] } bes8 c
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
  g8 g' g g
  g fis16 e d8 c
  b e e e 
  e d16 c b8 a
  g e' e e e d4 e8
  e d r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Affettuoso"
  \time 3/8
  \times 2/3 {bes16[ c d]} d8 es
  d g bes~
  bes a g
  g fis a
  d,4
  }
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Giga. moderato"
  \time 6/8
  \partial 4.
  r8 b c
  d d d \grace d8 g4 d8
  c4 b8 r g a
  b b b b4 b8
  a4 g8 r
}
  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Moderato"
  \time 2/2
  g'8. c,16 c8. c16 c8. c16 a'4
  g8. c,16 c8. c16 c8. c16 a'4
  g8. c,16 d8. f16 e4 d
  e8. c16 d8. f16 e4 d  
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Affettuoso"
  \time 3/4
  \partial 4
  c4
  g'2.~ g
  f4 g \grace es8 d4
  es32 d c8. c4 es~
  es d c
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro moderato"
  \time 3/8
  g'4 c,8 \grace e8 d8 c r
  g'4 c,8 b16 a g8 r
  
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro moderato"
  \time 2/4
  \partial 8
  c8
  f a4 g16 f g e f8 c \times 2/3 { f16[ g a] }
  c,8 \times 2/3 { e16[ f g] }
  c,8 a' \grace bes8 a8 g r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Amoroso"
  \time 3/4
  g'2. \grace f8 e8 d16 c
  b4 c
  d8. e16 f4 e
  a8 g16 a
  a,4 d8. e16
  \grace c8 b4. a8 g4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Giga. moderato"
  \time 6/8
  a8 bes c c f d
  d4 d8 d g e
  \grace e4 f4.~ f8 g a
  a4 g8
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 4/4
  cis8 e a,2 fis'4
  fis e r a, gis8 b e,2 d'4
  d cis r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Andantino"
  \time 2/4
  \partial 8
  a8 c b e, a
  \grace a8 gis4. b8
  e d4 c16 b
  c b a8 r
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Allegretto gratioso"
  \time 3/8
  e4. cis8 b a
  \grace e'8 d8 \grace cis8 b4
  cis16 a fis'8 e
  r16 b e8 d
  r16 a d8 cis
}
