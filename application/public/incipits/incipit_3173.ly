\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Ich bin herrlich, ich bin schön [BWV 49/4]" 
    }
    \vspace #1.5


}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key a\major
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    R1*4 r2 r8 e4 fis16 gis
    a gis fis e cis'8 b b2~
    b8 e16 dis cis b a gis a fis dis' cis b a gis fis
    gis8 
  



}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Violoncello picc."
  \key a\major
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    r8 a4 b16 cis e cis b a fis'8 e
    e2~ e8 a16 gis fis e d cis
    d b gis' fis e d cis b cis8
  



}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key a\major
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    R1*12 r8 a4 b16 cis d cis b a fis'8 e
    e1~ e8 a,4 b16 cis d cis b a 
   
    
    
    }
    

