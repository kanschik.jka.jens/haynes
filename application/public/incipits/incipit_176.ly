\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio [Tutti]"
  \time 4/4
  es8 es es es  es es d d
  g g g g  g g f f
  f f es es as, as g g
  g4 f
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio [Solo]"
  \time 4/4
    r2 f8 d bes as
    g es~ es f64 g as bes c d es f g8 es c bes
    as f
}
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 2/4
  es16 f g as bes8 d,
  es bes es, bes'
  es16 f g as bes8 d,
  es bes es, bes'
  
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Grave. Andante"
  \time 2/2
  g'2 c f, r
  g d' es, r
}
\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Allegro"
  \time 3/8
    es8 es4
    es8 d16 c d es
    f8 f4
    f8 es16 d es f g8 g4
}
