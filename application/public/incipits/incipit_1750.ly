\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Vivace"
  \time 2/2
  \partial 8
   a16 e
   a8 b cis d e4 fis
   e8 d16 cis d8 e
   cis4 r
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Soave"
  \time 4/4
  \partial 8
  b'8
  gis16 fis gis b fis8 b e,16 dis e4 cis8
  b16 a b e a,8 fis' a,16 fis gis4 ais8
  b4
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Vivace"
  \time 6/8
  e4 fis8 e d16 cis d8
  e4 fis8 d4 cis8
}