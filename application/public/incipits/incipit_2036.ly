\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sinfonia I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Con Spirito"
  \time 3/4
  \partial 8
   \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  r16 d16
  \grace es8 \times 2/3 {d8 c d} bes'8.[ g16 es8. d16]
  \grace d4 c8. b16 c4 r4

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. L'Arte della Fuga. Moderato"
  \time 4/4
  r4 d4 bes g
  es'2 d8 fis g4~
  g fis g r4
  r8 a8 a[ a] a bes16 a g f e d 
  cis8 a r8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gratioso. Pastorella"
  \time 6/8
  \partial 4.
  b4 c8
  d c b e d c
  c4 d8 a4 b8
  c b a e' d c
  \grace d4 c4 b8 d4.~d

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Gavotta"
  \time 2/2
  \partial 2
  g8 bes a c
  c bes a g bes d c es
  \grace f4 es4 d g8 bes a g
  g fis g a d, c bes a 
  a4 bes
  

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sinfonia II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 16
  c16 c4 d16 e f8 f4 e8 d
  c16 e f a \grace c,4 bes4 a r4

}

\relative c''' {
  \clef treble
  \key a\minor
   \tempo "2. Andante"
  \time 3/4
  a4. c8 b gis
  g4 a8 g16 f e4~
  e8 f f e e d
  \grace d4 c4. b8 a4

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Aria. Tempo di Minuetto"
  \time 3/4
  c2. e16[ d8. f16 e8. g16 f8.]
  \grace {g16[ a]} bes4 a g
  \times 2/3 {a8 g f} f4 r4

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sinfonia III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Moderato"
  \time 4/4
  \partial 8
   \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  d8 g g g a16 fis fis4 g8 d
  dis8. e16 d c b a \times 2/3 {b16 a g} g4 r16 g'16
  fis8 e d8. e16 e d d c c8 b16 d
  c8[ b]

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Aria Gratioso"
  \time 2/2
  \partial 2 
  d4. g8
  g fis e d e4. fis16 g
  d2 r2

}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Giga. Allegro"
  \time 6/8
  \partial 4.
  d4 c8
  b a g e'4 e8
  e4 d8 r8 r4
  

}
  
 
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sinfonia IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 2
  r2 r2 f2~
  ~f4 es8 f16 g a,4 bes~
  ~bes8 bes'16 a g f es d 
  \grace es4 d8 c r4

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
   \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  r16 f16
f8. \times 2/3 {bes32 a g} f8 es es d32 g f es d8 c
  \grace d4 c8[ bes] r16 

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Aria"
  \time 3/4
  \partial 8
  f8 f4 es8[ d \grace d4 c8 bes]
  \grace {es16[ f]} g4 f r8 bes8
  bes16[ a8. a16 g8. g16 f8.] 
  f4 es d

}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sinfonia V " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  c4. b8 a2
  g1~
  ~g8 c, d e f g a b
  c as~ as16 g g fis fis4 g8 r8

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Fuga Moderato"
  \time 4/4
  c4 c'4. b16 a b8 g
  a,4 a'4. g16 f g8 e
  f,4 f'4. e16 d e8 c
  d16 f e d g f e d e8 c r4

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Andante"
  \time 3/4
   \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  g8. a16 g4 f
  \grace g4 \times 2/3 {f8 e f} e4 r4
  r8 a8 g f e d
  dis4 e r4

}

\relative c' {
  \clef treble
  \key c\major
   \tempo "4. Aria"
  \time 2/4
  e8. g16 c8. e16
  e4 d
  c,8. e16 a8. c16
  c4 b
  a8.[ c16 g8. c16]
  \grace g4 f4 e
  
  

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sinfonia VI " 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  fis8 g a b cis d e fis
  g, a b cis d e fis g
  a, b cis d e fis g a
  b,4. b8 a4 r4

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Allegro ma non troppo"
  \time 4/4
  r2 r4 b4
  a16 a g fis g g fis e \times 2/3 {fis16 e d} d4 r4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Aria tempo di Minuetto"
  \time 3/4
  \grace e4 \times 2/3 {d8 cis d} a8 d fis a
  \grace a4 g2 fis4
   \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \times 2/3 {b8 a g} fis4 e
  \times 2/3 {fis8 e d} d4 r4

}


