\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Concert " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prélude. gravement "
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \time 4/4
  \partial 2
  d4~ d8. e16
  fis,4. r16 d16 g8. \times 2/3 {a32 g a} a8. g32 a
  \grace a8 b8 \grace a8 g8 r4 g'4. fis16 e
  \grace e8 d4. \grace c8 b8 \grace a8 g4 \grace {a16[ g fis g]} g8. fis32 a
  \grace g8 a4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allemande. Legerement "
  \time 4/4
  \partial 8*5
  b16 c a8 b16 c d8 d,
  g8. g16 c8. c16
  c8 b16 c a8 d
  b \grace a8 g g'8. g16 g4
  
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Sarabande "
  \time 3/4
  g4 d'4. es8
  c4. bes8 c4
  d g,4. c8
  bes2 \grace {a16[ bes]} a4
  bes8. c16 c4. bes16 c
  d8 es d es f g
  a, d c4. bes8 bes2

}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Gavotte"
  \time 2/2
  \partial 2
  d8 es d c
  bes a g a bes a g fis
  \grace fis8 g4 d g8 a bes c
  a bes c d c bes a g a2

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "5. Gigue. Legerement"
  \time 6/8
  \partial 8*5
 g8 fis g d e
 d \grace c8 b e d c4 \grace {b8[ c]}
 b8 b c d g, a b e, fis g \grace g8 a4

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "6. Menuet en Trio"
  \time 3/4
  g8 a bes c d es
  d2 c4 d c bes
  a g es' d c bes c \grace bes8 a4 

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Second Concert " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Prélude. gracieusement "
  \time 3/4
  \partial 4
  d4 fis4. e8 d fis
  \grace fis8 e4 \grace d8 cis4 \grace b8 a4
  d g4. fis8
  e4 \grace d8 cis4 

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Allemande. Fuguée. gayement "
  \time 4/4
  \partial 8*5
  a8 fis a e a
  d, a' cis, a' g fis16 a e8 a
  fis8 e16 fis d8 a d8. e16 e8. d32 e
  fis8 
  
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Air tendre "
  \time 3/4
  f4. e8 d4
  \grace c8 bes4 a d
  a4. g8 a4
  \grace g8 f2 \grace e8 d4
  a''4. g8 f e
  d4 cis d

}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Air contre fugue. vivement "
  \time 2/4
  \partial 8*3
  a8[ a e]
  \grace e8 f8[ a a cis,]
  \grace cis8 d8 a'16 b cis d b cis
  d e cis d e f d e 
  f8 f16 e
  f8 c
  

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "5. Èchos. Tendrement "
  \time 3/4
 a8[ g fis g g8. fis32 g]
 \grace g8 a2 r4
 a,4 d4. e8
 cis2 \grace b8 a4
  

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Toisiême Concert " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "1. Prélude. Lentement "
  \time 4/4
  \partial 16
  a16 a4~ a8 gis16 fis e4~ e8 d16 cis
  \grace cis8 b8 \grace a8 gis8 \grace gis8 a8. b16 cis4 b8. a16
  a8 r16

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allemande. Légerement "
  \time 4/4
  \partial 4*3
  r16 a16 gis fis e8 a8~ a gis
  a d16 cis b8 e16 d cis e a cis, b8. a16 a8 e' fis4~ fis8 e16 d e4~ e8
  
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Courante "
  \time 3/2
  \partial 8
  c8 c4. b8[ a gis] a b b4. a8
  gis4. b8 e4 d c4. b8 \grace b8 c2 \grace b8 a4

}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Sarabande. Grave "
  \time 3/4
  a4 d4. d8
  d2 r8 r16 b16 e8. gis,16 \grace gis8 a4. b8
  b2 \grace {a16[ b]} a4
  a' g4. g8 g2.
  

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "5. Gavotte "
  \time 2/2
  \partial 2
   a8 c b d
   c4 \grace d8 d4 \grace {c32[ d]} e4 d8 c
   b4 \grace {a32[ b]} a4 e' \grace fis8 fis4 \grace {e32[ fis]} g4 \grace f8 e4
  

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "6. Muzette. naiuement "
  \time 6/8
  \partial 4.
  cis4 d8
  e4 a,8 fis'4 gis8
  a4 e8 e4 fis8 e4 d8 cis4 b8
  cis b a cis4 d8 e4 a,8
  

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "7. Chaconne. Legere "
  \time 3/8
  \partial 8*2
  e8 b \grace b8 c8 e16 d c b a8 e' f
  e16 d d8. c16
  c8
  

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatriéme Concert" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Prélude. Gravement"
  \time 4/4
  r8 r16 e16 g8. fis16 \grace fis8 e8 dis \grace dis8 e8. fis16 
  dis8 r16 b16[ e8. fis16] \grace fis8 g8[ \grace {fis16[ g]} a8] a g16. fis32
  fis8 r16
  

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allemande. Legerement"
  \time 4/4
  \partial 8*5
  g16 a b8 a16 g fis8 g16 a
  g8 \grace fis8 e8 c'8. b16 a8 \grace g8 fis8 d'8. d16
  d8 b e8. d16 cis8[ \grace b8 a8]
  
}


\relative c'' {
  \clef treble
  \key e\major
   \tempo "3. Courante. Francoise. galament "
  \time 3/2
  \partial 8
  e8 e4. dis8 cis4 b a4. b8
  gis4 \grace fis8 e4 b' e dis4. e8 
  \grace e8 fis2 dis2.

}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Courante a L'Italiene. gayement "
  \time 3/4
  \partial 8*3
  e8 d c
  b c b a g fis
  \grace fis8 g4 \grace fis8 e8 g'[ fis e] 
  dis4 dis fis
  b,8 a b cis d e
  cis4 \grace cis8 dis4. e8 dis4 r8

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "5. Sarabande. Tres tendrement "
  \time 3/4
  b4 \grace b8 cis4. dis8
  \grace cis8 b4 e4. gis,8
  \grace gis8 a4 fis4. fis8
  b4 gis r4

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "6. Rigaudon. légerement, et marque "
  \time 2/4
  \partial 8
  b8 e4 e
  e4. e8
  dis[ cis dis e]
  dis cis16 dis b8 dis16[ e] fis4 fis
  fis4. gis8 fis[ e dis cis] dis8 cis16 dis b8
  

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "7. Forlane. Rondeau. gayement "
  \time 6/8
  \partial 8*4
  gis8 gis4 fis8
  fis4 a8 a8. b16 gis8
  gis4 b8 b4 cis8
  b8. e16 b8 b8. cis16 b8
  gis[ e]

}
