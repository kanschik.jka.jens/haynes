
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Largo"
  \time 4/4
 c8 es16. d32 c8 c c16. b32 c8 r8 g'16 d
 d8 es16 f es8. d16 es16 d c8 r8 bes'16 f
 f8 g16 as g8. f16 g16 f es8 r16

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro"
  \time 2/2
 c4 d es r8 f8
 \grace es8 d8 \grace c8 bes8 r8 es8 \grace d8 c4 r8 d8
 b8 g g'4~ g8 f es d 
 c bes as4 

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Adagio"
  \time 3/2
   g4 g g g g g8 as16 bes 
  f4 f f f f f8 g16 as
  g4 g g g g g8 as16 bes
  f4 f f f f f8 g16 as 
 g2 
 

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 3/4
  r4 g4 d es b2
 c4 as'2 g4 f8 es d c 
 b a g4 r4


}

