
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
  \tempo "1. Allegro"
  \time 4/4
  es4 es,8 g32 f es16 bes'4 bes,8 d'32 c bes16
  es8 es,16 d es f g as bes8 bes, r4
}
