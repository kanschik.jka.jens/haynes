 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef bass
  \key c\minor
   \tempo "1. Adagio [Basso]"
  \time 4/4
    c,,16. c'32 b16. c32 c,8 c' g,16. d''32 c16. d32 g,,8 f
    es16. es''32 d16. es32 c,8 es' bes,16.
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio [Oboe]"
  \time 4/4
      \set Score.skipBars = ##t
   R1*3 r2 g'8. f16 es8 d16 c
   b4. g8 g' b,16 c as'8 c,
   d16 c bes8
 
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Allegro"
  \time 4/4
  \partial 8.
  c16 b c
  g c b c f8 d es8.[ d16 c8 es]
  d[ c16 b c8 d] g,4 r16
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Andante"
  \time 3/2	
  r2 g' as
  b,1 c2
  r es c
  as'4. g8 f4. es8 d4. c8 b4. a8 g2 r
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Allegro"
  \time 9/8
  g8 f g c es g es d c
  g f g c es g es d c
  b c d g, b d  f es d
  es d c g' f es d es c c4 b r
  
}