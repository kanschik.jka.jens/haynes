\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
    g'4 d g, r8 b,8
    a b d d 
    g4 r8 b, a b d d g4 r8 a16 b
    a8 d, d4
}
