\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Die Seele ruht in Jesu Händen [BWV 127/3]" 
    }
    \vspace #1.5
}
 
 
\relative c''' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Recorder 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key c\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                   g8 g g g f f f f
                   f f b b c c b b 
                   c c c c 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Recorder 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key c\minor
                     \set Score.skipBars = ##t
                        % Voice 2
              es,8 es es es
              d d d d
              as' as d, d
              g g g g
              g g g g 
                       
                        
                
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob 3"
                     \key c\minor
                     \set Score.skipBars = ##t
                        % Voice 3
                r8 g,8 c16 d d es b2~
                ~
                b32 a b c d es f g as16 g g f f d es8 r16 d32 es64 f es16 d
                es b c8 r16
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key c\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    R1*8 r8 g8 c16 d d es b2~
    ~
    b16 b d f f g32 as g16 f f es f8 r4
   
    
    
    }