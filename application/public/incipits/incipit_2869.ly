 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

 

 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Tutti]"
  \time 2/2
  f4 r8 g a16 a g g f[ f c c]
  a a bes bes c[ c f, f]
  f' f e e d[ d d d]
  g[ g f f] e e e e  a a g g f[ f f f]
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Solo]"
  \time 2/2
  \set Score.skipBars = ##t
    R1*8
    c4 r16 f e d
    c8[ d16 bes] c8 d16 bes
    c16[ bes a g] f8 f' g16[ f g a] g a f g
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 2/2
  R1
  c4. f8 bes,4. c16 bes
  a8 f c' c
  f4. g16 f
  e4 g2 f4~
  f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro [Tutti]"
  \time 3/4
       r8 f, a c f a
       g e c e g bes
       a g16 a f8 a g e
       f e16 f d8 g g f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*12
    r8 f, a c f a
    g16 f e d c d e f g a bes g
    a bes a bes g a g a f a g f
    e4 r r
   }


