 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Moderato"
  \time 4/4
  \partial 4
    f,4
    c'2 bes8 a g f e d d2 f'4~
    f2 e8 g bes e, f c c2
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegretto"
  \time 6/8
  \partial 8
  c8
  c a c bes g e f4 a8 g4 bes8
  a f' c c bes a g g g g4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Largo"
  \time 6/8
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}