\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro Moderato"
  \time 2/2
  \partial 4
    g4
    c4 c2 c16 b c d
    e4 e2 \grace f8 e8 d16 c
    f8 f f f  f[ a16 g]
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Romance. Andante"
  \time 2/4
    e8. f16 g8 g
    g8. a16 d,4
    e16 g c,4 e16 c
    b a f' d c b a g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo"
  \time 3/8
  e8 e e
  g4 f8
  d d d f4 e8
  c c c \grace b8 a4
}