 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo"
  \time 4/4
    g4 r16 g a16. bes32 a16 g a8 r16 a bes16. c32
    bes16 a g8 r16 fis g16. a32 fis8 d' r16 f, f16. e32
    e8 c' r16
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro"
  \time 2/2
  g16[ a bes c] d8 es es[ d] r g
  a16 g fis e d8[ c] bes16[ a g8] r es'~
  es[ d16 c] d8 g
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Intermezzo"
  \time 2/4
  \partial 8
  bes8 es16 f g f es8 es
  es d16 c bes8 es
  f16 es d c bes8 as g16 f es8 r4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Allegro ma non presto"
  \time 2/2
  \partial 4
  g'4
  fis g a bes
  c,8 b c4. es8 d c
  bes4 a8 bes c bes a g d'2. r4
}