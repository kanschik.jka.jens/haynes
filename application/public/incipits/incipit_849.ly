\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8
  f8 bes bes bes bes
  bes a16 g  f g f es
  d c d es f es f g
  \grace d8 c4 r8
}
