 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Larghetto"
  \time 6/8
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
    f8. g16 f8 f \grace es'16 d8 c16 bes
    bes4. a4 g8
    f bes d \grace f,8 es4. d4 r8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegretto"
  \time 4/4
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
    f4 bes, \grace bes8 a4. bes8
    c d \grace d8 es4 d r8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro ma Vivace"
  \time 3/4
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
        f2 \times 2/3 { es8 f g}
        \grace f8 es2 d4
}