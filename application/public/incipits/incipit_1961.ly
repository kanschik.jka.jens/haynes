 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "1."
  \time 2/2
  a8 a16 a a8[ a] d d16 d d8[ d]
  e16 d e fis e[ a g a] fis[ a g fis] e d cis b
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2."
  \time 6/4
  r4 d d e e8 fis g4
  fis fis8 g a4 d, g8 a g fis
  e4
}
