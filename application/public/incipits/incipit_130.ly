\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Vieni o viventi [tutti]"
  \time 3/4
  f8. e16 f8. f16 e8. f16 g8. f16 g8. g16 f8. g16
  a8.
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Vieni o viventi [Soprano]"
      \set Score.skipBars = ##t
  \time 3/4
  R2.*19
  c4 a4. bes8
  c4 f, f'~
  f e8 d c bes
  a4. g8 f4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. [tutti]"
      \set Score.skipBars = ##t
  \time 2/4
  d4 e fis4. e8
  d16 cis d e  d fis e d
  cis e d cis d e fis gis a8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. [Sopran]"
      \set Score.skipBars = ##t
  \time 2/4
  R2*23
  d4 e fis4. e8
  d16 e  fis g a8 d, cis4 d
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. La Tortorella"
      \set Score.skipBars = ##t
  \time 6/8
  \partial 8
  d8
  g4. a
  b8. a16 g8 d'4.~
  d4 c8 b4 a8
  b8. a16 g8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. [La Tortorella]"
      \set Score.skipBars = ##t
  \time 6/8
  r4 r8 r4 d,8
  g4. a
  b8. a16 g8 d'4.~
  d4 c8 b4 a8
  b8. a16 g8
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "5. Bramo auer "
      \set Score.skipBars = ##t
  \time 3/8
  bes8 as4 g4.
  bes8 es as, g4.
  bes8 es as, g es16 f g as
  bes4.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "6. Solpirando [Flauto] "
      \set Score.skipBars = ##t
  \time 2/2
  g'2~  g16. g32 a16. b32  c16. d32 b16. a32
  g16. a32 c16. e,32  f8 f16. g32 e4
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "6. Solpirando [Soprano] "
      \set Score.skipBars = ##t
  \time 2/2
  R1*6
  r4 r8 c16 b c8 g r e'16 d
  e8 g16 e c8 e16 c d8 d d g16 d
  c b c8 r
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "7. Da quei...."
      \set Score.skipBars = ##t
  \time 12/8
  e8. fis16 e8 a4. gis r8 r fis
  e4 dis8 e fis16 gis a b
  cis4 b8 a4.
}