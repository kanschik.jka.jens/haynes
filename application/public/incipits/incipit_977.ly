\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
  g'8 d g d
  g fis16 e d8 c
  b a b cis
  d d, r d'
  d d,
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 4/4
  e8 b e g16 fis fis8 b, fis' a16 g 
  g8 fis fis e e fis16 g fis8 e 
  dis4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Rigadon"
  \time 2/2
  \partial 4
  d4
  b a8 g c4 d e2 d
  c4 b8 a b4 a8 g 
  a g fis e d4
 } 

  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Passpiet"
  \time 3/8
  \partial 8
  g8
  b16 g a b c8
  d d g
  fis fis16 g a8
 } 
