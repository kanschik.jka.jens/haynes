\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Grave [Tutti]"
  \time 4/4
  g'8 g, r16 g' g16. g32 a8 d,, r16 a''16 a16. a32
  bes8 bes, r16 d d16. d32 es8[ c,]
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Grave [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*5 	
   g'8 a16. fis32 g8 fis16. g32 a8. bes16 a fis g a
   bes8 g r16 d g16. d32 es4~ es8 d16. c32
   d4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 4/4
  d8 g, es'4~ es8 d16 c d8 d
  d g, c4~ c8 bes16 a bes8 bes
  bes a16 g a4~ a8 d, g4~
  g fis g r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Largo moderato a cantabile"
  \time 3/4
  f4 f4. es8
  d4. es8 f4
  bes, g' es
  c4. c8[ d es]
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro [Tutti]"
  \time 3/4
   g'8 g, r g' g8. fis32 g
   a8 d, r a' a8. g32 a
   bes8. d,16 a'8. d,16 g8. a16
   fis4 r16
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro [Solo]"
  \time 3/4
     \set Score.skipBars = ##t
   R2.*10 
   d2 g4~
   g a2
   bes4. bes8 a8. bes16 g8 a a4. g8
   fis2 r4
}