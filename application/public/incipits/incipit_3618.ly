\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
      \partial 4
      d8 f
      bes,4 bes bes bes
      c8 a \grace a8 bes2 bes'8 a
      bes4 bes, c8 a bes4
      es, d
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 3/4
       \set Score.skipBars = ##t
   R2.*12 
    g4 g16 bes a g es'8 es
    es4 d r
    fis16 g a c, c d es a, a f d' c
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 2/4
      d16 es f4.
            d16 es f4.
            f8 d16 f g8 f
            es d r4

}
