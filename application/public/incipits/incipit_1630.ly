 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. "
  \time 2/2
  \partial 4
    c8. c16
    f2 c4 c8. c16
    g'2 c,4 c8. c16
    a'2 g8 f g a
    f4 a8. a16
    f4 r
}


\relative c'' {
  \clef treble
  \key f\minor	
   \tempo "2. Adagio"
  \time 2/2
  c,2 as4. g8 f2. <as c>4
  <g bes>2. <as c>4
  <g bes>2. r4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 3/8
  \partial 8.
  c16 f g
  \grace {f16[ g]} a8 f16 c f g
  \grace {f16[ g]} a8 f16 f g a
  bes a g f e f
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}