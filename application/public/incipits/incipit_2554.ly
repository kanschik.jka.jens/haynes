\version "2.16.1"

#(set-global-staff-size 14)

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. 1 Nun danket alle Gott" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Vivace moderato [Organo]"
  \time 4/4
  <g b d g>8 r g16 b d g  g fis a g  fis e d c
  c8 b  d,16 g b d  c b a g  fis e d c
  c8 b g'4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Vivace moderato [Oboe]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*16
   r2 d d d e e 
   d1~ d2~ d4 r
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. 2 Befiehl du deiner Wege" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "Andante molto [Organo]"
  \time 4/4
    r8 bes es d d4 c8 bes
    as4~ as16 as bes c \grace { bes16 d } c8 bes d8. c32 bes
    \grace c16 bes8[ as]
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "Andante molto [Oboe]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*8
   r4 bes es d c bes as2
   bes1~ bes4 r4 r2
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. 3 Herr, ich habe mißgehandelt" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "Largo molto [Organo]"
  \time 4/4
  c16 e f e  gis, e' f e  a, e' f e  b e f d
  c e gis a  a b, r f' f e r a, a gis b d
  c4 b a
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "Largo molto [Oboe]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*7
   a4 gis a b
   c d c b a1
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. 4 Nun freut euch lieben Christen" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Andantino [Organo]"
  \time 4/4
  \partial 8
  d8
  d b b g g4. g'8~
  g16 a, c e  d c b a  \grace c16 b16 a g8 \grace b16 g'4~
  g8 e \grace fis16 e8 d \grace { cis16 e } d8[ cis]
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Andantino [Oboe]"
  \time 4/4
  \partial 8
  r8
   \set Score.skipBars = ##t
   R1*19
   r2 g
   g b a g a a b1~ b~ b4 r
}
