
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro moderato"
  \time 4/4
  \partial 4
  f4 c'2. d8 bes 
 bes a a2 bes8 g
 \grace g8 f4 f f f 
 e4. f16 g f4 c'8 a 
 g4. a16 bes a4 r4

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Cantabile"
  \time 2/4
   \partial 8
 bes8 f'4 f16 es c a
 bes8. d32 c c8. es16
 d32 [f es d] c bes a bes bes [g' f es] d c b c
 f8. g32 es d8 c bes4 r16

}


\relative c' {
  \clef treble
  \key f\major
   \tempo "3. Menuetto"
  \time 3/4
   \partial 4
 c4 f2 bes8. a16
 \grace a8 g4 f d'8. c16
 \grace c8 bes4 a f'8. e16
 d4 \times 2/3 {d8 e f} \times 2/3 {g8 a bes}
 f4 e

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Finale Presto"
  \time 2/4
   \partial 8
 c8 f [f, bes a]
 d [c bes a] 
 g [a g a] g4 r8 g'8
 c [c, f e] 
 g [e c bes]
 a [bes a bes] a4 r8

}