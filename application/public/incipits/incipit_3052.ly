\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  \partial 8
  a8
  d f16 e d8 a e' a, r g
  e' g16 f e8 a, f' d r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \partial 8
  r8
   \set Score.skipBars = ##t
   R1*8
   r2 r4 r8 a'8
   a f16 g a bes a g a8 g16 f g f e d
   g bes a e f cis d bes' cis,4 r
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 4/4
  a4~ a16 bes c d
  g,4~ g16 a bes c
  f,8 f'16 d
  bes4~ bes16 g' e c bes8 a16 bes
  a8 g16 f r4
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Presto"
  \time 3/8
  d16 a g f g a
  d,8 d'8. cis32 d
  e16 bes a g a bes
  e,8 e'8. d32 e
  f16 d cis b cis d
}