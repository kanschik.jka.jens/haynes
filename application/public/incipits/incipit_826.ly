\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Allegro [Violino Concertato]"
  \time 4/4
  e4 b r16 e, fis gis a b cis dis
  e4 b gis' b,
  \grace cis16 b8 a a4 r16 fis gis a b cis dis e
  fis4
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Allegro [Oboe d'Amore]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*29 
    e,16 gis fis e e'2 d4
    cis r r2
    r8 e dis e e,4 r
    r8 e' dis e e,4 r
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Affetuoso [Violino Concertato]"
  \time 4/4
  \partial 8
  gis16. a32
  b16 e r dis e gis gis, b b8 a gis e'16 e,
  eis fis r gis b a r dis b8 a gis
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Affetuoso [Oboe d'Amore]"
  \time 4/4
   \set Score.skipBars = ##t
   \partial 8
   r8
   R1*9
   r2 r8. e,16 e' e e e
   e dis32 e fis16 e dis dis e fis e fis gis a gis
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Allegro [Violino Concertato]"
  \time 3/8
    e8 e e
    \times 2/3 {gis16[ fis e]} \times 2/3 {dis[ cis b]} \times 2/3 {a[ gis fis]}
    \times 2/3 {gis[ fis e]} cis'8 b
    \grace b8 a8 gis r
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Allegro [Oboe d'Amore]"
  \time 3/8
   \set Score.skipBars = ##t
     R4.*29
     \times 2/3 {e,16[ fis gis]} \times 2/3 {fis[ e dis]} \times 2/3 {fis[ e dis]}
     e4 r8
     \times 2/3 {e16[ fis gis]} \times 2/3 {fis[ e dis]} \times 2/3 {fis[ e dis]}
     e4.
}
