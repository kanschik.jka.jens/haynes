 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro moderato"
  \time 2/2
    << {
      % Voice "1"
        R1
        r2 d'16 cis d cis d4
        R1
        r2 d16 cis d cis d4
         } \\ {
      % Voice "2"
        g,,4 r8. d16 g4 r8. d16
        a'4 a
        r2
        a4 r8. d,16 a'4 r8. d,16
        b'4 b r2
      } >>

  
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Larghetto"
  \time 6/8
  e8 d e f a, b
  c4 cis16 d d4 dis8
  e f g~ g16 f e d f d
  c8~ c16 b c d b4 r8
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Rondo"
  \time 2/4
   g8 g16 b d8 d16 b
   g8 g16 b d4
   c8 \grace d16 c16 b c8 e
   d4 b8 r
}
