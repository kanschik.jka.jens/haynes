\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. [Vivace]"
  \time 2/2
  \partial 8
  g'8
  g16 f e d c8[ g] c8 r d e16 f
  e[ d e d] c8 d e f g[ f16 e]
  d4
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Aria. Adagio"
  \time 2/2
  r2 e8 f16 e~ e8 a16 gis32 a
  gis4 r4 e8 a f8 e16 d
  c8 d e4 r2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. [Allegro]"
  \time 2/4
  c4 e8 f16 g
  e d e d c4
  c4 e8 f16 g
  e d e d c8 a'
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Menuett"
  \time 3/4
  c4 g g
  c' g, g
  e' g c, b d g,
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "5. "
  \time 3/4
  c2 r4
  d4. es8 f4
  es d c g'2 r4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "6. "
  \time 2/4
  es8. es16 es8 bes
  g'8. g16 g8 es
  bes'8. bes16 bes8 es,
}