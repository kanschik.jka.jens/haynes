\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4
      d8 c16 b c8 a b c16 b a g'16 d
}
