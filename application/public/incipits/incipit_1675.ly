\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
  
  bes16 bes  f f d d d d   c' c a a  f f c c
  d' d bes bes f f d d
  
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Siciliana. Vivace"
  \time 6/8
  
bes8. c16 bes8 f'4 g8
bes,4 a8 r r f
es'8. f16 es8  f,4 es'8 es16 c d4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Vivace"
  \time 3/4
      f4 es8 d c bes
      f' g2
      f4 es8 d c bes
      bes4 a2
}