\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Andante"
  \time 4/4
  g'8 d16. es32 d8 c c bes r g'
  \times 2/3 {fis16[ a g]}\times 2/3 {fis16[ e d]}
  \times 2/3 {c16[ es d]}\times 2/3 {c16[ bes a]}
  \times 2/3 {bes16[ g d']} g8 b,4
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 2/4
  d8[ d d d]
  d32 c bes16 a g d'4
  es16 fis,8 a16 a8. es'16
  \times 2/3 {d16[ c bes]}  \times 2/3 {d16[ c bes]} a8 g
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Un poco Allegro"
  \time 3/4
  d8. g16
  \grace f8 es4 d
  d8. a'16 a4 g
  d8. g16 \grace f8 es4 e
  fis8. bes16 bes4 a
}
