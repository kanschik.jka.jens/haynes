\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt + Tenor): Mein letztes Lager will mich schrecken [BWV 60/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key b\minor
                      \tempo "Duetto"
                      \time 3/4
                      % Voice 1
                    b8. d16 cis8. e16 d8. b16
                    e,2.~
                    ~e8. cis'16 d8. fis16 e8. g16
                    ais,4.
                 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key b\minor
                     \set Score.skipBars = ##t
                     R2. r16 g'16 fis e d cis b a g fis e g
                     ais,8 r8 r4 r4 r16 cis16 d e fis gis ais b cis d e cis
                     a'8 r8
               
                        
                
                  }
>>
}


\relative c'' {
	<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Alt [Die Furcht]"
                     \key b\minor
                      \tempo "Duetto"
                      \time 3/4
                      % Voice 1
                  \set Score.skipBars = ##t
                     R2.*12  g4 a b
                     cis,2.~
                     ~
                     cis8. fis16 gis8. b16 a8. gis16
                     a4 d, r4
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Tenor [Die Hoffn.]"
                     \key b\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                 R2.*15 r4 r4 ais4
                 b' cis d
                 e4. fis16 g fis8 e
                        
                
                  }
>>
}

	
	
