\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Amore andante"
  \time 3/8
    f8 f8. g16
    es16 es es8. \grace g16 f16
    d16. c64 d es8 d
    c bes r
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro assai"
  \time 2/2
  \partial 4
  f4
  f2. g4 g2. a4 a2. bes4
  bes2. f4
  g8 es bes g es4 r
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Menuett"
  \time 3/4
    \override TupletBracket #'stencil = ##f
    f4 f  \times 2/3 { es8 c a}
    bes4 bes f
    f' f \times 2/3 { es8 c a}
    bes4 bes f
}
