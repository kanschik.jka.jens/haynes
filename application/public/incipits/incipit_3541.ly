 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 3/4
  \partial 8
  e8
  a4. e8 fis d
  cis8. d16 e4 r8 e
  a4. cis,8 d b
  a8. b16 cis8 r
}


\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "2. Largo"
  \time 2/4
  \partial 8
  a8 e'4. \grace g8 f16. e32
  e4. \grace g8 f16. e32
  e16 a gis b a c c, e e8 d c
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "3. Menuetto"
  \time 3/4
  \partial 4
    e4 cis b b'
    d, cis a'
    a, fis' e
    \grace e8 d4 cis
}



\relative c'' {
  \clef "treble"   
    \key a\major
    \tempo "4. Presto"
    \time 3/8
    a4. a'~
    a8 e fis
    \grace e8 d4 cis8
    b gis a d b cis
    b16 cis d e fis d cis4 b8 a4.
}
