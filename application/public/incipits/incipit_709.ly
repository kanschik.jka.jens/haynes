\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Noel I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo ""
  \time 2/2
  \partial 2
  d4 cis8 d
  e4 e f f e2 d4 cis8 d
  \grace d8 e4. d8 c4 b a r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Noel II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Lentement et très tendre"
  \time 3/4
  \partial 8
  d8
  a'4. a8 e f \grace f8 g2 d8 e \grace e8 f4. f8 e f
  d2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Noel III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Très tendremt"
  \time 2/2
  r2 r4 g8 a b4. b8 b4 b
  b4. a8 g a b c
  d4~ d16 c b c c4. b8 b2.

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Noel IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo ""
  \time 2/2
  \partial 2
  b4 b8 a
  b4 c a2
  \time 3/2
  g2 b4 a8 g fis4 g
  \time 2/2
  a2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Noel V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo ""
  \time 2/2
  \time 2/2
   \partial 2
   d4 cis d2 e
   \grace e8 f4. e8 d e cis e
   d4.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Noel VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo ""
  \time 2/4
  \partial 16
  a16
  d8. d16 e8. f16
  cis4 \grace b16 a8. e'16
  f8. f16 g8. g16
  a4
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Noel VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Très tendremt."
  \time 3/4
  r4 r d8 e
  \grace e8 f4. f8 f g
  e4 e e8 f
  d cis d4 d8 e
  cis4.
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Noel VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. "
  \time 6/8
  \partial 4.
  g'4 g8
  fis4 g8 a4 a8 e4 fis8 g4 g8 fis4 g8 a4.
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Noel IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Très tendrement"
  \time 3/4
  a4 d e
  fis2. a4 a4. a8 fis2 e4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Noel X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo ""
  \time 2/2
  \partial 2
  b4 b
  a g d' d
  b2 d4 g, c b a2 g
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Noel XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Lentement et tendrement"
  \time 2/2
  r2 r4 a
  d e f4. g8
  e2 cis4. b16 cis
  d4 d8 cis16 d e4. f16 e a,2.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Noel XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Pesamant"
  \time 2/4
  a8. a16 a8. a16
  e'4 e f8. f16 f8. f16
  e4 e
}

