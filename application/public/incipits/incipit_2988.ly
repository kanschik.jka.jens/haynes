 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboes"
  \key a\major
   \tempo "3. o.Bez."
  \time 3/4
    r4 r a
    cis8 b cis a d a
    e' d e a, fis' a,
    e'4 fis8 e d e
    cis4 a
}
