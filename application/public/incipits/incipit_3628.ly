\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
   \partial 16
   c16
   f4 r g r
   \grace f8 e4. d8 c4 r
   f g a bes
   a2 g4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Romanze. Adagio"
  \time 2/2
    f2 bes4 r8 bes
    a2 es4 r8 es
    d d es f  g a bes d,
    f4. d8 c d es e
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo. Allegretto"
  \time 6/8
      f4. e8 r r16 c
      bes'4. a8 r r16 c
      f,4.~ f8 d g
      c,
}
