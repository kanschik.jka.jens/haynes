\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
    d4 g fis r8 d
    e d c8. d32 c b4 r8 d
    g, r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  r8 d d d g, g' g g
  fis e d c  b g' a, g'
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
  \partial 4
    d4 
    b g b d2 d4
    d8 e d e d e
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Piu Adagio"
  \time 2/2
  g4 a bes8. a16 bes8. c16
  d4. d8 d8. c16 d8. es16
  f2
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 12/8
  g4 g8 a4 a8 bes a bes c bes c
  d a fis g c bes a d c
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Aria"
  \time 4/4
  r8 d16 es d8 r c r bes r
  a d es r r c bes8. bes16
  a8
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Menuet"
  \time 3/4
  d4. es8 d4 d g, d' 
  d g, d'
  d g, d'
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "5. Adagio"
  \time 4/4
  R1 r4 d g, c8. bes16
  a2 g4 r
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "6. Giga. Presto"
  \time 12/8
  \partial 8
  d,8
  g d g a d, a' bes a g d'4.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
    f8. g16 f8. es16 d4. d8
    c8. c16 bes8. bes16 a8 f bes4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 6/8
    r8 f f f f16 f f f
    f8 f16 f f f
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio e Solo"
  \time 4/4
    r4 d8 bes r4 d8 g,
    fis4. fis8 g4 r
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Adagio e Piano"
  \time 4/4
    R1 R
    r4 f4 f8 f f f
    es es es es d8. d16 g4
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "5. Presto"
  \time 4/4
  \partial 4
    f,4
    bes d c f
    d2 es4 c d f f2 es
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  f4. c8 d8. d16 c8. bes16
  a8. d16 c8. bes16 a8. c16 f8. g16
  a4 r8 g f4 r8 e8
  d4 r
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 4/4
  r4 f8. f16 e8. e16 a8. a16
  d,8. d16 g8. g16 e8. e16 d8. d16
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Menuet"
  \time 3/4
  \partial 4
  c4 f c a' g c, c
  f c a' g c, g'
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 4/4
   f8 r e r  d r c r
   c r c r a r c r 
   d r c r a r c r
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. Giga"
  \time 3/8
  \partial 8
  f,8
  a f16 g a bes
  c8 e c
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Grave"
  \time 4/4
  g'4. g8 c,4. c8
  d4. e8 f4~ f16 e d f
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 6/8
  r8 g' g e e16 d e8
  d d8. c16 c8 e a
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Sarabanda"
  \time 3/4
  g4 g4. c8 c4 e2
  d4 b c d d2 d4 g2
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Presto"
  \time 4/4
  c4 g' f e
  d8. d16 c8. c16 g'4 r8 d
  e8. f16 e8. f16
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4
  c4. c8 g2~
  g g4. g8
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Vivace"
  \time 4/4
  r8 c16 b c8 d es f g d es d g2 fis4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Aria Come Giacona"
  \time 3/4
  r4 g2 c r4 d2 r4
  b4 b4. c8 c4 g2
  c r4 d2 r4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Sarabanda Adagio"
  \time 3/2
  es4 d c bes c bes a2 a1
  f'4 es d c d c b2 b1
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "5. Menuet"
  \time 3/4
  c8 b c4 d es2 d4
  c g' f es d8 c d4
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Vivace"
  \time 4/4
   g'4 r8 g fis g a bes
   a d, r d g f es4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Aria Solo"
  \time 4/4
  \partial 8
  d16 es
  d8 g, r d'16 es
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Menuet Alternativement"
  \time 3/4
  g8 a bes c d c
  bes c d d d d
  d4 g fis
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Presto"
  \time 6/4
  \partial 4
  d,4 g2 g4 a2 a4 bes4. c8 a4 g2 d'4
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 4/4
  d4~ d16 c bes a bes4. bes8 
  a4 r r8 cis d e
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro"
  \time 6/8
  d8 d16 cis d8 e a, e'
  f f16 e f g f8 d f
  a4. gis
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio e SOlo"
  \time 4/4
  a4. a8 bes2 g4. g8 f2
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro"
  \time 3/8
  \partial 8
  a'8
  f a16 g a8 e a e
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "5. Menuet"
  \time 3/4
  a4 a8 a a a
  a2 a4 a4 a8 a a a
  a2.
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "6. Presto"
  \time 2/2
   d4 cis d2 r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Adagio"
  \time 4/4
  d1~ d4. c8 b4. a16 g
  a2
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Allegro"
  \time 12/8
  d4 d8 d4 d8 cis d cis cis d cis
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Marche"
  \time 4/4
  a'4 a a8 a a g
  fis fis fis e d fis fis e
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "4. Presto"
  \time 2/2
  \partial 2.
  a8 a4 a
  d a b a8 g
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  b4 r8 b16 c d8 d d d
  b16 a g8 r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 4/4
  r4 f2 f8 e
  e4. g,8 c4. c8
  bes4 a a2 g1
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Randeau"
  \time 3/4
  \partial 2
  b4 g
  d' d d
  g fis8 e d c
  b4 g a b
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Solo e Poco Adagio"
  \time 4/4
  r4 b8 c d e16 d c8. c16
  b8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Presto"
  \time 2/2
  \partial 4
  b4 d2 e
  d2. c4 b2 c
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  c4 b8. a32 b c4 r8 g'
  e c g'4~ g8 f f8. g16
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 12/8
  r8 g' g g a g  g a g   g a g 
  c, g' g  a g a f e f g4 r8
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Adagio"
  \time 3/2
  r2 c e
  a,1.~ a2 gis a e
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Poco Allegro"
  \time 4/4
  c4 r8 c d g, r g'
  e c e f g4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Vivace"
  \time 3/4
  r4 g'8 a g4
  r f8 g f4
  r e8 f e4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Grave"
  \time 4/4
  d4 f bes, bes bes2 r
  r4 bes c d
  es8 g f8. es16 d8 f g f
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Vivace"
  \time 3/8
  bes8 bes16 bes bes8 bes d bes
  bes d f
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Passepied"
  \time 3/8
  \partial 8
  f8
  bes f g d4 es8
  f bes, g'
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Menuet"
  \time 3/4
    bes'4 f g
    d2.~ d
}
