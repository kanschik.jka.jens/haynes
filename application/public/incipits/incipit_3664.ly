 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 3/4
    a2.~ a~ a4 g fis
    e d r
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Allegro"
  \time 2/2
  \partial 8

}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Andante sostenuto"
  \time 6/8
  \partial 8

}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4. Menuet"
  \time 3/4
  \partial 4
  
}