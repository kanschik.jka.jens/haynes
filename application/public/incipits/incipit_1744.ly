\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. "
  \time 2/2
  c4. bes16 a d8 c r f
  d bes g c
  a f16 g a8 b c e16 f g8 g,
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/2
   << {
      % Voice "1"
      c'8 a16 g a8[ e] f c r4
      r2 c'8 a16 g a8[ e] f[ c]
         } \\ {
      % Voice "2"
      r2 r8 c16 bes c8 f16 e
      d c bes a g8[ c] a f r4
      r8 c'16 bes c8[ a]
      } >>


}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Largo"
  \time 3/2
  a'4 g a g f g
  e2 e4 f g f
  e2 e4 f g e
  f2 d bes'
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 3/8
  c8 f16 g e g
  f e f c a c
  f8 a16 bes g bes
  a g a f c f
  a16. bes32 c8 bes
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "5. Menuet"
  \time 3/8
  as'8 g f16 e
  f8 c f16 g
  as8 g c as4 g8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "6. Allegro"
  \time 3/8
  f8 a16 bes g bes
  a8 g16 a f8 f4 e8 f4. a8 g e a g e
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Largo"
  \time 2/2
   << {
      % Voice "1"
      R1 r2 e8 c16 a  f'8 f
      f e r e  e d16 c b8 a
      gis[ e]
           } \\ {
      % Voice "2"
      a'8 f16 d
      bes'8[ bes] bes a r a
      a g16 f es8[ d] cis a r
      } >> 
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro"
  \time 2/2
  d8 a a a  a4 r8 e'
  f e d e cis a r b
  bes16 a bes8 r c a4 r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Adagio"
  \time 2/2
  c2 r a'
  g r f
  e c f f1 e2 f c cis d1
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro"
  \time 3/8
  d'8 cis4
  d8 a bes
  a d, f e16. f32 g8 e
  f d bes'
  a g16 f e d cis4.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Largo"
  \time 3/4
  g4 c b c g es'
  d4. es8 c4 b2
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro"
  \time 2/2
  \partial 8
   g'8
   es c as'[ as] as g r g
   g f16 es f8 f f es r d16 c
   b8 c c b c
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 3/2
  d'2 bes g
  g1.~
  g2 fis a bes1.~ bes2 a c
  d1.
}
  
  \relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Allegro"
  \time 1/2
  \partial 2
  es4 c
  g' g g f8 g
  es4 c es c
  f f f es8 f
  d4 bes d bes
}
  
  \relative c'' {
  \clef treble
  \key es\major
   \tempo "4. "
  \time 6/8
  \partial 8

}

