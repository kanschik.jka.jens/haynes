
\version "2.16.1"
 
#(set-global-staff-size 14)


 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
 f4. es32 d c bes c8. d32 es \grace es8 d4
 bes g'8 \times 2/3 {f16 bes g} \grace f8 es4 d8 r
  
 
 
 
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/2
  bes2 f'4. es8 
 \grace es4 d2. c4
 bes2 g'4 f
 \grace f4 es2 d4 r4
  
 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Minuetto con Variatione"
  \time 3/4
  bes4 f' f8. g16 
 f4 c f 
 r4 f4 f
 bes8 g d es es4
}

