 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Andante"
  \time 6/8
  d8. f32 e d8 d cis d
  e a, gis a gis a
  e'8. g32 f e8 e d e
  f d, e f e d
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Allegro"
  \time 2/2
  d1 cis4 d cis d
  d1 cis4 d cis d
  d2 \grace c8 bes2
  a4 d g,2 f
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Allegro"
  \time 3/4
  \times 2/3 {d8 f e} d4 cis
  \grace cis8 d2.
  cis8 d e bes a g
  f a d f e d
}

