\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/2
  \partial 8
  d8
  g g g g   g fis16 e d8 c
  b8 c d e
  a,4 r8 d, 
  d'16[ c b c]
}
