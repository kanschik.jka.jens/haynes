
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sextuor I "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
  \tempo "1. Andante"
  \time 2/4
  c8 c16. c32 c8 c
 b4~ b16 d32 c \grace e8 d16 [c32 b]
 c8 c16. c32 c8 c 
 a16 c f4 
 
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 3/4
  R2.*2 c4 c4. b16 c
 d8 c b c d c 
 e4 e4. d16 e 
 d8 e32 f e4 r4
 
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sextuor II "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key es\major
   \tempo "1. Larghetto"
  \time 3/4
 R2.*2 r8 g8 [g g] \grace g8 f8 es16 d
 es8 [bes bes bes] \grace bes8 as8 g16 f
 g4 es' \grace as8 g8 f16 es es8 d4 r8 
 
 
 
}
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 4/4
  es2 d4 es
 c2 g'8 es f g 
 g f d f f es bes es 
 c bes16 as g2 \grace bes8 as8 g16 f g4 r4
 
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Minuetto grazioso"
  \time 3/4
  \partial 4
 g8. g16 g4 es es'
 es d d8. d16 d4 bes f'
 f es es8. d16 es8 c bes4 r4
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sextuor III "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Un poco Andante"
  \time 2/4
 a8. bes32 a a8 a
 a16 c c4 r16 c16
 c f f4 r16 a16 a8. f16 a g f e f8 r8
 
 
 
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 6/8
  \partial 8
 c8 c4 f8 \grace {g32 [f e]} f4 a8
 a4 c8 bes a g f4 r8
 
}