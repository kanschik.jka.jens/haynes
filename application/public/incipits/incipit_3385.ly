   \version "2.16.1"
         #(set-global-staff-size 14)

\relative c'' {
  \clef treble   
  \key bes\major
   \time 4/4
   \tempo "Adagio"
  f4 g a, bes c d8 es d c d4 
}