 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 6/8
  r8 c c c c c f c c c c c
  g' c, c   c c c
  g' bes, bes a4.
  g4 r8
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Adagio"
  \time 2/2
  a'4. a16 a, d cis d8 r16 d cis d
  e16. d32 e16. bes32 a16. g32 f8 
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 2/2
  f2 c
  f r4 cis
  f8 e f4 g8 f g4
  a g f g a8 g a4 bes8 a bes4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}