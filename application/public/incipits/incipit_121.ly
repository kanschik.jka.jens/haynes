\version "2.16.1"
         #(set-global-staff-size 14)

\relative c'' {
  \clef treble  
  \key f \major
    \time 4/4
    \tempo " overture "
    c4. c8 d8.d16 d8. d16 
    d8 (c) r c bes8. bes16 bes8. bes 16
    bes8 a4 a16 a a8.g16 g8. g16 
    g4 r8 c 
    
}