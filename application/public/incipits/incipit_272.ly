\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Suite" 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Ouverture "
  \time 2/2
 g4 g, d'4. d8 d4. d8 fis,4 fis8 g
 a4. b8 c a d c
 b4 \grace a8 g4
 

  
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. 1er Air "
  \time 3/4
  \partial 4
  d4 g fis4. g8
  a4 d, e
  c \grace b8 a4
  
 
 

  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. 1er Air. Gayment "
  \time 2/2
  \partial 2
  d4 g
  g d d c
  b g b8 c d e
  d4 e8 d c4 b
  a g
 
 

  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "6. 1er Menuet "
  \time 3/4
  g8 a b4 a
  g d'2
  e4 d c
  b a8 b g4
 
  
  

  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "8. 1er Gavotte. Tres tendrem.t "
  \time 2/2
  \partial 2
  b4 a8 b
  \grace a8 g4 a b4. c8
  \grace c8 d4 a 
  
 
 

  
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "10. 1er Gigue. "
  \time 6/8
  \partial 8
  g8 b8. a16 g8 fis8. e16 d8
  g4. d4 d8
  e8. f16 e8
  d8. e16 c8
  d4. b4
  


  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "12. Sarabande "
  \time 3/4
  b4 a4. b8
  \grace a8 g4. a8 b4
  c b4. c8
  a2
  
 
  
  

  
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "13. 1er Passepied"
  \time 3/8
  \partial 8
  g8 b16 a g a b c
  d4 e8
  d c b
  a g g
 
  
  
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "15. 1er Tembourin. Vivement "
  \time 2/4
  g4 b16 c b a
  g8[ d' g d]
  e f16 e d e c d
  b4 a
  
 

 
   
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "16. [1er] Tembourin "
  \time 2/4
  g4 g
  b16 c b a g8 b
  a[ g fis g]
  a16 g fis e d e fis d
  
  
 
  
   
}


\relative c''' {
  \clef treble
  \key g\major
   \tempo "18. Chaconne "
  \time 3/4
  r4 g4. g8
  g4 fis8 g a4
  d, e8 d c b
  a b a b c d
  b[ g]
 
 
  
 
 
  
   
}

