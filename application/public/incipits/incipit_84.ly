\version "2.16.1"
         #(set-global-staff-size 14)

\relative c'' {
  \clef treble  
  \key d\major
    \time 4/4
    \partial 4
    
    a4 d a8. a16 a4 a
    a2 fis
    a4 fis8. fis16 fis4 fis
    fis2 d4  
}