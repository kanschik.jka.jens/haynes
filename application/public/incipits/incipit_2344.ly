\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/4
  \set Score.skipBars = ##t
   R2*17 
      bes4 bes bes r16 f g a
      bes8 a bes c
      d c16 d bes8 f'
      g g4 a16 bes
      f8 f4 d8
      es16 d c bes a g f f'
}
