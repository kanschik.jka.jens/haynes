\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 6/8
  \partial 16
    d16
    b'4. fis g4 r8 r4 r16 g,
    g'4. a4 c8
    fis4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 2/4
    a'4 c bes r16 \grace c8 bes16 a g
    f8. fis16 g a bes g
    f8. g16 e8 r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
    r1
    r8 bes \grace d8 c8 bes a bes es g
    bes1~ bes8 as g f es d c bes
}

  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 3/8
    a'16. b64 a  gis8 a
    b4 cis8
    \grace e8 d4 cis8
    \grace cis8 b8 a r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 3/4
  \partial 4
      f,8. f16
      bes4 r \grace { bes16 c} d8. bes16
      es8. c16 a8 r f8. f16
      c'4 r \grace { c16 d} es8. c16
      c8. d16 bes8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 2/4
    e8 f fis g
    c,4 r8 cis
    d d16 f b,8 b16 d
    c8 e c
}
