\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
    f4 r8 f e4 r8 e
    f a d, g e16 d c8 r4
    f r8 c d4 r8 f 
    g4
    
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. o.Bez."
  \time 6/8
c8 f f  e f16 e d c
d8 g g  c, g' r
d16 c es d c bes c bes
    
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. o.Bez."
  \time 2/2
  r4 d a d f e8 d
  e4 a, r cis e d8 cis
  d4 f g d8 e
    
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. o.Bez."
  \time 4/4
        a16 f' bes, f'  c f d f  c f d f  c f bes, f'
        a, f' bes, f' c f d f
}
