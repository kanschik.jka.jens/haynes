 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 3/4
   g2 as8. f16
   es2 c'8. bes16
   bes4 \grace as8 g4 \grace f8 es4
   f4. g16 as g8 r
}

