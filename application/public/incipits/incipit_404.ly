 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo"
  \time 4/4
    d16 c bes a g8 d' es d r16 g es d
    c bes' a g fis es d c c8 bes r16
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Comodo"
  \time 4/4
  g'8. fis32 g d8 g es d~ 
  \times 2/3 { d16[ e fis]} \times 2/3 { g16[ fis g]}
  es8 d
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Allegro"
  \time 3/8
  d8 g8. a32 bes
  d,8 d'8. bes32 c
  bes8 a g \grace g8 fis4 g8
}

