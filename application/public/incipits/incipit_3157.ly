\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran + Alt): Du wahrer Gott und Davids Sohn [BWV 23/1]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key c\minor
                      \tempo "Duetto. Adagio molto"
                      \time 4/4
                      % Voice 1
                    r8 \times 2/3 {g16 f g}
                    \times 2/3 {as16[ g f]}
                    g16. g32
                    c,8 c' ~
                    ~
                    \times 2/3 {c16[ d es]}
                    \times 2/3 {d es f}
                    \times 2/3 {es16[ c d]}
                    \times 2/3 {es f es}
                    \times 2/3 {d[ b c]}
                    b16. c32 c8
                    
                 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key c\minor
                     \set Score.skipBars = ##t
                    r2 r8 \times 2/3 {g16 f g}
                    \times 2/3 {as16[ g f]}
                    g16. g32
                    c,8 c' ~
                    ~
                    \times 2/3 {c16[ d es]}
                    \times 2/3 {d es f}
               
                        
                
                  }
>>
}


\relative c'' {
	<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Sopran"
                     \key c\minor
                      \tempo "Duetto. Adagio molto"
                      \time 4/4
                      % Voice 1
                  \set Score.skipBars = ##t
                     R1*10  r8 g8 d'16 es32 d c16. d32 es4 r8 es8
                     es d~ d16 c b c b4 r4
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Alt"
                     \key c\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                 R1*10 r2 r8 c8  g'16 as32 g f16. g32
                 as4 r8 as8 as g~g16
                        
                
                  }
>>
}

	
	
