\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Ouverture]"
  \time 2/2
  d4. c16 bes a8 bes c a
  bes4 g d' g
  fis fis8 g16 a  c,8 d es c
  bes8. g16 bes8. d16
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Rigaudon"
  \time 2/2
    \partial 4 d4
    c4. d16 es c4. bes16 c
    d8 c d es d4 d

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Trio"
  \time 2/2
    \partial 4
    b8 c
    d g fis[ g16 a]
    c,8 b a g
    a4 d,
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Sarabande"
  \time 3/4
  d4 es d
  fis4 g4. a8
  c,4 d a8 bes16 c
  bes4. a8 g4
  
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "5. Aria angloise"
  \time 2/4
    \partial 8
    d16 c
    bes8 a16 bes g bes a g
    fis8 e16 fis
    
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "6. Aria. Adagio"
  \time 3/4
  bes4 d f8 bes,
  a2
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "7. Menuet"
  \time 3/4
  g8 a bes4 a bes8 c d4 c
  d g a
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "8. Aria. Adagio"
  \time 2/2
  d8[ a] bes a16 g fis[ g g fis32 g] a16 g32 a bes8
  a[ d,]
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "9. Gigue"
  \time 6/8
    \partial 8
    d16 c
    bes8 a g  bes a g
    d'8. es16 d8  d4
}