\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1."
  \time 2/4
  f8 c16 c c8 bes
  a f c' f
  g c, g' bes
  a g16 a f4
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. "
  \time 2/2

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. "
  \time 2/2

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. "
  \time 2/2

}
