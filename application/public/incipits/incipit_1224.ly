 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio [tutti]"
  \time 4/4
  f4. g16 f f8 g16 f f8 g16 f
  f8 bes, bes'4~ bes8 as16 g f8 es 
  d4r8
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio [solo]"
  \time 4/4
  f4. g16 f d8 es16 d d8 es16 d
  d8 bes f'4~ f8 g16 f es d c bes
  a
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro [tutti]"
  \time 2/2
  bes8 a bes f d' c d c
  f es16 d c8 bes a g16 a f8
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro [solo]"
  \time 2/2
  bes8 d16 c d8 bes f'4 r8 f
  d c d es16 d c4 r8
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Largo"
  \time 6/8
  f8. g16 f8 es8. f16 es8
  d4.~ d4 d8
  c8. d16 c8 bes8. c16 bes8
  a4.
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Vivace"
  \time 3/4
  f,4 bes c
  d2 es4 f g2
  a, bes
  f g f
}



