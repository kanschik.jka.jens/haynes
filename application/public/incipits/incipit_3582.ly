\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 2/2
  \partial 4
  c16 b c d
  c4 r r e16 d e f
  e4 r r g16 f g a
  g4 a, f'8 d c b
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*23
   r2 r4 \grace d16 c8. d16
   c4. e16 d c4 e8. f16
   e4. g16 f e4 g8. a16
   g4 a, f'8 d c b
   \grace d16 c8 b16 c c4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Largo [Tutti]"
  \time 3/4
  c4 c4. f8
  d4 d4. bes'8
  c,4 c4. a'8
  bes,4 bes4. g'8
  a,8. c16 f4 a,
  a g r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Solo]"
  \time 3/4
  \set Score.skipBars = ##t
  R2.*15
  \grace {e16[ f g]} f4~ f16 f, a c f f g a
  g16. fis32 g8~ g16 c,, e g c e g bes
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Tempo di Menuetto [Tutti]"
  \time 3/8
  c,8 e g c g'4
  a,8 a'16  f e d
  c8 b c
  a c'4
  g,8 c'4
  g16 e d c a' f
  \grace e8 d4.
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Tempo di Menuetto [Solo]"
  \time 3/8
  \set Score.skipBars = ##t
  R4.*43
  \grace a'16 g8 f16 e d c
  b c c'4
  a,8 a'16 f e d
  \grace c8 b4 c8
}