 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Vivace"
  \time 6/8
       bes8 bes bes bes f 8
       d' d d   d bes
       f'16 d  g f es d  es c f es d c
}
 \relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Largo e giusto"
  \time 4/4
    g,8[ r16 bes']  a[ r32 bes g16 r32 a]
        g,8[ r16 bes']  a[ r32 bes g16 r32 a]
}

 \relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 2/4
      f8 g 
        \override TupletBracket #'stencil = ##f
        \times 2/3 { g16 a g} f8

}
 