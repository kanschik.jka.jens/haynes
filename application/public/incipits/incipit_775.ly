\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro "
  \time 3/4
    c'4 g g2
    \grace f8 g8 f f2
    f8 e dis e g e
    \grace e16 d4 c r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Romance. Andante"
  \time 2/4 
    f8. g16 a8 cis,
    cis4 d8 c
    bes d g fis g bes d, g
    f4 es8 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondeau. Poco presto"
  \time 2/4
   \partial 4
   g8 a
   g4 c8. b16
   \grace b16 a8 g g a
   g c \grace c16 b8 a16 b
   c4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Moderato"
  \time 2/2
   \partial 16
    f16 f2 es8 d g f
    f4. g16 a bes8 a g f
    \grace f8 es4 d2 c4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Romance. Anadante"
  \time 2/2
    c4. d8 c4 c
    c8 bes bes4. bes8[ \grace d8 c8 bes]
    bes8 a a4.
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Rondo"
  \time 3/8
    f8 f f  f g es
    d d d   d es c
    bes bes bes bes4 b8
    c c c es4 d8
}

\pageBreak

\markuplist {
    \fontsize #3
    "Quartetto III " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Maestoso"
  \time 4/4
    \partial 8
   d8
   g2 g4 g g2 g,8 g a b
   \grace d8 c4 c4. d16 e d8 c
   c b b4 r8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Romance. Andante"
  \time 3/4
      e,4~ e16 e f e \grace g16 f8 e16 d
      d c b c c4. cis8
      d16 e d e  f g f g a a, d a
      c4 b 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Rondo"
  \time 2/4	
      b16 c d8 d d
      d d d d
      \grace fis8 e8 d d d
      d d d d
      g g fis fis e e d d 
      d c b c
      d c b r
}

\pageBreak

\markuplist {
    \fontsize #3
    "Quartetto IV " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
  bes8 c
  d c c4. cis8 d bes
  bes a a4. a8 bes g
  g f f4. f8 g a
  \grace bes8 a4 g r8
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Romance. Andante"
  \time 2/4
  \partial 8
  d16 es
  g f f8 f16 es es8
  es16 d d8 r16 bes c d
  d c b c c es g, c
  bes4 a8
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo"
  \time 3/4
     <a, f'>2. <c g'> 
     a'8 a16 bes c8 f, bes a \grace a8 g4 f8 e d c
     
}

\pageBreak

\markuplist {
    \fontsize #3
    "Quartetto V " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Moderato"
  \time 2/2
  \partial 4
  fis8 g
  b a a2
  cis,4
  e8 d d4. d8 cis d
  cis b b4. d16 cis \grace e16 d8 cis16 b
  b8 a a4
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio poco andante"
  \time 3/4
    \grace { f16 g} a4 g16 f e d c8 c
    \grace f16 e16 d d8~ d8 \grace f16 e16 d d8 \grace f16 e16 d
    d32 cis e d  f e g f  a8 a gis16 a bes a a,4.
}
 

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Rondo. Allegro"
  \time 2/2
     d16[ e fis e] d8 d  d d d dis
     dis e e2 eis4
     fis16[ g a g] fis8 fis fis fis fis fis
     fis g g2
     
}

\pageBreak

\markuplist {
    \fontsize #3
    "Quartetto VI " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegretto"
  \time 3/4
      bes4 es2. f2 g4
      \grace bes4 as2 g4
      \grace g8 f2 bes,4
      es2 des4 des8 c c2
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Romance. Andante"
  \time 2/4
    \partial 8
    d16 es
    \grace g8 f8 es16 d
     \grace d8 c8 bes16 a
     c bes bes8~ bes16 d f bes
     \grace a8 g8 f16 es d c g' es
     d4 c8
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Rondo. poco presto"
  \time 3/8
     \partial 8
       bes8
       es4 g8 f as d,
       es g bes
       f4 bes,8 es4 g8 f as d, es4 f16 g es4
}

