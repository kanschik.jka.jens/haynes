\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
    r4 c d e
    d g, c d
    e8 f16 e d8. c16 c4 g
    s r r2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 6/8
  e8 d c d16 c d e d8
  e d c d g,4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Largo"
  \time 4/4
    c4 r8 b a4 d8. c16
    b4 g a8. b16
    e2 d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Presto"
  \time 4/4
      c8. e16 d8. f16 e8. f16 g8. g,16
      c8. e16 d8. c16 d8 g,4.	
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Grave"
  \time 4/4
    d4 d e8 g, g' c,
    f4. g16 f e8 fis g4~ 
    g8 fis16 e fis8. g16 g2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "6. Allegro"
  \time 12/8
      e8 d e  f e d  e d e f e d
      e g e  e d c c4. r4 r8
}
