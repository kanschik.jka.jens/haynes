 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key g\minor
   \tempo ""
  \time 12/8
  r8 d16 c bes a g8 g' d es4. r4 r8
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Dafne"
  \key g\minor
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
     R1*6
     d4. g8 es4. es8
     es4. es8 es4. d8
}