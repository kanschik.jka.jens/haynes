\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Ouverture"
  \time 2/2
  g'4 g, es'4. es8
  d4. d8 g4. d8
  d4. a8 fis fis g a
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Bourree"
  \time 2/2
  \partial 4
  bes'8 a
  g4 a fis4. e16 fis
  g4 d es c
  d
  }

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Menuetto I"
  \time 3/4
  d4. c8 d4 bes es2
  d4 g a fis g
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Menuetto II"
  \time 3/4
  g4 d'2
  bes c4 d bes2
  a a4 g d'2
  }

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "5. Ciaccona"
  \time 3/4
  r4 bes8 c d e
  f4 d4. d8
  g4 g4. a8 fis4
  }
