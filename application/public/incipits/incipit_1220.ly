\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 2/2
  c4. d8 e4 f
  g c, g'4. g8
  a4. g8 f e16 f r8 g
  e4. d8 e4 c
}
