\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 3/4
  f2. \grace f16 g2.
  f8. bes16 \grace f16 es2
  d16 es f g f4. es8 d16
}



\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 4/4

}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Vivace"
  \time 3/4

}
