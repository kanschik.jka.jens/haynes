\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  r8 c e d16 c c8 d f e16 d
  e8 \grace d8 c8 r
  e16 f g8 d16 f e8 d16 c
  b8 \grace a8 g8 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Musetta 1"
  \time 2/2
  c4. e8 d f e d
  c4. e8 d f e d c d e f g4 c
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Musetta 2"
  \time 2/2
  g'4. f8 g a b c
  g4. f8 g  a b c
  g4 f es d es4.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 6/8
   \partial 8
   c8 g'4 c,8 a'4 c,8
   g'4 c,8 a' b c g4.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gratioso"
  \time 3/4
  c8 d e f g4 g,2 c4
  d8 c d e f g e4. d8 c4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. 1r Aria"
  \time 6/8
   c8 g' as g f es
   f d g f es d es[ c]
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. 2e Aria"
  \time 6/8
  c8 e g c, f a
  c, e g c, f a
  
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Presto"
  \time 2/4
   \partial 8

  g'8
  c c,16 d e f g a
  g8 c, a' g
  f e g f
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Andante"
  \time 4/4
  g8. as16 g c b c g8. as16 g d' c d
  g,8. as16 g es' d es f8 es16 d g8. f16
  es8. d16
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. 1. Minuetto"
  \time 3/8
  c16 d e8 d c f4 e16 f g8 f e a4
}
  
  \relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. 2e Minuetto"
  \time 3/8
  es16 d c8 b c4 g8 g'16 f es8 d es4 d8
}
  
  \relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 2/4
  \partial 8
  e16 f
  g8 c,4 e8
  d g,4 f'8 e g,4 e'8 d16 c d e c8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Moderato"
  \time 2/2
   \partial 2
   d4 g,
   g'4. a8 a4. g16 a
   b4 \grace a8 g4
   \times 2/3 { g8[ a b]} a8. c16
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. 1er Aria"
  \time 6/8
 \partial 4.
  d8 g fis g a b a b c
  b4 a8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Aria"
  \time 6/8
   \partial 4.
   d8 \grace c8 bes8 c
   d g, a bes c d
   bes8. c16 g8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro jn ma non troppo"
  \time 3/4
  r4 d8 g fis g
  a fis d a' g a b4 \grace a8 g b d b
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  g'8 fis16 e d8 g a fis16 e d8 a'
  b a16 b c8 b a d,~ d g16 fis
  g8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. 1. Gavotta"
  \time 2/2
   \partial 2
   d8 g fis g
   a c d, g a b c d b4 a
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. 2.e Gavotta"
  \time 2/2
   \partial 2
   d8 g a bes
   fis a g bes a bes c d
   bes4 a
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro E Spiccato"
  \time 6/8
  g'4. a
  b8 a g fis e d
  e4. fis
  g8 fis e d c b
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Moderato"
  \time 2/4
  d8 g4 fis16 g
  a4. d,8
  c'8 c4 b16 a
  b8 \grace a8 g8 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. 1. Aria"
  \time 3/8
   \partial 8
   d8
   g8. fis16 g8
   d16 e fis g a b
   c4 b8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. 2e Aria"
  \time 3/8
   \partial 8
   g'8
   g,8. a16 bes8
   c bes c d es f
   d8 \grace c8 bes8 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga. Allegro"
  \time 6/8
  g'8 d c b a g
  d'4.~ d8 e fis
  g fis g a g a b4 a8
}

