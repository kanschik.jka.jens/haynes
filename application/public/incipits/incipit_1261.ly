 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'M'adora l'idol'" 
    }
    \vspace #1.5
} 

 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key f\major
   \tempo "Allegro"
  \time 4/4
    r4 r8 c d d d c16 bes
    c8 f, f'4~ f8 g16 a e8. f16
    f4 r8
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Agilea"
  \key f\major
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
     R1*14
     r4 r8 c d d d c16 bes
     c8 f, f'4 g8 a e4
     f4 r8
}
