\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Ouverture"
  \time 4/4
  c4~ c16 c d e f4~ f16 e f g
  a8. g16 f8. e16 f8. g16 g8. f16 
  e4 c
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Rondeau"
  \time 2/2
  \partial 4
  f4
  c d d c
  c a8 bes c4 f
  c d d c 
  c a8 bes c4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Menuet"
  \time 3/4
  a4 g2
  f4 f8 g a bes
  c4 d2
  c4 a8 bes8 a
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Gigue"
  \time 6/8
  \partial 8
        << {
      % Voice "1"
      r8
      r r c f8. g16 f8
         } \\ {
      % Voice "2"
      c,8
      f8. g16 f8 f4 r8
      g8. a16 g8 g4
      } >>
}

