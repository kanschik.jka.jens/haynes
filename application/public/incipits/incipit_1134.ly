 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. [Allegro]"
  \time 4/4
      c8. g16 g'8 f f16 es d c c b8 g16
      c8. d32 es d8. es32 f es8 d r4
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Affettuoso"
  \time 3/4
    es,8. es'16 es4. d16 c
    bes8. as16 g4 r
    f8 g as bes b c
    es, d es4 r
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Allegro molto"
  \time 2/4
    c4 d es16 d c8 r g
    c es d f es16 d c8 r g
    c es d f es g b, d
    c es g, f es d r
}
