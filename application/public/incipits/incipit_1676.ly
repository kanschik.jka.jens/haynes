\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 2
  a4 e8. d16
  cis b a8 r16 cis16 d e fis e d cis d cis b a
  gis fis e8

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Minuetto I"
  \time 3/4
  a8 cis e a gis fis
  e d cis b a4
  e8 a gis b a cis
  b d cis b cis4

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Giga I"
  \time 6/8
  \partial 8
  e8 e4 a8 gis4 fis8
  e4 e8 e4 d8
  cis4 fis8 e4 d8
  cis4 b8 a4 cis8
  b cis a e'4 cis8


}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 16
  a16 a8. e16 cis' a e' cis 
  a'4~ a8 gis16 fis
  e d cis fis d b gis e'
  cis b a8

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Rondeau I"
  \time 2/2
  \partial 2
  cis4 b
  a cis8 d e4 fis
  \grace fis8 e2 a8 e gis fis
  fis e e d d cis b a
  a gis fis e cis' d b cis
  

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Tembourino I"
  \time 2/4
  \partial 8
  a8 a[ cis cis e]
   a e cis16 a cis e
   d8[ cis b cis]
   a16 gis a b a8 e


}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 16
  a16 a8. b16 b8. a32 b cis16 e a e cis a e a
  cis8. d16 d8. cis32 d e16 fis e d cis e a8~ 
  a16 gis fis8~ 

}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "2. Ciacconna"
  \time 3/4
  r4 a4 gis
  a gis4. fis8
  e4 fis e
  d cis4. b8 cis4

}
  
  \relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Tembourino I"
  \time 2/4
  \partial 8
  a8 gis[ a e cis']
  b[ d cis fis]
  e[ d cis b]
  a16 gis a b a8 a
  

}
  


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 16
  a16 a8. e32 a cis8. a32 cis
  e4~ e16 fis e fis
  e a gis a e fis e d cis b a8 

}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "2. Minuetto I"
  \time 3/4
  a8 e cis a a'4
  gis8 fis \grace fis8 e2
  fis4 d fis
  e8 d cis b a4
  cis8[ e cis a] e dis'
  \grace dis8 e2.

}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "3. Giga I"
  \time 6/8
  \partial 8
  a8 a4 e8 e4 cis8
  cis4 a8 a e a
  cis a cis e d cis
  b a b b4

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
  g8 d'[ d d e]
  d[ d d g]
  fis16 e d c b a g a 
  b8 g
  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Minuetto I"
  \time 3/4
  g2 a4
  g8 b d g d b
  g2 a4
  g8 b d g d fis
  g d b g b d
  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Muzetta I"
  \time 2/2
  \partial 2
  g8 a a4 \grace {g16[ a]}
  b4 \grace a8 g4
  b8 c d e 
  \grace e8 d2 g8[ fis16 e] e8 d16 c
  c8[ b16 a] b8 c 
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Tembourino I"
  \time 2/4
  g4 a
  b8 g g' g
  g fis16 e d c b a 
  b8 g g' g
  g fis16 e d c b a 
  b8 g

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Presto ma non tropo"
  \time 4/4
  \partial 2
  g4 g8. g16
  g4. fis16 e d c b a g a a8
  b16 d g d g,4 b16 g b d a fis a c

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Tembourino I"
  \time 2/4
  \partial 4
  d16 b g8
  e'16 c a8 d16 b g8
  g'4 fis16 e d c
  b d e d c b a g 
  a8 d,
  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Minuetto I"
  \time 3/4
  g8 b d g d b
  g4. d8 g b
  c g' fis e d c
  b2 a4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Tembourino I"
  \time 2/4
  b16 c d8 d g
  b,16 c d8 d g
  fis[ e d c]
  b a g4
  d16 g b d g d b g

}

