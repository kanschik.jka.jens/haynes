 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Grave"
  \time 4/4
    c'4. bes16 a g8 c, f8. e32 f
    g4 r16 g a g f e d e f8. f16
    f8 e4 d16 c b' g c e, d8. c16
    c4
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. All."
  \time 4/4
  \partial 8
  c8 
  f a g c a16 g f8 r a,
  f' a g c a16 g f8 r
  a g16 a f8. e16 e8
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Ad."
  \time 3/2
    a'2 d, bes'~
    bes a4 g f e
    f2. e4 d2

}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Giga. All."
  \time 12/8
    f8. g16 f8 a4 bes8 c4. f,
    c'8 bes a g a f e4. g~
    g8 f e d e c
    b'4. c4 c,8
    f g e d4 c8 c4
}