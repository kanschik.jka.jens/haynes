 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

 

 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
  \grace { c16[ d] } 
  e8. d16 c8 b16 a 
  gis4. a8
  b c d c16 b
  c4
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 3/4
  r4 r f~
  f2 e4
  f2 r4
  r8 g e c a d
  b2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/8
  e16 d c b a8
  f' e8. f16
  d4 c8~
  c b a gis a b
}

