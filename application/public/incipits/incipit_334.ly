\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 3/8
  g16 fis g a g a
  b a b c b c
  d c d e d e
  fis e fis g a fis
  g
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 4/4
g'8 g, r16 g' b g d'8 d, r16 d, fis d
g8 c16 b a8 a'16 c, c b b8 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Fantasia. Allegro"
  \time 2/4
  g8 d'4 g,8 g e'4 g,8 g d'4 g,8
  fis16 g fis e d4
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Menuet"
  \time 3/4
d4 c b8 e
d c b a g4
fis' g a b a8 b g4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1.Allegro"
  \time 4/4
  a'8 r gis r a4 r
  c,8 r b r c4 r8 e
  f16 a, f' a, f' a, f' a,    e' a, e' a, e' a, e' a,
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  bes4. bes'8 bes,4. bes'8
  bes, bes'4 a16 g f8 es d c
  bes4.
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Gravement"
  \time 4/4
  g'8 b, c16 g' f g as8 g~ g16 g c es,
  f es d es f as g f es8 c
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
d,16 fis e d   a' cis b a  d e fis g a b cis a
d d cis b a g fis e fis g fis e d a g fis
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Largo"
  \time 3/2
  b2 e, c'
  a fis'4 a, b a g2 e'1~ e2 dis
}
