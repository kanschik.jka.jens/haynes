\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
    c8 e g b, c4 r16 c e f
    g8 g g16 f e d c b c8 r16 c f g
    a8 a a16 g f e d c d8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
      \override TupletBracket #'stencil = ##f
      R1*11
      r2 c8 e g b,
      c8 e16 f g8 b, c e16 f g f e d
      c4 r16
   
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Largo [Solo]"
  \time 4/4
    e4~ e16. f32 e16. f32 e4~ e16. f32 e16. f32
    e16 d c b a4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Tutti]"
  \time 3/8
  \partial 8
  c8
  c8. d16 e f
  g8 c, r a' g r f e r
  a16 g f e d c
  b8. a16 g8
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Solo]"
  \time 3/8
  \set Score.skipBars = ##t
   \partial 8 
   r8
   R4.*19 
    r4 r16 c
    c8. d16 e f
    g f e d c e
    f e d c b d
    e d c b a c
}