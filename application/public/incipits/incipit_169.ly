\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 2/2
  R1
  r2 r8 e16 f e[ f e f]
  e[ d c d]  e d c e  d8[ d16 e]  d e d e 
}
