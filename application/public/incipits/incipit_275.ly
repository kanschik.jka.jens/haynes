\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
     \set Staff.whichBar = ""
   % hier können jetzt mehr als 4 viertel hinein
  d4~ d8[ es32 d c bes c a] bes8[ \grace a16 g8] r d'
  
  \unset Staff.whichBar
  \bar "|"
  g4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 12/8
  \partial 2.
  g4. r8 r d
  g a bes a bes c bes4 a8 g4 d'8
  g a bes a fis g
  fis4.
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 3/4	
  g4 g a
  bes bes c
  d c8 bes a g
  fis4. e8 d4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Adagio"
  \time 3/2
  d c bes   bes a g r d'4. g8
  fis2. e4 d2
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "5. Allegro"
  \time 2/2	
  g4 bes a d
  c8 bes a g fis4 d
  \times 2/3 { g8[ a bes]}  \times 2/3 { a8[ bes c]} \times 2/3 { bes8[ c d]} es8 d
  c bes a g g2
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "6. Largo"
  \time 3/4
  d4 \grace f8 es4 d4 \grace es8
  c4. bes8 \grace bes8 a4
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "7. Presto"
  \time 3/8
  g16 fis g a bes c
  d es d c bes8
  c16 bes a8 d
  bes a16 bes g8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 4/4
  bes4 r16 bes c es32 d  es16 d es8 r16 es d f32 es
  f8 bes, r16
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Siciliana"
  \time 12/8
  es8 d c  bes c as g4. r8 r as
  bes c bes as bes g f4. r8 r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Adagio"
  \time 4/4
   \set Staff.whichBar = ""
   % hier können jetzt mehr als 4 viertel hinein
   % an [] denken!
  bes2~ bes8[
  g'32 f es d
  c bes as g   f es f g
  as g f as
  g]
  \unset Staff.whichBar
  \bar "|"
  as4
 
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Presto"
  \time 12/8
  es8 f16 es d c  bes es d c bes as g4. r8 r as
  bes c16 bes as g as8 bes16 as g f f4.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 2/2
  d2 g
  fis2. g4
  a d, c bes8 a
  bes4. a8 g a bes c
  d es d es f4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Air"
  \time 3/4
  g4 bes d
  g2 a4 
  bes g8 fis g a
  fis4. e8 d4
}
  
  \relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Hornpipe"
  \time 3/2
  \partial 2.
  g'4 fis d
  g d2 es4 d c
  bes g2 g'4 a f
  bes f2 g4 f es d bes2
}
  
  \relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Giga"
  \time 6/8
  d8 es d c d c
  bes c a bes a g
  d' e f e fis g
  a bes g fis e d
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Rondo"
  \time 4/4
  f4 d \grace c8 bes4 g'
  g f r f
  f c c f f es r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Gavott"
  \time 2/2
  \partial 4
  f,4
  bes d2 c8 bes
  c4 es2 d8 c
  d4 f2 es8 d c bes a g f4
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4   
  \set Staff.whichBar = ""
   % hier können jetzt mehr als 4 viertel hinein
   % an [] denken!
  d4~ d32[ c b a g fis g a b c d d32. c32 d] e4 r16 e[ fis g]
  \unset Staff.whichBar
  \bar "|"
  d16[ g, a c32 b] c8.[ d16] b8[ g]

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Vivace"
  \time 2/2
  g16[ a b c] d e d c b8 g fis d
  g16[ a b c] d e d c b8 g r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 12/8
  d4. g fis r8 r g
  e4 d8 c4 d8 b a g e' fis g
  a4 a,8
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4
  g'2~ g8 \grace f8 es16 d32 c
  c4~
  c32[ g a b c d es f g f es d es d c]
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Vivace"
  \time 3/4
  r8 c16 d es8 c g' g, 
  c4 r8 c d8. c32 d
  es8 es16 f g8 es bes' bes, es4 r8
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Largo"
  \time 3/4
  bes4 r8 bes c d
  es4 r8 es f g
  f bes, c bes as8. as16 g4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Presto"
  \time 6/8
  \partial 8
  g8
  c4 d8 es4 f8
  g4.~ g8. f16 es8
  as8. g16 f8 es d c
  b4.~ b4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  c4. f,64 g a bes c d e c d4. e16 f32 d
  e8 f16 e f8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 3/4
  f4 c f~
  f e8 d c bes
  a bes c d e f e2 r4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio"
  \time 4/4	
  d2. g4~
  g8 f16 e f8 e16 d cis8 a r4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 2/2
  \partial 4
  c4
  c a c f e2. f4
  g8 f e d c bes a g
  a2 r4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 3/4
  bes4 \grace as8 g as16 bes es, es' d es
  d4. c8 bes c16 as
  g8 as bes16
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 2/2	
  \partial 4
  es,8 f g4 f8 es bes'4 es
  d bes2 c4
  bes as8 g f2
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Adagio"
  \time 3/2
  r2 r bes~
  bes1.~
  bes2 es1~ es \grace { d16[ es]} d2 g f es4. d16 es d2
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Allegro"
  \time 4/4
  \partial 8
  r16 es
  es4~ es16 es f g f es d c  bes as g f
  g8 bes es8. f16 d8 bes r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "5. Adagio"
  \time 3/4
  bes4 \grace d8 c4 d \grace { es16[ f] }
  c2 f4
  bes, es2
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "6. Presto"
  \time 6/8
  \partial 8
  bes8
  es4 bes8 bes4 es8
  g4 bes,8 bes4 f'8
  g4 c,8 c4 f8
   d c bes r r
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Largo"
  \time 4/4
  g'2.~ g16 f es d32 c
  f4. 
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro"
  \time 2/2
  \partial 8
  g8
  c[ d16 es] d es c d
  es[ d c b] c8 g
  c16[ d b c] d es c d es[ f d es] f g es f g8
}
  
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key es\major
                      \tempo "3. Adagio"
                      \time 3/2
                      % Voice 1
                      R1. R
                      bes2 c \grace es8 d2 \grace { es16[ f] }
                      es1.~ es2
                  }
\new Staff { \clef "bass" 
                     \key es\major
                        % Voice 2
                        es,,2 f g as bes4 as g f
                        g1 f2 es f g as1
                  }
>>
}
  
  \relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Allegro"
  \time 3/4
  g'4 c, c
  c b8 c d4
  g, f' f
  f4. es16 f es4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
  bes8. f16 f bes c d \grace d8 c8. f,16 f c' d es
  d es32 f a,16[ bes32 c] bes16[ d g, a32 bes] a16 bes c f, r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  f,8
  f g16 a  bes8 c16 d es4. d16 c
  d8 c16 bes a8 g16 f bes d c bes c8. f,16
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 3/4
  d8 c16 bes32 a g16 a bes c32 d es4
  fis,2 g4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Presto"
  \time 6/8
  \partial 8
  f,8
  bes4 f8 bes c d
  c4 f,8 c' d es
  d4 f8 f4 bes,8
  bes4 a8 r r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4   
  \set Staff.whichBar = ""
   % hier können jetzt mehr als 4 viertel hinein
   % an [] denken!
  g'2~ g8[ as16 g] f16.[ as32 g f es d es]
  \unset Staff.whichBar
  \bar "|"	
  d8[ bes] f'4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Spirituoso"
  \time 2/2
  \partial 4	
  g4
  g c c es
  es d8 c d4 g,
  g d' d f
  f es8 d es4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Adagio"
  \time 3/4
  es4 \grace f8 d4. c8
  bes8 as g f es f32 d es4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Presto"
  \time 6/8
  c4. g
  es'~ es4 d8
  c b c  c b c
  d4.~ d4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
  bes2~ bes8 c16 \grace es8 d16
  es4~
  es8 f16 g c, g'32 f es d es16 d8[ bes]
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Vivace"
  \time 2/2
  \partial 4
  bes8 c d4 bes8 c d es d es
  f es d es f4 bes,8 c
  d4 bes8 c d es d es f2.
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 3/2
  g2 d'2. c4
  bes a g2 g'~
  g4 f es d c bes a2.
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Presto"
  \time 6/8
  \partial 8
  f,8
  bes4 c8 d4 es8
  f4.~ f8 es d
  es f g f4 es8 d4. 
}
