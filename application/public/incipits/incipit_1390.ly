\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
    \partial 16
    g16
    c2. d4 e2. f4
    g8 g a8. f16 e4 d f16 e d c c4 r2
  }

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*46
   r2 r4 r8. g16
   c4. d8 e4. f8
   g4. \times 2/3 { a16[ g f] } e8 r r8. c16
   f2 f16 g a g f e d c
   b4 c r2
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio [Tutti]"
  \time 3/4
  es2 b4 c g r
  f'2 d4 es8. d16 c4 r
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*23
   es2. d4 c8 r r4
   f2. es4 d8 r r4
   g2. c2 es,,4
   d8 as'' g f es16 d g f
   f es d c c4 r
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allego [Tutti]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*19
   r4 r8 e16 f
   g8 g g g
   g16 a g f e8 c16 d
   e8 e e e
   e16 f e d c8 r
}
