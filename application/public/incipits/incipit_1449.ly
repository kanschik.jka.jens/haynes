 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Spirituoso"
  \time 4/4
  f4 g8. f32 g \times 2/3 { a16[ bes c} c8] r f,16 a
  g e f d \grace c8 bes4
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Adagio"
  \time 4/4
    c4~ c16 f d b c16. e64 d c8~ \times 4/6 { c16 d e f d b}
    c8[ \times 2/3 { e16 c a]} g8 f
    e
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Tempo di Menuetto"
  \time 3/4
  f8 c c4. cis8
  d8 bes bes2
  a8. c16 f,8. a16 d,8. g16
  f4 e8. d32 e f4
}

