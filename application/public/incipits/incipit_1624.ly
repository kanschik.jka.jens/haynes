\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "o.Bez."
  \time 4/4
  a'4~ a16 g32 f e f d16 a8 f' e d
  cis16 e f8 e16 g f d
  cis a b d cis e f d
}
