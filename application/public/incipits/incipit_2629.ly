\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Ouverture"
  \time 4/4
  f1
  r8 f, f g a a a bes
  c2~ c8 c bes a
  g
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2 Chasse. "
  \time 4/4
  f,2 g
  \times 2/3 {a4[ bes a]} \times 2/3 {a4[ g f]}
  g2 a
  \times 2/3 {bes4[ c bes]} \times 2/3 {bes4[ a g]}
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Menuet"
  \time 3/4
  a4 a8 bes c4
  f,2 g4
  a4 a8 bes c4
  f,2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. La  Joye"
  \time 4/4
        << {
      % Voice "1"
      r2 f2
      r4 f4 f2
      r1
      r2 r4
         } \\ {
      % Voice "2"
      f,2 r4 f
      f2 r4 f
      g a bes a
      g8 f g a g4
      } >>

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. Sarabande"
  \time 3/4
  a8. a16 a4. a8
  bes4 bes2
  d8. g,16 g4. g8
  g4 g2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "6. Loure"
  \time 6/4
  \partial 4
  c,4
  f2 f4 f4. g8 a4
  g2 g4 g2 c4
  a4.
}
