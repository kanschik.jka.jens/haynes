 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 2/2
    c16. f32 f16. bes,32 a8 d c16. f32 f16. bes,32 a8 d 
    c16. d32 c16. d32 c16.
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Vivace"
  \time 3/4
  r8 c f c a c
  f,16 g32 a bes c d e  f8 c a c f,16
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Andante"
  \time 3/2
<< {
      % Voice "1"
        R1.
        e2 a, g'~ f4.
   } \\ {
      % Voice "2"
        a,2 d, d'~ cis4. b8 cis2
        a' d,
      } >>
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}