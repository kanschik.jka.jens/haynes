
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo. Les heureux moments"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key g\major
   \tempo "1. Tres tendrement"
  \time 4/4
r2 r8 r16 fis16 fis8 e16 d
g8. g16 g8. fis32 e
fis8. fis16 \grace {fis16[ g]} a4~ 
a8 g16 fis g4. 



 
}



