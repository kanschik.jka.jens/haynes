\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Was Gott tut, das ist wohl getan [BWV 100/5]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key e\minor
   \tempo "o. Bez."
   \time 12/8
   \partial 8
   b8 g4 fis8 g4 c8 c4. b4 a8
   g c b a g fis e4. dis4 
   
   
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key e\minor
   \tempo "o. Bez."
   \time 12/8
   \partial 8
   \set Score.skipBars = ##t
    r8 R2.*7 r2. r4. r8 r8 b8 
    g4 fis8 g4 c8 c4. b4 a8
    g c b a g fis e4. dis4 r8
    
}



