 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef bass
  \key d\major
   \tempo "1. o.Bez."
  \time 4/4
    \partial 4
    cis,,4
    a' a2 b8 g
    cis4 cis2 d8 b
    a16 fis g a  b cis d e fis8 g a cis,
}
 