 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 3/4
  f2.~
  f8 c bes a g f
  bes2.~ bes8 e g e c bes
  gis a r f' es cis
  \grace cis8 d4. \grace c16 bes8 \grace a16 g8 f
  \grace f8 e4 f
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Presto"
  \time 2/2
  f1 c2. bes4
  a g f2~
  f e f4 c'8 bes a4 g8 f
  c'4 g e c
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 3/4
  c4~ c16 f e d c8 c
  c4 bes a
  a8 g bes g f e
  \grace e8 f4~ f16 f' e f g f e f
  es4 d16
}
