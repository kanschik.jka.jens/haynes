\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Prelude"
  \time 3/4
  r8 c d es f d
  es c d es f g
  as g f es d c
  b4. a8 g4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Canaries"
  \time 6/8
      c8. d16 e8 e8. d16 c8
      f4. e
      d8. e16 f8 f8. e16 d8
      g4. a
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Sarabande"
  \time 3/4
    c'4 g4. bes8
    as4. g8 f4
    g8 as g4 d8 f
    es2 \grace d8 c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Premier Menuet"
  \time 3/4
    c'4 g f
    e2 d4
    c8 d e f g a
    g4. a8 b g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Piece de petits coups"
  \time 2/4
      c4 g' a8 g f e
      f2 g8 a g f
      e4 d
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "6. allemende"
  \time 2/4
  \partial 16
  c'16
  c4. g8
  c es d g,
  as16 bes as g  f g f es
}

