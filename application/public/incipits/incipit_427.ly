\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 3/4
    f8 f,  f16 g a bes c8 f,
    d'4 c r
    f8 f,  f16 g a bes c8 f,
    f'4 e r
}
