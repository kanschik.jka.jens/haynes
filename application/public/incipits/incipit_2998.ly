 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "[6.] o.Bez.Adagio"
  \time 3/4
      g'2.~ g
      f8 es d c b4
      c8 d es2~
      es4 d8 es f4
}

