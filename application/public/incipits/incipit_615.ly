
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Les Idées Francoises Ou Les Délices De Chambray [31 Pieces]"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. L'Isle Enchantée. Tendrement"
  \time 3/4
\partial 4
c4 e4. f8 d g
e4 c e8 f
g4 f8 e d c
d2
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Les Cascades. Pastourelle. un peu Legerement"
  \time 6/8
\partial 4.
e4 g8 
f e d f16 e d c b c
d8 g,16 g' g f e8 d16 f f e
d8 c16 a' a g c b a g f e 
d4.
 
}
