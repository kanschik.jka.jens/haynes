\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 4
    a8. b16
    cis4. cis8 cis a e' cis
}
  
