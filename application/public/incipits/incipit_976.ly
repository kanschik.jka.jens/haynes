\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
      c4 b c r16 g' a g
      e c g e c e' d c g' g, d b g g' c g
      e
}
