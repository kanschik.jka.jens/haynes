\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "Bruit de chasse"
  \time 2/2
  r8 g' c, c b a16 b c8[ b16 a] g g' g g g[ g, g g] c8[ c] d c16 d
  e8 c
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "Legerement"
  \time 3/8
  a8 b c b c d
  c b a gis4 e8
  }

\relative c'' {
  \clef treble
  \key c\major
   \tempo "[Air]"
  \time 6/8
  g'4 a8 g4 f8
  e4 d8 c4 d8
  e f g g f e
  dis4.~ dis8
}
