 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
  bes,8 d f bes  d f bes4
  f8 g16 f es8 d es c d4
  bes,8 d f bes  d f bes4
  f,8 g16 f es8 d es c d4
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Tourne"
  \time 2/4
  \partial 8
  f8
  d16 bes a bes f' bes
  bes,4. c8
  d16 bes a bes f'8 bes
  bes,4 r
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Aria"
  \time 4/4
  f2 g4. a16 bes
  f4. g8
  es8. f32 g f8 es
  d4 r8 f bes8. a16 g8. f16
  e4 f
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Menuett"
  \time 3/4
    f4 g8 a bes4
    a2 g4
    f g es
    d2 c4
    bes f es
}