 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Vivace"
  \time 3/4
  r8 d,16 c b8 d g a16 b a8 d, d' c4 b16 a
  b a b c d8 c4 b16 a
  b a b c d8 b c d
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. Largo"
  \time 3/4
  b'4. a8 g fis g4 fis r8 a
  g4 fis r8 a
  g4 fis e b'2.
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Allegro"
  \time 2/4
  d4 d d16 c d4. d16 c d4.
  \times 2/3 { d8[ e fis] } \times 2/3 { g8[ fis e] } 
  d4 d
}
