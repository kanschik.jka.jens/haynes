 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 2/4
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
  g'4. a32 f e d
  \grace c8 b16. d32 f,8
}