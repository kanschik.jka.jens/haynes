\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 2/4
  \partial 8
  g8
  c c c c
  \times 2/3 { e16[ d c] } c4 c'8
  g16 e f4 e16 d
  \times 2/3 { e16[ c e] } g8
  \times 2/3 { g16[ e g] } c8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 2/4
  \partial 8
  r8
   \set Score.skipBars = ##t
   R2*34
   r4 r8 g
  c c c c
  \times 2/3 { e16[ d c] } c4 c'8
  g16 e \grace g8 f4 e16 d
  \times 2/3 { e16[ d c] } g'8[ b c]
  g,16 e \grace g8 f4 e16 d
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio un poco Andante [Tutti]"
  \time 2/4
  c8 c, e8. d32 c
  g'8 g, d''8. e32 f
  \times 2/3 { e16[ g e] } d16 c
  \times 2/3 { d16[ f d] } c16 b
  c8 g, g'4~ g2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio un poco Andante [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*18
   c'8 g f16 e d c
   a'32 g fis g g8 r16 d e f
  \times 2/3 { e16[ g e] } d16 c
  \times 2/3 { d16[ f d] } c16 b
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Tutti]"
  \time 3/8
  c8 d b 
  \times 2/3 { c16 b a} g4
  \times 2/3 { a16[ g f]} a8 b
  \grace b8 c4.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Solo]"
  \time 3/8
   \set Score.skipBars = ##t
   R4.*42
  c8 d b 
  \times 2/3 { c16[ b a] } g8. g'16
  \times 2/3 { a16[ g f]} a8 b
  \grace b8 c4 g,8

}