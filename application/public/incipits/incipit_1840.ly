 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 2/2
    es4 es2 g4
    f8 d es2 g4
    f8 d es g bes,4 as
    \grace as4 g2 r8
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Adagio"
  \time 4/4
  \partial 8
  bes8 es es~ \times 2/3 { es16[ g f]}  \times 2/3 { es16[ d c]} bes8 bes4 as8
  \times 2/3 { g16[ bes c]}  \times 2/3 { des16[ c des]} c8. es,16 d8 es r
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Allegretto"
  \time 2/4
  es4 g16 f es d
  es4 g16 f es d
  es8 bes4 bes8
  \grace bes8 c4 bes8 r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}