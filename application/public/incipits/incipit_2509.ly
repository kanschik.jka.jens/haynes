 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef bass
  \key bes\major
   \tempo "1.Allegro [Bassoon]"
  \time 4/4
  \partial 8
  d,16 c
  bes a g fis g8 d es g c c16 bes
  a g f es f8 c d f bes
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1.Allegro [Oboe]"
  \time 4/4
  \partial 8
  bes16 c
  d8 c16 d es8 d16 c d8 c16 d es8 d16 c 
  d8 g16 fis g bes a g fis8 d
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Adagio"
  \time 3/4
  r4 r8 a' bes fis
  g4 r8 g a e
  f4 r8 f g d
  es4 es4. f8 d4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Vivace"
  \time 3/8
  d8 es16 d es8
  c4 d8
  bes c16 bes c8
  a4
}