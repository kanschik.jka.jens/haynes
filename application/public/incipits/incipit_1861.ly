\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
    d2 c8 bes a bes
    c4. d8 es es es es
    es d es2 c4 a bes r2
}
