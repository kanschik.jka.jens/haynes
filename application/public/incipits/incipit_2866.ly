 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

 

 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. o.Bez. [Tutti]"
  \time 3/4
  f,8 f f f f16 g a bes
  c8 c, c c c c
  f f f f f16 g a bes c8
  c, c c c c c 
  f f a a c c
  e
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. o.Bez. [Solo]"
  \time 3/4
  \set Score.skipBars = ##t
    R2.*22
    f8 c16 bes a8 c f a
    f c16 bes a8 c f a
    g e16 d c8 e g bes
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Grave"
  \time 2/2
  \partial 8
  g8
  c[ c] c16 e d c c8 b r g
  d'[ d] d16 f e d d8 c r g' a16[ g f e] d8 c b16. a32 g8 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro [Tutti]"
  \time 12/8
  f8 c f  a f a  f c f  a f a
  g e g  bes g bes   g e g    bes g bes
  a f a  f c f   c a c   es c es
   }
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro [Solo]"
  \time 12/8
   \set Score.skipBars = ##t
   R1.*9
   f8 a16 g f e f8 a, bes c a'16 g f e f8 a, bes
   c d16 e f g a8 bes c e, d c r r
   }


