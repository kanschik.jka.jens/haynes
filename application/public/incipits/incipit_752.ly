 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Vivace"
  \time 4/4
    d8 a4 b16 cis d4 r
    d8 e4 fis16 g
    fis d e fis g a b cis
    d8 cis4 d16 e d4 b
}


\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "2. Siciliano Cantable"
  \time 12/8
  fis,8. g16 fis8 fis4 d'8 d4. cis4 r8
  fis,8. g16 fis8 fis4 e'8 e4. d4 r8
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Affetuoso"
  \time 3/8
  d8 fis b
  a16 g fis e d cis
  d8 fis b
  a16 a, cis a d a
}
