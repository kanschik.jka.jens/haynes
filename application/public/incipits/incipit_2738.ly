 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

 
\relative c'' {
  \clef bass
  \set Staff.instrumentName = #"BC"
  \key c\major
   \tempo "Demütig"
  \time 3/4
    e,4 c a
    e2 r4
    c' a f c2 r4
    d8 f e d c b
    c b a4 r
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Ob d'Am"
  \key c\major
   \tempo ""
  \time 3/4
     \set Score.skipBars = ##t
     R2.*5
     r4 r e~
     e \grace d8 c4 \grace b8 a4~
     a d \grace c8 b4
     \grace a8 g4. a8 b4
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alto"
  \key c\major
   \tempo ""
  \time 3/4
     \set Score.skipBars = ##t
     R2.*4
       r4 r e~
       e c a
       e2 gis8 a
       \grace g8 f4. e8 f4
}