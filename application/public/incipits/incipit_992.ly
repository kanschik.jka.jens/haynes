\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [tutti]"
  \time 2/4
     \set Score.skipBars = ##t
     g,16 g g g  b b d d
     b8 g r d'16 d
     e e fis fis g g a a b8 a r4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [solo]"
  \time 2/4
     \set Score.skipBars = ##t
     R2*16
     g'8 fis16 e d c b a
     g b a g a c b a
     b d c b
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 4/4
  bes2~ bes8 a e' g,
  fis2~ fis8 d e fis
  g1~
  g4 fis g2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/8
  \partial 8
  d,8
  g4 d8 g4 d8
  g16 fis g8 a b a r
} 

  