\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Höchster, Tröster, heiliger Geist [BWV 183/4]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Ob da Cac. 1+2 un."
  \key c\major
   \tempo "o. Bez."
   \time 3/8
   \set Score.skipBars = ##t
   c32 e d c g'8[ a]
   b,4 c8
   g32 b a g f'8[ e]
   \grace e8 d4.~
   ~
   d32 c b c d[ c d e]
   
  
 
 
                                                                                   

}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key c\major
   \tempo "o. Bez."
   \time 3/8
   \set Score.skipBars = ##t
 R4.*21 b4 c8
 g32 b a g f'8 e16[ d]
 d4.
       
}



