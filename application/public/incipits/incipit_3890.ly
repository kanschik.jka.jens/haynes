 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 



\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe Solo"
  \key c\major
   \tempo "o.Bez."
  \time 2/4
    r8 g' e c
    g'4 r8 f
    e4 d16 e f d
    e c g e  c8 a''
    g4 f
    e16 d c8 r8

}



\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alto"
  \key c\major
   \tempo "o.Bez."
  \time 4/4
     \set Score.skipBars = ##t
     R1*19
     r4 r8 g8
     e4 c g' r8 f
     e4 d16 e f d
     \grace f16 e8 d16 c r8

}