 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Vivace"
  \time 6/8
    f4.~ f~ f~ f
    bes,8 bes f d c bes
    bes' c d bes4 r8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Tempo giusto"
  \time 4/4
    d,8 r16 es
    d8 g fis d r4
} 

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/4
    bes8 d16 c bes8 bes, d f
    bes f' d f d f
    bes, d16 c bes8 bes, d f
    
}
