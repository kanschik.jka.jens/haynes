\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Komm, komm, mein Herze steht dir offen [BWV 74/2]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe da Caccia"
  \key f\major
   \tempo "o. Bez."
   \time 4/4
  r8 a8 bes16 a g f c'8 f,16 e f8 d'
  c16 bes a g f e c'8 a4.
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key f\major
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
    R1*7 r2 r8 a8 bes16 a g f
    c'4. d8 bes16 a g a bes d c8 
    a4 g
}



