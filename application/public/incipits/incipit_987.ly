\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 3/4
  c8 c, e g d g
  e c e g d g
  e c e g c a
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 4/4
  c2~ c16 a b8 r4
  f'2~ f16 d e8 r4
  a4~ a16 f e d  g4~ g16 e d c f4~ f16 d e c d b g8 r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [tutti]"
  \time 2/4
  c,16 c c c  e e e e
  g g c, c e e g g
  b, b b b  d d d d 
  g
 } 

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [solo]"
  \time 2/4
     \set Score.skipBars = ##t
     R2*14
     r4 r8  g
     e16 c d e f g a b
     c8 b16 a g f' e d c a' g f e d c d b8 g r4
 } 
  