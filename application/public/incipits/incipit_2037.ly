\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Andante"
  \time 6/8
  \partial 8
  bes8
  d4. f
  es8 d c bes a g
  f4. \grace c'16 bes8 a bes
  b c es \grace f8 es8 d c
  \grace c8 d4. \grace c8 bes4 r8
}
