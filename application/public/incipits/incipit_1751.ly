\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
      \set Score.skipBars = ##t
  \time 4/4
  f2.~ f16. g32 f16. es32
  d2.~ d16. es32 d16. c32  
  c8 bes
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro [Hautbois]"
  \time 3/4
        \set Score.skipBars = ##t
   R2.*6
   r8 f es d c bes
   a f' a, f' bes, f'
   c f a, f' e4
   d r r
}

\relative c'' {
  \clef bass
  \key bes\major
   \tempo "2. Allegro [Basson]"
  \time 3/4
        \set Score.skipBars = ##t
   r8 bes, a g f es
   d bes' bes, bes' c, bes'
   d, bes' bes, bes' d, as'
   g c bes a g f
   e
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 3/2
  d2.~ d4 es4. d8
  c2. es4 d4. c8
  bes4. a8 g2 bes4 g
  a4. cis8 d1
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 2/4
  f4 f
  f4. g16 a
  bes8 a16 g f8 es
  d c bes4
}
