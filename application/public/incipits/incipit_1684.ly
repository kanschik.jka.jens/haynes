\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. La Baussan. Gracieusement"
  \time 3/4
  \partial 4
  e8 g
  c,4 \grace c8 d4. e8
  b2 g4
  c8 b c e d f
  e4 \grace d8 c4 e8 g
  c,4 \grace c8 d4. e8
  b2

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Rondeau"
  \time 3/8
  r8 g8 c
  c16 b a g f e
  f d f8 a
  a16 g f e d c
  d b d8 g
 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Premier Tambourin"
  \time 2/4
  g8[ f e a]
  g a16 b c4
  g8[ f e d]
  e d16 e c8 c
  

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. La D'Agut. Gracieusement"
  \time 3/4
\partial 4
g4 c2 es4
d8 b c4 f
es8 d g4 c,
b4. a8 g4
bes8 a c4 b
es8 d f4 es
as8 g f es d c 
d2

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Légerement"
  \time 2/4
g4 c,8 c
d c16 b c8 c
g'4 f16 e d c
d8 c16 b
c8 c'
f,[ e d g]
e c4 

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gavotte"
  \time 2/2
\partial 2
c4. d8
e4 c f g8 a
f4 \grace e8 d4 e8 c f4
d8 b g'4 c,8 a f' e d2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Premiere Gigue"
  \time 6/8
c8 e g f e d
e4 d8 c4 g8
c d e d g f
e d e c4 g8


}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIIe. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. La Dubois. Légerement"
  \time 2/4
\partial8
c8 g'4 d
e16 d e f g a b c
d8 f, f e16 d
e c d e f g a b
c8 e,

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Modérément"
  \time 3/8
c8 g'4
e16 d e f g8
b, c d
e d16 e c8
f16 g a g f e
d e f e d c
b c c8. b32 c
d16 c b a g8

}
  
  \relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gaiement"
  \time 2/4
\partial 4
c8 g'
g16 f e g f e d f
e d c e d c b a 
g8[ c f e]
d4

}
  
  \relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Sarabande "
  \time 3/4
  g4 c4. d8
\grace d8 es4. f8 g4
c,4 f4. g8
d2 c4
g' f4. es8
f4. es8 d4
es8 f f4. g8 g2

}

 \relative c''' {
  \clef treble
  \key c\major
   \tempo "5. Premier Menuet "
  \time 3/4
  c8 g a f g e
f4 e8 f d4
g8 e f d e c
g4 a8 b g4

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IVe. Sonate " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. La Beaumont. Iere Ariete"
  \time 2/2
\partial 2.
g8 f f e f e 
d4 f8 e e d e d 
c4 e8 d d c b d
g, c f e d g f g
e4


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. La Comtesse de Lusse. Iere. Musette. Gracieusement"
  \time 3/4
e2 d4
g8 f e c f a 
g4. d8 e f
e2 d4
e8 c e g e c 
d b d g d b

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Contredanse"
  \time 2/2
\partial 2
c8 g' c,4
c8 g' c,4 e f 
g2 c,8 d e d 
c f e f d g f g
e4 c 

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "4. Premier Menuet"
  \time 3/4
g8 c, e g e c
a'4 g2
c8 g a g f e
d c b d g,4

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ve. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. La Persan. Gracieusement"
  \time 3/8
\partial 8
d8 g16 fis
g8 d
b'4 a8
g16 a g8 d
b4 a8
g c g
e' fis g
fis4 g8 a4 d,8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Gaiement"
  \time 2/4
\partial 8
g8 d'[ d d d]
e d4 g8
c, c c g'16 c,
c8 b4 d8
c[ a a c]
b g4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Premier Musette. Gracieusement"
  \time 3/4
b2 \grace a8 g4
e'2 d4
g fis8 g a4
d,2.
c4 c c 
c b2
e8 d c b a g
d' g fis e d c
b2 \grace a8 g4

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "4. Légerement et marqué"
  \time 2/4
g4 d
g16 a b c d8 d,
g4 fis 
g16 fis e d c b a g
g'4 d
g16 a b c d8 d,
e4 cis d2

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VIe. Sonate " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. La Simianne. Gracieusement"
  \time 2/2
r4 g4 fis8 e d c
b4 e d8 c b a 
g4 c b8 a b c
d e fis d g4. a8 fis4 

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Les Forgerons. Piqué"
  \time 2/4
\partial 8
d8 g b16 g d' g, b g
d8 g16 d e g c, e
d8 g16 b, c e d c
b8 a16 b g8 d'

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Sarabande"
  \time 3/4
d4 g4. b8
e,4 fis4. g16 a
d,4 c4. d8
b4. a8 g4
b \grace b8 c4. d8
\grace d8 e4. fis8 d4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Légerement"
  \time 2/4
\partial 8
d8 b8.[ c16 d8 e]
d g16 d a'8 d,
b' a16 g fis e d c
b8 a16 b g8 a

}

