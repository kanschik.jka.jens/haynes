\version "2.16.1"

#(set-global-staff-size 14)

\relative c' {
  \clef bass
  \key bes\major
   \tempo "1. Allegro non tanto"
  \time 2/4
  \partial 8
  bes8
  bes, c d es f bes, bes bes' bes, c d es f
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Largo"
  \time 4/4
  d8 g fis g d g fis g
  bes, a a4 a2
  a4 r16 c bes a bes d c bes c es d c d4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/4
  d8. es32 f es4 d
  es c2 
  d8. es32 f es4 d
  es c2
}
