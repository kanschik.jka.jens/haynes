\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. [Allegro moderato] [Tutti]"
  \time 2/4
  d,8. d'16 cis8. b32 cis
  d16 d cis b a g fis e
  d8. e16 e8. d32 e
  fis16 e fis e d8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. [Allegro moderato] [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*33 
  a4 b a16 cis b cis d8 b
  a16 g a b  g fis g a
  fis a b cis d8 b
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  fis,4 r8 fis' g g g g 
  g fis r fis e g g fis16 e
  e8 d r
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Adagio [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*5 
   fis4 \grace e8 d8 \grace cis8 b8 e4 \grace {e16[ fis]} g4
   fis8 e d cis \grace e8 d8 cis16 b r8
   
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Vivace [Tutti]"
  \time 3/8
  d8 d, d
  d8. e32 fis g a b cis
  d8 d, d
  d8. e32 fis g a b cis
  
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Vivace [Solo]"
  \time 3/8
  \set Score.skipBars = ##t
   R4.*34 	
   a8 d cis d16 e fis8 e
   g16 fis fis e e d
   cis d e cis d b
}