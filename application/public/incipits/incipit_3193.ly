\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran + Alt): Herr, du siehst statt guter Werke [BWV 9/5]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Traverso"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key a\major
                      \tempo "Duetto"
                      \time 2/4
                      % Voice 1
                    e16 fis e d e cis b a 
                    fis'2~fis8[ fis e d] cis d e4~
                    ~e8[ e d cis]
                 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key a\major
                     \set Score.skipBars = ##t
                     R2 a16 b a gis a fis e d
                     b'2~ b8[ b a fis]
                     fis gis a4~a8
               
                        
                
                  }
>>
}


\relative c'' {
	<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Sopran"
                     \key a\major
                      \tempo "Duetto"
                      \time 2/4
                      % Voice 1
                  \set Score.skipBars = ##t
                     R2*24 e4 r8 a,8 fis'2~ fis8 fis e d
                     cis d e4~e8 e d cis
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Alt"
                     \key a\major
                     \set Score.skipBars = ##t
                        % Voice 2
                 R2*25 a4 r8 d,8 b'2~
                 ~b8 b a gis 
                 fis gis a4~ a8
                        
                
                  }
>>
}

	
	
