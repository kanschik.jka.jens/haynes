\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
  f,8 a16. f32 c'8 c16. bes32 a8 f16. g32 a16 g f e
}
