\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Mund und Herze steht dir offen [BWV 148/4]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\major
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                      g4. a8 fis g a b 
                      c16 d e4 d8 c b b4
                      a16 c b a e'8
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\major
                     \set Score.skipBars = ##t
                        % Voice 2
                     d,8 c d e d e fis g 
                     a16 b c4 a8 a g g f e4 r8
                       
                        
                
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 3"
                     \set Staff. shortInstrumentName = #"Ob 3"
                     \key g\major
                     \set Score.skipBars = ##t
                        % Voice 3
                    b8 a g c a b c d~
                    ~d fis g a d,4 d c r8
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key g\major
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    R1*8 g4. a8 fis g a b
    c16 d e4 d8 c b b4
    
    
    }