\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro "
  \time 4/4
    <f, c' f>4 <f c' f> r8 a'4 g8 
    f f4 e8 d d4 c8 
    bes bes4 a8 g f4 f8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio"
  \time 3/4 
    a'4 d, r8 d'16 bes
    bes4 a r
    e, a, r8 bes'16 g
    g4 f r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Presto"
  \time 4/4
    f,16 f f f  f f f f  f f f f   f f f f
    f4 a' g f
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro Moderato"
  \time 2/4
    bes8 bes bes16[ c32 d] es f g a
    bes16 f f4 a8
    bes16 f f4 g16 es es[ d]
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 2/4
    \override TupletBracket #'stencil = ##f
    c8 c~ \times 4/6 { c16 a bes c d e}
  \times 2/3 { f16 a g } f8
    }

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Tempo di Menuetto"
  \time 3/4
    bes'2 f8 d
    bes4. c8 d es
    f2 c8 a
    f2.
    es8 d g f bes a
}

\pageBreak

\markuplist {
    \fontsize #3
    "Concerto III " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
    c,16 g a b c g a b  c b c d  e d e fis
    g d e fis
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 3/4
    g8 a b c d g, fis4 d' r
    e,8 fis g a b e, dis4 b' r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto"
  \time 3/8
    c,8. d16 e f
    g8 a b c4.~ c8 g b
    c c, b'
}
