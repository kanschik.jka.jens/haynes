 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/4
    \override TupletBracket #'stencil = ##f
      bes8 bes4 \times 2/3 { d16 es f}
      bes,8 bes4 \times 2/3 { d16 es f}
      g8 g4 \times 2/3 { bes16 a g} 
      \times 2/3 { f16[ es d]} 
}
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 4/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
     d8 d d d  d, d d d
     c' c c c   c, c c c 
      bes' bes bes bes bes bes bes bes a4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 4/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
        \partial 8
        f8
        bes16 f d4 f8 bes16 f d4 f8
        bes,16 c d es f8 d  c16 f, a c f8 r
}

