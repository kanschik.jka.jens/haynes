\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Moderato [Tutti]"
  \time 4/4
    c4 c, e g c c, e g c g'2 g4
    g c8 g g f e d 
    c4 g'2 g4
    g 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Moderato [SOlo]"
  \time 4/4
  \set Score.skipBars = ##t
   R1*31 
  r8 e f  e c b d
  c e d f  e c b d
  c4 e8 e e d c b
  c4 r r2
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 2/4
  g'2 g16 e e4.
  \grace e8 d8 c16 b \grace d8 c8 b16 a
  g4 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto Assai [Tutti]"
  \time 3/4
   c8[ e16 d] c8 d e fis
   g8. fis16 g8[ d] b g
   a[ c16 b] a8 b c d
   e8. d16 e8[ b] g e
}

