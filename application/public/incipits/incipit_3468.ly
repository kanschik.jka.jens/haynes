 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 3/4
    r4 a a 
    a g fis e a a a2 gis4 a a
}
 