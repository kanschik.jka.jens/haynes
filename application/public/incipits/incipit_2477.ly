 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "Aria 'Per vendetta'"
  \time 3/4
    r4 c c
    c g r 
    r e' e e c
}
