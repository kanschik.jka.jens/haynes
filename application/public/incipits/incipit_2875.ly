\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
   \time 4/4
   \set Score.skipBars = ##t
   R1*5 
     d4 g f r8 bes
     g g g f16 es  f8 bes, bes d
     es es es d16 c d8 f, f d'
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [Violine]"
  \time 4/4
    es2 r 
    r8 bes bes bes  bes as as as
    as g g g
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [Oboe]"
  \time 4/4
  es2 r R1 R
  bes2 r
  es4. d8 c4 f8. es16
}
