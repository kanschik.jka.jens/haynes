\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro "
  \time 2/2
  e4 r16 e e e  c8 e b e
  c a r16 e' e e f4 r16 d d d
  e[ f e d] c
}



\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio"
  \time 2/2
r8 e16[ d] e g f e d8 c16 b d[ f e d]
e[ d e f] g g f e d e c e d4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro "
  \time 2/4
  \partial 8
  a16 b
  c8 b16 a  c8 b16 a
  b8 e, b' e
  c b16 c a8 c16 d
  e8

}
