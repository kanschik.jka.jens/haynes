\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite en G ré sol Bémol " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "1. Les Heures Heureuses . Gracieusement"
  \time 3/4
  r4 g4 f
  es d c b2.
  c4 d es
  d e f
  e fis g fis2.

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Air des Paysans et des Pastres. Rigaudon. Rondeau"
  \time 2/2
  \partial 4
  d4 g8 f e a g4 bes
  g2 g4 a
  fis g a bes
  a8 g a bes a4 d,

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Gavotte"
  \time 2/2
  \partial 2
  d4 a
  bes g es'4. f8
  d4 d bes'4. bes8
  bes4 a g4. fis16 g fis4

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "4. Ier Menuet. Rondeau"
  \time 3/4
  g4 a bes
  a g fis
  g fis g d8 c d es d4
  

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite en G ré sol Bécarre " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prélude"
  \time 3/4
  \partial 4
  g4 d' c d
  b a g fis2. 
  g4 a b a b c
  b cis d 
  cis a 

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Symphonie"
  \time 2/2
  b4 g d'4. d8
  d2. d4 d1~
  ~d2 c4. c8
  c4 bes8 a bes4. c8
  a1~a4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Menuet"
  \time 3/4
  g4 g8 a b c
  d4 d d 
  e fis g
  fis e8 fis d4
  g d e
  b c8 b a g 
  fis2 g4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Les Matelots"
  \time 3/4
  g8 fis g a b c
  b4 g b8 c
  d c d e d c
  b4 g c8 d
  e4 e d8 c
  d4 d

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite en C sol ut Bémol & Bécarre " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Les Prisons. Prélude"
  \time 2/2
  es4. es8 f4 g
  b,4. b8 c4 d8 es
  d4. d8 d4 d
  g,4. g8 g4 g
  g as8 g f4. g8 
  g4

}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "2. Suite des Prisons. Gravement"
  \time 3/4
  r4 g4 d
  es4. es8 f8 g
  c,2.
  c4. bes8 c g
  as4 as4. bes8
  g4.

}
  
  \relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Marche des Barbets"
  \time 2/2
  \partial 4
  g4 c b c d
  e8 d e f e f e f
  g2 f
  e8 d e f e4 

}
  
  

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite en D la ré Bémol & Bécarre " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. La Tendresse"
  \time 3/4
  d4 a'4. a8
  a4. bes8 g8 f16 g
  a8 d, e4 f8 g
  cis,4 a2
  d8 e e4. d16 e
  f4. g8 e4
  f4 g4. a8
  bes4.

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Ier Menuet"
  \time 3/4
  d4 a'2
  bes a4
  g g2
  f8 e f g f4
  d a'2
  bes a4 g g2
  f2.

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Ier Rigaudon"
  \time 2/2
  \partial 4
  a4 d2 e
  f4. e8 f4 d
  a' g f e 
  f4. e16 f d4 

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Les Regrets. Tendrement"
  \time 3/4
  d4 cis d
  e2 e4
  f4 g2
  e2 e4
  d cis d e2 e4
  f4 g2 e2.

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Suite des Regrets"
  \time 3/4
  d4 fis2
  e2 fis4 g a2
  g fis4
  d fis2
  e2 fis4 g fis2 e2.

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite en G ré sol Bémol " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Ouverture"
  \time 2/2
  d4. d8 g4. g8
  g4 a8 bes a4. g8
  fis4. e8 d c bes a 
  bes4. bes8 bes4 d
  g,4. 

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "2. Prélude"
  \time 2/2
  g4. g8 b,4. c16 d
  c4. d16 es d4. es16 f
  es4. d16 c b4. b16 c 
  d2

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "3. Les Plaisirs. Ritournelle"
  \time 3/4
  bes4 g4. fis8
  g4. d8 es4
  d4 c4. f8
  d4. c8
  bes4 f' bes4. a8
  g4. 

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Sarabande"
  \time 3/4
  d4 g d
  es4. d8 c4
  d a4. bes8
  bes4. a16 bes g4

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "5. Loure"
  \time 6/4
  \partial 8*3
  bes8 c4
  d2 d4 d2 es4
  c2. c2 d4
  bes4. a8 bes4 c4. d8 bes4
  a2.~ a4.

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "6. Air des Paysans. Rigaudon"
  \time 2/2
  g4 g f es
  d8 c d es d4 c
  bes c d c8 bes
  a g a bes a2
  

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite en D la ré " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Ouverture"
  \time 2/2
  f4. f8 f4. d8
  a'2 a,4. a8
  d4. d8 e4. f8
  cis4. d8 d4. cis16 d
  e1

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Premiere Sarabande"
  \time 3/4
  a4 d4. e8
  f4 f4. g8
  a4 a bes8 a
  g4 g a8 g
  f4 f g8f
  e4 e f8 g
  a2.

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Bourée"
  \time 2/2
  \partial 4
  d4 a' g8 f g4 f8 e
  f4 d2 e4
  f8 e d c b4 e8 d
  cis4 a a 

}

\relative c''' {
  \clef treble
  \key d\minor
   \tempo "4. Fantaisie"
  \time 3/4
  \partial 4
  a4 f8 e f g f g
  e4 e f
  d8 c d e d e
  cis4 cis 

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite en C sol ut " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Prélude"
  \time 3/4
  \partial 4
  g4 g a g
  f g f 
  e f e
  d2.
  d2 d4 d c d
  e d e 
  f g f 
  e2


}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. L'Embarras de Paris"
  \time 2/2
 r4 g4 e f
 g a g f
 e e f g
 a g f e
 d e f g
 e f g a
 fis e d c b
  


}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Symphonie"
  \time 3/4
  \partial 4
  g4  c d e
  d e f
  e f g
  a g a
  d, e f 
  e f g
  c,2 f4
  d2 g4 e2


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Marche"
  \time 2/2
  c4. g8 c4 d
  e4. d8 e4 f
  g4. g8 g4 f
  e4. f8 g4 a
  d,4. d8 d4
 


}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite en F ut fa" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Carillon"
  \time 2/2
  \partial 2
  a4 bes
  c2 d
  c4 bes a bes
  c2 d
  c4 f e d
  c f e d
  


}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Air"
  \time 2/2
c4. c8 d4. e8
c2 c4. c8 
f4. f8 f2~
~f es4. es8 es2 d4. d8 c4.


}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Chaconne"
  \time 3/4
  r4 a8 bes c d
  c4 d e
  f g a 
  g8 f e4. f8
  f4 

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite en C sol ut Bémol" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Symphonie"
  \time 3/2
  r2 c2 c
  g'2. f4 g2
  as2. g4 as2 e2. d4 e2
  f f es 
  es1


}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "2. Air"
  \time 2/2
  g4. g8 c,4. c8
  c4. bes8 bes4 c8 g
  as4. as8 a4 bes8 c
  b4. b8 c4 d8 es
  d4. d8 d2

  


}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Passacaille"
  \time 3/4
  r4 c8 d es f
  d4 es8 f g as
  f4 f g 
  es es f g r4 r4



}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "4. Sommeil"
  \time 3/2
 g4 f g as g f
 es d es f es d
 c bes c d c bes
 a g a bes c d
 b a bes c bes a
 g2


}
