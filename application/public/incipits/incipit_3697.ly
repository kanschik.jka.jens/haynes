
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
g8 b16 a g8 g
g b16 a g8 g 
g4 fis e r4
  
} 

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 3/4
b8 a b c d4
g,4. a8 b4
e, fis g
a d r4
  
} 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Allegro "
  \time 3/8
b8 e,16 dis e8
e c' r8
a8 fis16 g a8
a16 fis g8 r8
    
} 

