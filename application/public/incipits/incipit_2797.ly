 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Marciata"
  \time 2/4
    c8 c16 c c8 g
    e' e16 e e8 d
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Minuet"
  \time 3/4
  c,4 c8 c c c
  e4 e8 e e e
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Giga"
  \time 6/8
  e8. f16 e8  d8. e16 d8
  e8. f16 e8  d8. e16 d8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Gavotta"
  \time 2/2
    e4 f8 e d4 c
    e4 f8 e d4 c
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "5. Minuet"
  \time 3/4	
  e4 f8 e d4 e4 f8 e d4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "6. Giga"
  \time 6/8
  c,4 c8 c e c g'4.~ g4 c8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "7. Gavotta"
  \time 2/2
  c2 g e'4. d8 e4. d8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "8. Gavotta"
  \time 2/2
  e4. d8 e4. f8
  g2 c,
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "9. Minuet"
  \time 3/4	
  c4 g e c' g e
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "10. Giga"
  \time 6/8
  \partial 8
  g8 c8. d16 c8 c8. e16 c8 c4.~ c4 d8
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "11. Gavotta"
  \time 2/4
  c4 g c8. d16 c8. d16 e4 d
}
\relative c'' {
  \clef treble
  \key c\major	
   \tempo "12. Minuet"
  \time 3/8	
  e16 d e8 d c4 g8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "13. Marciata"
  \time 2/4
    c8 c16 c c8 d
  e8 e16 e e8 f}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "14. Giga"
  \time 6/8
  \partial 8
  g8 c4 g8 e4 g8 c4 g8 e4 g8
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "15. Marciata vivace"
  \time 4/4
  c8. d16 c8. d16 c4 g
  e'8. f16 e8. f16 e4 d
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "16. Minuet"
  \time 3/4	
  g4 g8 g g g
  e4 r r
}
\relative c'' {
  \clef treble
  \key c\major	
   \tempo "17. Giga"
  \time 6/8
  \partial 8
  g8 c8. d16 c8 c8. d16 c8
  d8. e16 d8 d8. e16 d8
}
\relative c'' {
  \clef treble
  \key c\major	
   \tempo "18. Minuet"
  \time 3/4	
  c,4 e g
  c2 d4 e f8 e f e
}