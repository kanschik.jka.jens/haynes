\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Set 1, Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 4/4
  d,16 fis a fis  d fis a fis d8 <fis d'>8 <fis d'>4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Set 1, Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o. Bez."
  \time 3/4
  \partial 8.
  f,16 g a bes8 r <bes d> r <d f>8. <c es>16
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Set 1, Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 2/4
  es,16 f g as g8 f16 es
  <g bes es>8 <bes es g>  <bes es g> 
}

  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Set 2, Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/4
  <b, g'>4 g'16 fis g a
  <g b>4 r8 <b d>8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Set 2, Sonata II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  g'4 g16 f e d c4 r8 g'
  f4 f16 e d c
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Set 3, Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 3/8
  <c, f>4 f32 g a bes
  bes8 bes32 a g f f8
}
