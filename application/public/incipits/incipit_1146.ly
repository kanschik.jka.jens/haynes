 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Ouverture"
  \time 4/4
    r2 b4. g'8
    \grace g8 fis2 r4 r8. g16
    a2 r4 r8. fis16
    g4 fis e d
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Rondeau"
  \time 4/4
    r8 g'32 fis e16  g32 fis e16 g8  fis8 a32 g fis16  a32 g fis16 a8
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Menuet"
  \time 3/4
    e4 fis4. e16 fis
    g4 fis r
    e fis4. e16 fis g4 e r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Loure"
  \time 6/4
    \partial 4.
    fis8 g4
    g4. fis8 g4 g4. fis8 g4
    a8 g4 fis e16 fis g8 e r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "5. Air"
  \time 4/4
     e,1 b' e4 e e8 fis g4
     d1
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "6. Gavotte"
  \time 4/4
  \partial 2
  b4 e
  d g, fis c'
  \grace c8 b2 e,4 c'
  b e, dis a'
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "7. Gigue"
  \time 12/8
  \partial 8
  e,8
  g16 e g e g e  b' e, b' e, b' e,  e' b e b e b  g' e g e g e
}