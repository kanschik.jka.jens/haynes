\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran + Bass): Herr, dein Mitleid, dein Erbarmen [BWV 248iii/6]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key a\major
                      \tempo "Duetto"
                      \time 3/8
                      % Voice 1
                    cis8. d32 e d8
                    cis32 b a16 \grace a8 b4
                    a8. b32 cis b8
                    a32 gis fis16 \grace fis8 gis8 e~ e16 fis fis gis gis a
                    
                 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key a\major
                     \set Score.skipBars = ##t
                   R4.*2 cis,8. d32 e d8
                   cis32 b a16 \grace a8 b4
                   cis16 dis dis eis eis fis
               
                        
                
                  }
>>
}


\relative c'' {
	<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Sopran"
                     \key a\major
                      \tempo "Duetto"
                      \time 3/8
                      % Voice 1
                  \set Score.skipBars = ##t
                     R4.*16 cis8. d32 e d8
                     cis32 b a16 \grace a8 b4
                     a8. b32 cis b8
                     a32 gis fis16 \grace fis8 gis4
                      
                     
                  }
\new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Bass"
                     \key a\major
                     \set Score.skipBars = ##t
                        % Voice 2
                   R4.*18 cis,8. d32 e d8
                     cis32 b a16 \grace a8 b4
                     
                        
                
                  }
>>
}

	
	
