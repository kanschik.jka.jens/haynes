\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Vivace"
  \time 2/2
  bes2 bes,
  r4 bes'8 c d4 c8 bes
  f'2 f,
  r4 f'8 g a4 g8 f
  bes4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Grave [tutti]"
  \time 4/4
  g16. d32 bes16. d32 g,8 bes' a d, 4
  c'16. a32 fis16. a32 d,8 c' bes g, r
   }

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Grave [solo]"
  \time 4/4
  r2 d4. \times 4/6 {  c32[ bes a g fis g] }
  es'4 d8 fis g f16 es d8 c
  bes16 a g4
}
    
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro [tutti]"
  \time 3/8
  bes,8 d f
  bes16 c d4
  d16 es f4
  f8 es16 d c bes
  a8 g f
}

    
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro [solo]"
  \time 3/8
  bes4. c
  d8 bes f'
  es, g' bes
  g4 f8
}