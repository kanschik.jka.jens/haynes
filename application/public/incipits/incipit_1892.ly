\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Larghetto"
  \time 4/4
  \partial 8
  bes8
  f' d32 bes f' d bes'16 f \grace es8 d16 \grace c8 bes16
  g'8 f \times 2/3 { r16[ bes, c]} \times 2/3 {  d[ es f]}
  \times 2/3 {es16[ d c] } c8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/2
  \partial 4
  f4
  \times 2/3 { bes,8[ d f]} \times 2/3 { bes8[ f d]}
  bes4 bes' \grace bes8 a2. c,4
  \times 2/3 { f,8[ a c]} \times 2/3 { f8[ c a]} f4 es'
  \grace es8 d2.
  
  }

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Minuetto. Grazioso"
  \time 3/8
  bes4 d16 f
  es8 c d g,8. es'16 \grace d8 c16 \grace bes8 a16
  \grace a8 bes4.
}
