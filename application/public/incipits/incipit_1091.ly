\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 2/4
  \partial 8
  d,8
  g16 b b d d g32 fis e[ d c b]
  b8 a r b
  c16. d64 e d16[ c] b32 d c b a[ b a g]
  fis8 g r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
  d16 b g4 d'8
  e d g b,
  c b a16 e' d c
  \grace c16 b8 \grace a16 g8 r4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Minuetto"
  \time 3/4
  \partial 4
  a4
  d8. fis16 e8. g16 fis8. a16
  b4 a g
  fis8 e d4 cis
  \grace cis4 d2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga"
  \time 6/8
  \partial 8
  d8
  g fis e d c b
  a b g fis e d
  e fis g d c' b c a fis g4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Andante"
  \time 4/4
  a8 cis e a cis a gis fis 
  \grace { e16[ fis]} fis4 e r d
  cis16 a8. e'16 cis8. a'16 cis b a a gis fis e
  \grace {dis16[ e]} e4 dis r
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro Moderato"
  \time 2/4
  \partial 8
  e8
  cis a cis e
  a gis16 fis e8 d
  cis4 b a r8 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Giga"
  \time 6/8
  \partial 8
  cis8
  cis16 a8. e'8 e16 cis8. gis'8
  a8. gis16 fis8 e4 d8
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Minuetto"
  \time 3/4
  a8. cis16 cis8. e16 e8. a16
  e4 d cis
  \grace e16 d8 cis16 b a4 gis a e2
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro Moderato"
  \time 2/4
  \partial 8
  g8
  c \times 2/3 { e16[ d c]}
  g'8 \times 2/3 { g16[ f e]}
  c'8 \times 2/3 { c16[ b a]} g8. a16
  g16 c d, f e g c, e
  f d c b c8 f,
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Larghetto"
  \time 6/8
  \partial 8
  c8
  g'4 f8 e8. d16 c8
  d8. c16 b8 c4 a8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Giga"
  \time 6/8
  \partial 8
  g'8
  c g f
  e d c
  a'4. g
  c8 g f e d c d4. c
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andantino"
  \time 4/4
  f,8. g16 a8. bes16 c4 f8. c16
  d4 \grace { c16[ d]} c4 r  bes
  a8 c d bes a4 g \grace { f16[ g]}
  f2 r4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2.Allegro"
  \time 2/4
  f,8 f f 
  c' c c d16 e
  f8 e16 d
    c8 bes \grace bes8 a4 r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Minuetto"
  \time 3/4
  \partial 4
  c4
  d c f8 c bes4 a d
  \grace c8 bes4 a g f2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Giga"
  \time 6/8
  \partial 8
  a8
  d4 f8 e4 g8
  f4 a8 g a bes
  a g f e d cis
  d4.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro Moderato"
  \time 2/4
  g'4 b,8 g
  e8. c'16 b a g fis
  g'4 b,8 g
  e8. c'16 b a g fis
  \grace fis8 g4 r8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Amoroso"
  \time 3/4
  \partial 4
  d4
  c4. bes8 a g
  fis4 d fis g2 a4 bes a g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Pastoral"
  \time 6/8
  \partial 8
  d,8
  g8. fis16 g8 a8. g16 a8
  b8. a16 b8 c4 e8
  d8. e16 d8 d c b
  \grace b8 a4.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Minuetto"
  \time 3/4
  a'4 b g
  a8 g fis e d4
  e8 fis g4 b, \grace b4 a2 g4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro Moderato"
  \time 2/4
  d16 f bes a bes a g f
  g f es d es g f es
  \grace es16 d8 c16 bes \grace d16 c8 bes16 a bes4 r8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Cantabile"
  \time 4/4
  \partial 8
  f,8
  bes4 bes16 c d es f g a bes
  a g f es
  d4 \grace {bes16[ c] } c4 \grace { bes16[ c]} bes4 r8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Giga"
  \time 6/8
  \partial 8
  f,8
  bes d f f d a'
  bes4. bes8 a g f g es d4 c8 bes4. r8 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. "
  \time 3/8

}

