 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "1."
  \time 4/4
  g2 c4. g'8
  f e d e16 f e8 c g'4~
  g8 c, f16 e d c b c b8 c8. c16
  c4 d8 e c4. b8 c4 r r2
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2."
  \time 3/4
  e4 c8 d e4 d g, d' e d c g'2.~ g
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. "
  \time 4/4
   c4 es8 d d4 g8 d
   c d16 es d8. c16 b8. a16 g4
}
\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4."
  \time 6/8
  r8 g g c c c
  d d d e f16 e d c
  g'4 g8
}