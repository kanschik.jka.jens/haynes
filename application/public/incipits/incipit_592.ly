 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Adagio"
  \time 4/4
    e8 cis16. b32 a8 a' 
    fis e r \grace b'8 a16 \grace gis8 fis16
    \grace fis8 e8 \grace b'8 a16 \grace gis8 fis16
    dis b' \grace a8 gis16 \grace fis8 e16 
    \grace d8 cis4 r16
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
    d8 b16. a32 g8 g' 
    e d r g16 e
    d8 g16 e c a' fis d b4 r16
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "2. Allegro"
  \time 4/4
  a16 b cis d  e8 a, fis' e r cis
  d16 a e' a, fis' a, gis' a, a'8 a, r16
}
\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 4/4
  g16 a b c d8 g, e' d r b
  c16 g d' g, e' g, fis' g, g'8 g, r16
}

\relative c'' {
  \clef treble
  \key fis\minor	
   \tempo "3. Adagio"
  \time 3/4
  cis4 a16 fis a cis d4
  \grace fis,8 eis4 fis2
  d'4 cis fis,~
  fis fis4. eis8
}
\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Adagio"
  \time 3/4
  b4 g16 e g b c4
  dis,4 e2
  c'4 b e~
  e e4. dis8
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "4. Gavotta. Allegro"
  \time 2/4
    a4 b
    cis16 b a gis
    a8 e a a, b b'
    cis4 b8 b
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "5. Menuet. Allegro"
  \time 3/8
  a16 b cis8 b
  a16 e b' e, cis' e,
  b' cis d8 cis
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "5. Menuet. Allegro"
  \time 3/8
  g16 a b8 a
  g16 d a' d, b' d,
  a' b c8 b
  
}