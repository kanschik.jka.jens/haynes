 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Largo"
  \time 3/4
    g'4 as4. g16 as
    b,4 c r
    d g,8 b d8. f16
    f8 d es4 r
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key c\minor
                      \tempo "2. o.Bez."
                      \time 2/2
                      g'1 as2 b,~ b4 c d es
                      r e f2~ f4 es d g
                      es c8 d es4 f
                      % Voice 1
                  }
\new Staff { \clef "alto" 
                     \key c\minor
                        % Voice 2
                     c4 bes as g f d g g f es d c
                     c' bes as f b c2 b4
                     c1
                  }
>>
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Con Spirito"
  \time 4/4
  es4~ es16. d32 c16. b32 c8.[ g16] es8. c16
  g''4~ g16. f32 es16. d32 es8.[ g,16] c8. d16
  es8. es16 es16
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Andante"
  \time 4/4
  \partial 8
    g8 es' d4 c16 b c8 c4 d16 es
    f8 f4 es16 d es d c8 r es16 f
    g8 g4 as8 b, f'4 es16 d
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "5. Largo"
  \time 3/4
  es4 bes4. c8 g4. as8 bes4
  c d4. es8 d4. es8 f4
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "6. Presto"
  \time 4/4
  g'4 r as r f r g r
  c, r f8 g es f d4 g es8 d c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "7. o.Bez."
  \time 4/4
  g'2 b, c16 d e8 g,2 c4
  d16 e f8 g,2 f'4
  e16 f g8 e16 d c8 c,2
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "8. Minuetto"
  \time 3/4
  c,4 e g c8 b c d e f
  g4 c, a' \grace g8 f2 e4
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "9. Presto"
  \time 3/8
    \partial 8
    g'8
    es c g es c c'
    d16 es f8 d
    es c es
    f16 g as8 f
    g es g
}