\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 8
  c8
  f4. c8 g'4. c,8 a'4 f r8 f g a
  a bes bes4 r8 e, f g
  \grace a16 g8 f f4 r8
}

