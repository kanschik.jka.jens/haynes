 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Ouvertura"
  \time 2/2
  bes4. bes8 bes bes c d
  c4. c8 f4 bes,
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Gavotte"
  \time 2/2
  bes4. bes8 c4 f~
  f es2 d4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Menuet"
  \time 3/4
  f,4 bes bes
  c f, c'16 bes c8
  d4 bes8 d bes d
  es4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Bouree"
  \time 2/2
  \partial 2
  f4 bes bes a8 g f4 g~
  g f8 es d4 es4~
  es
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "5. Minuet"
  \time 3/4
  d8 es f4 f
  f es8 d c bes
  g' as g4 g g
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "6. Gigue"
  \time 6/8
  \partial 8
  f,8
  bes8. a16 bes8 c8. bes16 c8
  d4. r8 r
}
