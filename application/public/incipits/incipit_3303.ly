\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro moderato"
  \time 4/4
  fis2 a8 g fis e
  e d d4 r d
  e2 g8 fis e d
  d cis cis4 r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 2/2
  d1 d d2 c8 bes a g
  es'4 d8 c d4 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Rondeau"
  \time 2/2
  fis4. g8  a fis b a
  a g fis e e4. g8
  g fis e d d fis g b
  b2 a4 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 3/4
  g4. fis16 g a g c b
  e8 d d4 r8 d
  c'4. b16 a g fis e d c4 b r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Menuetto"
  \time 3/4
  g'2 a8 g
  fis4 fis fis e e e
  dis r r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Finale"
  \time 6/8
  \partial 8
  d8
  d b d c b a
  g4. g'8 fis e
  d e d \grace d8 c d c c4. b4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 6/8
  \partial 8
  g8 c e d c e d
  c4 e8 e4 g8
  \grace g8 f4 e8 \grace e8 d8 c d
  e4. c4 r8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 2/4
  \partial 8
  d8
  g4 g,8. g16 b8 a r8. a16
  c'4 c,8. c16
   \grace d8 c8 b r
}
  
  \relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Tempo di Menuetto"
  \time 3/4
  g'4 c, c
  cis8 d d2
  \grace { f16[ g a] } g4 d d dis8 e e2
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro moderato"
  \time 4/4
  c'4. a8 c a c a
  f4 \grace a16 g8. f32 g a4 r
  d,2 d8 f e g
  f2 f8 a g bes
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 4/4
  \partial 2
  d4 d d2 d8 es e f
  f4 c c c c2 es8 d d c c4 bes
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 2/4
  f8 f e e
  f8. e32 d c8 c
  d d cis cis d8. c32 bes a8 a8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante con variazioni"
  \time 2/4
  c4 b8 c
  a4 f'
  e8 g4 b,8 c4 r
  e d8 e c4 b8 e
  b4 a g r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Menuetto"
  \time 3/4
  e4 d e c2 f4
  e2 d4 c r r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondò"
  \time 2/4
  \partial 8
  c8 c b b d d4 c8 e
  e d d f f4 e8 r
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro"
  \time 4/4
  e2. d4
  d8 c c2 a4
  \grace c8 b4 a2 gis4
  a4. c16 b a4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 3/4
  g4 c e d g r
  c, f a g c r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Finale"
  \time 2/4
  a4 c b e e dis e r
  f2 e4 a,8 d c4 b a r
}


