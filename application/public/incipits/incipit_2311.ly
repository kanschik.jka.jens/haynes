\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Largo"
  \time 4/4
  es8 g16 as bes8 bes, es es, r4
  as8 c16 d es8 es, as r g r
  f8 d'16 es f4~ f8 es16 d es4~ es8 c d bes d4 c bes
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro [Tutti]"
  \time 2/2
  \partial 8
  bes'8
  g16[ as bes as] g f es d  es[ f g as]  g f es d
  es[ f g as]
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*10 
  r2 r4 r8 bes
  g16 es g bes f d f bes g es g bes es bes es g
  bes es, d es   c' es, d es d4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Andante [Tutti]"
  \time 4/4
  g'8 r g r   g r g r
  g r g r   f r f r
  es r es r d r d r
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Andante [Solo]"
  \time 4/4
  \set Score.skipBars = ##t
  R1*4
  r2 g'4~ g8 c, as'4~ as16  as g f es d c8 r g'
  as4 r8
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Allegro [Tutti]"
  \time 3/4
  es,8 es g g bes bes
  es16 es es es   es es es es  d d d d
  es4 es,16 es es es  bes' bes bes bes
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Allegro [Solo]"
  \time 3/4
  \set Score.skipBars = ##t
  R2.*15
  bes8 bes d d f f
  g16 es d es  c d bes c  a bes g a
  f g a bes   c d es f  es d es c
}