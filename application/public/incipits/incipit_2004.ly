\version "2.16.1"
         #(set-global-staff-size 14)

         
         \relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 2/2
    fis4. d8 a'4. d,8
    d4. e8 cis4. cis8
    d4. a8 c4. bes16 a
    bes4. bes8 bes4. c8
    a4.
}
         