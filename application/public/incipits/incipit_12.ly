 \version "2.16.1"
         #(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
  \tempo "1. o.Bez."
   \time 4/4
      g'2. fis8 g
      e2. d8 e
      c4. d16 e f4 f
      dis2 e
}

\relative c'' {
  \clef treble
  \key c\major
  \tempo "2. Andante"
   \time 3/4
  e8. f32 g  c,8 e \grace a8 g8 f16 e
  e8 d d4 e8 g
  f16 e f d c4. d8
  \grace d4 c2
}

\relative c'' {
  \clef treble
  \key c\major
  \tempo "3. Rondo"
   \time 2/4
    \partial 4
    g'8. a16
    a g \grace g8 f16 e d8 d
    d4 e8. f16
    g gis a e  f cis  d a 
    c b a g
}
