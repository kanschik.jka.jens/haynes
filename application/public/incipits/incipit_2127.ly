
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key es\major
  \tempo "1. Andantino"
  \time 4/4
  a16 bes8. r4 a16 bes8. a16 bes8.
 \grace bes16 as8 g16 f f2 g4
 \times 2/3 { c,8[ as' f] }  \times 2/3 { c'[ as f] } es4 d 
es8 bes c d es f g as a16 bes8. r4
 
 
}

\relative c''' {
  \clef treble
  \key es\major
   \tempo "2. Allegro assai"
  \time 3/4
  g2 bes4 g2 r4 r4 r4 bes4
 bes as g f2 as4 f2 r4 r4 r4 bes4
 as g f g2 bes4 g2 r4 
 
}
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Adagio ma non troppo"
  \time 4/4
  g4. as16 bes f4 bes
 g4. as16 bes bes,4 r4
 es'4. \grace {f32 [es d]} es16. f32 d4 g,
 c4. \grace {d32 [c bes]} c16. d32 bes4
 
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Menuetto Andante"
  \time 3/4
  \partial 4
 bes,4 \times 2/3 {es8 g bes} bes4. bes8
 bes as as4. as8 \times 2/3 {as8 bes c} c4 bes8. as16
 g16 as bes c bes4 r4
 bes4~ bes16 f g f f g as a bes4
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II "
    }
    \vspace #1.5
}


\relative c''' {
<<
\new Staff { \clef "treble"   
                     \key c\major
                      \tempo "1. Andantino "
                      \time 4/4
                      % Voice 1
\partial 4
r4 g2. f4 e g2 f4 e g c b a g8 a g4 r4
                  }
\new Staff { \clef "treble"
\key c\major
                        % Voice 2
g,4 \times 2/3 {c8 g g } g4 g16. e32 e16 d32 e \times 2/3 {f8 d b} \times 2/3  {c'8 g g} g4
                  }
>>
 
 
}

\relative c'' {
\clef "treble"   
                     \key c\major
                      \tempo "2. Allegro assai "
                      \time 4/4
c2 e4. f8 g8 e c'2 g8. e16 c2 f4. f8
a f c'2 a8. f16 c2 e4. e8 g e c'2 b8 a g
                  }
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Andante"
  \time 3/4
  f2 e4 f2 g4 a2 bes,8. c32 d d4 c4 r4
  
 
}

\relative c'' {
\clef treble
  \key c\major
   \tempo "4. Rondo Andantino"
  \time 2/4
 g'8. a32 g c16. e,32 g16. a32 g8 fis f4

 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III "
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andantino"
  \time 4/4
\partial 4
 a4 a8 fis fis4 a8 e e4

 
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Allegro con Spirito"
  \time 3/4
 d4 a fis d16 e fis e d4 r4
 
 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Rondo cantabile"
  \time 4/4
 a2 b8. cis16 b16. cis32 b16. cis32
  a2 b8. cis16 b16. cis32 b16. cis32 
 a4. gis16 a b8 a d cis fis e e4 r2
 
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Amoroso"
  \time 3/4
 a8 a a4. b8 \grace cis16 b8 a a4. d8 d a a4. b8 b a a4 r4

}

