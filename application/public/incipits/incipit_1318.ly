 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 6/8
  \partial 8
    d8
    a4 fis8 d4 fis8
    e d e b4 d'8
    a4 fis8 d4 fis8
    e d e a, fis' g 
    a4.~ a8 d fis
}

