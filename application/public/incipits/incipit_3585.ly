 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/2
  d2 a' fis4 d8. d16 a4 r r1 r2 r4 r8 a
  d2 e fis8 g a a
  \grace a8 g4 fis8 e
  \grace e8 d8 cis16 d d4 r2
}


\relative c'' {
  \clef treble
  \key a\major	
   \tempo "2. Andante amoroso"
   \time 3/4
   cis2 \grace fis16 e8 d16 cis
   b2 \grace e16 d8 cis16 b
   a4 a cis16 b a b a4 r
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Poco presto"
  \time 2/4
  \partial 8
  d8
  d4 r8 a a4 r8 d
  cis d fis d
  cis d a d
  fis, a d, fis a4 r8
}