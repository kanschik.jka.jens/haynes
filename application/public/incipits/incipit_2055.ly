 \version "2.16.1"
         #(set-global-staff-size 14)
         
     \relative c'' {
  \clef treble
  \key  c\major
 \tempo "1. Andante"
  \time 4/4
    r8 e e e  e d d4
    r8 f f f  f e e4
     }

     \relative c'' {
  \clef treble
  \key  c\major
 \tempo "2. Allegro"
  \time 4/4
      c4. c8  d d d c16 d
      d8 c16 d e8 e16 fis g8 d g4~
      g
     }

     \relative c'' {
  \clef treble
  \key  c\major
 \tempo "3. Largo e spiccato"
  \time 3/4
      d4 d d
      d2 r4
      d4 d d d2 r4
      fis fis fis
      g4. fis8 e4
     }

     \relative c'' {
  \clef treble
  \key  c\major
 \tempo "4. Allegro"
  \time 4/4
        g'8 e c e  d4 r8 g
        e c a d b4 r8 g'
        g f16 e f4 r8 e d4
        c
     }

