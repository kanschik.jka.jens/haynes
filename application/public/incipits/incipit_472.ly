\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 3/4
      c4 f c
      a g8 a f4
      c' f g
      e d8 e c4
}
