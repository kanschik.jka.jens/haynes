\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. [Allegro] [Tutti]"
  \time 4/4
   bes2 f
   g8 f16 es d8 es d es d es
   a g16 f e8 f e f e f
   bes4 f8 es d f c a'' bes4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. [Allegro] [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*17
   r2 d2
   e4. f16 g
   f8 e d b
   c2 d4. e16 f
   e8 d c a bes2
   c4. d16 es d8[ c bes \grace { g16[ a]} bes'8]
   es,4 d d8 c r4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [Tutti]"
  \time 2/4
  \partial 8
  g'16. d32
  bes8 bes4 a8
  g16 fis g4 d'16. g,32
  a8 a4 a16. c32
  c16 bes bes4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [Solo]"
  \time 2/4
  \partial 8
  r8
  \set Score.skipBars = ##t
  R2*5
  r4 r8 f
  \grace g8 \times 2/3 {f16[ es d] } d8~ d16 g f a,
  \grace c8 \times 2/3 { bes16[ a bes] } bes4
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro [Tutti]"
  \time 2/2
      bes4 bes, bes' bes bes bes, r bes''8 g
      g f bes g g f bes g
      \grace g8 f2.
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro [Solo]"
  \time 2/2
  \set Score.skipBars = ##t
  R1*49
    bes8 c d es  f g a bes
    a g f es  d c bes a
    bes c d es f g a bes
    a g f es  d c bes a
    bes4 f' d f
}
