\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
  bes'8. c16 bes8 as  \grace as8 g4. f8
  es d16 es \grace g8 f es16 d es8 bes16 bes bes4
}
