\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  f8 a16 g f8 e  d c bes a
  g4 a bes r
  g'8 bes16 a g8 f e d c bes a4 bes c r
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "2. Adagio"
  \time 2/2
  f4.. as16 c,8.. f32 as,8.. c32
  f,4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Menuetto con Variazioni"
  \time 3/4
  c2 e,4 f4. g8 a4
  d,8 fis g bes d, g
  f2 e4
}
