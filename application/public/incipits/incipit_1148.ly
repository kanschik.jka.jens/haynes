 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Ouverture"
  \time 4/4
    a'4 r r2
    cis,4 r r2
    d a8. a16 b4 fis
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Rondeau"
  \time 3/4
  \partial 4.
      cis8 d8. e16
      fis4 e d 
      e a,8 d e fis
      g4 fis e
      fis
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Tempo di Gavotte"
  \time 4/4
  \partial 2
  fis8 g e4
  fis8 g e4 g e
  fis e
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Bouree"
  \time 4/4
    \partial 4
    a'4 b a2 g8 fis e4 d2 e8 g
    fis4 e8 g fis4 d8 fis
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Menuet"
  \time 3/4
  d4 e fis
  \grace g8 fis4 \grace g8 fis4 \grace g8 fis4 
  a g fis
  e d8 cis b a
}
