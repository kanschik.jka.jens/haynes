\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 3/4
  c4 a f
  d'2.
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 3/4
  r4 c c
  a d2
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio"
  \time 4/4
  a'4~ a16 g f e  d4~ d16 e f g
  g f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 4/4
  \partial 4
  c4 f2 r4 c
  g'2 r c,
}

