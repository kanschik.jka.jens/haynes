\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 2
  r8 f,16 g f8 f
  c'4. a8 f c'16 d c8 c
  d4. bes8 f'4. d8 c4.
  

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondo, moderato"
  \time 2/4
    f8 f g g
    a a bes4
    a8 f e f 
    g g g r
}
