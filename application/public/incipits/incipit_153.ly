\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/4
  \set Score.skipBars = ##t
  \partial 8
  f8
  bes bes bes bes
  a16 bes a g f g f es
  d8 g16 f es d c bes
  a bes c bes a g f es
  d8 f'[ es d]
}
