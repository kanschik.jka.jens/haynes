 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
    f,2. d'8 bes
    es4. c8 a4 r8 f
    bes[ \grace { c32[ bes a]} bes8 d bes]
    g[ \grace { a32[ g fis]} g8 bes g]
    f4. es8 d4
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Andante"
  \time 6/8
    c8 a bes g4 a8
    f f f a4 g8
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Allegro con spirito"
  \time 2/4
  bes8 bes \grace c8 bes a16 bes
  f4. fis8 g g \grace a8 g8 fis16 g
  d4~ d16 bes c d
}
