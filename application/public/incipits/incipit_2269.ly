 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Ouverture"
  \time 2/2
  d2. c8 b a2. d4
  g,2. e'4
  d g c,4. b8 b2.
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Aria"
  \time 2/2
  d2 e2.
  d8 c d4 b c2.
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Menuet"
  \time 3/4
  d4 e8 fis g4
  c, a' c, b c8 d e4
  a,2
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Sarabande"
  \time 3/4
  \partial 4
  g'8. g16
  g4 g,8. d'16 c8. b16
  e4
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "5. Pastorella"
  \time 2/2
  \partial 4
  g'4~
  g a8 g fis4 a
  d,2 e fis4 g
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "6. Gigue"
  \time 6/8
  g4 r8 d'4 r8
  b4. b8 g' fis
  g d b e4 r8
}
