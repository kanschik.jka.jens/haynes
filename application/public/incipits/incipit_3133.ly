 
\version "2.16.1"
 
#(set-global-staff-size 14)
 \layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor) - Ich höre mitten in dem Leiden [BWV 38/3]" 
    }
    \vspace #1.5
}
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe I"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key a\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                     e8 f4 e16 d e8 a, gis a 
                     e'8. f32 g f4 d8 e4 d16 c
                     d8 g, fis g
               
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe II"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key a\minor
                        % Voice 2
                        c8 d4 c16 b c8 e, d e
                        bes' a~ a b16 c b8 c4 b16 a
                        b8 d, c d
                      
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key a\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
     R1*12 e8 f4 e16 d e8 a, gis a 
                     e'8. f32 g f4 d8 e4 d16 c
                     d8 g, fis g
    

}
