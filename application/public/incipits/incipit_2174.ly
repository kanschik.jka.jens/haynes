 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "6. Sans lenteur"
  \time 2/2
  r4 a d cis
  d c8 bes a bes g a
  f4 d f e
  f g8 e f4
}

