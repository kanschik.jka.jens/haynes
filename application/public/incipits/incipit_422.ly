\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. ohne Bez."
  \time 4/4
  es8 d16 c bes8 es~ es d16 c d d es f
  es4 r8 g8 f4 f16 f g as
  g8[ as32 g as16] bes8

}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 4/4
  es16 d es8 es16 d es8 es d16 c bes8 c
  as16 g f g as g as bes g f es f g f g es
  bes' a bes c d c d bes  
  

}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Siciliana"
  \time 12/8
  c8. d16 es8 es8. f16 g8
  \grace c,8 b2.
  c8. d16 es8 es8. f16 g8 as2.
  bes,8. c16 d8 d8. es16 f8 g4.~g4

}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Allegro assai"
  \time 3/4
  r8 r16 es16 g,4 as8. g32 as
  bes8. g16 es4 f8. es32 f
  g8 es r4 r4

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 3/4
  bes2.~
  bes2.~
  bes4 a4. g16 a
  bes8 a bes d c bes
  bes4 a r4

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro presto"
  \time 9/8
  f4. bes, g'
  a,8 bes c c d es es d c
  bes c d d c bes es f g
  f g a a bes c c bes a
  bes4. 

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Andante"
  \time 2/4
  d16 es es d d c c bes
  bes es es d d c c bes
  bes8[ d c bes]
  es d4 g8
  es d4 g16 f 
  es8[ d c bes] bes a4.

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Vivace"
  \time 3/4
  bes8[ f bes d,] bes'8. a32 bes
  c8[ a c f,] c'8. bes32 c
  d8 bes d f, d'4
  es8 c es a, es'4
  

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "5. Bourré"
  \time 2/4
  f4 g
  f16 es d es f8 bes,
  a[ bes c d]
  es d r4
  d4 es 
  d16 c bes c d8 d
  c[ bes a bes] a4 r4
  

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  f8 f,16 g a bes c d \grace f,8 e2
  d8 bes'~ bes16 e g bes, bes8 a16 bes c4~
  ~c8 bes16 a bes4~ bes8 a16 g a4~
  ~a

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 3/4
  f8[ f, a c] f8. e32 f
  g16 f e d c8 g c e
  g[ g, c e] g8. f32 g
  a16 g f e 

}
  
  \relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Sarabande"
  \time 3/4
  d4 a r8 r16 bes16
  f4 r4 r8 r16 g16 a4 bes8.[ a16 bes8. c16]
  a4 d2
  f,4 g8.[ f16 g8. a16] f2

}
  
  \relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Tempo di Bourre "
  \time 2/4
  c8 f, d'4
  c16 bes c d c8 f
  a,[ bes c d]
  bes a r4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Largo"
  \time 3/8
   a8 a d
   cis16 d e d e cis
   d8 f a 
   g16 d c bes a g
   c8 e g
   f16 c bes a g f
   bes8 d f

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Fuga Allegro"
  \time 4/4
  cis16 b cis d cis8 d cis4 d
  cis16 d cis d cis8 d cis4 r4
  r8 d8 d d cis16 b cis d cis d e cis
  d8 f f f

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Andante en Polonoise"
  \time 3/4
  f8 c a32 bes c16 f,8 bes16 a bes c
  a8 a f32 g a16 a8 g16 f g a 
  f8 a' f32 g a16 a8 g16 f g a f2

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro un poco"
  \time 12/8
  d8 f16 e d8~ 
  d a d d f16 e d8~ d a d
  e f g f g e f e f e f d
  c e16 d c8~
  ~c

}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "5. Gavotte"
  \time 2/2
  \partial 2
  d4 f
  e4. f8 g f g e
  f4 d a d
  cis4. d8 e d e cis 
  d4 a a' d,
  bes'4. a8 g f e d 
  \grace d8 cis2

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Largo"
  \time 3/4
  e4 e16 d c b a8 a'
  gis4 e r4
  e4 e16 d c b a8 a'
  f4 d r4
  

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Fuga Allegro"
  \time 4/4
  r8 a8 a a e'16 d e f e d c b
  c b c d c b c a b8 e, r8 e'8
  e16 d e f e d e c b8 gis b e~
  ~e8

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Aria en Sarabande"
  \time 3/4
  a4 a r8 r16 b16 
  c4 c r8 r16 d16
  e4 e r8 r16 gis,16 
  a4 e' d
  c4. b8 a

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Spirituoso"
  \time 3/4
  a4 a16 gis a b c b c d
  e8.[ f16 e8. f16 e8. f16]
  e4 a f 
  e a f
  e4 d8.[ c16 b8. d16]
  c4

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "5. Presto"
  \time 2/2
  a2 b
  c8 b c d e4 a,
  f' e d c
  b2 a a b
  c8 b c d e4 a,
  f' e d c 
  \grace c8 b1

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI. " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Siciliana"
  \time 12/8
  g4 d8 d8. e16 d8 d e d c4 b8
  g'4 d8 d8. e16 d8 e d c  b4 a8
  a8. b16 a8 d fis, g 

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Vivace"
  \time 3/4
  b4 a g8. a16
  fis2 g4~
  ~g8.[ fis16 g8. a16 b8. c16]
  fis,2 g4 r4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto assai"
  \time 6/8
  g4 b8 a g fis
  g4 a8 d,4. g8 g16 a b c d8 d, d'
  g8 g16 fis g a \grace g8 fis4.

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Affettuoso"
  \time 4/4
  d8 e4 d8 c2~
  ~c8 d16 e d8 c b16 c d e d e d e 
  d8 g~ g16 d e c d8 g~g16 fis e d e8 c c4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Menuet"
  \time 3/4
 d4 b c
 d8 c b c d4
 e d8 c b a 
 b4 g8 a b c
 d4 b c

}


