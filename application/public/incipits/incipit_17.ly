\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. o.Bez."
  \time 2/2
  \partial 8
  a8
  d a cis a  d cis16 bes a[ g f e]
  d4 e8. d32 e f4
}
