\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro e vivo"
  \time 4/4
    f4~ f16 d es c  bes c d es  f g a bes
    a8 r a4 a r
    f~ f8 g16 f es d c bes a g f es
    d8 r f'4 f r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. piu tasto allegretto"
  \time 2/4
  f32 e f8. e16 d c bes
  a8 g f d'
  c4 bes16. c32 a16. bes32
  g8. a16 bes c d e
  \grace g8 f4 e16 d c bes
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegretto"
  \time 3/8
  \partial 8
    f,8
    bes4 bes8 f'4 f8
    d4 d8 bes4 bes8 c4 c8
    c d bes es4 d8 d4 c8
}
