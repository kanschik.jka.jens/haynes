\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Sostenuto"
  \time 2/4
  fis,4. g8
  a b cis d
  \grace e16 d16 cis cis4 b8 a g fis e d r r4
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Minuet"
  \time 3/4
  fis2 g8 e d4 r r
  b'2 a8 b \grace a8 g4 r r
}
