 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Andante"
  \time 3/4
  d,8 fis fis fis  fis fis
  fis16 d' cis b fis' d cis b  b' d, cis b
  cis8 fis, fis fis fis fis
}


\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "2. Presto"
  \time 4/4
   fis8 b, b g' fis b, b g'
   fis16 e fis d  e d e cis d cis b4 g'8
   fis e fis d e d e cis d8 b r
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Aria. Vivace"
  \time 4/4
  fis,4 b a g  d g fis e
  d' a8 g fis4 r8 a
  d4 a8 g fis4 r8
}

\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "4. Presto"
  \time 12/8
    b4 fis8 b cis d cis4 fis,8 cis'8 d e
    d cis b fis' e d cis4 fis,4 fis4.
}