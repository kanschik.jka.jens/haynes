
\version "2.16.1"

#(set-global-staff-size 14)

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto "
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  c4 r4 c e c r4 e4 g e r4 g c
 g f e d c

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andantino"
  \time 2/4
   c4 a8. f'16
 c4 d16 c bes a 
 a g bes a c bes d c
 \grace c8 bes4 a


}

\relative c' {
  \clef treble
  \key c\major
   \tempo "3. Allegretto "
  \time 6/8
  \partial 8
  g8 c e g e g c
 g4. \grace a16 g8 f e 
 \grace e8 d4 c8 b4 c8
 d d d d4
   


}

