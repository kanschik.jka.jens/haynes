\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.S."
  \time 2/2
  \times 2/3 { c16[ b a] } g4 a8 a g r g 
  c32 g16. e'32 c16. g'8 e
}
