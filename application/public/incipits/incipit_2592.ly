 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Largo"
  \time 3/2
    a4. e8 a4. b8 cis4. d8
    b1 a2
}


\relative c'' {
  \clef treble
  \key a\major	
   \tempo "2. Allegro"
  \time 4/4
     e16 fis d e  cis d b d
     cis d b cis a8 e
     e'16 fis d e  cis d b d
     cis d b cis a8 b
     
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "3. Siciliano"
  \time 6/8
  \partial 8
    e,8
    e'8. fis16 e8 e4 e,8
    fis'8. gis16 fis8 fis4 fis,8
    e4 fis8 fis4 e8
    g4 fis8 e4 r8
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "4. Allegro"
  \time 3/4
  a8 b \grace cis16 b4. a16 b
  cis4 a8 b cis d
  e4 fis8 e d cis
  b a gis fis e4
}