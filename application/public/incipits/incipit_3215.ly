\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Saget, saget mir geschwinde [BWV 249/8]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key a\major
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
   a4 e cis16 a b cis d e fis gis
   a8 e cis' b16 a b8 e,
   d'16 cis d b
   cis b cis a e'8 d16 cis
 
 
                                                                                   

}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key a\major
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
 R1*12 a4 e cis16 a b cis d e fis gis
   a8 e cis' b16 a b8[ e,]
  
       
}



