\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  f,4 f8. f16 a4 c
  f2 g4 a
  bes4. a16 bes c8 bes a g
  f4
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*66 
  f,2 a4 c
  f4. e16 f g4 a
  bes4. a16 bes c8 bes a g
  f1
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Rondo. Allegretto"
  \time 2/4
    \partial 4
    c8 c
    c \grace e8 d16 c c8 \grace e8 d16 c
    c8. d32 e f8 f
    d d g g
    g32 c, d e f g a b
}
