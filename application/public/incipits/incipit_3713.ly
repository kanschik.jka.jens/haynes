 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. All. non tanto"
  \time 2/2
  \partial 4
  c4
  c8 f f2 g8 e
  f4. g16 f e8 f g a
  f a a2 bes8 g a4. bes16 a g8 a bes c
  a c r
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegretto"
  \time 6/8
     c4 c8 c a' f 
     f e g bes,4.
     a8 c c c a' f
     f e g bes,4.
}
