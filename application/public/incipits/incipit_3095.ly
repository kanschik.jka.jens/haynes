\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Nein, Gott ist allezeit geflissen [BWV 88/3]" 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key e\minor
   \tempo "o. Bez."
   \time 3/8
   \set Score.skipBars = ##t
    R4.*12 g8. fis32 e dis16[ e32 fis]
    e8. d32 c b16[ c32 d]
    c16 b32 a b16[ dis fis a,]
    g16 fis32 g a16[ g32 fis] e8
   
   
   
  
   
}

\relative c''' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key e\minor
   \tempo "o. Bez."
   \time 3/8
   g16 fis dis8 r8
   e8. d32 c b8
   c16 b a g fis g
   a g fis g e8
 
   
   
}



