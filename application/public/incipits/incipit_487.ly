\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata prima" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 3/4
  \partial 2
  d4 a fis'2 e4 d a' g
  fis g8 fis e d
  cis4 d a
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata seconda" 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4
  \partial 8
  r16 g16
  g4~ g16 b a g fis8 \grace e8 d e16 d c e
  d c b d  c b a c b8[ \grace a8 g]
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata terza" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Largo"
  \time 4/4
  g'16 fis g a  g a g a  b4. b8
  e,8 c'16 b a8. g16 fis4. fis8 g
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata quarta" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 3/4
  \partial 4
  a4
  b8 a b cis d e
  cis4 \grace b8 a4 d
  e8 d e fis g a
  fis e d e fis g
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata quinta " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4
   g4~ g16 g b c  d4~ d16 d e fis
   g8 g, r g'16 fis e8 d c b a4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata sesta" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Andante"
  \time 3/4
  \partial 4
  a4 e'2 d4
  cis8 d cis b cis a
  d4 cis b cis \grace b8 a4
}
