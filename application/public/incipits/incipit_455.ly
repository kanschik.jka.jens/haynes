
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro "
  \time 2/4
 d4 g
 g4. fis8
e d c b
a g d' d
b g d' d 
b g b b
 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Adagio "
  \time 3/2
 e4. g8 fis4. e8 dis4. fis8
e2 b b 
b a4. g8 fis4. g16 a
g2 \grace fis8 e2

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro "
  \time 3/8
 b8 b c
d b16 c d8
r8 d8 c
d b16 c d8

 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allegro"
  \time 3/4
e8 d16 c b a g fis e8 c'
c4 b4. e8
fis, c' b a g fis
g4 e 
 
 
} 

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Adagio"
  \time 2/2
b4. c8 c4. b16 c
d4. g8 d4. f8
e4. d8 c4. b16 c
b2
 
} 

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "3. Allegro"
  \time 2/4
\partial 8
g8 g fis b, dis
e b e, b'
b a a g16 fis 
g8
e4 
 
} 


 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
d16 e fis g a8 a,
d16 e fis g a8 a,
b g' g fis16 e fis8 d
 

 
} 

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Largo"
  \time 6/4
\partial 2.
fis4. g8 a4
a4. g8 fis4 g e4. d16 e
 fis4. e8 d4 fis4. g8 a4
a4. g8 fis4
 
 
} 

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 3/8
d16 e d8 d
e16 fis e8 e
fis e d 
a'4.
cis,16 d e4
cis16 d e4

 
 
} 

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV. "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 2/4
\partial 8
d8 g a bes c
d e fis g
bes,4 a
g8 d' g16 a g f
  
} 

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio "
  \time 3/4
\set Score.skipBars = ##t
   R2.*2
f8. bes,16 d8. f16 bes,8. f'16
g8. a32 bes es,4. d16 es
d4. es8 f4~ f 


  
} 

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro "
  \time 6/8
\partial 8
g8 d' es c d4 g8
fis4 e8 d4 r8
 
 
}

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V. "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key a\minor
   \tempo "1. Vivace"
  \time 3/4
\partial 4
e4 a8 c e c a c 
b f' e b e, d'
c4 \grace b8 a4 a'~ a8 b16 a gis a gis fis e d c b 
a8
  
} 

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Largo"
  \time 2/2
r4 e8 d e4 g
r4 d8 c d4 g
r4 c,8 b c e d c
b4 g

} 

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro"
  \time 2/4
a4 e'
e8. f16 d8. e16
c8. e16 d8. f16 
e4 a,
  
} 

