\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "Adagio"
  \time 6/8
  c4 es8 g r g~
  g f es f32 es d es d8 r
  d4 f8 as r f~
  f es d d16. es32 c8 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "Rondo"
  \time 2/2
  e4. f8 a g f e
  e f cis d d4 r
  f4. g8 b a g f f g dis e e g a b
  c4.
}

