\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Vivace"
  \time 4/4
  \partial 8
  g'8
  c, a' g8. f16 e4 r8 d
  e f d8. c16 b8 a16 g g'4~
  g8 f16 e f4~ f8 e16 d e4~
  e8 f16 e d8. c16 b4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 3/4
  g'8. a16 g8. f16 e8. d16
  c8. b16 c8. d16 e8. f16
  g4 a8. g16 f8. e16
  d2 c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Vivace"
  \time 3/4
  \partial 4
  g4 c c d
  e e f g f8 e d c
  b2 d4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 6/8
  \partial 8
  g8
  c g c d g, d'
  e g, e' d g, d'
  e g, e' f g e d4.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Vivace"
  \time 4/4
  \partial 8
  r16 d
  d4 r16 d e fis  g b, c d e  a, b c
  b8 a16 g r a b cis d8 e cis8. d16
  d4 r16
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Corrente"
  \time 3/4
  \partial 8
  r16 g
  g4 b8 c c8. b32 c
  d4. r16 d e8. fis16
  g8. fis16 g8. b16 a8. g16
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Ciaccona"
  \time 3/4
  r4 b8 c16 d c8. b16
  a4. a8 d8. fis,16
  g4. b8 a8. g16
  fis8. e16 fis8. g16 a8 b16 c
  b4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Vivace"
  \time 3/4
  r4 bes f
  bes2 bes4
  c c4. bes16 c
  d4. c8 bes4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 4/4
  d4. d8 d4. d8
  es16 d c bes a8 a32 bes c16  bes8 a16 g r8 bes
  c4~ c16 c f es d8 c16 bes r8
}
  
  \relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Vivace"
  \time 4/4
  f,4 bes2 c4~ c d8 es d4 c8 bes
  f'4 g8 f f4 g8 f
  es d c bes a2
}
  

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4
  c4. c16 d32 c c4. c8
  c8. d16 b8. c16 c4 r8 es
  f g as bes g f16 es r8
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Vivace"
  \time 4/4
  c8 g' g g as g r g
  f es d16 es f d es8 d16 c r8
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Piu Vivace"
  \time 3/4
  \partial 4
  g4
  c4. es8 d c
  b4 a8 g c4
  c4. d8 b4
  c2
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Allegro"
  \time 4/4
  c8. d16 es8. f16 d8. g16 g8. f16
  es8. d16 c8. d16 b4 g
  es'8. d16 c8. bes16 a8. fis16 g8. a16
  a4. g8 g2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Vivace"
  \time 4/4
  \partial 8
  d8
  bes a16 g d'8 g fis4 r8 g16 d
  es g c, es a, c f c d8 c16 bes r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Largo"
  \time 3/4
  \partial 4
  d4
  g,4. a8 bes4
  fis g a 
  bes a g d'2 d4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 6/8
  g16 a bes8 g d'4.
  g16 a bes8 g fis16 g a8 d,
  es16 f g8 c, d16 es f8 bes,
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Vivace"
  \time 4/4
  d,4 g a bes8 c
  bes4. a8 bes4 a8 g
  es'4 d c bes a8 bes g a fis2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  a8
  d e f g a f bes a
  g f e16 f d e cis8 a r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Vivace"
  \time 3/4
  \partial 4
  a'4
  f4. g8 a4
  cis, b8 a d4
  d4. e8 cis4
  d2 a4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro"
  \time 4/4
  \partial 8
  a8
  d d d d e a, e' a,
  f' a, f' e16 d e8 a, r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Andante"
  \time 3/8

}

