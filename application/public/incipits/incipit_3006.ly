\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "Andante "
  \time 3/2
  r2 c1~
  c4 bes a g f a
  bes a a2 r4 c
  d4. c8 c4. bes8 bes4. a8
  a4 g g2
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria II " 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\minor
                      \tempo "Un poco andante"
                      \time 4/4
                       r2 e~ e4 d8 f g2~
                       g4 f8 a bes2~ bes4 a g4. g8 f4
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key d\minor
                        % Voice 2
                     d2. cis8 e
                     f2. e8 g a2. g8 f e g f2 e8. e16 f4
                  }
>>
}

\pageBreak

\markuplist {
    \fontsize #3
    "Aria III " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "Andante"
  \time 3/2
  r2 r f,
  c'2. bes4 a2 d bes2. bes4
  a2.
}
