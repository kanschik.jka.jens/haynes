\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 2/4
    f,4 f f f
    f16 g a bes c8 d
    bes a r4
}
