\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegretto"
  \time 2/4
  \partial 8
  c8
  g'8. f32 g a16 g f e
  e8. d32 e f16 e d c
  c4 \grace d'8 c8 b16 a
  g8 f  e r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondeau"
  \time 6/8
  \partial 8
  g8
  c4. c8 d e
  f4. d4 r8
  e4. g4 b,8
  b4. c4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Moderato"
  \time 2/2
  g'4 g,16 fis g a g4 a
  a2 b4 r
  b' b,16 a b c b4 c
  c2 d4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Menuetto Grazioso"
  \time 3/4
  b2 \times 2/3 { a8[ c fis,] }
  \times 2/3 { g8[ b d] } d2
    b2 \times 2/3 { a8[ c fis,] }
  \times 2/3 { g8[ b d] } d2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 2/4
  bes'4 f8. d16
  bes8 f r f'16 d
  b c c4 g'32 f es d
  cis[ d d8]
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegretto"
  \time 2/2
  \partial 4
  bes4 d8 f f4 f f
  \times 2/3 { bes8[ a g] } f2 d4
  f8 es es4 es es es
  cis8 d d2
}
  



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 3/4
  f2. \grace bes8 a8 g16 f f2
  \grace es8 d4. f16 e \grace g8 f8 e16 d
  d4 c8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegretto"
  \time 2/4
  \partial 8
  c16. d32
  c8 f16. g32 f8 a
  a8. fis16 g8 c,16. d32
  c8 g'16. a32 g8 bes
  bes8. gis16 a8 r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  c1~
  c16 c' c4 b16 a a g g4 r8
  d4 f16 d f d e4 g16 e g e
  g f e4 d8 c4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegretto"
  \time 2/4
  c4 d8. e16 f8 f4 e16 d
  e4 f8. g16 a8 a4 g16 f
  g8 c4 g8
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 4/4
  \partial 8
  a8
  d4. e8 fis4 fis
  fis4. \times 2/3 { g16[ fis e] } d4 r8 a
  fis'4. g8 a4 a
  a4. \times 2/3 { b16[ a g] } fis4 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Tempo di Minuetto"
  \time 3/4
  d4. fis16 e32 fis g8 e
  a8. fis16 d4 a
  d4. fis16 e32 fis g8 e
  a8. fis16 d8 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Andante"
  \time 3/8

}

