\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Gold und Ophir ist zu schlecht [BWV 65/4]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe da Caccia 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key e\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                   r4 r8 b16 fis g8 dis e fis
                   b,4 r16 g'16 a fis dis e fis c' b8. a16
                   g  b a c
                   b g e g
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe da Caccia 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                     r2 r8 b16 fis g8 dis e fis
                   b,4 r16 g'16 a fis dis8. e16
                   e g fis a g4~ g
                       
                        
                
                  }
>>
}


\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key e\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    R1*6 r4 r8 b16 fis g8 dis e fis
    b,4 r4 r2
    r4 
    
    
    }