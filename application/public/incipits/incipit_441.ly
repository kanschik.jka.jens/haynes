
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Iere Sonate "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key a\minor
  \tempo "1. Gracieusement"
  \time 3/4
  \partial 4
 e4 a4. c8 b d c4 \grace b8 a4 b c4. e8 d f e4 \grace d8 c4 
 
}

\relative c' {
  \clef treble
  \key a\minor
   \tempo "2. Allemande"
  \time 4/4
 \partial 8
 r8 r8 e8 a c b e4 d8 c a'4 g8 fis b4 a8 gis fis e d c8. b16 b8. a16 a8 
 
}
 
\relative c' {
  \clef treble
  \key a\minor
   \tempo "3. Rondeau"
  \time 6/4
 \partial 4
 e4 a2 b4 c2 d4 e2 a,4 a2 e'4 e f e e f e c2. \grace b8 a2 e4
 
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Rondeau"
  \time 2/4
 r8 a8 a a b c16 d e8 e c a e'4~ e8 d16 c d4~d8 c16 b c4~ 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe Sonate "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Gravement"
  \time 4/4
 \partial 8
 r16 d16 d4~ d16 a d fis e a, cis e a, g' fis e fis8 e16 d cis8. d16 d e fis g a8 a cis, \grace b8 a8 
 
 
 
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Courante"
  \time 3/4
 \partial 4
 r8 d8 d4. a'8 g fis e4 a a, r8 a'8 fis a d, a' b4 d d,

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Lentement"
  \time 4/4
 \set Score.skipBars = ##t
   R1*1 fis8. e16 fis8. g16 g8. fis16 fis8. g16 e8. d16 e8. fis16 d8. cis16 d8. e16  cis4  
 
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Gigue"
  \time 6/8
 \partial 8
 a8 d g fis e a g fis e d a4 cis8 d e fis g e g a4.~ a4
 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIIe Sonate "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Gravement"
  \time 2/4
 b4 d8. e16 fis4 fis, b8. ais16 b8. cis16 ais4 \grace gis8 fis4
  
 
 
}
 
\relative c''' {
  \clef treble
  \key b\minor
   \tempo "2. Gracieusement"
  \time 3/8
\partial 8
 r16 b16 b4 fis8 g \grace fis cis8 fis \grace e8 d8 \grace cis8 b8 fis' g16 fis e g fis e d cis b8 cis d16 e e4 fis
  
 
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Lentement"
  \time 3/2
 e2 fis g cis, fis4. e8 d4. cis8 d4. e8 cis2. b4 g'2 g g g 
 
 
 
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "4. Gavotte"
  \time 2/4
 fis4 b ais4. gis8 fis8. g16 e8. fis16 d4 cis 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IVe Sonate "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allemande"
  \time 4/4
 \partial 4
 r16 d16 e fis g8 b,16 a g8 g' a fis,16 e d8 a'' b a g b a d, 
 
 
 
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Musette"
  \time 2/2
 \partial 2
 d4 c b a g4. fis8 g4 d d' c b a g4. fis8 g2

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Moderement"
  \time 6/8
 \partial 8
 d8 g4.~ g4 b,8 e4.~ e8 f e d4.~ d4 d8 d e d c d b a4
 
 
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Lentement"
  \time 2/2
 r4 e4 g \grace fis8 e4 a,4. c8 b4. a8 g4 \grace fis8 e4 

 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "5. Gavotte"
  \time 2/4
 g4 d g,8. fis16 g8. a16 b8. c16 a8. c16 b8. a16 g4
 
 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ve Sonate "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Legerement"
  \time 2/2
 \partial 2.
 e8 fis g a a g16 a b4 fis8 g a4 g8 fis g4
 
 
 
}
 
\relative c''' {
  \clef treble
  \key e\minor
   \tempo "2. Rondeau "
  \time 3/4
 g4 \grace fis 8 e4 b' b4. a8 b4 g4. fis8 g4 fis2
 
}
		

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gayment"
  \time 4/4
  \partial 8
e16 fis g8 e e g fis b b fis16 g a8 fis fis a g e e g16 a b8
 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Gravement"
  \time 4/4
 \partial 8
 e8 cis16 d e8 a, a' gis \grace fis8 e8 r8 a16 e
 fis8 e16 fis d8 cis16 d cis8 \grace b8 a8
 
 
 
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Legerement"
  \time 4/4
  \partial 8
 e8 a a, r8 e' gis, b e, e' a a, r8 
 
}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "3. Gavotte"
  \time 2/2
  \partial 2
 a4 gis a e fis gis a e cis8 d e4 cis8 d e4 b8 cis d e cis4 \grace b8 fis4 
 
}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "4. Sarabande"
  \time 3/4
  a8 e cis a a' e
 b gis e e' b' e,
 cis' a e
 cis' b a gis4. fis8 e4
 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "5. Gayment"
  \time 3/8
  cis16 d e8 e e d16 cis d8 d e16 d cis b cis b cis d e8 
 
}
