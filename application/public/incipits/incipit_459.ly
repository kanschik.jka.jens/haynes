
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio "
  \time 4/4
 r4 d8. d16 es8. es16 es8 d16 es
d4 g2 fis8. es32 fis
g8. a32 bes a8. g16 fis8[ d]
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Presto "
  \time 2/4
 g2
a
bes8 g d'4~ d c
d8 a d4~ d8 g, c a fis d'
 
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio "
  \time 4/4
 \partial 8
r8 r2 r4 g4~ g8. g16 g8. fis16 g8 d g8. a16 fis8[ d]

 
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro "
  \time 3/8
 r8 g8 g
d d' d
c c a 
bes bes4
a8 d4~ d8 c4~ c8 bes4
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Vivace"
  \time 3/4
\partial 4
d4 b g e'
d2 g4~ g fis2 
g4 b, g a
 
 
} 

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
g8 g g g c4. d16 c
b8 b b e a,4. g16 a
b8 g d'2 cis4 d4
 
} 

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
  \time 3/2
 \set Score.skipBars = ##t
   R1.*2
 r2 b2 d 
cis e cis dis b e~ e dis2. e4 e1  

} 

\relative c''' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 2/2
\partial 2
g4 d
e c a d
g, a b c d c8 b c2~ c4 b8 a b4


} 

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Andante"
  \time 4/4
e4 c' fis, b16 c a b
g8 e b'4. a16 g a4~ a8 g16 fis g4. fis16 e fis4~ fis8[ b,]

 

 
} 

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Presto"
  \time 2/4
b2
e
g
fis4. b,8
e4. fis8
dis b' a g
fis g a4~ a8 g16 fis g8. a16 fis4.
 
 
} 

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Adagio"
  \time 3/2
r2 e2 e
dis d d 
cis c c 
b b d 
cis cis

 
 
} 

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 6/8
e4 e8 e dis e
fis4. b,4.
e4 e8 e dis e 
dis cis b r8 r8 
 
} 

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Vivace"
  \time 3/4
\partial 4
d4 e8 e e fis16 g a8 a
d, d d e16 fis g8 g 
g2 fis4
  
} 

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Presto "
  \time 2/4
d8 d d d 
e e e d16 e
fis8 fis fis e16 fis
g4 fis4~ fis e~ e


  
} 

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Largo "
  \time 3/2
r2 b2 fis
d' d cis
b b b 
ais ais b
b b b a a a 
g g1 fis2
 
 
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "4. Presto "
  \time 3/8
r8 a8 a
b e, e a d,16 e fis8
g16 a g fis e g 
fis8. e16 d8~ d d8. cis16 d8

 
 
}


 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Andante"
  \time 3/4
\partial 4
d4 f e d 
cis a f'
a g f e2 f4
e4 e2
  
} 

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Presto"
  \time 2/4
d2
g
f4 bes
e, a
d, d'~ d cis
d bes~ bes a~ a g~g f e

} 

\relative c''' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio"
  \time 4/4
a4 c f,4. a8
d,4. d'8 c4. c8
c8.[ f,16 f8. e16] f4 c'~ c bes2 a4 g2 f
  
} 

\relative c''' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro "
  \time 2/2
\partial 2
a4 bes~ bes a2 g4
f d f a~ a g2 f4
e2 a4 g8 f
e4 f d e a, a2
    
} 


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI. "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 3/4
\partial 4
a4~ a g8 f g e 
f4. e8 f d 
e f e d e c 
d4 \grace c8 b4
  
} 

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro"
  \time 4/4
e4 e e d16 e f d 
e8 d c b  c4 c
b4. b8 c b a gis 
a c b a gis[ b] 
  
} 

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Largo "
  \time 4/4
\partial 4
c8 d16 e
f8 e16 f d8 c16 d e8 c g'4~ g8 f16 e f4. g16 f e8 f16 g a4
    
} 

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Allegro "
  \time 3/8
r8 e4
a,8 a'4
b,8 e16 d c b 
c8 a a'
gis4 gis8 a b4
    
} 


