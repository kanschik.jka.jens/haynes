\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 4/4
      es,16 es es es   bes' bes bes bes  c c c c  d d d d
      es8 bes g es  es' bes g es
      bes'16 bes bes bes  f' f f f   g g g g as as as as
      bes8 f d bes
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio"
  \time 3/2
     \set Score.skipBars = ##t
   R1.*8 
    g'1. g2 d1
    es2 c as'
    f d4. es8 f2
    f b, d
    es2. f4 g8 as bes g
    e2 g e
    bes1.
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro"
  \time 3/8
      es,8 bes'4
      es16 f f4
      g16 as bes8 bes
      bes as g
      f as,16 g as8
      g bes16 as g f
      es8 bes'4
      es16 f f4
}
