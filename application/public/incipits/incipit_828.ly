 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 4/4
    f4. f8 e d c bes
    a g16. a32 bes8 a a g r4
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 3/8
    f8 f f f e r
       \set Score.skipBars = ##t
       R4.*2
       f8 f f f e g
       f g a g f e f4
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Largo"
  \time 3/4
     \set Score.skipBars = ##t
     R2.*8
     r4 a4. g16 f
     e4 d2 bes'4 a2
     d4 cis2 f4 e d cis d2
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allegro"
  \time 4/4
      f2 e4 es
      d4. c16 bes c8 d16 e f8 f,
      bes d c bes a a' g f
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "5. Allegro"
  \time 12/8
    \partial 8
    f8
    c f c  bes f' bes, a4 r8 r r f'
    c f c  bes f' bes, a4 r8
}