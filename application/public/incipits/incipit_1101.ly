 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Largo"
  \time 3/4
       \set Score.skipBars = ##t
   R2.*4
   r4 a g fis8 g a4 b a d, g
   fis8 e g fis e d d4
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Allegro"
  \time 2/4
   \set Score.skipBars = ##t
   R2*6
   a8 cis b d cis16 b a4 b8
   cis16 d e4 d8
   cis16 b a4 b8
   cis16 d e fis e8 d cis4
}

\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "3. Arioso"
  \time 3/4
  b8. cis16 b4 fis
  cis'8. d16 cis4 fis,
  d' cis ais
  b8 ais b4 r
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4. Gigue"
  \time 6/8
       \set Score.skipBars = ##t
     R2.*2
     a8 cis d  g, cis d
     fis, a d d cis16 b a g
     fis4. r
}	