 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Largo [tutti]"
  \time 2/2
  bes8 bes bes c16. d32 es8 es, r es'
  as d,16. f32 d8 as' as g r16 g f16. es32
}
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Largo [solo]"
  \time 2/2
  r2 es
  es8 d16 c bes8. as16 g8 es r bes'
  c bes16 c as8. g16 g4 r8
}
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro [tutti]"
  \time 2/2
  bes2 es4. es8
  d c bes as g es bes'4
  bes as2 g4
}
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro [solo]"
  \time 2/2
  r2 r4 r8 bes
  c d16 es f8 es d bes es4
  es d es r
}
 
 \relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Largo"
  \time 3/4
  c2. c2 g'4
  as8 f es4. d8
  es4. d8 c4
}
 
 \relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Vivace"
  \time 3/8
  es8 as, g
  as16 bes c8 bes
  d, es as
  as g f
 }
 