\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude. Legerement"
  \time 3/4
  r4 g'8 f e d
  e c d e f d
  e c d e f g
  a g f g a b
  c4 c4.

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Piece depetits Coups"
  \time 2/4
      c8 e e c d4
      g, d'8 g g f e4 c

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Premier Menuet"
  \time 3/4
    c8 d es f g as
    g f es d es c

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Second Menuet"
  \time 3/4
    g'4 a8 b c4
    g8 c f, d' e,4
    e f8 g a4 g4. f8 e c

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "5. Troisieme Menuet"
  \time 3/4
    g'4 es f g c, d es f8 es d c d2 g,4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "6.Allemande "
  \time 2/4
    \partial 16
    e16
    e4 f16 e d c
    f4 r8 r16 f
    f4 g16 f e d
    g4

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "7. Sarabande"
  \time 3/4
    es4 f16 es8. d16 c8.
    b2 \grace a8 g4
    f' g4. as8
    f2 d4

}

