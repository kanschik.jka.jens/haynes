\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro non molto [Tutti]"
  \time 4/4
  c1~ c2~ c4~ c16 b32 a g f e d
  c4 r r2
  g''1~ g2~ g4~ g16 fis32 e d c b a 
  g4 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro non molto [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*17 
  c2~ c4~ c32 e g e  c[ e g e]
  c4 r d2~
  d4~ d32 f a f d[ f a f] d4 r
  
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Larghetto [Tutti]"
  \time 4/4
  g'4 r8 g fis4 r8 fis
  e4 r8 e dis4 r
  g,4 r8 g fis4 r8 fis
  e4 r8 e8 dis4 r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Larghetto [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*8 
    r4 r8 e g8. fis32 e b'8 b, 
    c b r e a \grace g8 fis8 \grace e8 dis8 e16 fis 
    g fis e8 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Tutti]"
  \time 2/4
      c4 b r8 g f e
      a4 g r8 e d c
      f4 e
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Solo]"
  \time 2/4
  \set Score.skipBars = ##t
   R2*27 
   c8 g' e d16 c
   d8 f d c16 b
   f'8 a f e16 d
   e8 c b a16 g
}