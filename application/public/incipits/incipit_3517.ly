\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
      c2 e g r4 e8. g16
      f8 r d8. f16 e8 r c8. e16
      d4 b g r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andantino con Variazioni"
  \time 2/4
  \partial 8
  g8
  c c e e
  d8. b16 c8 g16[ c]
  e8 e g g f8. d16 e8 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Allegretto"
  \time 2/4
  \partial 8
  g8
  c c  \grace d16 c16 b c e
  g4. e16 c
  f e d c  b c d b
  \grace d16 c16 b c e c8
}
