\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Ouverture"
  \time 4/4
      c4. c8 f4 g8 a
      g4 c, d e8 f
      e4. e8 e4 f8 g
      f4 g8 a g4 a8 bes
      a4 f
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Aria"
  \time 4/4
  f,4. a8 c4 a
  a2 g
  f4. g8 a4 bes
  g4. f8 e f g4
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Menuett"
  \time 3/4
      c4 a d
      bes2.
      c4 bes a g2 a4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Aria"
  \time 2/2
    a4. f8 a4 c
    f e8 d c d bes c
    a4. bes8 c4 d
    bes4. a8 g f e4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. Menuett"
  \time 3/4
    f4 c f
    e2.
    f4 g a
    g g8 a g a
    bes4 a g
    a2 a4	
}

