\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro con spirito"
  \time 4/4
   f,4. e8 f[ g a bes]
   c[ d e f] c4 r
   a' a bes4. g16 e
   f2 c4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Menuetto"
  \time 3/4
  \partial 4
  a8 c
  f4 r c8 f
  a4 r f8 a
  c4 c c
  a f r
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Romance. Adagio"
  \time 2/4
  d4 es8. g16
  f8. es16 d8 r
  f8. fis16 g8. es16
  d4 c8 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegretto"
  \time 2/4
  a'2 g
  \grace g8 f8 e f a
  g4 r
}
