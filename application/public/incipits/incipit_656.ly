
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite Ire."
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Rondeau"
  \time 4/4
\partial 2
e8 f g f 
e4 d e8 f g c,
b4 g c8 e b c 
d e d e f e d c 
d2

 
} 

 

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite II "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Canon. Gracieusement"
  \time 3/4
r4 e8 d e d 
c4 b c 
g g'8 f g f 
e4 f8 d e c 
b4 g g 
c g c

 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite III"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. La Paysanne. Gayment"
  \time 4/4
\partial 2
g8 a b c
d b c d e4 c
d g fis g
a8 g fis e d c b a 
b c b a g a b c 
d b c d e4 c 


}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IV."
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Courante"
  \time 3/4
\partial 8
c8 c g c d e f 
e d e f g a 
g4 g g 
g f8 e d c 
b4 c d 
g,8 a b c d e f 

}





