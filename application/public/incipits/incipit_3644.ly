 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Adagio"
  \time 4/4
   d2 d d cis4 b
   \grace b4 cis1
   b2 b ais d cis fis b, e
}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
  b32 d16.
  \grace d8 cis4 ais8 ais32 cis16.
  b16 ais b4 e32 g16. 
  
}
