\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4
  g8 e'16 c d8 fis g g, r e'16 c
  d e d b c d c a b8 g r
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
  d8 b16 c d8 e
  d[ b c a]
  b g16 a b8 c
  b[ g a fis]
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
  \time 3/4
    d2.~ d2. g4 e4. a8
    fis2
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 3/8
  d8 c4
  b16 g d' b g'8
  d c4
  b16 g d' b g'8
  g, a4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Largo"
  \time 4/4
  d8 fis16 g a8 e fis a d, cis
  d8 fis16 g a8 e fis a d, cis
  d8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  r8 a' d, fis e16 fis g fis e8 a
  fis d b' b b e, a a
  a d, g g g16 a fis g e4
  d8
}
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Largo"
  \time 6/8
  fis8. g16 fis8 g ais,4
  b8. cis16 d8 cis fis e
  d b d b gis cis
  cis4
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Allegro"
  \time 3/8
    r8 fis fis
    e a16 g fis e
    d8 d d
    cis fis16 e d cis
    b8 b b
    a16 b cis a d8~
    d16 e cis4 d8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 3/4
  r8 d g fis g a
  fis4 fis2 g8 a b a b c
  a4 a2
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  d8
  e g16 fis g8 e d g16 fis g8 d
  e d c4 b16 c b a g8 a
  b
}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Vivace"
  \time 3/4
    r4 d g
    fis2 b4
    a d, g
    fis2 \grace e8 d4
  }
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 3/8
  b16 c d4
  c16 d c b c a
  b c d4
  fis,4 \grace e8 d8
  }

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Andante"
  \time 3/4
  r4 b' e, c'8 b a g fis e
  dis4 b e~ e8 fis dis fis b a
  g4 e
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  g16 a b8 b b b g e b' e
  dis b fis' b g e r
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Aria 1. Gracioso"
  \time 3/4
  e2 fis4
  g dis e
  c' b a b dis, e
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Presto"
  \time 2/4
  r8 r16 b'16 g8. e16
  b'4 fis
  g b
  dis,8. e16 fis8. dis16
  e8.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Largo"
  \time 4/4
  fis8 b16 ais b8 b, g' e cis fis
  d b fis'4~ fis e~ e d8 fis b, b b8. ais16
  b8
}
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Allegro"
  \time 4/4
  	fis4 g8 fis e d16 e fis8 e
        d b b'4~ b8 cis, a'4~
        a8 b, g'4~ g8 g fis e
        d[ b]
}
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Aria. Gracioso"
  \time 3/4
  \partial 4
  b4
  cis d ais
  b2 fis'4
  g fis e d b
}
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "4. Giga. Allegro"
  \time 6/8
  b8 d cis b cis ais b4. cis d8 fis e d e cis d4. e
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Vivace"
  \time 3/4
  r8 a' gis fis e d
  cis b a b cis d
  e g fis e d cis
  b4 e d cis4. d8 cis d
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  e16 d
  cis8[ e a cis,] b16 a b8 r b16 cis
  d8 gis16 a b8 d, cis16 b a8 r
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Aria 1. Andante gracioso"
  \time 3/4
  cis8 d cis b cis a
  b cis d2
  cis8 b cis e d cis b a b2
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Giga. Allegro"
  \time 6/8
  a8 cis e  a b a
  gis4.~ gis8 a b
   e,4 e8 e d cis b4 e,8 e4
}

