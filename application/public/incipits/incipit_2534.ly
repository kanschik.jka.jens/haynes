\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. o.Bez."
  \time 3/4
  c16 b c g  es' d es c  g'4
  b,16 as b g
}
