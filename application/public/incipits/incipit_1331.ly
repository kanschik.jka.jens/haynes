\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante marcia"
  \time 4/4
  \partial 4
    a4
    d2 fis4. d8
    a'4 cis, d fis8. d16
    a'4 cis, d e16 d cis b
    a4 g fis r
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Allegro"
  \time 2/4
  a4. d8
  d4 cis8 r
  \grace a'8 g8. fis32 g a8 g
  fis16 e d cis b a g fis
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Menuetto"
  \time 3/4
  r4 a fis b2 a4
  g g' fis fis e2
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "4. Siciliano. Andante"
  \time 6/8
     a8. b16 cis8 cis b a
     gis8. e'16 d8 d cis4
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "5. Menuetto"
  \time 3/4
  \partial 4
  a'4
  a fis a b g b
  a d a g fis fis
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "6. Finale. Prestissimo"
  \time 2/4
    d,2 fis
    a4 fis'8 d a4 r
    a8 a' a g g fis e d b' g e cis d4 r
}