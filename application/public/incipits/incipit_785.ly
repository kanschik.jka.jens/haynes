\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro assai"
  \time 4/4	
  \partial 4
  d4 g2 g8 b a fis
  \grace a16 g8 fis16 g d4 r d8 g
  \grace fis8 e4 e4. g16 fis g8 e e d d4.
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andante poco Adagio"
  \time 3/4
     g'4. e16 f g e f d
     c4 b c
     a4. c16 b  \grace d16 c8 b16 a
     g4 f e
}



\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Rondo"
  \time 4/4
    \partial 4
    b8 c
    d r d r \grace c8 b4 a8 g
    a r b r c r b c
    d b a g e' c b a g2
}