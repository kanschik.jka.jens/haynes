\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 4/4
  \partial 4
  g'8. g16
  g8. a32 g c16 b a g g a f4 e8
  e32 d f d c4 e32 d c b \grace b8 c8 g r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Menuetto"
  \time 3/4
  g'4. gis8 a f
  e2 f8 d
  c4. e8 d b
  c e d4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Andante"
  \time 4/4
  << {
      % Voice "1"
      R1 r2 r4 d8 g
      g fis g b b a b d
      \grace d8 c2 b4 r
         } \\ {
      % Voice "2"
      d,2 e8 d c b a4. b16 c b4 r R1 R
      } >>
  
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro/Allegretto"
  \time 2/4
  e16. f32 g16 g a16. b32 c16 c
  c b b a a g g f
  e f fis g gis a fis d
  c4 b8 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro assai"
  \time 3/8
  f8 c c
  \grace { b16[ c] }
  g'8 c, c
  \grace { b16[ c] } a'16 c bes a g f
  e d c bes a g
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Menuetto"
  \time 3/4
  f8. f16 f4. e16 f
  g8. g16 g4. f16 g
  a4 g f
  e d c
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Adagio"
  \time 2/2
    c1 g'
    g8 f f4. g16 a g8 f
    f dis e4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Rondeau. Allegro"
  \time 2/4
  f8 f f f
  g4 e8 c
  f f f f
  g a16 g f e d c
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. "
  \time 6/8
  g'4 b8 g4 b8
  a4 c8 fis,4 a8
  g16 a b a g fis e d c b a g
  d'4 c8 b4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Menuetto"
  \time 3/4
  d2. g~
  g4. a16 g fis g a b c4 r r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Andante"
  \time 2/4
  \partial 8
  e16. f32
  g16 e c8 r16 c' b a
  g e c8 r16 c' b a
  g e d c \grace g'8 f4
  f32 e a g g8 r4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Rondeau. Allegretto"
   \time 3/8
     g'4 b16 g
     d8 d d
     a'4 c16 a d,8 d d
     d'4 b16 g
     g fis e d e fis g8 c b
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato"
  \time 2/4
  f8 f4 g16 es
  d8 d4 es16 c 
  bes8 bes'4 a16 g
  g8 f r4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Menuetto. Comodo"
  \time 3/4
  f2 g4 f2 es8 d
  c4 c d
  \grace f8 es2 d4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Romance"
  \time 3/4
  \partial 4
  c4
  a'4. g16 a  c bes a g
  \grace g16 f8. e16 f4 fis
  g8. a16 bes8 d, g f
  \grace f16 e8. d16 c4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Rondo"
  \time 6/8
  \partial 8
  f8
  f4 f8 \grace a8 g f g
  f4. bes
  g16 a bes a g f  es f g f es d  c d c bes a g f8 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro moderato"
  \time 2/4
  \partial 8
  a'32 b a g
  fis8 <a, fis'>4 g'32 a g fis
  e8 <a, e'>4 fis'32 g fis e
  d8 d16 fis fis e d cis
  d16. e32 fis8 r4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Menuetto"
  \time 3/4
  fis8. g32 a  g8 fis e d
  d4 d d d cis8 d e fis g4 g g
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Adagio"
  \time 2/2
  \partial 4
  d4 
  g2~ g8 d b' g 
  \grace a8 g8 fis fis2.
  a4 a4. fis8 d c
  \grace d8 c8 b b2 r4
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Rondo. Presto"
  \time 6/8
  a'4.~ a8 b g
  fis4.~ fis8 g e
  d e fis e d cis d d, fis a d fis
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro moderato"
  \time 2/4
  \partial 8
  bes8
  bes16 es es8~ es16 d32 es f16 d
  es16. d64 es bes8 r bes
  es16 g g8 g16 f32 g as16 f
  g8. f32 g es8 r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Menuetto"
  \time 3/4
  bes'2 as4
  as8 g f es d c
  bes4 es f
  g f r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Andante Romance"
  \time 3/8
  f8 es16 d es f g16 f f4
  f16 es d c d es
  d4 c8
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Rondo moderato"
  \time 2/4
  \partial 4
  f16 d bes bes'
  g es bes bes'
  f d bes bes'
  a f c' bes
}
