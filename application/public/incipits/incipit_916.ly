\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
      es2. f4 g f r2
      bes, es8 d c bes
      bes4. as8 g4 r
}

