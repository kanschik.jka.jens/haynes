\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/2
    d'2 a fis4 d r8 d' cis b
    gis a a4. b8 a g
    eis fis fis4. a8 g fis
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 2/2
  \partial 4
  d4
  g4. b16 a g8 d d e
  d c c4 r r8. c16
  a'4 fis16 g fis g a[ b a b] c a fis c
  c8 b b4
    
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Rondo. Moderato"
  \time 2/4
  \partial 16
  a16
  d8. fis16
  \grace fis16 e8. d16
  \grace d16 cis8. b16 a8 r16 d
  cis d e fis g e fis d
  fis8. e16 d cis b a
}