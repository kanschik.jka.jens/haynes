\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro moderato"
  \time 4/4
    d2 b4. d16 b g4. b16 g d4 r
    d'2 e8 d c b a4 a
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Rondo. Tempo di minuè"
  \time 3/4
 
}
