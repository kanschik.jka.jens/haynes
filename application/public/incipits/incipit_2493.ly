 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 3/2
    r2 r g'
    d2. d4 es2
    bes4. a8 bes4. d8 c4. es8
    d2 g,4. bes8 a4. c8
    bes4. a8 bes4. d8 c4. es8
    d2 g,
}

