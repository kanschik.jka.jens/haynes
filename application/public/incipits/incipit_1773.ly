\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. o.Bez."
  \time 3/4
    d4 d4. d8
    cis4 cis cis
    d4 d4. d8
    e4 e e
}
