\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Moderato"
  \time 3/8
  \partial 8
  c8
  b8. a16 b c
  g8 g f'
  e8. d16 e f
  g4.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro. Tempo di Gavotta"
  \time 2/4
  c4 g'
  c8 g a g
  g4 f e d
  e8 g d4 e8 g c,4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Aria. Affettuoso"
  \time 3/8
  \partial 8
  e16 f g8 c, b c b
  c4.
  g'16 c b a g f e8 d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 2/4
  \partial 8
  g'8 c g a g f e r g
  c g a g f e r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Allegro"
  \time 6/8
  \partial 4.
  c4 g'8
  c4 c8 c b c
  g4. c8 b c
  f, e f c' b c e,4 d8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Preludio. Adagio"
  \time 4/4
  c8 g'~ g16 a g f e8 c b c16 b
  a8 d g, c~ c16 b c d e d e f
  g8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro assai"
  \time 3/8
  e16 f g8 g
  g f e d16 e f8 f
  f e d

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Sarabande. Adagio"
  \time 3/4
  g'8 f e d c d
  b4. c8 d e
  f g f e f d
  e4 \grace d8 c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 2/4
  c'4 b8 a g4 f8 e d c b a
  g4 a8 b
  g c b c
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef bass
  \key g\major
   \tempo "1. Preludio. Andante [Bass]"
  \time 4/8
  r8 g,16 fis g8 d
  r e16 d e8 c
  d16 c b c d8 d,
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Preludio. Andante [Solo]"
  \time 4/8
   \set Score.skipBars = ##t
   R2*3 
  r8 g'16 fis g8 d
  r e16 d e8 c
  r d16 c d8 b
  r16 c b a a8. g16
  g8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro ma non Presto"
  \time 2/4
  g'4 d g, r8 d'
  g d b g 
  g' d b g
  c'4 b a r8
}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Sarabande"
  \time 3/2
  b2 c2. d4
  a1 d2 e2. e4 fis g
  fis2. e4 d2
}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Corrente"
  \time 3/4
  \partial 8
  d8
  b a g a b c
  d c d d e fis
  g fis g e a g
  fis e d b c d
}

  \relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Allegro"
  \time 6/8
  \partial 8
  d8
  g fis g g fis g
  d4. b4 c8
  d4 e8 d4 c8
  b4. g4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Preludio. Largo [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*3
   r8 cis16. d32 e8 a  gis16 e d cis b a gis fis
   e8 b'16. cis32 d8 b
}

\relative c'' {
  \clef bass
  \key a\major
   \tempo "1. Preludio. Largo [Basso]"
  \time 4/4
  a,8 cis16. d32 e8 a, gis16 e d cis b a gis fis
  e8 b''16. cis32 d8 b  cis16 a gis fis e d cis b
  a8 fis'16 gis a8 a
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro ma non Presto"
  \time 3/4
  a'8 a, a a a16 b cis d
  e8 e, e e e16 fis gis e
  a8 a' a a, a16 b cis d
  e8 e, e4 r8
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Pastorale ad libitum"
  \time 6/8
  r4 r8 cis4 b8
  a4 fis'8 fis e d
  cis b a e' a gis 
  fis e d d cis b
  e4.
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Allegro"
  \time 2/4
  e4 a
  gis4. gis16 a
  b8 d, b' d, cis16 b a8~
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Un poco Vivace"
  \time 3/4
  \partial 4
  c4
  d g, f'
  e \grace d8 c4 c'~
  c b8 c d4
  g, a8 g f e
  d4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro ma non presto"
  \time 2/4
  \partial 8
  g'8
  c c c c16 d
  b8 b16 a g8 g
  a a a a
  g g16 f e8 e16 f
  g8 g g g
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Un poco Vivace"
  \time 3/8
  g'8 c16 b c g
  as8 g r
  f g16 f es d
  es8 c r
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Giga"
  \time 6/8
  \partial 8
  c8
  g'4 c8  g4 f8
  es d es c4 es8
  d4 f8 es4 d8
  c b c g4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Adagio"
  \time 4/4
  c8 e16 d e8 g b,8. c16 d e f d
  e8 c a' g f e f16 g e f
  d8 c
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "6. Minuetto"
  \time 3/8
  g8 c d
  e16 c b c g c
  f8 e d 
  e16 c b c g c
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Vivace"
  \time 3/8
  \partial 8
  g'8
  g bes d,
  c16 bes c8 a'
  bes, a16 bes c d
  bes8 a
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Alla breve"
  \time 2/2
  r2 g'
  d bes'
  c,2. a'4
  bes, d g bes,
  c2 d
  g,4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Largo"
  \time 9/8
  d8. es16 d8 bes'4 a8 g4 fis8
  g d16 c d8 es c16 bes c8 d c bes
  a4.
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro ma non presto"
  \time 2/4
  g'4 d
  bes'4. a8
  g fis g a
  d,4. es8
  d c bes a
  bes g bes d
}

