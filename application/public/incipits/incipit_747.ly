\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
  <bes d,>4 <bes d,> <bes d,> r8 f'16 d
  bes8 f g as
}
