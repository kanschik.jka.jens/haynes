\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio assai"
  \time 4/4
    d4~ d32 d e fis g16 a32 bes
    fis4~ fis16 g32 fis e g fis a64 g
    g4
}
