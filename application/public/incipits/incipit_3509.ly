\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key d\major
    \time 4/4
    \tempo "1. Adagio"
      d,4 fis8 a d,8. fis32 d a8 r
      r a' r cis   d16 d cis d  e d cis d
}


