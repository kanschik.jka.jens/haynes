 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  e16 dis
  e8 e16 dis  e8 e16 dis e8 b r c
  fis, g a g16 fis g8 b e4
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. o.Bez."
  \time 3/4
  a,8 b c b c d
  e f e d c b
  a b c b c d e2 r4
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. o.Bez."
  \time 2/4
  e4 e e4. d16 c
  b8 g a b
  e, g' fis b,
}
