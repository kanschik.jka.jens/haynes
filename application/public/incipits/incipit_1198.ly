\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. o.Bez."
  \time 4/4
    r4 r8 e, a c e f
    gis,4 r8 d' b'8. a16 gis fis e d
    c16. b32 a8 r
}
