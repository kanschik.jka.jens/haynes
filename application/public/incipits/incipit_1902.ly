 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Largo"
  \time 4/4
    es,4 bes'8. d32 c bes8 bes'[ d, es]
    f4 bes,8. d32 c bes8 bes'[ es, f]
    g4 bes,8. d32 c bes8 bes'[ as g]
}
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro moderato"
  \time 4/4
  g8 g4 as8 g16 f f4 bes8
  bes16 es, es4 c'16 as g4 f8 r
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Larghetto"
  \time 3/8
   f8 d f32[ es d es]
   f16. d32 f8 r8
   bes a g
   \grace g32 f16 es32 d  d8 r
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Allegretto"
  \time 2/2
  \partial 2
  bes4 bes bes es g bes
  as f g es
  c4. d8 es4 f
  es8 d c bes bes4 bes
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}