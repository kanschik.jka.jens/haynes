\version "2.16.1"

#(set-global-staff-size 14)


      
\relative c'' {
  \clef treble
  \key es\major
   \tempo "Affetuoso"
  \time 4/4
  es4~ es16 bes c bes bes4~ bes16 f g d
  es d es f  es f g es  f es f g f g as f
  g f es8 r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "Aria 1. Presto"
  \time 4/4
  \partial 4
  bes4
  g bes f bes
  es, c'2 bes4 
  c d8 es f4 d
  es8 d es f es4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "Aria 2. Vivace"
  \time 6/8	
  \partial 4.
  es8. f16 es8
  d8. c16 bes8  es8. d16 es8
  f4.
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "Aria 3. Tempo di Ciaconna"
  \time 3/4
  r4 es,8. f16 g8. es16
  bes'4 bes8. c16 d8. bes16
  es2.~
  es8. f32 es d8. c16 bes8. as16
  g4 bes es
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "Aria 4. Allegro"
  \time 6/8
  \partial 4.
  es,8 g bes
  g bes es bes es g
  f d bes es bes g
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "Aria 5. Allegro"
  \time 4/4
  r8 es, es es  f f16 es f8 g
  as es  as2 g4 f bes  es,8 f g a
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "Aria 6. Tempo di Minuetto"
  \time 3/4
  g2 f4 es es8 d es4
  bes' c d es2.
}