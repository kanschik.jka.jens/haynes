 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo"
  \time 4/4
    r4 d4~ d8 bes g es'
    es d r g es c a d
    bes g cis4~ cis8 e cis4
    d8
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro"
  \time 2/4
  g'4 d
  es8 d16 c d8 g,
  c d16 es d8 c bes a16 bes g8
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Lamentabile et appoggiato"
  \time 12/8
  g4 c8 bes8. c16 a8 g4 c8 bes8. c16 a8
  g4 c8 b8. c16 d8 d4.~ d4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. ALlegro"
  \time 3/8
    d4 r8 es4 r8
    d4 r8
    c d a bes g d'
    c d16 c bes a bes8[ g]
}