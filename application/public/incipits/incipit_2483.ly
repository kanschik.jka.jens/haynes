 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Flute"
                     \set Staff. shortInstrumentName = #""
                     \key g\minor
                      \tempo ""
                      \time 4/4
                      % Voice 1
                      r4 d8 es16 d d g, g8 d' g es d16 c bes8 c d2~
                      d4 g8. a16 fis4 bes8. c16
                      a8 bes16 c a8. g16 g2
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #""
                     \key g\minor
                        % Voice 2
                        r2 d8 es16 d d g, g8
                        r2 r4 d'8 g
                        es d16 c bes8 c d4 g8. a16
                        fis8 g16 a fis8. g16 g2
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key g\minor
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
     R1*3
     r2 r4 d8 es16 d
     d g, g8 r4
}