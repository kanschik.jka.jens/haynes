\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Vivace"
  \time 4/4
  \partial 8
  b32 a b c
  d8 g, d' g fis d r b32 a b c
  d8 c b a a4 b8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Andantino"
  \time 6/8
  \partial 4.
  d8. es16 d8
  d8. es16 d8 g4 c,8
  c4. f4 bes,8
  bes4.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Spirituoso"
  \time 4/4
    r4 b16 d8. c16 e8. a,16 c8.
    b8 g g'4 r fis g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga"
  \time 6/8

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Vivace"
  \time 3/2
  r4 e a, c e a
  gis8 e b'4 r gis8 e b'4 b,
  c8 cis d dis e4 d c b
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Moderato"
  \time 2/4
  \partial 8
  c8 c16 b d8 d16 c e8 \grace f8 e d f4
  e16 g e c  d f d b
  c e c g c8
}
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key a\minor
                      \tempo "1. Allegro"
                      \time 12/8
                      \partial 4.
                      r4.
                      r4. c b bes
                      a a' gis8 b e, a c e,
                  }
\new Staff { \clef "treble" 
                     \key a\minor
                     a8 a a 
                     a a a e4 fis8 g4 g,8 d'4 e8
                     f4 f,8 c'4 d8 e4 e,8 e'4 e,8 
                  }
>>
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Vivace"
  \time 3/4
  r4 b cis
  d8 b4 d8 cis16 d e cis
  d8 b g'4 fis
  e8 d d cis cis b ais2
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Poco presto"
  \time 2/2
  d2 fis4 a fis8 d e4 a,2
  g'8 e fis4 d,2
  fis'8 d e4 a,2
}
  
  \relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Allegro"
  \time 4/4
  fis4 b, r8 g' g e
  e cis cis a a'4 d,
  r8 fis fis d d b b d
  cis e4 d16 cis d4
}
  

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  e16 dis 
  e8 b fis' b, g' e b'4~
  b8 a16 g fis8 e dis b a'4~
  a
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante"
  \time 3/4
  \grace g'16 fis8 e16 fis g4 d
  \times 2/3 { e8[ g e]} e4 d
  \grace d16 c8 b16 c \grace d16 c8 b16 c \grace d16 c8 b16 c 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Vivace"
  \time 6/4
  r4 b e2 dis8 e fis4
  b, g'8 fis g e fis4 a2~
  a4 g8 fis g4
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key a\major
                      \tempo "1. Allegro"
                      \time 9/8
                      r4. r r
                      r r cis8 a cis b4. r
                  }
\new Staff { \clef "treble" 
                     \key a\major
                       e8 a e  cis e cis a cis a
                       e4. a r
                       a8 fis a gis4.
                          
                  }
>>
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegremente"
  \time 2/4
  a16 d d4 cis8 a16 f' f4 e8
  a,16 a' a8 a,16 g' g8
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Presto"
  \time 4/4
  e4 a2 cis,4 d8 e fis2 a,4
  gis8 b e2 d4
  cis8 d cis b a4
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Presto"
  \time 4/4
  r4 gis'8 a b4 b, 
  e8 dis e4 r a,
  fis'8 e dis cis b4 a'
  a gis r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegretto"
  \time 3/4
  \partial 4
  e,16 fis g a
  b4 b e
  dis2 c4~ c b a g8
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "3. Vivace assai"
  \time 3/8
  e16 gis fis8 r
  dis16 fis e8 r
  cis16 e dis8 cis
  b16 a gis fis e fis
  gis8
}
