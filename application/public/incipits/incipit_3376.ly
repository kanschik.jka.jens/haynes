\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 8
  f8
  c[ a16 bes] c8 a16 bes c8 f c f
  c[ a16 bes] c8 a16 bes c8 f c f
  g16 f g a bes[ a bes a]
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Andante"
  \time 3/4
  r4 r a
  bes4. a8 g16 bes a8
  f4 e8 d f'4
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro assai"
  \time 2/4
  f8 c' c bes16 c d8 c16 bes c8 f
  c a16 bes c8 bes16 c
}