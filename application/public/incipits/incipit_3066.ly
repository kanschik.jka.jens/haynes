\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria Adagio (Sopran): Quia respexit humilitatem [BWV 243/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key b\minor
   \tempo "Adagio"
   \time 4/4
   r8 d16 cis e d cis b ais8. b32 cis e,4~
   e16 d' cis b ais ges fis e
   dis e fis g 
   a4~a16
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key b\minor
   \tempo "Adagio"
   \time 4/4
   \set Score.skipBars = ##t
     R1*5 d8. e32 fis e16 d cis b
     ais8. b32 cis
     e,4
     
}



