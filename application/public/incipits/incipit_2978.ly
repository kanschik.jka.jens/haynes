\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro moderato"
  \time 4/4
  c2. d8 c c2 a4 r
  c2. d8 c c2 a4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 2/4
  d4 d16. c32 es16. d32
  f8. g16 f8 r
  bes4 bes16. a32 g16. f32
  f16. g32 es16. f32
  d8 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Finale. Allegro molto"
  \time 2/4
  c16 d c b c8 f
  c a' f c'
  bes bes16 a g8 g16 f
  e8 e16 d c8 c,
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  bes'8 a16 g f8 f f4 r
  g8 f16 es d8 d d4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio Cantabile"
  \time 3/4
  c2 a'8. f16
  e8. f32 g bes,4 r
  a d8 g bes g
  \grace f8 e4 f r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Minuetto Allegretto"
  \time 3/4
  f8 d f d f d
  bes'4 bes bes
  a8 f a f a f
  c'2 r4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Finale. Allegro non troppo"
  \time 2/4
  \partial 8
  f8
  f4. es8
  es4 d8 f
  f4. es8
  es4 d8 d
  es d es f
  a4 g8 f
  es es d d
  d4 c8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  r8 d d d   d d d d
  c b a b g4 b
  b2 a4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 3/4
  c4. d16 e d8 c
  c4 b c
  \grace b8 a4. b16 c b8 a
  b8. a16 g4 r
}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Rondo. Allegro non troppo"
  \time 6/8
  \partial 8
  d16 c
  b4 b8 \grace c16 b8 a b
  d4 c8 b4 d8
  g,4 g8 \grace a16 g8 fis g
  b4. a4
}
  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 2/2
  r2 bes'4 bes
  bes4. g8 bes8. g16 c8. bes16
  \grace bes8 as4 g r2
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Minuetto"
  \time 3/4
  bes2 c8 bes
  bes4 as g
  f f g as r r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Finale. Allegro"
  \time 12/8
  es,4. g bes es
  c es8 d c bes4 g8 es4 es'8
  c4. es8 d c bes4 es8 g4 bes8
  bes as g  g f es  d c bes  as g f
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 3/4
  g4 as r as g r
  r8 d g bes a g
  a r bes r r4
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 3/4
  fis2. fis4 g cis, d2. d4 c a
  g8 g g a bes c d[ d d d]
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Menuetto"
  \time 3/4
  g8. fis16 g8[ a bes c]
  d4 d2
  g8. fis16 g8[ bes a g]
  g4 fis d
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Finale. Presto"
  \time 4/4
  g'4 d es r
  R1
  bes'4 fis g r
  R1
  \grace d8 d'2 d8 c bes a
  bes a g fis g d bes g
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro assai"
  \time 6/8
  c,4 \times 2/3 { g16[ a b] } c8 c c
  c e c g'4 a8~
  a f e d g f
  e g16 f e d c4 r8
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio Lamentabile"
  \time 2/4
  \partial 8
  g16. es32
  d16 c c8 r16 c' es c
  bes8 as r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Finale. Presto"
  \time 2/4
  \partial 4
    c,16 d e f g a b c d e f d
    e8 e c c
    a' a c, c b r
}
