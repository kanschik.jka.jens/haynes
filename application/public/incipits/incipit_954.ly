 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/4
  bes4. a'16 bes
  f8 f4 \times 2/3 { g16[ f es]} 
  es16 d d4 d16 es
  f8. d16 f d g es
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio"
  \time 2/4
  bes16.[ as64 g] g16 g g as as bes
  as16.[ g64 f] f16 f f g g as
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Tempo Moderato"
  \time 3/4
  bes,2. es
  d4 bes' bes bes8 a a2
  g2 a4
}

