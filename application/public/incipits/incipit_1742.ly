\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Vivace"
  \time 4/4
  d8[ a d a] e'[ a, e' a,]
  fis'4 r8 d cis4 a

}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Largo"
  \time 3/2
  r2 e g fis1 r2
  r e g fis1 r2 r b1~ b2 a4 g fis e dis1
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Vivace"
  \time 2/2
  r8 g16 fis g8[ a] b8 a b c
  d4. g8 fis4 r8 
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Vivace"
  \time 2/2
  g8 bes d[ fis] g g, r4
  g8 bes d[ fis] g g, r d'
  bes d a d bes g r
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Poco Largo"
  \time 2/2
  g4 b d g,
  a d fis, a g b e a
  fis2 d g r4
}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Andante"
  \time 4/4
  e8 g fis b g e b' b,
  e g fis b g e c' a
  fis d b' g e c a' fis
  dis b e2 dis4 e r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Andante"
  \time 3/8

}

