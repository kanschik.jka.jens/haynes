\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): An irdische Schätze das Herze zu hängen [BWV 26/4]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key e\minor
                      \tempo "o. Bez."
                      \time 4/4
                      \partial 4
                      % Voice 1
                      b4 e fis8 dis e4 g8 e
                      fis4 g8 e fis4 a8 fis
                      g4 fis8 e c'4 b8 a
                      b4 a16 g fis e dis4 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                    g,4 b c8 a b4 e8 b
                    dis4 e8 cis dis4 fis8 dis
                    e4 dis8 e dis4 e8 fis 
                    e4 e8 c b4
                       
                        
                
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 3"
                     \set Staff. shortInstrumentName = #"Ob 3"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 3
                 e,4 g a8 fis g4 b8 g
                 a4 b8 g a4 c8 a
                 b4 a8 b a4 g8 fis
                 g4 c8 a fis4
                       
                        
                
                  }
>>
}


\relative c {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key e\minor
   \tempo "o. Bez."
  \time 4/4
  \partial 4
     \set Score.skipBars = ##t
    r4 R1*15 r2 r4 b4 e fis8 dis e4 g8 e
    fis4 g8 e fis4 a8 fis
    g4 fis8 e c'4 b8 a
    b4 a16 g fis e dis4
   
    
    
    }