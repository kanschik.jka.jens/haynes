 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 2/2
    d4 es d16 c bes4 d8
    c16 bes a4 c8 bes a16 g g8 g
    bes4 c bes16 a g4 bes8
    a16 g fis4 a8 g16 a bes c d4
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Largo"
  \time 3/8
    c8. es32 d c8
    b a g
    f'8. as32 g f8
    es d c
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Allegro"
  \time 2/4
   r8 d g4~
   g8 a bes a
   g4 fis
   g8 d c4~ 
   c8 es c a
   bes d bes g
}
