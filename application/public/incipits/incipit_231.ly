\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef alto
  \key a\major
   \tempo "1. o.Bez."
  \time 2/4
\partial 8
  e,8
  \times 2/3 {a8[ b a]}
  \times 2/3 {a8[ b a]}
  e'16 d cis b a8

}
\addlyrics  { Lu-sin-ga ques to cor }