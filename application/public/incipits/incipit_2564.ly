 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Con Affetto"
  \time 12/8
  \partial 4.
  bes16. c32 d8 g
  d4 c8 d16 c es8 c a8. g16 f8 r4 r8
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "Aria 1. Presto"
  \time 4/4
  bes4 c8 d es f g4
  f es d c bes2 r4 bes
  c f, r es'
  d c f2~ f4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "Aria 2. Dolce"
  \time 4/4
  \partial 8
  d16 es
  d8 d d d16 es d8 d d d
  g f es d es8. f16 es8
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "Aria 3. Vivace"
  \time 4/4
  \partial 2
  \times 2/3 { d8[ es d] } \times 2/3 { c8[ d bes] }
  f'4. r16 d  \times 2/3 { es8[ f g] } \times 2/3 { f8[ g es] }
  d4.
}



\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "Aria 4. Largo"
  \time 3/4
  f4 es8 d c bes a4 bes2
  f4 es' d c4.
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "Aria 5"
  \time 4/4
  bes16 c d4 c16 bes c d es4 d16 c
  d es f4 es d8 g c,
  f bes,4
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "Aria 6. Allegro"
  \time 3/8
  \partial 8
  d8
  g f es
  \times 3/4 { d4 c}
  bes8 c16 bes a g
  a8[ f]
}