 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro [Tutti]"
  \time 3/4
  d8[ a a a] a8. b32 cis
  d8[ fis, fis fis] fis8. g32 a
  b8 g16 fis g8 g'16 fis e d cis b
  cis4 a8 e' e e
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro [Solo]"
  \time 3/4
    \set Score.skipBars = ##t
   R2.*28 
   r4 d, e
   fis16 g fis e d8 fis e g
   fis16 g fis e d8 a' g b
   a16 b a g fis g fis e d8 fis

}


\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  fis,8 fis fis b b16 gis ais8 r4
  e8 e e e  e16 cis d8 r4
}


\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "2. Adagio [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*7
   fis4~ fis16. cis32 d8~ d16. ais32 b8~ b16. fis32 g8~
   g16. dis32 e8~ e16. cis32 fis16. e32 dis16 cis b8
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Allegro"
  \time 2/4
  \partial 8
  a8 d16 e fis4 e32 d cis16
  d16 d, e fis g a b cis
  d e fis4 e32 d cis16
  d16 d, e fis g a b cis
}
