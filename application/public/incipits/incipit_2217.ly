\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Andante [Tutti]"
  \time 3/4
  es8 bes g es g bes
  es g4 es8 bes g'
  f d bes f d f
  bes f'4 d8 bes f'
  es4 es es
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Andante [Solo]"
  \time 3/4
   es8 bes bes4~ bes16 es f g
   f8 bes, bes4~ bes16 f' g as
   g8 f \grace {f16[ g]} as8 g f es
   \grace es8 d8. c16 bes4 r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  es,8
  g bes es g
  f16 d \grace d8 es4 es,8
  g bes es g as16 f \grace f8 g4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Solo]"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Cantabile [Tutti]"
  \time 4/4
  g'4 as8 c, \grace c8 b4 c
  d as~ as16 g32 f es16 d es d c8 r
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Cantabile [Solo]"
  \time 4/4
  g'4 as8 c, \grace c8 b4. c8
  d g, f' es16 d es 8 d16 c r8
}
\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Allegro"
  \time 3/4
  es,4 g bes es g bes
  as \grace g8 f2
  g8 es bes' g f es
}