\version "2.16.1"
         #(set-global-staff-size 14)

         
         \relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
    d4 g4. f8 f es16 d
    es4. es16 d d4. es8 c4 f4. f16 es e8. c16 a4
}
         