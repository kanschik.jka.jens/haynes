\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Aria"
  \time 2/2
   \partial 8
  g8
  c4 c8 c c4 c8 c
  d e f f f4 e
}



\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2.Aria Largo"
  \time 4/4
r4 r8 es16. f32 g8 g g as16. g32
f16. es32 f8 r8 d16. es32 f8 f f g16. f32
es16. d32 es8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Aria [Allegro]"
  \time 2/2
  d8[ e16 d] c8 g e'[ g16 f] e8 c
  g' r g r g r g16 g g g
}

\relative c'' {
  \clef bass
  \key c\major
   \tempo "4. Aria Adagio"
  \time 2/2
  r4 r8 c,16 d e8 d r d16 e
  f8 g r c,16 d b8 c r f16 f
  g8
}

\relative c'' {
  \clef treble	
  \key c\major
   \tempo "4. Aria "
  \time 2/2
  r4 r8 g8
  c g g c
  d4 r8 d e g, e' d16 c
  d4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Aria "
  \time 6/8
  r8 e16. f32 d16. f32 e8 g g 
  g g16. a32 f16. a32 g8 f e
}
