\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  \partial 4
  a4
  d8 fis16 e d8 d, fis d fis a
  d fis16 e d8 d, fis d fis a
  d16 e d cis  d e fis g a8 fis d a
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \partial 4
  r4
   \set Score.skipBars = ##t
   R1*28
   r2 r4 a4
   d a'4. b16 a a g fis e
   d4 fis4. g16 fis fis e d cis
   cis b d cis e d cis b a4 g 
   \grace g4 fis2 r4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  \partial 8
  d16 e
  d g g,8 r g'16 d \grace d8 c c r c16 d
  c a' a,8 r e'16 c \grace c8 b8 b r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio [Solo]"
  \time 4/4
  \partial 8
  r8
  \set Score.skipBars = ##t
  R1*3
  r2 r4 r8 d16 \grace fis8 e16
  \grace e8 d16 \grace c8 b16 \grace a8 g8 g'8. \grace fis8 e16
  \grace d16 c8 c4 c16 d
  c8 fis16.[ \grace { g16[ a g fis g]}   a32] c8 c, 
  \grace c8 b4 r8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro assai"
  \time 2/2
  \partial 4
  a4 d d2 fis4
  a a2 b4
  \grace a8 g4 g2 fis4
  \grace fis4 e4 d r
}

