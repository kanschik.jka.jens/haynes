\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Marche "
  \time 4/4
  f8 c c f  c a a c f,2 r
  r4 f'4 g f8 e
  f4 a bes a8 g a4 r r2
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Menuet"
  \time 3/4
  f,4 a a c c f
  e4 d8 e f4
  d8 f f d c f bes,2 a4
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Gavotte"
  \time 2/2
  f4 c c bes8 a
  bes4 d c2
  a4 c c bes8 a bes4 a g c,
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Gigue"
  \time 6/8
  \partial 8
  f8
  f4 c8 c bes16 a g f
  g8 g g g4
}

