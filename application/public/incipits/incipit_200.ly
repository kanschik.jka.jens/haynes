 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
    c4 r8 c  g' g g g
    a a a a  b b b b 
    c[ c,]
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Adagio"
  \time 4/4
  \partial 4
  es4~
  es d a'2~
  a4  g g f~
  f8 es d c  b4 r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Allegro"
  \time 3/8
    g'8 e c
     g'4 g8 
     a16 g f g a b
     c8 b c
     
}
