 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "Allegro"
  \time 4/4
  f,4 a2 c16 bes a bes
  c4 c16 bes a bes \grace bes8 a4 r
  a16 f g a bes c d e  f e a g f e d c
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "Allegretto"
  \time 2/4
  a4~ a8 d d4 cis8 r
  a4~ a8 g'
  g4 f8 r
}
