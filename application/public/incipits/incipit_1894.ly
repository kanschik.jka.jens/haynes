\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Concert " 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \key e\major
   \tempo "1. Ouverture"
  \time 2/2
  r8 e8 gis8. e16 b'8. b16 dis8. b16
  e4. fis8 fis4. e16 fis
  gis4. a16 gis fis4 b
  e,4. e8 a2~ a8. a16  gis8. fis16 gis4.

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. On peut passer la Venissienne Sil'on veut. Uenissienne"
  \time 6/8
  \partial 2
  b8 cis a cis
  b4 gis8 a fis a
  gis e b' cis a cis
  dis b e dis4
  

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "3. Air"
  \time 2/2
  \partial 4
  b4 e dis e dis
  e b e, fis'
  gis fis gis fis
  gis fis8 gis e4 gis 
  fis e dis e fis2

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "5. Premier Passepied"
  \time 3/8
  \partial 8
  b8 gis e b'
  e4 e,8
  fis16 e fis gis a b
  gis8 e b'
  cis8 fis,16 gis a fis
  b8 e,16 fis gis e
  a8 a gis fis4

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "6. Sarabande"
  \time 3/4
  b4 e4. gis,8
  cis2. b4 b4. a8
  gis4. fis8 e4
  e' \grace dis8 cis4 \grace b8 ais4
  fis

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "7. Premier Tambourin"
  \time 2/4
  b16 a b cis b8 a
  gis[ b fis a] 
  gis[ b fis a]
  gis fis16 gis e8 e
  gis[ gis b b] 
  e4 
  
}

\relative c''' {
  \clef treble
  \key e\major
   \tempo "8. Chaconne"
  \time 3/4
  r4 gis4 e
  fis a fis
  gis gis8 a b4
  a8 gis fis e dis cis
  b4 

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Airs a Danser - Premier Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Entrée"
  \time 2/2
  \partial 2
  c4 d
  e a, a' gis
  a e c d
  e f8 e d c b a
  gis4 e

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Air en Chaconne"
  \time 3/4
  r4 e8 d c b
  a4 b c
  gis2 r8 gis8
  a2 r8 a8
  b a b c d b
  c4 a
  

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Air de Paysan"
  \time 2/2
  \partial 4
  e4 a, a' a a 
  g2. g4
  f f f f
  f e2 e4 
  d d d d 
  b g

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Menuet"
  \time 3/4
  e8 d e f e4
  a, a'2
  gis a4
  e2 c4
  d e2
  c4 b8 c a4
  b gis a b e, e


}


\relative c'' {
  \clef treble
  \key a\major
   \tempo "5. Menuet"
  \time 2/2
  \partial 2
  a2 gis fis
  e a gis fis
  e4 e' cis e
  b e a fis
  gis


}


\relative c'' {
  \clef treble
  \key a\major
   \tempo "6. Rondeau"
  \time 2/2
  \partial 4
  e4 fis b, b d
  e a, a cis
  fis, a d cis 
  b8 a gis fis e4

}

\relative c' {
  \clef treble
  \key a\major
   \tempo "7. Premier Passepied"
  \time 3/8
  \partial 8
  e8 a a a 
  b b b 
  cis d16 cis b cis
  a gis a b cis d
  e8 a, fis'
  e a, a' 
  e fis16 e d cis b4


}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "9. Gigue"
  \time 6/8
  \partial 8.
  cis16 d8
  e8. d16 e8 fis8. e16 d8
  e8. d16 e8 fis8. e16 d8
  e8. d16 cis8 b8. cis16 d8
  cis4

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "10. Cotillon"
  \time 2/2
  \partial 2
  cis4 d8 cis
  b4 a cis d
  e2 e,4 a
  gis a b cis
  b a 


}



