\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 2/4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. "
  \time 3/4	

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. All."
  \time 4/4
  \partial 4
  a'8. f16
  b,2 c4 a'8. f16
  b,2 c4 f8. a16
  \grace c,8 c'2. a8. f16
  g4. a16 bes a4
}

\relative c'' {
  \clef alto
  \key f\major
   \tempo "2. Andante"
  \time 2/4
  \partial 4
  c,4
  a8 f a c
  \grace c8 bes4 a
  g8 f e f g4 c
  a8 f a c \grace c8 bes4 a
  g8 g a b
  c4 d8 e c8 r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 2/4
  \partial 4
  c'4 a8 f a c
  \grace c8 bes4 a
  g8 f e f g4 c
  a8 f a c bes4 a
  g8 f g e  f4
}
