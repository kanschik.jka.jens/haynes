\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 16
    g'8 g g g  fis fis fis fis
    e e e e   d d d d
    c d e fis g4 b,8 g
    c[ b]
}
