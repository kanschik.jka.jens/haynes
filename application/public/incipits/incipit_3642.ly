\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante comodo"
  \time 2/2
  bes8. c32 d  c8. d32 es d4 c8 d
  es8. f32 g f8 es es4 d8 es

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 3/8
    d8 f es
    d \grace {es16 f} g8 f
    es8 \grace d8 c4
    d16. es32 f8 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 3/8
    bes8 c16 d es f g8 f bes
    a16 bes c8 es, es d bes'
    a16 bes c8 es, es d f
}

