\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}





\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 4
  es,8 f
  g4 es bes' g
  es' bes g' g,,
  as2 bes
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Arioso"
  \time 2/4
  \partial 8
  d8
  \grace c8 bes8. c16 d8 d
  es d r d
  d16 bes c8 r c
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro"
  \time 3/8
    g16 es bes'8 bes bes c16 bes c d
    es8 bes bes
}
