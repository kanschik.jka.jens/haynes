 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Ouverture"
   \time 4/4  
      c4. c8 c4~ c16 c d e
      f4. f8 f4. g8
      e2
}

 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Bourèe"
   \time 2/2
        \partial 4
        f4
        f c d c8 bes
        c4 a
}

 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Gavotta"
   \time 2/2  
        \partial 2
        f8. f16 e8. e16
        f4 c d8. c16 c8. bes16
        bes4 a
}

 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Aria. Adagio"
   \time 4/4  
      a4. f8 c'4. d16 c
      bes8. a16 bes8 c a4~ a16 c d e
      f8. f16 f8 d es4 r8
}

 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. Menuet"
   \time 3/4  
      f2 e4 f e8 d c bes
      a4 bes c d2 c4
}

 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "6. Giga"
   \time 6/8
      f4 f8  f c a
      a'4 a8  a f c
      g'4 g8 g e c
      f4 f8 f d bes
}

