\version "2.16.1"
         #(set-global-staff-size 14)
         
         \relative c'' {
  \clef treble
  \key d\major
   \tempo "1.Allero assai [Tutti]"
  \time 4/4
  d16 d d d \repeat tremolo 4 a16 \repeat tremolo 4 fis \repeat tremolo 4 d 
  \repeat tremolo 4 d' \repeat tremolo 4 a \repeat tremolo 4 fis \repeat tremolo 4 d   
  \repeat tremolo 4 d' \repeat tremolo 4 b \repeat tremolo 4 g \repeat tremolo 4 d
         }
   \relative c'' {
  \clef treble
  \key d\major
   \tempo "1.Allero assai [Solo]"
  \time 4/4
  d2 fis16 (d8.) g16 (e8.)
  a16 (fis8.) \grace {e} d2 a4 b16 (cis8.) d16 (cis8.) \slashedGrace e d4 cis16 (b8.)
   }
   
    
         \relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Largo [Tutti]"
  \time 2/2
  g4~ \tuplet 3/2 {g8 d' b} \grace {a} g4~ \tuplet 3/2 {g8 e' c} \grace g8~ \tuplet 3/2 {g8 fis' g}\tuplet 3/2 {a, a a}\tuplet 3/2 {b b b}\tuplet 3/2 {c c c}
         }
         

         \relative c'' {
  \clef treble
  \key g\major
  \tempo "2. Largo [Solo]"
 \time 2/2         
  \partial4 d,4 g'~ \tuplet 3/2 {g8 d b} \grace {a8} g4~ \tuplet 3/2 {g8 b d}  \tuplet 3/2 {e c e} \tuplet 3/2 {g fis e} d4~ \tuplet 3/2 {d8 g b}
         }
\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro molto [Tutti] "
  \time 3/4
\chordmode {d8:5.10} a' fis d a fis d4 a2 b4\p (a) d-. \grace {c8} b4 a2
}
\relative c'' {
  \clef treble
  \key d\major
  \tempo "3. Allegro molto [solo] "
  \time 3/4
 d4 a2 g8. (a32 b) a2 b4 a d \grace {c8} b2 a4 d (cis) e-.
}