\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Moderato [Oboe]"
  \time 2/4
  d4 c32 b16. a32 g16.
  e'4 e  e8 g4 e16 c
  d8 b r 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Moderato [Sopran]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*28
   b16[ c d8 d] d d4 r
   c16[ d e8 e] e e4 r
  
}
