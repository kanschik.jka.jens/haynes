\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. [Allegro]"
  \time 4/4
d4 r16 a' g a fis8 d a fis
d4 r8 a'32 b cis64 d e32 fis8 e fis e
d4
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Andante [Tutti]"
  \time 2/2
  d,8 f'16 e f8 d, cis a''16 g a8 c,,8 b g''16 f g8 bes,, a
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Andante [Solo]"
  \time 2/2
  R1 r2 r4 a~ a4. e'8 f e16 d a'8 d, cis4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 6/8
  d8 cis16 b a g fis8 a d
  g, b d e, g cis
  d, a''16 g fis g a8 d,, fis
  e
}