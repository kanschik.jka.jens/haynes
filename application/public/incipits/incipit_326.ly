\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. All. Moderato"
  \time 4/4
  f2. bes8 d,
  d2. f8 bes, bes2~ bes8 g' es c
  bes2 a4 r
}
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio"
  \time 3/4
  \partial 4
  g8. f16
  es4 bes c
  es4. f8 g as
  g4 f g8. f16
  es4
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Rondeau. Allegretto"
  \time 6/8
    \partial 8
    f,8 
    bes4 d8 c a es'
    d bes f' f4 a,16 c
    f,8 d' bes a es' c
    bes16 a bes c d es c4
}
