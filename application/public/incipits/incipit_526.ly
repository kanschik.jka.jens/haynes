\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gratioso"
  \time 3/4
\partial 4
g4 c4. e8[ d f]
e4 \grace d8 c4 g'
a4. g8[ f a]
g4 \grace f8
e4
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  c8 g'[ g g a16 b]
  c8 c,4 g'8
  f16 e d c  e d c b
  c8 g4
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Aria"
  \time 3/8
  g'4 c8
  b8. a16 g8
  a g f e8. d16 e f
  g8 f e d4.
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Giga"
  \time 6/8
  \partial 4.
  c4 g'8
  e d c g'4 g8 g4. c8 b a
  g f e d g f e d c
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gratioso"
  \time 2/2
\partial 2
g'4. c8
\grace b8 a4 b8 c \grace g8 f4. e16 f
e2 g4. f16 e
a4 d,8 f e4. d16 c 
b4 \grace a8 g4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 2/4
    c4 g' c r8 c,
    a'[ a a a]
    a g4 c,8
    f[ f f f]
    f e4
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Aria"
  \time 2/2
  \partial 2
  e4 g8 e
  e d g d  d c a' c,
  c b a g a'4 c8 a
  a g c g
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Presto"
  \time 2/4
    c'8. g16 e8. c16
    g'4
    g, c8. e16 d8. c16
    b4 \grace a8 g4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gratioso"
  \time 3/8
e4 d8 c4 g'8
f e d e d16 e c8
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 16
  g'16
  g8 f16 e d8 c \grace { f16[ g]} a8 g r c
  f, g16 a g8 f e d16 e c8
}
  
  \relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro"
  \time 2/4
g'4 c
g4. as16 g f8[ es d g]
es8. d16 c d es f
  }
  
  \relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Adagio"
  \time 3/4
  g'2 c,4 as' g f
  g \grace f8 es4 g
  f es d
  es \grace d8 c4
  }

  \relative c'' {
  \clef treble
  \key c\major
   \tempo "5. 1er Tambourino"
  \time 2/4
e16 f g4 a8
g16 f g4 a8
g16 f e d c8 g'
a16 g f e f e d c
d4 d  }
  
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

  
  \relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Gratioso"
  \time 3/4
  \partial 4
  c4
  g'4. as16 g f8 g16 d
  es4 \grace d8 c4 g'
  c4. b8[ c d]
  b4 \grace a8 g4
  }
  
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. La Bully. Allegro"
  \time 2/4
c'4 c
c4. bes16 as
g8[ f es d] es d16 es c8 g
}

\relative c'' {
  \clef treble
  \key c\major  
   \tempo "3. 1er Minuetto"
  \time 3/4
e8. f16 g4 c
g f e
d8. e16 f4 d'
f, e d
}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. 2. Minuetto"
  \time 3/4
g'8. f16 es4 f
g c, \grace {f16[ g] } as4
g f8 es d g
es4 d8 es c4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Gratioso"
  \time 3/4
\partial 4
  g'4
  es4. d8 c4
  f8 as g f es d
  es d c d es f g8. as32 g g4.
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 2/4
   g'8[ c, c c]
   \grace e8 d c16 b c8 g'
   c[ c, c c]
   \grace e8 d c16 b c d e f g8
}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro"
  \time 2/4
c'8[ g f es]
as[ f es d]
g f16 es d g f g es8. d16 c d es f
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Giga"
  \time 6/8
g'4. c,
a'8 b c c b c
g4. c,
f8 g a g a f
e d e c4.
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Gratioso"
  \time 2/2
  \partial 2
  d4 g
  fis4. e8 d e c d
  b4 \grace a8 g4 b a8 g
  d4 e
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
g'8 d b g
e' d4 e16 fis
g8 d16 c b8 a16 g e'8 d4
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. 1er Aria"
  \time 6/8
\partial 4.
g'8 a b
b a g fis4 g8 
a4 d,8 g fis a
a g b b a d
b4 \grace a8 g
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Presto"
  \time 2/4
  g'8.[ d16 b'8. g16]
  d'4 d, e8.[ fis16 g8. a16]
  fis4 \grace e8 d4
}

