\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  d8
  a' a4 a8 \times 2/3 { a16[ g fis] } fis8 r fis
  e fis4 g \grace g16 \times 2/3 { fis16[ e d] } d r16
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegretto [Tutti]"
  \time 2/4
  a'4. b16. a32
  g4. a16. g32
  \grace g16 fis8 e16. d32 d8 d
  d2~
  d8 cis16 d e e fis g
  fis4
}
