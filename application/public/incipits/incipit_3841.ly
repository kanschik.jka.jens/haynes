 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  e,2 f4 g
  gis8 a a2 b4
  c e8 d d c b a
  a4 g c2~
  c4
}


\relative c'' {
  \clef bass
  \key a\minor	
   \tempo "2. Adagio"
  \time 2/4
  a,4 e'16 c b a
  a32 g b a gis8. f'16 e d
  d c b a e' d c b
  a32[ d f e d8.]
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Allegro"
  \time 4/4
  g'4. a16 g f8 e f g
  a b16 c b8 a a g f e
  e d e f a g f e
  d a f' d \grace c8 b4 a8 g
}
