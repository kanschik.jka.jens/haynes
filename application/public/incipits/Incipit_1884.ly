\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Concert " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Prelude. un peu lent"
  \time 3/4
d8. cis16 d8. e16 d8. e16
fis8. e16 fis8. g16 fis8. g16
a8. g16 a8. b16 a8. g16
fis8. g16 fis8. e16 d8. cis16
d4~ d8. cis16 d8. cis16

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Air"
  \time 2/4
\partial 4
fis8 e
g fis a g16 fis
e8. fis16 d e fis g
a8 g16 fis e8 a
fis d fis e16 r16

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "3. Premier Menuet. Gay"
  \time 3/4
a4 fis2
e2 fis4
d g2
e e4
fis b2 gis a4
e gis2 a2.

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Air. Gracieusement"
  \time 3/4
\partial 4
d4 a'2 bes4
a g f g f e
\grace e16 f2 \grace e16 d4
f e d 
g f e

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "5. Premiere Badine. Legerement"
  \time 6/8
\partial 2
a8 fis g a
d,4 a'8 fis8. g16 a8
cis,4 a8 d cis d
e d e fis g a
e4 a8 fis8. g16 a8
d,4

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "6. Air. Gay"
  \time 3/4
\partial 4
d4 cis4. b8 cis4
d a d
e4. d8 e4
f e f 
g4. f8 g4
a f a
g bes8 a g f
e2 d4


}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "7. Plainte"
  \time 3/4
\partial 8*3
fis8 g4
a \grace g16 fis4 \grace e16 d4
e a, r4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "8. Fugue. Legerement"
  \time 2/4
r8 a8 a a 
d d d d 
e16 d e fis e fis g e
fis8 d4 a'8 
a g16 fis g8 g
g fis4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxiéme Concert " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Prelude. Lentement"
  \time 2/4
\partial 4
b8. b16
e8. e16 e8 fis16. g32
dis4 e8. fis16
\grace fis16 g8. a16 a8 g16. fis32
\grace fis16 g8 \grace fis16 e8 


}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisiéme Concert " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Rondeau. Tendrement"
  \time 2/4
\partial 4
b8 \grace a16 g8
d' d g8. a16
fis4 g8. b,16
\grace b16 c8. d16 \grace d16 c8. b16
a8 a b \grace a16 g8


}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatriéme Concert " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Dialogue. Lentement"
  \time 2/4
a8. b16 b4 \grace {a32[ b]}
c8. d16 d4 \grace {c32[ d]}
e2
c8. d16 d4 \grace {c32[ d]}
e8. f16 f4 \grace {e32[ f]}
g2~ g8 f16. e32 d8 e

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquiéme Concert" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Prelude. Lentement"
  \time 2/4
\partial 4
e8. a16
gis8 e fis gis
a a, cis a
e' d cis a
d cis b a 

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixiéme Concert " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key b\minor
   \tempo "1. Andante"
  \time 2/4
\partial 16
b16 b2~ b8 d16 cis b a g fis
g8 fis e d16 cis
d8 b16 cis d e fis g
a8 g16 fis b8. cis16
a4. fis8

}

