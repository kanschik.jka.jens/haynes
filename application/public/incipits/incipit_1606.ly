\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8
  a8
  fis'8. e32 d
  e16. fis32 gis16. a32
}
