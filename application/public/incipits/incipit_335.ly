 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 8
  \set Score.skipBars = ##t
  r8
  R1*5
  r2 r4 r8 g'16 fis
  g8 g, g g'16 fis g8 g, g g'16 fis g8 fis16 e d[ c b a] g fis g a b[ a b cis]
  d8
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Adagio"
  \time 4/4
  b4. fis16 e dis4. e16 b'
  c4. b16 a b4. e16 g,
  a4. fis'16 a, g4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Vivace [tutti]"
  \time 3/8
  g'8 fis r
  e d r
  a' c, r
  b a4
  d8 c c b16 g b d b d
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Vivace [Oboe]"
  \time 3/8
  g16 a b c d8
  a16 c c b b a
  b a b d c b
  a8 d,4
}