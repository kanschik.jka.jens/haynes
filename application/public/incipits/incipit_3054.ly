\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Seufzer, Tränen, Kummer, Not [BWV 21/3]" 
    }
    \vspace #1.5
}

\transpose c d  {
\relative c''' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\minor
   \tempo "o. Bez."
   \time 12/8
   \partial 8*9
   g8 es b b4 c8 as' f des
   c4 b8 c e g bes4 as8 bes, d f
   as4 g8
  }
}


\transpose c d  {

\relative c''' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key c\minor
   \tempo "o. Bez."
   \time 12/8
   \partial 8*9
   \set Score.skipBars = ##t
    r4. r2. R1.*6 r4 r8 g8 es b b c c as' f des
    c4 b8 r4 r8 
}


}
