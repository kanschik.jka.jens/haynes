\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  r8 d16 e fis8 g16 a b8 b, a a'
  fis d b'4
}
      

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 4/4
      << {
      % Voice "1"
      g'2 r8 d16 cis d b ais b
      d2 cis4
         } \\ {
      % Voice "2"
      <cis e>8 <cis e> <cis e> <cis e> <fis, d' fis>4 r
      <e b'>8 <e b'> <e b'> <e b'> <e a>4
} >>}
  
          