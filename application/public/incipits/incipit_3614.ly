
\version "2.16.1"

#(set-global-staff-size 14)


\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. ohne Satzbezeichnung"
  \time 4/4
  a8 fis16 e d8 g fis4 r8 a8
 b g16 fis e8 d cis4 r8 e8
 fis8 d16. cis32 d8 b16. d32 gis8 b e, d
 cis fis e gis, a b16 cis d4~ d8 cis16. d32 b8. a16 a4 r8
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 2/4
   \partial 8
 a8 d8. e16 e8. d32 e
 fis16 g a4 g8
 fis16 a d8 g, fis fis e r8

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Largo"
  \time 3/4
    b4 d fis g2.~ g8 fis e d cis a' fis4 d r4 r4 e4 e e4. fis8 d cis
 d e cis4. b8 b4 r4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. ohne Satzbezeichnung"
  \time 3/8
   d8 fis 16 g a8 e8 fis16 g a8
 d,8 g16 a b8 cis,8. a16 b cis
 d cis d e fis g 
e d e fis g a 
 fis e fis g a b e,4
}