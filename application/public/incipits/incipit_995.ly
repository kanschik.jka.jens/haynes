\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 4/4
  a'4. gis16 fis e8. e16 fis8. d16
  cis4 a r8. a16 d8. fis16
  e8. gis16 a8. b16 gis4. a8
} 