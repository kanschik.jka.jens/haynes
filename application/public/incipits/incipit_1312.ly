 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 4/4
  \partial 4
  g,4 c2 e8. c16 f8. d16
  g4.. e16 c8 r \grace e8 d8 c16 d
  e8 r f r g r a r
  b4 c8 r
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Menuetto"
  \time 3/4
  \partial 4
  g'8 e
  b c g4 e''8 c
   b d f,4 d8 f
   f16 e r8 g16 f r8 a16 f r8
   d2 r4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegretto scherzando"
  \time 2/4
  \partial 8
  c,8
  f c16 r a'8 f16 r
  g8 c c c
  a d d \grace c16 b16 a32 b
  b4 c8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Finale. Presto"
  \time 6/8
  \partial 8
  c8
  c r c c r c16 c
  c8 b a g r f
   f4 e8 d f a
   c,4. b8 r
}