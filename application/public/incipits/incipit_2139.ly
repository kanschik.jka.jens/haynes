\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                    \set Staff.instrumentName = #"Oboe 1"
                     \key g\minor
                      \tempo "o.Bez."
                      \time 3/4
                      d4 c a
                      bes8. c16 d4 g,
                      a8. bes16 c8 bes a g g2 es'4
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                    \set Staff.instrumentName = #"Oboe 2"
                     \key g\minor
                        % Voice 2
                       bes4 a fis g2 g4 g fis4. g8 g2 g4
                  }
>>
}
