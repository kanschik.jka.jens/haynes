\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio [Tutti]"
  \time 4/4
  g8 b16. d32 g8 r
  g, c16. e32 g8 r
  g,8 b16. d32 g8 r r16 a b a r a8 c,16
  b b' c b
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*7
   d1 d  d2 \fermata g,4 \fermata r8 d'
   g g~ g16 d \grace c16 b \grace a16 g
   \grace {c16[ d] } e8 d 8
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegretto [Tutti]"
  \time 2/4
  \partial 16
  g'16
  g8 d16. g32 g8 d16. g32
  g8. fis32 e d8 r
  r \times 2/3 { e16[ fis g] } c,16 b b a
  b8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegretto [Solo]"
  \time 2/4
  \partial 16
  r16
   \set Score.skipBars = ##t
   R2*47
  r4 r8 r16 g'16
  g8 d16. g32 g8 d16. g32
  g2~ g
  c4. c,8
  b32 c d c b16[ g'] a,32 b c b a16[ fis]
  g4.

}

