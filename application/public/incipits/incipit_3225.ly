\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Mein Erlöser und Erhalter [BWV 69/5]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key b\minor
   \tempo "o. Bez."
   \time 3/4
  \partial 4
  fis8. b16
  \grace b8 ais4. b8 \times 2/3 {g8 e fis}
  e4 d g8. fis16
  e8. d16 cis8. b16 \times 2/3 {cis'8 ais b}
  ais4. fis8
  
  
  
   
   
   
}

\relative c {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key b\minor
   \tempo "o. Bez."
   \time 3/4
   \partial 4
   \set Score.skipBars = ##t
r4 R2.*7 r4 r4 fis8. b16
  \grace b8 ais4. b8 \times 2/3 {g8 e fis}
  e4 d g8. fis16
  e8. d16 \grace d8 cis4. b8
  fis'2.~ fis~ fis4  r4 
   
   
}


