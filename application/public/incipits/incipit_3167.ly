\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Warum willst du so zornig sein [BWV 101/4]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key a\minor
                      \tempo "Vivace"
                      \time 2/2
                      % Voice 1
                    c16 a e'8~ e16[ c a' gis] a g f e d[ c b a]
                    b g d'8~  d16[ b g' fis] g f e d c[ b a g]
                    a[ f d'8]~ d16
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                      \key a\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                    r8 c16 b c8 d e16 d c b a[ g f e]
                    r8 b'16 a b8 c d16 c b a g[ f e d]
                    r8 a'16[ gis] a8 d
                       
                        
                
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Taille"
                     \set Staff. shortInstrumentName = #"Ob 3"
                      \key a\minor
                     \set Score.skipBars = ##t
                        % Voice 3
                r8 a16 gis a8 b c16 b a g f[ e d c]
                r8 g'16 fis g8 a b16 a g f e[ d c b]
                r8 f'16 e f8 a
                       
                        
                
                  }
>>
}


\relative c {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key a\minor
   \tempo "Vivace"
  \time 2/2
     \set Score.skipBars = ##t
   R1*8 
   \tempo "Andante [Choralmelodie: Vater unser im Himmelreich]"
   r4 e4 e c d e c b
   a r8 
   
    
    
    }