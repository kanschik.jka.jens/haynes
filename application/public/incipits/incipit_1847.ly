\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
    f,8 f16 e  f[ e f e] f8 c r f
    bes16[ g a f]  g e f d  c4 r16 c' bes a
    d8[ f]
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio"
  \time 4/4
    c,4 r16 c' bes a d8 f r16 d c bes
    c8 e r16 c bes a bes8 d r16 bes a g c8
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 3/8
      f16 c f c f c
      g' c, g' c, g' c,
      <c a'>4. <c g'> <c f> <c g'> <c a'>
}
