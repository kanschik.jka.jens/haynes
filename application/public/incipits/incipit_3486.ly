\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.bez."
  \time 2/2
    b4. g8
    d'2 d4 a
    c2. b8 c
    b2
}
