\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gayment"
  \time 2/4
  c8 c c b16 a
  g8 a16 b c d e f
  g f e f d8 g
  e d16 e c8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement"
  \time 2/2
  \partial 4
  g'4
  e d8 e f4 f
  d4. c16 d e4 e
  \grace d16 c4
  f d4. d8 f e d e c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gaiment"
  \time 2/4
  \partial 8
  c8 d c c b16 c
  d8 c c a'
  g4 f e r8 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 6/8

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gaiment"
  \time 2/4
  c4 g' c8 g16 f e8 d16 c
  b8 a16 g f'4~
  f8 b,16 c d e f g
  e8 f16 g f8 e
  e d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Affeueusement"
  \time 3/4
  g'2 a4
  b, c2 d8 c d4 g,
  f' e8 d e f g2 a4
  bes, a
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gigue Gay"
  \time 6/8
  \partial 4.
  r8 r16 g' f8
  e8. d16 c8 a'4 g8
  g4. c8. b16 a8 g8. e16 f8 g8. f16 e8
  d4 c8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gay"
  \time 2/4
  c8 c, c' c c16 b a g f e d c
  d'8 d, d' d d16 c b a g f e d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement sans leteur"
  \time 2/2
  e2 g
  c,2. e4
  a, f' d4. g8
  f e d e c4 g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gay"
  \time 3/8
  g'16 g a b c8
  e,16 e f g a8
  g  f16 e d c b c b a g8 
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Gay mais pas trop viste"
  \time 2/4
  g'4 es8. d16 c8 f16 g as8 as
  g f16 es d8 c b a16 b g8 g
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Gracieusement"
  \time 2/2
  \partial 2
  g'4 f
  es g f8 es d es
  c2 as'4 g~
  g f8 es d es c d c b a g
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Gay"
  \time 3/8
  r8 g' g
  c, c16 d es f g8 g g
  as16 g f es d8
  g c, r
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gay"
  \time 3/8
  r16 c c c d e
  f8 f f
  f e16 e f g a8 a4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Menuet 1"
  \time 3/4
  g'8 es c g' as g
  fis d g2
  e8 c f g as c,
  d bes es2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Menuet 2"
  \time 3/4
  e2 g4
  g d8 c d4
  e4. e16 f g4
  g c, f f e e
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Gay"
  \time 2/4
  c4 e g8 g g a16 b
  c8 c, c c'~
  c b16 a b8 b
  b e, a4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gayment"
  \time 6/8
  \partial 4.
  g'8. a16 g8
  e8. d16 c8 f4 f8
  f4. e8. f16 e8
  d4 g,8 c8. d16 c8
  b8. a16 g8
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key c\minor
                      \tempo "1. Gravement"
                      \time 2/2
                      % Voice 1
                      R1 R1
                      r4 g' f es d c b a g4.
                  }
\new Staff { \clef "bass" 
                     \key c\minor
                        % Voice 2
                        r4 c, bes as g f es d c2 r4 c
                        d2 r4 d
                        es2 r4
                  }
>>
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gayment"
  \time 2/4
  g'2 c, bes'8 bes bes16 a g bes
  a g f e d e f g
  e8 d16 e f8 e
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 6/8

}
