\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Vivace [Tutti]"
  \time 4/4
  c4 d e8 c c d
  e c r4 d e
  f8 d d e f d r 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Vivace [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*9 
   g4 c b r8 c
   d e f4 e r8 c
   d e f4 e r8 g
   g a, a f' f e r
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key g\major
                      \tempo "2. Tempo giusto"
                      \time 6/4
                      % Voice 1
                      R1.
                      r2. a4. g8 fis4
                      R1.
                      r2. g4. f8 e4
                  }
\new Staff { \clef "bass" 
                     \key g\major
                        % Voice 2
                        g,2 dis4 e2 g4
                        fis b, b r2.
                        g'2 dis4 e2 g4
                        e4 a, a r2.
                  }
>>
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. [Tutti]"
  \time 12/8
  c8 e g  c g e  c e g  c g e
  b' g d e c g d' g, g g4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. [Solo]"
  \time 12/8
     \set Score.skipBars = ##t
   R1.*6 
  c,8 e g  c g e  c e g  c g e
   a4 r8 g4 r8 f4 r8 e4 r8
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 4/4

}