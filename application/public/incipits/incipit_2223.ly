\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 4/4 
    \partial 8
    d8
    g8. g16 f16. g32 a16. c,32 b16. c32 d8 r
}
