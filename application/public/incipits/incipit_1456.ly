 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 2/2
    r8 a16 bes c8 d  c d16 e f8 f,
    bes16 a bes c bes4 a r8 c
    f16[ e f a]
}
 