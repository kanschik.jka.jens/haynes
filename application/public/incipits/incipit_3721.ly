\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Vivace [Tutti]"
  \time 4/4
  r8 e16 e  a b gis b a8 e16 d c d b d 
  c4 r r8 e16 d c d b d
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Vivace [Solo]"
  \time 4/4
  R1
  r8 e16 e a b gis b a4 r
  r r8 e16 e  a b gis b a e d c
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Grave e staccato [Tutti]"
  \time 4/4
  c4 c c c   cis r r2
  a4 a a a b r r2
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Grave e staccato [Solo]"
  \time 4/4
  R1
  r8 g' g8. a16 bes32 a g a bes a g a  bes a g f e f e f
  g f e d cis d cis b
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro [Tutti]"
  \time 6/8
  \partial 8
  r8
  r4. r8 r e
  e b gis e b' e
  c4 r8 r r e
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro [Solo]"
  \time 6/8
  \partial 8
  a'8
  a e c a e' a
  gis4. r4 a8
}


