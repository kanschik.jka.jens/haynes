\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 2/4
  \partial 8
  g16. c32
  e8 e d d
  c4 g8 c16. e32
  g8 g a a d,4 r8
}
