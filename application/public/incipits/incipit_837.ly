\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Andante"
  \time 2/2
  f16[ e d cis] d8 d d cis r f
  e[ cis d d] d cis r4
  cis16[ b a gis] a8 g g[ f16 g] a8 a
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. un poco Allegro"
  \time 2/4
  \partial 4
  f16 g32 a g8
  f[ r16 g] f8[ r16 g]
  f8 r e16 f32 g f8
  e8[ r16 f] e8[ r16 f]
}


\relative c'' {
  \clef treble
  \key d\minor
     \tempo "3. Largo"
  \time 3/4
  r4 c a'
  f8. e16 f2
  f2. f4 e8. f16 g4
  c, f8. g16 a4
  d,
}



\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro"
  \time 3/4
  a2 d4 cis8 d e cis d4
  d,8 f a d f a
  e f g e f4
}