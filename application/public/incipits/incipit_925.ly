\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 2/4
  c8. d16 \grace f16 es8 d16 c
  f8. es16 \grace es8 f4.
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro e spirituoso"
  \time 3/4
  r8 g'8 es c g es
  c c' \grace d8 c4. b16 c
  b8[ d]
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro assai"
  \time 2/4
  g'4 as
  \grace c,8 bes4. c8
  d[ es f d]
}
