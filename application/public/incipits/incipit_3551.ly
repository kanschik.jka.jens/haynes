\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
    bes,2 es4 d
    g f es d
    c c8 es es d c bes
    bes4. c8 bes4 c8 d
} 
