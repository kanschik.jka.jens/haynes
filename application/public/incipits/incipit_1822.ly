\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
    e4 r8 e d4 r8 d
    e d e f16 e
    d4 r8 d16 e
    f8 f f f f e4 d16 c
    b8 c c8. b16 c4
}
