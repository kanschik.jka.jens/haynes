\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/4
      bes8 d4 bes8
      f'4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Largo"
  \time 2/2
      \partial 8
      c16 d
      es8[ es] es d16 c g4. d'16 es
      f8[ f] f es16 d es4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/4
      bes8 bes' bes, bes' bes, bes'~ 
      bes a16 g f es d c bes4~ 
      bes8 bes' bes, bes' bes, bes'
}
