\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\relative c'' {
  \clef bass
  \key es\major
   \tempo "1. Adagio [Basso]"
  \time 4/4
  es,,16. g32 f16. g32 d8 bes'
  es,16. g32 f16. g32 bes,8 bes'
  c,16. es32 d16. es32 g,8 es'
  
  
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio [Oboe]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*2 

  bes4.~ bes16 es32 d es4. d8
  es8 f16 g g f32 es es16 d32 c c4
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  bes8
  es d16 c bes as g f es4 r8 bes'
  es16 bes es bes es d c bes c4 r8
}

\relative c'' {
  \clef treble
  \key es\minor
   \tempo "3. Adagio"
  \time 3/2
  bes2. as4 bes es,
  ces' bes bes f bes as ges2 es
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Allegro"
  \time 3/4
  \partial 16
  bes16
  g8. bes16 es8. es,16 f8. d'16
  es4 es,
}
