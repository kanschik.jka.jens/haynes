\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Phoebus deine Melodei [BWV 201/9]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key fis\minor
   \tempo "o. Bez."
   \time 12/8
   cis2.~ cis4.~ cis8 b d
   cis b a gis a fis \grace fis8 eis4. r4.
  
   
  
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key fis\minor
   \tempo "o. Bez."
   \time 12/8
   \set Score.skipBars = ##t
   R1.*8  cis2.~ cis4.~ cis8 b d
   cis b a gis a fis eis4.
}



