\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Andante"
  \time 4/4
  R1 R1
  r16 f16 es16. d32  es16. c32 d16. es32   d16. f32 es16. d32 es16. c32 d16. es32
  d16. c32 c16. bes32 bes16. a32 a16. g32 g8 c r
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/4
    d16 es f8 f4
    f8 g f es
    d16 es f8 f4
    f8 g f es
    d16 f bes a bes es, d c
}


\relative c'' {
  \clef treble
  \key g\minor
     \tempo "3. Largo"
  \time 3/4
    d8. es16 d4 d
    d8 fis, g a bes c
    d4 c8 bes a g
}



\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 3/4
  f16 es d8 f d g4
  f16 es d8 f d bes'4
  f16 es d8 f d g4
}