 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Andante"
  \time 6/8
  es,8 bes g es' bes g
  es'16 bes g bes es bes  g' es bes' g es' g,
  f8 d bes f' d bes
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 3/8
  es4 es,16 g
  bes8 g es
  g'4 es,16 g
  bes8 g es
  es'16 bes \grace c16 bes16 as32 bes es16 bes
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. "
  \time 3/8

}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. "
  \time 3/4
  \partial 4
  
}