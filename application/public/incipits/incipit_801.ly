 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major	
   \tempo "1."
  \time 2/2
  r4 d, fis d
  a'2. a8 g fis g fis g a4 g8 fis
  e4. e8 a4 g fis2 fis4
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2."
  \time 3/4
  r8 d d d e e fis d16 e fis8 e fis g
  a4.
}

\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "3. "
  \time 4/4
 fis,4 b2 ais4 b2 r4 d8 cis cis2 r4
}
\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4."
  \time 2/2
  r4 d4 r8 cis16 d e8 cis
  r4 d4 r8 e16 fis g8 e
}