 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro assai"
  \time 3/4
    bes'8 bes,4 bes8 a g
    f es d c bes a
    bes'' bes,4 bes8 a g
}

