\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "18. Duetto"
  \time 6/8
    g'8 g g  g a16 g f8
    f f f  f g16 f e8
    a16 g a g f e  d c b a g c
    d e b8. c16 c4.
}
