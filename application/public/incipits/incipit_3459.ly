\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
  <f, c' f>4 r8 c'8 d r e r
  <f, c' f>4 r8 e'8 f r g r
  <f, c' a'>4 r8 g'8 a r bes r
  
}
