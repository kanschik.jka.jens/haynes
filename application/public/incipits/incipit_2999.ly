\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 4/4
  c2~ c16 f, a bes  c a g f
  d' f, bes c d bes a g
  c f, a bes c a g f
}



