\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
     fis2
     a,8 fis g e
     fis4 b8 g fis d e cis fis'2 a,8 fis g e fis4
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andantino"
  \time 2/4
  r16 g b d r g, b d
  r e, a	 c r e, a c
  r d, g b r c, fis a
  r b, d g
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro assai"
  \time 6/8
  fis,8 fis fis   fis fis fis
  g g g   fis fis fis
  fis fis fis   fis fis fis 
  g e e   fis fis fis
}

