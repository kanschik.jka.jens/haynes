 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8
  e8
  e4 f e16 d c b  a b c d
  e4 f
}
