\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Lamentatio I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. grave"
  \time 4/4	
  r2 as8 as as as
  as16 g g f f bes bes as g8 g g g
  g16 f f e e as as g f8 f f f
 
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Lamentatio II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "Affetuoso"
   \time 4/4
   r4 g2 fis8 a
   bes8. a16 g8 d bes'16 a g f e8 cis'
   d4. c16 bes a8 a a8. a16
   g4 r r2
}
