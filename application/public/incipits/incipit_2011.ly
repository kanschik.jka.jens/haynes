\version "2.16.1"
         #(set-global-staff-size 14)

         
         \relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. o.Bez."
  \time 2/2
  r8 a16[ b] c8 c16 d e8 e e e
  a4 r8 a  g[ g] g f16 e
  f4 r8 f e[ e] e d16 c
  g'4 r4 r8 d16 e
  fis8[ fis16 gis] a4
}
         