\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Vivace [Violin]"
  \time 2/2
  c8 b a g   f e d c
  f' f, b b, e' e, a a,
  d' r f r b, r d
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Vivace [Oboe]"
   \set Score.skipBars = ##t
  \time 2/2
  R1*3
  r2 g16[ g g g] g g g g
  b[ g g g] g g g g d'[ g, g g] g g g g
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 2/2
      << {
      % Voice "1"
      e1~ e2 e8. d16 c8. b16 c8. b16 a4
         } \\ {
      % Voice "2"
      a8. gis16 a8. b16  c8. b16 a8. c16
      b8. a32 gis a8. b16 e,4 r
      } >>
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Menuet alternat"
  \time 3/4
  c4 g' c,
  c g' c,
  c a' g
  f2 e4
}

