 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 4/4
  g'8 g~ g32[ a g a] b16 d, d32 c b c c4 b8
  b16 a a e'  e d d fis, \grace a8 g4
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Presto"
  \time 2/2
  \partial 4
  d4
  g4. fis16 e d4 d
  d2. dis4
  e4. d16 c b4 b b2. b4
  c b8 a a g g fis g4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Allegro"
  \time 3/4
  \partial 4
  g4
  d'8.[ \grace { d16[ c]} d16] e4. fis16 g
  d8 g fis g b, c
  d8.[ \grace { d16[ c] } d16] e4. fis16 g
  d4
}
