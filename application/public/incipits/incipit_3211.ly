\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Ach schläfrige Seele, wie ruhest du noch? [BWV 115/2]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key e\minor
   \tempo "Adagio"
   \time 3/8
   \partial 8
   \set Score.skipBars = ##t
  e8 
  c8. b16 a8 
  g fis16 e b'8
  \grace d,16 c8. b16 c8
  a'4 fis8
  \grace e16 dis8. c'16 b8
  g e c
 
                                                                                   

}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key e\minor
   \tempo "Adagio"
   \time 3/8
   \partial 8
   \set Score.skipBars = ##t
 r8 R4.*31 r8 r8 e8 
  c'8. b16 a8 
  g fis16 e b'8
  r8 d8 r8
  
  
       
}



