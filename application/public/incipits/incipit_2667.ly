 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Ouverture"
  \time 2/2
  r4 r8 d e d c b
  c2 b4. b8
  b4. c8 a4.
  \time 6/8
  \partial 8
  b8
  b' b,16 b b b  a'8 b, b b b
  g'8 fis g e e16 dis e fis
  g8
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. Rigaudon"
  \time 2/4
  b8 c b a
  b e16 dis e8 b
  c b a d
  g,
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Carillon"
  \time 4/4
  e8 g, b e g fis16 e
  fis8 b, dis fis a g16 fis
  g8
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "4. Air. Tendrement"
  \time 2/2
  \partial 8
  e8
  e, e' dis e b g'16 fis e8 c16 b
  a8 fis'16 e dis8
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "5. Gig"
  \time 6/8
  \partial 8
    b8
    e dis e e dis e e4.~ e4 b8
    a b g fis g a
    g
}
\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "6. Menuett"
  \time 3/4
  b4 b e
  d2 fis,4
  g8 a b4 a g r r
}
