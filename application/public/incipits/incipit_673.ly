 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Marche du Huron"
  \time 2/2
  \partial 8.
    g16 a b
    c2 e
    g4 f e d
    c2 g a4 a \grace d8 c4 b8 a g4. a8 g2
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Amoroso"
  \time 2/2
  \partial 2
  c4 e
  d8 c b c d e f d
  \grace c8 b4 \grace a8 g4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Allegro"
  \time 6/8
  c4 e8 d4 e8
  c4 d8 c4 b8
  a4 g8 a4 b8 c4. g4 r8
}

