\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Ich halt es mit dem lieben Gott [BWV 52/5]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key bes\major
                      \tempo "o. Bez."
                      \time 3/4
                      \partial 8
                      % Voice 1
                      f8 bes a bes4. f8
                      g f g4. d8
                      es d c bes g' f 
                      d4 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key bes\major
                     \set Score.skipBars = ##t
                        % Voice 2
                    d8 f es f4. d8
                    es d es4. bes8
                    bes as g bes bes c 
                    bes4
                       
                        
                
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 3"
                     \set Staff. shortInstrumentName = #"Ob 3"
                     \key bes\major
                     \set Score.skipBars = ##t
                        % Voice 3
                  bes8 d c d4. bes8
                  bes as bes4. f8
                  g f es f es c
                  f4
                       
                        
                
                  }
>>
}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key bes\major
   \tempo "o. Bez."
  \time 3/4
  \partial 8
     \set Score.skipBars = ##t
    r8 R2.*15  r4 r4 r8 f8
    d'4. es8 c bes
    es16 f g8 f4. 
    
    
    }