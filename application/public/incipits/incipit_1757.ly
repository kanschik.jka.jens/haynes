\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 3/8
  bes8 f'16 es d c bes8 c4
  d8 f16 es d c
  bes8 c4
}
