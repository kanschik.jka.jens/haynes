\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro moderato"
  \time 4/4
    d'2 a
    g4 fis16 fis g e d fis a8~ a16 a b g
    fis a d8~ d16 cis b a \grace a8 g8 fis r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Cantabile"
  \time 4/4
  \partial 4
  g4
  d' d~ d8 e16 fis \grace a16 g8 fis16 e
  cis8 d d4
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Thema con Variazioni"
  \time 2/4
    d8 fis e d
    e8. fis32 g fis8 r
    b d, g e
    cis8. d32 e d8 r
}