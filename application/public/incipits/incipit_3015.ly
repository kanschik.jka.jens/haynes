 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
 \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #""
                     \key d\minor
                      \tempo "3. Andante"
                      \time 3/4
                      % Voice 1
                      d2.~ d4. e8 f4
                      \grace f8 e4 d8 cis \grace cis8 d4
                      \grace { e32[ f] } g4 \grace g8 f4. e8
                      f8. e16 d8
}

\relative c'' {
  \clef alto
  \set Staff.instrumentName = #"Alto"
  \key d\minor
   
  \time 3/4
     \set Score.skipBars = ##t
     R2.*16
                        a2.~ a2 a8 g
                        f e d4 r d2.~
                        d4 e f

}