\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
    bes4 c8 r d4 es8 r
    d4 c8 r bes4 a8 r
    bes4 c
}
