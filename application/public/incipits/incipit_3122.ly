 
\version "2.16.1"
 
#(set-global-staff-size 14)
 \layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Recitativ (Soprano) - Er hat uns allen wohlgetan [BWV 244/57]" 
    }
    \vspace #1.5
}
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe da Caccia I"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key e\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                    b16 a b8~ b16 g fis g a g a8~ a16 fis e fis
                    g fis g8~ g16
                   
               
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe da Caccia II"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key e\minor
                        % Voice 2
                      g16 fis g8~ g16 e dis e fis e fis8~ fis16 dis cis dis
                      e dis e8~e16
                        
                  }
                  
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Sopran"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key e\minor
                        % Voice 2
                     r8 e'8 e b c a fis8. b16
                     g4 r8
                        
                  }
>>
}



