 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Fuga"
  \time 2/2
  c8 c16 c bes c a c g c f, c'
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Serenade"
  \time 2/2
  \partial 8
  f,8
  f4 r8 f g16 f g a g a bes c
  a4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Menuet"
  \time 3/4
  f,4 f g a g bes a4. bes8 c4
  d g,4. f8 e4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Aria"
  \time 2/2
  c4 f e r
  c f e c8. bes16 c4 d8[ a]
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "5. Gigue"
  \time 6/8
  \partial 8
    c8
    a8. g16 a8 bes8. c16 bes8
    a4 c8 c8. d16 c8
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "6. Marche de Post"
  \time 2/2
  \partial 4
  c,4 f f8 f f g f g
  a4 a8 a a bes a bes
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "7. Menuet de Post"
  \time 3/4
  f,8 g a bes c d
  c bes c d c bes
}