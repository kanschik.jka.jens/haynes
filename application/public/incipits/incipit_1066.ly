 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 4/4
    b4~ b16 e, g e  c'4~ c16 a fis d
    b'4 e~ e8 fis16 e dis8. e16
    e8. d16 c8 b a16 b g fis fis8. e16
    e8
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  b8
  g16 fis e e' fis8. g16 dis4 r16 g g fis
  fis e e d d c c b b a a g g g' fis e 
  dis8 e16 fis fis8. e16 e4
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Largo"
  \time 3/4
  e8 dis e4. fis8 dis cis b4. e8
  c b c4 d
  b8 a b4.
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "4. Vivace"
  \time 3/8
    b8 e16 dis e fis
    g4 fis8
    e16 dis e g fis e dis4 r8
}