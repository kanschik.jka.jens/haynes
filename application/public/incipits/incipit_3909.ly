 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #""
                     \key d\major
                      \tempo "Dolce"
                      \time 3/8
                      % Voice 1
                        fis,16 g a8 a
                        a g16 fis g e
                        fis16 g a8 a
                        a g16 fis g e
                        fis8 b a r b a
                        r fis' e \grace d16 cis8 b16 a r
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob"
                     \key d\major
                      % Voice 2
                      d,16 e fis8 fis
                      fis e16 d e cis
                      d16 e fis8 fis
                      fis e16 d e cis
                      d4 r8
                      e4 r8
                      fis8 gis4
                      a r8
                  }
>>
}


\relative c'' {
  \clef "treble_8"
  \set Staff.instrumentName = #"Tenor"
  \key d\major
   \tempo "Dolce"
  \time 3/8
     \set Score.skipBars = ##t
      R4.*18
      fis,,16[ g a8] a
      b[ a] d
      b[ a] d
      cis16[ d e8] a,
      
}