\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro ma non tanto"
  \time 4/4
  \partial 4
   e8. f16
   g4.. c,16 \grace e16 d8. c16 d8. e16
   c4 r r2

}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 2/2
  f,4.. c16 a'8 r r8. a,16
  bes4. bes32 c d c bes8 r r4
  g'4.. e16 bes'8 r r8. bes,16
  a4. a32 bes c bes a8 r r4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Menuetto. Moderato"
  \time 3/4
  \partial 4.
  g'8 a fis
  g g e g d e
  c4 g8 c d e 
  \times 2/3 { f8[ g e]} \times 2/3 { f8[ g e]} \times 2/3 { f8[ g e]}
  f4 r
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "Trio"
  \time 3/4
  \partial 4
  c4
  c'4. a8 bes g
  f4 f f
  g4. g8 \grace bes8 a8 g16 a
  bes4 g g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Rondo"
  \time 6/8
  \partial 8
  g'8
  e4 e8 d e d c d c g r c
  d4 d8 e4 e8
  g4 fis8 f r
}