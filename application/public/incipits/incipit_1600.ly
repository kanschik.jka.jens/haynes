\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  f2 g4. a8 \grace a8 bes4 bes2 a4
  \grace a8 g4 f2 e4
  f8 \grace g16 f16 e f8 g a4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 3/4
  f4 bes16 a g f f8 f
  f8. d16 f4 r8
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo"
  \time 4/4
  f2 bes8 a g f
  c'4 c c2
  bes8. c16 a8 r g8. a16 f8 r
}

