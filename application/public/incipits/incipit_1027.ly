
\version "2.16.1"

#(set-global-staff-size 14)

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Lento"
  \time 6/8
  \set Score.skipBars = ##t
   R2.*2 g4.~ g8 fis a
g c g \grace f8 e8 d c 
f4.~ f8 a g e2.~ e8 

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Aria "
  \time 4/4
  f1 d2 bes'2 e,,4~ 
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
   \times 2/6 {e16[ f g a bes c] }
    \times 2/7 {d16[ e f g a bes c] } d8[ e,8]
}

