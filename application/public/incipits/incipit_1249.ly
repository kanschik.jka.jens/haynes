\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'Lass ein sanftes..'" 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \key d\minor
                      \tempo "o. Bez."
                      \time 3/4
                        r4 f, g a d, d'
                        c d8 c bes c a4 f
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key d\minor
                     \set Staff.instrumentName = #"Oboe 2"
                        % Voice 2
                       r4 d e f g f g e g c,2
                  }
                  


>>
}
