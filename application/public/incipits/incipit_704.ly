\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro "
  \time 6/8
  \partial 8
  r8
  r4 r8 r8 r8 g
  \grace { a16[ b] } c4 c8 c4 c8 c4.~ c4 c8
  d4 e8 f4 g8
  e4.
}
\relative c'' {
  \clef treble
  \key c\minor 
   \tempo "2. Grave"
  \time 3/4
r4 r4 c
es4. d8 c4
\grace c8 d2 g,4
c8 b c es d c
b2.
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro assai"
  \time 3/8
  \partial 16
  c16
  c8. g16 a b c g c g d' g,
  e' c e c f c
  g'8 f16 e d c
  b8.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Rondeau. Allegro "
  \time 3/4
e2 f4
g c, c c c c
e8 d c b c4
}
