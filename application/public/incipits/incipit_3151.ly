\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Et in Spiritum sanctum Dominum [BWV 232ii/7]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key a\major
                      \tempo "o. Bez."
                      \time 6/8
                      % Voice 1
              r4 r8 a4 b8
              cis d e d cis b
              cis d e d cis b
              cis e a d, cis16 b cis8
              b4.~ b8 cis dis
              e
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key a\major
                     \set Score.skipBars = ##t
                        % Voice 2
                   r4 r8 e,4 gis8 
                   a b cis b a gis
                   a b cis b a gis
                   a cis e, gis4 a8 
                   gis2.~ gis8 
                       
                        
                
                  }
>>
}


\relative c {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key a\major
   \tempo "o. Bez."
  \time 6/8
     \set Score.skipBars = ##t
    R2.*12 r4 r8 a4 b8 cis d e d cis b
    cis d e d cis b
    cis16 d e8 a, d cis16 b cis a 
    b4 e,8 r8
    
    
    }