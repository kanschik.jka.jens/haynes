\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Twelve Minuets" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Minuet 1"
  \time 3/4
    d2 c16 b a g
    a4. b16 c b4
    \times 2/3 { a8 c a} g4 g
    \grace fis4 g2 r4
    
  
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Twelve Dances" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "Dance 1"
  \time 2/4
    c8 \times 2/3 { g16 a b} c16 g e c
    a' b c d c b a g
    c g a bes a f' a f
    e d c b c4
}

