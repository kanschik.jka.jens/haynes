\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 16
  g16
  d'1 e
  fis8 g a b c a g fis
  g4 g, r2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 2/4
      c8 c~ c32 d e f g16 f32 e
      \grace e16 d16 c32 d d8~ d16 d e f
      e g r g32. e64 f16 a r g64 f e d e16[ g] 
}



\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/8
  \partial 16
  d16
  g4. a \grace c16 b8 a16 g fis e
  \grace e16 d8 c16 b a g
  \times 2/3 { a16[ b c] } b8[ a]
  \times 2/3 { b16[ c d] } d8 r
  
}
