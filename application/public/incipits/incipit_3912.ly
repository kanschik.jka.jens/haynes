\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria " 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \key g\major
                      \tempo "Aria. Cantabile"
                      \time 4/4
                      d8 e16 fis g g, g8
                      d'8 \times 2/3 {c16[ b c] } \grace c8 b4
                      a16 a32 b c8  b16 b32 c d8 
                      c16 b c d  e d d8
                      % Voice 1
                  }
                  
\new Staff { \clef "treble" 
                     \key g\major
                     \set Staff.instrumentName = #"Voice"
                        % Voice 2
                       R1
}

>>
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key es\major
   \tempo "o. Bez."
   \time 2/2
   r4
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key es\major
   \tempo "o. Bez."
   \time 2/2
   r4
}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef bass
  \set Staff.instrumentName = #"Basso"
  \key g\minor
   \tempo "o. Bez."
   \time 4/4
   r4
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key g\minor
   \tempo "o. Bez."
   \time 4/4
   r4
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria" 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key bes\major
                     \set Staff.instrumentName = #"Oboe 1"
                     \tempo "o. Bez."
                     \time 3/4
                      % Voice 1
                      r4
                  }
\new Staff { \clef "treble" 
                     \key bes\major
                       \set Staff.instrumentName = #"Oboe 2"
                        % Voice 2
                        r4
                  }
                  
\new Staff { \clef "treble" 
                     \key bes\major
                       \set Staff.instrumentName = #"Tenor"
                        % Voice 2
                      r4
}

>>
}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria " 
    }
    \vspace #1.5
}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "o. Bez."
  \time 2/2

}
