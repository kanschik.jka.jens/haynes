 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Spirituoso"
  \time 3/4
   \override TupletNumber #'stencil = ##f
  e,8. e16 e8. e16 e8. \times 2/3 { fis32[ g a]}
  b8. b16 b8. b16 b8. c32 d
  e8. e16 e8. e16 e8.  \times 2/3 { fis32[ g a]}
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. Grave"
  \time 4/4
  g'8. g16 g8. g16 g4 r
  fis8. fis16 fis8. fis16 fis4 r
  b8. b16 b8. b16 e,4 a
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Presto"
  \time 4/4
  b2 e,4 c'
  dis,2 r8 e e d
  c a a'2 g4
  fis2 e4 r
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Grave Andante"
  \time 4/4
  g8 g g g  g4 fis
  b8 b b b   b4 a
  d8 d d d  d4 c8. b16
  a b b c  c b b a
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "5. Allegro assai"
  \time 3/8
  e16 dis e fis g e
  fis8 b, b'~
  b a4~
  a8 g4~
  g8 fis4
  b,8 cis16 dis e fis
  g8 fis e
}
