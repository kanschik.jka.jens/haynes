\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Drum wenn der Tod zuletzt den Geist [BWV 111/5]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key a\minor
                      \tempo "Recitativ/Adagio"
                      \time 4/4
                      % Voice 1
                      c1~
                      ~c2 cis
                      d f 
                      a
                      
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key a\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                        g,1~
                        ~g1 a2~ a4 c~ c2 
                       
                  }
                  
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Sopran"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key a\minor
                     \set Score.skipBars = ##t
                        % Voice 3
                        r8 g8 c8. d16 bes8 bes d bes
                        g4 r16 g16 c e g g, g a bes8 a f'4
                       
                  }
                  
>>
}

