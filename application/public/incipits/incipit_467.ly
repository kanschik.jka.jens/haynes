 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. ?"
  \time 4/4
    r8 bes f' g f16 bes, a bes es8 d
    c r16 d c f e f c8 r16 d c f e f
    bes,8 a
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 2/2
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Largo"
  \time 6/8
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}