 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 3/4
    es4 f es d es r8 bes
    as d f4. as,8 g f es4 r8
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Allegro"
  \time 2/4
  es4 f8 es16 d es4 r8 bes
  es, g bes d
  es g4 f16 es f8 as4 g16 f
  g f es4
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Adagio"
  \time 3/2
  es2. bes'4 g es
  f2. bes4 f d
  es2. g4 f es
  d c bes2.
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "4. Allegro"
  \time 3/8
    es8 d16 c bes8
    g' f as
    as g f g16 f es d es8
}