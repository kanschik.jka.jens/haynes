\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Gott, man lobet dich in der Stille [BWV 120/1]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key a\major
                      \tempo "o. Bez."
                      \time 6/8
                      % Voice 1
                      cis8. d16 b8 d cis r8
                      gis8. b16 a8 d, cis r8
                      cis'8. d32 b cis16[ d32 b] cis8. d32 b cis16[ d32 b] 
                      cis16 e e d  d cis cis b b4
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key a\major
                     \set Score.skipBars = ##t
                        % Voice 2
                        a8. b16 gis8 fis e r8
                        b8. d16 cis8 b a r8
                        a'8. b32 gis a16[ b32 gis] a8. b32 gis a16[ b32 gis]
                        a16 cis cis b b a a gis gis4
                      
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key a\major
   \tempo "o. Bez."
  \time 6/8
     \set Score.skipBars = ##t
   R2.*10 a8 r16 e16 fis16[ a32 gis] a8. b32 gis a16 cis32 b
   cis8. d32 b cis16 e32 d e8~ e32 d cis e d cis b d
   

}