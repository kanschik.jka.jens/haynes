
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio I"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
  \tempo "1. Allegro"
  \time 6/8
  c16 b c4 c16 b c4 c16 c' b a g f e a g f e d c b c4 c16 b c4 
 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Minuetto"
  \time 3/4
g8. c16 g4 f \grace f8 e2. e8. c'16 e,4 d c2 r4 

 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio II "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. ohne Bez."
  \time 4/4
 \partial 4
 bes4 f'2 es8 c \times 2/3 {a8 c es} \grace es8 d2. bes8 g \grace g8 f2.
 
 
 
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Tempo di Minuetto"
  \time 3/4
 f8 d d2 es8 c c2 g4. bes8 a c bes c d es f r8

}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio III "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Moderato"
  \time 4/4
 a1 d2. e16 d cis b b8 a a2. 
  
 
 
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Minuetto"
  \time 3/4
 e2 cis8 b16 cis a2. fis'4. \grace a8 gis8 \grace b8 a8 \grace gis8 fis8 e cis a4 r4
  
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio IV "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
 g2 b4. c8 d8 d d d d g16 d b' g d' b \grace b8 g8. fis16 g4 r4
 
 
 
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Minuetto"
  \time 3/4
 g4 d'2 d16 b g' e d2 
\times 2/3 {d,8 g b} \times 2/3 {d8 g b} \times 2/3 {d8 b g}
d16 b g' e d2

 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio V "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Moderato"
  \time 2/2
 c1 \grace cis8 d1 bes'2 \grace f8 e2
 f16 e f g f8 r8 
 
 
 
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Tempo di Minuetto "
  \time 3/4
 f8 c c2
\grace e8 d8 c c2 r8 bes'8 a g f e f c c2
 
}
		

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio VI "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Poco Andante"
  \time 3/4
 g8 g4 e4 f16. g32 a8 a4 c16 b d c b a g8 g4

 
 
 
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Poco Allegro"
  \time 6/8
  g4.~ g8 e f 
 g a b \grace d8 c b a \grace a8 g4. f8 e f g a b \grace d8 c8 b a \grace a8 g4.
 
}

