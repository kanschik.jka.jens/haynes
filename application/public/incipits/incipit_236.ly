\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key g\major
    \time 3/8
    \tempo "1. o.Bez."
      g4 a8
      b g c  b g c
      a4. fis4 g8
      a fis b a fis b
      g4 r8
}

 