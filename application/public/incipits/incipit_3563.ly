 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Tempo Comodo"
   \time 4/4
      f2. a8 g16 f
      c'4 c2 e,4
      \grace g8 f8. e16 f4 f16 c d e f g a bes
}


\relative c'' {
  \clef treble
  \key bes\major
       \tempo "2. Adagio"
   \time 4/4
   \partial 4
    f8. a,16
    bes8 bes~ bes16 bes d f \grace f8 es4 r16 g \grace f16 es16 \grace d16 c16
    c8 c~ c16 c d es \grace es16 d4

}
\relative c'' {
  \clef treble
  \key f\major

   \tempo "3. Allegro"
   \time 2/2
      f4 a a2~
      a bes4 g
      a c c2~ c a4 f
      \grace f4 e1 f4 a
}
