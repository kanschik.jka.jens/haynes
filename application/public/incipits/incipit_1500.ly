
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4
  c1~ c8 b16 a b as' g f es8 es4 d16 c
  as'4. g16 f f8 es r8
  
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key c\minor
                      \tempo "2. Allegretto"
                      \time 4/4
                      % Voice 1
                  R1 r2 r8 d8 g4~
                  g16 es es d32 c d16 f f es32 d es16 c c b32 a b16 d d c32 b
                  c16 c8 d16 c8 b c4 r4

                  }
\new Staff { \clef "bass"
                   \key c\minor
                        % Voice 2
                  c,,8 es f g c, c' b g
                  c es, f fis g g, r16

                  }
>>
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro assai"
  \time 3/4
   r4 c es
  d g, g' c, f2~
  f4 es8 d es4
  d4. es16 f es8 d es
}



