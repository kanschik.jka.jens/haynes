\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Naivement"
  \time 3/4
  e2 d4
  c g' f e d c
  b2 g4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gracieusement"
  \time 2/2
  \partial 2
  e4 \grace d8 c4
  g' \grace f8 e4 d4. c8
  b4 g c d
  e8 d c d
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Tres gayement"
  \time 2/2
    \partial 2
    c4 c
    d e8 f g4 g
    e d f e
    d c d8 c b a b4 g
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Gracieusement"
  \time 2/2
  \partial 2
  g'4 c,
  as'4 g f4. g8
  es4 d es d8 c
  f4 es d4. c8 b4 g
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 2/4
  c8. g'16 f8. g16
  e4 c
  g'8. c,16 a'8. b16 c4 g
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VI" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Gracieusement"
  \time 2/2
  \partial 2
  g'4 b
  a g8 fis g4. a8
  fis4 \grace e8 d4 g8 fis e d
  e4. f8
  d4. c8
  b4 \grace a8 g4
}
