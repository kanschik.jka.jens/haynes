 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 3/4
    r8. d16 g4 g
    g8 fis16 e fis4 r
    r8. d16 a'4 a 
    a8 g16 fis g4 r 
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Largo"
  \time 3/4
   f4 \grace es16 d4 \grace c16 bes4
   a16 c es g g4. f16 es
   d4 \grace f16 es4 \grace d16 c4
   a bes  r8
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Presto"
  \time 2/2
  d2 g, g fis
  es' fis, fis g g' f f es4 d
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Menuett"
  \time 3/4
  \partial 8
  
}