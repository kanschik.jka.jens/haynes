\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro ma non presto"
  \time 4/4
      es16 g,8 bes16  as c bes f  g es g bes  es g f d
      es g,8 bes16 as c bes f g8 d es r
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio con affetto"
  \time 3/8
     \set Score.skipBars = ##t
     c8 g es c \grace c'8 es4 d r8
     R4.*10 
    c4. c8 es16 es es es
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro molto"
  \time 6/8
       es8 bes as  g  es16 f32 g as bes c d 
       es8 bes as g
}
