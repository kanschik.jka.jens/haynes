\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  g8
  c8. d16 es8 f g es16. d32 c8 g'
  as8. g16 f8 es d4 r8 d
  es c16. b32 c8 es
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
    g'8
    c, b c d  es d es f
    g f g as
    g c,4 as'8 g c,4 as'8 g c,4
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Vivace"
  \time 3/8
    g'8 c, b
    c c16. d32 es8
    d8 d16. es32 f8
    es8 c16. d32 es16. c32
      
}

