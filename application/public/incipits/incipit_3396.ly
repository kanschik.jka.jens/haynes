\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key bes\major
    \time 2/4
    \tempo "1. Affetuoso"
          bes4. d16 bes
          \grace c8 bes16 a a8 r4
          es'4. g16 es
          \grace f8 es16 d d8 r4
}


\relative c'' {
  \clef treble  
  \key bes\major
    \time 2/4
    \tempo "2. Allegro"
        \partial 8
        f,8
        bes4. d8
        f4. d8
        bes8. c32 d  c8. d32 es
        \grace es8 d4 r
}

\relative c'' {
  \clef treble  
  \key bes\major
    \time 3/4
    \tempo "3. Menuet"
        bes2 c4
        d8 bes es4 r
        d8 bes es4 r
        d8 bes g'4 f
        \grace f8 es2 d4
        
}
 