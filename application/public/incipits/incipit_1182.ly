 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Larghetto"
  \time 4/4
  f2~ f16. e32 d16. c32 bes8. a16
  a16. g32 f8 r a16. bes32 c8 cis a'16. g32 f16. e32
  d2~ d16. cis32 d16. e32 e8. d16
  d4
}
