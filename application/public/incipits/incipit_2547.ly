 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 4/4
  <g e' c'>4 r <g d' b'> r
  <e c' a'> r r c'8 e~
  e a, d d b a' g f
  f e d16 f e d \grace d8 c4 \grace c8 b4

}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. "
  \time 2/2
  \partial 2

}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. "
  \time 2/4
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}