 \version "2.16.1"
         #(set-global-staff-size 14)
         
 \relative c'' {
  \clef treble
  \key a \major
   \tempo "1. Tempo giusto [Tutti]"
  \time 4/4
  e,16 \f (d) cis4 cis8 ~ cis cis4 cis8~ cis \p cis4 cis cis cis8 a16 fis'8. d4 d8
 } 
  \relative c'' {
  \clef treble
  \key a \major
   \tempo "1. Tempo giusto [Solo]"
  \time 4/4
  e16 d cis4 cis8 cis16. \trill b32 cis16 d cis d e cis \grace {b} a8.\trill g16 a8 a a16 (b32 cis d e fis gis)
  }
  
   \relative c'' {
  \clef treble
  \key c \major
   \tempo "2. Andante [Tutt]"
  \time 4/4
  c8 e \tuplet 6/4 {r16  b c d c b} c8 e \tuplet 6/4 {r16 b c d c b} c16 g'\trill a e-. f (d) g-. f-.
   }
   
    
   \relative c'' {
  \clef treble
  \key c \major
   \tempo "2. Andante [solo]"
  \time 4/4
  \partial 8 g8 c8. g'16 \tuplet 3/2 {f d c} \tuplet 3/2 {b f d} \tuplet 3/2 {e( c' e)} g8 \tuplet 3/2 {c,,16 (e g)} \tuplet 3/2 {bes (a g)} a16 (d) cis-.
   }
   
    \relative c'' {
  \clef treble
  \key a \major
   \tempo "3. Vivace"
  \time 2/4
  \partial8 e8~ e cis4 d8~ d b4 cis8 a8.\trill gis32 a b8 gis a e r4
    }