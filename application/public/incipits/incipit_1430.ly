 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 4
  g'4
  c8 g g2 \grace b8 a8 g16 f
  f8 e e2 \grace g16 f8 e16 d
  c4 c d d
  e4. f16 e d4 r8
}

 \relative c'' {
  \clef treble
  \key f\major
   \tempo "2. o.Bez."
  \time 2/4
      c4. f16 d
      c[ c] bes32 a g f g8. a32 bes
      bes16 a a8
}
 
 \relative c'' {
  \clef treble
  \key c\major
   \tempo "3. o.Bez."
  \time 3/4
    e4 e8. f16 g4
    c,4 c8. d16 e4
    f4. d8 e c
    g a b c d dis
}
 