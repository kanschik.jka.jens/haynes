\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
      \time 4/4	
        d4 cis8. b16 a4 a'16 fis d a
        b4 a8. g16 fis4 fis'16 d a fis
        g4 fis8. e16
}
