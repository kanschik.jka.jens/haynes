 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Affetuoso"
  \time 4/4
    es8 g, c b16 c d8 g, d' c16 d
    es8 g, c b16 c d8 g, r4
}


\relative c'' {
  \clef alto
  \key c\minor	
   \tempo "2. Allegro"
  \time 4/4
  r8 g g g es g d g
  c, f f f d f c f
  bes, es es es c es bes es
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Adagio"
  \time 4/4
  des2 r8 des des des
  c2 r8 c c c
  c4 d es8 es16 d es8 es
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Vivace"
  \time 3/8
  es4 d8 es4 d8 es16 f g8 f
g as16 g f es
f8 g16 f es d
es8 f16 es d c
}