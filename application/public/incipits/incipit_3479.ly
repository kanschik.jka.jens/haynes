\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "[Allegro]"
  \time 3/4
  a4 d4. e8 cis2 d4 e4. g8[ f e]
  f2 f8. g16
  bes,2 g'8. a16
  a,2
}
