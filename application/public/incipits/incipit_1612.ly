 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. o.Bez."
  \time 3/4
  \partial 4
  e,4
  gis16 e dis e  gis8 e a e
  b'16 e, dis e b'8 e, cis' e,
  dis'[ fis, e fis]
}

