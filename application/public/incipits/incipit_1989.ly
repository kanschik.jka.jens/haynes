\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
  d16. es32 d16. es32   c16. d32 c16. d32
  bes16. c32 bes16. c32
  a16. bes32 a16. bes32
  g4 r
    d'16. es32 d16. es32   c16. d32 c16. d32
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 2/2
  d16 es d es  d es d es
  d es d es  d es d es
  }

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. [Adagio]"
  \time 3/2
  d4 es   d es  d es
  d2 d, r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Gigue"
  \time 6/8
  \partial 8
   d,8 g4 bes8 a4 c8
   bes4 d8 c4 es8
}
