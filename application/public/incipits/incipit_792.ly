\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allemande"
  \time 2/4
  \partial 16
  e16
  e f g a  g8 c
  e,16 d f g  e8 a
  g a16 g  f8 e 
  d4 g,
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Premiere Musette. Tendrement"
  \time 3/4
      e4 f g
      b,2 c4 f8 e d c b c
      d2 g,4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Les Petits Coups. Fort vite"
  \time 2/4
    c4 e d8 g g f
    e4 d
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Sarabande"
  \time 3/4
      g'4. f8 e d
      e4 \grace d8 c4 f
      e d4. c8 b2 g4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Premier Menuet"
  \time 3/4
      e8 c e g e c
      c'2 b4
      c c, d
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "6. Premiere gavotte"
  \time 2/2
    c'4 c, e8 f g a g4 f8 e
    d4 g e c g'4 f8 e a4. g8
}

