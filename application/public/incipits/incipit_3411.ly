\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  c,16 d
  e8 g c c,16 e' d8. c16 r8 c,16 d
  e8 g' f e b,16 a g8 r
}

