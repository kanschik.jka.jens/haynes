 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Larghetto"
  \time 3/4
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
   g'4 \grace f16 es4 \grace d16 c4
   \times 2/3 { as'8 g f} f2
   f8 g, b d  f16 d as' f
   \times 2/3 { es16 d c} c4.
     
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro ma non troppo"
  \time 4/4
  \partial 8
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
    g8 g'8. c,16 c8 b16. f'32  f8 es32 d es f g[ c, c es]
    es16 d f8
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro assai"
  \time 3/8
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
       R4.
       d~
       d16 bes c es d c
       f4.~
       f16 d es g f es
       as8. g16 as f
       g8. f16 g c,
}