\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 3/4
    r8 d, d d d d
    e e e e e e
    fis fis fis fis fis fis
    g8. b16
    d,4 c
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
      g'4 g,16 a b c d8 b a g
      g'4 g,16 a b c d8 b a g
      e'4 d
}
