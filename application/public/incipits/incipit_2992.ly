 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #""
                     \key d\major
                      \tempo "o.Bez."
                      \time 4/4
                      % Voice 1
                      r16 a b cis  d fis e d  cis b a b  cis d e cis
                      d a b cis d fis e d  cis b cis d  e fis g e
                      fis g e g  fis cis d e  d e d e  d b cis d
}
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #""
                     \key d\major
                      % Voice 2
                      fis,4 r e r
                      fis r e r
                      d'8 a a cis b4 r8 fis
                  }
>>
}


\relative c'' {
  \clef "treble_8"
  \set Staff.instrumentName = #"Tenor"
  \key a\minor
   \tempo "Andante"
  \time 4/4
     \set Score.skipBars = ##t
      
      R1*7
      d,4 r e r
      fis r cis r
      d r r r8 d
      e d e b cis8. b16 a4
}