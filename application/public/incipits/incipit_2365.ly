\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  g'8 g f f g g e e g, g a a d d16 c b8 a16 g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Piu Vivace"
  \time 3/4
  g'4. f8 e4 a g2 f e4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/4
  \partial 4
  r16 g' f g e g f g d g f g e g f g d g f g
  
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Menuet"
  \time 3/4
  c'4 b8 a g f
  e2 d4 e a g8 fis g fis g a b g
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  d4 cis d8 a r e'
  fis fis fis e16 d e8 a, r 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 3/4
  fis4 e d a'2 b4 g4. fis8 g a fis4. e8 d4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 3/4
  r8 a' fis a e a
  d,4 r r
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  c4 f e r8 c
  d8. d16 c8. bes16 a4 r8 g a4 d b
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio e Solo"
  \time 4/4
  f4 r16 f g a bes8 f32 g a16 bes8. a16 a4
}
  
  \relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 4/4
  a'8 c bes16 a g f e4 r8 c d16 e f a, g8 c a f r
}
  
  \relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 6/8
  f4 e8 f4 g8 a g f e4 c8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 3/4
  r4 d d g a a bes2 d,4 g g a fis2 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Cantabile e Solo"
  \time 4/4
d4 g8. a16 fis8. e16 d8 es
c d bes8. a16 bes8 g r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 4/4
  g'4 fis g r8 d
  es d c bes16 a g8 a
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 3/4
  g'4. a8 bes4
  a d c bes4. a8 g a
  fis4. e8 d4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Un poco Vivace"
  \time 3/4
  r4 c d es4. d8 c4
  r es f g4. f8 es4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio e Tutti"
  \time 3/4
  r4 c d es4. f8 g4
  c, f8 es d c
  b4. a8 g4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro"
  \time 6/8
  c4 d8 es4 d8 es d c b4 g8
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef bass
  \key d\minor
   \tempo "1. Adagio"
  \time 4/4
  d,8 e f e16 d cis8 a r d16 c bes8 g e c'16 bes a8 f r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Vivace"
  \time 4/4
  a'4 d c d8 a bes4 c a2 a4 bes g a a g8 f e2
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro"
  \time 4/4
  \partial 8
  a8
  d d d cis16 d e8 a, a e'
  f f f e16 d e8 a, r
}

