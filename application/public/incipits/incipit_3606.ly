\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro moderato"
  \time 4/4
    g8 d b' d, a'16 g a b g4
    b8 d, d' d, c'16 b c d b4
    d8 b e4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 4/4
 
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 2/4
 
}
