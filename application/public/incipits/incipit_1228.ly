 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "Adagio"
  \time 3/4
    r4 d b
    fis'2.~
    fis4 e4. fis8
    d8. cis16 b4
}
