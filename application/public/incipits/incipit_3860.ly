\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Largo"
   \time 12/8
   b8. c16 b8  e4 fis8 dis4. r8 r fis
   g8. fis16 e8 d8. e16 c8 b8. c16 a8 b8. g'16 fis8
   g4
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
   \time 2/4
   r8 e fis dis
   e4 fis
   g8 fis16 g e8 g fis g fis e
   dis16 e dis cis b cis b a
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
   \time 3/4
   g8. a16 b8. c16 d8. e16
   fis,2 e'4
   d c4. d8
   b8. a16 b8. a16 g8. a16
   fis2 r4
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
   \time 2/4
   b4 e8. fis16
   dis4. fis8
   g c, b a 
   g e16 fis g a b c
}

