\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key es\major
                      \tempo "1. o.Bez."
                      \time 2/2
                      \partial 4
                      bes8. bes16
                      es2 r4 bes8. bes16
                      f'2 r
                      bes4 g8. f32 g as4 r
                      % Voice 1
                  }
\new Staff { \clef "alto" 
                     \key es\major
                       bes,,8. as16
                       g4 <g es'>8. <g es'>16 <g es'>[ g' f g]
                       as g f g
                       <bes, f'>4 <bes f'>8. <bes f'>16
                       f'16 d c d  f[ d c d]
                       es8 bes4 bes r8 r4
                  }
>>
}
