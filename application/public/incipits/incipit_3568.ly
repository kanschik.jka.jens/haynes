\version "2.16.1"
         #(set-global-staff-size 14)
         \relative c'' {
  \clef treble
  \key  f\major
   \tempo " 1. Allero molto"
  \time 4/4
  f,8 (a) c4 c c c2~c8 f e f c2~ c8 f e f  c2~ c8 e f a
  
 }
 
  \relative c'' {
  \clef treble
  \key c \major
   \tempo "2. Adagio [Tutti]"
  \time 3/4
  c16 (b c) e-.  g8-. g-. g -.g-.
  g16 (f) e-. f-. e8 r r4 
  c,16 \p (b c) e-. g8-. g-.g-.g-.
  }

 \relative c'' {
  \clef treble
  \key c \major
\tempo "2. Adagio [Solo]"
\time 3/4
  c16 b (c) e-. g8-. g-. g-. g-.  
  g16 f (e) f-. e8 r r4
  c'2. 
  b8. (c32 d) c8 r r4
   }
     \relative c'' {
  \clef treble
  \key f\major
   \tempo " 3. Allegro [Tutti] "
  \time 3/8 
  f16 e32 f c8 c c4 a16 f bes8 d bes d c r  f16 \mordent e32 f c8 c
     }

 \relative c'' {
  \clef treble
  \key f\major 
  \tempo " 3. Allegro [Solo] "
 \time 3/8 
 f16 e32 f c8 c c4 a16 f d'8 (f) d-. d (c) r  f16 e32 f c8 c
 }
 