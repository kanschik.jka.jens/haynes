
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro "
  \time 2/4
 g4. a16 b
 c8 c c c 
c b d4~ d8 c16 d e d c b 
 a g a b a b c d 
 b4 \grace a8 g4

 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Corrente "
  \time 3/4
 \partial 4
 r8 r16 g16
g4. a8 fis a
 g4. d8 e b
c4 \grace b8 a4

 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Sarabande "
  \time 3/4
 g4 d e
d4. c8 b4
 c4 a4. b8
b2 \grace a8 g4

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Gavotta "
  \time 2/2
 \partial 2
\override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
\times 2/3 {g8[ a b]} \times 2/3 {b8[ a g]}
d'2 \times 2/3 {e8[ fis g]} \times 2/3 {e8[ fis g]}
fis2 d8 g d b c2
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Vivace"
  \time 3/4
d8 g, bes d g, g'
fis2 r4
d8 g, bes d g bes a4 d, r4
d8 g, b d g d es2 r4
 
 
} 

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 2/4
d2 
g,8 bes16 a g8 bes
a c16 bes a8 d
 bes g bes c16 bes a4 d~ d8 es16 d c4~ c
 
 
} 

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Affetuoso"
  \time 2/2
\partial 2
bes4 c
 d g, g' a
 bes2 a8 bes g a 
fis4. e8 d es c d 
bes4 a bes c d4.
 
} 

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 3/8
g16 a bes8 a
g16 a bes8 a
 d, g4
fis8. e16 d8
bes16 c d8 c
 
 
} 
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III. "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. Grave"
  \time 4/4
a4. a8 b a r8 a8
b a g8. fis16 fis4 r8 a8
d, e16 fis g8 g g4
 
 
} 

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro. Allemanda"
  \time 4/4
\partial 8
a8 fis d fis a d a r8 d8
e16 d e fis e fis g e fis8 d r8 a'8 
b d, cis a' g b, a g'
 
 
} 

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Affettuoso"
  \time 3/8
\partial 8
a8 d16 e cis8 d 
e16 fis d8 e
 fis g fis 
e a, a'
b16 a g fis e d 
cis b a g fis d' 
b g' e8. d16 d4
 
 
} 
\relative c''' {
  \clef treble
  \key d\major
   \tempo "4. Giga"
  \time 6/8
\partial 8
a8 fis a fis e a cis,
d4. cis4 a'8
a fis a g e g 
fis4 e8 d4
 
 
} 

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV. "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro. Allemanda"
  \time 4/4
\partial 8
a8 f a, d f e a, r8 e'
f a, d4 cis r8 a'8
 a fis d a' bes,4 r8
  
} 

\relative c''' {
  \clef treble
  \key d\minor
   \tempo "2. Corrente "
  \time 3/4
\partial 4
r8 r16 a16
a4. d,8 f d 
e a, cis e g e 
 f a, d f a f 
g c, e g bes g a4
  
} 

\relative c' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio "
  \time 3/2
d2 e a 
e2. bes'4 a g
f2 \grace e8 d2 d'~ d4 e cis2. d4 d2
 
 
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Minuetto"
  \time 3/8
f8 a16 g f8
g e16 f g e
f8 e d 
e16 d e f e8
f8 a16 g f8
 
 
} 
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 4/4
\partial 4
f8 e 
f c d16 bes c d c8 \grace bes8 a8 r4
r2 r4 f'8 e
 f c d16 bes c d c8 \grace bes8 a8
  
} 

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Presto"
  \time 3/8
r8 a8 a
g c c 
f, f'4~ f8 e4 f8 a f 
bes16 a g a bes g 
a8 f a

} 

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Adagio"
  \time 4/4
bes8. bes16 bes8. bes16 bes8. a32 g a8 r8
a8. a16 a8. a16 a8. g32 f g8 r8

} 

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Gavotta"
  \time 2/2
\partial 2
f4 a
g \grace f8 e4 f4. g8
\grace f8 e4 \grace d8 c4 f8 e d c 
d4. d8 c4. bes8 a4 \grace g8 f4
  
} 

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio. Allemanda"
  \time 4/4
\partial 8
r16 b16 
b4 r16 g'16 fis e dis8. cis16 b a g fis
 g8 \grace fis8 e8

  
} 

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 2/4
e4 dis
 e~ e16 fis g8 
fis16 g a8 dis,16 e fis8
e4.
  
} 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Affettuoso. Aria"
  \time 3/8
b8 e dis
e8. g16 fis a
g8 a16 g fis e 
fis8 b, r8

  
} 
\relative c' {
  \clef treble
  \key e\minor
   \tempo "4. Giga"
  \time 6/8
\partial 2
e8 g4 a8
b4. e
dis g
fis8 g fis fis g a 
g4 fis8 e4

  
} 

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
c4 g' g16 f e d c8 a'
g c, c a'
g4. a16 b 
c8 e, e d16 c d8 g,4

  
} 

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Largo"
  \time 3/4
e4 f8 e d f
e2 \grace d8 c4
g' a8 g f a
g2 \grace f8 e4

  
} 

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/8
c16 b c d c8
c c c 
d16 c d e d8
e c g'
g f e 

  
} 

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro"
  \time 3/4
\partial 4
e4 a8 a16 b c8 c16 d e8 e 
e4 a gis
a gis8 fis e d 
c4 \grace b8 a4
  
} 

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio"
  \time 3/2
\override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
e4. c8 a4. c8 e4. d16 c
d4. b8 e,4. b'8 d4. c16 b
c4. e,8 a2. \times 2/3 {b8 gis a} gis1.
  
} 

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro"
  \time 2/4
\partial 4
a8 b 
c b a gis
a e a b
c d16 c b8 a
e' e,
  
} 

