\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
    c8 r \grace e8 d8. c32 d
    c8 r r4
    e8 r \grace g8 f8. e32 f
    e8 r r4
} 


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Largo"
  \time 2/2
    g2 a16 g8. c16 b8.
    \grace gis8 a2 b16 a8. d16 c8.
    b8 a4 g8 e8 c'4 fis,8
} 


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/4
  \partial 8
  g8
  c16 d c b  c8 g' g4
  d16 e d c  d8 g g4
  e16 d e f
} 

