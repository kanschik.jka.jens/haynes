 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/2
    g4 r8 d'
    b8 g a d,
    g16 g g g   g b a g   a a a a  a c b a
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. Adagio"
  \time 4/4
   r4 b e
   dis dis g
   fis fis e
   b2. b4
   a
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Aria. Largo"
  \time 3/4
  d4 b g
  g2 d'4
  e4 c4. d8
  b2
}

