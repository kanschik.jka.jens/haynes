 \version "2.16.1"
         #(set-global-staff-size 14)
         
     \relative c'' {
  \clef treble
  \key  g\major
 \tempo "1. Allegro"
  \time 2/4
    \override TupletNumber #'stencil = ##f
  \override TupletBracket #'stencil = ##f
    \partial 8
    d8
    g g g g
    g g g g
    g fis16 e d8 c
    b g4 a8
    fis g a b
    fis g
     }
