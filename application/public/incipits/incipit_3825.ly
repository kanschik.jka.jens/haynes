\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 2/2
  e,4 e2 f4
  g4. a16 b c4 r8 e,
  cis d d d  dis e e e
  g4. f8 e
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 2/2
  e4 e2 f4
  g4. a16 b c8 g e c
  cis d d d  dis e e e
  g4. f8 e
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 2/4
  \partial 4
  f8. g16 
  \grace { f16[ g]} a4 g8 f d4 f8. d16
  c4 d8 f g4.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo"
  \time 2/4
  \partial 8
  g'16 f
  e8 g4 c8
  \grace c8 b4 d,8. f16
  e c f d g8 a
  cis,4 d8 r
}