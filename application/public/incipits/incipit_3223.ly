\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Es ist vollbracht [BWV 194/4]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key bes\major
   \tempo "o. Bez."
   \time 4/4
   r4 r8 bes8 d, f as g 
   g4 r8 g8 es' c a bes
   bes4 r8 bes16 c d es f8~ f16 g as8~
   as8 g a bes c, des a' bes
   des, c r8
  
  
   
   
   
}

\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key bes\major
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
 R1*8 r4 r8 bes8 
 d, f as g 
 g4 r8 g8 es' c a bes 
 bes4 r8
   
   
}


