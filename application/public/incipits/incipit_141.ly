\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
  f4 r8 f16 f  g4 r8 g16 g
  f8[ f16 f] es8 es16 es  d8[ d16 d] c8 c
  d16[ es d es]  f g f es d4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Duetto"
  \time 2/2
  g4~g8 a bes[ a16 bes] g8 a
  fis16[ d d d'] d c bes a bes4~ bes16 d g f
  es[ c bes a] a8. g16
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 2/4
  f16 es d c bes8 f'
  g bes, bes g'
  f es16 d es8 d
  c4 r8
}

