\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 3/8
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

  \partial 8
  a'16. fis32
  d8 d~ d16 fis e g \grace g8 fis4. \times 2/3 { d'16 cis b}
  a8 d,16. b'32 \grace a8 g4 \times 2/3 { fis16 e d} d4
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Largo"
  \time 4/4
  \partial 8
  a8
  d8~ d32 f e d d16 d d d
  bes'8 a~ a32 f16. e32 d16.
  cis32 e16. g,32 bes16. cis,32 bes'16. a32 g16. \grace g16 f16. e32 d4
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. o.Bez."
  \time 3/8
      \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \times 2/3 { fis16 g a} a4
  g16 e cis a g'8
  \times 2/3 { fis16 g a} a4
  g16 e cis a g'8
  \times 2/3 { fis16 e d} d4
  
}
