\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 4/4
    \partial 4
    e8. f16
    a g f e  e d c b b16. c32 c8 r a'16 c
    b g f a g e d f \grace f8 e4 r8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Larghetto"
  \time 3/4
  \partial 8
  d8
  g4. fis16. e32 d8 d
  d16 e d c c4 r8 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Allegro moderato"
  \time 2/2
    \partial 4
    g'8. f16
    e4 f8 g  a g f e
    d g g2 f8 e
    d c d e  f e d c d8. b16 g4 r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}

