\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  d8
      bes' fis g4~ g8 b, c4~
      c8 c a'4~ a8 d, bes' g
      cis, d r
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 2/4
  d4 f32 es16. d32 cis16.
  d8 c4.
  c32 bes16. a32 g16.  bes32 a16. g32 fis16. g16 es' es8[ d c]
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Vivace"
  \time 3/4
  d4 c8 bes a g
  a cis \grace cis8 d2
  g8 a c bes a g
  f a \grace a8 bes2
}
