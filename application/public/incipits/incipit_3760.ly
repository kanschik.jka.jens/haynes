\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  f1 f,1 \grace b8 c1 c,
  f8[ g16 a] bes c d e f[ g a bes] c8 c
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio"
  \time3/4
  a'8. f16 d8 r r16 f e d
  d8 cis cis4 r
  bes'8. g16 e8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo. Moderato"
  \time 2/2
  \partial 2
  c4 c
  d2 c4 bes bes8 a a4 f' f
  d8 g bes a a g g f
  }
