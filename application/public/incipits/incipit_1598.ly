 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro non troppo"
  \time 4/4
    f2 a4. f8 c'2 r8 f c a
    f c' a f c f c a
    f2. bes8 a a4. g8 f4 r
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Adagio"
  \time 2/4
    r16 c' b bes  \times 2/3 {f16[ bes a]} \times 2/3 {bes16[ g e]}
    g16 f f8. f16 bes32 a g f
    d'8~ d64 cis d es d c bes a
    \grace a8 g8 r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Menuetto"
  \time 3/4
  \partial 4
    f,8 a c2 a8 f
    f'2 \times 2/3 { c8[ a f]}
    c'8 bes g4 e
    e2 e4
    f16 e f g a4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "Trio"
  \time 3/4
    a'4. bes8 g a
    f4 f f
    e8 g bes g e g
    f4 a c
    a4. bes8 g a
    f4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allegretto"
  \time 2/4
    f8 f16 e f8 a
    f a c, f
    e16 f g a bes g e g
    f g a bes c bes a g f8
}