\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Gamme I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Allegro"
  \time 4/4
    g4. \times 2/3 { a16[ g fis] } g8 d g a
    b4. \times 2/3 { c16[ b a] } b8 g b c
    d g g fis fis a a g
    
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Gamme II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "Lentement"
  \time 2/2
  \partial 2
  e4 g
  fis fis8 g \grace b8 a4 g8 fis
  g4 \grace fis8 e4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Gamme III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Allegro"
  \time 2/4
    b4 \grace d8 c b16 c
    d8 b g d'
    e c a fis'
    g fis16 e d8 c
    b4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Gamme IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "Air un peu lent"
  \time 4/4
  \partial 4
  \grace d8 cis16 b cis16. d32
  e4 d16 cis b a
  fis'4 gis 
  \grace b8 a gis16 a e4 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Gamme V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "Tempo di Minuetto"
  \time 3/8
  a'4 cis16 e,
  e8 d cis
  e4 a16 cis, cis8 b a
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Gamme VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "Larghetto"
  \time 3/4
  a8. b16 a8 a a d
  cis4. d16 e
  d8 r
  g8. a16 g8 g fis fis
  e8. fis32 g fis8 r r4
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Gamme VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "Allegretto "
  \time 2/4
  d16 e fis g \grace gis8 a8  \grace gis8 a8
  fis16 g a d, \grace d8 cis8 b16 a
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Gamme VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "Andante"
  \time 2/4
  \partial 8
  c8
  f g32[ f e f] g8 a32[ g f g]
  a16 bes bes4 a16 g
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Gamme IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "Allemande"
  \time 2/4
  f8. g32 f e8 f
  g a bes4
  d,8. e32 f g8 f
  e d e c
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Gamme X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "!er Menuet"
  \time 3/4
  g'8 fis  g fis g fis
  g2 b8 d
  c4 b8 g e fis
  \grace a8 g8 fis16 g d2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Gamme XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "Andante"
  \time 2/4
   e8. g16 fis8. e32 fis
   g8 e dis e
   fis8. g16 g8. fis32 g
   a4 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Gamme XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Allemande"
  \time 3/8
  b8 g' g
  d a' a
  b16 a c b a g
  a g fis e d c
}

