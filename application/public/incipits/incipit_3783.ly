 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. [Allegro]"
  \time 2/2
  f4 r8 c d f16 es d8[ es16 d]
  c4 r8 c d f16 es d8[ es16 d] 
  c8 f, f'4 f e f r
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Adagio"
  \time 3/4
  d4 f8 e d c
  bes4 bes4. c8
  a4 a'8 cis, d e
  f2.
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 2/2
  f,8 c a f r2
  f'8 c a f r2
  r8 c'' a f d' c c8. bes16
  a8 f r4 r2
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}