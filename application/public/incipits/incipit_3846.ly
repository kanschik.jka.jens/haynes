\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}




\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI [IX] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Largo"
  \time 2/2
  c4~ c16 d e f
  g a g a f[ g f g] e8 c r g'
  d b' c d
  g,4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4
  c4. e8 d16 c d e  d f e d
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Adagio"
  \time 3/2
  e2 c a f'2. g4 f g
  e f
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 3/8
  r8 g c b4 e8
  r b g'
}

