\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Choralbearbeitung (Sopran): Valet will ich dir geben [BWV 95/3]" 
    }
    \vspace #1.5

}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore 1+2"
  \key d\major
   \tempo "Choral"
  \time 3/4
     \set Score.skipBars = ##t
     R2.*6 r4 r8 fis16 cis d fis b8 b e, r8 e16 b cis e a8 a d, r8

}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key d\major
   \tempo "Choral"
  \time 3/4
  a2 a4 b2 cis4 d2. d2 fis4 e2 d4 d2 cis4
  d2 r4
    

}