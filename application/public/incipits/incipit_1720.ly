\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
      f4. a32 g f16  bes,4. d32 c bes16
      a f e f c'8 d16 f, f8 e r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 4/4
    a16 g f g  a bes c d  a g f g  a bes c d
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Largo"
  \time 4/4
    a2.~ a 
    d8 bes g2
    d'8 b gis2 a2.~ a
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. allegro"
  \time 2/4
   \set Score.skipBars = ##t
   R2*6
   r4 r8 c
   f,16 g a bes  c8 f
   e f r
}

