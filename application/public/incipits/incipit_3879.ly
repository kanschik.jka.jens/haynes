\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro risoluto [Tutti]"
  \time 2/4
  \partial 8
  c,8
  f4 a8. c16 f8 c f a c2
  c4 c c8 f, f f
  e16 f32 g f8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro risoluto [Solo]"
  \time 2/4
  \partial 8
  r8
   \set Score.skipBars = ##t
   R1*20 
   r4 r8 c
   f f f f f8. g16 a8 c,
   g' g g g g8. a16 bes8 g
   a16 c a f c4
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Largo [Tutti]"
  \time 3/4
  \partial 16
  f16
  bes4 bes,~ bes16. d32 f16. bes32
  bes4 a
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Largo [Solo]"
  \time 3/4
  \partial 16
  r16
   \set Score.skipBars = ##t
   R2.*10
   r8 f g a bes d,
   es8. f32 g \grace g8 f4 c'8 es,
   es4 d r8.
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegretto"
  \time 2/2
  \partial 2
  c4 f
  \grace f8 e4 d8 c
  g' g a bes
  \grace bes8 a4 g8 f c'4 c
  c8 bes bes a a g g f
  e32f g8. r4
}