\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Ermuntert euch, furchtsam und schüchterne Sinne [BWV 176/5]" 
    }
    \vspace #1.5
}
 
 
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Ob 1+2+Taille un."
  \key es\major
   \tempo "o. Bez."
  \time 3/8
  \partial 8
     \set Score.skipBars = ##t
   es16 f
   g as bes8 es
   des c bes
   a bes16 f g es
   g8 f16 es f g
   as bes c g as f
   d'16 es f8 r8
    
    
    }

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key es\major
   \tempo "o. Bez."
  \time 3/8
  \partial 8
     \set Score.skipBars = ##t
    r8 R4.*11  r8 r8 es16 f
   g as bes8 es
   des c bes
   a bes16 f g es
   g8[ f]
    
    
    }