\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Vivace"
  \time 4/4
  \partial 8
  a8
  d4. d8 e d cis d
  a d cis d fis d a' fis 
  e4. g8 fis e dis e
}
  
