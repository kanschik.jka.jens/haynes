\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
      f,2 a4. f8
      c2 c'4. a8
      f4 g8 a bes16 a bes c d8 e
      \grace g16 f8 es16 d c8 c c4	
} 


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 3/4
    c,8 c4 c c8
    c c'4 c e,8 f f'4 f8 f16 d c b
    g' e c8
} 

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/4
      c4 f8 f
      f e g4~ g8 f  a16 f a f
      bes8 g \grace f16 e8 d16 c
} 
