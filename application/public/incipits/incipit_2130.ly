 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

\relative c' {
  \clef bass
  \key d\major	
   \tempo "1."
  \time 2/2
  d,8 d16 d d8 d e a, r e'
  fis fis fis e16 d a'8 a, r 
}


\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "2."
  \time 4/4
  b2 ais4 b cis4. cis8 cis4 ais
  b2 b4. b8
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. "
  \time 6/8
  a4. d b8 cis d cis b a b cis d cis d e
}
