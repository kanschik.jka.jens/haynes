\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante [Tutti]"
  \time 2/2
  <bes d,>4 <bes d,> 2 \times 2/3 { c8[ a f]} d'4 d2 \times 2/3 {es8[ c a]}
  f'8.[ g16] f g f g f8 g a bes
  \grace a8 g4 f r
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante [Solo]"
  \time 2/2
  f,8 bes bes2 \grace d8 c8 bes16 a
  bes8 d d2 \grace f8 es8 d16 c
  d f8. f2 f8. es32 f g8 f
  \grace f8 es4 d
}
