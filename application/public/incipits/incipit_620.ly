
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Les Plaisirs DE BAGNOLET amusements de S.A.R. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gracieusement sans lenteur"
  \time 3/4
e2 d4
e2 d4
g8 f e d c b 
c2 g4
f'8 e f e d c
f e f e d c  

}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxieme Suite. Le Trenel "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Prelude. lentem.t"
  \time 4/4
r4 g8 c e, d f16 e d c 
b8 \grace a8 g8 e'16 d e f g8 e a fis 
g4 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisieme Suitte. Le Salon Des Bois "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Gracieusem.t"
  \time 2/2
\partial 2
d8 c d e
d4 g fis8 e d c 
b4 a d8 c b a 
c b a g c4 b a2



}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatrieme Suitte. La Marquise De Conflanc "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Prelude"
  \time 3/4
r4 g8 d g a 
fis4. e8 fis d 
g4 d8 e d c
b4. a8 b c a4

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquieme Suite. La Beaujoloise"
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Un peu legerem.t"
  \time 3/4
\partial 4
g4 e4. f8 g4
f8 e d c b a 
g4 g'8 f e d 
e4 d g a4. b8 c4
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    " Sixieme Suitte"
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Prelude"
  \time 2/2
\partial 16
g16 g4 r16 c16 b a g4 r32 c32 b a g f e d 
e4 r16 f16 e f g4 a32 g f e d c b a 
b4. 

 
}

