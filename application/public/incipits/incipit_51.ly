\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}




\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Menuet"
  \time 3/4
    g'4 as8 g f es d2 d4
    es4 f8 es d c b2 b4
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gavotte"
  \time 2/2
  \partial 2	
  g'4. f8 e4 f d4. c8 b4 g c4. d8 e4 f fis4. fis8 g2

}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Ouverture"
  \time 2/2
  g'2. r8 g
  a2 g4. g8 g2 f4. f8
  f2 e4. e8 e2 d4. d8 d2
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Simphonie"
  \time 3/4
  c4 c8 d e4
  e f g
  e e8 f g4 a f a

}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Menuet"
  \time 3/4
  a'4 f d e a e f g8 a bes g a4 a8 g a bes
  g4 c a f bes g
  e8 a g4. f8 f2.

}
