
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "La Julliet"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key d\minor
   \tempo "1. gayement. Rondeau"
  \time 6/8
\partial 2
a8 d f a,
b g b e g b,
cis a cis f a cis,
d4.
 
}




\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Le Dodo ou L'amour au Berceau "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Rondeau. Sur le Mouvement des Berceuses"
  \time 2/2
\partial 2
b2 
a b
a b4 cis
d cis b8 e d e
cis4 \grace b8 a4


 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Muséte de Choisi "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Sujet. tendrement contre-partie"
  \time 6/8
\partial 4.
a8 cis b
a cis d e4 fis8
e a e cis4 d8
e a e cis4 fis8
e16 fis e d cis b a8 cis b
a cis d e4



 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Muséte de Taverni "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Legerement. premier partie"
  \time 12/8
\partial 8*9
r8 r8 e8 d cis b a gis fis
e4. a b8 a b cis d cis
b4. a4 

 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "La Létiville "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Sujet. contre partie"
  \time 12/8
\partial 8*9
g8 fis g a b c c b a 
b a g b c d a d, d' g, fis g fis4.



 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Le Tic-Toc-Choc ou Les Maillotins "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key f\major
   \tempo "1. Rondeau. Légérement et marqué"
  \time 2/2
\partial 2
f16 c' f, c' f, c' f, c'
g[ c g c] g c g c f, c' f, c' f, c' f, c'
e,[ g c g] e g c e, f[ a c a] f a c f, 

 
}
