\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 3/4
  \set Score.skipBars = ##t
  R2.
  g8[ fis g a g a]
  b[ a b c b c]
  d[ c d e d c]
}
