\version "2.16.1"
         #(set-global-staff-size 14)
         
         \relative c'' {
  \clef treble
  \key c\major
   \tempo "19. Aria. Amorosa"
  \time 4/4
    c4~ c32 d d16 e32 fis fis16
    \grace fis8 g4~ g32 fis g fis g f e d
    \grace d8 c16. b32 c8 r c'
    gis16 a fis g \grace g8 f4
}