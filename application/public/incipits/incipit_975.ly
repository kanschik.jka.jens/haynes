\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. "
  \time 2/2
  g4. c8 b4. g'8
  es c f es  d bes r es
  c d16 es d[ es c d]  b8 g c4~
  c
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 2/4
  c4 b
  c r8 g
  c16 d es f  g a g f
  es f es d  c es d es
  f g f es d[ es] 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 3/4
  g'4~ g8 es d c g'4 g, r
  c4~ c8 es d c b4 g r
 } 

  
  \relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Gigue"
  \time 9/8
  \partial 8
  g8
  c b c   c b c  c b c
  d4. g,4. r8 r g
  d' c d  d c d  d c d
 } 
