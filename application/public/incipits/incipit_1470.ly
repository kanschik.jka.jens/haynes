
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Suitte"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key g\minor
   \tempo "1. Prelude. Gravement"
  \time 2/2
r4 g4 d' d8 a
bes4 bes8 d g,4 g
es' es8 d c4 c8 bes
a2.

 
}

 
\relative c'' {
\clef "treble"   
  \key g\minor
   \tempo "2. Caprice. Gai"
  \time 3/4
  r4 d2~
  ~d4 bes d
  g, g'fis g8 fis g bes a g 
  fis e d c bes c
  a4 
 
}

 
\relative c'' {
\clef "treble"   
  \key g\minor
   \tempo "3. Allemande. Piqué"
  \time 2/2
  \partial 4
  r8 d8
  d2 r8 d8 e fis
  g4 a bes a8 g
  fis4. e8 d c bes a 
  bes4.
 
}

 
\relative c''' {
\clef "treble"   
  \key g\minor
   \tempo "4. Sarabande"
  \time 3/4
  g4 \grace {g16[ a]} bes4. a8
  \grace a8 g8 d
  d4. c16 d
  es8[ d c bes] a bes16 c
  bes4. a16 bes g4

}

 
\relative c'' {
\clef "treble"   
  \key g\minor
   \tempo "5. Ier Menuet"
  \time 3/4
  g2 a4
  bes8 c c4. bes16 c
  d4 c es 
  d c bes
  a8 bes c bes a g
  fis4. e16 fis d4
  
}

 
\relative c'' {
\clef "treble"   
  \key g\major
   \tempo "6. IIe Menuet"
  \time 3/4
  b2 a4
  b g d'
  e d c
  b8 a b c b4
  
 
}

 
\relative c'' {
\clef "treble"   
  \key g\minor
   \tempo "7. Sicilienne. Un peu lentement"
  \time 12/8
  r8 r8 d8 d g es d4 c8 bes4 a8
  bes a c bes a c bes a g fis4 g8

}

 
\relative c''' {
\clef "treble"   
  \key g\minor
   \tempo "8. Gavotte. Gracieusement"
  \time 2/2
  \partial 2
  g4 d
  bes' a8 g fis4. g8
  a4 d, g8 f es d 
  es d c bes a bes a bes16 c
  bes4 \grace a8 g4

 
}

 
\relative c''' {
\clef "treble"   
  \key g\minor
   \tempo "9. Gigue"
  \time 6/8
  r8 g8 d bes d bes
  g bes g es' d c d4. g8 d g
  bes c bes a bes g
  fis4.~fis4
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxiéme Suitte "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\minor
   \tempo "1. Prelude. lentement/vivement"
  \time 2/2
r4 r8 g8 g4. as8
\grace {as16[ g]} f4. f8 f4. g8
\grace {g16[ f]} es4. es8 es4 d8 es d2

 
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allemande. tendrement"
  \time 4/4
  \partial 16
  c16
  c4. d8 es f g as
  b,4. c8 d es f es16 d
  es8[ c] 
 
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Sarabande"
  \time 3/4
  c4 g4. g8
  as2. a4 g4. g8
  g8 as f4. es16 f
  g4. a8 b c
  d c d es f d
  es4.
 
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Ier Menuet"
  \time 3/4
  es8 f g f es d 
  c4 es2
  d4 f2
  es d4
 
 
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "5. IIe Menuet"
  \time 3/4
  g4 c c 
  c2 b4
  c d2
  es2.
 
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "6. Gavotte. tendrement"
  \time 2/2
  \partial 2
  es4 f
  g f8 es d4 c
  b \grace a8 g4 d' es
  \grace d8 c8 f d es 
  d4. c8
  c2
 
}
\relative c''' {
  \clef treble
  \key c\minor
   \tempo "7. Rondeau"
  \time 6/4
  \partial 1
  g4 g f g
  c,2 as'4 as g as
  bes,2 g'4 g f g
  as,2
  
 
}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "8. Gigue"
  \time 9/8
  r8 r8 g8 es d c es d c
  g'4. d4 f8 es4 d8
  c d es d es c b c d
  g,4.
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisiéme Suitte. Sonate "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Prelude. Lentement"
  \time 4/4
fis4 r16 fis16 g a g8. fis16 e8 fis16 g
fis8. e16 d fis e d a'8. e16 d8 cis16 d
cis4
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allemande. Gay"
  \time 4/4
  \partial 8
  r16 d16
  d4~ d16 d fis d a'8 a, cis b16 a
  d8. e32 fis e8. d16 cis8 a b  a16 g
  fis8 d' e, cis' d, 

 
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Courante"
  \time 3/4
  \partial 4
  r8 a8
  a4. a8 b cis
  d[ a d fis e fis16 g]
  fis4. e8 d4

 
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Grave"
  \time 3/2
  b2 d b
  cis \grace fis,8 fis2 r2
  d'2 fis d 
  e a, r2

 
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Gigue"
  \time 6/8
  d4 a8 fis'4 d8
  a'4.~ a8 g fis
  e fis g fis e d
  cis4 b8 a4

 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatriéme Suitte. Sonate "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key b\minor
   \tempo "1. Prelude. Gay"
  \time 4/4
\partial 8
b8 fis e fis b, g'4. g8
fis e d cis d4 b
r8 cis8 d fis e g ais, b cis4 fis,
 
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Allemande. Gracieusement"
  \time 4/4
  \partial 4
  r16 fis16 g a 
  b8 fis g fis16 e
  fis8 b, r16 b16 cis d
  e fis e fis32 g fis8. e16 d cis d b g8.

 
}
\relative c''' {
  \clef treble
  \key b\minor
   \tempo "3. Courante. Gay"
  \time 3/4
\partial 4
r8 b8 
b4 a8 g fis e 
fis g fis e d cis 
d4 b d8 e
fis4 e8 d cis b
ais4.

 
}
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "4. Rondeau. Gay"
  \time 2/2
  \partial 2
  b8 cis d b
  a4 fis cis'8 d e cis
  d4 fis, d'8 e fis d
  e fis g fis e d cis b
  ais2

 
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "5. Grave"
  \time 3/2
  r2 dis2. fis4
  b,1 e2~
  ~e dis2. dis4
  e fis g fis e d
  cis2 d2. cis4 cis2


 
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "6. Gigue"
  \time 12/8
\partial 8*7
b8 d4 fis8 b cis ais
b4 fis8 g a g fis g fis e d e
fis cis e d cis b ais4

 
}

