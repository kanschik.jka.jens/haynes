\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Gott, wir danken deiner Güte [BWV 193/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key e\minor
   \tempo "o. Bez."
   \time 3/8
   b8 a16 g fis e c'8 \grace b8 a4
   b16 dis e dis e g
   fis8 e16 dis cis b
   g'8 r8 a8 
   fis r8
   
   
   
  

 
   
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key e\minor
   \tempo "o. Bez."
   \time 3/8
   \set Score.skipBars = ##t
  R4.*32 
  b8 a16 g fis e c'8 \grace b8 a4
b16 dis e dis e16[ fis32 g] 
fis8 e16 dis cis b
         
}



