\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.bez."
  \time 2/2
    r4 c8 d  e f e d
    c4 g  c8 b c d
    e4 d8 c d c b d
    c4 g
}
