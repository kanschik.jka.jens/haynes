\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
        g'2 c8 b a g
        g4. e8 g4 r
        a8 f c'4. d16 c bes8 a
        a g g4 r2
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 2/4
    f,4 a8. f16 bes8 g e r
    g4 bes8. g16 c8 a f r
    f''2 c4 bes16 a g f
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Allegretto"
  \time 2/4
    c8 e e g
    g f d4
    c8. d16 e8. \grace g16 f16
    \grace e16 d4 c16 b a g
}
