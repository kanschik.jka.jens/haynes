
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquiéme Concert"
    }
    \vspace #1.5
}
 
\relative c' {
\clef "treble"   
  \key f\major
   \tempo "1. Prelude. Gracieusement"
  \time 3/8
\partial 8
f8 c' \grace bes8 a8 d
\grace d8 c8 \grace bes8 a8 f'
\grace f8 e8 \grace d8 c8 f~ f e16 d c bes
a8 \grace g8 f8
 
}



\relative c' {
  \clef treble
  \key f\major
   \tempo "2. Allemande. gayement et les croches égales"
  \time 2/2
\partial 8*5
f8 c'4~ c8 f,
d'4~d8 g, e' d e c
f c f4~ f8[ g16 f] e8 f16 e 
d4~d

}


\relative c'' {
  \clef treble
  \key f\minor
   \tempo "3. Sarabande. Grave"
  \time 3/4
as4 \grace as8 bes4. c8
as2 \grace g8 f4
des'8 c bes4. as8
g2 \grace f8 e4



}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "4. Gavote. Coulament et les croches égales"
  \time 2/2
\partial 2
f4 e
f g bes8 as g f 
e4 \grace d8 c4 f es
des \grace c8 bes4 es des
c \grace bes8 as4


}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. Muséte dans le goût de Carillon. Rondeau"
  \time 6/8
\partial 4.
f8 e d
c d e f e d
c4 r8 d8 c bes
a bes c d c bes c4 f,8

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixiéme Concert "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Gravement et mesure"
  \time 4/4
bes4~ bes8 c16 bes c4~ c8 d16 c
d8 bes es4~ es8. d16 d8. c16
c4
 
}

\relative c''' {
  \clef treble
  \key bes\major
   \tempo "2. Allemande a 4.tems Légers. Vivement et les chroches égales et marquées"
  \time 2/2
\partial 8*5
bes8 bes,4. bes'8
a g a bes g4 c
f, r8 f8[ f es16 d] es8 es
es[ d16 c] d8 f es d c bes a4
 
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Sarabande Mesurée. Noblement"
  \time 3/4
\partial 4
bes4 es4. f8 \grace es8 d4
c \grace bes8 a4 bes
c \grace c8 d4. es8
d4 \grace c8 bes4

 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Air de Diable. Tres viste"
  \time 2/4
\partial 8
bes8 d d16 es f8 f
d bes r16 f'16 g a 
bes8 a16 g f g f es 
d4 r8

 
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "5. Siciliéne. Tendrem.t et loûre"
  \time 12/8
\partial 8*10
d8 d c bes c f, f' f4 es16. d32
d4 f,8 bes c d g, es es' es d c d[ bes]

 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Septiéme Concert "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Gravement et gracieusem.t"
  \time 4/4
r8 d8 a bes fis8.[ g16 a bes c a] 
\grace a8 bes8 \grace a8 g8 g'8. f16 es8 d c8. bes16
a8

 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allemande. Gayement"
  \time 4/4
\partial 8*5
g16 a bes8 c d d,
g4 r8 g'16 a bes8 a16 g f8. es16
d4 r8 d16 es f8[ g16 f es d c bes] a4 r8

 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Sarabande. Grave"
  \time 3/4
g4 es' d
g4. a8 fis4
bes a g g4. fis8 g4

 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Fuguéte. Légérem.t"
  \time 6/8
\partial 4.
g16 a bes c d es
f,8 d' d, es c' c,
d16 a' d c bes a g a fis8. g16
g d bes d g,8


 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "5. Gavote. Gayement"
  \time 2/2
\partial 2
bes8 c d es
a,4 d g, d'
fis, d' g, es'
d c8 bes a4. g8
g2

 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "6. Siciliéne. Tendrement et louré"
  \time 12/8
\partial 8*7
d8 c4 d8 g,4 a8
\grace a8 bes4. a4 bes8 c d bes a bes g 
fis4.
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Huitiéme Concert dans le goût Théatral "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Ouverture"
  \time 2/2
\partial 4.
d8 g8. a16
\grace a8 b4. a8 a4. g8
fis4 \grace e8 d4 r8 d8 g8. fis16
e4. e8 d4. c8
b2
 
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Grande Ritournéle. Gravement"
  \time 2/2
d2. g4
fis4. fis8 g4 a
d,4. d8 c4 bes8 a
\grace a bes4 c8 d c4. bes8 a2
 
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Air. Noblement"
  \time 2/2
d4. e8 f4 d
g4. a8 a4. g8
fis2 r8 d8 d es
c4. bes8 c4 \grace bes8 a4
\grace a8 bes4 \grace a8 g4 


}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Air tendre. Rondeau"
  \time 3/4
\partial 4
d4 g fis g
d4. es8 d4
a bes a 
\grace a8 bes4 \grace a8 g4

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Air Léger"
  \time 3/8
\partial 8
b8 b4 a8
g e d'
d4 c8
b g g' g4 fis8
e fis g
d e16 d c b a4

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "6. Loure. pesament"
  \time 6/4
\partial 2.
b2 c4
d4. c8 b4 e fis2 \grace {g8[ a]}
b4. a8 g4 a a2
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "7. Air. animé, et léger"
  \time 2/2
\partial 4
g4 d2 a
b8 c b a g4 d'
g b a g 
fis d
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "8. Sarabande. grave, et Tendre"
  \time 3/4
b4 c d
g, g'4. fis8
e4 d4. c8
b2 \grace a8 g4 

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "9. Air Léger"
  \time 3/4
g8 fis g a b g 
d'2 g4
fis4. e16 fis g4
e f8 e d c 
b4. a8 g4


 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "10. Air Tendre. Lentement"
  \time 3/4
r4 bes4 g
es' es4. f8
d4 d4. d8
g4 g4. a8 fis4

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "11. Air de Baccantes. tres animé"
  \time 6/4
\partial 1
d4 b g d'
g4. a8 b4 a b g
a a d, g g b,
e4. f8 e d c d e d c b
a4 a

 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Neuviéme Concert. Intitulé Ritratto dell' amore "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Le charme. Gracieusement, et gravement"
  \time 4/4
r4 r8 e16 dis e8 fis \grace fis8 gis8. a16
gis8 \grace fis8 e8 r8 e16 dis cis8[ b] cis \grace dis8  dis8
 \grace {cis[ dis]}
e8 dis16 cis b8 a16 gis

 
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. L'enjoiiement. Gayement"
  \time 4/4
\partial 8*7
b16 a gis8 cis gis e'4 dis16 cis
b8 e4 dis16 cis b8 a16 gis fis8 b
gis b4 e8 dis e16 fis gis8 fis16 e
fis gis e fis gis8 fis16 e fis8

 
}

\relative c' {
  \clef treble
  \key e\major
   \tempo "3. Le Graces. Courante francoise"
  \time 3/2
\partial 8
e8 gis4. a8 b4 cis \grace cis8 dis4. e8
dis4 \grace cis8 b4 e8 dis cis4 b4. a8 gis2 \grace fis8 e4

 
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "4. Le je-ne-Scay-quoy. Gayement"
  \time 2/4
\partial 8
e8 b4 r8 b8
gis e r8 e16 fis 
gis8 fis gis a 
\grace a8 b4. cis16 b
cis4 \grace cis8 dis8. e16 
dis4

 
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "5. La Vivacité"
  \time 4/4
\partial 8*5
e8 gis gis16 a b8 e,
fis gis16 a b8 dis, e fis16 gis a8 cis,
dis16[ b cis dis e fis gis a] b8 a16 gis fis8. e16 e8 
 
}

\relative c''' {
  \clef treble
  \key e\major
   \tempo "6. La Noble Fierté. Sarabande. Gravement"
  \time 3/4
gis4 e,4. gis'8
\grace gis8 fis4 dis,4. gis'8
e dis \grace dis8 e4. fis8
dis2 \grace cis8 b4

 
}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "7. La Douceur. amoureusement"
  \time 3/8
\partial 4
g16 fis8.
e8 dis e
b8. c16 b a 
g a a8. g32 a
b8 e, fis
\grace fis8 g8 fis a

 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "8. L'et Coetera ou Menuets"
  \time 6/8
\partial 4.
e4. g16 fis e dis cis b e8 b a 
g fis16 g e8 g16 fis e fis g a 
fis8 b16 a g fis
e8

 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Dieziéme Concert "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Gravement et mesuré"
  \time 4/4
r4 r16 c16 b a gis8 \grace fis8 e8 a8. b16
\grace b8 c8 \grace b8 a8 r16 c16 d e b8 e d16 c b a gis4
 
}

 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Air Tendre et Louré. Sans lenteur"
  \time 6/8
\partial 4.
a8 e a 
c a c e4 b8
\grace b8 c8[ a] c e4 e,8
a gis a d d c16. b32 b4
 
}

 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Plainte. Lentement, et douloureusement"
  \time 2/2
r4 cis8. d16 e4~ e8. fis16
e4~ e8. fis16 e4~ e8. fis16
e4~ e8. a16 e8. fis16 e8. d16
cis8. b16 cis8. d16 e4~ e8.
 
}

 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. La Tromba. légérement"
  \time 6/8
\partial 8
a8 a e a a e a 
a4. cis
e8 fis e e fis e
cis4. \grace b8 a4

 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Onziéme Concert "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\minor
   \tempo "1. Majestueusement, sans trop de lenteur"
  \time 3/2
g4. f8 es4. d8 c4. d8
b2. a8 b c2
g es'2.. f8
d1.
 
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allemande. Fiérement, sans lenteur"
  \time 4/4
\partial 2.
r16. es32 f16. g32 as,8. as16 as8. g16
g4 r16. g32 as16. b32 c8 d16 es \grace es8 d8 b16. c32
b4 


 
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Courante"
  \time 3/2
\partial 8
g8 g4. g8 as4 g c4. d8
b4. g8 c4 b \grace b8 c4. d8
\grace d8 es2 \grace d8 c4

 
}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "4. Sarabande. tres grave, et tres marquée"
  \time 3/4
g4 c,4. c8
c2.
as'4 d,4. f8
b,2. 

 
}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "5. Gigue. Lourée"
  \time 6/8
\partial 4.
g4 f8
es d c g'4 g,8
c4 c,8 g' a b
c b c d8. c16 d8
\grace d8 es4 \grace d8 c8
 
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "6. Rondeau. Légérement et galament"
  \time 3/8
\partial 8
g8 c es16 d c b
\grace b8 c8 d es
\grace es8 d f16 es d c
b8[ \grace a8 g8]


 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Douziéme Concert "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key a\major
   \tempo "1. pointé-coulé"
  \time 3/2
\partial 2
e2 a4 gis a b cis d
b2 e, e'
d4 cis b cis d e
cis2 \grace b8 a2
 
}

\relative c' {
  \clef bass
  \key a\major
   \tempo "2. Badinage"
  \time 6/8
r8 a8 b cis d e 
b e d cis b a
gis fis gis a b cis
gis cis b

 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Lentement, et patétiquement"
  \time 2/2
r4 cis4 \grace {cis8[ d]} e4. a,8
a4. \times 2/3 {b16 a b} b4. a16 b
\grace b8 cis4 \grace b8 a4 r8 

 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Air. gracieusement, et légérement"
  \time 2/4
\partial 4
cis16 d cis d
e8 a, cis16 d cis d
e8 a, cis16 d cis d
e8 d16 cis b8 e
cis \grace b8 a8
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Treiziéme Concert "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Vivement"
  \time 4/4
r8 d8 g d b' g d'16 e d c
b8 g d'16 e d c b8 d g, fis16 g
fis8


 
}

\relative c' {
  \clef treble
  \key g\minor
   \tempo "2. Air. agréablement"
  \time 6/8
\partial 2
d8 g a bes
a d c bes a g
fis4

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Sarabande. Tendrement"
  \time 3/4
b4 \grace {a8[ b]} d4. a8
\grace a8 b4 \grace a8 g4 d
g b4. b8
b2 a4


 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Chaconne. Légere"
  \time 3/8
r8 g16 a b c
a8 d4~ d8 c16 b c d
b c a g a fis
g8

 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatorziéme Concert. Et dernier de cet oeuvre "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. gravement"
  \time 4/4
a4. \times 2/3 {a16 b c} bes8 a d8. e16
cis4 r8 d16 a \grace a8 bes8 a g8. c16
a8 \grace g8 f8 r8


 
}

 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allemande. Vivement"
  \time 4/4
r8 a8 d, a' bes g e c'
a f r8 d'16 c b8 cis d e
cis8


 
}

 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Sarabande. Grave"
  \time 3/4
f4 a,4. r16 f'16 
e4 a,4. r16 e'16
d4 gis,4. r16 e'16
cis4 \grace b8 a4 r4

 
}

 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Fuguéte"
  \time 6/8
r8 d8 f e a, fis'
g f16 e d e f g e4 \grace {d8[ e]}
d8 a'4~ a8 g16 f g e
f8.


 
}







