\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 2/4
  \partial 4
  d8 d
  \grace fis8 e8 d g8. fis16
  \grace fis8 e8 d fis8. e16
  g8. fis16 a8.[ \grace { b32 a g a} b16]
  d,8 cis r4
}

