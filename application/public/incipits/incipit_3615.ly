 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Largo"
  \time 4/4
    g'4~ g16 c, g'16. f64 g as8. g16 f es d c
    b8 g d' g es c r
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Allegro"
  \time 2/4
  g'8 c,4 as'8 g c,4 as'8 g f16 es d8 c b4 r8
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Siciliano"
  \time 6/4
  c2.~ c2 f4 e2.~ e4. f8 g4
  c2.~ c2
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Allegro"
  \time 6/8
  \partial 8
    g8 c d es d g f es d c b a b
}