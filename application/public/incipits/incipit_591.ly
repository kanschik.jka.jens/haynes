\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
   f,16 g a bes c8 f, d'16 f, f d' c bes a g
   a8[ f]
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  f,8
  f4. g8 a bes c d
  c16 f e f  c f e f  g c, c c  a' c, c c a'4 g 4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Gavotte"
  \time 2/4
  \partial 8
  c8
  f e16 f c8 f
  a4 g8 f bes a g f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Gigue"
  \time 12/8
  \partial 8
  c8
  f g f c d c d4. c
  bes8 a bes g a bes a4. f
}