\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/2
  \partial 2.
  bes'4 r8. bes16
  a16. g32 f16. es32
  d4 r8. d16 d4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Gavotte"
  \time 2/2
  \partial 2
  f4 bes
  a g8 f g4 a8. g32 a
  bes4 r8. bes,16 es4 g
  }

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Menuet"
  \time 3/4
  d8 c bes4 f'
  d8 c bes4 bes'
  a a8 bes a bes
  c4 f,
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Combat"
  \time 2/2
  \partial 1
  f,4
  bes bes8 bes bes4 c
  d d8 d d4 es
  f2.
  }

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "5. Gigue"
  \time 6/8
  \partial 8
  f8
  d d16 es f8 bes,16 c d c bes a
  bes8 d f bes4
  }
