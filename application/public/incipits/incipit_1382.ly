\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Cantabile ma andante"
  \time 4/4
  r4 g8 c  b8. a16 g8 c
  b c f es   d8. es16 d8 c
}
