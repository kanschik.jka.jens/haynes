 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
    g'4. f8 e4 d
    e2. r4
    e4. e8 fis e16 d g4
    g fis g r
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 4/4
  c8. c16 c8 c d4. c16 d
  e8 c e fis g d g4
  g4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Grave"
  \time 3/4
  e2 e4 e2 e4 e2 d4 e2 e4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Spirituoso e adagio"
  \time 4/4
    r4 r8 g c4 d
    e8. f16 g8. c,16 f4. e8
    e4. g8 f4 e d
}