 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 4
  g4 e'4. f8 d4. e8
  \grace d16 c4 c r8. c16[ d8. e16]
}
