\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
    es,2. g8 f16 g
    es8 r bes' a16 bes
}
  
