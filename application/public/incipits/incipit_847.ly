\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/4
  d16 c b a  g8 e'
  d r c r
  b[ c d e]
  a,[ d,]
}
