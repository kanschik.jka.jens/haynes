\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef alto
  \key c\minor
   \tempo "1. Largo"
  \time 4/4
    g,8. c16 c8 b16. d32  g,8. es'16 es8 d16. f32
    f16 es es g~ g d8 es32 f \times 2/3 { es16 d c } c8 r4
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro"
  \time 3/4
    r8 g c es es d16 c
    d8 g, r g'16 fis g8 d
    d c r c f d
    \grace c4 b2 c4~
    c8. as'16 \grace as4 g4. f8
    es8 es16 d es8 d16 c fis8 e16 d
    g4 g, r
}



\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Presto"
  \time 4/4
    \partial 4
    g4
    c2 es
    g1~ g4 f8 es d4 c
    \grace c2 b1 c4 g r es'
    es d r f
    \grace f2 es1 d2 r
}

