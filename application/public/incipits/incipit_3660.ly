 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 3/4
   r8 e e e f e16 d
   e8 e e e f e16 d
   e8 e e f16 e d8 c
}
