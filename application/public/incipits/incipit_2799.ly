\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Grave"
  \time 4/4
    c4. f32 g f e  f4. c8
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 3/8
  r8 c c
  f16  e f e g f g
  e d e f e f
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Adagio"
  \time 3/2
  r2 f,4 g g4. f16 g
  a2. bes4 c2
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allegro"
  \time 3/8
  f,8 a c
  f,16 g a bes c8
  f, d' c
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
    \set Staff.whichBar = ""
  g'4. g32[ a g f e d64 c]   b8[ c]
  r g'32[ a g f e d64 c]
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 3/4
  \partial 4
  r8 c8
  c4. g8 a8. b16
  c8. b16  c8. d16 e8. f16
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Adagio"
  \time 3/4
  c4. g8 c16 d c b 
  c4. g8 c16 d c b
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Allegro"
  \time 2/4
  c4 g'
  e8 d16 e c8 g
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
    d4. e16 d c8 b r e16 d
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
  d4 e
  d8. c16 b8. c16 d4 e
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Adagio"
  \time 3/4
  d4 e8 d  e d 
  c2 b4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 2/4
  g4 b8. c16
  d4. r16 e
  d8. c16 b8. c16
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 3/4
    e4 a, f'16 g f e
    d2 e4
}


\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "2. Allegro"
  \time 4/4
  e4. e8  e16 d e f e g f e
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "3. Allegro"
  \time 6/8
  e4. r8 r e
  e d e  e d e
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Adagio"
  \time 3/4
  \partial 4
  e,4
  b'4. c8 b16 c b a
  g a g f e4 b'
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 3/4
  d4 g, g'16 a bes8
  fis4 g2
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 4/4
  g2 d'4. r16 d
  g8. bes16 a8. g16 fis4. r16 a,
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 3/4
  d4 g,4. g'8
  fis4 g4. d8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 6/8
  g4. d'
  b8 c d g,4 g'8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  c4. e32 f e d c4. c8
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 2/2
  c2 g c2. d4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Adagio"
  \time 3/4
  c4 g c8 d
  e16 f e d c4 e16 f f e32 f
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Giga. Allegro"
  \time 6/8
  c4. b
  c~ c4 d8
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 3/2
  f1 g4 f
  f1 f2
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegro"
  \time 3/8
  bes4. f'
  g8 f es d c bes
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Adagio"
  \time 3/2
  f2 bes, bes8[ c  d16 es f g]
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Allegro"
  \time 6/8
  bes4 bes8 d4 d8
  f4.~ f4 f8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Grave"
  \time 4/4
  d4 g32 fis e d c b a g
  d'4 g32 fis e d c b a g
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 2/2
  g2 d'
  b4 a8 g a4 d
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Adagio"
  \time 3/2
  \partial 2
  d2
  cis1 d2
  d8 e b4 cis2. d4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Allegro"
  \time 3/8
  b16 c d8 e
  d16 c b a g8
  b16 c d8 e
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 4/4
  \partial 16
  d16
  d8. e16 e8. d32 e f4. r16 a,
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  a'8
  a g16 f e8 d
  cis4 r8 a'
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Allegro"
  \time 3/4
  d8 e f4. g8
  e4 a, d8 cis
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4. Gavotta. Allegro"
  \time 2/2
  d4 a d4. e8
f16 g a8 g8. f16 e4 a,
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "5. Minuet"
  \time 3/4
  d8 e f4 g 
  e a, a 
  a' g8 f e d
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Vivace"
  \time 4/4
  r4 a bes c
  f, f'2 e4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 3/2
  \partial 2
  c2 a1 f'8 e d16 c b c
  b2 a4 g c2
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 4/4
  c4. c8  f16 e f g f g f g
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Adagio"
  \time 3/4
  c4 f, f16 g a32 bes c d
  e,2 f4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "5. Fuga. Allegro"
  \time 9/8
  \partial 8
  c8
  f g f f g f  e d c
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 3/4
  e4 a,16 b c32 d e f b,8. a16
  gis2 e'4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro"
  \time 2/4
  a4 e'8. d16 c4. r16 b
  a8. b16 c8. d16
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Minuet"
  \time 3/4
  e4 d2 c b4
  c8 d d4. c16 d
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Giga"
  \time 6/8
  \partial 8
  e8
  e d c d c b
  c b a r r b
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
    g'4~ g16 g, a c32 b  c d b16 c8 r e
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 3/8
  r8 c16 d e f
  g4.~
  g16 f e8 f
}

\relative c'' {
  \clef bass
  \key a\minor	
   \tempo "3. Adagio"
  \time 3/4
  a,8. gis16 a4 e
  b'8 a b e,4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Allegro"
  \time 12/8
  \partial 8
    g8
    c b c  
    d, c' b c4.
    r4 c8
}
