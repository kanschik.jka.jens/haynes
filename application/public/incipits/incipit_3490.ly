\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 3/4
  \partial 4
  r4
  R2.
  r4 r b8. c16
  d4. e16 d d c c b
  b8. c16 b8
}

