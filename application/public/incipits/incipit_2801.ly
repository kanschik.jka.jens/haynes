\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 2/2
  c16[ d e d] c8 g c16 b c8 r c
  d16 e f e d8[ f] f[ e16 d] e8 g
  c,16[ b c b] c8 c c4. d16 c
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro [Violin]"
  \time 2/2
  c4 r16 d e f g[ g g g] g g g g
  g[ g g g]  g[ g g g]   a a a a  b[ b b b]
  c8[ b16 a] g8 f e d e c
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro [Oboe]"
  \time 2/2
   \set Score.skipBars = ##t
   R1*5
   g'4 a g8[ f16 e] f8 g
   e c d g e c d g
   e c d g e[ d] e d16 c
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Largo"
  \time 3/2
    f2 f f  dis dis dis  e e e
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Gigue. Allegro"
  \time 12/8
  c4 c8 c b c  d4 d8 d c d
  e4 e8 e d e f4 f8 f e f
  g f e a g a  f e d  g f g
}
