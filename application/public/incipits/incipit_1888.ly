 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "6. Air. Gay"
  \time 6/8
  \partial 4.
  a4 a8
  d4 e8 fis d fis
  e4 e8 a,8 a4
  d4 d8 cis d4
  e4
}

