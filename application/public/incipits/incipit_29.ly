\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [tutti]"
   \time 3/4
    c4 c,8 d e f
    g4 a b
    c c, r
    c c8 d e f g8 g, r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [solo]"
  \time 3/4
     \set Score.skipBars = ##t
   R2.*11 
   g'4 g f e e8 f e f
   g4 g f e e8 f e f
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio"
  \time 4/4
  es2 d c b4 c~
  c bes a2 g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [tutti]"
  \time 4/4
  g'4 c b r8 c
  b c b c  b c r a
  g a g a  g a r f
  e f e f  e16 g f a g a f g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*7 
   r4 r8 g'8 g a g f
   e e16 f g8 f e f16 g a8 b 
   c b16 a g8 f e4 r
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [tutti]"
  \time 3/4
  g'4 f2 e4 g8 f e4
  a8 g a g f4 e g8 f e4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [solo]"
  \time 3/4
  e4 f2 g4 e8 f g4
  a8 g a g f4 g e8 f g4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio"
  \time 4/4
  e8. f16 e f e d c4 r8 c
  d8. e16 d e d c b4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [tutti]"
  \time 12/8
  c4. r8 r c g'4. r8 r g
  e d c  g'f e  d4 g,8 r r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [tutti]"
  \time 12/8
     \set Score.skipBars = ##t
   R1.*7 
   r4 r8 r4 g'8 g4 g8 g4 a8
   g e f g4 a8 g e f f d f
   e4.~ e8 f d e4.~ e8 f d
   e2.
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro [tutti]"
  \time 4/4
  \partial 8
  a8
  d,16 e fis g  a g fis e d b' cis d d, b' cis d
  e, cis' d e  a, fis' g a fis8 d r16
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro [solo]"
  \time 4/4
  \partial 8
  r8
  \set Score.skipBars = ##t
   R1*7
   r2 r4 r8 a'
   fis g a b a fis16 g a8 b
   a g16 fis g8 e fis4 r
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Largo"
  \time 3/4
  fis4 d e
  fis g8 fis g fis
  e2 r4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro [tutti]"
  \time 12/8
   d16 e fis g a8 a,4 cis8 d4 d,8 r r a'
   cis16 d e fis g8 a,4 g'8 g4 fis8 r r
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro [solo]"
  \time 12/8
   \set Score.skipBars = ##t
   R1.*5 
   r2. r4 r8 r4 a'8
   b4 a8 b a g a4. r8 r d,
   e4 fis8 g4. fis r8 r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [tutti]"
  \time 3/4
  c,8 c c c c c
  g' g g g g g
  c c c c c c 
  b c d e f g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [solo]"
  \time 3/4
     \set Score.skipBars = ##t
   R2.*20 
   g'4 f8 g e4 a g r
   g f e d e8 d e f
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [tutti]"
  \time 4/4
  e16. d32 e16. f32 e8 f
  d16. c32 d16. e32 d8 e16. d32
  c16. b32 c16. d32 c16. e32 d16. c32 c8 b r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [tutti]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*2 
  g'16. f32 g16. a32 g8 a
  f16. e32 f16. g32 f8 g16. f32
  e16. d32 e16. f32 e16. g32 f16. e32 e8 d r4
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 4/4
  \partial 8
  g8
  c d e d16 c d4 r8 d
  e f g f16 e d8 g, r
}

