\version "2.16.1"

#(set-global-staff-size 14)

\relative c' {
  \clef alto
  \key g\major
   \tempo "1. Andante"
  \time 4/4
  r8 d b' b b d16 c b8 a16 g
  a8 d, a' a a d a b16 c
  b4 r
}


\relative c'' {
  \clef alto
  \key g\major
   \tempo "2. Air"
  \time 3/4		
    g8 fis g2
    b8 fis g2
    e'8 g, d' g, c d, b' g a2
}


\relative c'' {
  \clef alto
  \key g\major
     \tempo "3. Allegro"
  \time 4/4
  g8 g g g g16 a b a g8 g
  a b c a b16 c d c b[ e]
}



\relative c'' {
  \clef alto
  \key g\major
   \tempo "4. Menuet"
  \time 3/4
  g4. fis16 g d'4
  d,4. c16 d g4
  b,4. a16 b d4
  g,8 a b c d e
}