 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 4/4
    d8. e32 fis g8 d e d a8. b32 c
    b8 e16 d fis,8 e'16 d  g,8 e'16 d a8 e'16 d
    b a32 g d'8
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro assai"
  \time 2/4
  g'4 b,16[ c32 d] c16 d32 e
  d8 fis, g c16 d32 e
  d8 e16 fis32 g d8 a16 b32 c
  b8 fis g e'
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Allegro"
  \time 3/8
  g'8 g g \times 2/3 { g16[ fis e] } d4
  c8 b a \times 2/3 { g16[ fis e] } d4
}
