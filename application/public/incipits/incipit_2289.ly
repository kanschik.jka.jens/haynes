\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Minuetto "
  \time 3/4
  d4 d d 
  d~ \times 2/3 { d8[ e fis]} \times 2/3 { g8[ a b]}
  g4 fis \grace a16 g8 fis16 e
  \grace c2 b4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Larghetto"
  \time 3/8 	
  c16. d32 e16 e e e
  \grace e16 d8 c r
  a'16. b32 c16 b a g
  \grace g16 f8 e r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro assai"
  \time 2/4
  \partial 8
  d8
  g4 a8 fis g4 d8 d
  a'4 b8 g a4 r8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Minuetto"
  \time 3/4
   \partial 8
   bes8
   bes g' g4 g
   g8. as16 f4 r8 bes,
   bes f' f4 f f8. g16 es4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Larghetto"
  \time 2/2
  \partial 4
  f,4
  bes4. c16 d es4. d8
  \grace d8 c4 bes2 b4
  c4. d16 es f4. es8
  \grace es8 d4 c r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro con brio"
  \time 2/4
  \partial 4
  bes'8 bes
  g r f f es r es es
  d es as g
  g f
}

\pageBreak

\markuplist {
    \fontsize #3
    "Trio III " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Larghetto"
  \time 2/4
  e8 e16. e32 \grace g16 f8 e16 d
  c d32 c b16. b32 b8 r16 g
  c8. e16 d8. f16
  f32 e a g g16 g g8 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Presto"
  \time 3/8
  c8 b16 c b c
  d8 r r
  d c16 d c d e8 r r
}
