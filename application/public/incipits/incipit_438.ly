
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
  \tempo "1. Adagio "
  \time 3/2
 \partial 2
 b4. c8 d4. g8 d4. a'8 c,4. d8 b4. a8 g2 d' g1.~ g4.  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  r8 g8 b a g a b a g a b a g a b c d4 d, r8
 
}


\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/8
  
g16 a b8 a g d d b'16 c d8 c b a a b16 a d8 d, e16 f e d c b a4.

 
 
}

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro "
  \time 4/4
 a4 e' e16 d c b a b c d e8 a, a e' a gis a gis a e 
 

}

\relative c''' {
  \clef treble
  \key a\minor
   \tempo "2. Largo"
  \time 3/2
 a4. gis8 a4. e8 a4. c8 b4. a8 gis4. fis8 e4. d8 c2
 
 
}
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro"
  \time 6/8
  a4. b8 a b c b c a4 b8 c4. d8 c d e d e c4

}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
 r8 d8 d d d16 e fis e d e fis g a8 a, a' a a8. b16 gis8. a16 a8[ e] 
 
}
 
\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Adagio "
  \time 4/4
 b8. ais16 b8. fis16 cis'8. g16 g8. fis16 d8. cis16 b4 b' b b 

 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro "
  \time 3/4
 r4 d4 d e8 a, e'4 e fis8 d fis4 g8 fis e4 a,
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Adagio "
  \time 3/4
 r4 b4 fis'4~ fis8 g e4. d16 e fis4 cis d e4. fis8 d cis d2
 
 
}
 
\relative c''' {
  \clef treble
  \key b\minor
   \tempo "2. Allegro "
  \time 2/4
 b4 b fis4. g8 fis g fis e d4 cis8 fis, b16 cis d cis b cis d cis b cis d cis b a g fis g8 fis g e fis4

 
 
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Allegro "
  \time 6/8
 r8 d16 e fis8 fis16 g fis g fis e d cis b8 b' b4 ais8 b fis d' cis8. b16 cis8 fis,4  

 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 4/4
  a4 gis a8 gis16 fis e8 d cis a' b, gis' a gis16 fis e8 d cis
 
 
 
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Affettuoso "
  \time 2/2
 \partial 2
  a4 cis8. d16 e4 a, a' gis8. a16 e4 a, r4
 
 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Allegro "
  \time 2/4
 cis8.[ d16e8. fis16] e4 a, fis'8.[ gis16 a8. b16] gis8. fis16 e4

 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 3/2
 g2 g g fis1. a2 a a a1. g2 g b a4. g8 fis2. e4 e1.

}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro "
  \time 4/4
 \partial 8
 e8 e, e' e, e' e d16 c b a g fis e8 fis g a b4 r8
    
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Allegro "
  \time 3/4
  \partial 4
 r8 r16 e16 e2 b'4 \times 2/3{b8[ a b]} \times 2/3{g8[ a b]} \times 2/3{g8[ a b]} \times 2/3{a8[ g a]}  \times 2/3{fis8[ g a]} \times 2/3{fis8[ g a]}  
 
 
}


