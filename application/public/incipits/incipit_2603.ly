 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Mesto"
  \time 4/4
  g8 bes16 a g8 es' es d r d16 cis
  d8 f16 e d8 a' a16 fis g8 r
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro"
  \time 4/4
  g4 g' r8 f16 es d8 c
  bes a bes g a4 fis
  g
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Andante"
  \time 4/4
  g'2 f4 r
  es2 d4 r
  c2~ c4. es8 c f, bes4. 
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Andante"
  \time 3/8
  d8 g4
  es d8
  c bes a
  bes g16 a bes c d8
}
