 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o,Bez."
  \time 4/4
  d,4 fis8. e32 d a'4 g16 fis e d
  d'4. e16 fis a,4 r8 r16 b
  b8. cis32 d cis16 b a g fis4 a16 g fis e
}
 
