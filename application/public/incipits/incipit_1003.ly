\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Ouverture"
  \time 2/2
  a'4. a8 gis4 a
  e4. d8 e d c b c4 a
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Ouverture"
  \time 2/2
  c4. d8 e4. d16 c
  g'4. g8 g4. g8 a2 r8 a b c b4 g
}

