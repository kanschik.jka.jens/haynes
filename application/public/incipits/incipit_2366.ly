\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Ouverture"
  \time 4/4
  c4. c8 c8.[ c16 c8. c16]
  d4. d8 d8.[ d16 d8. d16]
  e8 d16 c g'8. g16 g4.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo ""
  \time 3/4
  g4 g' g
  e e8 f g4
  c, b c d e4
  f f8 g a4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 3/4
  c8 c16 c c8[ c c c]
  c8 c16 c c8[ c c c]
  d8 d16 d d8[ d d d]
d8 d16 d d8[ d d d]
e4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Entrée"
  \time 4/4
  c8.[ c16 c8. d16] e8.[ e16 e8. f16]
  g8.[ g16 f8. e16] d8.[ d16 d8. e16]
  f8. f16 f8 g8 e
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Menuet"
  \time 3/4
  c4 e g c b8 a g f
  e4 d c
  b8[ c b a b g]
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Allegro"
  \time 4/4
  e4 d e b
  c b c d e8[ e e e] f4 r
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  f4~ f16 a g f e8 d16 c r8 f16 a,
  bes d g, bes e, g c g a8 g16 f 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 3/4
  r4 f,8[ g a bes]
  c[ d c bes] a4 d d8[ c bes a] g2 g4
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Vivace"
  \time 4/4
  c8 d16 e f e f g e8. d16 c8 f16 a, bes8 a16 bes g8 c a g16 f
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 4/4
  f,16 g a bes c d e c f8 f, r4
  f16 g a bes c d e c f8 f, r4
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  a'4 a a8 g16 fis e fis g a fis8 d r4 r2
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 3/2
  \partial 2
  a2
  d2. fis4 e4. g8
  fis2 e4 d a'2~ a g4. fis8 g2~ g
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro "
  \time 3/4
  r4 a d cis4. d8 e4
  fis g8 fis e d
  cis4. b8 a4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  d4 g fis4. g16 d
  e8 d c b a4 r8 a
  b4 cis d a'
  g16 a fis g e8. d16 d4 r8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 3/4
  g4 b8 c d e
  d2 e4
  c4. b8 c d
  b4. a8 g4
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 4/4
  g8 g g g r g g g
  r a a a   r a a a
  r b b b r b a a
  r g g g r a a a
  b[ b]
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro"
  \time 4/4
  
        << {
      % Voice "1"
     R1
     a'8 a16 a a8 a a8. b16 gis8. a16
     a4 r4 r2
     a8 a16 a a8 a a8. b16 gis8. a16
     a8
           } \\ {
      % Voice "2"
  e8 e16 e e8 e e d16 c b c d b
  c8 a r4 r2
 e'8 e16 e e8 e e d16 c b c d b
  c8 a r4 r2
      } >>
  
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Trio Adagio"
  \time 3/2
    e2 a2. b4
    gis1 gis2
    a4 b8 fis gis2. a4
    a2
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Vivace"
  \time 3/4
  a2 b4
  c8 b c d e4
  d e8 d c b b2 a4
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Allegro"
  \time 12/8
  \partial 8
  c16 b
  c4 a8 e'4 a8 gis4. r8 r a
  f4. r8 r g e4. r8 r f
  d4.
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  g'8 e c e d16 g, a b c d e f
  g8 e c e d g, r
}
