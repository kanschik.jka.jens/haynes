\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Neusiéme Suitte" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Lentement"
  \time 4/4
  e4. e8 fis8. g16 g8 fis16 e
  dis4 b'8 fis \grace fis8 g4 e8 \grace d8 c8
  a \grace g8 fis8 d' a fis[ \grace e8 d8 d'8. g,16] e8

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Courante. Affectueusement"
  \time 3/4
  \partial 4
  r8 b8
  b2 e,4 c' b4. a8
  g fis e4 r8 e'8
  e2 b4 g'4 fis4. e8
  dis4. e8 \grace e8 fis4
  e8. d16 cis4. 

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Rigaudon en Rondeau"
  \time 2/4
  \partial 8
  b8 b16 a b c b8 a16 b
  g4 fis8 b
  cis dis e fis
  dis b e fis
  g fis16 g a8 b
  g4 fis8

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Premier Couplet"
  \time 2/4
  \partial 8
  b8 e[ fis g a] 
  d,4 d8 c16 b
  a8[ b c d] 
  g,4 g' fis8 e d cis 
  d8. cis32 d b8 cis
  e16 cis d e d8 e
  \grace {d16[ e]} fis8 fis,

}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "5. Gigue"
  \time 6/8
  \partial 8
  b8 b8. a16 b8 e,4 e'8
  dis4. b4 e8
  fis8. g16 a8 g4 fis8
  \grace fis8 g4. e4

}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "6. Fugue "
  \time 3/8
  b16 a g fis e16. b'32
  c16 b a g fis e 
  dis8 b'4
  e,8 fis16 g a8~ 
  a b16 a g fis
  g8 g8. fis16 fis8 

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Dixiéme Suitte " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "1. Sarabande. Lentement, et tres proprement"
  \time 3/4
  g4 f4. es8
  d4. g8 a4
  \grace a8 bes4 a4. g8
  fis2 \grace g8 g4 \grace {f16[ g]}
  a4 e4. fis8
  cis4 d e 
  \grace e8 f4 e4. d8 d2.

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Premier Rondeau. Gayment"
  \time 3/4
  \partial 4
  g4 d2 e4
  d \grace c8 b4 d
  g,2 a4 fis \grace e8 d4 g'
  d2 e4 d \grace d8 b4 d
  g,4. a8 fis4 \grace {e16[ fis]} g2

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gigue. La Coquette"
  \time 6/8
  \partial 8
  g8 d'4 d8 d e fis
  g4. b
  e,8 fis g d e c
  b4.~ b8 d a
  b c d a d a 
  b cis d cis d e 
  a4 

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Badine"
  \time 2/2
  \partial 2
  b8 c d e
  d c b a c b a g
  fis4 g a8 b c d
  b c b[ c16 d] c4. b8
  a2

}


