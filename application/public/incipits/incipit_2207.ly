
\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro moderato [Tutti]"
  \time 4/4
 \partial 4
 c4 f a r4 c,4 d f r4 f4 e8 g bes,4 r4 bes4 a8 c f a 
 
}



\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro moderato [Solo]"
  \time 4/4
 \partial 4
   \set Score.skipBars = ##t
   r4 R1*28 r2 r4 c4 f a r4 c,4 d f r4 f,4 \grace f8 e4. d'8 \grace d8 c4. bes8 a16 f g a bes c d e f8 a c a 
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante [Tutti]"
  \time 2/4
 d,
8 bes'16. bes32 a16. a32 gis16. gis32 a8 a, r4 c8 a'16. a32 g16. g32 fis16. fis32 g8 g, r4
 


}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante [Solo]"
     \time 2/4
   \set Score.skipBars = ##t
   R2*12 d2~d16 cis e bes' bes a  a g  \grace g8 fis4. a16 c, bes d c bes a g fis g d4.
   


}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Vivace [Solo]"
  \time 3/8
 \set Score.skipBars = ##t
   R4.*41 f4 a16 f c8 bes a c, d e \grace e8 f4 r8 c''4 a16 f c'4 a16 f c8 bes' a \grace a8 g r8



}

