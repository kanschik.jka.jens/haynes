\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 8
   a8
   d fis, g a
   d,16[ e fis g] a8 a 
   d fis, g a
   d,
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Aria Lentement"
  \time 3/4
    a4 d cis
    \times 2/3 { d8 e fis } e4 r
    fis8 e g fis e d
    cis4. b8 a4
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Rigadong [sic]"
  \time 2/2
  \partial 4
 a'4
 b2 a
 b4 a8 b g4 b
 a g8 fis g4 fis8 e
 fis4 e8 fis d4 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Menuet"
  \time 3/4
   a'4 fis8 g a4
   a4 fis8 g a4
   a4 g8 fis  e fis 
   g fis e fis d4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Caprice"
  \time 6/8
  d8 cis d  e d e
  fis e fis  g fis g
  a d,r r4 r8
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. "
  \time 2/2
  \partial 8
 
}