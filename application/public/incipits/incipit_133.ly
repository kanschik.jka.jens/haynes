\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key g\minor
    \time 2/2
    \tempo "1. Ouverture"
       d4. g,8 bes'4. bes8
       bes4. a8 a4 bes8 c
       fis,4 es8 d g4 a8 g16 a
       bes4. g8 d4
}
