\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 16
    g'16
    g8. f16 e8. f16 d4. r16 g 
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  g8
  c16 b c d  c8 g
  d'16 c d e d8 g,
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Adagio"
  \time 3/4
  c4 g4. d'8
  e4. d8 c4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/8
    e16 f g8 c,
    e16 f g8 c,
    e16 f g8 a
}
\relative c'' {
  \clef treble
  \key c\major	
   \tempo "5. Giga"
  \time 6/8
  \partial 8
  g8 c b c d c d
  e d c r4 g'8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 3/4
    c8. g16 c8. d16 c8. d16
    e8. d16 e8. f16 e8. f16
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 6/8
  c8 b c c b c 
  d c d d c d
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Minuet"
  \time 3/8
  g'8 g16 a g a 
  g8 c, c
  g' g16 a g a
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 16
  d16 d8. e16 d8. e16 d4. r16 d
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
  d8 d d e16 fis
  g8 fis16 e d8 e16 fis
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Adagio"
  \time 4/4
    d8 b16 c d8 e16 d c8 a16 b c8 d16 c
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga. Allegro"
  \time 6/8
  d4. r4 d8
  b8 a g  e' d c
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
    g'4. a16 g g4. a16 g
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 2/4
  c4 g'
  e8 d16 c d8 g,
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Minuet"
  \time 3/8
  c8 d8. c32 d
  e8 d16 e c8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 3/4
  d4 e8 d e d
  c2 b4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  d8
  g d b d g,4 r8 d'
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Giga. Allegro"
  \time 6/8
  \partial 8
  g8
  d' c d  d c d
  b a g r r g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Minuet"
  \time 3/4
  b4 d g
  b, a8 b g4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 16
  f16
  f8. c16 d8. e16 f4. r16 c
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 2/4
  c8 f e d  c f e d
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Adagio"
  \time 3/4
  c2 f,16 g a32 bes c d
  c2 f4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allegro"
  \time 3/8
    c8 f f,
    c' f f, a16 bes c8 d
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 3/4
    d4 g4. a8
    fis2 g4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 2/4
  d4 e d8 b16 c d8 e
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Minuet"
  \time 3/8
  d8 b g d' b16 c d8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 16
    d16
    d8. e16 d8. c16 b4. r16 c
    }


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  d8 
  d g, g e'
  d16 c b a g8 e'
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Adagio"
  \time 4/4
  g8 b16 c d8 c16 b a g a8 r d 
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Giga. Allegro"
  \time 6/8
  \partial 8
  d8
  b a g  e'4 e8
  e d c d4 g,8
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  c8 e16 f  g8 g, c16 b c8 r g  
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 2/4
  g'4 a
  b,4. r16 c
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Adagio"
  \time 3/4
  c2 e16 d c8 c2 c4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Allegro"
  \time 2/4
    c4 g c8. d16 e8. d16
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Minuet"
  \time 3/8
  g'8 e16 f g8 c,4 d8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 3/4
  \partial 4
  g4
  c8 b c4 d
  e8 d e4 f
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 2/4
  c8. g16 c8. d16
  e4. f8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Adagio"
  \time 3/4
  g2 a8 c16 b
  c2 d8 f16 e
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Minuet"
  \time 3/8
    c16 d e8 d
    c16 d e8 d
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 3/4
  a8. gis16 a8. b16 c8. d16
  c8. b16 c8. d16 c8. d16
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro"
  \time 3/4
  a4 c e
  a a, g'
  f f, e'
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Adagio"
  \time 3/4
  e4 f gis, a2 b4
  c16 d e f d4. c8
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Minuet"
  \time 3/8
  e8 a, gis
  a a16 b c d
  e8 a, gis
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
    d4. r16 d e8. d16 d8. d16
    e8. d16 c8. d16 b4. r16 d
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 2/2
  g2 d'
  b2. b4
  e fis g d
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Adagio"
  \time 3/4
  d4 b4. c8
  a2 g4
  d' e8 fis g8. a,16
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Giga. Allegro"
  \time 6/8
  \partial 8
    d8
    b a g g'4 b,8
    c d e d4 g,8
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Pastorale " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Pastorale"
  \time 6/8
  d4.~ d4 e8
  d8. c16 b8  c8. b16 a8
}