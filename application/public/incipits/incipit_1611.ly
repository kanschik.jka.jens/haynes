\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  d4~ \times 2/3 { d16[ e fis]} \times 2/3 { g16[ a b]}
  b8 a r16 c, c c
  c8 b b'~ \times 2/3 { b16[ a g] } g8 fis r16
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Presto"
  \time 2/2
  g2~ g8 b' g d
  b4 g' d b
  g2. e'4
  \grace e8 d2. fis,4
  g4. a16 b c4 b
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Siciliana"
  \time 6/8
  g'8. fis16 g8  g dis e
  g8. fis16 g8  g fis, g
  fis'8. a16 c8~ c8. b16 a8
  g8. a16 fis8 \grace fis8 e4 r8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro un poco"
  \time 3/8
  g'16 fis g8 e
  e d c'
  r b16 g a fis
  fis8 g e
  r d16 b c a
  a8 b d
  e16 fis fis8. e32 fis
}