 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
    bes8 bes r d bes bes r f
    bes bes r d bes bes r f
    bes4 r bes8 c d es
    f4 f f fis4
    g4 g g8 a bes g
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Adagio"
  \time 3/4
  g'2 bes8 g
  f4 r r
  as2 c8 as g4 r r
  bes,2 \grace f'8 es8. d16
  d4 c' bes
  bes8 g c bes as g g4 f r
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Rondo. Moderato"
  \time 2/4
  \partial 4
  bes8 c d es f g
  a, r a bes c d es f
  bes, r \grace a'8 bes r
  \grace a8 bes r \grace a8 bes r
  \grace a8 bes r \grace a8 bes \grace a8 bes
  f f g16 f es d c4
}
