\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 12/8	
     <es g>4. <es g>4. <d f>4. r4 r8
     <c es>4. <c es> <bes d> r4 r8
}
