\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  d8 g8. a32 b g16 d \grace c16 b16 \grace a16 g16 c16. e32 d8 g4~
  ~g8 fis16. g32 a8. c,16 b8[ a]

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  g4 d \grace c8 b4. c8
  d g4 fis16 e e8 d r8 d16 f
  f e e8 r8
 
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Vivace"
  \time 3/4
 g4 b16 a b c b4
 b d16 c d e d4
 g b16 a b c b4
 a g r4
 
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  d8 a r8 d16. e64 fis fis8 e g4
  fis32[ d16. g32 e16.] fis16 d16. e32 cis16 
  \times 2/3 {d16[ e fis]}  \times 2/3 {fis16[ e d]}

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  d8 cis16 e e8 e e4. fis16 g
  fis32 g a8. fis32 e d8. b'8 a r8
 
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Vivace"
  \time 3/4
  d2 \times 2/3 {cis8 d e}
  d8 g g2
  fis \times 2/3 {e8 fis g}
  fis8 b b2

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 3/4
  b16 c d e d4. g8
  \times 2/3 {fis8 g a} a4. c,8
  \times 2/3 {b8 a g} \grace dis'8 e2
  \grace g,8 fis2 g4

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 4/4
  g8 a16 g g16. c32 g16. f32 e16 f g8 c,4~
  ~c8[ b ]
 
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 2/4
  r8 g8[ d c]
  b4 a
  g8[ g' d c] 
  b4 a
  g8 g'4 fis8
  g16 a b4 a8
 
}



  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allegro non tanto"
  \time 2/2
  e8 b g'2 fis4
  e8 dis e4 r2
  fis4. g16 a fis4. g16 a
  g8 a b4 r2

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Largo"
  \time 3/8
  d16 g g8 fis
  g16. a32 b a8 r8
  c32 a16. b32 g16. a32 fis16.
  g16. fis64 e d8 c
  b
 
 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Allegro"
  \time 2/4
 \partial 8
 e16 fis 
 g8 fis16 e fis8 d16 dis
 e8 b4 a8
 g g' fis e16 dis 
 e8 b4 
 
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  c8
  f f4 \times 2/3 {a16 g f} g8 c, r8 bes'8
  a8. bes32 c f,8. g32 a a4 g8 r8

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andantino"
  \time 3/8
  d8 e f
  g16 bes a g f e
  f16. e32 d8 r16 a'16
  f16. e32 d8 r16
 
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 2/4
  f4 c8. f16
  f4 e8 r8
  bes'8 bes4 a16 g
  g4 a8 r8 
  r8 bes8[ g e]
  f e16 d c8 bes
 
 
}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 3/4
  a8 e e4. cis8
  cis4 d cis 
  r8 e8 fis gis a e
  e4 fis e

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 4/4
  a8 d16. e64 fis fis16 e g8 g16 fis a8 a8. b32 c
  b16. a32 g16. fis32 fis16 e b' d, 
 
 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Allegro"
  \time 2/4
  \partial 8
  a16 b cis d e8 a, cis16 d
  e d cis4 e16 fis
  gis8 a4 cis,16 d
  e d cis8~cis8
 
}

