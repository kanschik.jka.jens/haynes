 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe and Soprano"
  \key b\minor
   \tempo ""
  \time 3/4
  b4 b b b4. cis8 b4
  cis4 cis8 d e fis d4 b r
}
