\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}





\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. [Allegro] [Tutti]"
  \time 4/4
    f,4. f' e4 f
    des des2.~ des8 bes'4 g e des8
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. [Allegro] [Solo]"
  \time 4/4
  \set Score.skipBars = ##t
    R1*12
    c4 as'8 f e4 f
    \grace es8 des4 des2.~
    des8 bes'4 g e des8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Grazioso"
  \time 3/4
  a,8 bes16 c c4. f8
  f4 e f
  d8 d4 d8~ d16 f e d
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "3. Allegro [Tutti]"
  \time 2/4
  \partial 8
  c8
  as c g bes as16 g f8 es4
  f8 f'4 e8 f4
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "3. Allegro [Solo]"
  \time 2/4
  \set Score.skipBars = ##t
  \partial 8
  r8
   R2*45
   c2 f e g
   f8 c4 des8~ des c4 bes8
}

