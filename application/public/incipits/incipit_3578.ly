\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 4/4
  \partial 4
    g4
    c2. e8 c
    g'2~ g8 f e d
    c4 c2 e4 d8 c c4. 
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio con moto"
  \time 3/4
  \partial 4
    c4
    f8 c c4. d8
    c16 bes a g g4. bes8
    a16 c bes g f4 a16 g f g f4
}