\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonate I.ere" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Grave "
  \time 4/4
  r2 f8[ g] a bes
  es,8. f32 g f8. es16 d8[ bes] f' f,
  g[ bes] es g, a[ f]
  
  
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Gay "
  \time 2/2
  r4 bes4 d f
  a, f g a 
  bes f f'2~
  ~f es~
  es4
 
  
  
}

\relative c''' {
  \clef treble
  \key bes\major
   \tempo "3. Grave "
  \time 3/2
  r2 g2. d4
  bes'1.~
  ~bes2 es,2. c4
  a'1.~
  ~a2
  
  
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Sicilienne. gracieusement "
  \time 6/4
  \partial 4
  d4 
  d2 g,4 g2 g'4
  fis2. \grace es8 d4  r4 d4
  es d c d c bes
  a bes g fis2 
 
  
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "5. Gigue "
  \time 6/8
  \partial 8
  bes8 
  d4 bes8 f'4 bes,8
  bes'4 f8 bes a bes
  f es d es d c
  d c d bes4 
  
  
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonate II.e" 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Air grave "
  \time 3/2
  r2 g2 d
  b' a2. g4
  fis2 \grace e8 d2. d4
  d2 c2. c4
  c2.


}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Gay "
  \time 6/8
  r8 d4 g8 g g 
  fis d d e16 d e g fis a
  g8 d b e c a
  d b g d g4

 
  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Grave "
  \time 3/2
  r2 g2 b
  d b d
  g1.~
  ~g~
  ~g2 e cis
  fis1.~fis2
  

}

\relative c''' {
  \clef treble
  \key b\minor
   \tempo "4. Gavotte "
  \time 2/2
  \partial 2
  b4. ais8
  b4 fis g e8. g16
  fis4 \grace e8 d4 \grace fis8 e4 \grace d8 cis4
  d4 e8. fis16 e4. d8 cis4 b
  
  
  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Gigue "
  \time 6/8
  \partial 4.
  r8 d8 c
  b c b a b g
  d'4 g,8 g' a b
  a b g fis e fis
  g4 d8 r8
  
   

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonate III." 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Grave "
  \time 4/4
  \partial 8
  a8 c8. c16 d8 e b8. b16 d8. c32 b
  c8 \grace b8 a8 a'4~ a8. g32 a b8. a16
  gis8. 
  
  
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allemande "
  \time 4/4
  \partial 8
  r16 a16
  a4~a16 c b a b a b c b c d b
  c8 \grace b8 a8 e'8 d16. c32 d16 c d e  d e f d
  
  
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Gavotte "
  \time 2/2
  \partial 2
  a4 c
  b e, b' d
  c a a' gis a e c d
  e2
  
  
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Rondeau "
  \time 3/4
  a2 e4
  b'2 e,4
  cis'4. d8 b4
  \grace b8 cis2 \grace b8 a4
  d2 cis4
  \grace cis8 b4 \grace a8 gis4 a
  b \grace b8 cis4. d8 
  b2.
  
  
  
}

\relative c''' {
  \clef treble
  \key a\minor
   \tempo "5. Courante "
  \time 3/4
  \partial 8
  a8 a4. gis8 a e
  f e d c b a 
  d f e d c b
  c b a b c d
  e4 a4. b8 gis4.
 
  
  
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonate IV." 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "1. Allemande. un peu légére "
  \time 4/4
  \partial 8
  r16 b16
  b8 a g8. fis16 g8 \grace fis8 e8 a8. a16
  a8 g fis8. e16 dis8 b c[ dis,]
  

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Gigue "
  \time 6/8
  \partial 4.
  r8 e8 dis
  e4 fis8 g4 a8
  b4 e,8 b' a b
  g fis g e dis e
  dis c b r8
  

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "3. Sarabande "
  \time 3/4
  b4 b4. a8
  b4 \grace a8 gis4 b4
  cis \grace cis8 dis4. e8 
  fis,2 \grace {e8[ fis]} e4
  

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Gay"
  \time 6/8
  r8 b8 b a a16 b c a
  b8 c16 b a g fis e fis g a fis 
  g8 e e' e8. dis16 e8
  fis
 

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonate V. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Grave "
  \time 4/4
  e8. fis32 gis fis8. e16 fis8 \grace gis8 b,8 e8. gis,16
  a8. b32 cis a8 gis16 a gis8[ \grace fis8 e8] 
  

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Un peu gay et fiere "
  \time 2/2
  e2 a
  gis4 gis8. a16 b4 a
  gis b gis e
  b2 b4 b
  b a8. gis16 a2
 

}


\relative c {
  \clef bass
  \key e\major
   \tempo "3. Facilité pour l'Entrez "
  \time 2/2
  b2 e
  cis4 dis8. e16 fis4 e
  dis fis dis b
  


}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Sarabande. grave "
  \time 3/4
  e4 g b
  dis,4. e8 fis4
  fis8. g32 a g4. fis8
  fis2 e4
  

}

\relative c' {
  \clef treble
  \key e\major
   \tempo "5. gay "
  \time 6/8
  e16 fis gis fis gis e fis gis a gis a fis
  gis8 b gis a cis a
  b b16 cis d8 cis cis16 d e8
  

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonate VI. " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. Un peu légére "
  \time 3/4
  r4 a8.[ g16 fis8. e16]
  d4. cis8[ d8. e16]
  cis4 d e
  fis8. g32 a g4. fis8 e2
  
  

}

\relative c''' {
  \clef treble
  \key d\minor
   \tempo "2. Air badin "
  \time 2/2
  \partial 4
  a4 f e8 f g f e d 
  d cis d e f g a bes
  bes a g f e f g e 
  g f e f d4
   
  

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Gigue "
  \time 6/8
  \partial 8
  cis8 d a fis d4 a'8
  a4.~ a8 g a
  b c b a b g fis4. r4
  

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Gavotte. Gracieusement "
  \time 2/2
  \partial 2
  d4 cis8. d16
  a4 d e fis8. g16
  fis4 \grace e8 d4 fis4. fis8
  gis4 a e gis a2
  

}
\relative c''' {
  \clef treble
  \key d\major
   \tempo "5. gay "
  \time 4/4
  r8 a8 a[ d,] g g g fis16 e
  fis8 g16 fis g fis e d e8[ e] a a,
  b[ d] g g, 
  

}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Seconde Partie en Trio - Sonate VII. " 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \key e\minor
   \tempo "1. Grave "
  \time 2/2
  e2. r16 e16 e e 
  g2 g4. g8 
  b2 b4. b8
  g'2. r8
 

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Gay "
  \time 4/4
  r8 e16 fis gis8 e a[ fis] b a 
  gis fis16 gis e fis gis a b8 fis b4~
  ~
  b8[ cis,]
 

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Mouvement de Chaconne "
  \time 3/4
  r4 e4 b
  g fis e
  b' g' fis
  e4. dis8 e4
  dis b'4. b8
  b4 a g fis4. fis8 g4 a4
  

}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "4. grave "
  \time 3/2
  r2 b4. a8 g4. b8
  e,2 a1~
  ~a2 g4. fis8 g4. a8
  fis2 b1~
  ~b2
  
  
  

}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "5. gay "
  \time 4/4
  b16 a g a b a g a b a g a b a g a
  a g fis g a g fis g
  

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Seconde Partie en Trio - Sonate VIII. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. grave "
  \time 2/2
  d4. d8 e4. e8
  a,4. a'8 g fis e d
  cis d e cis fis2
  e d
  

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. gay "
  \time 4/4
  r4 d4 a4. a8
  b d16 cis b cis d8 cis a16 b cis d e cis
  fis g fis g a8 d, 
  

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "3. Sarabande légére "
  \time 3/4
  a4 a b
  g \grace g8 fis4 a 
  g4. a8 fis4
  e2 d4
  

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "4. Gigue "
  \time 6/8
  \partial 4.
  a4 g8
  fis e d r4.
  r4. a'4 g8 
  fis e d cis b cis
  d4. e
  fis8 e fis g fis g 
  a4.~ a4
  

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Seconde Partie en Trio - Sonate IX. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Allemande "
  \time 4/4
  \partial 8
  r16 b16
  b8[ ais] ais ais d8. cis16 b16. cis32 d16. b32
  g'4~ g16. fis32 e16. g32
  fis16. e32 d16. e32
  

}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Courante "
  \time 6/4
  \partial 4
  r8 fis8
  fis4. d8 g4 fis e4. fis8
  d4. cis8 d e cis d cis4. b8
  b2. fis'4 gis ais b4.
 

}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Gavotte "
  \time 2/2
  \partial 2
  b4 d
  cis fis \grace e8 d4 \grace cis8 b4
  ais \grace g8 fis4 r2
  

}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "4. Sarabande "
  \time 3/4
  fis4 fis4. b8
  ais4. ais8 b4
  fis e4. d16 cis
  \grace cis8 d2 \grace cis8 b4
  
 
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "5. Gay "
  \time 6/8
  b4. cis
  d8 b d g e16 fis g8
  fis4 b ais 
  b4.~ b
  
 

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Seconde Partie en Trio - Sonate X. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Un peu grave "
  \time 3/4
  a4. a8[ a8. a16]
  b4 b cis
  d d4. e8
  cis4 \grace b8 a4 
   

}


\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. gay "
  \time 4/4
  e4. e8 fis fis fis gis16 fis
  e8 fis16 gis a4~ a8 gis fis e16 d
  cis e d cis b cis a b gis8[ e] a4~
  ~
  a gis a r16
  

}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Sarabande "
  \time 3/4
  e4 d c
  b4. b8 c4
  d d4. c16 b
  \grace b8 c2 \grace b8 a4
  a' c \grace b8 a4
  \grace g8 f4 dis
  

}


\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. gay "
  \time 4/4
  e16 a gis a e a gis a cis, a' gis a b, a' gis a
  a,8 e b' e, cis' e, d' e,
  e'16 fis e d cis e d cis
  

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Seconde Partie en Trio - Sonate XI. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Marché. Grave"
  \time 2/2
  \partial 8
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  d8 d2. r8 es8
  es2. r8 a,8 
  a2. r8 bes8
  bes4.[ \times 2/7 {g16 a bes c d e fis]}
  g4.[ \times 2/7 {bes,16 c d e fis g a]}
  bes4. bes8 g4 d
  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Gay "
  \time 4/4
  d4. c8 b16 c b c a8 d b g d'2 cis4
  d4. c8 c d16 c b8 c16 b
  a4. a8 b g g8. fis16 g2
  

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "3. Grave "
  \time 4/4
  bes2 a 
  bes1~
  ~bes4 a8 g fis4. g8
  g4 fis g r4
 

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "4. Gigue "
  \time 6/8
  \partial 8
  g8 bes a g bes a g
  a4. r4 d,8
  g a bes a bes g fis4. r4
  

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Seconde Partie en Trio - Sonate XII. " 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \key f\major
   \tempo "1. Allemande "
  \time 4/4
  \partial 8
  r16 f16
  f4~ f16 . g32 a16. f32 g16. a32 g16. a32 bes8 g
  a f c'[ f] e8. d16 e16. f32 g16. e32
  f8
  

}

\relative c' {
  \clef treble
  \key f\major
   \tempo "2. Gay "
  \time 6/8
  f8 f f g4 a8
  bes a g a16 bes a g f8
  a a a bes4 c8
  d c bes c16 d c bes a8
 

}

\relative c' {
  \clef treble
  \key f\minor
   \tempo "3. Air grave "
  \time 3/4
  \partial 4
  f4 as4. bes8 g4
  as \grace g8 f4 as
  \grace {as8[ bes]} c4. des8 bes4
  c \grace bes8 as4 
  

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Un peu gay "
  \time 4/4
  r4 f8 f bes[ g] c bes
  a f c'2 bes4~ bes a g2
  f4. f8 e e e f16 e
  d e f e d4 c
  

}


