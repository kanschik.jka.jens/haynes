 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  a8[ a16 bes] c8 c f,4 r8 f
  g f bes a  g c, c a'
  g c, c a' g c, c a'
  g4 r r2
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Adagio"
  \time 3/4
  d8 c c4. bes8
  a g g4. f8
  e f e4. d8 
  d d' fis, a c b
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 6/8
  f,4 f8 f c f
  f a f f c f
  f a c c bes a
  g4 c,8 c4 r8
}

