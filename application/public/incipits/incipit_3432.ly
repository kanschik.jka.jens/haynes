\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key b\minor
    \time 4/4
    \tempo "1. Andante"
      r16 d fis d  b4 r16 e16 g e
      ais,4
      r16 b d b  b'4
}

 