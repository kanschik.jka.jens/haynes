
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ier. Divertissement "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude. Lentement"
  \time 2/2
\partial 2
 c4. b16 a
 g2~ g8.
\override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
 \times 2/3 {a32 g a} b32 c d e f g e f 
 e4 \grace d8 c4
 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe. Divertissement "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prelude. Lentement"
  \time 2/2
\partial 4
 d4 g4. fis16 e d8. c16 b[ c a b] 
 b4 \grace a8 g4 r4 b8 c
 d4~ d16 b c d d4. c16 d
 e4.  e8 e4
 
 
 
} 
 