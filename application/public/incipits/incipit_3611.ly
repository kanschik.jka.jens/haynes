 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "[Allegro]"
  \time 3/4
    r4 g d'
    b4. b8[ b c]
    a4. a8[ a b]
    c4 c4. d8
    b4. b8 a4
}

