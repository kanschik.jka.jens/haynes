\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 4/4
  d16. a32 a16. a32 f8 r e'16. a,32 a16. a32 a,8 r
  f''16. a,32 a16. a32 bes16.
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro"
  \time 2/2
  r8 a d d cis cis f f
  e4 d2 cis4
  d8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Largo"
  \time 4/4  << 	
       {
         a'4 r g r
       f4 r g }
      \new Staff {  
        \key d\minor
        \clef bass
            f,,16 e f g a g a f c' b c d e d e c
          f e d c bes a g f bes a bes d c bes a bes}
    >>

}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro"
  \time 6/8
  d8 f d e g e
  f4. cis
}
