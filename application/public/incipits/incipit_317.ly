\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Ouverture"
  \time 2/2
      r4 g8 g es'4. f8
      d4. d8 c4 d8 a
      bes4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. gravement"
  \time 2/2
  \partial 2
      g'4. a8
      fis4 d  bes'4. c8
      a4 a
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Ouverture"
  \time 2/2
    g'4. d8 g4 a
    fis2~ fis8 g a fis
    g4. a8 b4 c
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Simphonie"
  \time 2/2
   g'1
   g,4. a8 b4. c8 d2~ d4. g8
   fis4. e8 d4. c8
   b4. c8 d4.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Ouverture"
  \time 2/2
    fis4. e8 d4 e8 fis
    e4 a, a'4. a8
    a4. g8 g4. a8 fis2.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. gay"
  \time 4/4
     r4 r8 fis g fis16 g  e8 d16 e
     fis4 r r2
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Ouverture"
  \time 2/2
      f4. g8 a4. g16 f
      g2~ g4. c,8
      f4. e8 d4. g8
      e4. d8 c4.
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
      r2 a8. c16 f8. f16 f1~ f4 g8 f16 g a8. a16 a8. bes16
      g4.
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Ouverture"
  \time 2/2
    b2~ b8. a16 g8. fis16
    g4 e c'4. d8
    b4. b8 e4. fis8
    dis2.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. o.Bez."
  \time 2/2
    b4. g'8 dis4. fis8
    b,4. g'8 dis4. fis8
    b,2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Ouverture"
  \time 2/2
      a'4 e8 e e4. fis
      cis4 cis8 cis cis4. d8
      b4 b2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. gay"
  \time 2/2
    r4 a8 b  cis4 cis8 d
    e4 e8 e e4 fis
    e4 e8 e e4 fis
    e4 d8 cis
}

