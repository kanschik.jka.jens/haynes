\version "2.16.1"

#(set-global-staff-size 14)

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Prelude. Lentement"
  \time 4/4
  r4 g2 fis8 a
  d,2~ d8 g c,4~ 
  c8.[ b16 b8. a16] a4 d~ d cis8 e a,4
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Fugue"
  \time 2/2
   \set Score.skipBars = ##t
   R1*2 r4 d4 d d 
   g4. b16 a g8 fis e g
   fis d fis a fis d fis d
   b g d'2 c4~ c b a2

}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gravement"
  \time 3/4
  r2.
  r4 g4 b
  dis, fis a~
  a g b fis fis2
  e2 r4

}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Gigue"
  \time 6/8
  \partial 8
  d8 g, b d g d g
  fis4. r4 d8 g b g d b' d, e4. c'
  d,4 c'8 b4 b,8
  c4 e8 a4 c,8
 
  

}
