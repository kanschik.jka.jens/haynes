\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro "
  \time 4/4
  c2. d8 c
  e2 f8 e
  g2~ g8 f16 g a8 g e4 f r2
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio non troppe"
  \time 3/4   \set Score.skipBars = ##t
   R2.*2
   d2 d8. c16
   c8 b4 b8 d16 c b a
   a8. g16 g4 r
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Un poco Allegretto "
  \time 2/2
  e,2. d4 d c2 c4
  d4. e16 f e4 d
  d2 e4 r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  c2. b4 b a2 a4
  a8 b c b  d c b a
  a4 g r2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante"
  \time 2/2
  \partial 2
  b4 c
  d8. e32 d b4 e8. fis16 g8 fis
  \grace fis8 e4 d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegretto"
  \time 3/8
   e4 d8 c b c
   f4 e8 d c d
   g4 f8 e d c
   b c e e4 d8
}

\pageBreak

\markuplist {
    \fontsize #3
    "Quartetto III " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andantino"
  \time 6/8
  \partial 8
  g8
  d'4 d8 d4 c8
  c4 b8 a a[ b] c b c e d c b4 r8 
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio con Variazioni"
  \time 2/4
   e4 g
   c, c'
   a8 g \grace g8 f4
   e g,16 c e g
   d4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegretto"
  \time 3/8
   b4 c8 d4 g8
   b,4 c8 d4 g8
}
