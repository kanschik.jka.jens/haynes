\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4
    g4 g c4. c8
    d d d c16 d e8 c e f g d g f e4 e
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4
  r8 g' e g f a f a
  g e16 f g8 e f d16 e f8 d e
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4
    g'4 g16 a g f e8 e d d
    e4 d8 d e e d16 e f g
    e8 c e16 f g a f8. e16 d4 e2 r
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4
  g'4 a16 g a b g4. e8 f e f d e c16 d e8 e
  d d16 e f8 f e e16 f g8 g f4 e d2 c r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4
  e4 d e e8 f g4 g f8 g e f d4 b8 c d4 d
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4
  c16 g a b c d e f
  g8 d g f
  e c c c b b b b
  c
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4
  c4 c e4. f8 e c e f g4 e
  g8 f g a g4. f8 e2.
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4
  g'4 g16 a g f e4 d
  c4. g8 c d e16 d e c
  d8 e f16 e f d e4. e8
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4
  r2 e4 d e r r2
  r e4 d e r e d e8 e e e d d g g
  g g fis fis g4 r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4
  r2 e4 d e r r2
  r e4 d e r e d e8 e e e d d g g
  g g fis fis g4 r
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4
  c8 c16 c c8 c16 c
  c8 g c d e e16 e e8 e16 e e8 c e f
  g4 g
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4
  r4 g'8 f e4 d e r r2
  r4 e8 f16 g c,8 c c16 d e f
  d4 d r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite XIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4
  g'8. a16 g a g f e8. f16 e f e d
  c8 c c8. b16 c4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite XIV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4	
  r8 g' e c
  g'4 a
  g f16 e f g
  e4 d16 c d e f4 e d c~ c b c2
}

