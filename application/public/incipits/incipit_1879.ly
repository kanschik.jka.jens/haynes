 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "Scene III. Leger"
  \time 2/2	
  fis2 e8 g fis e
  d cis \grace cis16 d2 d4
  cis8 d e fis g4 fis
  fis2 e
}
