 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro molto"
  \time 2/2
  \partial 4
    d4
    a'1~ a~ a~ a
    a4 f8 a g4 e8 g
    f4 d8 f e4 cis8 e
    d4 bes2 a8 g f e d4 r2
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Andante"
  \time 3/4
  \grace { e,32[ f g]} f4. a16 bes \grace d16 c8 \grace bes16 a8
  g16 c \grace d16 c16. b64 c e8 e4 g8
  g[ f] f
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Allegro assai"
  \time 3/8
  f,16 a d8 d
  \grace d8 cis4 r8
  cis16 d e8 e \grace e8 d4 r8
  d16 e f8 f f4 g16 e d4 e16 cis d4 r8
}
