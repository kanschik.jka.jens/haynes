 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Andante"
  \time 4/4
    g'8 d r16. es32 d16. c32 bes16. c32 d8 r16. es32 d16. c32
    bes16. d32 g16. bes32 bes16 a a g fis8 g r
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro"
  \time 2/2
  \partial 4
  d4 bes' bes2 a4
  g8. fis16 g4 r bes
  a4. c8 c4. bes16 a
  bes4 a r
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Allegro assai"
  \time 3/8
    g'4. fis4 r16 g
    d8 c bes a bes c
    bes16 c d8 r16 d
    es8 d r16
}
