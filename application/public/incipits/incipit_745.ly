 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Moderato"
  \time 2/4
  \partial 8
    c8
    f2 f f f16 e bes'4 bes,8
    \grace bes8 a4 r
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Menuetto"
  \time 3/4
  f2 a4 g8 f f2
  g2 bes4 a8 g g2
  f2. f
  \grace c8 bes4 a g f2.
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Adagio ma non troppo"
  \time 2/4
  c2 g'4. a16 b c8 c,16 e \grace e8 d8 c16 b
  c4 r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Menuetto"
  \time 3/4
    c2 d4 c8 bes a g fis g a2 bes4
    a8 g f e d c
    f'4 f g a
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "Trio"
  \time 3/4
  f2. f4 bes8 a g f
  f2. f4 bes8 a g fis
  fis g g4. f8 f e e4. d8
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "5. Andante"
  \time 2/4
  f4 g8 a bes g a f d4 g
  \grace f8 e8. d16 c8 d16 e f4 g8 a bes g a f
}