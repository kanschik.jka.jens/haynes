\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran + Alt): Gedenk an Jesu bittern Tod [BWV 101/6]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Traverso"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key d\minor
                      \tempo "Duett"
                      \time 12/8
                      % Voice 1
                  r4. r8 r8 d16 e32 f \grace f8 e8. f16 d8 d cis e16 f32 g
                  \grace g8 f8. g16 e8 
                 
           
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe da Caccia "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key d\minor
                     \set Score.skipBars = ##t
                     % Voice 2
                      r4. r8 r8 a,8 a2.~
                      ~a4.
                       
                
                  }
                  
                  
>>
}


                  
                   
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Sopran"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key d\minor
                      \tempo "Duett [Choralmelodie: Vater unser im Himmelreich]"
                      \time 12/8
                      % Voice 1
                  R1.*15 r4. r8 r8 e8 e2.~
                   ~e4. c d e
                   
                      
                 
           
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Alt"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key d\minor
                     \set Score.skipBars = ##t
                     % Voice 2
                      R1.*12 r4. r8 r8 a,8 a2.~ 
                    a4. f g a 
                    f e d4 a'8 d4 f,8
                    e4 e8 a gis a 
                    
                       
                
                  }
                  
                  
>>
}




	
	
