\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegretto"
  \time 2/4
  \partial 16
  c16
  c4. a16 bes c8 c c c
  d4. bes16 c d8 d d d
  c4. a16 bes c8 c c f
  c4 bes a
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 4/4
  \grace { d16[ es]} f4 \times 2/3 { f16[ es d]} \times 2/3 { d16[ c bes]}
  \grace { g'16[ a]} bes4 \times 2/3 { bes16[ a g]} \times 2/3 { g16[ f es]}
  f16 f8 es32 d es8 es32 f64 g f32 es
  \times 2/3 {d16[ c bes]} bes8 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 3/8
  c8. f16 a c
  \grace c8 bes4 a8
  g16 bes a g f e
  f c32 d c16 c c8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro ma non Tanto"
  \time 2/4
  \partial 8
  g8 g16 bes bes8 r bes
  bes16 d d8 r d
  d16 g g bes bes g g d
  c8. es16 es4
}



\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Andante"
  \time 3/4
  c4 es g
  as2 g8. f16
  es8. d16 c4 c'
  as8 g16 f es4 d
  c r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 2/2
  \partial 4
  d4
  g bes r d,
  b c r c
  fis a r c, 
  a bes r g' 
  g es r g g d r g
  \grace f8 es4 d8 c \grace bes8 a2
  g r4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegretto"
  \time 2/4
  \times 2/3 { d16[ e d]} g4 d8
  \times 2/3 { e16[ fis e]} g4 e8
  \grace e8 d8 c16 b \grace d8 c4 b r
  
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Largo. Cantabile"
  \time 6/8
  d8. es16 d8 g4 d8
  es4 d8 c4.
  c16 es d c fis g a g f es d c
  bes8. c16 d8 c4.
}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 4/4
  r4 d g b
  \grace d,8 c1
  b4 d g b
  \grace d,8 c1
  b4 g' r e
  r c r a
  d1
}
  


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegretto"
  \time 3/4
  c4~ \times 2/3 { c8[ d e]} \times 2/3 { d8[ e f]}
  \grace f8 e4~ \times 2/3 { e8[ f g]} \times 2/3 { f8[ g a]}
  a8. f32 g a4 \grace g8 \times 2/3 {f8[ e d]}
  c8. b16 c4 r
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Largo"
  \time 4/4
  c1~
  c16 f f d d c c bes a16. g32 f8 r4
  
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegretto"
  \time 3/8
  g'8. a16 g8
  g f r
  e16 f32 g f8 e
  e d r
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/4
  bes4~ bes8 es16 g
  \grace g8 f4. es16 d
  c8. d32 es
  d8 c
  bes4. 
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Largetto"
  \time 4/4
  es8 bes4 \times 2/3 { es16[ f g] } f8 bes4 \grace a8 g8
  es16. d32 es8 r es d g4 \grace f8 es
  c16. b32 c8 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 2/2
  \partial 4
  f4
  bes f r g
  es c r f
  d bes2 a4 bes f r
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegretto"
  \time 2/4
  \partial 8
  e16. f32
  g8 c, r16 c' a f
  \grace f8 g8 c, r16 c' a f
  \grace f8 g8 c, r g'16 a
  f16. e32 f8 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 4/4
  c8 a16 bes c8 c f8. a16 f4
  g8 a32 bes a g f8 e f16 c32 d c16 c c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 6/8
  g'2.~ g~
  g8 c f, e4 d8
  c4. g
  
}

