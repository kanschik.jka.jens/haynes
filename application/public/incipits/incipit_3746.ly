\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key g\minor
    \time 2/4
    \tempo "1. Allegro"
    \partial 8
   d32 c bes a|
   bes8 g a fis|
   g4 r8 d'32 c bes a|
   bes 8 g es' b |
   c4
  }

\relative c'' {
  \clef treble  
  \key g\minor
    \time 4/4
    \tempo "2. Adagio"
    bes4~ bes16 d c bes  a8 bes bes16 d f bes
    g16. bes32  f16. bes32  es,16. bes'32 d,16. bes'32 c,4 r8 d
    c4 r8
  }

\relative c'' {
  \clef treble  
  \key g\minor
    \time 4/4
    \tempo "3. Fuga. Presto"
      r4 g2 bes4
      a d, d' c8 d
      bes4 g d' d
      es1 d c bes a g2 r4
  }
