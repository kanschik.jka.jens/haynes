\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Ich bin vergnügt mit meinem Glücke [BWV 84/1]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key e\minor
   \tempo "o. Bez."
   \time 3/4
   b8.[ a16 g8. fis16 e8. dis16]
   e2 e'8 d
   d e16 fis g8 e4 d8
   c16 b c8 a'8 c,4 b8

 
   
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key e\minor
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
  R2.*24 b8.[ a16 g8. fis16 e8. dis16]
   e2 e'8 d
   d e16 fis g8 e4 d8
   c2 b4

  
}



