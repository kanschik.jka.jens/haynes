 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Moderato"
  \time 4/4
    b4. d32 cis b16 b4. d32 cis b16
    ais' b b,4 b8 b cis16 g' fis e d cis
    d cis b g fis[ e d]
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Amabile"
  \time 2/4
  \partial 8
  a8
  \times 2/3 { fis16[ g a] } d,4 d'8
  d4 cis8 e,
  \times 2/3 { cis16[ d e] } a,4 g'8
  g4 fis8
  
}

\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "3. Presto"
  \time 6/8
  b4 r8 fis'4 r8
  d cis b  ais gis fis
  b4 r8 fis' r8
  d cis b  e d cis
}
