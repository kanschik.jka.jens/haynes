 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
    \partial 16
    g'16
    g4. a16 g e4 r8 r16 e
    e4. f16 e c4 r8 r16 c
    c4. d16 c b4 a
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Andante"
  \time 3/4
  c4 g4. as16 f
  g4 c, \grace es16 d8. c16
  c8. c'16
  g4. as16 f
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Allegretto"
  \time 2/4
  g'8. f16 e8 d
  c16 d c d  c8 r16 e,32 f
  g8. a32 g c16 g f e
  f8. g32 a g4
}

