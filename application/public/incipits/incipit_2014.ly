\version "2.16.1"
         #(set-global-staff-size 14)

         
         \relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 4/4
      g'4 r8 d16. d32 d8. c16 bes8. a16
      bes4 r16 bes a bes  g8. a16 a8. g16
      fis4 r16 a d a bes8. bes16 f'8. f16
      f8 d
}
         