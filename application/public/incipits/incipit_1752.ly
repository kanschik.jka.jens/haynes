\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef bass
  \key f\major
   \tempo "1. Allegro [Bassoon]"
  \time 2/2
  r4 r8 f,,8 c'8. c16  c16. bes32 a16. g32
  f16. g32 a16. bes32 c8. c16  d8 c16. a32 bes4
  a r8 f d' d d d
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Hautbois]"
      \set Score.skipBars = ##t
  \time 2/2
  R1*10 f4. g8 f4. g8
  f2 r
  f8. g16 f16. g32 f16. g32 f4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 3/2
  a2 d e4 f
  cis2 d r
  r r r16 g, a bes cis d e f
  g2 g4 bes g2 g
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro presto"
  \time 3/8
  c8 c c
  a f16 g a bes
  c8 d e
  f4.
}

