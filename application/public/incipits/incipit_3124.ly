\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Der Heiland kennet ja die Seinen [BWV 109/5]" 
    }
    \vspace #1.5
}
 
 
\relative c''' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key f\major
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                      a4 g32 f e8. f4
                      d bes4. c16 d
                      c4 bes8 a g f
                      g4. a16 bes a4
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key f\major
                     \set Score.skipBars = ##t
                        % Voice 2
                   c4 bes32 a g8. a4
                   bes g4. a16 bes
                   a4 g8 f e d
                   e2 f4
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key f\major
   \tempo "o. Bez."
  \time 3/4
     \set Score.skipBars = ##t
     R2.*16 a4 g32 f e8. f4
     d' bes4. c16 d
     c4 f,2
     g4. a16 bes a4 
}