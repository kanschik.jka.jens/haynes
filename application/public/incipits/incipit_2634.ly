\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. [Allegro]"
  \time 2/2
  f8 a16 bes c8 c c c c c
  d c bes[ c16 bes] a8 f c'4~
  c8 d16 c b8. c16 c4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Largo"
  \time 3/4
  a'8 g f e d4 cis2 d4 e f8 e f g e2 d4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Vivace"
  \time 3/4
    c4 \grace c8 d2
    bes2 c4
    a \grace a8 bes2
    g f4
}

