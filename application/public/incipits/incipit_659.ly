
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Largo"
  \time 12/8
\partial 8
g8 c4 c8 c4 g'8 a,4 a8 a4 f'8
g,4 g8 g4 e'8 d4 c8 b4 c8
g4 g8 g4 g8 g4 g'8 c8. b16 c8 f,4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 2/4
 \partial 4
c8 c16 c 
g'8 g g4
c,8 d16 e f8 e16 d e8 c c c16 c
g8 g g4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Andante"
  \time 4/4
 c8 c c c d4 r4
d8 d d d e4 r4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 3/8
 \partial 4
r16 c16 c c 
g'8 g16 g g8
c, d e 
f e d 
e c16 c c  c 
g'8 g16 g g8

}
