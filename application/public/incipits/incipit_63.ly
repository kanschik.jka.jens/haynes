\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 2/2
  bes8 c d es
  f16 a, bes8 r g'
  f16 a, bes8 r g' f es16 d c8[ d16 bes]
  c e, f8 r
}



\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/2
  bes'8 f g[ f16 es] f8 bes, d f
  bes f g[ f16 es] f8 bes,4 g'8
  f bes,4

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Largo"
  \time 6/8
        << {
      % Voice "1"
      r4. d8 g d
      r4. c8 a' c,
      r4. d8 g d
         } \\ {
      % Voice "2"
      g,16 a bes c d8 r4.
      a16 bes c d c8 r4.
      g16 a bes c d8 r4.
      } >>
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 3/4
  bes8 f f f bes f
  c' f, f f c' f,
  d'4 bes r
}
