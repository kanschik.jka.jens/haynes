\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef bass
  \key g\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  g2 d
  b8 g g g b16 a g8 d' g,
  g' g, g g b16 a g8 d' g,
  g' g, r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*20 
  b8  d4.~ d8 c16 b \grace d16 c8 b16 a
  b8 f'4 f8 e16 fis g4 g8
  fis16  e d e  fis g a g
}
