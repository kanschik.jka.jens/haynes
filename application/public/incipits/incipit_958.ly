 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 3/4
  \partial 4
  g8. g16
  g4 e g8. g16
  g4 d g8. g16
  c4. d16 e d8 c
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 2/4
  g4 fis32 g16. fis32 g16.
  d'8. c16 b8 r
  g4 fis32 g16. fis32 g16.
  e'8. d16 c8 r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Allegretto"
  \time 6/8
  \partial 8
   g8
   g8. a16 b8 c d e
   fis8. g16 fis8 \grace fis8 e4 d8
   e c a g a c
}

