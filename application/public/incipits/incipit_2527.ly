 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 2/2
  f8 bes,16 a   bes32[ bes c d] c[ c d es] d8[ c16 d] f,8 r
  f'8 bes,16 a  bes8[ a32 bes c d]
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegro"
  \time 3/4
  f4 bes,8 a bes c
  d16 es c d  bes c a c  bes d c es
  d es c d  bes c a c  bes d c es
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Adagio"
  \time 3/4
  f2.~ f2.
  f2. \grace { f32[ g f] }
  f8. es16 es8. d16 d4
  es \grace d8 c4 r
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Gigue"
  \time 6/8
  f8 es d d es f
  g4. f4.
  es8 d c  d c bes
  c4 c8
}