\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  r8 g' g g c c, c c
  a' c, g' c, a' c, g' c,
  f a b, g' e c a' e 
  fis d g g, c fis4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*10
   g'4 f16 e d8 c16 b c8 r g'16 c, 
   a' g a8 r g16 c, a'16 g a8 r g16 c, 
   f g a g f4 e8 c r4
   
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Largo [Tutti]"
  \time 4/4
    r8 e a b16 a gis8 b c d16 c b8 b, e4. a,8 d4~
    d c b2
    a4 r r2
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Largo [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*3
   e16 d c b a8 f' e d16 c d f e d
   c8 a a'16 b g a f8 e4 d8
   e4 r r2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro Spiccato"
  \time 3/4
  r4 g' a
  b, r c a' r g
  r f2
  e4 c d
  e8 d c4
}
