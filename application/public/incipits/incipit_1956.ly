\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro moderato"
  \time 2/4
  \partial 8
  d8 g16 fis a g b a c b
  a8. b32 c b8 g
  fis16 b, e8 d16 g, c8
  c16 b e d r8

}



\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Andantino"
  \time 2/2
  \partial 4
  g4 g e8 f g4 g
  g8 e c'4 r4 e,4
  d8 f e g f4 e8 d
  e c c'4 r4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Tempo di Minuetto"
  \time 3/4
  d4 b8 d c e
  d4 b8 e c e
  d4 g8 fis g d
  \grace c8 c2 b4
  e8 fis e fis e d
  \grace d8 c2 b4

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegretto"
  \time 2/2
  e2 cis8 b cis d
  e4 e e e 
  e2 fis4 e8 d
  cis2 d4 cis8 b
  e4 a, b a8 gis
  e'2 fis4 e8 d cis2

}

\relative c''' {
  \clef treble
  \key fis\minor
   \tempo "2. Aria cantabile"
  \time 3/4
  a4 fis a16 g fis e
  e4 d8 a' fis a,
  ais4 b8 e cis e
  e4 d8 fis e g 
  g4 fis8 a g b
  a4

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Rondeau. Allegretto e gratioso"
  \time 2/4
  e4 fis8 e16 d
  e gis a8 r8 e8
  d[ cis b a] 
  b8. cis32 d cis4

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Spirituoso"
  \time 3/4
  c4 r8 g8 e c
  a'4 g r4
  c4 r8 g8 e c
  f4 e r4
  c'4 r8 g8 e c a'4 g r4

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andantino"
  \time 2/4
  \partial 8
  c8
  f8. e32 d c8 c
  c4 f8 a,16 c
  c bes  g[ bes] bes a f'[ a]
  a g g bes bes g f e
  f8 c

}
  
  \relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Tempo di Minuetto"
  \time 3/8
  c8 e d
  e g f
  g f16 e d c
  a'8 g r8
  f8 a16 f a f
  e8 g16 e g e
  d f e d c b c8 g r8

}
  


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. Vivace"
  \time 2/4
  a8 cis,4 d8
  b' dis,4 e8
  \grace d8 cis8. b'16 a g fis g
  fis2 g
  a8[ d, cis a]

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Andantino"
  \time 2/4
  g4 a8 g16 fis
  g8 a r8 a8
  b d c16 a g fis
  g8 d a'8. b32 c
  b8 d c16 a g fis g8 d r8

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "3. Tempo di Minuetto"
  \time 3/8
  a16 g32 a b8[ a]
  g16 fis32 g a8[ g]
  fis16 a g fis e d
  \grace d8 cis8.[ d32 e] d8

}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allegretto"
  \time 2/4
  \partial 8
  e8
  b'4 a
  g8 fis16 e fis8 e16 dis
  e8 b r8 c'8
  c4 b8 a g fis16 e fis8 e16 dis 
  \grace dis8 e8 b
  

}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "2. Larghetto"
  \time 3/8
  g16 e c'8 r16 a16
  g e c'8 r16 a16
  g c, d fis e g
  \grace g8 fis4 e8

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Rondeau spirituoso"
  \time 2/4
  e8 b g'4
  fis8 b, a'4
  g8 fis16 e b'8 e,
  dis fis b,4
 

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. Allegro moderato"
  \time 2/4
  a8 a4 g8
  fis16 d' a4 g8
  fis16 d' a4 g8
  fis g16 e e d d cis
  d b' a4 

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Aria Cantabile"
  \time 3/4
  r4 d4 c
  b a g
  r4 a b
  a4. b16 c b4
  r4 d4 d, e fis g r4

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "3. Allegro Gratioso"
  \time 3/8
  a4 g16 a32 b
  a8 cis, d
  e \times 2/3 {e16[ fis g]} \times 2/3 {fis16[ g a]}
  \grace a8 g4 fis8
  b cis, d
  a' cis, d
  e16 fis g a b d,
  \grace d8 cis4 r8

}


