\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "Voice and Violin 1"
  \time 4/4
  c2 \grace e8 d4 c8 b
  e2 \grace g8 f4 e8 d
  g4 g2 bes,4 
  bes a
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "Oboe"
  \time 4/4
   \set Score.skipBars = ##t
   R1*6 
    r2 r4 e
    f2 e4 r
    R1
    d1~ d~ d~
    d8 fis g d c b a g g2
}
