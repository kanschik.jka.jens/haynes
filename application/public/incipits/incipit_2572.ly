 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
  d1 g,2 r8 g' f es
  d fis, g4. g'8 f es 
  d
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Alla breve"
  \time 4/4
  g2 bes a d,
  es'2. d4 c bes a c bes g g'2~ g fis
  g4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Adagio"
  \time 3/4
  b2. c
  d4 g8 f es d es2.
  as4 g4. as8
  f4 d es
  es8 f d4. c8 c2.
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Allegro"
  \time 2/4
  \partial 8
    bes'8
    \times 2/3 {a8[ g a]} a8 d,
    g4 r8 g
    \times 2/3 {f8[ es f]} f8 bes,
    es4 r8
}
