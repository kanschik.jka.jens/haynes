\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Grave. Adagio"
  \time 4/4
  r16 g b16. c32 d16. d32 e16. fis32 g16. fis32 g8~ g16 b, a16. g32
  e'16. e32 fis16. g32 c,8. b32 c b16
}
