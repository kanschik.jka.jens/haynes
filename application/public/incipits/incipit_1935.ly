\version "2.16.1"
         #(set-global-staff-size 14)
         
         \relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
  \set Timing.measurePosition = #(ly:make-moment -9/16)
  c16 g'8. f16 es8. d16
  c2. b4 c4. d8 es4
}
