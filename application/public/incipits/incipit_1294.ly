\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8
  a8
  d d4 cis8 d8[ cis16 b]
}
