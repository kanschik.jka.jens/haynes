\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
  d'16. a32 a4.
  \grace b32 a16. g32 g4.
  fis16[ a] \grace g32 fis16 e32 d e16[ g] \grace fis32 e16 d32 cis
  d8 e4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio"
  \time 4/4
  r8 a' bes a gis4 g~
  g8 g a g fis4 f~
  f8 f g8. a32 bes \grace d,8 cis4 d
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 3/4
  fis8 a16 g fis8 e d cis
  d4. cis8 d e
  fis a16 g fis8 e d cis d4.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8 d8
  g  g, \times 2/3 { g16[ b b] } \times 2/3 { d16[ b g] }
  e'8 g,~
  \times 2/3 { g16[ e' fis] } \times 2/3 { g16[ fis e] }
  d8 g,
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Andante"
  \time 2/4
  \partial 8
  a'8
  a16 fis fis4 a8
  a16 e e4 \times 2/3 { a16[ e cis] }
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
  \partial 4
  g4
  d'~ \times 2/3 { d8 g d]} c4
  \grace c8 b4~ \times 2/3 { b8[ g' b,] } a4
  \grace a8 g4.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}



\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 16
  d16
  a'4 g \times 2/3 {fis16[ d d'] }d4 a8
  \times 2/3 { b16[ g d']} d4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 3/4
  g'8. b32 a g8 g g g
  d'16 e d cis d2
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Con Spirito"
  \time 2/2
  fis8 g a4 a a
  a1
  fis8 g a4 a a
  a2. g4 fis d b' d,
  a' d, g2
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 16
  d16
  g, g' fis e  d16. e32 fis,16. a32
  \grace a16 g16. fis32 g8 r16
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 4/4
  \partial 8
  a8
  fis'8. g32 a
  cis,8. d32 e
  d8 a r d
  \grace e16 d16 cis d b8 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/8
\partial 16
d16
g d c b a g
\times 2/3 {fis16[ g a]} g8. d'16
g d c b a g
\times 2/3 {a16[ b c]} b8
  
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
  a'8
  \grace g16 \times 2/3 { fis16[ e d] } d4 a'8
  \grace d,16 \times 2/3 { cis16[ b a] } a4 a'8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio"
  \time 4/4
  \partial 8
  a8
  d \grace g16 f16 e d8 d~
  d16 a' \grace g16 f16 e d8 d
  \grace e16 d16 cis d8 r16
}
  
  \relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 3/8
  \times 2/3 {a'16[ g fis]} \grace e16 d8 cis
  \grace e16 \times 2/3 {d16[ cis b] } a4
}
  
\pageBreak



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 4/4
  \partial 8
  g8
  c16 e d c g'8 g  \grace a32 g16[ f32 e]  
  \grace g32 f16  e32 d c8 a'
  g16 c c b32 a g16 f32 e f16 f f8 e r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  g8
  c16 g e'4 f16 d
  e c g'8 g4
  f16 e d c a'8 a a g r
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Larghetto"
  \time 3/4
  es8. c16 c4 b
  c8. es16 g2
  as16 f8. g16 es8. f16 d8.
  es8. d16 c4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 3/8
  c8 e16 d e c
  a'8 a4 g16 f e8 f
  e16 d c8 g
}
