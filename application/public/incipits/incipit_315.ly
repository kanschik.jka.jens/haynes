\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 3/4
  d2. \grace fis8 \times 2/3 { e8[ d c]} b4 \grace b8 a4
  \times 2/3 { b8[ c d] } d2
  \times 2/3 { e8[ d c]} b4 \grace b8 a4
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegretto"
  \time 3/8
  d4 e8 \grace fis8 e d g
  \grace a8 g8 fis c
  \grace d8 c8 b e
  \grace g,8 fis4 g8
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Tempo di Menuette"
  \time 3/4
  g2 \times 2/3 {b8[ a g]} a4 d, \times 2/3 {c'8[ b a]}
  b4 g \times 2/3 { d'8[ c b] } \grace d8 c2 b4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 2/4
   \partial 8
   d8
    g8 g,4 fis8 g bes4 a8
    bes d4 c8 bes g'4 fis8 g
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 3/4
  g4 a bes c d es
  \grace f8 es4 d2~
  d8 c d es d c
  \times 2/3 { bes8[ a g] } fis4 g
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegretto"
  \time 3/8
  g8 g g g es' d
  cis d r
  c bes a \times 2/3 {bes16[ a g]} g4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
   g'8
   g c,4 b8
   c16 d es4 d8 es16 f g4 as8
   \grace as8 g f16 es \grace g8 f es16 d
}
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio"
  \time 2/4
  \partial 8
  es,8 bes' bes es, \grace g8 f
  g16 bes bes4 es8
  es32 d f f f es g g  \times 2/3 { f16[ g as] } g8
  \grace as8 g8 f r
}
  
  \relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Tempo di Menuette"
  \time 3/4
  c4 \grace d8 c2
  \times 2/3 {b8[ d c]}  \times 2/3 {b8[ a g]} \grace a8 g4
  es g8 f es4 d r r
  }
  \pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegretto"
  \time 2/4
  c8 g'4 f8
  e c4 b8
  c[ g a a] \grace b8 a8 g r 
}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio"
  \time 3/4
  c4 es \grace es8 d4
  \grace d8 \times 2/3 { c8[ b c] } \grace a8 g2
  as'4 as8 g g f \times 2/3 {es8[ d c] } c4 r
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Tempo di Menuette"
  \time 3/4
  \times 2/3 { g'8[ f e]}  e4  \times 2/3 {e8[ f g]}
  \grace g8 f2 e4
  c8 e d f e g
  \grace c,8 b2 c4
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegretto"
  \time 3/8
  r8 bes d
  \grace es8 d c es
  \grace f8 es d f
  \grace f8 g f es
  \grace es8 d8 r
}
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Molto Andante"
  \time 2/2
  es4 es,8. es16 f4 \times 2/3 {f8[ g as]} 
  \grace bes8 as4 g
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. tempo di Menuette"
  \time 3/4
  bes2. c2 c4 d es
  \grace f8 es4 d
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 3/4
  e2. \grace g8 fis4 e dis
  e8 c c4 b
  fis'8 c b a g fis g8. dis16 \grace dis8 e2
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 3/4
  e,4 e'4 e
  \grace fis8 e4 dis r
  d d d \grace e8 d4 cis r
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Andantino"
  \time 3/4
  e2. \grace e8 dis4 e r
  a2. \grace a8 g8. fis16 \grace fis8 e2
}
