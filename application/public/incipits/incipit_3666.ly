 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro [Tutti]"
  \time 2/4
  a'4 gis8. fis16
  e8 a, a, fis''
  e4 d
  cis8 e, a, fis'' e4 d
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*26
     e8 b'16 a gis8 fis
     e16 fis gis fis e8 d
     cis cis'16 b a8 gis fis16 gis a gis fis8 e

}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Andante"
  \time 3/4
  e4. d8[ c b]
  \grace b8 a8 gis a2
  b16 c8. d16 c8. b16 a8.
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "3. Tempo di menuet"
  \time 3/4
    e2.
    \grace a,4 gis2.
    a4 e fis
    \grace e4 d2. cis
}