\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/4
    g4 \grace b8 a8 g
    c4 b8 r
    e4 d8 fis16 g
    d8 c b16 d, e fis
}
