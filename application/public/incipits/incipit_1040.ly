\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4
      c8 c32 es d c b8 c g'4 r8 g~
      g f32 as g f es8 d c4 r8
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro"
  \time 2/2
 \partial 4
   g'4
   g c, r c
   b8. c16 d4 r g
    \grace as16 g4 f r f
    es8. f16 g4 r
  }

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Menuetto"
  \time 3/4
  c4 c c
  c8 d d2
  d4 g f
  es d c
  }