 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key es\major
   \tempo "Adagio"
  \time 3/8
  r8 es4~ es8 d c16. bes32
  bes4.
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Zenobia"
  \key es\major
   \tempo ""
  \time 3/8
     \set Score.skipBars = ##t
     R4.*10
     r8 bes g es4 es8 as4 as8
     as g r
}