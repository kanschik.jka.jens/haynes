 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/2
    bes1~ bes8[ c16 g] a8. bes16 bes4 f'
    f8[ g16 a32 bes a g f es d es] es2~
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegro"
  \time 2/2
  \partial 8
  bes8 bes d16 bes f'8 a16 f bes8 bes, r16 f' g a
  bes a g f es d c bes a8 f r16
}

\relative c'' {
  \clef bass
  \key g\minor	
   \tempo "3. Adagio"
  \time 2/2
  g,4 bes8. a16 g4 d
  a' c8. bes16 a4 d, bes' d8. c16 bes8 fis g c,
  d4 d, g
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Allegro"
  \time 2/4
  \partial 8
  bes8
  bes16 d c bes  f' a g f
  bes f d bes g' es c a
  f' d bes g es' c a f
  d'8 c16 d bes8
}