 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Arioso [Oboe]"
  \time 4/4
    g'4~ g16 bes a g fis a c,8~ c es d c
    b d f8~ f16 as g f  e g bes,8~ bes16 d c bes
    a c es8
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "1. Arioso [Tochter Zion]"
  \time 4/4
       \set Score.skipBars = ##t
   R1*4
  r2 d4. es8
  d c bes a bes g r4

}
