\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Allegro"
  \time 2/4
        r8 fis16 e d cis b ais
        b8 ais b ais
        b8 fis'16 e d cis b ais
        b8 ais b ais
}


\relative c'' {
  \clef treble
  \key fis\minor
   \tempo "2. Largo"
  \time 4/4
      r8 cis16 d cis8 cis cis cis16 d cis b a gis
      fis8
}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Allegro"
  \time 2/4
      b'4 fis8 g
      a4 e8 fis g4 d8 e fis e16 d  cis d e cis d8
}
