\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro con Spirito"
  \time 2/2
   e2~  e8 g f e
   \grace e8 d4 d4. e16 f e8 d
   d c c c d c b c
}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio"
  \time 2/4
  R2
  r16 as'16 g fis  g as f d
  g16. es32 c8 r4
  r16 as'16 g fis  g as f d
   c4
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Allegretto"
  \time 6/8
  \partial 4.
  e8 f fis
  g e c \grace d16 c8 b c
  cis4 d8 d dis e
  f d c
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro con Spirito"
  \time 2/2
  c4 a8. bes16 c4 c
  c4. d16 e f8. e16 g8. f16
  a2 g8 f e d
  c d a bes bes4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Corrente. Allegro"
  \time 3/8
  c8 \grace { d16[ c] } g16 e g c
  b32 gis b a a16 r16 a16. g'32
  f8
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo. Allegretto"
  \time 2/4
\partial 8
c8
a'4 g8 f
f4 e8 c bes'4 a8 g
g4 f8
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro poco moderato"
  \time 2/2
  bes2 es8. d16 g8. f16
  f8 e es2 d4
  c16 d c bes  c d es c  g8. bes16 a8. c16
  bes4. d16 bes f4
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Pastorella. Larghetto"
  \time 6/8
  \partial 4
  d8 d
  d8. es16 d8 c bes a
  g4.~ g8 a[ bes]
  b c d es c a
  g4. fis8
}
  
  \relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro ma non troppo"
  \time 2/4
  \partial 8
  g'16 f
  f d c bes  es c bes a
  bes d f bes  bes a g f
  f d c bes es c bes a
  bes d f a bes8
  }
 
