\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
    bes'16 g es8 r16 es es es  es d d8 r4
    as'16 f d8 r16 d d  d   d es es8 r4
    c'16 as es8 r16 es as c  bes g es8 r16 es g bes
    \grace bes8 as8. g16 \grace g8 f8. es16
    \grace es8 d4 r
}
