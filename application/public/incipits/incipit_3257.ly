 
\version "2.16.1"
 
#(set-global-staff-size 14)
 \layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran) - Herr, deine Güte reicht soweit der Himmel ist [BWV 17]" 
    }
    \vspace #1.5
}
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe I"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key e\major
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                  e8 e,16 fis gis a b e, cis' e, fis gis a b cis a
                  fis dis' e fis e dis cis b e8. fis32 gis fis16 e dis cis
                  dis8 fis b,4~ b8 gis' b,4
                   
               
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe II"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key e\major
                        % Voice 2
                   \set Score.skipBars = ##t
                   R1*2 b8 b,16 cis dis e fis b, gis' b, cis dis e fis gis e
                   
                        
                  }
                  
              
>>
}


\relative c''' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key e\major
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
     R1*10 b8 b,16 cis dis e fis8 gis a16 fis gis8 cis,
     ais' b16 cis b ais gis fis b8 dis cis b
     a cis fis,4~fis8
  
}

