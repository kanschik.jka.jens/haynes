\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 3/4
    g'8. f16 e4 e
    f8. g16 a4 r
    f8. e16 d4 d
    e8. f16 g4 r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Gavotte"
  \time 2/4
  \partial 8
  c8
  f c16 f a8 f16 a
  c4 bes8 a
  g16 fis g a g8 f
  e16 f g f e
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo"
  \time 4/4
     \times 4/6 { g'4 a g f e d}
     c e d f 
     e a fis2 g r
}
