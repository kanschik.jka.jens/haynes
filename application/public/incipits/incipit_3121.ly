 
\version "2.16.1"
 
#(set-global-staff-size 14)
 \layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Soprano) - Liebster Gott, erbarme dich [BWV 179/5]" 
    }
    \vspace #1.5
}
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe da Caccia I"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key a\minor
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                     r8 c8 b a gis a
                     d2.~
                     ~d8 c b a gis a 
                     fis2.~
                     ~fis8
               
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe da Caccia II"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key a\minor
                        % Voice 2
                      R2. r8 c'8 b a gis a
                      e2.~
                      ~e8 e d c b c16 d
                      ais2.
                        
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key a\minor
   \tempo "o. Bez."
  \time 3/4
     \set Score.skipBars = ##t
     R2.*12  r4 e8 d16 c b8 a
     f'4 r4 r4 r8 e8 d c b c
     c

}
