 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. [Allegro]"
  \time 4/4
    es4 d es16 bes c d es d es f
    g8 es f d es16 bes c d es f g as
    bes8 es, f d es g4 f16 es
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Siciliano - Larghetto"
  \time 12/8
  c4 c8 c8. d16 es8 d4 g,8 r r g'
  g8. f16 es8 f4 f8 f4 es8 r r
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Allegro"
  \time 3/8
  r8 c16 d es d
  c8 es, g c, c'16 d es d
  c8 es, g c,
}
