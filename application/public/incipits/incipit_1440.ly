 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 3/8
  <a e'>16 a32 a a16 a a a 
  <d, a' e'>8 r r
}
