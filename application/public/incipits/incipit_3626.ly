 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. o.Bez."
  \time 4/4
    g'1 g2 g8. f16 f4
    f4. es8 es2
    es4. d8 d4 r8
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. o.Bez."
  \time 4/4
  g'2 c,4 r8 g
  c c16 es d8 d16 f es8 c r g'
  c,16 b c es  d c d f  es8 c r
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Adagio"
  \time 4/4
  r2 c2
  c8. g16 g8. es'16 es8. d16 d8. c16
  bes8 a g4 r2
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Menuett"
  \time 3/4
   g'4 as f g2 r4
   es8 d f es d c
   b c d4 g,
  
}