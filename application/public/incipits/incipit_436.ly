
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite I"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
  \tempo "1. Prelude"
  \time 2/2
  \partial 8*5
   g'8 e4 d8 c
   d4. d8 d4 e8 f
   e4 \grace d8 c4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite II"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
  \tempo "1. Allemande"
  \time 4/4
  \partial 8
  r16 c
  c8. d16 e f d f  e8. f16 g c, b d
  c f e d d8. c16 c8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite III"
    }
    \vspace #1.5
 }
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prelude"
  \time 3/4
  g'4 fis8.[ e16 d8. c16]
  b4 a8.[ g'16 g8. fis16]
  g8.[ b,16 c8. b16 a8. c16]
  b8.
}
 
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV"
    }
    \vspace #1.5
 }
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Modérément"
  \time 4/4
  \partial 8
  g8
  c e16 d c8 e d16 c b a g4
  r2 r4 r8 g'8
  g f16 e d8 c b c d4
}
 
 
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite V"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
  \tempo "1. Adagio"
  \time 4/4
  \partial 8
  d8
  g b16 a g8 d e c c e
  d c16 b a8 d b g g b16 c
  d8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VI"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
  \tempo "1. Gaiment"
  \time 2/4
  \partial 8
  c8
  f f f g16 f
  e8 e e f16 e
  d8 e16 f g8 g
  c,
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonate I"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
  \tempo "1. Allemande"
  \time 4/4
  \partial 8
  r16 g'
  g4 r16 c b a g8 f16 e d8 c
  b8. a16 g8 g' a, f' g, f'
  e
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
  \tempo "1. Modérément"
  \time 3/4
  \partial 4
  d4
  \grace c8 b4 \grace a8 g4 g'8 fis
  e4 d g8 fis
  e4 d8 c b c
  d4
}
