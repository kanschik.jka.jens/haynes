\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante comodo"
  \time 2/2
  \partial 4
  g4
  c c c16 e8. g16 e8.
  \grace d8 c16 b8. c4 c'4. c,8
  d4 e8. f16 e4 d
}

