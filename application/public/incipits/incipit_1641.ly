\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
    f,4 r8 f16 g a8 a16 bes c8 d16 e
    f4 r r2
    r r4 c
    f8 g a bes c4 r
    f,8 g a bes c4 r
    f,8 g a bes c4 d
    c2 bes4 r
}
