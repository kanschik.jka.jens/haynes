 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef alto
  \key es\major
   \tempo "1. Allegro moderato"
  \time 2/4
  \partial 8
  bes8
  \times 2/3 { bes16[ as g]} g8 r16 g as bes
  \grace bes16 \times 2/3 { as16[ g f]} f8 r as
  \grace bes16 \times 2/3 { as16[ g f]} f8
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Romance"
  \time 2/2
  \partial 2
  g4 g
  \grace f8 es4 es \grace  d8 c4 \grace bes8 as4
  g bes bes bes
  \grace es8 d8 c d es \grace  g8 f4 f
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Presto"
  \time 2/4
  \partial 8
  bes8
  es f g f es d es bes
  es f g f es4 r8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}