\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}




\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Maestoso"
  \time 4/4
  bes4 bes8. bes16 bes4 bes
  f'1~ f~ f2 fis
  g a bes bes,4 bes
}

\relative c'' {
  \clef bass
  \key g\minor
   \tempo "2. Adagio"
  \time 3/4
    \partial 4
    g,8. a16
    bes2 bes8. c16
    d2. d4. c16 bes a8. c16
    bes4. a16 g fis8. a16
    g8 f es2
}

\relative c'' {
  \clef alto
  \key bes\major
  \tempo "3. Allegro"
  \time 2/4
  \partial 8
  bes16 a
  bes8 bes, bes bes 
  bes c'16 bes c8 a
  f g f es d r r
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto VI " 
    }
    \vspace #1.5
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 4/4
  \partial 8
  g8
  g4. a16 b c4 c
  c4. e16 d c4 r8 g
  g4. c16 d e4 e
  e4. g16 f e4 r
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio"
  \time 2/4
  g4 as g~ \times 2/3 { g16[ g' f]} \times 2/3 { es16[ d c]}
  c16 b b4 d8 d32 es b c c4 es8
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto"
  \time 2/2
  c,1 c' d, d' e, g' f, a'
}

