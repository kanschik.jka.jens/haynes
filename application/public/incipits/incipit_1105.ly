 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. [Allegro]"
  \time 2/4
  \partial 8
  bes8
  es es es16 f g as
  bes8 bes4 as8
  g16 f es8 f16 g as8 g f r
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Largo"
  \time 3/4
  g4 c8. d32 es d8 c
  c4 b c
  d8. es32 f es4 d
  es8. d16 c4 r
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Allegro"
  \time 3/8
  es8 bes bes
  bes c16 d es f
  g8 as g g f r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}