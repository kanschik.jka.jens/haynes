\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
     c,4 e8. g16 c4 c' 
     b r8 e,
     a d, g c, f4 r16 b, c d
     e8 a, d g, c4
} 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Largo [Soprano]"
  \time 3/4
     r4 c4. g8 a4 a c8. a16
     \grace a8 g2 e'4
     d16 c8. b16 a8. g16 f8.
     \grace g8 f4 e r
 } 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Largo [Oboe]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*4 
    r4 r8 e c g
    a8. g16 a8 f' d a
    b8. a16 b8 g' e b
    c4
 } 


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Aria. Andante"
  \time 4/4
      bes8 f4. f'32 d16. es32 c16.  d32 bes16. c32 a16.
     \grace c16 bes32 a16. bes4 f8
      \override TupletBracket #'stencil = ##f
      \times 2/3 { d'16 es f} es8 es d
      \grace es16 d8 c r
} 
