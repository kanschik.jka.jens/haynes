
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "1r. age"
    }
    \vspace #1.5
}
 
\relative c {
\clef "bass"   
  \key d\major
   \tempo "1. Ouverture"
  \time 2/2
\partial 8
r8 d2 d8 e fis g
a4 a8 b cis d e cis
d e d cis b a g b
a cis, cis d e4 a,
 
}



 

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "2e. age "
    }
    \vspace #1.5
}
 
\relative c {
  \clef bass
  \key f\major
   \tempo "1. Entreé du pedant"
  \time 2/2
f2 bes
f2. f4
bes,2 a4 g
f f' d2
g4 g d e 
f2 e
f g c, c,


 
}





\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "3e. age"
    }
    \vspace #1.5
}
 
\relative c {
  \clef bass
  \key d\major
   \tempo "1. Vite"
  \time 3/8
\partial 8
r8 d4 r8
cis4 r8 b8 gis4
a a'8
cis16 cis cis cis cis cis
d4

}





\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "4e age"
    }
    \vspace #1.5
}
 
\relative c' {
  \clef bass
  \key a\minor
   \tempo "1. Les Vieillards decrepits"
  \time 2/2
f4 f f f 
d d d d 
d d cis cis
d a a a
a a a a 
a a a d
d c c c 


}

