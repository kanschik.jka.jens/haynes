 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro moderato"
  \time 4/4
    d8. g16 fis e d c \grace c16 b8 a16 g g'8. b,16
    b a e' d \grace d8 c4 \grace c8 b4 f'4~
    f8 e4
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. Andante"
  \time 3/8
  b8[ g'] fis32 e dis e
  e8 dis r
  b[ g'] fis32 e dis e
  e8 dis r
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Allegro"
  \time 3/4
  r4 d d
  \grace c4 b4. c8 d e
  d g d4 c
  \grace c4 b4.
}
