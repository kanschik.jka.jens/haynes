\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 2/2
  f4. g16 f f4. g16 f
  f8[ e] f g16 f e4 r8 f16 e
  d[ c d e]  f e f g
}

\relative c'' {
  \clef bass
  \key f\major
   \tempo "2. Vivace [Viola da Gamba]"
    \time 3/4
   f,,4 a a8 bes
   c4 bes8 a d4
   c bes8 a d8 c8 bes[ a bes c a]
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Vivace [Oboe]"
    \time 3/4
   \set Score.skipBars = ##t
   R2.*12
   r8 f, a c f4 
   d c8 d c bes a f a c f4
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Largo"
  \time 4/4
  r4 a d
  cis2 d4 
  e4. a8 g f
  f4 d8 c bes a
  bes4
}
\relative c'' {
  \clef bass
  \key f\major
   \tempo "4. Allegro [Viola da Gamba]"
    \time 2/2
    a,16 [ g a bes] a bes a bes c8 a a c
    d16[ c bes c] d c d bes  c[ bes a bes] c bes c a
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro [Oboe]"
    \time 4/4
   \set Score.skipBars = ##t
   R1*4
   r16 f, g a bes[ c d e] f8 c f4
   d8 d c c16 bes a8 f r4
}