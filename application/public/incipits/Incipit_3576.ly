 \version "2.16.1"
         #(set-global-staff-size 14)
         
 \relative c'' {
  \clef treble
  \key bes \major
   \tempo "1. Allegro Molto [Tutti]"
  \time 2/4
  \partial 8 bes32 a bes16 d8-. bes-. a'-. d,-. bes'4 bes, c8 (bes) es (d) c bes r bes32 a bes16
 }
 
 \relative c'' {
  \clef treble
  \key bes \major
   \tempo "1. Allegro Molto [Solo]"
  \time 2/4
  \partial 8 bes32 (a bes16) d8 bes f' d bes'4 bes, \appoggiatura {d8} c8 bes \appoggiatura {f'} es d \appoggiatura {d} c bes r4 r2
 }
 
 \relative c'' {
  \clef treble
  \key f \major
   \tempo "2. Andante [Tutti]"
  \time 2/4 
  f4. a,8 a\prall g r4 g'4. bes,8 bes16\prall a a8-. r4 f32 (a) g (bes) a (c) bes (d) 
 }
 
 
 \relative c'' {
  \clef treble
  \key f \major
   \tempo "2. Andante [Solo]"
  \time 2/4 
  \grace {e16 f g} f4~ f32 a, (c) f (a) c, (bes a) a16\prall g g8 r4 \grace { fis'16 g a} g4 e,32 d c d e f g a 
 }
 
  \relative c'' {
  \clef treble
  \key bes \major
   \tempo "3. Allegro assai "
  \time 2/2
  bes2 f'8 (es d c) bes2 f'8 (es d c) bes4 g' f a bes2 f8 (es d c)
  }