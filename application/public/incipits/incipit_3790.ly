 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 4
  g4
  c8 g f e  c' g f e
  \times 2/3 { f8[ g a]} g2 c4
  d,2 \grace { e16[ f] } e4 d
  \times 2/3 { e8[ f g]} g2
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Adagio"
  \time 3/4
  c4 g8 g' \grace f16 es8 \grace d16 c8
  \grace c8 b8. c16 d4 r16
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Allegro"
  \time 3/8
  r8 g' c,~
  c b16 a b8
  c4. d
  e16 d e8 a~ a g r
}
