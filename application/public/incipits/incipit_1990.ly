\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 4/4
  f4 f f r8 f
  g f g f r4 r8 f
  g f g a16 bes a8 g16 f f4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 4/4
   d16 c bes c  d c bes es  f8 f f g16 f
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 3/2
   r2 r g' a2. bes4 c a d2 g,
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 3/4
  r8 d16 es f8 d g f16 es 
  f8 d16 es f8 d
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Vivace"
  \time 3/4
  \partial 4
  d4
  g d b8 c
  d4 b g
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Grave"
  \time 4/4
    d4. d8 d8. c16 c8. bes16
    bes4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 6/8
   g4. a
   b8 a g  b a g
   d'4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Vivace"
  \time 3/4
  r4 f f
  f2 f4
  r4 g f g2 g4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Largo"
  \time 4/4
   d4. g16 f  es8 d c16 es d c
   bes8 g 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 4/4	
   d8 f c a  bes d c a
}



  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 4/4
  c'4 r8 c d d c8. bes16
  a8 a16 bes c8 a d c c8. bes16
  a4 r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 4/4
   c8 d c d  c bes a bes
   c d c d c4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Largo"
  \time 3/4
   a'2.
   r4 f8 e f g e e f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 12/8
  a'8 bes c  c bes a  g a bes  bes a g
  a g f
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Largo"
  \time 4/4
  c8 e16. f32 g8 b, c4 r8 d
  e4 r r2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4
 \partial 16
   g'16
   g4 r16 g a b
   c8 g g16 g a b
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Largo"
  \time 3/2
   e2 c a
   f'4. e8  f4 a g f e2.
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 12/8
  e8 f e  f g f  g4. r4 f8
  g a g
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 4/4
  c'4~ c16 bes a bes  g a g a bes c bes c
  a8 f c'4 r8 d, a'4
  r8 bes, f'4 r8 f e8. f16
  f4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 4/4
   c16 d c bes  a8 c d4 r8 d
   c16 d c bes
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio"
  \time 3/4
   a'4 g f  e2 f4
   g g8 f f e
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 4/4
  c'4 c \times 2/3 { c8 bes a}  
  \times 2/3 { c8 bes a} 
  \times 2/3 { d8 c bes } 
  \times 2/3 { d8 c bes}   
}
