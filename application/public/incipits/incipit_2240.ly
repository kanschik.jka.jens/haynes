 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 3/4
  \partial 4
  c4
  g' g8 a g f
  e d c4 r
}
 
