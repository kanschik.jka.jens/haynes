
\version "2.16.1"

#(set-global-staff-size 14)

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Fugue. Gay"
  \time 2/2
  r2 g2 d2. d4 e e e8 fis g e
  fis4 d8 e fis g a fis
  b4 b g b
  e, a fis a
  d, b'2 b4 a2
  
  
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Lentement"
  \time 3/4
  g2 r4
  fis2 r4 e2 r4
  dis2 r4
  e2.~
  e8 dis e g fis a
  g fis g fis g fis
  e2 r4
  
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Vivement"
  \time 3/4
  \partial 4
  r4 r2.
  r4 r4 d4
  c8 b a g fis e
  d4 g8 a b4
  a2 d4~ 
  d c c
  c4. b8 a c
  b a b c b c

  
}




