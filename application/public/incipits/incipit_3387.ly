   \version "2.16.1"
       #(set-global-staff-size 14)

\relative c'' {
  \clef treble   
  \key c\major
   \time 4/4
   e4 a, gis e a b c 
}