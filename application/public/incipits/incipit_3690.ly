\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. [Allegro]"
  \time 2/2
  g4 d'2 e16 c8.
  d4 b a g
  g'16 d8. b2 e16 c8.
  d4 b a g
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [Tutti]"
  \time 2/2
  d4. es16 d d8 c16 bes c8[ bes16 a] 
  bes8 g r g' f es16 d es[ g f es]
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   f4. g16 f f8 es16 d es8[ d16 c]
   d8 bes r g' g4 g16 es bes g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 3/4
  g'4 g g
  g g g
  g fis8 e d c
  b2 a4
  g8 d d g g b
}