\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Mache dich, mein Herze, rein [BWV 244/75]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe da Caccia 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key bes\major
                      \tempo "o. Bez."
                      \time 12/8
                      % Voice 1
              r4 r8 f4 es8 d4 es8 f g a
              bes4.~ bes8 d, es a bes es,~ es c d
              a' bes d,~ d bes c a' bes c,~ c bes es
              d4 c8
                 
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe da Caccia 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key bes\major
                     \set Score.skipBars = ##t
                        % Voice 2
              r4 r8 d4 c8 bes4 c8 d es c
              d4.~ d8 f bes es,4.~ es8 a bes
              d,4.~ d8 f a c4 a8 bes4 bes,8
              bes4 a8
                      
                  }
                  

                  
>>
}


\relative c {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key bes\major
   \tempo "o. Bez."
  \time 12/8
     \set Score.skipBars = ##t
  R1.*8 r4 r8 f4 es8 d4 es8 f g a
  bes1.~ bes4. r4 r8
  
   

}