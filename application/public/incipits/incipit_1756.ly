\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Aria o.Bez."
  \time 3/8
    b16 c d8 d
    d4 r8
    c16 d e8 fis
    g fis e
    d e16 d c b
    a8 a b c4 r8
}
