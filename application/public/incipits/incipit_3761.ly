\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante"
  \time 6/8
  f8. g16 g8 d r es
  c r d bes r r 
  r g' g g a bes
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro assai"
  \time 4/4
  d4 es r  c
  d es r c
  d4. es8  f g a bes
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Grazioso"
  \time 3/4
  f4. fis8 g es
  d4 c d
  es4. f16 g f8 es es d d4
}
