 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro assai"
  \time 3/8
  g4 b8
  g d b'
  g4 d'8
  b g g'
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Andante"
  \time 2/2
  \partial 8

}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Allegro"
  \time 3/4
  \partial 4
  
}