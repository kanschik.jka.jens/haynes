\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.bez."
  \time 2/2
  \partial 2
  r4 b8 d
  c4 b a8. g16 a4
  b g r d'8 f
  e4 c
}
