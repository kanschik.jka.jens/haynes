\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Suite " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prélude. Lentement"
  \time 3/2
  \partial 2
  c2
  \grace d8 e2. f4 g2
  e \grace d8 c2
  g'2
  a2. g4 f2
  g \grace f8 e2
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxiéme Suite " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Premiere Ariette"
  \time 3/4
  \partial 4
  g8 f 
  e d c4 e
  d g, c
  d8 c d e f d e d c4 
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisiéme Suite" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prélude Tendrement"
  \time 3/4
  g'4. g8 d4 e4. fis8 g4
  d c4. d8 b4. a8 g4
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatriéme Suite " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. 1ere Gavotte"
  \time 2/2
  \partial 2
  e8 d e d
  c4 g' f8 e d e
  e4 d g8 c, b d c d e f d2
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquiéme Suite " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Modérément"
  \time 2/2
  \partial 2
  g4 b8 a c b e4 d g
  fis4. e8 d c b c d4 e8 d c4. b8 a2
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixiéme Suite " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Pastorelle"
  \time 3/4
  g'2 f4 e8 c a' g c4
  g8 f e f g d
  e4 d8 e c4
}
