\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. o.Bez."
  \time 3/4
  r8 e16 fis g8 e fis dis
  e b4 cis16 dis e8 dis
  e b4 cis16 dis e8 dis
  e dis16 cis b8 a g fis
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Siciliana"
  \time 6/8
    c8. d16 c8  \grace { e16 f} g4 c,8
    b8. a16 g8 g8. b16 d e
    fis8. g16 fis8  g, a' g f e d
    e8. d16 c8 c4
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. o.Bez."
  \time 2/4
      e4 g fis8 b, r fis'16 g
      a8 fis g e a fis g e
      b' a16 g fis8 e
      dis16 b a b  e b a b 
}
