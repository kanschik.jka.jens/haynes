\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
  \set Staff.instrumentName = #"Oboe"
   \tempo "o.Bez."
  \time 2/2
    g2 g4 b
    d c  e4. d8
    g4 r d2~ d d4 b'
    fis2
}

\relative c'' {
  \clef treble
  \key g\major
  \set Staff.instrumentName = #"Soprano"
   \tempo ""
  \time 2/2
    r2 g4 b
    d c e4. d8
    g4 r d8 c a c
    c b d g, fis4 g 
    \grace b8 a2 r
}

