\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Ouverture"
  \time 2/4
    fis,4 fis
    fis8 d'16 e fis8 e
    fis,4 fis
    fis8 fis'16 g a8 g
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Siciliano"
  \time 6/8
  \partial 8
  a8
  fis8. g16 a8 a8. g16 fis8
  fis4 e8 e4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Paysane"
  \time 2/4
  d4 fis16 e d cis
  d8 a a4
  fis4 a16 g fis e
  fis8 d d4
  }
  
\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Menuet"
  \time 3/4
  d8 e fis4 d
  a d2
  e8 fis g4 e
  a, e'2
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Rigaudon"
  \time 2/2
  \partial 4
  a4
  d2 e
  cis4 b8 cis a4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "6. Harlequinade"
  \time 3/8
  fis16 a d,8 d
  fis16 a d,8 d
  b' a g
  fis e
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Ouverture"
  \time 4/4
  
}
