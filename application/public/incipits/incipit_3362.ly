 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "o.Bez."
  \time 2/4
    f,8 bes4 d8
    c16 a bes8 bes16 d d f
    es d32 es d8 r4
}
