\version "2.16.1"
         #(set-global-staff-size 14)
         
         \relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo [Oboe]"
  \time 3/4
   g'4 bes a
   g8 r r4 r
   a c bes
}

         \relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo [Sopran]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*15
   r4 r r8 d8
   g,4 bes4. a8 g g r4 r8 g
   a4 c4.
}
