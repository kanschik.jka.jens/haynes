 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. "
  \time 2/4
    g8 d4 g8  b g4 b8
    d8 g4 d8
    \grace d8 e2
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Arioso. Andante"
  \time 2/4
 
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro non tanto"
  \time 3/8
  
}
 