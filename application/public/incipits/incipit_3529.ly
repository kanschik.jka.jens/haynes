\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key f\major
    \time 4/4
    \tempo "1. o.Bez."
      \partial 4
      c4
      f8. c16 c2 e16 g e g
      f8. c16 c2 bes'16 g e g
      f4~ f16 e a g f8 e d c
      bes4.
}


\relative c'' {
  \clef treble  
  \key f\major
    \time 3/4
    \tempo "2. Tempo di Minuetto"
      c4 c c
      \grace d8 c8 bes a bes c f
      c4 c4. d8
      \grace c4 bes2 a4

}



