 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Thema. Andante"
  \time 2/4
  c8 c16 d e8 c
  a d4 c8 b b16 b c8 d
  \grace d16 g,4 r
}

