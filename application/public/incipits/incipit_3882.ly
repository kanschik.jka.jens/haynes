\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 16
  c16
  g'4 b,
  \grace b8 c4. c'16 a g e c4 c'16 a g e c4 c'16 a 
  g e d c d8. e32 f
  e4
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  g'8 g16. g32 g8 g,
  \grace d'8 c16 b c8~ c16  g' es c
  \grace es8 d16 c d8~ d16 as' g f
  \times 2/3 { es16 d c} c8 
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*8
   g'2 c,4 \fermata r8 g'
   c,8 c4 \times 2/3 { f16 d b } c8 c4 
   \times 2/3 { \grace g'8 f16 \grace es8 d16 \grace c8 b16 }
   \times 2/3 { \grace d8 c16 b c } g4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Tutti]"
  \time 3/8
  g'4 a8 g e r g4 a8 g e r g e r g e r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Solo]"
  \time 3/8
   \set Score.skipBars = ##t
   R4.*66
    g'4 a8 g c r
    e,4 f8 e c' r
    r g f e d c
    \grace {f16 g} a4. g4 r8
}