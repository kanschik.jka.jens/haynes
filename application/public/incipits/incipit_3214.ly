\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Qui sedes ad dextram Patris [BWV 232i/10]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key b\minor
   \tempo "o. Bez."
   \time 6/8
   \partial 16*3
   \set Score.skipBars = ##t
  b16 ais b
  fis4.~ fis8. cis'16 b cis
  fis,4.~ fis8. ais16 cis e
  d8 cis b~ b a g fis e r8
 
                                                                                   

}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key b\minor
   \tempo "o. Bez."
   \time 6/8
   \partial 16*3
   \set Score.skipBars = ##t
 r8. R2.*17 r4 r8 r8 r16 
  b16 ais b
  fis4.~ fis8. cis'16 b cis
  fis,4.~ fis8. ais16 cis e
  d8 cis b~ b a g fis e r8
  
       
}



