
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key d\major
  \tempo "1. Allegro "
  \time 2/4
 \partial 8
 a16 g fis g e fis d e cis e d8 a4 e'8 fis8 a,4 fis'8 g8 a,4 g'8 g16 a g a fis g fis g e4 r8

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Largo"
  \time 3/2
  \partial 4
 r8 fis8 fis 4 b b b, d fis cis2. cis4 d e d2 \grace cis8 b2 b'2~ b2 a1~ a4 b, dis fis g2~ g4
 
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 6/8
  \partial 8
 a8 fis' g fis a,4 g'8 fis4. e4 a8 b4 d,8 cis4 d8 e4 cis8 a4

 
 
}

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allegro "
  \time 2/4
 e4~ e16 b' a b e,8 e8~ e16 b' a b g8 fis16 g e b' a b e,8 e4 b8 c b a b g e r8
 
 

}

\relative c' {
  \clef treble
  \key e\minor
   \tempo "2. Adagio"
  \time 4/4
  e8. g16 b8. e16 fis,8. a16 c8. a16 fis8. c'16 b8. a16 g4 \grace fis8 e4
 
 
}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Allegro"
  \time 3/4
  \partial 4
 b4 e g b b a8 g fis e dis cis b4 e 
 dis8 cis b a g fis g fis e fis g a b2

}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 3/4
 g8[ d d d] d e16 fis g8[ d d d] d e16 fis g8 b, a g' a, fis' g4
 
 
 
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Affetuoso "
  \time 2/2
 \partial 2
 b4 c8 d \grace c8 b4 \grace a8 g4 g'8 d e fis g4 d b c8 d
 \grace c8 b4 \grace a8 g4 b8 c b cis d2  

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro "
  \time 3/8
 b16 c d e d e d c b c d8 e d c d16 c b c d8g16 fis e d c b a8 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Allegro "
  \time 4/4
 b4 fis b r8 fis'16 e d8 cis b ais b fis r8 fis'16 e d8 cis b a g fis e d e4
 

 
 
}
 
\relative c''' {
  \clef treble
  \key b\minor
   \tempo "2. Adagio "
  \time 3/4
  d8[ a fis d] fis e16 d e8 a, cis e a4~ a8 fis d b g'4~ g8[ a, cis e] g fis16 e fis4 \grace e8 d4
 
 
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Allegro "
  \time 6/8
 \partial 8
 fis8 b fis d cis b' a b fis d b g' fis e cis g ais fis' e d4 cis8 b4
 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 2
 e4 a gis4. fis8 e d cis a' gis4. fis8 e d cis dis e4. fis8 gis a16 gis fis8. e16 e4

 
 
 
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Adagio "
  \time 4/4
 r2 r4 r8 e8
 cis16 b cis e cis b cis e b a b e b a b e a gis a cis, a' fis gis a gis fis e8
 
 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Allegro "
  \time 3/8
 e8 a, a b a fis' e d4 cis16 d e8 r8 

 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 2/4
 g4. bes 16 a g8[ g g g] g f16 es d8 c bes[ a bes c] d

}
 
\relative c''' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio "
  \time 4/4
   bes4 a8 \grace g8 fis8 g4 r8 g8 a8. bes16 a8. g16 fis8 \grace e8 d8 d'4~ d8 g, c2 bes4 a2 g4 r4
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro "
  \time 3/8
 g8 es'4 d8. c16 bes a g8 fis g d4 r8 g16 a bes8 g d4 r8
 
 
}


