 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Vivace"
  \time 2/4
  \partial 4
    c8 d
    e16 d c d  e f e f
    g8 d f e
    d c16 g' f e d c
    b4 r
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 2/2
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Largo"
  \time 6/8
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}