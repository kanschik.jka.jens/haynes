\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro"
  \time 2/4
  d4 d d f16 e d cis
  d8 d, f a
  d4 f16 e d cis
  d8 d, f a d[ e]
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Andante"
  \time 3/4
  r8 f, f f f f
  f a16 bes32 c  f,8 f' f f
  f a16 bes32 c
  f,8 a, a a
  a a16 bes32 c
}


\relative c'' {
  \clef treble
  \key d\minor
     \tempo "3. Allegro"
  \time 3/4
  a4 d a'
  cis,4. d16 e d4
  a d a'
  e4. f16 g f4
}

