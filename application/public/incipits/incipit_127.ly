\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. [Largo] [tutti]"
  \time 3/2
  b8 c b c  d4 d e e
  d g fis8 g a g  fis e d c 
  b c a b 
  g4
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. [Largo] [solo]"
  \time 3/2
     \set Score.skipBars = ##t
     R1.*11
     d1. d1. d2. c4 b a
     g1.
     g4. a8 b4. c8 c4. b16 c
     d2.
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro ma non presto"
  \time 2/4
\partial 8
g16 a
b8 g d' g,
g' g, g r
}

\relative c'' {
  \clef bass
  \key g\major
   \tempo "3. Tuetto"
  \time 2/2
  \partial 8
  d,8
  d b c d
  e4 d
  c8 b16 a b8 a16 g a8[ d,]
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Grave"
  \time 2/2
  d1~ d2. cis4 d1
}
