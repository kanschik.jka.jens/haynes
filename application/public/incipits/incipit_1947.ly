\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Wunderbarer König" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 2/2
  \set Staff.instrumentName = #"1 Clav"
  g16 b d4 c8 b16 d g4 g8
  fis4 r r r8 fis
  g16 d d4 e8 d b d16[ c b a]
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo ""
  \time 2/2
  \set Staff.instrumentName = #"Oboe"
  \set Score.skipBars = ##t
  R1*4
  b4 b b b a2 a4 r
  g g g g fis2 fis4 r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Zion klagt mit Angst und Schmerzen" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef alto
  \key g\major
   \tempo "1. Poco Vivace"
  \time 4/4
  \set Staff.instrumentName = #"1 Clav"
  r2 r8 g4 fis8~
  fis e4 d8~ d c16 b c8 c
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo ""
  \time 4/4
  \set Staff.instrumentName = #"Oboe"
  g8 fis16 g a8 g16 a b4 a g fis e2
  d4 r r2
}
