\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante [Tutti]"
  \time 4/4
  f,2~ f8 a a c
  c8. a16 c4~ c8 f f a
  a8. f16 a4 r8 c \grace d16 c8 bes16 a
  g8. a16 g4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante [Solo]"
  \time 4/4
    f1 c'2. c16 bes a g
    f1~ f1~ f4. e16 f
    f,4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Larghetto"
  \time 3/4
  c2.~ c~ c4 f e
  \grace e4 d2 c4
  g'8. a32 f e4 r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegretto"
  \time 2/4
  \grace bes16 a g a bes c8 d
  e f a, bes
  c4 d16 c bes a
  g8. a16 bes8 r
  
}