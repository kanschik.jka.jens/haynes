 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Siciliana"
  \time 12/8
  \partial 8
  e8
  a,8. b16 c8  c8. b16 a8  a'8. g32 f e8 r r a,
  f'8. e16 d8 gis8. a32 b d,8 d8. c32 b c8 r r
}


\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "2. Spirituoso"
  \time 3/4
  a16 b c d e8 a, e' a
  gis a r b,16 c d8 c
  b a16 gis a c b a b d c b
  c b a c b8 e, b' e
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Andante"
  \time 4/4
  c8 c~ \times 4/6 { c16[ g' f e d c] } d8 g,~ \times 4/6 { g16[ a b c d e] }
  f8 f~ \times 4/6 { f16[ g a g e f] } f8 e r
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "4. Vivace"
  \time 2/4
  \partial 8
  e8
  c16 d e8 e a,
  gis16 a b8 b e,
  a16 b c4.
  b16 c d4.
  c8 b a gis
}