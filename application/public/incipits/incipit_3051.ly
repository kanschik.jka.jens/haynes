\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio assai"
  \time 4/4
  r2 e~
  e16 f e f g32 bes a g f16 g32 e f16 e d f32 e f g f g g16 f32 g a1
}
