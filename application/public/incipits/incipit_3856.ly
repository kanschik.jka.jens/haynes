\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4	
  g'4~ g16 b a g  fis e d8 r16 d e fis
  g4~ g16 e a, g' g8 fis r16 d c d
  e4. fis16 e d8 d g4~
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4	
  r8 d b g a d, d' a b d g2 fis4
  g8 b, e c a d16 c b8 a16 g
  a4
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
  \time 3/2	
  r2 b e
  dis1 b2
  e4 d c b a g
  fis1 e2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga"
  \time 6/8	
  g'4. d
  e8 b c  d a c
  b g b a d, fis
  g4 g'8 fis4 e8
  d4 d8 c4 b8
}

