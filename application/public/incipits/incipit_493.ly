 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4  
    \partial 16
    d,16
    a'2 cis4 r8 r16 d,
    e2 cis'4 r8 r16 a
    g2 fis8 d' d d
    cis2 b4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio cantabile"
  \time 4/4  
    c4 e,8. e16 e2
    c'4 e,8. e16 e2
    d8 d d d  d16 e f e  g f e d
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Rondo. Allegro"
  \time 2/4  
    \partial 8
      b16 c
      d4. e16 fis
      g4. a16 b
      c8 c b b a4.
}
