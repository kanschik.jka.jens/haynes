\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "[Voce]"
  \time 4/4
   \set Score.skipBars = ##t
  d8 a d e16 fis e4 r
  R1*4
  d8 a d e16 fis e4 r8
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "[Oboe]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*2 
   r16 g' e c  f a c, f e c e g a c, f a
   g f e d e8 g a f f g
   a g g f  g a g8. g16
   f4 r r
}
