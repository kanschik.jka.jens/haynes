 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Aria"
  \time 4/4
        << {
      % Voice "1"

    r4 es8 d es f16 g f8. es16 
    d4 r f~ f16 g f g
    es4~ es16 f es f d2
         } \\ {
      % Voice "2"
      r4 c8 b c d16 es d8. c16
      b8 a16 g es'4~ es8 f16 es d4~
      d8 es16 d c4. d16 c b8. c16 c4 r
      } >>

}


