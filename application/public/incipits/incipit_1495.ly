\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo"
  \time 3/4
    g4 d es es d bes'~
    bes a8 es' \grace d16 c8 \grace bes16 a8
    \grace g16 fis8. e16 d4 r8
}


\relative c'' {
  \clef alto
  \key g\minor
   \tempo "2. Allegretto"
  \time 4/4
    d,8 bes'4  a16 g g4 fis8 \times 2/3 { fis16 g a}
    d,8 d'4 c16 bes bes8 a r4
}



\relative c'' {
  \clef alto
  \key f\major
   \tempo "3. Adagio. Melodie 'O Haupt voll Blut und Wunden'"
  \time 4/4
    r16 f,,16 a c  g'16. f,32 f8
    r32 c' b c  e c g' c, bes'16. c,32 c8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Vivace"
  \time 3/4
    d4 es fis,
    \grace fis8 g2 g'4
    \grace f4 es2.
    \grace es4 d2 es4
    \grace d4 c2.
    bes4 r r
}
