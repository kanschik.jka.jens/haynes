\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro [Tutti]"
  \time 2/4
  g'4 c, as'8 g16 f g8 as
  g f es d c4.
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*29 
  g'4 c, b8 c r as'
  g f es d es16 d c b c8 g
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  es8 es, r16 es' f g f a, bes8 r16 bes d f
  as8 as as16 f g as g f es8 r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*8
  es8 es, r16 es' f g f a, bes8 r bes
  as f'16 es d c bes as as f g8 r
     
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro"
  \time 3/8
  g'4. c, as'8 g f es16 d c4
  as8 g f es16 d c4
}