
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key es\major
  \tempo "1. Adagio"
  \time 4/4
 R1 es2 es' d4 c2 bes8 as g4 r4 r2
 r4 es'2 f8 g \grace bes16 as4 g r4 g4
 as2~ as8 g f es \grace es8 d2 r4
  
 
 
}

\relative c''' {
  \clef treble
  \key es\major
   \tempo "2. Allegretto"
  \time 3/4
  r4 bes4 g es4. f8 es4 
 c4. d8 es g g4 f bes,
 bes8 es es d d c c4. bes8 as g f4 as' g8 f es4 d r4
 
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  d2 d'4. cis8 b cis ais cis b4 cis16 b a g
 fis8 e e fis \times 2/3 {g8 e d}  \times 2/3 {cis8 a' g} 
 fis8 b a4
 
 
 
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 2/4
  g8 \grace a16 g32 fis g a b16 b b e
 d cis c4 b8 
 b16 a c32 a c a g8 fis 
 a32 g fis g d8 r8
  
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 6/8
  \partial 8
 a8 fis'4 d8 e4 \times 2/3 {d16 cis b}
 a8 b cis d4 d8 
 a'4 fis8 a gis g
 fis e d d[ cis] 
  
 
}