 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
    c4. a8 d4. bes8
    c f, bes8. c16 a8 f r4
}


\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key f\major
                      \tempo "2. Vivace"
                      \time 3/4
                      % Voice 1
                      r4 a' f
                      g c, c
                      f f d
                      e a, a
                      d d bes
                      c f2~
                      f8 g e4. f8
                      f4
                  }
\new Staff { \clef "treble" 
                     \key f\major
                     % Voice 2
                     r4 f2~
                     f4 e8 f e4~
                     e d2~
                     d4 c8 d c4~
                     c bes2~
                     bes4 a8 g a4
                     g g4. f8
                     f4
                     
                  }
>>
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Adagio"
  \time 4/4
  d4. cis8 f4. e8
  a bes16 a g a g f e8 a, d4~
  d16 f e g f4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allegro"
  \time 2/2
  \partial 4
    c4 a d c e f2 r4 g
    a f g c, f d e
}