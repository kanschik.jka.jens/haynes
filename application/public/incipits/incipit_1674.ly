\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Vivace"
  \time 4/4
  \partial 8
  g8
  c e g g, c8. b32 a g8 c
  d16. f32 e16. g32 f8 e e d r c
  d16. f32 e16. g32 f8 e e d r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Affettuoso"
  \time 3/8
    e8 a, a' f e r
    d16 f e d c b
    c8 \grace b8 a4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro assai"
  \time 6/8
   c4 r8 c4 r8
   c g' f e d c
   b d c b a g
      c g' f e d c
   b d c b a g
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Non tanto Allegro"
  \time 2/4
    g4 d'
    g8. fis32 e d8 e
    \grace e16 d8 c16 b \grace d16 c8 b16 a
    b16 c d4 e8
    d16 d \grace d16 c16 b c c \grace c16 b16 a
    b a g4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Andante delicato"
  \time 4/4
     e8 b4 c8 c b r e
     dis16 e fis g a c, b a g fis \grace fis8 e4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Menuet"
  \time 3/4
   g4 b d
   \grace {e16 fis} g4 g r
   g,4 b d
   \grace {e16 fis} g4 g r
   e8 g c, e d c
   d g b, d c b
   c a' a, c b a b g d' b g'4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Affettuoso"
  \time 3/4
    d4~ d16 fis e d a' g fis e
    \grace e16 d cis d8 \grace e16 d cis d8 \grace e16 d cis d8 
    e16 d cis b  a b cis d  e g fis e
    fis e d cis \grace cis8 d4.
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Moderato"
  \time 2/4
     b8 \grace { e16 fis } g8 fis ais,
     b8 \grace { e16 fis } g8 fis ais,
     b16. b32 cis16. d32 cis16. cis32 d16. e32
     d8 cis r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 4/4
     d8 a'4 g8 \times 2/3 { fis16[ g a]} a8 r8. a,16
     d8. fis16 e8. g16  fis e d cis d e fis g
     a8 a, r
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Spirituoso"
  \time 2/4
    a4 a'
    gis16 fis e8 r e
    fis a e a
    \grace e4 d2
    cis4 r8 e
    fis16 a a a  e a a a 
    \grace e4 d2
    cis4 r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Andante delicato"
  \time 3/8
     a8 e' d  c b a
     \grace { d16 e } f8 e r
     d16 f e d c b
     c8 b a
     gis a4 b8 e, r
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Allegro"
  \time 4/4
     r8 a cis e a e cis a
     fis'4 e d8. cis32 d e8 d
     cis8. b32 cis d8 cis b8. a32 gis a8 b
     e,4 r8
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Vivace"
  \time 4/4
    f2 c
    \times 2/3 { a16[ bes c]} c4 f8 \times 2/3 { e16[ f g] } g8 c,16 e g bes
    \times 2/3 { a16[ g f]} f4  f8 \times 2/3 { e16[ f g] } g8 c,16 e g bes
    \times 2/3 { a16[ g f]} f4  
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Largo e legato"
  \time 4/4
     d4~ d16 a d f \grace f8 e4~ e16 a, e' g
     f cis \grace cis8 d4. f4~ f16 c f a
     \grace a8 g4~ g16 c, g' bes a e\grace e8 f4.
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Con Spirito"
  \time 3/8
   f8 c a
   a16 f a c f a
   g8 e c c16 g c e g bes
   a8 g f
   \grace { a16 bes} c8 c4
   \grace { a16 bes} c8 c4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro"
  \time 4/4
    d4 a r8 f' e d
    e4 a, r8 g' f e
    d16 cis d e  f e f g
    a8 a, a4
    d a r8 f' e d
    e4 a, r8 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 3/4
   f8 f4 f8. c16 f a
   g8 g4 g8. c,16 g' bes
   a8 a4 a8. bes16 a e
   f8 f4 f8. a16 f c
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Vivace"
  \time 2/4
     d4 d d r8 \grace { g16 a} bes8
     a g f e
     d4 d d r8 \grace { g16 a} bes8
     a8 g f e f e16 f d4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
    bes'8 bes,8. c16 d es  f8 f8. a16 g f
    bes8 bes,8. c16 d es  f8 f8. a16 g f
    bes8 bes4 bes8 c16 bes a g f es d c
    d8 bes4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Andante"
  \time 2/4
 \partial 8
   d8 g g,4 es'8
   es d r d
   c16. es32 a,16. c32
   fis,16. es'32 d16. c32 bes8 g r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Presto"
  \time 3/8
     bes4. f' bes8 a g  f es d
     es c f bes,4.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Vivace"
  \time 2/4
    e4 b g8 e g b
    e4 b g8 e g b
    e4 e
    e8 fis16 g fis8 e dis b r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Largo"
  \time 3/8
   g'4.~ g16 g, b d g b
  \grace b8 a4.~
  a16 a, d fis a c
  \grace c8 b4.~
  b16 a g fis e d cis8 a r16
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Allegro assai"
  \time 4/4
   e2 c'
   dis, e8 b r4
   d2 b' cis, d8 a r4
   c2 a'
   b,8 g a b  c d e fis
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Vivace"
  \time 4/4
  \partial 8
    d8
    \times 2/3 {b16[ a g]} g4 g'8 \times 2/3 {fis16[ e d]} d4 d8
    \times 2/3 {e16[ fis g]} \times 2/3 { c,16[ d e]}
    \times 2/3 { fis,16[ g a]} \times 2/3 { a16[ b c] } b8 g r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Adagio assai"
  \time 3/4
   e4. b8 e g
   \grace g8 fis4. b,8 fis' a
   g dis \grace dis8 e4. b8
   c a' a4~ a16 g fis e
   d4 c2 \grace c8 b4.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Spirituoso"
  \time 2/2
    g'2
    g,~
    g4 fis8 g a4 g8 fis g a b c d b a g
    a b c d  e c b a
    b4 g r2
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
    c8 r c r c16 g c e c g c e
    d8 r d r d16 g, d' f d g, d' f
    e8 r e r
    e16 c e g e c e g
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Andante di Menuet"
  \time 3/8
     \times 2/3 { c16[ b a] } e'8 d
     \times 2/3 { c16[ b a] } e'8 d
     c b a
     fis16. b32 \grace a8 gis4
     fis16. b32 \grace a8 gis4
     a8 e r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro assai"
  \time 2/4
     c16 d e f g8 g,
     c4 r
     c16 e d c d f e d
     e4 b
     c16 d e f g8 g, c4 r
}

