 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
  r16 c, d e f g a b
  c8 g e' d16 c
  d8 g, r g'
  e a4 g8
}
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio"
  \time 3/8
  c8 g as
  \grace as8 g4 c8
  d f es16 d
  es4 \grace d8 c8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 2/2
  \partial 4
  g'4~
  g e e c c g g c
  \times 2/3 { d4 c d } g, g'
  e2 d
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Duetto"
  \time 2/2
  \partial 4
  g8 as16 g
  c2. d4 es d d d
  es d c b c
}
