\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro moderato"
  \time 4/4
  \partial 8
  a8
  d a16. d32 f8 d16. f32 a4. f16 d
  d32 cis16. e32 d16. cis32 bes16. a32 g16.
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Andante molto"
  \time 3/4
 
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3.Rondeau. Allegretto"
  \time 3/8
 
}
