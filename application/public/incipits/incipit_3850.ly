\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 2/2
  d4~ d16. e32 d16. e32 c4~ c16. d32 c16. d32
  b16. c32 b16. c32  b16. d32 c16. b32  a16. g32 a8 r16
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  g8
  g4 r16 g a b  a g a b  a b g a
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Adagio"
  \time 3/4
  b4 e fis
  g8 fis g4. fis8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 6/8
   \partial 8
   d8
   b4 g8 d4 fis'8
   g fis g a4
}


