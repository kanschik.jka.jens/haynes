
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef "treble"  
  \key c\major
  \tempo "1. Andante"
  \time 3/8
  % Voice 1
  
       << {
      % Voice "1"
  e8. \times 2/3 {f32 e d} c8 R4.
  [e8. \times 2/3 {f32 e d]} c8 R4.
 e8. \times 2/3 {f64 e d} c16 a' f e4
       } \\ {
      % Voice "2"
R4. c16 d d b c8
  R4. c16 d d b c8
 g8. \times 2/3 {a64 g f} e16 c' a g8 e16 r16
        } >>

}



\relative c'' {
 \clef "treble"
  \key c\major
   \tempo "2. Tempo di Minuetto"
  \time 3/4

       << {
      % Voice "1"
r4 r4 r8 g8 g e e4 r4
  r4 r4 r8 f8 f d d4 r4 
 e'4. g16 f e c f e 
 e c e g 
          } \\ {
      % Voice "2"
e4. \times 2/3 {f16 g a} g8 r8 R2.
 d4. \times 2/3 {e16 f g} f8 r8
       } >>
 
}


 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II "
    }
    \vspace #1.5
}
 
 \relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 2/4
  \partial 8
      << {
      % Voice "1"
  r8 r8 c8 [a]
 r8 r8 f'8 [d] r8
 r8 c8 [a] r8 R2
 r4 r8 d16 d d c c bes bes a a g a4 r4
         } \\ {
      % Voice "2"
  f16 c a'8 r8 r8 c16 a
 d8 r8 r8 d16 bes
 c8 r8 r8 a'16 f
 f e e d d c c bes bes4 a8 r8
      } >>
}

 

 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/2
  f2 c'4. a8 f2~ f
  f4 c8 a' a g g f e4 c8 e e d d c c2 c4 r4
  
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Andante ganzioso"
  \time 3/4
   r2. g2 \grace g16 f8 e16 f
 \grace f16 e4 e8 c' a fis
 \grace fis16 g16 fis g a \grace c16 b16 a b c d4
 
 
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro moderato"
  \time 2/4
   \partial 8
  r8 b32 c d e d8 b32 c d e d8
 r8 d16 b r8 e16 c
 b32 [c d e] d16 g, g [b] \grace d16 c16 b32 a g4 r4
 
}
