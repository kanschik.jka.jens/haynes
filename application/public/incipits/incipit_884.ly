\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro a piacere"
  \time 4/4
    e,2 g8 f e f
    g4. e8 c c f e
    e2 d8 d g f
}
