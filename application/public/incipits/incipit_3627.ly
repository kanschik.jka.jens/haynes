\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 3/4
    g4. a16 b  c d e fis
    g4 d r8 dis
    e4 b r8 b
    c4. b16 a d8 d,
    g4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 2/2
    \override TupletBracket #'stencil = ##f
  \partial 4
    \times 2/3 {e8 f d}
    c4 r r \times 2/3 {g'8 a f}  e4 r r e8 f g2
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Rondo. Moderato"
  \time 2/4
  \partial 8
  b16 c
  d8 d d d
  d8. e32 fis
      \override TupletBracket #'stencil = ##f
    \times 2/3 { g16 fis e} \times 2/3 { d16[ c b]}
    a8 b c d
    c4 b16
}
