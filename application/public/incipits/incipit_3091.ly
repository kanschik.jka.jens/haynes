\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Qui tollis peccata mundi [BWV 235/5]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key es\major
   \tempo "Adagio"
   \time 4/4
   r16 es32 d es16. es,32 es16. f32 es8 r16 es'32 d es16. f,32 f16. g32 f8
   r16 f32 g as bes c16~ c32 es d c bes as f'16 g,8 f16 es
   
   
   
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key es\major
   \tempo "Adagio"
   \time 4/4
   \set Score.skipBars = ##t
    R1*4 r2 r8 bes8 bes4~ 
    bes16 es32 d es16. es,32 es16. f32 es8 r16
  
   
 
   
   
}



