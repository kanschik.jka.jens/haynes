\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 3/8
  bes16 a bes c d es
  f8 d g
  es es4 d4.
  c8 f a, bes16 a bes c d bes
}
