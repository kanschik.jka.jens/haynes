 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
    d16 e
    \grace g8 f32 e d16 cis d e8 g, 
    g f \times 2/3 {r16 d[ e]} \times 2/3 { f16[ g a]}
    bes8 a 
}


\relative c'' {
  \clef treble
  \key f\minor	
   \tempo "2. Andante"
  \time 6/8
   c8 des c c des c
   f4. f8 e f 
   g bes, c des c bes
   as g f
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Allegro"
  \time 3/4
  d4 \grace f4 e2
  f4 a r
  a,8 f' e g f e
  f4 a r
  bes2.
}
