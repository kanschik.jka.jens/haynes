\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro ma non troppo"
  \time 4/4	
     d4 r fis r
      a r16 a b cis d cis b a  g fis e d
      e8. fis32 g fis8. g32 a  g8. a32 b a8 g
      fis32 g a8. 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Andante"
  \time 2/4
    fis32 g a16 d,8[ d d]
    \grace fis8 e d r d
    e d g fis
    a8. fis16 e8 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Vivace"
  \time 6/8
  \partial 8
    a,8
    d4 d8 fis4 fis8
    e e e fis4 d8
    fis4 fis8 a4 a8
    e e e fis4
}
