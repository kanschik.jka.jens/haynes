 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/2
  bes4 d8. es16 f4 r8 f
  bes bes bes bes a[ g16 f] r8 bes
  g f es4 d
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Adagio"
  \time 4/4
  d4~ d16. fis32 g16. es32
  \grace d8 c4~ c16. es32 d16. c32
  bes16. a32 g16. a32 bes16. c32 d16. es32 fis,4 r
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. o.Bez."
  \time 12/8
  r4 r8 r r bes c4 f,8 f'4 es8
  d c bes r r c d es f es4 d8
  f, g f
}