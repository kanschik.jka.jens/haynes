\version "2.16.1"

#(set-global-staff-size 14)

\relative c' {
  \clef bass
  \key f\major
   \tempo "[Voce]"
  \time 4/4
   \set Score.skipBars = ##t
  R1*9
  a16 g f g a g bes a g f e f g8 c16 c
  f, d d e f a g f e8 c r4
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "[Corno d'Anglois]"
  \time 4/4
   \set Score.skipBars = ##t
   r16 d cis a d8 r r16 d cis a d8 a
   b r a r g r fis r
   e r
}
