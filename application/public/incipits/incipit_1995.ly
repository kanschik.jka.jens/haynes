\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Aria	"
  \time 4/4
   r2 r4 r8 bes
   bes a es'8. d32 es
   d8 bes f'4
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Aria	"
  \time 2/4
  bes8 c d es f d bes g' g4 f8
}
