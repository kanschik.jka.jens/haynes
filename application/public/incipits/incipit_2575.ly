 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Grave"
  \time 3/2
    r2 d d,
    g1 r2
    r g' g,
    c2. d4 es2~
    es d4 c bes a
    bes2. a4 g2
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Alla breve"
  \time 4/2
    d1 es
    d4 g, g'1 fis2 g4 g, c2. bes4 a g
    a d, r
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Adagio"
  \time 4/4
    bes4 es2 d4
    r8 es bes as g16 f es g bes
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Vivace"
  \time 6/4
    r4 g8 fis g a bes a bes c d bes
    es2. d~
    d4 c8[ bes]
}