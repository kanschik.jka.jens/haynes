 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "Aria con gusta"
  \time 2/2
  \partial 2
  d4 g
  d es d8 c bes a 
  g2 bes8 d c es
  es d c bes
  es d c bes
  bes a g fis g bes d g
  d4 es d8 c bes a
  g2
  
  
}


