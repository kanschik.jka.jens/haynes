\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key c\major
                      \tempo "1. o.Bez."
                      \time 4/4
                      \partial 4
                      r4
                      r d8 c16 b e8 e e d16 c
                      d4. c16 b c4. b16 a b4
                      
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key c\major
                        % Voice 2
                       \partial 4
                       b8 a16 g
                       d'8 d d d  d c16 b c4~
                       c8 b16 a b4. a16 g a4~ a8 r
                  }
>>
}
