\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Le Mariage de la Grosse Cathos " 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \key g\major
   \tempo "1. La Noce de la Couture. La Pavane"
  \time 2/2
  \partial 4
  d4
  g d g g8 a
  b4 g b b8 c
  d2 b4 a
  a d, fis g
  a a8 b c b a g
  a4 d c b
  a

}


