\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Andante"
  \time 4/4
    c2 d
    e4 c g'2.
    f2 e4~
    e
}
