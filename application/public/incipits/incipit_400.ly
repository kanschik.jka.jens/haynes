\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "Thema"
  \time 2/2
  a'2 \grace { bes32 a g a } bes8 g e g
  f2 g4 a
  bes2 \grace { c32 bes a bes } g4 c
  bes2 \grace { c16 bes } a8 a, c f
}

