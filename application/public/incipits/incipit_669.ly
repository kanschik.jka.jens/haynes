 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 3/4
  <c es g>4 <c es g>4 <c es g>4
  <es g>4 <c es>8 <d f> <es g>4
  <b f' as>4 <b f' as>4 <b f' as>4 <b f' as>4
  <c es>4 <es c'>8 <d b'>8 <es c'>4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "Allegro"
  \time 2/4
  r4 es16 f g8
  c,4 d
  es8 d es16 f g8
  c,4 d es
}


\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key c\minor
                      \tempo "2. Adagio"
                      \time 4/4
                      % Voice 1
                      R1 r2 r4 r8 g'16 f
                      es8 d16 es c es d c b8 g16 c d es f d
                      es f g es 
                  }
\new Staff { \clef "bass" 
                     \key c\minor
                     % Voice 2
                     r8 c,, c bes as as as g
                     f es d c g' g' a b
                     c bes as f g c, b g c c'
                  }
>>
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Allegro"
  \time 3/8
  r8 es16 f g8
  c,8 d4
  es16 d es f g8
  c,8 d4
}
