\version "2.16.1"

#(set-global-staff-size 14)


      
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "Andante"
  \time 4/4
  r8 e, e e  fis fis16 g a8 fis
  g b e2 dis4 e r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "Aria 1. Vivace"
  \time 3/4
  \partial 8
  e,16 fis
  g8 e a e b' e,
  c' e, b' e, a g16 fis
  g8[ e g b]
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "Aria 2. Presto"
  \time 4/4	
  \partial 2.
  b4 e, c'
  b e2 dis4
  e2 d4 cis
  d4. e8 cis d e cis
  d4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "Aria 3. Vivace"
  \time 2/4
  \partial 4
  e,16 fis g a
  b c d b e8 d
  c b g' r
  fis r e16 dis e fis dis4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "Aria 4. Siciliana"
  \time 6/8
  \partial 4.
  b4 c8
  b4 e8 dis4 e8
  fis4 b,8 a16 b c8 b
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "Aria 5. Vivace"
  \time 12/8
  \partial 4.
  b4.
  g8 fis e g b e  dis4 b8 e4 b8
  fis'4 b,8 a' g fis g fis e b4.
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "Aria 6. Presto"
  \time 4/4
  \partial 4
  g8 a
  b4 e, c' e,
  b'2 a
  g4 b fis b
  g
}