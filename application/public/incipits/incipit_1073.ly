\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}




\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 4/4
  \partial 2
  c2 d1 c2 f
  f4 e8 d d c c bes
  bes a gis a d2
  c4 \times 2/3 { d8[ c bes] } a4 g f r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 2/2
    bes4 bes8. c16 d4 d8. es16
    f8 d bes2 d4
    d16 es b c c2
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key f\major
                      \tempo "3. Allegro"
                      \time 6/8
                      % Voice 1
                      \partial 2
                      c8 c f a,
                      a c f, f a c,
                      c4. c4 c8 
                      c2.
                      c4.
                  }
\new Staff { \clef "treble" 
                     \key f\major
                       % Voice 2
                       r8 r4 r8
                       R2.
                       r4 r8
                       f4 f8
                       g f g c4 a8 g4
                  }
>>
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto III " 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\major
                      \tempo "1. Andante"
                      \time 2/4
                      \partial 8
                      % Voice 1
                      fis16 cis32 d
                      a'8 r r4
                      r4 r8 g16 dis32 e
                      a,8 r r4
                      r4 r8 fis'16 cis32 d
                  }
\new Staff { \clef "treble" 
                     
                        % Voice 2
                        r8
                        r8 a a a
                        a32 g fis g a[ g fis g] g8 r
                        r g g g
                        g32[ fis eis fis] g fis eis fis fis8 r
                  }
>>
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio"
  \time 4/4
    a1 a8 a' a2.
    a8 f d2 a'4
    bes2 a4 a8 d,
    d cis cis2
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 2/2
  \partial 8
  a8
  d2 cis \grace cis4 b2 a
  b4 a b d
  a2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante"
  \time 4/4
  \partial 4
  f4
  d d d es
  f4. bes8 d,4 g
  es2 c4 f
  d2 bes4 g'
  d2 c4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Siciliana"
  \time 6/8
   \partial 8
   bes8
   es4 es8 \grace g16 f8 es f
   g4 f8 es4 c8
   bes4 bes8 \grace bes16 as8 g f es4 \grace g8 f8 es4
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 2/4
  \partial 8
  c16 bes
  a bes es8 es d
  \grace d8 c4. d16 c b c f8 f es es4 d8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
