\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/2
  d2 d'4. d8 
  \grace d8 cis16 b a8 a2 r4
  a,2 g'4. g8
  \grace g8 fis16 e d8 d2 r4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Larghetto ma Gratioso"
  \time 3/4
  b2 \grace b8 a8 g16 a
  \grace a8 g2 g'8 e
  d2 \grace d8 c8 b16 c
  \grace c8 b2
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Presto"
  \time 2/4
    d8 r fis r
    d4 r
    fis8 r a r
    fis4 r
    d'4 cis8 b b a r4

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/2
  c'2 c,4. c8
  c4. d8 e4 e
  \grace f8 e4. f8 g4 r
  b c8. a16 \grace a8 g8. f16 \grace f8 e8. d16
  \grace d8 c2 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Larghetto Gratioso"
  \time 2/4
    f4 \grace a8 g8 f16 g
    \grace g8 f8. g16 a8 r16 a
    a4 \grace c8 bes8 a16 bes
    \grace bes8 a8. bes16 c8 r16
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 2/4
  c4. e16 d c4 r
  c'16 b a g  f e d c c'4. c,8
  d4. f16 e d4 r
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante e molto gratioso"
  \time 2/4
   \override TupletNumber #'stencil = ##f
  f4 d8 r16 d
   c \grace d8 c32 b c16 d es8 r16 es
   d bes bes' g f8. \times 2/3 { es32[ d es] }
   es4 d8 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro gratioso"
  \time 2/4
  \grace es8 d8. es16 f8 f
  f8. bes16 f8 r
  \grace d8 c8. d16 es8 es
  es4 d8 r
}
  


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 3/4
  \partial 4
  b16 c c d
  \grace e8 d8 c16 b b4 g16 b d g
  \grace g8 fis8 e16 d c4 a16 b b c
  \grace d8 c8 b16 a a4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  d16. d32
  d2~
  d16 g g fis fis e e d
  d2~ d16 g g fis fis e e d d8 b r
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante Gratioso"
  \time 2/4
        << {
      % Voice "1"
      R2 r4 f8. e32 d
      c8. a16 g a a bes
      bes4 a8 r
         } \\ {
      % Voice "2"
      f8. g16 f16. g32 f16. g32 a8 r r4 R2 R
      } >>


}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/4
  f4 f8. f16
  f16. a32 f8 r r16 a
  a16. c32 a8 r r16 f
  f4 d8 r16 f
  f4 c8 r16 f
  e8 bes' r
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 2/4
  \partial 8
      << {
      % Voice "1"
      r8
      r g'16. e32 d8 r
      r g16. e32 d8 r
      r16 g b d
         } \\ {
      % Voice "2"
      b,32 c d e
      d8 r r b32 c d e
      d8 r r b32 c d e
      d8 r
      } >>

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  g'4 d8. d16 \grace c8 b32 a g16 g4 \grace a'8 g32 fis g a
  b4 g8. g16 \grace e8 d32 c b16 b4
}
