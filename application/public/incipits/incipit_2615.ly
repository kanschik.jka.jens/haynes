\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Moderato"
  \time 2/2
    b8 c d e d c b c
    \times 2/3 { b16[ c b] } \times 2/3 { c16[ d c] } \times 2/3 { d16[ e d] } 
    \times 2/3 { e16[ f e] } \times 2/3 { d16[ e d] } \times 2/3 { c16[ d c] }
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/2
  d4 d d r8 a'
  g fis16 g a8 fis g fis16 g a8 fis 
  g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Andante"
  \time 2/2
    e8 e e e e4 r
    e8 e dis dis e4 r
    g1
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Vivacett"
  \time 3/4
    b4 \grace b8 a2
  g4 b8 a g fis
    b4 \grace b8 a2
  g4 b8 a g fis
}
