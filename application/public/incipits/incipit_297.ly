 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  f,4 f2 a4
  \grace g16 f8 e \grace g16 f8 e f r r4
  a4 a2 c4 \grace bes16 a8 g \grace bes16 a8 g a8 r r4
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Tempo di minuetto"
  \time 3/4
  c,2. d2 d'8 bes
  a4 g c, f r g a r 
}

