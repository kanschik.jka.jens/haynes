\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. [Allegro]"
  \time 2/2
  c8[ a16 bes] c8 d c8[ a16 bes] c8 d 
  c8[ bes16 a] bes8 a16 g a8 f r 
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 2/2
  a4. d16 e cis8 a a' g
  f d r f g c,4 bes8 a[ f]
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 6/8
  c8 a c c a c c4 r8 r4 r8
  c8 a c c a c c4 r8 r4 r8
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. "
  \time 2/2

}
