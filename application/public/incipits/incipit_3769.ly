\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. [Adagio]"
  \time 3/4
  r4 a f
  c4. c8 d e
  f8. d16 bes4. bes8
  bes2 a4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. [Allegro] [tutti]"
  \time 2/2
  \partial 8
  f16 e
  f8 c c f
  f bes, bes g'16 f
  g8 bes, bes g' g a, a f'16 e
  f8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. [Allegro] [Oboe]"
  \time 2/2
       \set Score.skipBars = ##t
       \partial 8
       r8
  R1*6
  c4 a8 g16 f e'4 r8 f
  g f g e f4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. [Allegro] [tutti]"
  \time 3/4
  \partial 8
  f8 f4. a8 g f
  e d c c d e f g e4 f8
  f4.
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. [Allegro] [Oboe]"
  \time 3/4
       \set Score.skipBars = ##t
       \partial 8
       r8
  R2.*20
  f,4 c'2
  a4. g8 f4
  c'4 d8 e f g
  e4. d8 c4
}
