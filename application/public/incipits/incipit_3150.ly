\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Fürwahr, wenn mir das kömmet ein [BWV 113/3]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key a\major
                      \tempo "o. Bez."
                      \time 12/8
                      % Voice 1
                 r4. r8 r8 e8 a4 gis8 a b cis
                 b gis e e fis16 gis a b cis8 a b cis d e
                 gis,4.~gis8 a16 b cis d
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key a\major
                     \set Score.skipBars = ##t
                        % Voice 2
                   R1. r4. r8 r8 e,8 a4 gis8 a b cis
                 b gis e e fis16 gis a b
                       
                        
                
                  }
>>
}


\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key a\major
   \tempo "o. Bez."
  \time 12/8
     \set Score.skipBars = ##t
    R1.*6 r4. r8 r8 e,8 a4 gis8 a b cis
    b a16 gis fis e e8 fis16 gis a b cis8 a b cis gis a 
    gis4.~gis4
    
    }