\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor + Chor): Ich will bei meinem Jesu wachen [BWV 244/26]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key g\minor
   \tempo "Andante"
   \time 4/4
   r4 r8 g8 c d16 es d es c8
   g'2~ g8 f16 es d8 c
   b8. c32 d g,8 
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key g\minor
   \tempo "Andante"
   \time 4/4
   \set Score.skipBars = ##t
     R1*10 r4 r8 g8 c d16 es d es c8
   g'2~ g8 f16 es d es f8 b, a g4 r2
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Orch./Chor"
  \key g\minor
   \tempo "Andante"
   \time 4/4
   \set Score.skipBars = ##t
     R1*12 r4 r8 es8 d es f es 
     d g f es d es f es d4 r4
   
}
