\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 3/4
     c2 e4 f8. g32 a c,8 r r4
     c e f g8. a32 bes c,8 r r4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Rondo. Vivace"
  \time 2/4
 \partial 4
   a8 a
   \grace { bes32[ a g]} a8 c c16 bes g e
   f8 a16 f c8 f
   d d g16 a bes g
   f8 e
}



% \pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio cantabile"
  \time 3/4
    bes4 f' f
    f8. bes16 f4. es16 d
    c8. d32 b c8 g' f es
    \grace f8 es4 d r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Rondo. Presto"
  \time 2/4
    f16 d f d   f d f d
   f es d es f8 d
   f16 es d es  f es d es f es d es f8 d
}

% \pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio non troppo"
  \time 3/4
    g'8. a32 g f4 e
    d8. b16 c8 d e f
    g8. f32 g a8 a, f' d
    c4 b8 r r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Presto"
  \time 2/4
 \partial 8
   e,16 g
   c8 c c f16 e d8 d d g16 f
   e8 c' a f
   e4 d8
}

  
% \pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Moderato cantabile"
  \time 2/4
  \partial 8
    g8
    c8. d16 e8 c a4 r8 f'16 d
    b8 b b g'
    \grace f8 e8 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegretto scherzoso"
  \time 3/4
   e4 e8. g16 f8. a16
   g4 e c
   d d8. f16 e8. g16
   f4 d b
}



% \pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Tema"
  \time 2/4
  f8 c c8. d32 bes
  \grace bes8 a8 g16 f \grace f8 e8 d16 c
  f8 a' g16 f e d
  \grace c8 bes4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Presto"
  \time 6/8
 \partial 8
   a'8 a4 bes8 g a bes c a f f r g
   g4 a8 f e d c b c c r
}


% \pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegretto"
  \time 3/4
  \partial 4
    g'4
    e c g e r c'
    b b a g r g'
    f d b g r f' e e d c r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegretto"
  \time 2/2
    c2 \grace e8 d8 c d e
    c4 e, g c
    e e \grace g8 f4 e8 f
    g2 e4 r
}
