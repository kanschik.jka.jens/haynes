 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Larghetto e Cantabile"
  \time 3/4
    es2. d8. es32 f es4 r8 r16 es
    f8. c'16 c4 bes8. as16
    as4 g
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Allegretto"
  \time 4/4
  \partial 8
  bes8
  bes' bes4 as16 g
  f8. g32 as g8 bes,
  \grace bes8 c8. as'16 
  g f \grace g32 f16 es32 d es8 bes r16
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Vivace"
  \time 3/4
  bes2 es8 bes
  \grace bes4 as2 g4
  c  \grace bes16 as4 \grace g16 f4
  \grace es16 d4 es r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}