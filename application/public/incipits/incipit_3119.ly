\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Recitativ (Sopran): Wiewohl mein Herz in Tränen schwimmt [BWV 244/18]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key c\major
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                       \times 2/3 {a16[ b c]}  \times 2/3 {c16[ b a]}
                       \times 2/3 {a16[ b c]}  \times 2/3 {c16[ b a]}
                        \times 2/3 {a16[ b c]}  \times 2/3 {c16[ b a]}
                       \times 2/3 {a16[ b c]}  \times 2/3 {c16[ b a]}
                       
                       \times 2/3 {a16[ b c]}  \times 2/3 {c16[ b a]}
                       \times 2/3 {a16[ b c]}  \times 2/3 {c16[ b a]}
                        \times 2/3 {gis16[ a b]}  \times 2/3 {b16[ a gis]}
                        \times 2/3 {gis16[ a b]}  \times 2/3 {b16[ a gis]}
                     
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key c\major
                     \set Score.skipBars = ##t
                        % Voice 2
                    \times 2/3 {fis16[ g a]} \times 2/3 {a16[ g fis]}
                     \times 2/3 {fis16[ g a]} \times 2/3 {a16[ g fis]}
                       \times 2/3 {fis16[ g a]} \times 2/3 {a16[ g fis]}
                     \times 2/3 {fis16[ g a]} \times 2/3 {a16[ g fis]}
                     
                      \times 2/3 {fis16[ g a]} \times 2/3 {a16[ g fis]}
                       \times 2/3 {fis16[ g a]} \times 2/3 {a16[ gis fis]}
                       \times 2/3 {e16[ fis gis]} \times 2/3 {gis16[ fis e]}
                       \times 2/3 {e16[ fis gis]} \times 2/3 {gis16[ fis e]}
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Sopran"
                     \set Staff. shortInstrumentName = #"So"
                     \key c\major
                     \set Score.skipBars = ##t
                        % Voice 3
                   r4 r8 fis'8 fis a, r8 a8 c c c8. b16 gis4 r8
                     
                  }
                  
>>
}

