\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Concerto. Allegro [Tutti]"
  \time 4/4
  \partial 16
  g16
  d'4 \times 2/3 { g16[ fis e] } \times 2/3 { d16[ c b] }
  \times 2/3 { a16[ b c] } c8 r4
  c'4 \times 2/3 { a16[ g fis] } \times 2/3 { e16[  d c] }
  \times 2/3 { b16[ c d] } d8 r4
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Concerto. Allegro [Solo]"
  \time 4/4
  \partial 16
  r16
   \set Score.skipBars = ##t
   R1*17
   r2 r4 r8 r16
  g16
  d'4 \times 2/3 { g16[ fis e] } \times 2/3 { d16[ c b] }
  \times 2/3 { a16[ b c] } c8 r4
  c'4 \times 2/3 { a16[ g fis] } \times 2/3 { e16[  d c] }
  \times 2/3 { b16[ c d] } d8 r4
  e16 g fis g g, c' c, e
  d g fis g g, b' b, d
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Adaio [Tutti]"
  \time 2/2
  \partial 16
  e,16
  b'4. r16 b g'4. r16 e,
  b'4. r16 b a'4. r16 fis
  g8 b e, g b, e g, b
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Adaio [Tutti]"
  \time 2/2
  \partial 16
    r16
   \set Score.skipBars = ##t
   R1.*18
   r2 r4 b
   e e2 g4
   g16 fis b a a2.~
   a8 g16. a32 b8 e,4 dis8 fis a,
   a4 g r2
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegretto"
  \time 3/4
  g'4 d d
  \grace e16 d8. c32 b b2
  \grace d16 c8. b32 a a4. c8
  \grace { c16[ b a] } b8. c32 d d2
}
