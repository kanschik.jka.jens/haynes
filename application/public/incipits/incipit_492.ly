\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
\partial 4
bes4
f'2 f8 bes d, f
es2 es8 g c, es
d4. d8 f16 es8. d16 c8.

}
