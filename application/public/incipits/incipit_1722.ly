\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 2/2
    a4. d16 b cis8 a d e
    fis d r fis e e d cis
    d8 d16 e fis[ g fis g]  e8 fis16 e d[ e d e]
    cis8
}
