\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Affetuoso"
  \time 4/4
  c4 c16 a f c' d4 d16 f e f
  c4 c16 f e f  bes,4 g'8 bes,
  a4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  c8
  a4 \grace { bes16[ c] } d4
  c4. bes8
  a4 \grace { bes16[ c] } d4
  c4 r8
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Adagio"
  \time 4/4
    r1 r2 r4 d~ d1~
  d8 cis r e f d bes' d,
  cis a r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 4/4
  \partial 8
  f,8
  a16 g a bes c8 f e4. f16 e
  f8 e16 d c d bes c  a4 r8
}