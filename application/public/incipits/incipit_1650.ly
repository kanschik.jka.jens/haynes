\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Sonata"
  \time 4/4
  fis4 r r r8 fis
  g8. fis16 fis8 fis g8. fis16 fis4
  fis gis8. a16 a4 r
}
