 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key g\major
                      \tempo "1. Adagio"
                      \time 4/4
                      r4 r8 b' a16 d, b' a g fis g a
                      fis8 e16 d g4~ g4. fis8
                      g[ b]
                  }
\new Staff { \clef "treble" 
                     \key g\major
                      d,2~ d4~ d16 e cis8
                      d d, r d' e16 cis d g  c, b c d
                      b8
                  }
>>
}

 


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 2/2
  r8 g' fis g d[ c16 d] e8 d16 c
  d8 b a b~ b[ a16 b] c8 b16 a
  b4
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Largo"
  \time 3/4
  b8 e g fis16 e b'8 e,
  dis b fis' e dis fis~
  fis b, e dis fis e
  e4
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 6/8
  \partial 4.
  g8 a b
  b c d c d e
  e4 d8 g fis e
  d e d  c d c
  b a g r4 r8
}