\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "March I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 2/2
  a'2 d4 cis8 b
  a2 fis8. g16 e8. fis16
  d4. e8 d8. e16 d8. e16
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "March II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 4/4
  d,4 fis8 a d,4 \grace fis8 e8 d
  d4 fis'8 a fis g g fis
  a,4 cis8 e a,4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "March III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 2/2
  d2 e8[ d] \grace fis8 e8 d16 cis
  d2 e8[ d] \grace fis8 e8 d16 cis
  d8 e fis g a b cis d
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "March IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 2/2
  \partial 4
  g'4
  c c,8. c16 c4 c
  c8. g16 g'8. e16 f8. d16 c8. b16
}
