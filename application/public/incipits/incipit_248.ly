\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key d\major
    \time 4/4
    \tempo "1. o.Bez."
        r8 d16 a d4 r8 fis16 d fis4
        r8 a16 fis a8 a16 a b a g a  b a b g
        a g fis g a g a fis   g fis e fis  g fis g e
}

 