 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "Cantabile"
  \time 3/4
  \partial 4
    a4
    \grace g8
    fis2 e4
    fis8 a b cis d cis
    d4 fis e8 d
    a2.~ a8
}
