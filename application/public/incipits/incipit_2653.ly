\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Siciliano [Tutti]"
  \time 12/8
  a4 a,8 a4 a'16 b  cis8. d16 e8 d8. cis16 b8
  cis4 a8 a4 a16 b  cis8. d16 e8 d8. cis16 b8
  cis4 a8 a4
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Siciliano [Solo]"
  \time 12/8
  \set Score.skipBars = ##t
  R1.*5
    r4 r8 r4 a16 b  cis8. d16 e8 d8. cis16 b8
  cis4 a8 a4 a16 b  cis8. d16 e8 d8. cis16 b8
  cis4 a8 a4
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro [Bass]"
  \time 3/4
  a'16 e fis gis  a8 cis, b gis'
  a16 e fis gis a8 cis, e d
  e4. fis16 e fis e d cis
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro [Solo]"
  \time 3/4
  \set Score.skipBars = ##t
  R2.*14
  a8 e4 fis16 gis a8 b
  cis a8. gis16 a b cis8 d
  e cis4 e8 fis16 e d cis
  b a gis a b8 cis4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Largo [Bass]"
  \time 3/2
  r2 e, e
  e1 r2
  r d d d1 r2
  r c c c1 b2 c4 d d2. e4 e1 r2
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Largo [Solo]"
  \time 3/2
  \set Score.skipBars = ##t
  R1.*7
  r2 e1~
  e2 a,2. \grace { a16[ b] } c4
  b2 e,4 f' e d c b a2 r4
}


\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Allegro"
  \time 2/2
  \times 2/3 { cis8[ d cis]} cis8 b a e a b
  \times 2/3 { cis8[ d cis]} cis8 b a e a b
  \times 2/3 { cis8[ d cis]} e4 \times 2/3 { cis8[ d cis]} d4
}
