
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key b\minor
  \tempo "1. Con Spirito"
  \time 2/4
  \partial 8 
 fis8 
d b cis fis,
b16 cis d d   cis d e e
d e fis fis   e fis g g
fis8
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Allegro"
  \time 3/8
  fis8 b ais
  \grace ais8 b4 fis8
  \grace { e16[ fis] } g8 fis16 e d cis
}
 
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
  \tempo "1.Allegro "
  \time 2/4
  \partial 8 
 c8
 c g g c c d r d
 d g, g d' d e r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Presto "
  \time 3/8
  c4. d4. e8 c g'8
  a b c \grace g8 f4 e8
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III"
    }
    \vspace #1.5
 }
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andantino"
  \time 2/4
  \partial 8
  d,8
  g g \times 2/3 { g16[ a b] }  \times 2/3 { b16[ c d] }
  e16 c d8 r
 }
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 3/8
   d,8 g, fis
   \times 3/2 { g16[ a b] } a4
  \times 3/2 { b16[ c d] } c4
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 2/4
    d,4 fis, g r8 e'
    e4 d8 c b16 a g fis g4
    b a g r8	
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV"
    }
    \vspace #1.5
 }
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8.
  d16 e fis g8[ g g g]
 g8. fis32 e d8 e e[ e e e] 
 e8. d32 c b8 
 
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Presto"
  \time 6/8
 g4. d'
 b8 a g a g fis
 g d g b a g
 a d, a' c b a b g b 
 d c b a4.
  
}
 
 
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
  \tempo "1. Moderato"
  \time 4/4
  \partial 8 
 b8 e e4 e e e8
 e16 fis g a b8 e, \grace {a16 [b]} c8 b r8 c8
 b a g fis g16 fis e8 r8
 
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Vivace"
  \time 2/4
  e8 dis16 e fis8 e16 fis
 g8 fis16 g a8 g16 a
 b8. a32 b c8 b a4 g
 
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
  \tempo "1. Andante"
  \time 2/4
  g8 g4 a8
  b8 b4 c8
  d8 e32[ fis \times 3/2 { g32 fis e]}
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 6/8
  g8 d b' b g d d' b g g' fis e d c b a g fis
  g
}
 

