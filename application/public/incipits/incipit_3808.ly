\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 4/4
  e1~ e8 dis r b a'8. c16 b a g fis g4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  b8
  e16 dis e fis e8 fis g4 r8 a b a16 g fis8 e dis4 r8
}
  
  \relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Largo"
  \time 3/2
  b2 e4. dis8 g4. fis8 b4. g8 e4 c' a fis dis2 r r
}
  
  \relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 2/4
  e8 b'4 a8 g16 e fis8 r dis e g fis dis e b4
}

