 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Ouverture"
  \time 2/2
   g'4. c,8 b4 g
   c4. d8 d4. c16 d
   e4. e8 a4. a8
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allemande"
  \time 2/2
  \partial 8
  g'8
  g2 g8 g, c d
  b4 g g8 d' e fis
  e4 d8 c b4. c8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Marche"
  \time 2/2
  \partial 2
  e4 f
  g g, c d e2 f e8 d
  g4 f8 e a g f[ e]
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Air"
  \time 3/4
  r4 e8 d e f
  d4 g, r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "5. Menuet"
  \time 3/4
  \partial 4
  e4
  f g a g a8 g f e
  d4 e f
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "6. Bouree"
  \time 2/2
  \partial 4
  c8 g
  a4 b8 c d4 d8 c
  b4 g2 g'8 c, a'4 a8 g f4 e8[ d]
}
\relative c'' {
  \clef treble
  \key c\major	
   \tempo "7. Rondeau"
  \time 6/8
  \partial 8
  g8
  c d e f d e
  d b g g4 e'8
  a, b c d e16 d c b
}
\relative c'' {
  \clef treble
  \key c\major	
   \tempo "8. Rondeau"
  \time 2/2
   \partial 2
   e4 g
   c,4. c8 c4 e
   a,2 c4 f
   e f8 e d[ e f g]
}