 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 4/4
    g'4. b8 d4. b8 g b d, g b,4
}

