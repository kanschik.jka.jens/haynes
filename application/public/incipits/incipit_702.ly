\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Prima " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  r8 e8 g8. f16 e8 d16 c b4
  c8 d16 e f a g f e8 c e f16 e
  d8 g16 g, c8. c16 c8 b r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. un poco Allegro"
  \time 2/4
   e8 f g f
   e d c b
   c d e f e4 d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Affetuoso"
  \time 3/8
   e4 g8
   c,16 b c8 a'
   c16 b a g c8
   e,4 d8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Giga. Allegro"
  \time 6/8
  \partial 4.
  c'8 b c
  g4. a8 g f 
  g f e f e d
  e d c d c b
  c e g c, e g
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Vivace et puntato"
  \time 4/4
  a4 b cis4. d8
  e d16 cis b8 a gis4 r8 e'
  fis16 e d e fis gis a b e, d cis d e fis gis a
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Affetuoso"
  \time 3/8
  \partial 8
   e,8
   a4 c8
   b16 e gis, a  e8
   a'16 gis a gis a e
   f8 e d
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Allegro ma non presto"
  \time 2/4
   cis4. r16 d
   b4 e
   a,8. gis16 a8. b16
   gis2
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Adagio"
  \time 3/4
  r4 cis4. d8
  b2 e4
  a,8 b16 cis b4. a8 gis2.
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "5. Presto"
  \time 3/8
  a16 b cis d e8
  cis a gis
  a b4
  cis8 a a'~ a gis4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Terca " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  g'8 d e d16 c b4 c \grace { b16[ c]}
  d8 c16 b a8. g16 g4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
 \partial 8
   d8
   g fis g a b a b c
   d c16 b a8 g fis4 r8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Andante"
  \time 3/4
   g'8 fis g4 d
   a'8 g a4 d,
   b' c8 b a g 
   fis2.
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro non tanto"
  \time 4/4
  r8 d b g a d4 c8~
  c b e fis16 g fis8 g4 fis8 g4 r8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Allegro"
  \time 2/4
  g'4 b a fis
  g4. g8 g4 fis e a d, g
  c,8. b16 c8. d16 b2
}

  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 3/2
  r2 a c b e, e'~
  e d4 c b c8 a gis1 r2
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro"
  \time 4/4
 \partial 16
   a'16
   a8 e c b16 a  e'8 b e, e
   a b16 c b8 a gis4 r8 e'
   a16 e cis e  a g f e f8 d r16
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Gratioso"
  \time 3/4
    a'2 g4 f e d
    c b a
    e'2.
    d4 e f e d c
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Giga"
  \time 6/8
  \partial 4.
  r8 e d
  c b a gis a b
  e, fis gis a b c
  d e f e d e
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
  r16 c
  c8. d16 e f e f
  g8 d d e
  c d16 e d8 c
  b g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Largo e Cantabile"
  \time 3/4
   c'4 b8 a g f
   e2 f4
   g a8 g f e d2 c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/8
   c8. d16 e8
   f d g
   e \grace d16 c8 c'~
   c b4
   c4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 4/4
  c4 c g' r
  a8 b c f,
  e4 d8 c
  a' g f e d c b c d e f e d4 r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
  g'8 bes a g fis d d e16 fis
  g8 a16 bes a8 fis16 g fis2
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 4/4
   g16. g32 a16. bes32 a16. a32 bes16. c32
   bes8 g4 fis8
   g16. g32 a16. bes32 a16. a32 bes16. c32
   bes16. bes32 c16. d32 c16. c32 d16. es32
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Largo"
  \time 3/4
   r4 d g,
   es'4 d4. c8 bes4. c8 d4~
   d c4. c8 c4. d8 bes4~
   bes8 a a2 g4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Giga. Allegro"
  \time 6/8
  \partial 4.
  r8 r d
  g a bes a g a
  bes c d c bes a
  d c bes a bes g
  fis4.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. o.Bez."
  \time 4/4	
  \partial 8
  g8
  c8. es16 d8. c32 d es8. g16 f8. es32 f
  g8 f16 es d es c d
  b4.
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Courante"
  \time 3/4
  \partial 4
  c4 c4. d8 es f
  g4 f8 es d c b c c2
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Gigue"
  \time 6/8
  \partial 4.
  r8 r c
  es d c es d c
  d4 g,8 g4 d'8
  f es d f es d es4 c8 c4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. o.Bez."
  \time 4/4
  e,8 g16 a b8 cis16 dis e4~ e16 dis e fis
  g8 fis e fis16 g dis4 r8
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Presto e Spirituoso Assai"
  \time 4/4
  e,4. e8 b'4. b8
  e g fis e dis b b'4~ b8 e, a4. b8 g fis fis2
  e8 g fis e
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Largo e cantabile"
  \time 3/4
  \partial 4
  b4
  e fis g fis g a
  g a8 g fis e dis4. cis8 b4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Gigua"
  \time 6/8
  e8 b e fis b, fis'
  g b, g' a b, a'
  b a g fis g e
  dis4. r8
}
