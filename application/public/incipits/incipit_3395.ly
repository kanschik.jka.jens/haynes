\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegretto"
  \time 2/4
    d4 \grace {c16[ d]} e8. d16 \grace d8 c b \grace {a16[ b]} c8. b16
    \grace b8 a g g a16 fis g8 d r4
}
      
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Andantino"
  \time 2/4
  d2~ d2~ d8 c bes a 
  g4 r8 g as4 g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Menuetto in Tempo Allegretto"
  \time 3/4
  b16 a b c d8 g d g
  d2 \times 2/3 {e8[ fis g]}
  \times 2/3 {fis8[ e d]} \times 2/3 {c8[ d e]} \times 2/3 {d8[ c b]}
  \times 2/3 {a8[ b c]} b4 r
}
