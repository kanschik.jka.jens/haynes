\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 3/4
  c4. bes8 a g f4 bes2
  bes4 a g a g c c bes8 a g f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/2
  r8 f8 a, bes c4 d
  c8 bes16 a bes8 c a[ a] f g
  a4 bes
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Largo"
  \time 3/2
  r2 a g f e d
  e a4 g f e d1.
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 2/4
  \partial 8
  c8 a g a bes
  c4. d8 c bes16 a bes8 c
  a g f g
}
