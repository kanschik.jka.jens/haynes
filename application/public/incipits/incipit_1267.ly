 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 \markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'Chi ritorna alla mia mente'" 
    }
    \vspace #1.5
}

 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe solo"
  \key c\minor
   \tempo "o.Bez."
  \time 4/4
    r4 es8. bes16 c8. es,16 d8. d'16
    es8. bes16 f'8. bes,16  g'8 f32 g as16 d,8. es16
    es4 r r2
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Teseo"
  \key c\minor
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
     R1*2
     r4 es8. bes16 c8. es,16 d8. d'16
     es16 d es8 r
}