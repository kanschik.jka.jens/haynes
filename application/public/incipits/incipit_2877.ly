\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 16
  c16
  e4 f g r8 g
  a f4 a8 g e4 g8 a f4 a8 g e4 g8
  f16 e f8 e16 d e8 d4
}



\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Largo"
  \time 4/4
  es8 es g es  d d f d
  es es g es es4 d
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Tutti]"
  \time 2/4
    e8 e g e
    d d f d
    e e g e
    e4 d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*11 
   r4 r8 g'
   e c d b
   c a' g f 
   e c d b c a' g f
   e4 d
}