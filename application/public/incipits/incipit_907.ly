\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto [I] " 
    }
    \vspace #1.5
}


\relative c''' {
  \clef treble
  \key es\major
   \tempo "1. Allegro Moderato  Es-Dur  4/4"
  \time 4/4
  \partial 4
  bes8 g
  es4 r4 r as8 f
  d4 r r bes
  es f g as g2 f4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante  B-Dur  2/4"
  \time 2/4
  \partial 8
  d16 es
  f8. es16 d8 r16 d
  d es c8 r es
  d4 f16 es d c bes4 a16
}


\relative c' {
  \clef treble
  \key es\major
   \tempo "3. Menuetto  Es-Dur  3/4"
  \time 3/4
  \partial 8*3
  es8 g bes
  es4 r8 es16 d es8 f
  g4 r8 g16 f g8 as 
  bes4 r8 bes16 a bes a bes a
  bes4
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Rondo. Presto  Es-Dur  2/4"
  \time 2/4
  \partial 8
  bes8
  bes4 c8 bes
  bes4 r8 g
  g4 as8 g
  g4 r8 es'8
  es4 d8 c 
  bes bes' as g f es d es f4
}



