\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
  r2 f4 \grace a8 g4
  \grace { f16 g} a2 bes8 d, \grace f8 es8. d32 es
} 
