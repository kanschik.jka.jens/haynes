 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
    a'4. d8 fis,4 g
    b8. a16 a4~ a8 b16 a \grace a8 g8 fis16 e
    d4
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Andante"
  \time 3/4
  b4. c8 d4
  g d e
  d a b
  d c b
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Allegro assai"
  \time 6/8
  \partial 8
  a'8
  a fis g a fis d'
  cis4 b8 a4 d8
  a fis a fis d g
  fis4 e8 d4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}