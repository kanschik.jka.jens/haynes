\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key c\major
    \time 4/4
    \tempo "1. Allegro"
      \partial 4
      c8. e16
      g8. e16 g4 r8 g g g
      c8. b32 a g4
}


