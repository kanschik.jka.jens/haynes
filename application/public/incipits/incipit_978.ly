\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  g4 r8 a g4 r8 c
  g4 r8 a8 g4 r8 c
  b b b b b16 d e fis
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Alla Siciliana"
  \time 6/8
  a8. b16 c8 c8. b16 a8
  f'4.~ f4.~
  f8. e16 f8 e8. b16 d8
  c8. b16 c8 a8.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto"
  \time 2/4
  r16 c, d e f g a b
  c4 r
  r16 c, d e f g a b
  c4 r
 } 

  