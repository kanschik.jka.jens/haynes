\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante [Adagio]"
  \time 2/4
  \partial 8
  c8
  f8 f  f16 c' d c 
  bes16 a32 bes a8 r
}



