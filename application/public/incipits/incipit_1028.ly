\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 2/2
  \partial 4
  g4
  c g g'4. e8
  \grace e16 d2. f8. d16
  c8 b d b
  g4. f8
  \grace f16
  e2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   \partial 4
   r4
   R1*38 
   r2 r4 r8 g'8
   c2 g4. e8
   e d d2 f8 d
   c b d b g4. f8
   f e e2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondo. Allegretto"
  \time 6/8
  \partial 4
  e8 f
  g4. a8 g f
  e4. e8 f g
  \grace a16 g4 f8 \grace g16 f4 e8
  e4 d8 r
}

