\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Der Herr ist mein getreuer Hirt [BWV 85/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key bes\major
                      \tempo "Choral"
                      \time 3/4
                      % Voice 1
                      bes8. c16 d8. es32 f es8. f32 g
                      f4~ f8 es es[ d]
                      d8. es32 f c2
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key bes\major
                     \set Score.skipBars = ##t
                     R2.*1 r8 f,8 g8. a32 bes a8. bes32 c
                     bes2 a4 bes4 r4
                        % Voice 2
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key bes\major
   \tempo "Choral [Grundmelodie: Allein Gott in der Hoeh' sei Ehr']"
  \time 3/4
     \set Score.skipBars = ##t
     R2.*16 r4 bes8. c32 d c8. d32 es
     d4. es16 f es8. f32 g
     f4~ f8 es es[ d]

}