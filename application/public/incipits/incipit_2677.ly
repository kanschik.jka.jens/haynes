\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 4/4
  d8. c16 d16. es32 f16. es32 d8. c16 d16. es32 f16. d32
  g16.
}
      

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 4/4
  bes4 r16 bes a bes c bes a bes d bes a bes
  es d c bes c bes a g a8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Dolce"
  \time 6/8
  \partial 8
  d8
  g8. a16 bes8 g4 d8
  c8. d16 bes8 a8. es'16 d8
  bes8. a16 g8
}
        
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 6/4
  d16 c bes c d8 bes f' bes, g'4 f es
  d16 c bes c d8 bes f' bes, g'4 f es

}
          