\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
    \override TupletNumber #'stencil = ##f
  \time 4/4	
      bes2 bes
      bes~ \times 2/3 { bes8 c d} \times 2/3 { c a bes }
      f'4 d8 r
}
