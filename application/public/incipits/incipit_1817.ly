\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro Moderato"
  \time 3/8
  a8. b16 a8
  d4 a8 d, e16 g fis a
  a8. g16 fis8
}

  
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Menuet"
  \time 3/4
  \partial 4
  a'8. g16
  g8 fis fis4 fis8. e16
  e8 d d4 d8. cis16 b4 b' a a8 g fis4
}

 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Moderato"
  \time 2/4
  \partial 8
  g8
  d' d d16 e32 fis \grace a8 g16 fis32 e
  e8 d r e32 d c b
  b8 a r d32 c b a a8 g
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Menuet"
  \time 3/4
  \partial 4
  a4
  d d fis8. e16
  d4 r a
  e' e g8. fis16
  e4
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. o.S."
  \time 2/4
  \partial 8
  a'8
  a16 g fis g fis g a b
  a4 \autoBeamOff g8 g8 \autoBeamOn
  g16 fis e fis e fis g a
  g4 fis8
}