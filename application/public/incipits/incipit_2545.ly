\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 4/4
  \partial 4
  g'4
  g4. e8 g f d b c2 r8 d16 f e g f a
  g4. e8 g f d b c2

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio"
  \time 3/4
  a8 e' d c b a
  a4 gis a
  e8 gis b d c b
  \grace d4 c2 b4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo"
  \time 2/2
  \partial 2
  c8 b a g
  \grace b8 a4 g c d
  \grace f8 e4 d

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro moderato"
  \time 4/4
  \partial 4
  r8. c16
  c4 a8. f'16
  f4 e8. d16 c4 a8. f'16 f4 e8. d16 c4 a'~
  \times 2/3 { a8 g f]} \times 2/3 {e[ d c]}
  c2 bes4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Aria. Andante"
  \time 4/4
  \partial 4
  f4
  d f g f \grace f16 es8 d es f d4 bes8 d
  c es bes d c es a, c c2 bes4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Largo"
  \time 6/8
  \partial 8
  c8
  c a c c a c
  d f d c4 c8
}

