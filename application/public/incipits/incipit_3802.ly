\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "XIe Suite" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Ouverture "
  \time 2/2
 b4. b8 d8. d16 b8. b16
 fis'4. fis8 e4. d16 cis
 d4 b fis' b
 ais4. fis8 b8. b16 fis8. fis16
 g4 \grace fis8 e4
  
  
}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. 1er Rondeau. très gracieusem.t "
  \time 3/8
 b16 d fis8 g
 g16 fis e d cis b
 ais cis e8 fis
 fis16 e d cis b a 
 g fis e d' cis b
 

  
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "4. 1er Rondeau. Moderem.t "
  \time 2/4
  r8 b8[ d cis]
  b a16 g fis e d cis 
  b8[ fis'' b, e~]
  ~
  e fis16 e d8 cis16 d
  cis8 

  

  
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "6. [1er] Ariette. tendrem.t "
  \time 3/8
  \partial 8
  fis8 
  fis, b cis
  \grace cis8 d8. b16 g'8 
  fis e8. fis16
  d8 cis fis
  
  

  
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "8. [1er] Air. Legerem.t "
  \time 2/4
  fis4 b,
  g'4. fis8
  e[ d cis fis]
  d4 cis8 b
  
  

  
}

\relative c''' {
  \clef treble
  \key b\minor
   \tempo "10. 1er Gavotte. gracieusem.t "
  \time 2/2
  \partial 2
  b8 b, d cis
  b cis d b g'4 fis8 e
  d4 \grace cis8 b4
  d8 cis b d
  cis4 fis b,4. cis8 ais4 fis
  
  

  
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "12. 1er Menuet "
  \time 3/4
  fis4 d8 b g' e
  fis d e cis d4
  cis8 d e4 fis
  d cis8 d b4
  
  

  
}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "14. 1er Rigaudon "
  \time 2/2
  \partial 4
  fis4 d8 b d b g' fis e g
  fis b, d fis b fis b fis
  d cis d b cis fis e fis
  
  
  
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "16. 1er Ariette. très gracieusem.t "
  \time 6/8
  \partial 8
  b8 g'4 fis8 b,4 cis8
  \grace cis8 d4.~ d4 r8

 
   
}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "18. 1er Tembourin "
  \time 2/4
  b16 cis d cis b8 fis
  g[ fis b fis]
  d'16 e d cis b cis d e
  fis g fis e
 
  
   
}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "20. Chaconne "
  \time 3/4
  r4 b4 d
  cis8 b ais b cis4
  fis, b fis'
  e4. d8 cis4
  d b d
  cis8 b ais b cis4
  fis,4
  
 
 
  
   
}

