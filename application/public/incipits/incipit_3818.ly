 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 2/4
  \partial 8
  bes'16. g32
  \grace { g16[ f es]} es8 es4 bes'16. g32
  e16 f f4 c'16. as32
  \grace a16 g8. bes,16 c d es f
  g f r
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Allegro"
  \time 2/4
  es8. g16 bes8 bes
  bes8. g16 bes8 bes
  bes8. g16 bes8 bes
  bes as16 g \grace bes8 as8 g16 f
  \grace f8 g4 r8
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Allegro"
  \time 3/8
  g4 \grace bes8 as16 g32 f
  bes8 d es
  g,4 \grace bes8 as16 g32 f
  bes8 d es
  es16 c c as as es'
  es bes bes g g es'
}
