
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Prima "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\minor
  \tempo "1. Sostenuto Cantabile"
  \time 4/4
  e4 g b b, a' b,8 a' g fis e4 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
 b8 e c' b16 a g fis e dis e8 r8 fis8 g8 c16 b a g fis e dis4
 
 
}


\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 4/4
  g4~ g16 d c d a'4~a16 d, c d b'4~ \times 2/3 {b16[ c b]} \times 2/3 {a16[ b g]} a8 d, r8 

}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 3/8
  e8 \times 2/3 {g16 fis e} b'8 e, \times 2/3 {g16 fis e} c'8 dis, e fis g16 e fis dis e8 
 
 
}

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Seconda "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. ohne Bez."
  \time 4/4
 g4. e16. c32 d8 b c4 f16. f32 g16. a32 d,16. a'32 g16. f32 e16 d c8
 
 

}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
 g8 e16 f e c d e d b c8 d16 e f g a b c8 b16 a g f e d e d c8 r8
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Adagio"
  \time 3/4
  a4 c e f8 dis e4 r4 d8 f e d c b c b a4 r4
 
 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "4. Giga"
  \time 6/8
  \partial 8
 g8 e g e d g d c g e c c' e, f c' f, g c b c g e c
 
  
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Terza "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Grave"
  \time 4/4
 \partial 8
 d8 g16. d32 b16. d32 g,16. g'32 a16. b32 a16. fis32 d16. fis32 a,16. a'32 b16. c32 b16. g32 d16. g32 b,16.
 
 
 
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Allegro Cantabile"
  \time 3/4
  g8 d b g b d g4 a4. g16 a
 b8 g d b d g b4 c4. b16 c d4 c8 b a g fis2
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Grave "
  \time 4/4
  b4. e,8 f dis e4 a8 c b a g e b'4~b16 c32 d e16 g, a8 ais b4 r8
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "4. Allegro Assai"
  \time 2/4
  g4 fis g8 g, b d g a16 b a8 g16 fis g8 g, b d g
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Quarta "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio Cantabile"
  \time 4/4
 \partial 8
 a8 d d d d d c r8 a8 e' e e e e d r8 a8 f' f f f f4 r8
 
 
 
}
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Affetuoso "
  \time 4/4
  \partial 8
 a8 d16 a f' d a'8 bes cis,4 r8 d8 
 \times 2/3 {e16[ d cis]}  \times 2/3 {g'16[ f e]} bes'8 a16 g  \times 2/3 {f16[ e d]}  \times 2/3 {e16[ d cis]} d4
 
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Largo "
  \time 4/4
 f8 e16 d c8 bes a f r8 f'8  \times 2/3 {g16[ f e]}  \times 2/3 {f16[ g a]} bes4 a r4
 
 
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro aßai "
  \time 2/2
  d2 f es4 cis d2~d4 g f e a cis, d2~d4 g f e a cis, d2 ~d4 bes'8 a g f e d cis4 bes a2
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Quinta "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo Cantabile"
  \time 4/4
 \set Score.skipBars = ##t
   R1*2 g4. g8 d'4. d8 g4~ g16 bes a g fis es d8 r8 d8 g g16 f g8 f es16 d es8  r8

 
 
 
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro "
  \time 3/4
  r8 g16 a bes8 g a fis g bes16 c d8 bes c a bes g' g16 a g d g a g d es8[ c]
 
 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Largo "
  \time 3/4
 \partial 8
 f8 bes,4. d8 c bes c4 f a bes,4.  es8 g,[ c] a g f4 r4
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Giga "
  \time 12/8
 \partial 8
d8 g fis g d c d bes a bes g bes a bes c d e fis g fis e d r8 e16 fis g a bes8
  
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Sesta "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 4/4
 e4. gis,8 a4. a8 b f' e16 d c b c b a8 r8 c8 
 b4~b32 f' e d c b a b a4~a32 e' d c b a gis a gis16 f e8 
 
}
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro "
  \time 4/4
  a8 c16 b a c b a e'4 r8 e8 a gis a16 c b a gis4 r8
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Siciliana "
  \time 12/8
 c8. d16 c8 g'4 a8 b,4. c4. f8. g16 f8 g,4 f'8 f4. e4. e8. f16 e8 g4 e8 d8. e16 d8 g4. 
}
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Allegro "
  \time 3/8
 e8 a,16 b c d e8 a, a' gis e g fis d f e c16 d e f d8 b16 c d e c a a' f e d c a a' f e d c  
 
}


