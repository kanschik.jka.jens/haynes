\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
\partial 4
  d8 es
  f4 es8 d c4 bes8 a
  bes4 bes4. bes'8 a g
  f4 es8 d c4 d8[ es]
}
