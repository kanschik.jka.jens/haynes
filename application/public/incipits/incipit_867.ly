\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 2/4
  g'4 d16 c bes a
  g4 d16 c bes a 
  g8 es'' d c
  d bes a g
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Andante"
  \time 6/8
  a8 d cis a'4.
  a2. a4. f16 g a8 a
  a8. g32 a bes8  g8. f32 g a8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 3/8
  g8 bes d g4 g,8
  fis a d a'4 fis,8
  g bes d bes'4 g,8
}
