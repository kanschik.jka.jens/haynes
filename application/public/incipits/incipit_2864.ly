\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  c4 r8 g c4 r8 g
  c g e c c' g e c g'4 r8 g, g'4 r8 g,
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio [Solo]"
  \time 4/4
    \override TupletBracket #'stencil = ##f
   \set Score.skipBars = ##t
   R1*23
  g'2 \times 2/3 {a16 g f} \times 2/3 {g16 f e} 
  \times 2/3 {f16 e d} \times 2/3 {e16 d c}
  a'4 f8 d c b c4
}
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key a\minor
                      \tempo "2. Andante"
                      \time 4/4
                      % Voice 1
                      R1 r2 e16 d c b a gis fis e
                      cis'8 cis cis cis c c c c
                      b b b b
                  }
\new Staff { \clef "bass" 
                     \key a\minor
                        % Voice 2
                        a,16 g f e  d c b a e'8 e e e
                        a, a a a gis gis gis gis
                  }
>>
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 2/4
  c4. g'8
  c,4.g'8
  c,4. g'8
  b, g g'16 f e d
}

