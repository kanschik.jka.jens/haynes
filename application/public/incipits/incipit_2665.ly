 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 3/2
        << {
      % Voice "1"
        r2 a1 a2 a' a, f' a4 f d
           } \\ {
      % Voice "2"
        d,8 f a4 d,8 f a4 d,8 f a4
        cis,8 e a4 cis,8 e a4 cis,8 e a4
        d,8 f a4 d,8 f a4 d,8
        } >>
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. [Allegro]"
  \time 2/2
  \partial 8
  a8 d4 e f8 e16 d e8[ a,]
  d4 e f8 e16 f g8[ c,] a'[ g16 f] g8
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Adagio"
  \time 2/2
  r4 e4. a,8 d f
  es16 d es4 f16 es d8 b gis8.[ a16]
  g'4
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "4. [Allegro]"
  \time 2/2
  d16[ cis d e]  f e f g a[ g a bes] a g f a
  g[ f g a] g f e g f
}