\version "2.16.1"

#(set-global-staff-size 14)

m = \relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Andante"
  \time 4/4
  a'4~ a16 gis a e  f e e f  f e d32 e f16
  e8 gis, a e' d8 f32 e d16 b' d, c b
  \times 2/3 { c16[ d e] }   \times 2/3 { d16[ e f] }   e16 d c b  a8 a' r
}
\new Staff \transpose a g \m

  \relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 2/2
  r8 bes16[ c] d8 es  d[ c16 bes] a8 d
  bes g g'2 fis4
  g r8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Affetuoso"
  \time 6/8
  bes4. d16 es f8 bes, 
  a8. bes16 c8 c4 d8
  es16 f g8 f es d c
}

  \relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Vivace"
  \time 3/8
    r8 d16 c bes a
    bes8 c d
    g, g'16 f es d
    es8 d c
    d c16 bes a g fis8 g4
}

