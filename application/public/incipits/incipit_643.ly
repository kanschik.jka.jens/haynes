\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 3/4
        << {
      % Voice "1"
        r4 r fis8. d16
        e2. \grace a16 g16 fis g a g4 g g fis r
         } \\ {
      % Voice "2"
      d2. d4 cis8. b16 cis4
      \grace fis16 e16 d e fis e4 e
      e d r
      } >>

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Vivace"
  \time 2/4
  \partial 4
  g8 g
  g d b' b
  b g d' d d b a a g4 r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Andante"
  \time 3/4
  r4 bes4 g
  c2 as4
  \grace g4 f2 as4 g r r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. All. spiritoso"
  \time 3/8
  c4. d e8 r r
  R4.
  e4. f g8 r r 
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Con brio"
  \time 2/2
  f2 a4 f
  c c c c a2 r
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. "
  \time 4/4
  fis,2 a
  a8 fis16. g32 a8 a a a' d,4~ d8 b cis4 d r8
}

