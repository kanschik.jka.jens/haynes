 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2e Rigaudon"
  \time 2/2
  \partial 8*5
  fis8 e d e16[ d e fis]
  e4. a8 fis g e a
  fis8. e16 d8[ fis] e[ d] e16 d e fis
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
e4. \times 2/3 {e16 fis g} a8. b16 gis8.[ a16] a4.
  
}


\relative c''' {
  \clef treble
  \key d\major	
   \tempo "2e Passepied"
  \time 3/8
  \partial 8
  a8 a4.
  fis8. g16 a8
  a4. fis4
  

}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "Canaries"
  \time 6/8
  \partial 4.
   d8. e16 fis8
  g8. a16 bes8
  a8. bes16 g8
  fis8. g16 a8 d,8. e16 fis8
  

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "Menuet"
  \time 3/4
  e4. f8 g4
  f2 e4
  g d f e2 d4
  e4. f8 g4
  f2 e4 a fis2
  g2.
  
}