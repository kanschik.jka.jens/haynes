\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Jesu, lass dich finden [BWV 154/4]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key a\major
                      \tempo "o. Bez."
                      \time 12/8
                      % Voice 1
                     e4 cis8 d4 e8 fis e fis e4.
                     cis4 a8 b4 cis8 d cis d cis16 e, fis gis a b
                   
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key a\major
                     \set Score.skipBars = ##t
                        % Voice 2
                 cis4 a8 b4 cis8 d cis d cis4.
                 e,4 cis8 d4 e8 fis e fis e4.~
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key a\major
   \tempo "o. Bez."
  \time 12/8
 
     \set Score.skipBars = ##t
     R1.*6 cis4 a8 b4 cis8 d cis d cis4.
     e,4 cis8 d4 e8 fis e fis e4.
     
}