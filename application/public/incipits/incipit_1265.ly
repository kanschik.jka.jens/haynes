 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'Deh serbate, oh giusti Dei'" 
    }
    \vspace #1.5
} 

 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Violin"
  \key b\minor
   \tempo "Andante"
  \time 4/4
  r16 d fis d b4 r16 e g e ais,4
  r16 b d b b'4 r16 a, e' a, a'4
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key b\minor
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
     R1*4
     r2 r4 b
     r8 d cis4 r8 e ais,4
     r8 fis' d b r2
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Teseo"
  \key b\minor
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
     R1*4
     r2 b4. d8
     cis4. e8 ais,4. fis'8
     d4 b r2
}