\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/2
  d8 d, d' d, d' d, r d'16 cis
  b8 a16 g fis8 g a d, r fis'16 g
  a a a a    b[ a a a]
}

  
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 3/4
  
  << {
      % Voice "1"
      r2.
      r2.
      fis4 b, g'
     } \\ {
      % Voice "2"
      fis b, g'
      ais,8 fis ais cis fis e
      d cis b4 r
  } >>
}

 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 2/4
  a'4 fis
  e8 d16 cis d8 a
  b cis d e
  fis e16 d e8 fis 
  g fis16 e fis8 e16 d
}
