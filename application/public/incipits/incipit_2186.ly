\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "I.ere Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gayment"
  \time 2/2
    e4 c e f
    g2. c4
    b8 c b a g4 f
    e8 f e d c d c d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondeau. Tendrement"
  \time 2/2
 \partial 2
   c8 g' f e
   d4~ d16 f e d c4. d8
   e4 \grace d8 c4
 
}

\relative c'' {
  \clef treble
  \key c\minor
  \tempo "3. 2.e Rondeau"
  \time 2/2
  \partial 2
   c8 g' as g
   f4~ f16 as g f es8 d c b
   c4~ c16 d es f g8 as b c
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Courante. Legerement"
  \time 3/8
  \partial 16
    c16
    c8. d16 e f
    g f g a g f g f g a g f
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "II.e Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Moderement"
  \time 2/2
    e4. g8 f e d c
    g'4 a8 b c4 g
    a8 b g a f d e f
    e4 c r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondeau. Gracieusement"
  \time 3/8
 \partial 8
   g'16 a
   g8 f16 e d8
   c4 g'8
   a16 g c g g f
   e d e f g a
   g8 f16 e d8
   c
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. 2.e Rondeau"
  \time 3/8
  \partial 8
   es16 d c8. d16 es f
   g4 c8
   bes8 as16 g f g
   es as g f es d
   c8.
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Gigue. Gayment"
  \time 6/8
  \partial 8
  g'8
  g as g g f g
  es4 d8 c4 c'8
  c d c b a b c4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "III.e Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Vivement"
  \time 3/8
    c16 d e f g c
    g g g g  c e,
    f e f g f e
    d d d d b' d,
    e d e f e d
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Chaconne"
  \time 3/4
   r4 c g'
   \grace f8 es4 \grace d8 c4 \grace es8 d4
    b8 c d es f g
    es g as g f es
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Ariette"
  \time 6/8
 \partial 8
   g'8
   e d c f e d
   c4. a'
   g8 b d f, g f
   e f g c, e g
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. 2.e Ariette"
  \time 6/8
  \partial 8
  g'8
  as c as  as c as
  g4. r8 c bes
  as g f es f g
  es4 d8 c
}

  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "1.ere Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Lentement"
  \time 3/4
  \partial 4
    es4 d c b c g g'
    g2.
    f4 es d
    es4. d8 c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. 1.ere Ariette"
  \time 6/8
 \partial 2
 c8 c d c
 c g d' d e d
 d g, e' e f e
 e g f e d c d4 c8
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. 2.ere Ariette"
  \time 6/8
 \partial 2
  g'8 as g f
  f es d d es b
  c d es f g as
  g c d b c4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Allemande"
  \time 2/4
  \partial 16
  c16 c4 r16 d e f
  g as f as g8 c
   c b r16
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "II.e Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Moderement"
  \time 4/4
  \partial 2
    c4 g'
    g8. f16 e8 g b,16 g b c  d e f d
    e8 c a'16 g a bes  g f g c  g f g c
}	

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Caprice. Vivement"
  \time 3/4
 \partial 8
 g8 g c16 d es8 es16 f g8 a16 b
 c8 bes16 as g8 f es d
 c
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. 1.er Menuet"
  \time 3/4
  c8 g c4 d e8 c g' c, a' c,
  c' b a g f e e4 d2
 }


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. 2.e Menuet"
  \time 3/4
    c2. as'
    g8 c d es d c b as g f es d
    c2.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "III.e Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Vivement et marqué"
  \time 2/4
  \partial 16
    g'16
    g8. f32 es d16 c bes a
    g a bes c  d es d c bes8 g' a, fis'
    g4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Rondeau. Gracieusement"
  \time 2/2
   d4. es8 d4 c
   bes4 a8 bes g bes a bes c bes a bes g bes a bes c bes a bes
   g a bes c
   d4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. 2.e Rondeau"
  \time 2/2
   b4. c8 b4 a
   g4. a8 b4 c
   d e8 fis g b a g fis e d c b a g a
   b4. c8 b4 a g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Menuet"
  \time 3/4
  b8 d c b a b
  g4 d'8 e fis g
  a b g b a g
  fis e d c b a
}
