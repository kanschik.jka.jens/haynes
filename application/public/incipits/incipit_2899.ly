\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "[voce]"
  \time 4/4
  r8 f d es16 f bes,8 c d es16 es
  f8 f
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "[Oboe]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*6 
   r8 c8 b c16 d g,8 a b c
   d d e d e16 e d e c e d c 
}
