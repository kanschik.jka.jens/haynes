\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  a8
  a16 cis d4 cis16 b
  a cis d4 fis8
} 



\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Largo"
  \time 3/4
    fis,4 b8. cis32 d cis4
    fis cis8. d32 e
    d4
    cis8 d e fis g b,
} 


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 3/4
    fis,8 g a4 b
    cis8 e d cis d4
    e8 g fis e fis4 fis e r
} 