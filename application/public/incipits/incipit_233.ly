\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key c\major
                      \tempo "1. o.Bez."
                      \time 3/8
                      r8 e f
                      g~ g16. f32  e16. f32
                      g16. e32 f8. e16
                      d16. c32 d4
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key c\major
                        % Voice 2
                      r8 c d
                      e~ e16. d32 c16. d32
                      e16. c32 d8. c16
                      b16. a32 b4
                  }
>>
}
