\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                    \set Staff.instrumentName = #"Oboe 1"
                     \key g\major
                      \tempo "o.Bez."
                      \time 3/4
                      g'8. fis32 e d4 e
                      e d g c b a
                      \grace c8 b4 a8 g r4
                      
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                    \set Staff.instrumentName = #"Oboe 2"
                     \key g\major
                        % Voice 2
                       g8. fis32 e
                       d4 c
                       c b e a g fis 
                       g c, r
                  }
>>
}
