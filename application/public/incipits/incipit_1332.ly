 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro ma non troppo"
  \time 3/4
    \times 2/3 { bes8[ f' d]} bes4 \times 2/3 { bes8[ c a]}
    \times 2/3 { bes8[ f' d]} bes4 \times 2/3 { bes8[ c a]}
    bes8 r c r d r
    f2 es8 r
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Adagio"
  \time 2/4
  c4 bes8 a \times 2/3 { bes16[ c d]} d4 c8
  \times 2/3 { d16[ e f] } f8~ f16 e d c
  \grace c8 bes8 a r4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Menuetto"
  \time 3/4
  d2 es4 f2 g4
  es c a'
  bes2 d4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Finale. Allegro molto"
  \time 4/4
    r2 bes'4 bes
    \grace g8 f4 es8 d
    es16 f g8 g g
    f4 r bes bes
    \grace g8 f4 es8 d c16 d es8 es es
    d4 r
}