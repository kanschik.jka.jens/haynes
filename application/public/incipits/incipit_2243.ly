 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Romance"
  \time 2/2
  f2 \grace { g32[ f es f]} g8 f es d
\grace d16 c4 c2 d4
f8 es es4. d16 es f8 es
\grace { d16[ es f] } es2
}
