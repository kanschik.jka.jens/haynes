\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Ouverture"
  \time 4/4
  r2 c4. f8
  e4. d8 c8. bes16 a8. g16
  a4 c
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Passepied"
  \time 3/8
  \partial 8
  c'8
  a f c
  f4 c8
  f16 g a8 g
  f a
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Sarabande"
  \time 3/4
        << {
      % Voice "1"
      r4 f8. e16 f4
      r d8. c16 d4
      c
         } \\ {
      % Voice "2"
      a8. g16 a4. a8
      bes8. a16 bes4. bes8
      a4 g
      } >>
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Rigaudon"
  \time 2/2
  \partial 4
  c4
  f2 e
  f2. a4
  a g g f e8 d e f e4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. Le Plaisier"
  \time 6/4
  c2 f4 a,4 a8 bes a bes
  c2 f4 a,4 a8 bes a bes
  c2
}
