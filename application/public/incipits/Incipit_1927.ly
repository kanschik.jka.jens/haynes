\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 3/4
  g4 b d
  g8 g4 g g8~ 
  g fis16 e d e d cis b cis b a
  g8

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Affetuoso"
  \time 6/8
  \partial 4.
  \grace es8 d16[ c d8 \grace {g16[ a]} bes8]
  a8 fis4 \grace {e16[ fis]} g16 es es d d c
  c d c bes c a

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 2/4
  r8 g8[ g g]
  fis16 e d8 e e
  d16 c b8 c c
  b16 a g8 b cis
  d16 cis d e fis g a fis
  g8 d

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro"
  \time 3/8
  r8 d8 d
  e a g
  f d bes'
  bes e, a
  a d, g
  g f16 e f8
  e a, a' 
  a8 
  

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Andante"
  \time 3/8
  f16 e f g a bes
  e, d e8 a
  a g16 a bes8 bes a r8

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro"
  \time 2/4
  \partial 8
  d8 e e e a16 g
  f8 d a'4~ a8 g16 f g4~
  g8 f16 e f8 a
  g16 a g f e d cis b a4

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro ma non troppo"
  \time 2/4
  \partial 8
  e16 d
  \grace d8 cis8 e16 cis \grace cis8 b8 e16 a,
  gis8 a r8 gis8
  a gis16 fis e8 a 
  a gis r8

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Aria I. Affetuoso"
  \time 3/8
  cis16 d cis b cis a
  a4 e'8 \grace {d16[ e]} 
  fis16 gis gis8. fis32 gis
  a4 e16 cis
  d cis d b cis a

}
  
  \relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Allegro"
  \time 3/8
  \times 2/3 {cis16 d e} a,8[ a]
  a16 b cis d e a,
  fis' gis gis8. fis32 gis
  a4 e8

}
  
 
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allegro"
  \time 3/4
  e4 b g
  e8 e e e e e 
  g g g g g g 
  b b b b b b 
  e e e e e e 
  dis fis b b  a16 gis a b
  gis4
  

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Aria I. Affettuoso"
  \time 6/8
  \partial 8
  b8 e16 dis e8 g
  fis16 e fis8 \times 2/3 {a16 b c}
  b16 c b a g fis g8 \grace fis8 e8 b

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Allegro"
  \time 2/4
  \partial 8
  b8 g[ e b' g]
  e'16 e e e e e e e 
  b8[ g e' b]
  g'16 g g g g g g g 
  e8[ b g' e]

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Allegro"
  \time 3/4
  b8 b b b b16 cis d e
  fis8[ fis fis fis] e fis16 e
  d8 b4 b' b8~
  b4 ais2
  

}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Adagio"
  \time 3/4
  b8 d d8. cis16 b8. cis32 d
  cis8 e e8. d16 cis8. d32 e
  d cis b8. b'4 b
  b ais2

}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Allegro"
  \time 2/4
  b8[ d fis b]
  ais fis a b16 a
  gis8 e g a16 g
  fis8 gis16 ais b a g fis
  e8 

}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
  r8 d16 e fis8 d
  g fis16 g e fis g a
  fis8[ b b b] 
  e,[ a a a]
  d,[ g g g]

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 4/4
  r8 d8 a'4. b16 a g4
  g8 a16 g fis4. g16 fis e4~
 ~e8 d16 cis d fis e d cis4. a8

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 2/4
  \partial 8
  a8 d,16 cis d e d e fis g
  e d e fis e fis g a 
  fis e d e fis e fis g
  e8[ a a a]

}


