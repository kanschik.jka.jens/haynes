\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
    f8 d r g
    f bes a g
    f  d r g f es d c
    bes2. \grace d16 c8 b16 c

} 
