 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Grave"
  \time 4/4
    bes1 g2 f
    r4 c'~ c8[ c]
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Tempo giusto"
  \time 4/4
  r4 bes2~
  bes4 a8. bes16 c4 f, d'2~
  d4 c8. d16 es4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Adagio"
  \time 4/4
  r4 g2 fis4
  d'4. es8 a, bes c d
  bes g r4 r2
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Allegro"
  \time 4/8
    r8 bes[ d c16 bes]
    f'8[ f f f]
    bes bes, d c16 bes
    f'8[ f f f]
    bes bes,
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "5. Menuet"
  \time 3/4
  bes4 d8 c bes a
  bes4 a8 g f4
  d' f8 es d c
  d4 c8 d bes4
}
