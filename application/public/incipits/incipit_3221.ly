\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Nun du wirst mein Gewissen stillen [BWV 78/6]" 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\minor
   \tempo "o. Bez."
   \time 4/4
   \partial 8
   g8
   es8. f16 f8. es32 f g8 c, es16 d c b
   c8 g16 f g a b c d es f d f es d c
   b c d b d c b a
   
   
   
}

\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key c\minor
   \tempo "o. Bez."
   \time 4/4
   \partial 8
   \set Score.skipBars = ##t
 r8 R1*7 r2 r4 r8 g8
 c4 b8. a32 b c8 g bes16 as g f
 g4 c,8
   
   
}


