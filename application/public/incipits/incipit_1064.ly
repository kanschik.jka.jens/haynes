\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 4
  r8 r16 g
  c8. e16 d8. g,16 e f g8 r8 r16 g'
}
