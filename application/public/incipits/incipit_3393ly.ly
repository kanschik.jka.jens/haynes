 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Ouverture"
  \time 4/4
    b4. gis8 e'4. b8
    gis'4. a8 fis8. fis16 e8. dis16
    e8. e,16 e8. e16 fis8. gis16 a8. b16
    cis4. a8 fis'4. cis8
}
 