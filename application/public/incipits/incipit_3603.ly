\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro moderato"
  \time 2/4
  d4 d fis fis
  a2 fis4 r
  e e8 g
  cis,4 cis8 e
  d2
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adaio"
  \time 4/4
 
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Menuetto con Variazioni"
  \time 3/4
 
}
