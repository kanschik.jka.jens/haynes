\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro [Tutti]"
  \time 2/4
  g4 es'
  d16 g bes, d g, bes d, fis
  fis4 g8 d16 a'
  a4 bes8 d,16 c'
  c4 d8 es
  g, fis r4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro [Solo]"
  \time 2/4
  d4 bes'
  a16 cis, d fis, a d, c'8
  bes4 \grace {a16[ bes] } es8 a,
  \grace g8 fis8 g r
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 2/2
    bes4 bes2 bes4
    \times 2/3 {bes8[ f' es] } \times 2/3 {d8[ c bes] } bes2
    bes'4 bes,2 bes4
    \times 2/3 {c8[ g' f] } \times 2/3 {es8[ d c] } c2
    
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro moderato [Tutti]"
  \time 3/8
  d4 g8
  bes,4 d8
  g,4 bes8
  \times 2/3 {d,16[ e fis] } g4
  a d8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro moderato [Solo]"
  \time 3/8
  d4.~ d~ d~ d
  fis,32 g a bes c8 bes  bes a g
}
