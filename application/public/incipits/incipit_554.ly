\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. All. Grazioso"
  \time 4/4
c8 bes a2 bes4
c8 bes a2 bes4 c8 cis d2 e4
\grace g16 f8 e16 d16 c8 c c4 r
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Rondeau. Menuetto gratioso"
  \time 3/4
  f8 c c2
  \grace d16 c8 bes16 a a2
  c8 a g f g a
  \grace c8 bes2 a4
}
