\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key a\major
   \tempo "Allegro"
  \time 3/8
    a8 b16 cis d e
    d4 cis8
    a8 b16 cis d e
    d4 cis8
}

