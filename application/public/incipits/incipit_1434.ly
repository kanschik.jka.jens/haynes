 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  c'4. b16 a g8 g g g
  a4 g c,2
  c4 b16 c d e  f e d c  b a g f
}
