 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 2/2
    g'4. as8 b,4. c8
    d g, g' d es d16 c r8 g'
    g16[ e e c] c bes bes des  des[ c c bes] bes as as g
    f es f8 r
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Allegro"
  \time 4/4
  g'16 f es d c8 b c4 r8 c
  d16 c b a g as' g f es d c8 r
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Sarabande"
  \time 3/4
  es4 bes es, f' d d,
  g' f es d4. c8 bes4
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. o.Bez."
  \time 3/4
    c8 es16 c g' as
    g16 f es d c4 es
    d8 es f g as c,
    b2 c4
}