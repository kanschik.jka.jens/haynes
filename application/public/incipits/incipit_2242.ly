 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 3/4
    a,4 bes c
    d2. d4 c bes a r r
}
 
