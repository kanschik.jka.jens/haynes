\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Concerto"
  \time 6/8
    \partial 8
    f,8
    bes4. c
    d~ d4 f8
    g bes g f bes f
    g bes g f bes f
    g4 f8 es4 d8 c d es c4 c8
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio assai"
  \time 2/2
      d4 r8 d es d es d  
      c4 r8 c d c d c
      bes4 g' bes,2
      r4 bes a8 f' e d
      cis4 r8
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Fantasia"
  \time 2/2
     bes2 d es r4 es
     d es d es  d es d g
     f2 bes4 d,
     c a8 bes c4 f,
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Menuet"
  \time 3/4
    f4 g f
    f es8 d c bes
    f'4  g f
    f es8 d c bes
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "5. Aria"
  \time 2/2
      d4 bes f' r8 d
      g4 f es d
      f16[ d g8]  es16 c f8  d16 bes es8  c4
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "6. Gigue"
  \time 12/8
    \partial 8
      f8
      f4 r8 g4 r8 f4 r8 g4 r8
      f4 r8 es4 r8 d4 r8 c4 r8
      d4 r8 d4 r8  d4 r8 d4 r8
}

