\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Vivace"
  \time 6/4		
      d4. es8 d4 c4. d8 c4
      es4. f8 es4
      d4. es8 d4
      f4. g8 f4
}

