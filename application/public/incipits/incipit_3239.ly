\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran + Alt): Wenn Sorgen auf mich dringen [BWV 3/5]" 
    }
    \vspace #1.5
}
 


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Ob. d'am. 1+2 un."
  \key e\major
   \tempo "Duetto"
  \time 4/4
     \set Score.skipBars = ##t
     r8 b8 e4. dis16 fis a4~
     ~a8 gis16 fis gis8 e~ e dis16 fis a4~
     ~
     a8 gis16 fis gis8 e16 gis d'8 cis16 b cis8 fis,16 a
 }
 
\relative c' {
	
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Sopran"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key e\major
                      \tempo "Duetto"
                      \time 4/4
                       \set Score.skipBars = ##t
                      % Voice 1
             R1*10 r8 e8 b'4. ais16 cis e4~
             e8
                 
                    
                   
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Alt"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\major
                     \set Score.skipBars = ##t
                        % Voice 2
              R1*8 r8 b,8 e4. dis16 fis a4~
              ~
              a8 gis16 fis gis8 e~ e dis16 fis a4~
              ~
              a8 gis16 fis gis8 e cis4~ cis16 fis gis ais
              b4~b16
                      
                  }
                  

                  
>>
}


