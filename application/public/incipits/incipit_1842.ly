\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Poco Allegro"
  \time 4/4
    bes4 f' d8 c16 bes a8 bes
    c f,16 bes a8 bes c f,16 bes a8 bes
    a bes a bes
    c16 g' f es  d es f g
}
