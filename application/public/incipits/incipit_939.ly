 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Ouverture"
  \time 2/2
  a'4. a8 f4~ f16 e f c
  d4. d8 d4 c
  bes4. c8 a4 g
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo ""
  \time 6/8
  c8 c16 d e c f8 f16 g a f
  bes8 bes16 a g f f8 c f
  f8. g16 e8 f4.
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Aria. presto"
  \time 2/2
  \partial 4
  c4 f c c f g c, c g'
  a a8 f bes4 bes8 a g2.
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Menuet"
  \time 3/4
  c8 d c bes a4 
  f'8 g f e d4
  g a f
  e d8 e c4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Couranté"
  \time 3/2
   f,4 a2 c4 f c
   d c8 bes a bes c4 g8 a bes4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "5. Gigue"
  \time 3/8
   \partial 8
   c8
   f c f f a f
   f c f f4 d8
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "6. Aria"
  \time 2/2
  c4 f8 c f4 a8 f
  g4 g8 e c4. c8
  d f f[ d] e bes bes[ a]
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "7. Gigue"
  \time 6/4
   \partial 4
   c4 f2. c2 f4
   g8 f g a g4 g2 g4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "8. Boure"
  \time 2/2
   \partial 4
   f8 e
   f4 c c f8 e
   f4 c c f8 e
   f4 g a bes8 a
   g4 e c
}
\relative c'' {
  \clef treble
  \key f\major	
   \tempo "9. Gigue"
  \time 2/2
   \partial 4
   c4 f e f g a4. bes8 a4 c, d c bes c8 bes
   a4 f2
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "10. Chacone"
  \time 3/4
  f4 c8 f f4
  f a8 f f4
  f4. f8 g4
  a f c a c8 f f4
}