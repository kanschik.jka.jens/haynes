 \version "2.16.1"
         #(set-global-staff-size 14)
         
     \relative c'' {
  \clef treble
  \key  bes \major
 \tempo "1.Allegro assai [Tutti]"
  \time 4/4
  bes'2 f g d es \afterGrace a, {bes16 c} bes8 r f r bes16 c d c bes8 r
  }
   \relative c'' {
  \clef treble
  \key  bes \major
  \tempo "1.Allegro assai [Solo]"
 \time 4/4
 bes'2 f g d es \afterGrace a, {bes16 c} bes8 r bes4 ~ bes8 f' e d c r c4
   }

  \relative c'' {
  \clef treble
  \key  f \major
 \tempo "2. Adagio "
  \time 4/4
  f4 f16. c32 f16. a32 \grace {a8} g16. \trill f32 \grace {f8} g8 r4 
  g4~ f16 e c bes' bes8 \trill a r c,
   }
  
    \relative c'' {
  \clef treble
  \key  bes \major
 \tempo "3.Allegro assai [Tutti]"
  \time 3/8
  bes4. f'4 es8 d16 bes c8 a bes bes,16 c32 d es f g a bes4. \p
    }
  \relative c'' {
  \clef treble
  \key  bes \major
  \tempo "3.Allegro assai [Solo]"
 \time 3/8
  bes4. f' bes16. (g32) f4 es4.\trill d4 r8 r4. bes \grace {d8} f4
  }
  