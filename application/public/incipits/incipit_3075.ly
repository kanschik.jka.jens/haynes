\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Weh der Seele, die den Schaden [BWV 102/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key f\minor
   \tempo "Adagio"
   \time 4/4
  r4 des2.~
  ~des8 c16 bes e4~ e8 f f, g 
  as16 des b c g as e f c e g bes g'4~
  ~g8 bes,~ bes as16 g as e f c' es4~
  es16

   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key f\minor
   \tempo "Adagio"
   \time 4/4
   \set Score.skipBars = ##t
   R1*9 r4 des2.~
   ~des16 c bes32 a bes16 e,4~
   ~e8 f r8 des'16 c
   as8 g g4 r2
     
     
}



