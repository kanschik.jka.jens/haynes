\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Largo [Violine]"
  \time 2/2
  d,4 r8 d'16. cis32 b8[ b16. a32] g8 g16. fis32
  e4 r8 e'16. d32 cis8[ cis16. b32] a8 a16. g32
  fis4 r8 fis'16. e32  d8[ d16. cis32] b8 b16. a32
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Largo [Oboe]"
  \time 2/2
  a'1~ a1~ a4. fis8 d4 r
  d d~ d16[ e fis g] a fis e d 
  d8. cis16 e4
}
