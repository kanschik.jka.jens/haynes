\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro "
  \time 4/4
      \override TupletBracket #'stencil = ##f
    es4 bes16 g as f  \grace es8 d es r c'
    \times 4/6 { bes16 g' f es d c} \grace c8 bes8 as \grace as8 g4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 3/4 
     bes8 d4 f8 f16 d c bes
     \grace c8 bes8 a r c16 bes  a g f es
     \grace f8 es8 d r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro"
  \time 3/4
    \override TupletBracket #'stencil = ##f
    es,8 g bes4 es
    \times 2/3  { c8 d es} \grace c8 bes4 r
    c8 as f4 es d es r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
     bes8 bes'4 d32 c bes a  bes16 d f,4 es8
     d16 f bes, d  c es a, c  \grace c8 bes32 a bes16 f8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 2/4
  f16 c r c d bes' r e,
  f c r c d bes' r e,
    \override TupletBracket #'stencil = ##f
    \times 4/6 { f16 a g f e d}
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro assai"
  \time 2/4
    bes8 d4 f8 f bes4 g8
    \grace g8 f4. es8
    d16 c bes a bes8 g
    \grace g8 f4. es8
}
