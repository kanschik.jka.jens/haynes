\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Grave"
  \time 4/4
  f,4 f f f
  bes bes bes bes8 f'
  es4 es es es8 f
  d4 bes f' f
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Presto"
  \time 4/4
  bes2 c
  d4 es2 d4
  c2 bes8 c d bes
  f' es d c bes d es bes
}


\relative c'' {
  \clef treble
  \key g\minor
     \tempo "3. Adagio"
  \time 4/4
    d2 g4. g16 d
    c2 es d8 c bes a bes4 g
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 2/2
  r16 f,[ g a]  bes c d es
  f[ bes, c d] es f g a
  bes4 r r2
}