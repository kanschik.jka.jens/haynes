\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/4
  c4 f
  e r8 f
  d c  bes8. c16
  a8. g16 f4
}

  
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio"
  \time 3/4
  
  << {
      % Voice "1"
      r2.
      r4 a2
      d4 d4. e8
      cis4 a g'
      f d f
     } \\ {
      % Voice "2"
  r8 a, f d r4
  r8 a' e cis r4
  r8 a' d, bes r4
  r8 a' cis, a r4
  } >>
}

 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 6/8
  r8 c16 bes a g f8 c' f
  e4 d8 c f16 e d c
  bes8 g' g bes, g' g
}
