\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Suite in g " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Prélude"
  \time 2/2
  r4 g4 g4. g8
  a4 a a4. a8
  bes4 bes bes4. g8
  d'2 d~
  ~d c~
  ~c4 c c bes8 a
  bes4 d g4. g8 g2
  

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "2. Sarabande"
  \time 3/4
  bes4 a4. g8
  a2 fis4
  g \grace g8 a4. bes8 a2 g4
  bes a4. g8 a2
  
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "3. Gavotte"
  \time 2/2
  \partial 2
  g4 d
  g a8 bes a bes g a
  fis2 d4 d
  d c8 bes a bes c a 
  bes4 g
  

}

\relative c''{
  \clef treble
  \key g\minor
   \tempo "4. Petit rien"
  \time 2/2
  \partial 2
  d4 es8 c
  d4 c8 bes c4 bes8 a
  bes4 g d' es8 c
  d4 c8bes c4 bes8 a bes2
  

}

\relative c''{
  \clef treble
  \key g\minor
   \tempo "5. Rigaudon "
  \time 2/2
  \partial 4
  d4 es2 c \grace {bes16[ c]}
  d2 es4 d8 c
  d4 c8 bes c4 bes8 a
  bes2 g4
 
  

}

\relative c''{
  \clef treble
  \key g\minor
   \tempo "6. Menuet"
  \time 3/4
  d4 bes c \grace {bes16[ c]}
  d4 d d \grace {c16[ d]} 
  es4 d g
  fis8 a fis e d4
  d bes c
 
  

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite in G " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prélude. Lentement"
  \time 2/2
  d4 d d c8 b
  c4 c c c 
  c b8 a b4. c8
  a4 r4 c2 \grace c8 
  b4 r4 c2 \grace c8 b4 r4 r4
 
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite in b " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key b\minor
   \tempo "1. Prélude. Gravement"
  \time 2/2
  \partial 2
  r2 r4 b4 a b8 a
  g4 g8 fis e4 d 
  cis2 r2
  r4 fis4 gis a 
  b a gis4. fis8
  e2 e4. e8 fis2 e4. fis8 fis2 r2
 

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite in A " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Prélude"
  \time 2/2
  r4 e4 e e 
  a2. a4 a g8 fis g4. a8
  fis2 fis4. fis8 gis2 gis4. gis8
  a2 a~
  a gis4. a8 a1
  

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite in e " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Prélude"
  \time 3/4
  r4 e4 g
   fis fis a 
   g8 a g fis e4
   b'2 b4
   b a4. a8
   a4. a8 b8 fis
   g4 g b
   a a b
 

}



