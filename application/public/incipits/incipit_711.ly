\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Largo"
  \time 4/4
    g'4 r8 g a4 r8 a
    g4 r8 g f g16 a g8. f16
    e8 c e16 d c d d8 g, g'4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/4
  
}
