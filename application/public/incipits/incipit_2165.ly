\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro"
  \time 4/4
  d8 d d d d16 e f g a8 bes
  d,8 d d d d16 e f g a8 bes
  cis,8 a cis e g e4 f16 e
  f8
}

