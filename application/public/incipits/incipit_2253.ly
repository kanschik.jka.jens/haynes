\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
    f2 g a4 bes r8 c, f a
    c4. a8 d4. c8 
    c bes bes4 r2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*68 
  r2 r4 c
  f4. a8 c a bes g
  g f f4 r8 c f a c2 d8 c bes a
  a g g2 r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Romance"
  \time 4/4
    g'4. f8 e4 e
    a16 c b c  d c b a g4 r
    c8 g a g  g f f e
    e f fis g d e f fis
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo. Allegro"
  \time 2/4
  \partial 8
  c'16 a
  f8 f f bes16 a a4 g8 c16 a
  f8 f f f16 a
  g f e d c8
}