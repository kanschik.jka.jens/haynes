\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. "
  \time 2/2
  r16 d,[ e fis] g a b cis d[ d d d] d d d d
  d8[ fis16 e] d8 d e16[ d e fis] e d e fis
  g8 g a16[ g fis e] fis8 d r
}


