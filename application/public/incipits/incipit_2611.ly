 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef bass
  \key f\major
   \tempo "1. Adagio"
  \time 2/2
  a,8 bes c d  c bes c a
  bes a g f  bes a g f
  bes a g f \times 2/3 { e8[ f g]} f8 c'16 d
  c8 bes16 a g4 f
}


\relative c'' {
  \clef bass
  \key f\major	
   \tempo "2. Presto"
  \time 4/4
  \partial 4
  r4 r c,, f r
  r c f a
  bes bes bes a8 g a4 a a g8 f
  g a bes c bes a g f
  e d e f e4 r
}

\relative c'' {
  \clef bass
  \key f\major	
   \tempo "3. Sarabande"
  \time 3/2
  f,,4. g8 a bes g4 a4. bes16 c
  bes4. a8 bes4 c bes2
  a8 bes a4 bes4. a8 g4. f8
  c2 c'
}

\relative c'' {
  \clef bass
  \key f\major	
   \tempo "4. Menuett"
  \time 3/4
    a,2 g4 f2 r4
    a a8 g a f
    c'4 r r
}