 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 3/4
  R2.
  a'4 f cis
  d8. e16 f8 r r8. a,16
  gis8. a32 b a4 r
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 2/2
     a2 \grace cis8 b8 a b cis
     a r e' a e cis \grace e16 d8 cis16 b
     a2
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Siciliano. Adagio"
  \time 6/8
  e8. f16 e8 e4 d8
  c a gis a r r
  f'8. g16 f8 f4 e8  a a, c e
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Tempo di Menuetto con Variazioni"
  \time 3/4
    a2. b2 d8. cis16 cis4 a gis8. a16 b4 r r
}

