\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro assai"
  \time 4/4
    c4 g8. e'16 c4 r
    d g,8. f'16 d4 r
    g1
}
