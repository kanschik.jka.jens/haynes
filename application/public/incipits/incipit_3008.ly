\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria " 
    }
    \vspace #1.5
}

\relative c'' {
                   \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \key g\major
                      \tempo "Andante"
                      \time 3/2
                        r2 g1~ g1.
                        g2 g'1~ g1.~
                        g2 fis4 e d c b4. a8 g2 r
                        % Voice 1
}

\relative c'' {
                    \clef "tenor" 
                     \key g\major
                     \time 3/2
                     \set Staff.instrumentName = #"Tenor"
                        % Voice 2
                       \set Score.skipBars = ##t

                       R1.*12
                       r2 r d,~ d d1~
                       d e4 fis g2 g, r
                  }
                  
