\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  <c, g' c>2 c'4. c8
  c b16 a g f e d c8 e g c
}
