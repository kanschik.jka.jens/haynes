\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran und Bass): Mein Freund ist mein, und ich bin sein [BWV 140/6]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key bes\major
   \tempo "Duett"
   \time 4/4
   \partial 8
  f8 d es es f f4. g8
  f16 bes a g f8 es d4. 
   
   
 } 
 


\relative c'' {
<<
\new Staff { \clef "treble"   
  \set Staff.instrumentName = #"Sopran"
  \key bes\major
   % Voice 1
   \tempo "Duett"
   \time 4/4
   \partial 8
   \set Score.skipBars = ##t
   r8 R1*7 r2 r4 r8 f8  d es es f f4. r8
                     
                  }
   \new Staff { \clef "bass"
   \set Staff.instrumentName = #"Bass"
   \key bes\major
   % Voice 2
   \tempo "Duett"
   \time 4/4
   \partial 8
   \set Score.skipBars = ##t
   r8 R1*8 r2 r4 r8 g,,8 f16 bes a g f8 es d4.
                       
                  }
>>
}


 

