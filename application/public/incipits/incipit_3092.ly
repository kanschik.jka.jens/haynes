\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Quoniam tu solus sanctus [BWV 236/5]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key e\minor
   \tempo "Adagio"
   \time 4/4
   r4 fis32 g a8 g32 fis g16 dis e b c32 d e8 d32 c
   b16 fis g e
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key e\minor
   \tempo "Adagio"
   \time 4/4
   \set Score.skipBars = ##t
    R1*4 r4 fis32 g a8 g32 fis g16 dis e b c8. d16
    b8 a16 g
}



