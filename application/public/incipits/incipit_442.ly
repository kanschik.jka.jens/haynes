
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto Io. "
    }
    \vspace #1.5
}
 
\relative c {
  \clef bass
  \key e\minor
  \tempo "1. Allegro"
  \time 2/4
  e8 e, r8 e'16 fis
g8[ g, g' g] 
g[ g, g' g]
fis8 g16 a b8 b,
e[ fis g e]
 
}

\relative c {
  \clef bass
  \key e\minor
  \tempo "2. Largo"
  \time 3/2
  e2 dis b
e g e
a1 fis2
b4 c b a g fis

 
}

\relative c' {
  \clef bass
  \key e\minor
  \tempo "3. Allegro"
  \time 6/8
  \partial 8
r8 e8 r8 r8 d8 r8 r8
c8 r8 r8 b8 r8 r8
a8 r8 r8 g8 r8 r8
fis8 r8 r8 e8 r8 r8
b'8 r8 r8 b,8 r8 r8
 
}



 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IIo. "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef bass
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
g4 r4 
g4 r4
g8 g g g
g fis r8 d8
e d e c
d d'16 c b8 g
 
 
}

\relative c' {
  \clef bass
  \key g\major
   \tempo "2. Largo"
  \time 3/4
r4 r8 g8 a4
b4. c8 g4
a4. g8 fis4
b4. a8 b4
g e c'
b4. a8 b4
g e g
 
 
}

\relative c' {
  \clef bass
  \key g\major
   \tempo "3. Allegro"
  \time 3/8
c8 g16 fis e d 
c d e fis g8
g, g g 
g4 r8
g16 a b c d e
fis8 e16 fis d8
 
 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IIIo. "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef bass
  \key b\minor
   \tempo "1. Allegro"
  \time 3/4
r4 r4 b4
a16 g a b a8 a a g16 fis
g fis g a g8 g g a16 g
fis2 
 
}

\relative c' {
  \clef bass
  \key b\minor
   \tempo "2. Adagio"
  \time 2/2
d2 fis,
g a 
b fis4. g8
a2 cis,
b e
a,2 r2 a r2
d2
 
}

\relative c' {
  \clef bass
  \key b\minor
   \tempo "3. Allegro"
  \time 2/4
\partial 8

d16 cis
b8[ ais b cis]
d b b, d'16 cis
b8 a g4
fis r4
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerti IVo. "
    }
    \vspace #1.5
}
 
\relative c {
  \clef bass
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
d8[ e fis d]
a'[ b cis a]
d[ e fis e]
b[ a g b]
a[ g fis e] 
d[ fis e d]
cis[ cis d d,] a'4
 
 
}

\relative c {
  \clef bass
  \key d\major
   \tempo "2. Largo"
  \time 6/4
r2 r4 b2 r4
b2 r4 b2 r4 b2 r4 cis2 r4
d4. cis8 b4 fis'2 fis,4 
b2 r4
 
 
}

\relative c {
  \clef bass
  \key d\major
   \tempo "3. Allegro"
  \time 3/8
d8 d d 
g4 d8
g4 d8
g4 d8
g16 fis e8 d
a' g fis 
cis d d, a'


 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto Vo. "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef bass
  \key a\major
   \tempo "1. Allegro"
  \time 2/4
\partial 8
a16 b
cis8[ cis cis cis]
d cis r8 d16 cis
b8 cis16 d e8 e,
a a, r8 
 
}

\relative c' {
  \clef bass
  \key a\major
   \tempo "2. Largo"
  \time 2/2
\partial 4
r4 a4 r4 a4 r4
gis4 r4 gis4 r4
a4 r4 r2
gis4 r4 gis4 r4
fis4 r4 fis4 r4
e4

 
}

\relative c' {
  \clef bass
  \key a\major
   \tempo "3. Allegro"
  \time 3/8
a8 cis a
a e'16 d cis b
a8 cis a
a e'16 d cis b
a8 cis a
fis a fis
d e e, a4 r8
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VIo. "
    }
    \vspace #1.5
}
 
\relative c {
  \clef bass
  \key f\major
   \tempo "1. Allegro"
  \time 2/4
f4 a8. bes16
c8 c, r8 d'16 e
f8 e16 d c8 bes
a g r8 c16 bes
a8 c d, f bes,4 r8
 
}


\relative c {
  \clef bass
  \key f\major
   \tempo "2. Adagio"
  \time 3/4
d4 f d
e a g
f4. e8 d c
bes2. 
a4 a' d,
cis2 cis4 d2 f4 
g a a,
d4 d d
 
}

\relative c {
  \clef bass
  \key f\major
   \tempo "3. Allegro"
  \time 3/8
f8 f f 
bes f a
bes c c,
f a f
bes4 g8
c d a
bes f4
 
}



