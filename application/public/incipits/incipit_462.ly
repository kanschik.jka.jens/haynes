\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria Nr. 1 " 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \key c\minor
                      \tempo "o. Bez."
                      \time 3/2
                      r2 es d c1 b2 c2 g1
                      as2 c1
                      b c2~ c c b
                      c c g'~ g4 f8 es d2. d4 c1.
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key c\minor
                     \set Staff.instrumentName = #"Oboe 2"
                        % Voice 2
                       R1.
                       r2 es d c1.
                       c2 es1 d2 g, g'
                       f8 es d4 d2. d4
                       c2 es d c c2. b4 c1.
                  }
                  
\new Staff { \clef "treble" 
                     \key c\minor
                     \set Staff.instrumentName = #"Tenor"
                        % Voice 2
                       R1.
                       R r
                       r2 r g
                       g1. as2 g2. g4 g1.~ g2 r r R1.
                  }

>>
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria Nr. 3 " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key es\major
   \tempo "o. Bez."
   \time 2/2
   bes8. c16 bes8. as16 g8. f16 es8. es'16
   d8. es16 d8. c16  bes8. as16 g8. c16
   f,4. bes8 es,4 es es4. d8 es4 r
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key es\major
   \tempo "o. Bez."
   \time 2/2
   bes8. c16 bes8. as16 g4 es
   d'8. es16 d8. c16 bes4 g
}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria Nr. 5 " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef bass
  \set Staff.instrumentName = #"Basso"
  \key g\minor
   \tempo "o. Bez."
   \time 4/4
   g,16 fis g a bes8 fis g16 fis g a bes8 fis 
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key g\minor
   \tempo "o. Bez."
   \time 4/4
   g8 a bes fis g a bes a
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria Nr. 7 " 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key bes\major
                     \set Staff.instrumentName = #"Oboe 1"
                     \tempo "o. Bez."
                     \time 3/4
                      R2. R
                      bes2 c4 d4. c8 bes4
                      R2. R R R 
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key bes\major
                       \set Staff.instrumentName = #"Oboe 2"
                        % Voice 2
                     R2. R
                     r4 bes a8 g
                     f4. es8 d4 R2. R R R
                  }
                  
\new Staff { \clef "treble" 
                     \key bes\major
                       \set Staff.instrumentName = #"Tenor"
                        % Voice 2
                      bes'2 c4 d4. c8 bes4
                      R2. R
                      bes2 c4 d4. c8 bes4
                      f'4. g8 f es
                      d2 c4
}

>>
}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria Nr. 9 " 
    }
    \vspace #1.5
}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "o. Bez."
  \time 2/2
    r4 g8 c  b c d es
    d g, es' d c b16 c d8 c
    b16 c a b g g' f g es g f es d8 g,
    c d es f g c,16 d d8. d16
    c4 r r2
}
