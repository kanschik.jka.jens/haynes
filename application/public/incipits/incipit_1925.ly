\version "2.16.1"

#(set-global-staff-size 14)

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Diverses Pieces [36 pieces] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Rondeau"
  \time 2/4
 g4 bes8 g
 d'16 c bes a g8 g'
 fis8 d4 g16 f
 es8 d c bes 
 a d c16 bes a g
 fis8 e16 fis d4
  

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "2. Rondeau. Tendrement"
  \time 3/8
  g16 a \grace a8 bes8 a
  r8 r8 r8 fis16 g \grace g8 a8 g
  r8 r8 r8
  g16 f es d c bes
  a g fis e d8
  c'16 d bes8. c16 a4.
  
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Menuet"
  \time 3/4
  g8 a bes c d4
  bes8 c bes a bes g
  d'4 e8 fis g4
  fis8 e d c bes a

}


