 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
  <d, bes'>2 r4 f'8 d
  bes4 bes r f'8 d
  bes4 bes r bes'8[ d,]
}
