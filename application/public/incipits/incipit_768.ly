\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Ouverture. gay"
  \time 2/2
g'4. g8 g4 a8 bes
fis2. fis8 fis g4. f8 f4. es16 d es4.
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Prelude"
  \time 2/2
\partial 4
a4 cis2 r4 cis
e2 r4 e8 cis
fis2. gis8 a
e2. d8 cis
b2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Prelude"
  \time 2/2
r2 f2
e4. e16 f g2~ g4 f8 e f2
e
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Ouverture"
  \time 2/2
  d4. d8 a'4 g8 fis
  g4. g8 g4 fis8 e
  fis2 fis4. g8
  e2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Ouverture. Gay"
  \time 2/2
  f4. g8 a4 g8 f
  e4. e8 e4 g
  c,4. c8 f4 a
  d, d8 c bes4 bes8 a g4
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor  
   \tempo "1. Prelude. Lentement"
  \time 4/4
  r4 b8. b16 g'4 gis8. gis16
  a4 ais8. ais16 b4. fis8
  b,8 cis dis e fis4.
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata en quatuor " 
    }
    \vspace #1.5
}
\relative c'' {
  \clef treble
  \key b\minor  
   \tempo "1. Gravement"
  \time 4/4
  \partial 2.
  fis2.
  fis4 r  g2
  fis4. fis8 fis e e e e d16 cis d8 fis fis4 e fis1
  
}
