\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  a8. bes16 g8. f16 c'4 r16 c16 d e
  f8 g16 a g8. f16 e8 c r8

}

\relative c''' {
  \clef treble
  \key f\major
   \tempo "2. Allemande. Allegro"
  \time 4/4
 g8. f16 e8. d16 c8 a r8 c8
 d16 a bes c bes d c bes a8 f r8
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 3/4
 c8  a f c' a c
 d bes f d' bes d
 e c g e' c e
 f c a c f,4
 
}

\relative c''' {
  \clef treble
  \key f\major
   \tempo "4. Ciacona"
  \time 3/4
  a4 a g8. f16
  e4 c d
  bes8 a bes d c bes
  a g f c' d e 
  f e f a d, f

}

\relative c' {
  \clef treble
  \key f\major
   \tempo "5. Giga"
  \time 6/8
  f4 f8 c'4 c8
  f8. g16 f8 e4 c8
  d16 e f8 a, bes16 c d8 g,
  a8. g16 f8 g4.
 

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 4/4
  f4. a8 d,4~ d16 f e d
  cis8 a r8 cis8 d a e' a,
  f'4 ~ f16 f e d cis g' f e e8. d16 d4
  

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allemande. Allegro"
  \time 4/4
  d8 a d f e a e a,
  d f e8. d16 cis8 e, a g
  f a d c b g e' d
  cis[ a]
  

 
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Corrente. Allegro"
  \time 3/4
  \partial 8
  r16 d16 d8. a16 d8. f16 e8. d16
  cis8. e16 a,8. g16 f8. e16
  f8. d'16 cis4. d8 d4
 
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Largo"
  \time 3/2
  f2 e d4 e
  cis2 a' g
  f2. e4 f4. g8
  e1 d2

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "5. Vivace"
  \time 3/4
  r4 d4 cis
  d2 a4
  r4 f'4 e f2 e4
  r4 a4 cis, d e f
  e4. f8 d[ e] cis2
  
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "6. Allegro"
  \time 4/4
  d8 a e' a, f' a, g' a,
  a' g16 f e f e d cis d cis b a8 g
  f d a' f  
  

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 3/2
  f2 bes4. c8 bes4. c8
  a2 f c'
  d4. c8 d4. f8 es4. d8
  c1

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 4/4
 \partial 4
 r8 bes8
 bes16 a bes c bes d c bes c bes c d c es d c 
 d c d es f8 es16 d c8 f, r8
 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 3/2
  d1 c2
  d f d
  c a bes4. c8
  c1 bes2
  d g a4. bes8
  e,2
 
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 4/4
  bes8 c d c16 bes f'4 r8 f8
  g g g f16 es f8 bes, bes c16 d
  es8 es es d c4 r8

}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "5. Fuga"
  \time 4/4
  r8 bes16 c d c d bes c8 f4 es8~ 
  es d4 c16 bes a8 bes bes8. a16
  bes8 f'16 g a g a f

}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "6. Largo"
  \time 3/2
  bes2 f' es4 f
  d2 bes f' g4. f8 es4. d8 c4. bes8
  a1


}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "7. Giga. Allegro"
  \time 6/8
  bes16 c d8 bes c16 d es8 c
  d8. c16 bes8 a4 f8
  f'16 es d8 f g16 f es8 g
  a16 g f8 a bes4.
  

}

  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 3/2
  c2 es d 
  es c g'
  as4. g8 f4. es8 d4. c8
  b2

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allemande. Allegro"
  \time 4/4
 es8 c g' d es c r8 g8
 c es d8. c16 b4 r8 d8
 es d r8
 
}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro"
  \time 3/4
 \partial 8
 g8 es g es g d g
 es g es g d g
 es g c, es d c 
 b2
 
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Adagio"
  \time 3/4
  es4. d8 es[ f]
  d2 g4
  c,4. es8 d[ c]
  b2 d4 es4. f8 d4
  es4. g8 d4
  es4. f8 d4 es2

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "5. Giga. Allegro"
  \time 6/8
  c4 d8 es4 d8
  es16 f g8 c, b4 g8
  a16 b c8 g f c' es, 
  d c' b c4.


}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "6. Gavotte. Presto"
  \time 4/4
  c4 g'2 f4~
  f es2 d4~
  ~d c b c8 d d4. c8 c2
  

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
  \set Score.skipBars = ##t
   R1*4 
   bes4 r8 bes32 a g16
   es'4~ es16 es d c
   d d g fis g bes a g fis4 r16 c16 bes a
   bes4 r16
  

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "2. Allemanda. Allegro"
  \time 4/4
 g4 fis g8 d r8 es8
 d c bes a bes  g r8 bes
 \times 2/3 {c8 d c} \times 2/3 {d8 es d} es8 g16 f es d c bes
 a8 bes16 c c8. bes16 bes4 r8
 
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "3. Corrente. Allegro"
  \time 3/4
 \partial 8
 g8 g g, bes d g16 bes a g
 fis8 a d, a d16 c bes a 
 bes8 d g, d' g16 f es d
 es8[ g a, c]
 
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Adagio"
  \time 4/4
  d4~ d16 d c bes a4~ a16 c bes a
  bes8 g r8 bes8 c d es16 d es f
  d8 bes r8

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "5. Allegro"
  \time 3/4
  g4 bes d
  g, g' g
  a a8 bes g a
  fis4 d d 
  es8 g c, es d c
  d f bes, d c bes

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "6. Giga. Allegro"
  \time 6/8
  g16 a bes8 g a d c
  bes8. a16 g8 a d d
  bes d d a d d 
  bes d d a d d 
  

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 3/2
  \partial 1
  r2 r2 
  fis4. e8 fis4. a8 g4. fis8
  g4. fis8 e2 b
  e2. g4 fis4. e8 dis1.
  

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allemanda. Allegro"
  \time 4/4
 \partial 8
 b8 e dis e fis g fis
 r8 b,8
 e dis e fis g fis r8 b,8
 c b r8 a8 b a r8
 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Allegro"
  \time 3/4
  e8 b e g fis e
  fis b fis b a g16 fis
  e8 a e a g fis16 e
  dis8 b' fis b dis, fis
 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Adagio"
  \time 4/4
  e4. fis8 dis e fis32 g a16 g16. fis32
  g8 fis16 e r8 e16 fis g8 a g fis16 e
  dis4 r8

}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "5. Giga"
  \time 6/8
  \partial 8
  b8 e, fis g fis g e 
  dis4. r8  r8 b'8
  e fis g fis g e
  dis4. r8 

}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "6. Gavotta. Allegro"
  \time 4/4
  b4 dis e fis
  g fis8 e fis4 b~
  ~b a2 g4~
  ~g fis8 e dis4 
  

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
 c4~ c16 c b c d8 g, r8 d'8
 e f16 e d8. c16 b8 a16 g
 g'4~
 g16 c, d e f4~ f16 b, c d e4~ e8 f16 e

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Vivace"
  \time 6/8
 r8 c16 d e f g8 e a
 a d, g g c, f
 f8. g16 e d e f d8. c16
 c8 e e
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Corrente"
  \time 3/4
  \partial 4
 r8 c8
 c8. g16 g8. c16 c8. c16
 d8. g,16 g8. d'16 d8. d16
 e8. g,16 g8. e'16 d8. c16
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Vivace"
  \time 12/8
  c8 d c d e d e f e c4 e8
  f g a d, e f e4 r8 r8 g8 g,
  a f' a, b g' f e d c r8

}
