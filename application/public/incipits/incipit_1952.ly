\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  e,8 f g4. f8 e d
  \grace e8 d8 c c2 d8 e
  g f f4. a8 g f
  g4 f e8
}

\relative c'' {
  \clef tenor
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*64 
  r2 \grace g,8 g'2~ g8 f e d
  \grace e16 d8 c c4~ \times 2/3 { c8[ e d]} 
  \times 2/3 { c8[ b a]} g4. a8 g f e d
  \grace e16 d8 c c4 r2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Pastorale. Grazioso"
  \time 6/8
   \partial 8
   g8
   c4 c8 \grace d8 c8 b c
   b4 a8 g a b
   c d e a, f' f
   e4. d8 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. [Solo]"
  \time 3/8
   \set Score.skipBars = ##t

}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. All. ma non troppo"
  \time 2/4
  \partial 8
  c16 d
  e4 f16 e d e
  c8 c c d16 e
  f8 f16 g e8 e16 f
  d8 d
}