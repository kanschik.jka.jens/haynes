 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 3/4
  \partial 8
  f,8
  bes16 d d8~ d16 f f8 f16 bes bes4
}

