\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4	
    \set Score.skipBars = ##t
   R1*32
   d4 d2 e8 c
   \grace c8 b4 b4. c16 b  d c b a
   b a g a g4. g8 a b
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Tutti]"
  \time 4/4\override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  
      c4 r8. \times 2/3 {g32 a b}	 c8. c16 e8. e16
      g4 r8. g16  f8. e16  d8. c16
      b4 g' g
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Tutti]"
  \time 4/4
     \set Score.skipBars = ##t
     R1*6
     c2 b8 c d c
     e2 f8 e d c
     b4. c16 d c4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Rondo"
  \time 6/8
    \partial 8
    d8
    \grace e8 d8 c d  \grace c8 b8 a b
    \grace d8 c8  b c  \grace b8 a8 g a
    \grace c8 b8 a b  \grace a8 g8 fis g
    a8 a16 g fis e d4
}