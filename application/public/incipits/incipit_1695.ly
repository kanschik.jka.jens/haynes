\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegretto"
  \time 2/2
 c4. d8 c d c bes
 a4. bes8 a4 f8 a
 a g f2 e4
 f16 g f g a4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Presto"
  \time 3/8
    f,4 a16 c
    f8 f8 f f4 a16 g
    \grace g8 f8 e f d4. c4 
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. "
  \time 2/4

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. All. moderato"
  \time 2/2
  es4. bes8 es bes g' es bes'4. g8 as f d bes
  es4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. "
  \time 3/4
  
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. "
  \time 2/4

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Moderato"
  \time 2/2
  R1
  \grace a'8
  g4. f8 e4 r
  a2~ a8 f c' a \grace a8 g4. f8 e4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. "
  \time 3/4
  
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. "
  \time 2/4

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. "
  \time 6/8
  \partial 8
  a8
  e'8. fis16 e8
  a16 gis b a gis fis
  e8. fis16 e8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. "
  \time 3/4
  
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. "
  \time 2/4

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegretto"
  \time 4/4
  \partial 4
  \grace c8 b8. c16
  d8. e16 d4~ d8. d16 e8. c16
  d8. e16 d4~ d8. d16 e8. c16
  b8.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. "
  \time 3/4
  
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. "
  \time 2/4

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Moderato"
  \time 2/4
  d'16 a a8~ a16 fis g e
  d4. cis8
  d16. a'32 a4 g8
  fis16. fis32 fis4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. "
  \time 3/4
  
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. "
  \time 2/4

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
