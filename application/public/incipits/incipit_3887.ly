\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                    \set Staff.instrumentName = #"Premier Dessus d'Hautbois"
                     \key d\major
                      \tempo "Doucement"
                      \time 3/4
                      fis4 fis4. g8 a4 b8 a g4
                      fis8 e fis4. g8
                      fis4 e d
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                    \set Staff.instrumentName = #"Second Dessus d'Hautbois"
                     \key d\major
                        % Voice 2
                     d4 d4. e8
                     fis4 g8 fis e4
                     d8 cis d4. e8 d4 cis d
                  }
>>
}
