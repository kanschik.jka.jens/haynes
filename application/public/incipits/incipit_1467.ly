
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisieme Suitte"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key c\major
   \tempo "1. Fanfare. Animé"
  \time 2/2
\partial 2
c8 d c d
c4 g e'8 f e f 
e4 c g'8 a g a
g4 c8 g g4 a8 g
f4 a8 f f4 g8 f
e4
 
} 

 
