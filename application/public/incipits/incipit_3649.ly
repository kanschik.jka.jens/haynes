 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. o.Bez."
  \time 2/4
  a8 a4 a8 
  a8 a4 a8 
  a8 a4 a8 
  a8 a4 a8 
  b8 b4 c16 d
  c b a8 r4
}

\relative c' {
  \clef tenor
  \key c\major
   \tempo "2. o.Bez. [Tutti]"
  \time 4/4
  e16 c f d e a d, b c e f a e c d a'
  c, e f a  c, a' c, a' c,8 b r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*3
   r2 r4 r8 g'8
   c,1 c2 c8 b r4
   g'16 e a f g e f d e c a' f g e f d
   e f g a g a f e e8 d r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro [Tutti]"
  \time 4/4
  \partial 8
  e8 a4 r8 e
  b'4 r8 e,
  c'4 r8 e,
  f16 g f e d c b a gis8 e r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro [Solo]"
  \time 2/4
  \partial 8
  r8
   \set Score.skipBars = ##t
   R2*15
   r4 r8 e
   a a a a
   a a a a
   a g16 f e8 d
   c a r
}