\version "2.16.1"

#(set-global-staff-size 14)

\relative c' {
  \clef treble
  \key f\major
   \tempo "1. Allegretto  F-Dur  3/4"
  \time 3/4
   f8. f16 a8. a16 c8. c16
   c a'8. f4 \times 2/3 { a,8 c f } 
   g4 bes \times 2/3 { g,8 c e }
   f2
}
