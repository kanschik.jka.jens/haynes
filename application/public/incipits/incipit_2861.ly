\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro non molto [Tutti]"
  \time 4/4
  \partial 8
  c,8
  c d d e e f f g
  a4 g r r8 g
  g a a b b c c d
  e4 d r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro non molto [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
      \override TupletBracket #'stencil = ##f
      \partial 8
    r8
   R1*28 	
   c2~ \times 2/3 {c8 b c} \times 2/3 {\grace e8 d8 c b} 
  c2~ \times 2/3 {c8 b c} \times 2/3 {\grace e8 d8 c b} 
  c4 a'
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Larghetto [SOlo]"
  \time 3/8
    e4.~ e8~ e16 \grace d8 c16 \grace b8 a16 a'
    d,8~ d16 f e32 d c b
    c16 b32 a f'16 e32 d c16 b
    a4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Tutti]"
  \time 3/4
  r8 c c,4. c'8
  b d g,4. f'8
  e g c,4. bes'8
  a c fis,4. 
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Solo]"
  \time 3/4
  \set Score.skipBars = ##t
   R2.*16 
    r4 c~ c8. d16
    \grace c8 b4 f'~ f8. g16
    \grace f8 e4 bes'4~ bes8. g16
}