 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Un poco Allegro ma non molto"
  \time 2/2
  bes4. d16 c bes4 bes bes r r2
  d4. f16 es d4 d d r r2
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Un poco Andante"
  \time 3/4
  c8. d32 bes a8 r r4
  bes8. c32 a g8 r r4
  f16 a c f a8 r r4
  f,16 bes d f bes8 r r4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Allegro"
  \time 3/8
  bes4. d f bes
  bes, es g bes
  bes, d f bes
}
