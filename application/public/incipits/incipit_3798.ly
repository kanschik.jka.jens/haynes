 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Ouverture"
  \time 3/4
  r8 d e d e fis
  g4 d g
  fis8 fis g fis g a
  d,4 d4. c16 b
  a4 d c b
}
 
\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. 1.er Rondeau. Gracieusement"
  \time 3/4
  \partial 2
  b4 b
  a g8 a b c
  \grace c8 d4 \grace c8 b4 e
  d c b a
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. 1.er Air"
  \time 2/4
  \partial 4
  g8 d'
  b a16 g c8 b a g a16 g a b c8 b a g
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "6. 1.er Gavotte"
  \time 2/2
  \partial 2
  b4 a g4. d'8 e g fis a
  g4 d g d b e8 d c4. b8 a2
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "8. 1.er Menuet"
  \time 3/4
  g4 b8 d g d
  c b a g fis e d4 fis8 a d c b4 a8 b g4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "10. 1.er Air"
  \time 2/4
  \partial 4
  g8 g c c b b e4 d8 d g fis16 e d8 c b8 g g g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "12. 1.er Sicilienne"
  \time 6/8
  \partial 8
  d8 g4 d8 d8. e16 d8 d4. b4 d8 
  \grace d8 e4 d8 c4 b8 a8. g16 a8 d,4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "14. 1.er Rigaudon"
  \time 2/2
  \partial 4
  g4 g d d b'
  b g g d'
  d e8 fis g4 fis8 g a4
  g8 fis g a g4
}



\relative c'' {
  \clef treble
  \key g\major	
   \tempo "16. 1.er Ariette"
  \time 6/8
  \partial 2
  b8 b4 e8
  e4 a,8 a4 d8
  d4 g,8 g4 c8
  c a g fis d' c
  b g b
}



\relative c'' {
  \clef treble
  \key g\major	
   \tempo "18. 1.er Tembourin"
  \time 2/4
  g4 b8 a g a16 b c8 c
  b d c b a g16 fis g8 d
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "20. Chaconne"
  \time 3/4
  r4 g b a d8 c b a
  b g b c d4
  c b2
}