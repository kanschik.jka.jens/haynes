\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "[Andante sostenuto]"
  \time 3/4
  f4 d8 es f d
  g4 f r8 r16 bes
  g8 bes f4 r8 r16 g
  f8 bes g f es d
  d bes c4
}
