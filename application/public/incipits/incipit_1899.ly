 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
    c8
    f2~ f8 \grace g8 f16 e f8 g
    a \grace bes8 a16 g a8 bes c r d r
    c bes a g g4 g4 g2
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\minor
                      \tempo "2. Adagio"
                      \time 3/4
                      R2. R R
                      a'2.~ a~
                      a4 r32 a gis a gis a gis a
                      \times 2/3 { bes16[ a g] }
                      \times 2/3 { f16[ e d] }
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key d\minor
                        % Voice 2
                      a'4 d, bes'
                      d, cis4. g'16 e
                      d4 cis8 f e g,
                      g16 f bes a a4 r
                  }
>>
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Rondeau. Allegro"
  \time 6/8
  \partial 8
  c8
  f4. \grace { f16[ g]} a8. g16 f8
  c'4. a8 r f
  d'4. bes8 r f
  c'4. a8 r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}