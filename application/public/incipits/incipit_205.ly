\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Partita I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 2/2
  \partial 8
  bes'8
  bes es,4 d8 \grace d8 es4. c8
  bes es4 d8
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Partita II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
e,4. f8 e4. a8
\grace a4 gis4 a8 b c[ b]
}

\pageBreak

\markuplist {
    \fontsize #3
    "Partita III " 
    \vspace #1.5
}


\relative c' {
  \clef treble
  \key as\major
   \tempo "1. o.Bez."
  \time 3/8
  r16 f as c d f,
  e[ d' c b c]
}
