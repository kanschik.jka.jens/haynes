\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro aperto [Tutti]"
  \time 4/4
  c4 c2 b16 c b c
  f4 f2 f4
  dis8 e e2 f16 e f e a4 a2 a4 fis8 g g4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro aperto [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*31
    r8 c8 c8. b32 c b16 c d e   f g a b
    c1~ c~ c~ c
    c8 b16 c d c b a g4 g
    g4. a16 fis g4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio non troppo [Tutti]"
  \time 3/4
  f,4 c a
  bes8. f''16 f4. e16 d
  d c c4 d16 c \grace c16 bes8 a16 bes
  g4 a r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio non troppo [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*10
   f4 c a'
   fis16 g a g g4 r
   fis16 g a g g4. bes8
   gis4 a r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Allegretto"
  \time 2/4
 \partial 4
 g'16 e f d c8 r d8. c32 d 
 e8 r e f
 g a b c
 g4
}