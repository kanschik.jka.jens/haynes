\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key c\minor
    \time 2/2
    \tempo "1. o.Bez."
      r4 r8 g'16 f
      es8 d es f
      d c d es  f es f g
      
     es d es f  g f g as
     f g as g f g as g
}

 