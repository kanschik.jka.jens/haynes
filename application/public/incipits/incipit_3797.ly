 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Ouverture"
  \time 2/2
  e4. d8 c b a b
  g4 e a c
  b4. a8 b c d e
  c4 \grace b8 a4
}
 
\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "2. 1. Rondeau. Gracieusement"
  \time 3/8
  a16 b b8 a16 b
  c b c d e8
  a,8 d4 c8 f e d4 c8
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "4. 1.er Vilagoise. Gayment"
  \time 2/4
  \partial 4
  a8 e'
  c b16 c d c b c a8 b c d e f16 e
}


\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "6. 1.er Badine. Gracieusement"
  \time 6/8
  \partial 4.
  a4 b8 c d c b4 a8
  e'4. g,4 a8 b4 c8 d4 e8
  c4 \grace b8 a8
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "8. 1.er Rigaudon"
  \time 2/2
  \partial 4
  a4 a c c e e2 a,
  d4 c b \grace a8 g4
  a8 g a b a4
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "10. 1.er Muzette Rondeau"
  \time 3/4
  \partial 4
  a4 e'4. d8 e4
  c4 \grace b8 a4 e'
  f e8 d c d e2 a,4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "12. 1.er Bagatelle. Gayment"
  \time 6/8
  \partial 4.
  a4 a8 e4 e8 f8. g16 f8 e4. a4 a8 b4 c8 d4 e8 c4 a8
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "14. 1.er Vielle. Gayment"
  \time 2/4
  \partial 4
  c16 d e8 a, a a a
  a4 c16 d e8 a, f' e d c a
}



\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "16. 1.er Menuet"
  \time 3/4
  c8 d e4 a,
  f'8 a, g a e'4
  d e8 d c b c4 b8 c a4
}



\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "18. 1.er Muzette"
  \time 2/2
  \partial 2
  c8 d e f e4 e, a b c b d d 
  d c8 b b4. a8 a2
}


\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "20. 1.er Tembourin"
  \time 2/4
  a8 e' e d c4 b
  c8 d16 c b a b g
  a8 e e a
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "22. Chaconne"
  \time 3/4
  r4 a e'
  f e8 d c d
  e4 a, d c b8 a b g
  a4
}