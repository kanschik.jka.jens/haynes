\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nürnberger Marsch" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  e8. e16 e8 f16 e d4 r8 g
  c,8. b16 c e d c b8. c16 c4
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Wormser Marsch" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/8
  \partial 8
  g8 c e16. d32 c8 c
  d16 b c4 g8
  a8. f'16 e d c b c4.
}
