\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  d8 d4 \times 2/3 {fis16[ e d]} a'8 a4 \times 2/3 { cis16[ b a]}
  d8 d, r d' d16 e, e b' b a a g
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Andantino"
  \time 3/4
  \partial 4
  a'4
  \grace g4 f2 e4
  g f e
  d8 bes a g f e
  f4 d
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 2/2
  a'4 fis d a
  fis8 a b cis d e fis g
  a4 fis d a
  fis8 a b cis d e fis g
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante staccato"
  \time 3/4
    \partial 16
    g'16
    e8. c16 b8. c16 g8. e'16
    \grace f16 e4 d r8 r16 g,
    a8. a'16 \grace a16 g8. f16 \grace f16 e8. d16

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4
  c4 c c r8 c
  g' g4 f8 e e4 d8
  \grace d16 c8 b16 c  \grace e16 d8 c16 b c g a b c d e f
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Minuet. Andantino grazioso"
  \time 3/8
  e8. d16 c16. e32 \grace e16 d8. c16 b16. d32
  \grace d16 c8. f,16 e16. d32
  e8 c r
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Allegro staccato"
  \time 4/4
    \partial 16
    fis,16
    b8 r16 fis d'8 r16 b cis4 r8 r16 fis,
    cis'8 r16 fis, e'8 r16 cis d4 b16 d fis b

}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Largo"
  \time 3/4
    \partial 16
    fis16
    fis2 r4
    b,4 d fis g fis r8 r16 fis
    fis2 r4

}
  
  \relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Allegro di molto"
  \time 3/4
  fis8[ g fis b,] cis e
  d[ cis b cis d e]
fis8[ g fis b,] cis e
  d[ cis b cis d e]
  }
  


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  g'8 d \grace c16 b8 a16 g d'8 a \grace g16 fis8 e16 d
  g8 d \grace c16 b8 a16 g d'4 r8
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Largo"
  \time 4/4
    \partial 8
    b8
    e16. b32 e16. g32  fis16. e32 dis16. e32 \grace dis 16 c8 b r32 b c b e b c b
    \grace b16 a8 a'16. fis32 \grace e16 dis8. a16 \grace b16 a8 g r16 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Andante"
  \time 3/4
  d4~ d8. b16 e8. d16
  \grace d8 c4 b8. a16 b4
  e8. g16 c,8. e16 a,8. c16
  b8. d16 fis,8. a16 g4
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 16.
  c32 d e
  f8 f f f f4 f16 e a g
  f8 f f f f4 f16 e a g
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Grave"
  \time 4/4
  a'8 f d a bes4 r
  a'8 fis c a g'4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Andante"
  \time 3/4
  f2 e8 f16 g
  bes,4 g8. bes16 a4
  d16 f e d c8 bes a g
  \grace g8 f4~ f8 g16 a bes c d e
}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  d,4 fis a4. g16 fis
  g8 a b cis d d, r fis'
  e16 a, e' a, \grace a'16 g8 fis16 e fis g a8 r
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Grave"
  \time 3/4
  fis8 fis r16 d cis b ais b fis b
  \grace a8 g4 fis r16 fis' b fis
  e8 e32 fis g fis e16 d cis b ais g fis e
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Andante"
  \time 3/8
  d,8 a' d
  \grace d16 cis8. b16 a8
  b \times 2/3 { b'16[ a g]} \times 2/3 { fis16[ e d]}
  \times 2/3 { cis16[ d e]} d8 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Andante"
  \time 3/8

}

