\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Song I: 'But to hear the Children mutter'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "Larghetto"
  \time 6/8
  \partial 8
  g16 a
  bes8. c16 d8 g4 bes,8
  bes a r  r r bes16 d
  g,4 g8 es'16 c a8 g
  g fis r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Song II: 'He's a Man evry Inch'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo ""
  \time 4/4
  r4 r8 bes16 c d8 d16 fis g8 es16 d
  es8 d4 c16. bes32 c8 d16 es d c bes[ a]
  bes4 r8
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Song III: [Gavotte] 'Zeno, Plato, Aristotle'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo ""
  \time 4/4
  \partial 2
  b4 c
  \grace c8 d4. g8
  d c b a
  b a g4 a8 d, b' d, c' b b4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Song IV: 'Gentle Knight'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Lento"
  \time 4/4
  \partial 8
  g16. d32
  b'8 b b16. d32 g16. b,32 b8 a r a16. d,32
  c'8 c c16. a'32 fis16. c32 c8 b r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Song V: 'If that's all you ask' " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo ""
  \time 6/8
  d4. e8 fis g
  d4. c'
  b8 a g fis16 g a8 d,
  c b a
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto VI: 'Let my Dearest be near me' " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo ""
  \time 6/8
  a'4 e16 g f8 e a~
  a d,16 f e d e8 a, d'~
  d c16 bes c a bes8 a d
  d, g16 f e d cis4 r8
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Song VII: 'By the Beer as brown' " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo ""
  \time 2/4
  \partial 8
  fis16 g
  fis8 b, d cis16 b
  ais b cis8 r d16 fis
  e8 fis g fis16 e
  fis e d8 r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto VIII: 'Pigs shall not be' " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo ""
  \time 3/8
  b8 b c d e fis
  fis32 a g fis g8[ c,]
  \grace c8 b4 r8
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Song IX: 'Sure my Stays' " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "Largo"
  \time 4/4
  c4. bes8 as g f es
  d4 r d'4. c8
  b as g f es4 r
  es'4. d8 c bes a g g fis r4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Song X: 'Oh I would not' " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo ""
  \time 3/8
  es4 es32 f g16
  f8 es16 d es[ bes]
  c8 bes as
  g16 as bes8 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto XI: 'Insulting Gipsey' " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "Allegro "
  \time 4/4
  \partial 8
  a8 d4 fis e32 g fis8. r8 e16 d
  cis d e8 a, e' fis16 e d8 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Song XII: 'Oh give me not up to the Law' " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "Largo "
  \time 4/4
  R1
  r2 r4 e~
  e a8 f e \times 2/3 { d16[ c b]} c8 b16. a32
  a4 r r e'8. a16 
  g8 c, b c d g, r4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Song XIII: 'Dragon, Dragon'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo ""
  \time 2/4
  a8 b16 cis  d e fis gis
  a e, fis gis a b cis d
  e a, b cis d e fis gis
  a8 e cis b16 a e'8 e, r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Song XIV: 'Oho Mr Moore'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo ""
  \time 4/4
r2 r4 fis,
b fis8 a g4 r8 fis16 e
a8 g fis e fis4 e8 d
g4 fis8. e16 fis4 b4~
b ais b
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto XV: 'My sweet Honey suckle' " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Allegro"
  \time 2/4
  \partial 16
  d16
  b32 fis g16. r32 d16 e32 g c16. r32 a16
  fis32 d fis a d16[ c] c[ b] r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Song XVI: 'Poor Children Three' " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Grave"
  \time 4/4
  r4 r8 r16 c'
  b4 a g4. r16 c, b4 a
  g4.
}

