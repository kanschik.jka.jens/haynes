 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro"
  \time 2/2
    d4 d d d
    d cis r a
    a' a a g8 f
    g4 g g f8 e
    f4 d
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Siciliano"
  \time 6/8
  c4 c8 c4 f8
  f8. e16 d8 e4 g8
  g8. f16 e8 f4 a8
  a8. g16 f8 g4
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Allegro [Tutti]"
  \time 3/8
  d8 f16 e d8
  d8 f16 e d8
  d8 f16 d a'8
  d,4.
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Allegro [Solo]"
  \time 3/8
     \set Score.skipBars = ##t
   R4.*24 
   a16 a a a a a
   a a e' a, bes a
   a a d a bes a
   a a cis a bes a
}