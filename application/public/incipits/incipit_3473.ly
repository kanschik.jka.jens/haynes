\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\major
   \tempo ""
  \time 4/4
  a4 a8 a d8 d16 d d8 d
  e16 fis e fis g8 a16 g fis8. e32 fis d4
}
