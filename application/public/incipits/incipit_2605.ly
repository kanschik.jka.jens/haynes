 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
        << {
      % Voice "1"
        r4 g'4. a16 bes fis8 g16 a
        d,4 d'4. c16 b c4~
           } \\ {
      % Voice "2"
      d,4 g,8. a16 bes4 a g8 d' f es16 d es4. f16 es
      } >>
 
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro"
  \time 2/2
  d8 g, es' d16 c d8 g, es' d16 c
  d8 c16 bes c8 bes16 a bes8[ g]
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Adagio"
  \time 4/4
  bes2 b4 r
  f'2 e4r
  g2 fis4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Vivace"
  \time 3/8
   r8 d es
   d es16 d c bes c8 a d
   bes g16 a bes c d4.
}