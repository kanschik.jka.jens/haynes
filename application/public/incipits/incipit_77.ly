\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/2
    a'4 a a a   a16[ g fis e] fis8 g
    a4 a a a   a16[ g fis e] fis8 g
    a[ d,] a'16 d, b' d,
    a'8[ d,] a'16 d, b' d,
    a'8[ b16 cis] d8 a a b a b
    e,16[ fis e fis] g8 fis e4 r
}

\relative c'' {
  \clef treble
  \key fis\minor
   \tempo "2. Adagio"
  \time 3/4
       cis4 fis fis16 gis a8
       cis,4 gis' gis16 a b8
       b,8 d cis b4
       b a8 gis a4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 2/4
     a'16 g fis g  a b a b
     a4 r
     a16 g fis g  a b a b
     a4 r     
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Minuet"
  \time 3/4
    d4 cis8 b a g
    fis2 g4
    a b cis16 d e8
    cis4 b8 cis a4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Presto"
  \time 2/2
    b'8 e, e cis'  dis, e r4
    r2 r4 e16 fis gis a
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Largo"
  \time 3/4
    gis'4 fis e8 dis
    dis2 cis4
    fis8 dis bis a' gis fis e2 dis4
 
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "3. Vivace"
  \time 6/8
     e4 r8 dis4 r8
     e gis16 fis e8 fis b b,
     e4 r8 fis4 r8
 
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "4. Minuet"
  \time 3/4
      e4 gis,8 a a8. gis32 a
      b4 gis e
      cis' dis e8 fis
      dis4 cis8 dis b4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Largo"
  \time 3/2
     b2 a4 g fis e
     c'2 b e
     e4 b a2. a4
     a2 fis'4 dis b a
     g2 fis2
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Presto"
  \time 2/2
     b2 cis
     b4 e e dis
     gis2 a gis r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Grave"
  \time 4/4
    c4. c8 d8. f32 e  d8. c32 d
    e8 g, c e, e'4. e8
    e d r d d c r
 
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Vivace"
  \time 3/8
    r16 b' g e  fis e32 dis
    e16 e, b' g a g32 fis g16
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "5. Minue"
  \time 3/4
    b4 e2~ e4 dis2
    e8 fis g4 fis e r r
}

  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/2
     d4 e d16[ c b a] g a b c
     b4 c b r
     e16 c b c  e8[ g]  d16 b a b d8[ g]
     c, g c e d g, r4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Siciliano"
  \time 12/8
     d4 g8  d8. c16 d8 e8. d16 e8 d4 r8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Vivace"
  \time 2/4
     d8 g4 fis8
     g fis16 e d8 e
     d b c a
     b a16 b  g a b c	
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Minuet"
  \time 3/4
    b4 a2
    g8 a b c d4
    g, c b
    a8 g fis e d4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Adagio"
  \time 2/2
      fis8 fis b b
      fis4 r8 fis
      e[ e] e d16 cis  d cis b8 r fis'
      g g e g  g4 r8
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Allegro"
  \time 2/2
     r8 b b d  cis fis, r4
     r8 cis' cis fis d b d fis
     b4 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Largo"
  \time 3/2
     a'2 d,4 e fis g
     a fis d1
     b2 cis d g4 e fis2 r
}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "4. Presto"
  \time 2/2
    fis4 fis fis b,8 cis 
    d2. cis4
    cis cis cis d
    e2 d4 cis
    b ais b
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "5. Menuet"
  \time 3/4
    b8 cis d4 cis
    b fis'2
    e4 d cis
    d8 cis d cis b4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Vivement"
  \time 2/2
    e8 e e e   e16[ d cis d] e8 a,
    r4 d8 cis  cis16[ b a b] cis8 a'
    fis a e a  d,4 cis
}

\relative c'' {
  \clef treble
  \key fis\minor
   \tempo "2. Adagio"
  \time 2/2
     cis8[ fis,] fis16 a gis fis eis8. dis16 cis[ dis eis fis]
     gis8 gis gis16[ b a gis]
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Vivace"
  \time 3/4
     a8 a a a a a 
     b e, e e e e
     b' b b b b b
     cis a a a a a 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Mennuet"
  \time 3/4
    a4 cis e
    gis,2 a4
    a d fis d2 cis4
}
