 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "1."
  \time 2/2
  c2 d e4 fis g16 fis e8 d[ b]
  d4. fis8 g2
  fis4 r
}


\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "2."
  \time 4/4
   e2~ e4.~ e16 gis,
   a8 c b8. a16 gis8 b e b
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. "
  \time 2/2
  r2 e8 d16 c d8[ d]
  e8 d16 c d8[ d] e8 f16 e d8.[ d16]
  c4 r
}
