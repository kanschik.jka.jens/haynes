\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. o.Bez."
  \time 2/4
  a8[ \grace { a16[ b]} c8] c16 b a gis
  a8[ \grace { c16[ e]} f8] f16 e e d
}
