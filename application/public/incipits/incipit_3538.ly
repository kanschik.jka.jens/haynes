\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key g\major
    \time 2/2
    \tempo "1. Alla breve Andante"
      g'4 d8. b16  \grace d8 c4 b8. a16
      b8 g b d g d g b
}

 