 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4  
  \partial 4
    c8 e
    a g g2 f8 e
    d c c4. c' g e
    e d d4. g8 f d
    dis4 e r8
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2.Andante"
  \time 3/4  
    f4 r16 a,32 c  a16[ a] a a32 c a16[ a]
    r bes32 d bes16[ bes] r bes32 d bes16[ bes]
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo"
  \time 4/4 
  \partial 2
  g'4 g
  \grace a8 g4 f8 e
  a b c a
  \grace a8 g4. e8 \grace g8 f4. d8
}
