\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 3/8
  \partial 8
  g8
  e'4.
  d16 a' g fis g b,
  c4. b16 f' e dis e g, a4.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 2/4
 \partial 16
 g'16
 g4~ g8. e16 e4~ e8. c16
 c4 r8 g
 a a' a, a'
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/4
   c2 \grace f16 e8 d16 c
   g'4 g, r8 r16 g'
   g8 f f4 r8 r16 f
   f8 e e4
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 2/4
  \partial 8
  a16. f32
  c'8 c~ c16 a f f'
  d8 d~ d32 g fis g bes16 d,
  c8 c
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
   c8 f f f f
   f4~ \times 2/3 { f16[ g a]} \times 2/3 { bes16[ a g]}
   f8 f f f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 3/4
   f8 c c c c c
   c4. d8 c4
   g'8 bes, bes bes bes bes
   bes4. c8 bes4
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 2/4
  \partial 8
  b16. g32 d'8 d4 fis16. d32 g8 g, r g'16 b,
  b a a8 a16. b32 b16. c32
  c4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
   d8 d4 c8
   b16 a' g fis g d e c
   b8 b4 a8
   g16 e' d cis d g, b g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
   g'4 fis g d2.
   e4 c fis
   \grace fis4 g2 g,4
}

 
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 3/4
  \partial 4
  bes4 bes4. c8 as4 as4. bes8. g4
  \grace bes8 as4 g f
  es8 g bes es g es,
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 2/4
   bes'4~ bes16 as g f
   es8 es es es
   f16 d es8 es es
   
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro"
  \time 3/4
 \partial 4
   bes4 g' bes8 g bes g
   f bes bes4. d,8
   es4 g8 es g es
   d g g4.
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  d8
  a'16. b32 a8~ a16. b32 a8~ a16. a32 g8~ g16. g32 fis8~
  fis e4 d8 \times 2/3 { cis16[ d e]} d8 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 2/4
 \partial 8
   a'8
   fis d
   \times 2/3 { e16[ fis g]} \times 2/3 { a16[ g a]}
   d,8 b
   \times 2/3 { cis16[ d e]} \times 2/3 { fis16[ e fis]}  
   b,8 g
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 3/4
   d2. e fis2 r16 a a8
   fis4 d e
   \grace e4 fis2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 2/4
   g'8 g~ g16. a32 d,16. f32
   e8 e~ e16. f32 b,16. d32
   c8 c~ c16. g'32 e16. bes32
   bes16 a a8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 2/4
   c4 d
   e16 g c b c b c g
   a8 a4 c16 a
   a8 g r16
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Pastorale. Andante"
  \time 6/8
   e4 f8 d4 g8
   c, g b c4 d8
   e4 f8 d4 g8
   c,8 g b c g b
}

