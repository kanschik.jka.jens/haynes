\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 4
  bes4
  es4. g8 f4. as8 g as bes2 c4
  bes8 as g bes as g f as
}

