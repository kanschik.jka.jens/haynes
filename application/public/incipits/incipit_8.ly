\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. o.Bez."
  \time 4/4
\partial 8
bes8
es es, g16 g f es  c'8 bes4 es8
}
