 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Amabile"
  \time 3/8
  a8. b16 a8
  e' a \grace g8 f
  \grace e4 dis4.~ dis4
}
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro non tanto"
  \time 4/4
    e16 e e e  e e e  e e16. a32 e8 r d
    c16. b64 c d16 b    a16. gis64 a b16 gis
    gis a e a   a b e, b'
}
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro alla Siciliano"
  \time 6/8
    e4 e8 e4 e8
    e8. d16 c8 c4 b8
    c8. b16 a8  c8. b16 a8
    gis a b
    e,4
}
 