\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Minuet [I] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   
  \time 3/4
  es,8 f g as bes es
  \times 2/3 { d8[ c d] } \grace d8 es4 r
  bes8 c d es f as
  g4 f r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Minuet [II] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major

  \time 3/4
  f2. bes,
  \times 2/3 { c8[ d es]} \times 2/3 { es8[ f g]} f8 es
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Minuet [III] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
    \time 3/4
    d8 c bes a g fis
    g4 d d'
    c8 bes a bes c d
    bes4. a8 g4
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Minuet [IV] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
  \time 3/4
  e8 b \grace cis8 b4 cis8 b a2 gis4
  \times 2/3 { a8[ b cis]} b8 a gis fis
  \times 2/3 { gis8[ fis e]} 
  \times 2/3 { fis8[ e dis]}  e4
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Minuet [V] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major

  \time 3/4
  a'4 gis2 
  \times 2/3 { a8[ gis fis]} e4 \times 2/3 { cis8[ d e]}
  \times 2/3 { fis8[ a gis]}
  \times 2/3 { fis8[ e d] }
  \times 2/3 { cis8[ b a]}
  \grace cis8 b2
}

