 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

   \tempo "1. Moderato"
   \time 2/2
      g'8[ d]  bes a16 g d'4 r
      c8 c c c  c bes r d
      f16 g as4 as8 g f es d
      es4 r8
}


\relative c'' {
  \clef treble
  \key g\minor
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

   \tempo "2. Allegro"
   \time 2/2
   \partial 8
     d8
     d16[ c bes a] g8 es' d16[ c bes a] g8 es'
     d c16 bes a8[ g] fis[ e16 fis] d8

}
\relative c'' {
  \clef treble
  \key g\minor
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

   \tempo "3. Gigue"
   \time 6/8
    \partial 8
    d8 g4. r8 r g 
    a4. r8 r a
    bes4. r8 r bes
    a4.
}
\relative c'' {
  \clef treble
  \key g\minor
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

   \tempo "4. Menuet"
   \time 4/4
       g'4 d c
       bes a2 g4 d d'
       bes8 a g bes a fis
       g4.
}
