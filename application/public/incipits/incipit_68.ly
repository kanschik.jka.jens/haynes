 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key g\minor
                      \tempo "1. Adagio"
                      \time 4/4
                        r4 g'4. a16 bes fis8 g16 a
                        d,4 d'~ d8 c16 b c4
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key g\minor
                        % Voice 2
                       d,4 g,8. a16 bes4 a
                       g8 d' f es16 d es4. f16 es
                  }
>>
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro"
  \time 4/4
  d8 g, es' d16 c d8 g, es' d16 c
  d8 c16 bes c8 bes16 a bes8 g d' e
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key es\major
                      \tempo "3. Adagio"
                      \time 4/4
                       g2 f4 r b2 c4 r
                       cis2 c
                       % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key es\major
                        % Voice 2
                        bes2 bes4 r
                        f'2 e4
                        g2 fis8 g a4
                  }
>>
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Allegro"
  \time 3/8
    r8 d es
    d es16 d c bes
    c8 a d
    bes g16 a bes c
}