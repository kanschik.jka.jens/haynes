
\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Alcione - Act I Scene III. Le flambeau de l'amour" 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "hautbois seul - Le grand Prestre"
  \time 3/4
  \partial 2
  b8[ g] a d,
  d2 cis8. b32 cis
  d4 a' b8 g
  g4 fis4. e16 fis
  g4

}
