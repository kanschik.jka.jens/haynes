 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro"
  \time 4/4
    d4 a8 d e4 a,8 a
    fis' fis fis e16 fis g4 d8 d
    a' a a g16 a bes8 a16 bes g8 a
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Largo"
  \time 3/4
    r4 c8 a d c
    bes' a a g g f
    f4 r r
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Allegro"
  \time 3/4
    r8 d e d16 cis d8 e
    f f g f16 e f8 g
    a a, bes a16 g a8 d
    cis b16 a d8 cis d e
    f4 r r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allergo"
  \time 2/4
    a'16 g f g a8 bes
    a16 g f g a8 bes
    a8 bes16 a g8 f
    e16 d c d  e f g f
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "5. Andante"
  \time 3/4
  a'8. bes16 a4 g
  f16 e d8 e4 r
  g16 f e8 f4 r
  a8. bes16 a4 g
  f16 e d8 e4 r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "6. Allergo"
  \time 3/4
    f4 e8 d c bes
    a4 bes g f r g'
    f g e
    f16 g a8 a a a a
}
