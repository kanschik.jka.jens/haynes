\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 2/2
    c2 e g c e c a fis g1
}
  
