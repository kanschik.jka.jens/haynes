\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  g4 es8 es' d4 es
  es, c8 c' b4 c
  c, as8 as' g4 as
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*22 
    g'4 as \grace as4 g2
    r8 es g f \grace f4 es2
    c4 \grace f8 es8 d \grace d4 c2
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio"
  \time 4/4
  g'2~ g8 es d c
  \grace d8 c8 b b4 r2
  f'2~ f8 d c b~
  b f' f4 es2
  es4 d
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. A Tempo primo"
  \time 4/4
  \partial 8
  es8
  es es, es4 \times 2/3 { es8[ f g]}  \times 2/3 { f8[ g as]}
  \grace as8 g4 g2 es'4
  es8 f, f4 \times 2/3 { f8[ g as]}  \times 2/3 { g8[ as bes]}
  \grace bes8 as4 as2
}