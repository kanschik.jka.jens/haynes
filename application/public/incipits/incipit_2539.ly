 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  bes16 c d es  f8 f  f bes f bes
  f16 bes a bes  f es d c  d es d c  bes c d bes
  c d es d c d es d  c d es d  c bes c d
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Larghetto"
   \time 3/4
   d8 es16 d  d8 es16 d c8 bes16 a
   bes8 fis g2
   g'8 as16 g g8 as16 g f8 es16 d
   es8 b c2
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Presto"
  \time 3/4
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \times 2/3 { bes8 a bes d c d   f es f 
  bes a bes  f es f  d c d
  bes a bes  d c d } f8. f16
  f4 es r
}