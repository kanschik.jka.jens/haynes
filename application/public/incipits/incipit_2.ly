\version "2.16.1"

#(set-global-staff-size 14)

\relative c' {
  \clef alto
  \key bes\major
   \tempo "1. ohne Satzbezeichnung  B-Dur  4/4"
  \time 4/4
   \partial 4
   bes4
   f'2 g8 f es d
   d2 es8 d c bes 
   bes bes' bes2 a8 g
   g f e f f4 r8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Rondo  B-Dur  3/4"
  \time 3/4
   d2 \grace d8 c8 bes16 c
   bes4 f' es
   d8 bes' a g f g
   c,4. d16 es d4
}

