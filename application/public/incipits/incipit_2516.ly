\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. o.Bez."
  \time 4/4
  b16 cis d e  fis fis fis fis  
  g fis fis fis b fis fis fis
  g fis fis fis b fis fis fis
  g fis e d e d cis b
  ais8 fis r8
  
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 3/4
  r4 a4. b16 c
  b4 b4. c16 d
  a4 d8 cis d a b2.
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Allegro"
  \time 3/4
    b'4 fis g e2 fis4
    fis, b cis
    ais2 b4
}
