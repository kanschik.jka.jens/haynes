\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 3/4
    g'2 e4 f d r
    e c d \grace c8 b2 r4
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. o.Bez."
  \time 2/2
    f1
    c2 \grace { d32[ e f g a]} bes2
    a4 bes 
      \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \times 2/3 { bes8[ a g]}
  \times 2/3 { g8 f e}
  a4 bes
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. o.Bez."
  \time 6/8
  \partial 8
  e16 f
  g4 g8 g c f,
  e4. d4 e8
  f4 f8 f g e
  d4. c8 r
}
