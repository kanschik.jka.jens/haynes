\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. o.Bez."
  \time 4/4
  d4. a8 f4 e8. d16
  e'2 r8 e f cis
  d4. d8
}
