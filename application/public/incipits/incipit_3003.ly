\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Larghetto"
  \time 3/4
  g4 c4. c8 c b b4 r
  g8 a bes4. bes8 bes a a4 r
}



