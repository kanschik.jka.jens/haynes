\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  a4~ a16 bes c d  g,4~ g16 a bes c
  f,8 f'16 d bes4~ bes16 g' e c bes8 a16 bes
  a8 g16 f r4
}
