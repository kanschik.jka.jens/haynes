\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "1r. Divertissement " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Ouverture. Fierement"
  \time 2/2
  \partial 2
 r16 g16 a b c d e f 
  g4. g8 c4. c8
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  b a g f e4. \times 2/3 {e16 f g}
  a4. \times 2/3 {a16 g f} e4. d8
  c d e f g4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondeau. gracieusement"
  \time 3/4
  \partial 4
  c8 d
  \grace d8 e4. f8 e d
  e4 \grace d8 c4 g'
  g f8 e d c 
  b4 \grace a8 g4 g'
  a8 bes a g a f
  g4 a8 b c4


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Premier Gigue"
  \time 6/8
  \partial 4.
  e4 f8
  g4 c8 b c d
  d,4 f8 e4 c'8
  g4 f8 e4 d8
  e4 \grace d8 c8

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Premier Menuet"
  \time 3/4
  e8 f g4 c,
  b2 c4
  d c d
  e2 d4
  e8 f g4 c,
  a2 g4 a a4. g8 g2.

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Musette en Rondeau. nonchalament"
  \time 2/2
  \partial 2
  e4 d8 c
  g'4 a8 b c e, f d
  e4 d f8 e d c
  b4 c d e 
  d2 

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "6. Premier Rigaudon. gaiement"
  \time 2/2
  \partial 2
  c4 g
  a g8 f e4 f
  g c, d8 e f e
  d c b c d g f g
  e4 \grace d8 c4

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe. Divertissement " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "1. Fierement. Entrée"
  \time 2/2
  \partial 2
  c4. g8 
  as4. g8 f as g f 
  es4 \grace d8 c4 as' g
   \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  c4. \times 2/3 {c16 d es} d4. c8
  b4 \grace a8 g4
  

}

La Bechamel

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIIe. Divertissement - La Bechamel " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. gracieusement"
  \time 2/2
  \partial 2
  e8 d c b
  c4 d e4. f8
  e4 d g8 f e d
  f e d c b4. c8
  d4 g,
  
  

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    " IVe. Divertissement" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Premier Rondeau. nonchalament"
  \time 2/2
  \partial 2
  g4 d'8 c
  b a b c d c b a
  b4 \grace a8 g4 d' g8 fis
  e d e fis g a fis g
  a4 d, g,

}


