 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Alleg."
  \time 4/4
  d8 g,16 fis g a b c
  d8 g4 d8
  e d c g'
  b, g'4 d8
  e d c g' b,4 r8
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Largo [Solo]"
  \time 3/4
  d8. d16 d8. d16 d8. d16
  es8. g16 bes8. g16 es8. g16
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Largo [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*8 
   d8. g16 f8. es16 es8. d16
   d8. g16 f8. es16 es8. d16
   d4. d8 es c a8. es'16 d4. c8 bes4 g r
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. o.Bez. [Tutti]"
  \time 3/8
  b'8 a d, b' a d, b' c d, b'4 a8
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. o.Bez. [Solo]"
  \time 3/8
     \set Score.skipBars = ##t
   R4.*14
   g8 d'4 g8 fis4 g8. fis16 e8 d8 a'8. c,16 b8. c16 d8
}

