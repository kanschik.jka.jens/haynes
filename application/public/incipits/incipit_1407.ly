\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 2/2
  d2.~ d16 c d a
  bes8[ g~]  g16 bes c bes a[ g a8~] a16 a bes a
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 4/4
  r8 g bes c  d16 c bes8 es4
  d es d16 c bes8 a16 g g8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 3/4
  bes4 d16 es f8  es16 f d8
  c4 c16 d es8  d16 es c8
}


\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key g\minor
                     \tempo "4. Vivace"
                     \time 3/4
                      % Voice 1
                           \set Score.skipBars = ##t
   R2.*1 
  r8 f,16 g  a bes a bes  c bes c a
  bes8 g bes4 r
}
\new Staff { \clef "treble" 
                     \key g\minor
                        % Voice 2
                        r8 d,8 g d bes' g
                        a d, d'4 r
                        r8 d,8 g d bes' g
                        a d, d'4 r
                        
                  }
>>
}
