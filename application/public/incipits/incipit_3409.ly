\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 4/4
  a8. b32 cis d8 d d16 cis d8 a16 d fis a
    \override TupletBracket #'stencil = ##f

a8 g \times 2/3 { fis16[ g e]}   \times 2/3 {d16 e cis}
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Andante"
  \time 3/8

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Presto"
  \time 2/4

}
