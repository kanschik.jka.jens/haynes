\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante alla Pastoralle [Tutti]"
  \time 2/4
  \partial 8
  g8
  c4 \grace d16 c16 b c d
  e4 \grace f16 e16 d e f 
  g4 e16 c d b
  c4 g8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante alla Pastoralle [Solo]"
  \time 2/4
  \partial 8
  r8
   \set Score.skipBars = ##t
   R2*34
   r4 r8 g
  c4 \grace d16 c16 b c d
   e4 e16 c d b c4 g8
 
}

