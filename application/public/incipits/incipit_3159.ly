\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Lass uns, o höchster Gott, das Jahr vollbringen [BWV 41/2]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\major
                      \tempo "o. Bez."
                      \time 6/8
                      \partial 8
                      % Voice 1
                  d8 d b c d e fis
                  g fis e d a' c,
                  b4. a4 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\major
                     \set Score.skipBars = ##t
                        % Voice 2
                     b8 b g a b c d e d c d4 a8
                     g4. fis4
                       
                        
                
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 3"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\major
                     \set Score.skipBars = ##t
                        % Voice 3
                     g8 g4 d8 g4 a8
                     b a g a4 fis8
                     d4. d4
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key g\major
   \tempo "o. Bez."
  \time 6/8
  \partial 8
     \set Score.skipBars = ##t
    r8 R2.*14 r4 r8 r4 d8 
    d b c d e fis
    g fis e d a' c, b4. a4
    
    
    }