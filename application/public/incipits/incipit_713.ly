
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key bes\major
  \tempo "1. Allegro "
  \time 2/2
  f8 bes,16 c d8 bes bes4 es8 d f c4 a8 f4. d'8 bes g4 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio Soli"
  \time 3/2
 \set Score.skipBars = ##t
   R1.*1 d2 d2. c4 bes2 a r2 d2 d2. c4 bes2 a fis g1
 
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Vivace"
  \time 3/4
 d4 bes f f c' f, f' d bes a c a bes f bes bes2
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/2
 g8 e16 f g8 a g e16 f g8 c a g a g16 f g a g f e8 f g8
 
 
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Largo e Cantabile"
  \time 12/8
 e8. f16 e8 e b e c4. gis e'8. f16 e8 e b e c8. d16 e8 e4 d8 e4.

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Giga Vivace"
  \time 6/8
 c4. d e8 c e f d f g4. a g8 e g f d f e f g g f e d4.
 
 
 
}


