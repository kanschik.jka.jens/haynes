
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "II.eme Divertissement"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key g\major
   \tempo "1. Le Marquis de Nelle. Rodeau. Gratieusement"
  \time 3/4
\partial 4
b8 a
c4~ c8 b a g
a4 g b
\grace {b16[ c]} d2.~ d4 c8[ b] a g
g[ fis] e d r8

}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Mineur"
  \time 3/4
\partial 4
bes8 c
d4~ d8 bes a g
\grace {c16[ d]} es4 d r8 g,8
a4 c8 a g fis
g4 d


 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Menuet"
  \time 3/4
\times 2/3 {g8 fis e} d2
\times 2/3 {e8 d c} b2
\times 2/3 {c8 b a} \times 2/3 {e'8 d c} \times 2/3 {b8 a g}
g4 fis g

 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "4. Tambourin"
  \time 2/4
g4 \grace e8 d8 c16 b
\grace {c16[ d]} e4 \grace d8 c8 b16 a
d8 g, a g16 fis
g8 b d g
b4 \grace a8 g8 fis16 e a4

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Premier Rondeau"
  \time 3/4
\partial 4
b4 b8[ a] c b a g
a4 g d'
e8 d c b a g
\grace b4 a4 d, 
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "6. 2e. Menuette Mineur"
  \time 3/4
\partial 4
bes8 c
d es d bes a g
\grace {c16[ d]} es4 d bes8 g
a c bes a g fis
g4 d
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "7. Contre-danse"
  \time 6/8
\partial 4.
b8. c16 b8
a4 g8 e'4 e8
d4. e8. fis16 g8
d4 d8 c8. b16 a8
b4 g8

 
}






 

 
