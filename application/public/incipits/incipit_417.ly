 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Les plus jolis Mots" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. Introduction. Maestoso"
  \time 4/4
    c2 c4. c8 c2 r8 c g' f
    e g bes as  g f es des
    c2. r4
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Romance. Andante"
  \time 2/4
  \partial 16*3
  c16 \grace e8 d16. c32
  a'8. a16
  a8 g16 r32 g
  g8 f r16 f e d
  c8 f e f16 g f4

}
