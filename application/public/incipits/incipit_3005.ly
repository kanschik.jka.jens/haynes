 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 3/2
  r2 a d
  cis d2. d4
  e d d2 r4 d
  g fis fis2. e4
  e2
}

