\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 4/4
    d2~ d8 c16 d32 e d8 c
    b4 r8 b16. c32 d8 e r16 d g16. e32
    c8[ b]
}
