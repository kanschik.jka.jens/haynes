
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Suite"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key c\major
   \tempo "1. Menuet"
  \time 3/4
c4 \times 2/3 {e8 f g} \times 2/3 {d8 e f}
e4 g c
g, \times 2/3 {d'8 e f} \times 2/3 {f8 e d}
e4 d c
 
}

\relative c'' {
\clef "treble"   
  \key c\major
   \tempo "2. Air. Gracieux"
  \time 3/4
g8. c16 c4 b
\times 2/3 {c8 d e} e4 d
\times 2/3 {g8 e c} c4 b
\times 2/3 { c8 d e} e4 d
\grace f8 \times 2/3 {e8 d c} b4 a

 
}

\relative c'' {
\clef "treble"   
  \key c\major
   \tempo "3. Menuet"
  \time 3/4
c8.[ g'16 e8. c16 d8. b16]
c8.[ g'16 e8. c16 d8. b16] c8. d16 e4 d 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxiéme Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Rondeau Gracieux"
  \time 3/4
b2 \grace c8 c4 \grace {b16[ c]} 
d2 \grace {e16[ fis]} g4
g fis8 e d c
c b a d, g b
a c b d fis, a

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Menuet"
  \time 3/4
g2.
a
b4 c d
e fis g
d c b
\grace {c16[ d]} e2 d4
 
}






