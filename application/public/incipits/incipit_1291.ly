\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. o.Bez."
  \time 4/4
  r8 f,16 g  a8 b \grace b4 c4. c8
  d16 e f[ e32 d]
}
