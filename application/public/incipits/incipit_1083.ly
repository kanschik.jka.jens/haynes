\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro"
  \time 2/4
    e4 a \grace a8 gis4 r8 a
    f e4 d8 c16 d c b a4
 
} 
