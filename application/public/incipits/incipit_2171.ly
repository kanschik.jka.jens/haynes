 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Musette"
  \time 3/4
  \partial 4
    a8 d
    d4 cis8 d a fis'
    fis4 e8 d fis16 e d cis
    d4 a8 r
}

