\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato"
  \time 2/2
    \partial 16
    f,16
    bes4 d f r8. d16
    es4 c a r8. f16
    bes4. a8 \grace a8 g4 es'8 d
    d c d bes  bes a g f
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio"
  \time 3/4
  \partial 4
  r16 es, g bes
  es4. d8 c bes
  bes16 c g as as4 r
  f' f16 g f g  as f d bes
  as bes g as g8 es' es es
  \grace d8 c4 r8
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Rondo. Allegro"
  \time 6/8
\partial 8
 f,8
 bes4 d8 f4 g8
 f es d c r f
 es d c bes r c
 f,4. r4
}