 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio ma non troppo"
  \time 3/4
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
   r8 c f a  \grace g16 f8 \grace e16 d8
   \grace c8 bes4~ bes16 d c bes  bes' g f e
   d8 c~ c16 g' f e d c g' bes,
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro non tanto"
  \time 4/4
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
  f4~ f16. c32 d[ bes f' d] d8 c a'4~
  a16 d, d bes' \grace a8 g8. f16 \times 2/3 { e16 d c} c8 r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegretto"
  \time 6/8
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
   a'4.~ a16 f e f c' a
   g4 c,8 c4 d16 e
   f  f e f g f b f e f g f f4. e4 r8
}