\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Quia respexit humilitatem [BWV 243a/3]" 
    }
    \vspace #1.5


}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    r8 es16. d32 f16. es32 d16. c32 b8 c16. d32 f,4~ 
    f16[ es' d c b as g f] e f g as bes4~bes16
  
  
   

}


}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key c\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
R1*5 es4 f16 es d c b c d8 f,4 r8
d'8 c16 b as g f8 es es4 r8
  
   

}