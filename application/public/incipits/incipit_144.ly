\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 2/4
c8 g16 g g8 g
c8. d16 d8. c32 d
e8 f16 e d16. f32 e16 f32 g
e8 d16 e c4
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 2/2
  \partial 8
  e8
  e16 d d c c[ e d c] b[ e e d] d e d e
  f[ d d c]
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/8
    c8 g g
    a16 g f a d a 
    b a g b e b
    c b a c f c
    d c b d g f
    e4 r8
}
