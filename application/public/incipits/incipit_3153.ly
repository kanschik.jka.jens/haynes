\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor + Bass): Gott, der du die Liebe heisst [BWV 33/5]" 
    }
    \vspace #1.5
}
 
 
\relative c''' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key e\minor
                      \tempo "Duetto"
                      \time 3/4
                      % Voice 1
                  g2 a4
                  fis2 g4
                  e fis8 g a fis
                  \grace e8 dis4 e8 fis b,4~ b
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                    b2 c4
                    a2 b4 g a8 b c a
                    fis2 r4 
                        
                
                  }
>>
}


\relative c''' {
	<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Tenor"
                     \key e\minor
                      \tempo "Duetto"
                      \time 3/4
                      % Voice 1
                  \set Score.skipBars = ##t
                     R2.*16 g2 a4
                     fis2 g4
                     e fis8 g a fis
                      dis2 r4
                      
                     
                  }
\new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Bass"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                    R2.*16 b,,2 c4
                    a2 b4
                    g a8 b c a fis2 r4
                        
                
                  }
>>
}

	
	
