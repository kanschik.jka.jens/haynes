 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 2/4
  c4 b16 c d b
  c4 g8 g
  c e a, f'
  e4 d8 r
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Polonaise"
  \time 3/4
  g4. e'16 d32 e c8 c
  e32 f16. e32 f16. d4. b8
  g4. e'16 d32 e c8 c
  d16 g fis g  a g fis g g,8 r
}
