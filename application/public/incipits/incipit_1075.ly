\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. "
  \time 2/4
  a'8 a16. a32 a8 a
  a16 g32 fis fis4 d8
  b' g16. a32 b8 cis
  d16 a a4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*3 
  
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Tutti]"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Solo]"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 4/4

}