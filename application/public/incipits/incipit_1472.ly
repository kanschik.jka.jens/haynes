 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude"
  \time 3/4
    r8 c e c e c
    g c e c e c
    g e' g e g e 
    c e g e g e
    c g' g2~ g2.
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. 1.ere Marche"
  \time 2/2
  \partial 2
  c4 g'
  g4. f8 e f g4
  c, g c d8 c16 d
  e4 d8 e c d e c d4 g,
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Sarabande"
  \time 3/4
   es4 \grace d8 c4. g'8
   g4. f8 es4 d f8 es d c
   b4. a8 g4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Marche due Retour"
  \time 2/2
  \partial 2
    g'4 f
    e2 d4 c
    f2 e d g4 f
    e2 d4 c d2 b c
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "5. Les Complimens. Gravement"
  \time 6/4
  \partial 2.
  c2. \grace { b32[ a]}
  g2. c4 b c
  d c d e2.
}

