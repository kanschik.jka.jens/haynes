\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
      f,8
      c'8. c16  \grace bes16 a16 g a bes  c8. c16 \grace bes16 a16 g a bes
      c8. f16  a8 f bes8. g16
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andante"
  \time 2/4
  \partial 8
    r8 c,16 d32 e  f g a b c8 c,
    b16 d c c  b d c c
    b d c8
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro assai"
  \time 3/8
    f,4. c' a8 c f, e g bes, a r r
}
