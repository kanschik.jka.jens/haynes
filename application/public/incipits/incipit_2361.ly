\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 4/4
  e4 r16 e fis g fis8 b, g8. fis16 g8 fis16 e r8
  b' e a fis8. e16
  dis8 cis16 b e8. d16 c8 b a b16 c
  b8 a16 g
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allemanda. Allegro "
  \time 4/4
  \partial 8
  b8
  g fis16 e r8 b'
  e8. fis16 fis8. e32 fis
  g8 a16 g fis g e fis
  dis8 cis16 b r8
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Corrente"
  \time 3/4
  \partial 8
  r16 e
  e4 b8. a16 g8. fis16 g8. e16 fis8. g16 fis8. e16 dis4. r16
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Largo"
  \time 3/2
  b2 e,4. g8 fis4. e8 dis2. r8 e fis4. g8
  a4. c8 b4. a8 g4. fis8
  g2
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "5. Giga. Allegro"
  \time 6/6
  \partial 8
  b8 g a b e, b' e
  dis e fis b,4 a8 g4 fis8 g fis e
  dis4.~ dis4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 4/4
  a8 b c b16 a e'4 r8 e
  d e c8. b16 c8 b16 a r8 c
  d e f16 e f g e8 d16 c r8
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Vivace"
  \time 3/4
  a4 e'2 d4 e8 d c b
  c4 d8 c b a gis4 gis16 a b8 e,4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allemande. Allegro"
  \time 4/4
  \partial 8
  r16 a
  a4 r16 b c d
  e8 d16 c b c a b
  gis8 a16 b c d e f
  e8 d16 c b c d b c8 b16 a r8
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Giga"
  \time 6/8
  \partial 8
  e8
  a,4 c8 b4 a8
  gis a b e,4 e'8
  d e c b c d c4 b8 a4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "5. Allegro"
  \time 4/4
  a4 e a b c d b c8 d
  e4 a, f' e a b gis2
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allemanda. Largo"
  \time 4/4
  \partial 8
  r16 f
  f8 c c8. bes16 a8 g16 f r8 g
  a16 bes c d bes8. a16 g4 r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Corrente"
  \time 3/4
  \partial 8
  r16 f
  f4 c8. f16 e8. f16
  g4 c,8. es16 d8. c16
  bes8. a16 bes8. d16 c8. bes16
  a4
}
  
  \relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondeau"
  \time 3/4
  f2 c4 d e f
  c bes a g8 f g a g4
}
  
  \relative c'' {
  \clef treble
  \key f\major
   \tempo "4. March"
  \time 4/4
  \partial 8
  c8
  f8. g16 g8 f16 g a4 r8 c,
  d16 c bes a g8 c a8. g16 f8 c'
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 3/2
  c4. d8 e4 f8 g c,4. d8
  b2 \grace a8 g2 c
  d4. c8 d4. e8 f4. g8
  e1 d2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allemanda. Cantabile"
  \time 4/4
  \partial 8
  g8 c g' e d16 c c4 r8 g'
  a8. g16 f e d c b8. a16 g8 c16 b
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gavotta. poco Allegro"
  \time 4/4
  \partial 2
  c4 d e g c, d
  b a8 g c4 d e d8 e f4 e d2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Aria"
  \time 3/2
  g'2 e a g f4 e d c
  b1 c2 d g, f' e a1
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Giga"
  \time 6/8
  \partial 8
  g'8
  e8. f16 g8 c,4 d8
  b4. g4 c8
  a8. b16 c8 f4 e8 d4.
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 4/4
  d4. d8 d4. d8
  e8. d16 e fis g a fis8 e16 d r8 e
  fis16 g a8 r e fis16 g a8 r e
  fis b gis8. a16 a8 a, r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 3/4
  fis8 g fis g a4 e2 a4
  d, cis8 b e4 cis4. b8 a4
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Adagio"
  \time 3/2
  r2 r fis
  b,2. d4 cis4. b8 ais2. fis' g4. fis8
  e4. d8 cis4. e8 d4. cis8
  d2 b
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Allegro"
  \time 6/8
  \partial 8
  a8
  d4 e8 fis4 g8
  a g fis e4 a8
  b, cis d e fis g
  cis,4. a4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 3/2
  d2 g d b2. a4 g2
  d' g4. b8 a4. g8 fis2. e4 d2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allemanda. Vivace"
  \time 4/4
  \partial 8
  d8
  b c16 d g,8 g' fis16 e d c b8 e
  d c16 b a b g a fis8 d4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
  g4 d'2
  e8 d e fis g4 d8 e d c b c
  a2 g4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga"
  \time 6/8
  \partial 8
  g'8 g fis g g fis g
  fis16 g a8 fis d4 e8
  d8 c b a b c b4. g4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Menuet"
  \time 3/4
  d4 c8 b e4
  d8 e d c b a g4 g' fis
  g8 fis g a g4
}

