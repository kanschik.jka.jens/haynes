\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII [XII] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Largo"
  \time 3/2
  d2 a d e1 e2 fis e d cis a' a a1.
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  d16 a b cis  d e fis g  a8 a, r a'16 g
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio"
  \time 3/2
  a'2 f d g2. e4 cis a'
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Allegro"
  \time 6/8
  \partial 8
    a8
    d e d  d e d
    d4. r4 a8
}

