\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Vivace"
  \time 4/4
  d8 d d d d e16 fis g fis g a
  b8 d, d d d e16 fis g fis g a
  b8
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Largo"
  \time4/4
  b4. g'8 fis b~ b16 a g fis
  g8 e r e~ e g16 e dis4
  e8 b c a fis g16 a fis4
  e
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 4/4
  g'8 fis16 e d c b a g8 b d g
  fis e16 d c b a g fis8 a d fis 
  g d b'4
  }
