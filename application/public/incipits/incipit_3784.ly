 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  r8 f, g a g c, c'16 bes a g
  a8 f f'4 f e f8[ f,]
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Adagio"
  \time 3/4
  a4 c bes8 a
  bes4 a2
  a4 g8 f e d
  cis4 r r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 2/4
\partial 8
  f,8
  g[ a bes c]
  a[ f f c']
  g[ a bes a]
g c, c4
}

