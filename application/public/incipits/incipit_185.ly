\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/2
  g8 b g b  d d, fis d
  g b g b  d d, fis d
  g b d g fis4 r
}
