\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Suite" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude"
  \time 2/2
  c8 d e f g4 c,
  b g'2 f4
  e c8 d e4 a
  d, b8 a b c d b
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxiéme Suite" 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/2
  g8 fis g a  g a g a
  b4 a8 g  fis e d c
  b4 \grace a8 g4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisiéme Suite " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude"
  \time 3/4
  c4 d2 e4 c8 d e4
  f e d e8 f e d c4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatriéme Suite" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Prelude. Lentement"
  \time 2/2
  g8 fis g a g a b a
  g fis g4~ g8 b a g
  c b c d c b a g  fis e fis4~ fis4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquiéme Suite " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude"
  \time 3/4
 e8 f g f e d
 c4 c'8 b a4 g f2
 e2.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixiéme Suite" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prelude"
  \time 3/4
  d4 g8 fis g a
  fis2 g4
  e8 f e d e c
  d4 b8 c d4
}
