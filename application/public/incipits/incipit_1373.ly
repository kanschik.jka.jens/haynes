\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 12/8
  e4. fis fis4 e8 e4 fis8
  e8. d16 cis8 d8. cis16 b8  cis8. d16 e8 cis8. b16 a8
}

