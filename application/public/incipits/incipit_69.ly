\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 6/8
  \set Score.skipBars = ##t
   d8 a d   d16 e fis e d8
   e a, e'  e16 fis g fis e8
  fis16 d cis d e fis
}
