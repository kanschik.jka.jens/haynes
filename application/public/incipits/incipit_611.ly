\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Lentement"
  \time 2/4
  \partial 8
      g8
      c8. d32 e d8. c16 b8 \grace a8 g8 r16 c b c
      d8. e32 f e8. f16
      d4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe. Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Lentement"
  \time 2/4
    r8 c e g
    d4 \grace {d16 e} f8. f16 f8 e16 f d8. g16
    e8 \grace d8 c8
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIIe. Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Moderement"
  \time 3/8
  \partial 8
    g'8
    e16 d e f g8
    c, d e f g e
    f8. g16 e8
    d g8. g16
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IVe. Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Moderement"
  \time 2/4
    c4. d8 es d c b
    c4. d8 es d c b
    c es d f
    es d16 c
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ve. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Moderement"
  \time 3/8
  \partial 8
    e16 f
    g8 d f
    e \grace d8 c8 g'~ g fis4 g4 g8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VIe. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Lentement"
  \time 3/4
  \partial 8
      g'g4~ g8. a16 g f e32 d c b
      c4~ c8. g16 a32[ b c d] e f g e
      a8. b32 c
      f,4. e8 d2 c4
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VIIe. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Moderement"
  \time 3/8
      c8 g' f
      es \grace d8 c8 es
      d e4
      f16 g as g f es
      d c d es f d
      es f g f es d
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VIIIe. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Lentement"
  \time 2/2
  \partial 2
    r8 g' fis e
    d2 r8 b' a g
    a4 d, r g8 fis
    e4. fis16 g
    c,4. b8 a4 g

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IXe. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Lentement"
  \time 2/4
  \partial 8
    g8
    d'4~ d16 es d c
    bes8 \grace a8 g8 r d' g4~ g16 bes a g fis8 \grace e8 d8 r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Xe. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Gracieusement"
  \time 3/8
  \partial 8
      b16 c
      d4 g,8 a g d'
      e fis g
      fis16 e d c b c
      d4 g,8
}


