 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Vivace"
  \time 4/4
      r4 r8 r16 f f4. r16 d'
      c8. c16 bes8. bes16 f4. r16 f
      es8. es16 d8. es16
      c8. f,16 d'4~ d4 c2
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Fuga. Allegro"
  \time 2/2
  f2 g4. a16 g f4 bes es,2
  d4 d'~ d8 c c bes
  a f bes d, es g f es
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Andante"
  \time 4/4
  bes4~ bes16 d f d
  bes16. a32 bes8~ bes16 f' bes f
  g16. f32 g8~ g16 a f es 
  d c bes8
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Allegro"
  \time 3/4
    r8 bes16 c d4 r
    r8 c16 d es4 r
    r8 d16 es f4 r
    r8 es16 f g4 r
    r8 c16 bes  a bes a g  f g f es
    d8 bes' r
}