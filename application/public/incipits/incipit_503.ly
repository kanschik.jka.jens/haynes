\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 4/4
  g'8 d es d  a' d, es d
  bes' a16 bes c8 g fis8. e16 d4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Grave"
  \time 3/4
    g8. bes16 bes8. bes16 bes8. bes16
    g'4 e8. f32 g \grace as8 g8. as16
    as2
  }

 
  
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 3/4
  r8 d bes g bes d
  g d a' d, bes' d,
  c'4 bes r
}
