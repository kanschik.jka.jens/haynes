\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio [Tutti]"
  \time 4/4
  \partial 8
  g'16. a32
  b8 e,16. fis32 g8 e e dis r fis16. g32
  a8 dis,16 e fis8 b, a' g r c,~
  c b4 a8~ a16. b32 c8 r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio [Solo]"
  \time 4/4
  \partial 8
  r8
   \set Score.skipBars = ##t
   R1*3 
   r2 r4 r8 b16. e32
   c8 b16. a32 g8. fis16 g16. fis32 e8 r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro [Tutti]"
  \time 2/4
  e4 b
  g8 fis16 e fis8 dis
  e fis16 g a8 fis 
  g a16 b c8 a
  b c16 b c8 e, dis4 e fis r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*31 
  e4 b
  g'8 fis16 e fis8 dis
  e b a fis 
  g a16 b c8 a
  b c16 b c8 e, dis4 e fis r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Andante [Tutti]"
  \time 3/4
  r4 d8. g,16 \times 2/3 { c8[ b a] }
  b4 d8. g,16 \times 2/3 { c8[ b a] }
  b4 d8. g,16 \times 2/3 { e'8[ g fis] }
  g4 d r
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Andante [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*11 
   r4 d g fis d b
   e d4. c8
   b4 a c b c8 b a g
   d'4 a r
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 6/8
  e4. fis8 e dis
  e4.~ e8 g fis
  e d c b c a
  g fis e r4 r8
}