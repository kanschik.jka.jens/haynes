\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Wie zittern und wanken [BWV 105/3]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key es\major
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                    r4 r4 r8 es8
                    des' c des4 r8 es,8 
                    c'8 bes c4 r8 as8
                    es' c d4 r8
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine 1"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key es\major
                     \set Score.skipBars = ##t
                        % Voice 2
                    bes16 bes bes bes
                    bes bes bes bes
                    bes bes bes bes
                    bes bes bes bes
                    bes bes bes bes
                    bes bes bes bes
                    as as as as
                    as as as as
                    as as as as
                    as as as as
                    as as as as 
                       
                        
                
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine 2"
                     \set Staff. shortInstrumentName = #"Ob 3"
                     \key es\major
                     \set Score.skipBars = ##t
                        % Voice 3
                 g16 g g g
                 g g g g 
                 g g g g
                 g g g g
                 g g g g
                 g g g g
                 g g g g
                 g g g g
                 g g g g
                 f f f f
                 f f f f
                       
                        
                
                  }
>>
}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key es\major
   \tempo "o. Bez."
  \time 3/4
     \set Score.skipBars = ##t
    R2.*16 r4 r4 r8 es8
                    des' c des4 r8 es,8 
                    c'8 bes c4 r8 as8
                    es' c d4 r8
                    f8
                    as, f g4 r4
   
    
    
    }