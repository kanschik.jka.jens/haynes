\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                    \set Staff.instrumentName = #"Chaloumaux"
                     \key a\minor
                      \tempo "o.Bez."
                      \time 4/4
                        d4 a bes'2~
                        bes4 a8 g f4 e8 a
                        a g16 f e8 d cis4 r
                        a4. f'8 e4 r
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                    \set Staff.instrumentName = #"Flute/Oboe"
                     \key a\minor
                        % Voice 2
                       r2 r8 d e d
                       cis d e4~ e8 d4 cis8
                       d4 r a'8 g16 f e8 d
                       cis4. d8 cis4 r
                  }
>>
}
