\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 3/4
    d8 a a a a 
    e' a,  a a  a a
    fis' g16 a  g8 fis e d
    cis d16 e a,2
} 
