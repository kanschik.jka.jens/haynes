\version "2.16.1"

#(set-global-staff-size 14)



\relative c''' {
<<
\new Staff { \clef "treble"   
                     \key g\major
                      \tempo "1. Adagio"
                      \time 3/4
                      % Voice 1
                      g2. g g g
                      b,8 c d4 e~e8 fis g4 g~
                      g fis8 g a4
                  }
\new Staff { \clef "treble" 
                     \key g\major
                        % Voice 2
                        r4 b, c
                        d f8 e e4
                        d f8 e e4
                        d8 c c b b4
                  }
>>
}



\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 3/4
  g4 g'2
  fis4 e2
  d4 c2
  b8 c16 d e8 d c b
}

\relative c''' {
<<
\new Staff { \clef "treble"   
                     \key g\minor
                      \tempo "3. Adagio"
                      \time 2/2
                         \set Score.skipBars = ##t

                      % Voice 1
                      R1*4
                      r2 d,4. g,8
                      fis4. fis8 g c16 d es[ d c bes]
                      a8. g16 f
                  }
\new Staff { \clef "bass" 
                     \key g\minor
                        % Voice 2
                        g,8 g~ g16[ a bes c] d8 d,8 d d'8
                        es[ es16 d] c bes a g f8[ f']
                  }
>>
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 2/4
    b16 c d8 d d
    d e16 d c b a g 
    a b c8 c c c
}

