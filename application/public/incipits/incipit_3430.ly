\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. [Allegro]"
  \time 2/2
  d2 r8 d e fis
  e4 a, r8 e' fis g
  fis8. e16 fis8 g   fis[ g16 a]  g8 fis
  e2
}



\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 2/2
  d8 cis16 d b[ \grace c8 d16 c b] ais8. fis'16 fis[ e d c]
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Angloise altern."
  \time 2/2
  \partial 2
  a4 a8 b
  a4 d, d'8. fis16 e8 fis16 g
  cis,4 \grace b8 a4
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Trio"
  \time 2/2
 \partial 2
 d8[ e] fis e16 d
cis8 d e[ d16 cis] 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Largo staccato"
  \time 2/2
R1
b4~ b16 d cis b cis4 cis16 fis fis32 e d cis
d4
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "6. Menuet"
  \time 3/4
  a'4 fis d
  e2 a,4
  d  e8 fis g a
  fis2 e4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "7. Arietta altern."
  \time 2/2
 \partial 4
 a4
 d8 e d e fis g fis g
 e fis e fis g a g a
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "8. Adagio"
  \time 2/2
  f16[ d d b] b gis gis f' e8[ e16 fis] g g fis e
  fis8.
}


\relative c'' {
  \clef bass
  \key d\major
   \tempo "9. Duetto a 2 bassoni"
  \time 2/2
   \partial 4
   a,8 g
   fis4 a e fis8 g16 a
   g8 fis e fis  d4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "10. Gavotte"
  \time 2/2
 \partial 2
 fis8 g16 a a[ g fis8]
 fis e d4 e a,
 d8 e
 a,4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "11. Rondeaux"
  \time 3/4
d4 fis a
fis2 e8. g16
fis2 e8 fis16 g
g8 fis g fis e d
e2 a,4
}
