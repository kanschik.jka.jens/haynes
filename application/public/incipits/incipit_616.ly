
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Les Impromptus De Fontainebleau [38 Pieces]"
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Vivement"
  \time 2/4
\partial 4
r16 g16 a g
c8[ b a g]
f e16 f g[ d f e]
d8[ c b a] 
g16 c e g g,[ b d f]
 
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Les Sentiers D'Avon. Fanfare"
  \time 6/8
\partial 8
g8 f4 f8 e4 e8
d4. c4 f8
e4 e8 d8. c16 d8
c4 c8 c4

 
}
