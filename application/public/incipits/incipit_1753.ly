 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  e,8
  a4. e'16 gis, a4. e'16 gis,
  a4. e'16 gis, a4 r8 e
  b'4. e16 a, b4. e16 a,
  b4
}


\relative c'' {
  \clef treble
  \key e\major	
   \tempo "2. Affetuoso"
  \time 6/8
  \partial 8
  b'8
  gis8. a16 b8 a8. b16 gis8
  fis8. e16 fis8 fis4
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "3. Allegro"
  \time 2/4
  cis8 e4 d8 cis e4 d8
  cis d cis4 b r8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}