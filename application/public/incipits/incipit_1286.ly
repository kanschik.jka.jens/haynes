 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key d\minor
   \tempo ""
  \time 12/8
  d8. e16 d8 d4 d8 d4 cis8 r r a
  f'8. g16 f8 f4 f8 f4 e8 r
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key d\minor
   \tempo ""
  \time 12/8
     \set Score.skipBars = ##t
     R1.*4
     r2. r4. r4 a8
     d4.~ d16 e f8 e16 d d4 cis8 r r a
     f'4.~ f4 g8 e4. r8r e~ 
     e4 d8 r r r d4 cis8 r r
}