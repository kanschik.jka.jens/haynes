
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sextetto B-Dur "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key bes\major
  \tempo "1. Andante Grazioso"
  \time 2/4
  \partial 8 
  f16. f32 f8. bes16 d,8 d16. [d32]
 d8. f16 bes,8 r16 f'16
 g8. a16 \grace a16 bes8 a16 g g4 f8 r8
 
 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 4/4
  bes4 d16 c bes c bes8 d,16 f f bes bes d
 d4 f16 es d es d8 f,16 bes bes d d f
 f4 \grace es16 d16 c d es f4 
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sextetto F-Dur"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante Grazioso"
  \time 3/4
 c8. bes16 a8 a a a16 c
 c8. bes16 bes4 r4
 bes8. a16 g8 g g g16 bes bes8. a16 a4 r4
 
 
 
}
 
\relative c' {
<<
\new Staff { \clef "treble" 
  \key f\major
   \tempo "2. Allegro ma non molto"
  \time 4/4
 % Voice 1
 r1 d'8. bes'16 bes8. [a32 g f e] e16 [f f f] f8 r8
  

}

\new Staff { \clef "treble"
  \key f\major  
 % Voice 2
  f,8. a16 a8. c16 c f c c c8 r8 r1
  

}
>>
 
 
}
