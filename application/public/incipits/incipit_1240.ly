 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Hautbois solo"
  \key c\minor
   \tempo "o.Bez."
  \time 4/4
  r4 r8 g' es c f as
  as g r g g f16 es es d es c
  b4 r16 es f g  as f g as  bes, as' g f
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Almira"
  \key c\minor
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
     R1*5
     r2 r4 r8 g'
     es c f as as g r g g f16[ es] es[ d] es[ c] b4 r 
}