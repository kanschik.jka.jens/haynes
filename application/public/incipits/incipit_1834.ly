\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 4/4
    \override TupletBracket #'stencil = ##f
 
  \partial 16
    g'16
    g4~ g16 g, a b a8. \times 2/3 { c32 b c} d8 b16 c
    b8 g g'8. f32 e e8. d16 c16 b a g
    fis8.
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 2/2
  \partial 4
    a4 d f8 e d4 a
    f d r8 a' b cis
    d cis d e f4. g8 e2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. "
  \time 2/2
  \partial 2
  r8 es f g
  f4. es8 d4 b8 d
  b4 g r8 c d es
  f4. g8 es4 d8 es
  c2
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 4/4
    g'4 r8 d16 d bes8 g
      \override TupletBracket #'stencil = ##f
   g'8. \times 2/3 { g32 a bes}
   fis4 r8 d16 d a'8. a16 a8. g32 a bes8 g
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. "
  \time 4/4
  \partial 8
    b8
    g e c' b16 a b4 e~
    e8 fis16 e dis8 e16 fis g8 e g a16 g
    fis4 fis4.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. o.Bez."
  \time 4/4
    e4 r16 e fis gis
    a,8 a a a
    dis4 r16 dis e fis  b,8 
    fis'32 e dis e64
    fis a,8 gis16 fis
    gis8 e
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 4/4
    r4 r16 a d fis  e d cis b a a e' g
    fis8 d r16 fis g a
    e fis g a d, a' gis d
    d cis cis8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 16
    a16
    a4~ a16 gis a b cis8 b16 a cis b a gis
    a8 e r16 gis a b cis8. d16 d8. e16 e4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. o.Bez."
  \time 3/4
    r4 fis g
    ais,2 b4
    c4. b8 c4 d b
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 4
    r16 f g a
    bes8 f16 g f8. es16 d8 bes r16 c d es
    f8 bes,16 c32 d
    c8. bes16 a
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. "
  \time 3/4
    cis4 a4. e'8 e2.
    a8 gis fis e d e16 fis
    b,2 a4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
  f4 g4. f32 g a8 g16 a f8 c
  d16 c bes a g c bes c  a bes a g f f' e f
  c
}

