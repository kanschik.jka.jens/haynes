\version "2.16.1"
         #(set-global-staff-size 14)

\relative c'' {
  \clef treble  
  \key c\major
    \time 2/2
    \tempo "1. Adagio"
    \partial 8
\set Timing.baseMoment = #(ly:make-moment 1/4)
    g'8 e16 f g8  g, b c4 r8 d|
    e g e d16 c c4 r8 e|
    a a16 g g f f e d8 g, g e'16 f|
    g8 c, e a
    
}