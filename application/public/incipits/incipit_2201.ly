\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  f,8
  bes32 a16. g32 f16.  bes32 d16. c32 es16.
}
