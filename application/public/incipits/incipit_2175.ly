\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1e Entrée, Scéne 9e"
  \time 2/2
  \partial 4
  b8 c
  d4 d d d
  e d g2~
  g4 a8 g fis e d c
  b4 \grace a8 g4

}
