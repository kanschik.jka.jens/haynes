\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. [Allegro]"
  \time 4/4
    bes2 f d r4 bes'
    d2 bes f r
    es'4 r es r es r4 r2
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Polaca. Variata"
  \time 3/4 	
  \partial 4.
  f8 g a
  bes f16 d bes8 bes' r d
  c16 bes a g  f8 e es d
  d16 c fis g fis g f es d c es c
}

