\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Mein Jesus soll mein alles sein [BWV 75/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key g\major
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
   r8 g a fis g b
   a16 b c8 c2
   r8 b8 c a b d
   c16 d e8 e2
  
 
 
                                                                                   

}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key g\major
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
R2.*14 b8 c16 d c8 b a g
d'2.~
~d8 e c4. b8 a4 r4
  
       
}



