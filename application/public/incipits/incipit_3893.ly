 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Violin"
  \key bes\major
   \tempo "Adagio"
  \time 2/2
    f4. \times 2/3 { d16[ c bes] }  a8[ bes] es d16 c
    d8[ bes] r16 f' es d  c8[ d] bes16 c bes a
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key bes\major
   \tempo "Adagio"
  \time 2/2
     \set Score.skipBars = ##t
     R1*6
      r2
      f4. \times 2/3 { d16[ c bes] }  a8[ bes] es d16 c
    d8[ bes] r4 

}