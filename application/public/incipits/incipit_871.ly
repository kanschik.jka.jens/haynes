\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Vivace"
  \time 2/4
      d4 fis, g8 a b cis
      d4 fis, g8 a b cis d a b a
      b a r a
      b a
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Largo"
  \time 2/2
  fis4. d8 b ais b cis
  ais8. b32 cis fis,8.[ cis'16] d16. cis32 d16. e32 d4
  cis2
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 2/4
  \partial 8
  d8
  d d, r d'
  d d, r fis'16 g
  a8 fis g e
  fis d r
}


