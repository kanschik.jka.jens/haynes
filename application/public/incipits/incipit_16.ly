 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 2/2
  c4.   \override TupletNumber #'stencil = ##f
  \times 2/3 { c16 b c }  d4. \times 2/3 { d16 f e32 d }
  e16 d c8 g'4~ g16 f32 e32 d c d e f4
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 2/2
  c16[ d e f] g8. b,16 c4 b16 d c e
  c[ d e f] g8. b,16 c4 b8 g'
  a16[ g f g]
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Largo"
  \time 6/8
  \partial	8
  g8
  c e c g' c,4~
  c8 c' c,4. d8
  e4 g8 d g d
  e c16 d e f d4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
    c8 g e' c g' e
    c' g, c e g b
    a f, a c e g
    f d, f b c e
    d e f4
}