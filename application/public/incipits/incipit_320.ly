\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Grave"
  \time 4/4
  \partial 8
  a'16 g
  fis8 g16 a b8 b b a r a16 d,
  g8 g g g g[ fis]

}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  a'8 a16 g fis e d8 b' a cis, d a'
  a16 g fis e d8 b' a cis, d a'
  b16 g a fis  g e fis d  e d cis b a8
}
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Largo"
  \time 3/2
  fis2. g4 fis2 g1 g2
  g fis b
  e,2. fis4 g2~ g
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Allegro"
  \time 3/4
  d4 e cis d cis8 b a g
  fis a b cis d e fis4 g e
  fis e8 d cis b
  a
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Grave"
  \time 4/4
  d8 cis16 b ais4 b8 fis b4~
  b8 ais16 b cis8 e, e d d'4~
  d8 cis16 d e8 g, g[ fis]
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  b8 fis' fis fis fis d b r16 d cis b
  g'8 b, g' b, a g' r16 cis, b a
  fis'8 a, fis' a, g g' r16
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Largo"
  \time 4/4
  d8 cis16 b a8 g fis4 r8 cis'
  d fis e a16 g  fis8 a4 gis8
  a cis, b16 dis e8~ e16 cis dis b cis eis fis8~
  fis16 dis e8~ e16 cis dis8~ dis16 b cis8 r
}
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "4. Allegro"
  \time 4/4
  \partial 4
  fis4 g fis b ais
  b cis d fis,8 e
  d4 cis8 b cis4 b8 ais b2 r4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4
  g8 b16. c32 d8 fis, g16 fis g8 r c c b r e16 g, g8 fis d'4~
  d16 g e32 d c b
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
    g'4. a8
    fis a c,4~ c8 e d c b g r4
}
  
  \relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Grave"
  \time 3/2
  g'2 fis e fis dis1
  e2 b g' g fis e dis1.
  }
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 4/4
  \partial 8
    g'8
    d4 c b16 a b c d8 g
    d4 c b16 a b c d8 g,
    a g d' g, a g d' g, fis g c b
  }

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Vivace"
  \time 4/4
  \partial 16
  e16
  e8 g, fis dis' e e, r16 e' dis e
  c8 b4 a8 b4 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Grave"
  \time 3/4
  \times 2/3 {g'8[ fis g]} \grace c,8 b4 r8 g'
  fis e d4 fis
  g8 a b4 r8
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Allegro"
  \time 12/8
  \partial 8
  b8 e b e  g fis e  fis b, fis'  a g fis
  g a g  fis g e  dis e fis  b, cis dis
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Largo"
  \time 4/4
  g'4 a8 c, c b r4
  r8 fis' fis16 a g fis fis4 e
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4
  r8 g' e c  d4 g16 f e d
  c d e c g'4. f16 e f4~ f8 e16 d e8 a16 g fis4 g~ g8 fis16 e fis4
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Largo"
  \time 3/4
  b4 e c
  a2 r4
  a fis' a, g2 r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 3/4
  \grace {e16[ f]} g2 f4
  e8 d c4 g'
  a d, c b8 a g4 r
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Grave"
  \time 3/4
  a8 f' e4. d8 c b a4 r8 a'
  gis e g4. a8
  fis d fis4.
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro"
  \time 12/8
  \partial 8
  e8 a a,16 b c8 b a gis a c16 d e8 d c b
  c a16 b c d e8 f a, a4 gis8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Grave"
  \time 4/4
  \partial 8
  g'16 f
  e8 d16 c a'8 g f4~ f16. a32 g16. f32
  e4~ e16. g32 f16. e32 d8 g c,16. e32 d16. c32
  c8 b r
}
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Allegro"
  \time 3/8
  a16[ b] b8. a32 b
  c16[ a c e] a8
  f e16[ d e f] d4 r8
}

