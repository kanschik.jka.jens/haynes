 
\version "2.16.1"


#(set-global-staff-size 14)



\relative c'' {
 \clef "treble"   
                     \key es\major
                      \tempo "March"
                      \time 4/4
                      \partial 16
                      bes16
                      es8. es16 es8. es16 es8. es16 es8. es16 
                      es8. es16 es8. es16 d8. as'16 g8. f16
                      \times 2/3 { g8[ as bes]} es,4 r2
}

