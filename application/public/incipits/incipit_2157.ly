 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Andante"
  \time 4/4
  c4. g'16 c, c4. g'16 es
  d8 f4 es16 d es8 c r g'
  as as as as as16 f f d  d bes bes as'
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Allegro"
  \time 2/4
  g'16 f es d c8 g'
  as g r g
  as g c g as g r
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Larghetto"
  \time 3/4
  bes2 es4 es d8 c bes as
  g4 as f
  g8 f es4 r
}
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Vivace"
  \time 3/8
   c4. d
   es16 d c8 g'
   as g16 f es d
   g8 f16 es d c
}