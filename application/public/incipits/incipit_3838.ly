\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key c\minor
                      \tempo "o.Bez."
                      \time 4/4
                      r2 r4 es8. g16
                      g8 f f4 r2
                      r r4 d8. f16 f8 e4. r2
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key c\minor
                     % Voice 2
                     c,8 es c g' c, g' c, g'
                     d f d as' d, as' d, as'
                     d, f d g d g d g
                     des e des g des g des g
                  }
>>
}
