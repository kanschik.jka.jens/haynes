\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  r8 c, c c   c c c c
  c d16 e f g a b c8 c, c c
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*14
  c8 g c d e d16 e f8 e
  e d e d e d16 e f8 e
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Largo [Tutti]"
  \time 4/4
  e8 e e e  f f f f 
  e e e e  e e e e
  d d d d
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Largo [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*5
   r2 e16. f32 e16. f32 e16. f32 e16. a32
   f4 r f16. g32 f16. g32 f16. g32 f16. g32
   e4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto [Tutti]"
  \time 2/4
  g'4 c,8. d16
  b4 r8 c
  d e f g
  e4 d8 g
  e4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*12
   r8 e d c
   b e d c
   b4 r
   r8 e d c b e d c
   b4 r
}