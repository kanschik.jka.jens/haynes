\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.bez."
  \time 3/4
    r4 d2
    cis r4
    d8 e e4. d16 e
    fis4. g8 e4
    \grace e8 fis2 d4
}
