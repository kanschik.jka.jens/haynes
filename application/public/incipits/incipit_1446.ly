\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  a16. d32
  d4~ d16. cis32 e16. g32
  \grace g8 f8 e r a16. f32
  d8~ d16. f32 e16. g32 f e d cis
  \grace cis8 d8[ a]
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. [Tutti]"
  \time 3/4
  <f, a,>4.a16 c f c a' f
  e8. f32 g f8 f, a c
  \grace c8 bes4 a r8 r16 d
  \grace f,8 e8. d32 e f4 r
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro assai [Tutti]"
  \time 2/2
  <d f, a,>2 a4. f8
  d2 f' e4. gis,8 a2
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro assai [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   R1*48
   a'1~ \times 2/3 { a4[ g f e d cis] }
   d4. cis8
   d2~
   d a bes a
}
