\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 4/4
    \partial 8
    a'8
    d8. cis32 b a8 a8 a8 a8 a a
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto III " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 3/8
    c8 a bes  c a bes
    c16 bes a8 g
}
