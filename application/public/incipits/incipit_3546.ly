\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. allegro"
  \time 4/4
  f2 d8 bes es c
  f2 d8 bes es c
  f4. es8 d4
}
  
