\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 8
  d8 
  b d a fis g4 r8 d'
  b d a fis g b g d
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 3/4
  b4 e4. fis8
  dis4. e8 fis4
  g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 6/8
  r8 g a  b4 r8
  r b c d4 r8
}
