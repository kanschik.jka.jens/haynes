
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Rondeau. Le Complaieant. legerem.t"
  \time 2/2
\partial 2.
e4 e e 
d c8 d e f e f
g4 c c c 
g f8 g a g f e
d4 e e e 
d c8 d e f e f g4
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Iere. Musette. La Sineere"
  \time 3/4
\partial 4
e8 f
g2 a4
g8 c b a g f
e a g f e d
e d c d e f
g2
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gavotte. La Nanette. un peu legerement"
  \time 2/2
\partial 2
e4 d
c d e f
g c, f e
d c b c d2

 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Iere. Menuet"
  \time 3/4
e8 f g4 g
g8 f e d e c
d e f4 e
d8 c b a g4


 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe. Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. L'aimable. gracieusem.t"
  \time 3/8
\partial 8
c16 d e8. f16 e d
c8 g g'
g8. a16 g f
e8 d g 
g16 f e d c b
c d e f g e 
a8 f8. e16 d4
 
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Musette. La champetre. sans lenteur"
  \time 2/2
\partial 2
e8 d e f
e f g4 f e
d c f8 e d c
f e d c b4 c 
d2

 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Menuet l'italien"
  \time 3/4
c4 e g 
c c, r4
c'8 b a g f a
g a g f e d 
c4 e g 
c c, r4

 
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Gavotte La francoise. Tendrem.t"
  \time 2/2
\partial 2
c4 g'
f4. es8 d es f g
es4 \grace d8 c4 es8 d es f
g4 as8[ g f es d c] 
b4 \grace a8 g4 

 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Rondeau Le joyeux. gaym.t"
  \time 6/8
\partial 4.
c8 d b
c4 g8 e' f d 
e4 c8 g' a f
g e f g f e
d4. c8 d b c4 g8

 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIIe. Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Le plaintis. Lentem.t"
  \time 3/4
c4 e \grace d8 c4
g'2 f4
e8 f f4. e8
d2 c4 
e8 d e f e f 
d4


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondeau. Le Badin. gaym.t "
  \time 2/2
\partial 2
c8 e d f
e g f a g f e d
e4 \grace d8 c4 c8 e d f
e g f a g f e d c2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Musette. La Favorite"
  \time 6/8
\partial 4.
e4 g8
b,4 c8 d e f 
e4 d8 e f g
a4 g8 f4 e8
d4. 

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Tembourin "
  \time 2/4
c4 d8 e
d4. e8
f[ g d e]
c16 b c d c8 g




}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Iere. Menuet. allemand "
  \time 3/4
\times 2/3 {e8 f e}
\times 2/3 {d8 e d}
\times 2/3 {c8 d c}
d4 g, c
\times 2/3 {f8 e f}
\times 2/3 {d8 e f}
\times 2/3 {g8 f g}
e4 d8 e c4





}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IVe. Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude. L'Espagnol. gravement"
  \time 2/2
\partial 4
c4 g'2~ g8 a g a
f e d e f g f g 
e d c d e d e f
d4 d


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondeau. Le quiney. gaym.t"
  \time 2/2
\partial2
g4 c
c8 d e4 e d8 c
d4 g, g d'
d8 e f4 f e8 d
e4 c c e

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. La Belotte. gracieusem.t"
  \time 3/4
\partial 2
es8 d c b
c4 g'8 f es d 
es4 f8 g as f
g4 f8 es d c
b4


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Gigue. La folette "
  \time 6/8
\partial 8
g8 c b c d c d 
e d e f e f 
g f e d e c 
b a b g4

}





\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ve. Suite. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prelude. lentement"
  \time 3/4
\partial 4
g4 d'4. e8 d c
b4 \grace a8 g4 b8 c
d4 g2
fis4. g8 a4 
b4 \grace a8 g4 \grace fis8 e4

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Ier. Rigaudon"
  \time 2/2
\partial 4
b8 c
d2 g
fis2. e4
d c b a 
b8 c b a g a b c
d2
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Rondeau. Le Brunet. gracieusem.t"
  \time 2/2
\partial 2
g8 fis g a
g4 d e8 d e f
e4 \grace d8 c4 d8 c d e
d4 \grace c8 b4 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Ier. Gigue"
  \time 6/8
\partial 8
d8 g4 d8 d e fis
g4. g4 d8
e d c b a d
b4 a8 g4 
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Contre dance"
  \time 2/2
\partial 2
b4 b
a g8[ a b c b c]
d4 g, g' g
fis g8[ fis e d e fis]
g4 d b b a4
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VIe. Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Rondeau. Lamoureux. gracieusem.t"
  \time 3/8
d4 g8
fis4 e8
d c b a4 g8
b c d 
e fis g
fis4 g8
a16 b a g fis e
d4
 
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Rondeau. Lindifferent. Legerem.t"
  \time 2/4
g8[ fis e d]
c4 b
e8[ d c b]
a4 g
b8[ c d e]
d[ e fis g]
a4 g 
fis \grace e8 d4
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Gavotte. La Sensible "
  \time 2/2
\partial 2
d4 g,
es' d g8 bes a g
fis4 \grace e8 d4 g d
es \grace d8 c4 es8 d c bes
a2
 
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "4. Ier. Menuet"
  \time 3/4
g8 a bes g a fis
g4 d g
d8 es d c bes a
bes4 a8 bes g4
 
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "5. Le Jaloux. marqué"
  \time 2/4
g8. a16 b8. c16
d4 d,
g8. b16 a8. g16 
fis4 \grace e8 d4
g8. fis16 g8. d16
e8. g16 d8. g16
 
 
}

