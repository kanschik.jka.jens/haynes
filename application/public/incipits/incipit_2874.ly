\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro [Tutti]"
  \time 3/4
  d8 a'4 a a8~
  a a4 a8~ a16 f e d
  cis8 a'4 a a8~ a8 a4 a8~ a16 g f e d8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*21
    d8 a'4 f e16 d
    e8 g4 e d16 c
    d8 f4 d c16 bes a4 r r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Largo"
  \time 4/4
  d2 d cis8 d16 e a,8 g f8. e16 d4
  f'2 f e8 f16 g c,8 bes a8. g16 f8[ f']
}



\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro [Tutti]"
  \time 4/4
  bes4 a~ a8 g' f e
  f16 e d cis d8 a bes4 a~
  a8 g' f e f16 e d cis d8[ a']
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*11
     r2 r4 r8 a8
     d16 cis d f  e d e g  f e f a  g f g  bes
     a8 g16 f e8 d cis b a a
}
