\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
  f4 f16 es d c bes a bes8 r bes
  c16 d es4 f8 d c f a,
  bes4. a8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/4
  bes4 c
  d8 es f a,
  bes4 c
  d8 es f a,
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 3/2
  d1 r2
  d1 r2
  d4 c es d c bes
  a d2 c4 bes a~
  a g
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Tempo giusto"
  \time 3/8
  bes8 f' g
  f8. es16 d8
  c16 d es f g bes, a8. g16 f8
}