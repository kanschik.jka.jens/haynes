 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "6. Lira"
  \time 4/4
    c2 \grace { c16 d}  e4 \grace e8 d4
    \grace d8 c4 \grace c8 b4 \grace b8 a4 \grace a8 g4 
    c2 \grace { c16 d}  e4 \grace e8 d4
}
 