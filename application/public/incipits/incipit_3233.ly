\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Höllische Schlange, wird dir nicht bange [BWV 40/4]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key d\minor
                      \tempo "o. Bez."
                      \time 3/8
                      % Voice 1
                  d8. f16 a8
                  d fis, r8 g8. bes16 d8
                  f gis, r8
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key d\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                     d8. f16 a8
                  d fis, r8 g8. bes16 d8
                  f gis, r8
                      
                  }
>>
}


\relative c'' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key d\minor
   \tempo "o. Bez."
  \time 3/8
     \set Score.skipBars = ##t
   R4.*16 d,,8. f16 a8
   d fis, r8
   g8. bes16 d8
   gis,4 gis8 R4.
  
   

}