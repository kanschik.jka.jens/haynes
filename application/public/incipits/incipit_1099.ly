\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Largo"
  \time 4/4
  a2 a16 b a g fis g fis e
  fis d e fis g a b cis d2
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Spirituoso"
  \time 2/2
  d8 a r16 d cis d e8 a, r16 e' d e
  fis a g a d, fis e d cis4 r16
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Affetuoso"
  \time 3/4
  fis2 d4 fis8. g16 a4 cis,
  d2 cis4 d8. e16 fis4 a, b2
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Largo"
  \time 3/2
  g2. a4 b2
  fis1 b2
  e,2. c'4 a fis d1 r2
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Vivace"
  \time 2/2
  \partial 4
  r16 b c dis
  e8.[ b16] g'fis e dis e8. b16 g'[ fis e dis]
  e[ g fis g]
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Andante"
  \time 4/4
  b4. g'8 e8. dis16 e4.
  a,8 a' fis dis2
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Affetuoso"
  \time 3/4
  g8. a16 b4 e,
  \grace {a16[ b]} c2 b4
  e8 b e g fis a16 b
  dis,2.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Largo"
  \time 4/4
  bes4 bes16. d32 c16 d32 es bes16. as32 g16 as32 bes es,4~
  es16. g'32 f16 g32 as
  g16.
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Vivace"
  \time 2/2
  \partial 4
  r8 es
  bes4 as g16 f g as g8 bes
  es f16 g d4 es r8
}
  
  \relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Largo"
  \time 3/4
  g8. as16 g8. as32 bes as8. g16
  f4. es8 f8. f16
  as8
}
  
  \relative c'' {
  \clef treble
  \key es\major
   \tempo "4. o.Bez."
  \time 6/8
  \partial 8
    g8
    f4 es8 d es f
    bes,4. es4 d8
    d c bes bes as g f4.

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. largo"
  \time 3/2
  a2 a4. b16 c c4. b8
  b2~ b4. f'8 e4. d8
  c2 a r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Vivace"
  \time 2/2
  \partial 8
  e8
  a4 b c16 b a gis a8 e
  d4 c b r8 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Grave"
  \time 4/4
  c8 c c e16 d c8 c c c
  g'4. g,8 a a a c16 b 
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Vivace"
  \time 12/8
  \partial 8
  e8
  a4 e8 b'4 e,8 c'4. b4 e8
  a, d c b4 a8 gis4. r8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Largo"
  \time 4/4
  \partial 8
  g8
  c4 c16. d32 e16 f32 g
  d8 b g f'
  f16 g32 a g16. f32 e8 c
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Vivace"
  \time 2/2
  g'8
  e c4 d16 e f8 e4 f16 g
  a8 g4 c,8 b c r16
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Largo"
  \time 3/4
  \partial 4
  e4~
  e a, d~ d c2 b b4 c d2 e

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Vivace"
  \time 2/4
  e4 g
  c,4. c8 d e f g e c g'4~
  g fis~
  fis e~
  e d
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Menutet"
  \time 3/8
  g'8 e8. d16
  c8 g c b16. c32 c8. b32 c
  b16. e32 e8. d32 e
  f8 e16 d c b
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4
  \partial 8
  g8
  d'8. e16 d8. 
  e16 d16. g32
  fis16. g32 d16. c32 b16. c64 d
  g,32 d' e fis g d g a b4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro assai"
  \time 4/4
\partial 8
b16 c
d8 d d e16 fis g fis g d a' g a d,
b'8 d, d c16 b
b8 a r16
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Siciliano"
  \time 12/8
  \partial 8
  b'8
  e,8. fis16 dis8 e8. fis16 g8
  d8. e16 d8 c4 b8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Menuet"
  \time 3/4
  b8 c c4. b16 c d8 g, c4. d8 e f e d c b b4 a2
}

