 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. Allegro"
  \time 2/4
  f8. e16 f8 g
  as4 f
  es8 des16 c des8 des
  \grace des8 c4.
}


\relative c'' {
  \clef treble
  \key f\minor	
   \tempo "2. Recitativ accomp."
  \time 4/4
  r8 c c f \grace f8 e8. e16 f8 g
  des des des c c bes des c
  c16 as f8 r
}

\relative c'' {
  \clef treble
  \key f\minor	
   \tempo "3. Vivace"
  \time 3/4
  c4 bes8 as g f
  e'2 f4
  c8 d e f g as
  g2 r4	
}
