\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 2/4
  d8 fis16. e32  d8 fis
  e8. fis32 g fis4
  fis8 a16. g32 fis8 a
  g8. a32 b a4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Pazziando. Allegro"
  \time 2/4
  \partial 4
  a8 d
  cis a e'16 fis g e
  fis8 d a d
  cis a e'16 fis g e d4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Minuet"
  \time 3/4
  d8 e cis e d4
  fis g e g fis4
  a8 g16 a b4 a
  \grace a4 g2 fis4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 4/4
  \partial 8
  d8
  g b16 a g8 d e d r d
  g b16 a g8 b, c b e e
  d c16 b a8 c \grace c8 b4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  g8
  d'8. e16 d8 g
  fis16 e d8 r b'
  a b c a b d r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Minuet"
  \time 3/4
  g'4 d b
  g' d b
  g4. b8[ a c]
  b4. d8 c e
  d4 c b
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 4/4
  \partial 8
  c8
  g'8 e16 g \grace g16 f8 e16 d e g a g g8 c,
  g'8 e16 g \grace g16 f8 e16 d c g a g g8 g'
  g16 e a4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Fuga"
  \time 2/4
  g'2 e4 c d8 e \grace g16 f8 e16 d e8 c e fis
  g a b g
  fis e d fis
  g4
}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Minuet Amoroso"
  \time 3/4
  e2 f4
  \grace a8 g4. f8 e4
  g8. a16 e4 d
  c2 r4
}
  

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 4/4
  \partial 8
  d8
  g g,16. g32  g16. a32 b16. g32 a8 a16. a32 a16. b32 c16. a32
  b8 b16. b32 b16. c32 d16. b32 c8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Al Inglese Allegro"
  \time 2/4
  \partial 4
  d8 b
  c a b16 c a b
  g4 g'8 d e c a16 b c d
  b4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Minuet"
  \time 3/4
  g4 b d
  \grace { e16[ fis]} g4. fis8 e4
  d4 b g
  \grace { a16[ b] } c4. b8 a4
  b
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Andante"
  \time 4/4
  \partial 8
  cis8
  e cis r e d b r d
  cis8. b32 cis d8 cis cis b r
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Burlando. Allegro"
  \time 2/4
  \partial 4
  cis8. d16
  e8 e d d
  cis4 a8. b16 cis8 cis b b a4
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Menuet"
  \time 3/4
  a2 b8 gis a b cis b a4
  cis2 d8 b cis d e d cis4
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 4/4
  \partial 8
  d8
  a' a16 a a d cis b b8 a r a16 fis
  fis e g e e d fis d cis8 d r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  a'8 r fis r d16 cis d e d8 r
  g r e r cis16 b cis d cis8 r
  d8 fis16 g a8 a a8. g16 fis8 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegretto"
  \time 3/4
  d4~ \times 2/3 { d8[ e fis] } e8 d
  cis4 b a
  g'4~ \times 2/3 { g8[ a b] } a8 g fis4 e d
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Andante"
  \time 3/8

}

