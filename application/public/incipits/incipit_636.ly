\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4	
     g4 b8. g16 a4 d, a' c8. a16 b4 d,
     b'8 a16 g b8 cis d4 d,
     g8 fis16 e g8 ais b4 b,
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Grave"
  \time 4/4
  \partial 16
    b16
    b8. b16 c8. b16 b4 r8 r16 b
    b8. fis'16 g8. fis16 e4. e8
    e8. e16 fis8. e16 \grace e8 dis4 r8	
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 4/4
    g2. \grace e'4 d2. \grace a'4 g2.
    fis4 e d
    e8 fis g a b c
    d, e fis g a b
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Con Spirito"
  \time 4/4	
     g'4 e8. c16 d4~ d16 f e g
     c,4~ c16 e d f  \grace f16 e8 d16 c g'8 c
     \grace g4 f2 
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Andantino"
  \time 4/4
    e8 f16. e32 a16. e32 c16. a32 \grace { d16 e} f8 e r16. e32 f16. e32
    \grace e16 d8. b'16 \grace a16 gis8. d16 \grace d16 c16. d32 e8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/4
     g'2. \grace g4 f2 e4
     d8 f e d c b
     c g f e d c

}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4	
  \partial 16
     c16
     d8. c16  e8. f16 \grace f8 e4 r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 4/4
     f4 c8 a f4 r8 c f c g' c, a' g r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Minuet Andante grazioso"
  \time 3/4
      c8. d16 c8. f16 e8. f16
      \grace c4 bes2 a4
      d8. c16 d8. f16 e8. g16
      f8 c4 bes8 a g
}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro ma non Presto"
  \time 4/4	
  \partial 16
      a'16
      a8 fis16. e32 a8 e16. a32
      d cis d e d4 a8
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 4/4
  \partial 16
    fis16
    fis8 r16 fis e16. fis32 g16. fis32 \grace fis8 e4 r8 r16
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro assai"
  \time 3/8
      a'4. g fis16 e g fis b a g8 fis e d a r
}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4	
  \partial 8
     b'8 b b, e16 g fis a \grace a16 g8 fis16 e fis b, c b
     b'8 b, e16 g fis a \grace a16 g8 fis16 e fis b, c b
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andantino"
  \time 4/4
  \partial 8
    d8
    \grace c16 b8. g'16 fis16. e32 b16. cis32 d16. d,32 c'4 b8
    \grace b16 a8. e'16 d16. c32 b16. a32
    \grace g16 fis8. e32 fis g16	
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 4/4
    b4 e dis e8. fis16 \grace a8 g4 fis
    b, g' fis
    g8. a16 b4 a
}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 6/8
  \partial 8
     g8 d'8. e16 d8  d16 g d b c a
     \grace {g16 a} b8 a4 r16 d, e d g fis
     e g a g e' d c8 \grace b8 a4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
    d8 g d4 c8  \grace c8 b4 d16 d, c' d,
    b4 d16 d, c' d, b'4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Minuet. Andantino"
  \time 3/4
    d4~ d8. c16 b8. d16
    \grace d4 e2 d4
    d4~ d8. c16 b8. d16
    \grace d4 c2 b4
    
}
