\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Seinem Schöpfer noch auf Erden [BWV 39/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key f\major
                      \tempo "o. Bez."
                      \time 3/8
                      % Voice 1
                     a16 e f a bes f
                     c' e, f a d f,
                     e c' bes a bes g'
                     a, g bes a g f
                     c'4.~ c8 e, d~
                     ~d c' b c4.
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key f\major
                     \set Score.skipBars = ##t
                        % Voice 2
                    R4.*4 e16 b c e f c
                    g' b, c e a c,
                    b g' f e f d'
                    e, d f e d c
               
                      
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key f\major
   \tempo "o. Bez."
  \time 3/8
     \set Score.skipBars = ##t
    R4.*20 a16 bes bes8. a32 bes
    c8 c d
    e,16 g bes8 a8
    \grace a8 g8 f16 e d c
    f a c8 es,8 es d4
    g16 b d8 f,8 f e4
   
    
    
    }