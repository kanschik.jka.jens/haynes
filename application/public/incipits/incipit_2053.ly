 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 3/4
  r8 c, f a c f
  a4 r r
  r8 c, c c  c c
  d16 f e f   d f e f  d f e f
  c f e f  c f e f   c f e f 
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Gavotta"
  \time 2/2
  \partial 4
  f8 e
  f4 c f c
  a g8 a f g a bes
  c2 d
  g, r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Aria"
  \time 4/4
   f4 c a'~ a g f
   e c bes' bes a g
   a f8 g a4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Menuett"
  \time 3/4
    c4 f8 c g' c,
    a'4 g f
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "5. Capricio"
  \time 2/4
  f8 c a' c, f c a' c,
  \times 2/3 { f8[ e d]}
  \times 2/3 { c8[ bes a]}
  g8 c, c c'
}