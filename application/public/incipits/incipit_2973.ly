\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 3/4
  c'4 g8 g g g
  gis4 a r
  f d8 d b b b4 c
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "Allegro"
  \time 2/2
  c8 e d c   b c b c
  d2. c4
  d c f e d r r2
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante"
  \time 6/8
  \partial 8
  d8
  d8. e16 d8 d d d
  g4. fis8 r e
  d r c b r c
  b4. a8 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Menuetto. Poco Allegro"
  \time 3/4
  \partial 4
  g'4
  g2 f4 e r e
  f g a
  e d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Rondo. Allegro"
  \time 2/4
  \partial 8
  g'8
  a g r g a g r g
  g f r f f e r e
  r d r c r b r c r d r e
  e4 d8
}
