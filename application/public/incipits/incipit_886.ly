\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro assai"
  \time 4/4
    c,2 b8 c d c
    a a f'4 r2
    d2 cis8 d e d
    b b g'4
}
