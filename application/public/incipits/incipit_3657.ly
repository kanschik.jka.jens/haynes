 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 2/4
    r16 c b a g f e d
    c8 e4 g8 g c4 e16 c
}


\relative c'' {
  \clef alto
  \key c\major	
   \tempo "2. Allegro"
  \time 2/4
  r8 g g f
  e c4 d8 e g4 d8
  c4 g'
}
