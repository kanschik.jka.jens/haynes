 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/4
    c2 f,4 d'
    c8 bes16 a bes8 a16 g a8 g f4
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. o.Bez."
  \time 2/2
  f4 f16 a g f  e8 d16 c f8 a,
  bes16 c d8 d[ c16 bes] a8 f r a'
  g16[ c, a' c,] g'[ c, f d]
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. o.Bez."
  \time 3/4
  f8 e16 d c8 c c c
  f e16 d c8 c c c
  d f16 e f8 c bes a16 g a4 g r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}