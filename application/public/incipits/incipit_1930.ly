 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
    d2 es8 d c bes
    a4. c8 bes4 r
    f'2 g8 f es d
    c4. es8 d4 r
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegretto"
  \time 6/8
  \partial 8
  d16 es
  f4 f8 \grace f8 es8 d es
  d4 f8 bes,4 c8
  d8. c16 d8 es4 g8
  c,8. d16 bes8 c4
}

