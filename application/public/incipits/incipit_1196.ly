 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
    f2~ f8 g16 a  c,8 bes16. a32
    a16 g f8 r8 c' a g r c
    a g r f' d c r
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 4/4
    r8 c16 c c8 c  a f f d'
    c f, f f' e c c a'
    g c, r16
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\minor
                      \tempo "3. Adagio"
                      \time 3/4
                      R2. R R R
                      a2.~ a2 d4
                      cis2 a'4
                      g f4. e8 f4 d r
                      % Voice 1
                  }
\new Staff { \clef "bass" 
                     \key d\minor
                     d,,4 f' e
                     d c bes 
                     a g f 
                     g a a, 
                     d f' e
                     d c bes 
                     a g f 
                     g a a, 
                     d f' e
                     % Voice 2
                  }
>>
}



\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Bourrée angloise"
  \time 4/4
  \partial 4
  c4 
  c a bes8 a g f 
  d'4 f2 e8 d
  c4 bes8 a g a bes g
  a4 f2
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "5. Menuett"
  \time 3/4
    f4 c4. bes16 c
    g'4 c,4. bes16 c
    a'4 g8 a bes4 a g8 a f4
}