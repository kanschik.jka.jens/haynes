\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Aus Liebe will mein Heiland sterben [BWV 244/58]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Traverso"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key a\minor
                      \tempo "o. Bez."
                      \time 3/4
                      \partial 4
                      % Voice 1
                     a4 e'4.~ e32 f e d \grace d8 e8. c'16 \grace e,8 d2 b'4~
                     ~b16 c b a b a gis fis e d c b
                     b d c b \grace b8 a4 r16 a16 c e
                     f e f a
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe da Caccia 1"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key a\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                    r4 c,4 c c 
                    b b d
                    d d b
                    c8. d16 e8. d16 c8. b16
                    c4 c c
                       
                        
                
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe da Caccia 2"
                     \set Staff. shortInstrumentName = #"Ob 3"
                     \key a\minor
                     \set Score.skipBars = ##t
                        % Voice 3
                 r4 a4 a a
                 a a a 
                 gis gis gis
                 a8. b16 c8. b16 a8. gis16
                 a4 a a
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key a\minor
   \tempo "o. Bez."
  \time 3/4
  \partial 4
     \set Score.skipBars = ##t
    r4 R2.*12 r4 r4 e4
    e2.~
    ~e16 g f e f e d c b d c e 
    d e f4 e8 e
   
    
    
    }