 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 2/2
  c8[ d16 es] d8 g, d'[ es16 f] es8 c
  g'[ as16 g] fis8 d  f8[ g16 f] e8 c
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  g8
  es16 d es g  c es d c
  g'4. as8
  g f16 es d8 g
  es d16 es c8
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Siziliana"
  \time 12/8
  \partial 8
  bes8
  es,8. g16 bes8 es4 bes8 c4 bes8 es4 bes8
  c8. bes16 as8 f4 bes8 g4 f8 es8
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. o.Bez."
  \time 6/8
  \partial 8
  g8
  c g c  c es16 d c8
  c g c c es16 d c8
  d g, d' d f16 es d8
  d g, d' d f16 es d8
  es8 d c r r 
}