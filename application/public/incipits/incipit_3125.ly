\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Ich liebe den Höchsten von ganzem Gemüte [BWV 174/2]" 
    }
    \vspace #1.5

}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe 1"
  \key d\major
                      \tempo "o. Bez."
                      \time 6/8
                      \partial 8
                       \set Score.skipBars = ##t
                      r8  R2.*3 r4 r8 r8 r8 a8 e'4.~ e8. fis16 gis8
                      a8. gis16 a8 a,4 fis'8
                      e8. gis16 a8
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe 2"
   \key d\major
    \tempo "o. Bez."
                      \time 6/8
                     \set Score.skipBars = ##t
                        \partial 8
                        d,8 a'4.~ a8. b16 cis8
                        d8. cis16 d8 d,4 b'8 a8. cis16 d8 g,8. a16 fis8
                        fis8. e16 fis8 e4 
}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key d\major
   \tempo "o. Bez."
  \time 6/8
  \partial 8
     \set Score.skipBars = ##t
     r8 R2.*15 r4 r8 r8 r8 d8 a'4.~ a8. b16 cis8
     d8. cis16 d8 d,4
}