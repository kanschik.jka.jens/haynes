\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Andante"
  \time 4/4
  c8 b16 c d8 es d g, r4
  es'8 d16 es f bes, as bes g8 es r4
  g'8 f16 es d8 c b g r4
}
