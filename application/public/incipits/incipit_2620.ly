\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Ouvertüre"
  \time 2/2
    c4. c8 d4. es8
    d2. r8 f
    d4. c8 d4. es8
    c4.
}
      
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Passetemps. Vivement"
  \time 2/4
  \partial 8
  f16 e
  f8 c d a
  bes d r bes16 a
  bes8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Sarabande"
  \time 3/4
  a4 bes4. c8
  d2.
  e4 f4. g8
  e4.
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Rigaudon"
  \time 2/2
  \partial 4.
  c4
  f8 g a4 a g
  f8 g a4 a g 
  f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. Rondeau"
  \time 3/4
  \partial 2
    c4 f
    e8 g16 f e8 d c bes
    a f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "6. Polonaise"
  \time 3/4
  a8. bes16 c4 c
  d8 f c2
  bes8. c16 d4 es
   d8 bes16 a bes4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "7. Chasse"
  \time 6/8
  \partial 8
  c8
  f8. c16 a8 a8. c16 f8
  a8. f16 c8 c8. f16 a8
  c4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "8. Menuet"
  \time 3/4
  a8 bes c2
  c f4
  g f8 e d c
  f
}
