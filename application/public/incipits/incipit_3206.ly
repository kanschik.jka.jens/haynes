\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Gedenk an uns mit deiner Liebe [BWV 29/5]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key b\minor
   \tempo "o. Bez."
   \time 6/8 
   \partial 8
   \set Score.skipBars = ##t
   fis8
   cis8. e16 d8 e8. g16 fis8
   \grace e8 d4.~ d4 d8
   d8. b16 c8 g' fis32 e16. d32 c16.
   ais8. cis16 e8 e4
 
                                                                                   

}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key b\minor
   \tempo "o. Bez."
   \time 6/8
   \partial 8
   \set Score.skipBars = ##t
  r8 R2.*7 r4 r8 r4 fis8
  cis8. e16 d8 e8. g16 fis8
  \grace e8 d4.~ d4 d8
  d8. b16 c8 g' fis32 e16. d32 c16.
   ais8. cis16 e8 fis,4
       
}



