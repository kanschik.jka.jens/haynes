 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
    r2 r8 c c c
    c8. d16 e4. e8 d c
    c4 b r8 d d d
    d8. e16 f4. f8 e d
}

 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*48
   g'4 g2 f8 e
   e4 e2 f8 g
   \grace g8 a4 a4. g8 f  e
   e d d4.
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Rondo. Moderato"
  \time 6/8
  e4 r8 \grace f8 e8 d e
  g4. f4 r8
  d4 r8 \grace e8 d8 c d
  f4 e8 c'4 a8
  g8 e c
}

