\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Zudem ist Weisheit und Verstand [BWV 92/4]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key fis\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                       \set Score.skipBars = ##t
                       r8 fis8 cis16 b cis fis gis8 gis cis,16 b cis gis'
                       a8 cis fis4~fis16 eis fis dis eis8. fis16 fis8
                    
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key fis\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                        R1 r8 fis,8 cis16 b cis fis gis8 gis cis,16 b cis gis'
                        a2~ a8
            
                  }
>>
}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key fis\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
     R1*6 r4 cis8 d e4 fis
     e a a8. b16 gis4 a2 r2
}