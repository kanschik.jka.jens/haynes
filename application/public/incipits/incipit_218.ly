\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. [Vivace]"
  \time 2/2
  g'4 r8 d b8. g16 b8. d16
  g8. d16 g8. b16  g8. d16  g8. d16
  b4. b8 d4  g8. g16
}



\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Tremulo Adagio"
  \time 2/2

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 2/2

}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Largo"
  \time 2/2

}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Menuet altern."
  \time 2/2

}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "6. Trio"
  \time 2/2

}
