\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Ich säe meine Zähren [BWV 146/5]" 
    }
    \vspace #1.5
}
 
 
\relative c''' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Traverso"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key d\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                    r4 r8 d8 e16 d cis d g,8 bes~
                    ~bes a4 g8~ g16 f e f d8 a'
                    a1~ a2~ a4 r8
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key d\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                    R1 r2 r4 r8 f8
                    e16 f g4 f16 e f e f e d e f d
                    e f g e g f e d e4 r4
                       
                        
                
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 3"
                     \key d\minor
                     \set Score.skipBars = ##t
                        % Voice 3
                R1 r2 r4 r8 d8
                cis16 d e4 d16 cis d a d a fis g a b
                cis d e cis e d cis b cis4 r4
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key d\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    R1*16 r4 r8 d8 e16 d cis d g,8 bes
    bes a a g g16 f e f d8
   
    
    
    }