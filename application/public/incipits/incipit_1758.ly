\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Aria o.Bez."
  \time 2/4
  r4 r8 c
  f a16 g f8 g
  a  g  f  c'
}
