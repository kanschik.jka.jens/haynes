\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Thema. Andante"
  \time 6/8
   \partial 8
   c8
   f4 f8 \grace g8 f8 e f
   g4 g8 g4 gis8
   a4 a8 bes4 bes8
   g4.~ g4
}

