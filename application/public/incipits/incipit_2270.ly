 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Symphonie. Adagio"
  \time 2/2
  d2 g2.
  f4 es2
  d2. c4 d4. c8 bes2
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Bouree"
  \time 2/2
  \partial 4
  g'8 a
  bes4 a8 g g4 a
  d,2. r4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Sarabande"
  \time 3/4
  \partial 4
  g'8. fis16
  g4. f16 es d8. g,16
  es'4. d16 c bes8. g16
  a4 d,
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Menuet"
  \time 3/4
  d4 bes8 a g4
  g g' d
  a' fis8 e d4
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "5. Aria"
  \time 2/2
  g'4 d r a'
  bes g8 a bes a g bes
  a4 d, r
}
