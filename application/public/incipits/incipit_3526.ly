\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Larghetto - Allegro Vivace"
  \time 3/4 
    f2.
    c4 bes a a8. fis16 g4 r
    g'2. d4 c bes
}


