\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro assai"
  \time 3/4
    d4 r8 d16 e fis8. e32 d
    a'4 r8 a,16 b cis8. b32 a
    d8 fis d fis d fis
    g e cis e cis e
    fis4 d r
}
