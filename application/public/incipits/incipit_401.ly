\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 2/2
      f2 a,4. bes8 c2 r4 c
      d bes g c  a4. g8 f4 r
}
