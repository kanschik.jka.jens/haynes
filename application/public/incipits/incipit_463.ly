\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Preludio. Largo"
  \time 4/4
  r4 r8 e cis16 a cis e a8 e
  b16 dis e8 r b a16 fis a cis d8 a gis16 b cis8 r

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Corrente. Allegro"
  \time 3/4
  \partial 8
  a8 a4. e8 fis gis a b b4. a16 b
  cis4 a
}

\relative c'' {
  \clef treble
  \key fis\minor
   \tempo "3. Sarabande"
  \time 3/4	
  fis,8 cis' a'4 eis4 fis fis, d' cis b2
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Allegro"
  \time 2/4
  e4 gis, a4. fis'8
  e16 d e cis d cis d b
  cis a cis e a4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Largo e puntato"
  \time 4/4
\grace fis8 e8 d16. c32 b8 a g \grace fis8 e8 r16 b' cis16. dis32
e16. b32 b16. e32 fis16. b,32 b16. fis'32 g8 \grace fis8 e r4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Corrente"
  \time 3/8
\grace fis8 e fis g
b,16 e dis4
e16 dis e fis g8
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Adagio"
  \time 12/8
  b8. d16 c8 b8. fis16 d'8 cis4 eis8 fis4 cis8 
  b8. ais16 b8 g'4 b, b ais r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 2/2
\partial 4
    b4
    e2 e e~ e8 b cis dis e4 fis8 g a4 fis g fis8 g e4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 12/8
  b8. g16 fis8 e4 e'8 dis4. r8 r e
  fis e16 dis cis b a'8. g16 fis8 g16 fis e8 r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 4/4
  r4 r8 b g fis16 e b'8 e
  dis4 e fis16 e dis cis b a' g fis g e b g e4
}
  
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Adagio"
  \time 3/4
  b8 g' fis4 ais,
  b8. fis16 fis4.
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 2/2
\partial 4
  b4 e8 dis e fis g4 a
  b dis, e c
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  d8 a a b16 cis d cis d8 r16 d cis d
  e d e fis e fis d e fis8 d r
}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Adagio"
  \time 12/8
  d8. cis16 b8 fis'4 ais,8 b4.~ b8. fis16 b8
  cis4.
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 4/4
  \partial 8
  a8
  d,16 e fis g a8 cis d cis16 b a8 d
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Preludio Andante"
  \time 4/4
  \partial 8
  b8
  e b e fis g fis r dis e16 
  b g b a'8 g g fis r4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 2/4
  b'4 dis,
  e8 d16 c b8 e
  fis16 b, ais b fis' b, ais b
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Minuetto"
  \time 3/4
  e8 fis g e fis dis
  e4 b c c b
}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Largo"
  \time 3/4
  r4 d a d2 r4
  r a' cis, d4. e8 f4 e g8 f e d cis4. b8 a4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro Giga"
  \time 12/8
  \partial 8
  a8 d f e d cis d d4.~ d8 c bes
  a cis e g f e f e d r4 r8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro"
  \time 2/4
  \partial 8
  a8 d4 e a16 g f e d8 cis d e16 f g8 f e16 cis d b a8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. "
  \time 3/8

}

