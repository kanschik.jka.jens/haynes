\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}




\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro un poco Presto"
  \time 2/4
    g8. a16 g8 c
    c4 b8 a
    a4 g8 f
    \grace { e16[ f g]}  f4 e8 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante Grazioso"
  \time 3/4
    d16 b c a g8 r r4
    g'8 g \grace a8 g8 fis16 e d8 r
    d16 b c a g8 r r4
    e'8 e \grace f8 e8 d16 c b8 r
    
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Giga"
  \time 6/8
  \partial 8
  g8 c4 e8 c4 e8 c4.~ c4 g8
  a g a  b a b c4 c8 c4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "Trio"
  \time 6/8
    c8 d c c bes a
    c4. bes8 r r
    bes a g f g e e4. f4 r8
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Presto"
  \time 2/4
    bes8 bes4 d8 bes4 r
    f'8 f4 d8 bes4 r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Andante un poco Largo"
  \time 3/8
    g8 bes d
    d8. c16 bes8
    c bes a d8. c16 bes8
    c bes a
    bes16 g d' g, es' g,
    \grace g8 fis4 g8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Menuetto"
  \time 3/4
    bes4 bes' a g f r
    g8 es d f a, c c4 bes r
}
\relative c'' {
  \clef treble
  \key es\major
   \tempo "Trio"
  \time 3/4
    es2. c bes2 as4 g r r
    g2 f4 es bes bes bes'2 as4 g es es
}
