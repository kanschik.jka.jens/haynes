\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Sich üben im Lieben [BWV 202/7]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key d\major
   \tempo "o. Bez."
   \time 3/8
   \partial 8
  a8 d16 cis d8 a~
  ~a16 fis' e d cis d
  e d e8 a,~
  ~a16 g' fis e d e
  fis e fis8 a,8~a16
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key d\major
   \tempo "o. Bez."
   \time 3/8
   \partial 8
   \set Score.skipBars = ##t
    r8 R4.*21 r8 r8 d8 
    a d16 cis d8
    d e fis a, e'16 d e8 e fis
}



