\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
   \time 6/8
   d8. e16 d8 g b, c
   d8. e16 d8 g b, c
   d8 g fis e8. fis16 d8
   c8 b8. c16
   a4 d8
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
   \time 2/4
   g4~ g8 b16 c
   d c d e  d8 d
   e d4 g8
   c,8 b4 g'8
   a, g4 a8
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
   \time 3/4
   d4 g,4. d'8
   e4 d c b4. d8 c b
   a4 d,2
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga"
   \time 12/8
   \partial 8
   d8
   g a g  fis e fis g4.~ g8 fis e
   d e d  c b c b4. r8
}
