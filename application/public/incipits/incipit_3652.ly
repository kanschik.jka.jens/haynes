\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 4/4
      g'8 d4 c8 bes g4 a16 fis
      g8 es' d c bes a4 a16 fis
      g8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. o.Bez."
  \time 3/4
    r8 g' g c, c b
    r8 g' g c, c b
    r g'4 c, as'8
    b,16 a g8
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. o.Bez."
  \time 2/4
      g4 es' \grace g,8 fis4. g8
      a bes c d es d4 c8
      bes16 a g4.
    
}

