\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8
  a8
  \times 2/3 { fis16[ g a] } d,4 d'8
  \times 2/3 { cis16[ d e] } a,4 g'8
}
