\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key c\major
    \time 4/4
    \tempo "1. o.Bez."
      c2 g4 r
      c8 e16. d32 c8 c d16 e32 fis g[ fis g g,] g16[ d'32 e] f e f d
}

 