 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  f,8 g a g  f4 f
  f8 e e4. c8 d e
  f g a g f4 f
  \grace { e16[ f g] } f8 e e4. c8 d e f2
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Romance. Adagio non tanto"
  \time 2/2
  e4. e8 f e d c
  b c d c  c4 g8 r
  d' d e f e e f g
  a g f e \grace e8 d4 r
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Rondeau. Allegretto scherzando"
  \time 2/4
  \partial 8
    c8 
    a'8. bes32 a g8 a bes2
    a8 c bes a g f e f16 g
    a8. bes32 a g8 a bes2
}