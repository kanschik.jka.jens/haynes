\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "1er Trio " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Presto/Allegro"
  \time 2/2
  \partial 2.
  d4 d d 
  d c8 bes a4 g 
  fis fis8 g a bes c d 
  e,4 e fis fis g g g g
  c,

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Sarabande"
  \time 3/4
 g4 b, c
 \grace c4 d2 a4
 c b a
 \grace a4 b2 \grace a4 g4
 
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "3. Gigue"
  \time 6/8
 \partial 4.
 g8. a16 g8
 bes8. a16 g8 g8. a16 g8
bes8. a16 g8 fis8. g16 a8
 d,8. c16 bes8 a8. bes16 c8
 d4.
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Adagio"
  \time 3/2
  r2 bes bes
  es g bes
  d, f d 
  c es4. f8 g4. g8
  \grace bes4 bes2 

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "5. Corrente. Allegro"
  \time 3/4
  \partial 4
  r8 d8
  g4 d bes
  g8 fis g a bes c
  d4 c8 bes a g
  fis a d
  a fis' d
  a' d, fis d a' d, d'4

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "2me Trio " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 4/4
  d8 g, g d' e e e e 
  e d r8 d16 b g8 b b b 
  c4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allemande. Allegro"
  \time 4/4
  \partial 8*5
  d8 g4. fis16 d
  d8 g fis e d16 e d c b c b a 
  g8 d' g b a16 b a g a b c a
  b8 g e d
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gavotte en Rondeau"
  \time 4/4
  \partial 2
  d4 g
  b,4. a8 b4 c
  d2 \times 2/3 {g8[ d g]} \times 2/3 {b8 a g}
  \times 2/3 {a8[ g fis]} \times 2/3 {g8 fis e} a4. g8
  fis4 d 
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Gigue. Allegro"
  \time 6/8
  \partial 8
  g8 d'4 d8 d e d 
  d a b c4 c8
  c d c c b a 
  b4.~ b4 


}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "3me Trio " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  r2 r8 a8 fis d
  g4. e8 a a, d d 
  d4 cis d b
  a8 a a a d16 cis d e d fis e d

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 3/4
 \partial 2
 r4 r4 
 r4 d4 d
 b' b b 
 a a8 b g a
 fis4 fis8 g fis g
 e4 fis8 g a fis 
 g a g fis e d 
 cis4
 
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "3. Premier Menuet"
  \time 3/4
  a4 g8 fis e d
  cis4 e8 e e e
  a4 g fis
  e8 d cis b a4

 
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Presto"
  \time 4/4
  \partial 8*5
  a8 d e fis g
  a[ d,] r8 a'8 b a g fis 
  e16 d e fis d cis d e cis b cis d e8 a
  gis[ a]

}

  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "4me Trio " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 3/4
  f8 es f g es f
  g4 g, g'
  g8 f g a f g
  a4 f a
  bes a8 g f es
  d c bes4

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allemande"
  \time 4/4
 \partial 8
 f8 d[ c d16 es f8] bes, f bes d
 c[ bes c16 d e8] f4 r8
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Andante"
  \time 6/4
 g4. a8 g4 bes d g
 bes4. a8 g4 es c a 
 fis2. g4. a8 g4
 d' g bes c,4. bes8 a4
 bes2 g4
 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Aria. grazioso"
  \time 6/8
  \partial 2
  f8 g f es
  f bes, f' g f es
  f f f f g a 
  bes a g f4 es8
  d bes f' g f es 
  f bes, f' 
  

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "5. Allegro"
  \time 2/4
  f4 d
  bes4. c16 d
  es8[ g f es]
  d bes r8 f'8
  g16 f g a g bes a c
  f, bes a bes es, f d es
  
  

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "5me Trio" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Larguero"
  \time 3/4
  \times 2/3 {c8 bes a} \times 2/3 {d8 e f} \times 2/3 {bes,8 a g}
  a8. c16 f4 f
  d8. c16 f4 f

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Presto"
  \time 2/4
 f4 c8 c
 f,16 g a bes c d e f
 g8[ bes g bes]
 a16 f, g a bes c d e
 f8 a a g16 f
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Andante"
  \time 6/4
 \partial 2.
 f2 f4
 d4. cis8 d4 d cis d
 a2 a4 a'2 a4
 f4. e8 f4 f e f 
 e2 e4
 
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Premier Gavotte en Rondeau"
  \time 2/2
  \partial 2
  f4 c 
  d c d e 
  f2 bes4 a8 g
  f c d c f c g' bes
  a4 g f c
  d c d e f2

}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "5. Adagio"
  \time 2/2
 \partial 2
 f4. f8
 f4 as g f
 es2 es4. es8
 d4 es es4. d8
 c2 
 
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "6. Allegro"
  \time 3/8
  f8 a f
  c g' bes
  a g f
  e c' bes
  a16 g f e d c
  d8 bes' a
  g16 f e d c bes
  a4 g

 
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "6me Trio " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8*5
  g8 c16 g c e c g c e
  d g, d' f d g, d' f e8 d16 e f e d c
  d8 g, r16 fis'16 g a b8 d, d g
  g4 fis
  

}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "2. Sarabande"
  \time 3/4
 g4 as g
 f4. es8 d4
 es4 f d
 \grace d4 es2 \grace d8 c4
 fis fis fis
 g4. g8 a4
 bes a4. g8 g2.
 
 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/8
  g8 c, a'
  g c, c'
  b16 a g8 f
  e16 f g8 g,
  a a' a
  a g16 f g8 
  g c, f
  e16 g f e d c 
  d4.
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Fugue"
  \time 2/2
  \partial 2
  g4 g
  e' g8 f e4 d8 c
  d4 g, g' g
  g c, f f 
  f g8 f e4 f8 e
  d c d e f4 e8 d 
  e4 c r2

}
