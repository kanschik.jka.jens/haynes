\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Jesu, deine Gnadenblicke [BWV 11/10]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Traverso 1+2 un."
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\major
                      \tempo "o. Bez."
                      \time 3/8
                      % Voice 1
                    d8 e d 
                    g b,4
                    c8 d e
                    d c16 b a g
                    fis' g g8. fis32 g
                    a8 c,4
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \set Score.skipBars = ##t
                     \key g\major
                        % Voice 2
                   b4.~ b8 g' f
                   e d16 c b a
                   b8 c d 
                   c4.~ c16 d e fis g a
                   
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key g\major
   \tempo "o. Bez."
  \time 3/8
     \set Score.skipBars = ##t
    R4.*16 d8 e d
    g b,4
    c8 d e
    d c16 b a g 
    a b b8. a32 b
    c4.
   
    
    
    }