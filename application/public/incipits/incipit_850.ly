\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/2
  r8 b16 c d8[ fis,] g[ b16 c] d8 fis,
  g g'16[ fis] g8 g, c[ c] c c
  c b r16 b' a b
}
