 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 4
  cis'8 d
  a2 \times 2/3 { a8[ b g] } \times 2/3 { fis8[ g e] }
}
