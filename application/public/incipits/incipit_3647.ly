  \version "2.16.1"
    #(set-global-staff-size 14)
\relative c'' {
  \clef treble  
  \key f\major
    \time 4/4
  r8 f d bes es4. d16 c|
  d8 g16 f f8 es16 d c4. r8
}