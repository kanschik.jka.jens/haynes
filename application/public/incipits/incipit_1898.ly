\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "Adagio"
  \time 2/2
  c2 f4 a,
  c8 b bes4 r bes
  bes g'4. e8 c bes
  c16 bes a bes a4 r2
}
