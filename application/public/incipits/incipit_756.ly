 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro"
  \time 2/2
  \partial 8
  e8 
  a,4 f' e8. d16 c8 b
  a4 a' gis
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Largo"
  \time 3/4
  c2 e8 f g4 c, d16 c d8
  b a g4 d'8 e
  f d d b g32 a b c d e f g
  e8 d c4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Allegro"
  \time 2/4
  \partial 8
  e8
  a,16 b c d e8 f
  e a,4 f'8
  a,16 b c d e fis gis a
  f8 r e r
  r e d c
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}