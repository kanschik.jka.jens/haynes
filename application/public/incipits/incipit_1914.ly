\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 2/2
  es2. g8 es es4 d r2
  f~ f8 g as as,
  bes8. as16 g4 r2
}
  
