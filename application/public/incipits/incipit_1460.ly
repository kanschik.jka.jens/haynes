 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 2/4
  \partial 8
  d16. e32
  f8 f16. g32 e8 e16. f32
  d4 a8 d16. e32 f8 f \grace a16 g16 f e d
  \grace gis8 a4
}


