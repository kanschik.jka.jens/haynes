\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ah! vous dirai-je Maman" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Marche. Gravement"
  \time 2/2
   \override TupletNumber #'stencil = ##f
  a'2 a a2. cis8 b
  a4 cis a cis
  e,4. \times 2/3 { a16[ gis fis] } e4. fis16 gis
  a4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Mon coeur volage" 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "1. Gay"
  \time 2/4
  g,4 c8 es es4 d
  g, d'8 f f4 es
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Le tambourin anglois " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Tambourin Anglis. Gayment"
  \time 2/2
  \partial 2
  a4 a
  a2 cis4 d8 cis d4 gis, d' d d2 fis4 e8 d e4 cis
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Les Folies d'Espagne" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Largo"
  \time 3/4
  r4 r c
  as'4. g8 f4 g c, c'
  d4. es8 f4
  b, g
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Vos Beaux Yeux " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Ouverture. Grave et Piqué"
  \time 2/2
  \partial 8*5
  a8 cis4 e
  a4. a,8 cis4 e
  e,4. gis8 b4 d
  cis4.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Vivre sans Aimer" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Vivre sans aimer"
  \time 2/2
  gis'4. a8 gis4 fis8 e
  fis2. gis4
  e dis e fis
  dis2 \grace cis8 b2
  e
}
