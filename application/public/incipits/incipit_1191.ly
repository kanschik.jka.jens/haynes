 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro moderato"
  \time 3/4

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Cantablie"
  \time 2/2
    c'2 c4 d8 c
    c2 bes4 a
    g2 f8 e d e  g4 f r2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Rondo. Allegretto"
  \time 2/4
  \partial 8
    c16 d
    e8 e16 d c8 c16 b
    a4 d8 e16 d
    c b a g a8 b
    c c c
}

