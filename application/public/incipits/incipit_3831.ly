 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 4/4
  \partial 8
  g8
  c4 c8. c16 c4 c
  \grace c8 c'2. b8 a
  g e c e  e d c b
  d16 c b c g8 r r
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Adagio"
  \time 2/4
  \partial 8
  c8
  e16 f c8 r16 c c c
  e f c8 r16 d d d
  \times 4/6 { c16[ d e f g a]} c,8 bes
  bes16 a d c c8
}

