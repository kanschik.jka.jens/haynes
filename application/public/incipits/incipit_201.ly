\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Largo"
  \time 4/4
    d4~ d16 e fis g a8 a a a
    fis4 e fis16 e fis g  fis a g fis
    e4 a a16 g fis8 r4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 2/2
    d2 e fis g
    a4 fis e2 d4
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Largo"
  \time 3/4
    fis4 b, g'~
    g8 fis e d cis b
    e2~ e8 fis16 g ais,8 g' fis4. e8 dis
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Allegro"
  \time 3/8
    a8 d cis  b e d
    cis a16 b cis a
}

