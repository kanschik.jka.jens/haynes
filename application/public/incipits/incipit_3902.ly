\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Andante"
  \time 2/4
  r4 r8 bes
  d8. es16 d8. es16
  \grace es8 d16 c bes8~ bes16 a' bes f
  \grace f8 es8 d4 c8
  \grace es8 d16 es f8~ f16 bes a bes
  
}
