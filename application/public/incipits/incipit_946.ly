\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  r8 c16 c c8 g c e16 e e8 g
  e c16 c c8 e c g16 g g8 c
  b16 a g a b c b c d8
}
