\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 3/4
  a8 c bes a g f
  f'4 e d
  c8. d16 \grace c8 bes2
  a4 g8 f e f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegretto"
  \time 4/4
   \partial 8
   c8
   \grace bes8 a8 g16 f c'8 f \grace f8 e4. c8 d e4 f16 d c8 e4 f16 a,
   bes8 a4 g8 a8
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Adagio"
  \time 4/4
      r4 r8 a a g r bes
      bes a r g a16 g bes8 \grace a8 g4
      f r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro ma non poco"
  \time 3/8
  c16. d32 c8 bes
  \grace bes8 a4 g8
  f d' c
  \grace c8 bes4 a8
}
