 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato"
   \time 4/4
      bes,4 r r bes'
      \grace { a16 bes} c4 bes \grace { c16 d} es4 d
      \grace { es16 f} g4 f r bes,8 r
      c r bes r
}


\relative c'' {
  \clef treble
  \key f\major
       \tempo "2. Andante molto"
   \time 4/4
      f8 c a f cis'4 d
      d8 f bes d,  b4 c
      f,8 a a c c16 bes a g  g f e d

}
\relative c'' {
  \clef treble
  \key bes\major

   \tempo "3. Allegro"
   \time 2/4
      bes2 d16 bes8. f'16 d8. g4 g~ g bes8. g16
      f4 f~ f bes8. f16
}
