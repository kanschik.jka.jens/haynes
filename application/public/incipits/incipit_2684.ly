 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  d,8 fis a d fis d4 e8
  fis8 d4 e8 fis d e cis d8 a4 cis8 d a4 cis
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Andante"
  \time 3/8
  \partial 8
  a8
  a b a g4 fis8
  g8 e4
  d16 e fis g a8 a b a
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Allegro"
  \time 6/8
  \partial 8
  a8
  fis4. g
  a8 fis d
  d'4. d cis
  d8 e16 d cis b cis8 a cis
}

