\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato [Violin]"
  \time 4/4
  c4 e c r8 g,8 
  c4 b8 c d e d4
  d dis8 dis e f g4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato [Oboe]"
  \time 4/4
  c8. g16 c4. c8 d e
  d4 c r2
  e8. c16 e4. e8 f g
  f4 e r2
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [tutti]"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [solo]"
  \time 4/4

}
