
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro"
  \time 2/4
\partial 8
d8 g g g fis 
g f16 es d8 d 
g g g f es4 d8 d
g g g fis g f16 es d8 d 

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio"
  \time 3/4
 d4 d d d8 g g f d f es4 es es
es8 a a c c d, 


}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro"
  \time 3/8
 \partial 8
d8 g g fis
g f16 es d8
g g f es4 d8
g g fis
g f16 es d8
g g f
es d f


}

