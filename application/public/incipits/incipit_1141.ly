 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Vivace e sostentato"
  \time 6/8
  \partial 8
  c8
  c4. d4 c8
  c4.~ c4 f8
  c4.~ c4 f8
  a,8. bes16 c8 a8. bes16 c8 d4 r8 c4 r8
}


 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. pizzicato"
  \time 4/4
  r4 r8 a e'4 r8 a,
  f'4 r8 g \grace f8 e16 d c8 r f
  \grace e8 d16 cis d8 r
}

 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Vivace e un poco allegro [Tutti]"
  \time 6/4
  f,2 g4 a bes c
  d c bes a2 g4
  f8 g a bes c d es4 d r
}

 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Vivace e un poco allegro [Solo]"
  \time 6/4
     \set Score.skipBars = ##t
   R1.*10 
   c2.~ c2 a4
   d2.~ d2 bes4
   f'2.~ f~
   f~ f2 r4
}
