 
\version "2.16.1"


#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key c\minor
                      \tempo "Grave"
                      \time 4/4
                      r4 es8 g es[ c d a]
                      bes g es' a,16 g fis8 g32 a bes16 a8. g16 
                      g4
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key c\minor
                     % Voice 2
                     es'8 g es d  c4 r8 a
                     g bes c16 d es8  a, bes16 g fis8. g16
                     g4
                  }
>>
}

