\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Treu und Wahrheit sei der Grund [BWV 24/5]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key a\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                   e4. b8 c gis a b
                   e,2~ e8 e' d16 c b a
                   f'8 e16 d b8.
                     
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key a\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                    r2 e4. b8 c gis a b
                   e,2~ e8 a g16 f e d 
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key a\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
     R1*10 e4. b8 c gis a b
     e,2 r2
}