\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
  bes4. c8 bes4 r8 d
  c r bes r
}
  
