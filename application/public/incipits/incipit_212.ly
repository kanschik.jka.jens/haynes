\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key es\major
    \time 3/2
    \tempo "1. Adagio"
        r2 es, es
        r g g
        r f f
        es es d
        es r4 bes' es d
}

\relative c'' {
  \clef treble  
  \key es\major
    \tempo "2. Allegro"
       
}

\relative c'' {
  \clef treble  
  \key es\major
    \tempo "3. Adagio"

}

\relative c'' {
  \clef treble  
  \key es\major
    \time 3/4
    \tempo "4. Menuet alternative"

}

