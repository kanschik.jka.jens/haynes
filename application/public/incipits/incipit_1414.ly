\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
  a4
  d2 d8 fis a fis 
  d4 a d fis
  e8 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 2/4
  d4. e16 g
  d4. e16 g
  d8 c32 b a g c4
  b r8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Rondo"
  \time 2/4
  \partial 4
  d8 d
  e4 d16 cis b cis
  d fis a fis d8 d
  e4 d16 cis b cis d4
}
