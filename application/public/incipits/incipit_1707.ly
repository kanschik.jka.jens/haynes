\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allemande"
  \time 4/4
  \partial 16
    f16
    f4~ f16 g f e d8. e16 e8. d16
    cis2 r8 d d16 c bes a
    \grace a16 bes4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allemande. Grave"
  \time 2/2
  fis,4~ fis16 g fis g g4. fis16 g
  a4 fis b cis d2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allemande"
  \time 2/2
    a4. b8 b4. a16 b
    c8. b16 a8[ b] c8. d16 d4
    e2
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. "
  \time 2/2

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allemande. Gaye"
  \time 4/4
  \partial 16
  e16
  e4~ e8[ d32 cis b] cis8.[ b32 a gis] a8.[ b16]
    
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allemande. Grave"
  \time 4/4
  \partial 8.
  e,16[ f g]
  a4. bes8 g4 f
  c'2~ c8 bes a16 g f e
  d4 bes' bes a8. bes16
  g2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key fis\minor
   \tempo "1. Allemande. Gaye"
  \time 4/4
  \partial 8
  fis8
  fis4~ fis16 fis e cis d8 b cis gis a2.
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allemande"
  \time 4/4
  r4 r8 d, g g16 a32 bes a8 fis
  g4~ g16 bes a g f8 f16 g  f g f g
  f4. es8 d4
}

