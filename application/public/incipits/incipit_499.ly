\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 4/4
\partial 2
  d4 d8 d
  g4 d r d
  b a8 b c4 b8 a

}
