 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andantino"
  \time 2/4
  \partial 4 
  g8. g16
  c8. \times 2/3 {d32 c b} c16 r16 c32 \times 2/3 {d16 e f}
 f16 e e8 r16
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4
  e4 e8. e16 e4 r8 c8
  c b16 r16 b8 a16 r16 a4 g8 a16 b
  c8 d e f g r8
}