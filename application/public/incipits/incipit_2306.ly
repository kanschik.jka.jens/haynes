\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 3/4
  g'4 d4. r16 g
  a8. fis16 g4. r16 d
  a'4. r16 c,
  b8. a32 g g'4.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro assai"
  \time 4/4
  g4 b8 a16 g d'4 r16 d e fis
  g8 g \grace b8 a8 g16 fis g8 d g,4
  b'8 b d b b a r4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Adagio"
  \time 4/4
  cis4~ cis16. a'32 e16. cis32  d4~ d16. a'32 fis16. d32
  g4~ g16. b32 a16. g32 fis16 e fis8 r16
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro assai"
  \time 3/8
  g8 b d g b d
  fis,16 g \grace a8 g4 \grace g8 a4.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 4/4
  d4~ d16 g fis g  d4~ d16 g fis g
  e4~ e16 g fis g  d4 c
  \grace c8 b4
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 12/8
  \partial 8
  d8 d4 d8 d4 d8 \grace fis8 g4.~ g8 b d,
  \grace fis8 g4.~ g8 b d, 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. o.Bez."
  \time 2/4
  \partial 8
  g8 g b4 d8
  g fis16 e d8 g
  a, g' fis c
  b a16 b g4
  \time 3/4
  d'4 g fis g d es
  c8 es d4 c b a8 b g a
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata [III] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  g4~ g16. d32 g16. b32  d8 d, r d'
  e c r16 a d16. fis32 \grace fis16 g8 g, r g'
  a, g' b, g' c, g' g, d'
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro assai"
  \time 4/4
  g4 g g b16 c d b
  g4 g g b16 c d b
  g4 g g16 d g b d g d b
}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Minuet"
  \time 3/4
  g8 d g b d[ g]
  \grace g4 fis2.
  e16 c8. d16 b8. c16 a8.
  b16 a g8 g2
}
  

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 12/8
  d4. d8. g16 d8 \grace d8 e4. e8. g16 e8
  d4 g,8 \grace d'8 c4. \grace c8 b2.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  d,8 g b d fis, g g'4 g,8
  a e'4 d16 c b a g8 r d
  e c' b16 a g fis g8[ \grace { c16[ d] } e8] d16 c b a
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Adagio"
  \time 4/4
  \partial 8
  b8 c c c g' \grace g16 fis eis fis8 r fis,
  b b b fis'\grace fis16 e dis e8 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Andante"
  \time 4/4
  r8 d es4 r8 cis d4
  r8 b c4~ c8[ \grace { c16[ d]} es8 d c]
  bes a g4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Presto"
  \time 3/8
  g8 bes16 a g fis
  g8 a16 bes c a bes a g a bes c
  d es fis g a fis
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Minuetto"
  \time 3/4
  g2. b
  d4 g8. b16 a8. fis16
  g4 d2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro moderato"
  \time 4/4
  g4. d'16 c32 b \grace b8 a4. e'16 d32 c
  b16 d c e  d b c a
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Adagio"
  \time 4/4
  b4 e~ e8 fis \grace e8 dis4
  e8 b g'4~ g8 a \grace g8 fis4
  g8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Grazioso"
  \time 3/4
  g4 b c
  d2 e16 fis g8
  a,4 b c \grace c8 b2 e16 fis g8
  a,4 b c
  b c8 b d c
  e d c4 b
  b a r
}
