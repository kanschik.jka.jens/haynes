\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Sonatina"
  \time 6/8
  d8 f f, bes4 r8
  d f f, bes bes bes
  d d d f bes,16 c d8
  c16 bes c d c es
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Aria"
  \time 6/8
  r4 d8 c8. d16 es8
  d d d c8. d16 es8
  d4
  }
