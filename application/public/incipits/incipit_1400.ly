 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
       \partial 8
       es,8
       bes'2~ bes4. as8
       \grace as8 g8 f16 es
       c'8 c \grace d8 c8 bes r
}
 