 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major	
   \tempo "Andante"
  \time 2/4
  \partial 8
  \times 2/3 { c16[ e d]}
  \grace d8 c4 r8 \times 2/3 { c16[ e d]}
  \grace d8 c4 r8 c
  d d d e16 f e16. f32 g8
}
