\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Poco lento"
  \time 2/4
      f,4 bes
      d2
      \grace d16 c8 bes c d
      c4 bes8 r
      bes4 d f2
      \grace f8 es8 d es f
      es4 d8 r
}
