\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro Moderato"
  \time 4/4
  f2~ f8 bes d, g
  f es es4. f8 c f
  es d d4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 3/4
  a'4 g f f e f g a bes c4. bes8 a r
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Rondo"
  \time 6/8
    f4 d8 f a bes
    f4 d8 bes d f
    es c a f c' es
    d g f f r4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  f4 a c r8 c
  bes4 g e r8 g
  f a c, c c4. bes8 bes a a4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Minuetino"
  \time 3/4
    g'4 f16 e d e f g a c, e4 d e
    g16 f e d c b a g a'8. g16
    g c g e f a f d e8 r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Presto"
  \time 3/8
  \partial 8
  c8
  f4 a8 g e c
  f4 a8 g r c
  c
  bes g c a f
  e bes' a g4 r8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro Grazioso"
  \time 4/4
  g'8. e16 g4 r8 e16 f g e f d
  c8 c c4 r16 c d e f e f g
  a8. f16 a4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondo"
  \time 6/8
  e4 g8 \grace d8 c b c
  d4 e8 f4 a8
  g4 e8 d4 g8
  f4 e8 r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  f2~ f8 a f c
  a4. bes8 c d e f
  e f g a bes r r4
  
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio"
  \time 3/4
  d2 f4
  \grace e8 d4 cis d
  e r r
  g f e f8 \grace g8 f16 e f8 g a r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo"
  \time 6/8
    c'4 a8 c bes g
    f4 c8 f4 g8
    a4 f8 \grace c'8 bes4 a4
    g8[ a f] g4 r8
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/2
    c2 e4 g
    c r8 g e4 r8 g c,4 r g'4. f8
    e4 e f g a4. b8 c4 r8
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio"
  \time 3/4
  a8. c16 e8 a gis a
  \grace g8 f4 e r
  d8 a' g f e d
  c8. d16 e4 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo"
  \time 6/8
  g'16 e f fis g gis a4 g8
  f a16 g f e d8 r4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allegro"
  \time 2/2
  \partial 4
  b8. b16
  e8 \grace fis8 e16 dis e8[ fis] g \grace a8 g16 fis g8[ a]
  c b b4 r8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Grazioso"
  \time 4/4
    \partial 2
    d4 d
    d b8 r e fis g e
    d4 b8 r g4 g 
    \grace b8 a4 g \grace d'8 c4 b
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. "
  \time 2/4

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
