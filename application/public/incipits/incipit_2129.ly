 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

 \relative c'' {
  <<
  \new Staff { \clef "treble"   
                       \key c\major
                        \tempo "1. "
                        \time 4/4
                        % Voice 1
                        R1 R R
                        c8 g'16 f e8 d16 c d8 g, g' g16 f
                        e8
                        
                    }
  \new Staff { \clef "bass" 
                       \key c\major
                          % Voice 2
                         c,,8 g'16 f e8 d16 c g'8 g, g' g16 f
                         e8 d16 e c8 d16 e f8 e16 f g8 f16 g e8 f16 g
                         a g f a g8 g,
                         c4
                    }
  >>
  }
  
 
\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2."
  \time 4/4
  es4 es es r
  es es es d
  des
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3."
  \time 3/8
  g'8 g g e c c
  d d16 e f d e8 e e
}
