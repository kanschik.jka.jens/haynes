 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/4
    f2 c4. a8 f4 f8. f16 f8 a c f
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Menuett moderato"
  \time 3/4
  c'2 \grace c16 bes8. a32 bes
  a4 e f
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Adagio"
  \time 4/4
  c4~ c16 bes bes a d4~ d16 bes g e
  f f a f c8 bes a4 r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Menuett poco vivace"
  \time 3/4
  \partial 4
    f4
    \grace f8 e4 d8 c d4 c2 d4
    g, g c f, r
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "5. Finale. Presto"
  \time 6/8
  \partial 8
  f,8
  f16 g f e f8 a16 bes a g a8
  c16 d c b c8 f4 c8 c bes' bes c, a' a
}