 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato"
  \time 2/2
d8. es32 d f8[ f] f4 f
f8. d16 r4 f8. d16 r4
g8. f32 es d8 c bes \times 2/3 { d16[ es d]} f8 f
f4 f f8. d16 r4
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Andante"
  \time 2/2
  a8 a32 bes c bes64 a a8[ a] d d32 e f e64 d d8 d
  g8
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Allegro Grazioso"
  \time 3/8
  \times 2/3 { d16[ es d] } f4
  f16 d bes8 r
  \times 2/3 { es16[ f es ] } g4
  g16 es bes8 r
}

