\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Affetuoso"
  \time 2/2
  f4. \times 2/3 { c16 d e }
  f4. \times 2/3 { c16 d e }
  f8 d c bes a f r4
  d'4 d16 e f g
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/4
  r8 f, a c
  f4. g8
  a f4 g16 a
  g8 c, e g a f4 g16 a
  g8 c, r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Largo"
  \time 4/4
  R1
  r2 a~
  a4. cis8 d4. cis8
  d4 r8 f e g4 e8
  cis16. e32 a,16 bes4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Tempo di menuet"
  \time 3/4
  f4 g e
  f8 e16 d c8 bes a g
  f16 c' bes c a' c, bes c
}