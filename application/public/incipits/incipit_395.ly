 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\major
                     \set Staff.instrumentName = #"Violin"
                     \set Staff. shortInstrumentName = #"Vn"
                      \tempo "3. Allegro"
                      \time 4/4
                      r4 r16 a b cis  d8 a d a
                      d16 cis d e d e cis d e8 a, e' a,
                      e'16 d e fis e fis d e  fis8 d fis fis
                      g d g d  fis d fis d
                      g d g d fis4 e8 e
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key d\major
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob"
                        % Voice 2
                       r1 r r2 r4 a8 a
                       b16 a g a  b a g b
                       a g fis g a g fis a
                       b a g a  b a g b
                       a8 d, cis cis
                  }
>>
}


\relative c'' {
  \clef treble
  \key d\major
  \set Staff.instrumentName = #"Soprano"
   \tempo "3. Allegro"
  \time 4/4
     \set Score.skipBars = ##t
     R1*5
      r2 d4 cis8 d 
      e cis a g' g4 fis
      d16 e fis g a8 g16 fis e8 cis a4
}