\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Vivace]"
  \time 2/2
  c4. g8 c g c d
  e4 c8   e c e f
  g a g a  f g f g
  e f e f  e4
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allemande"
  \time 2/2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. March"
  \time 2/2

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Aria"
  \time 2/2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Tempo di Gavotta"
  \time 2/2

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "6. Duetto"
  \time 2/2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "7. Arie en Menuet"
  \time 2/2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "8. Trio"
  \time 2/2

}
