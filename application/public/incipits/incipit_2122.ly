
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
  \tempo "1. Adagio"
  \time 3/4
  \grace e8 d2 \grace c8 b4
 \grace a8 g2 g8 b
 a \grace c8 b8 \grace d8 c4 c
 \grace d8 c4 b r4
 
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
  g4 d8. c16
 \grace c8 b4 r8 \grace {c16 [d]} e8
 d g, \grace b8 a8 g16 fis
 g4 
 
 
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Menuetto"
  \time 3/4
  g4 g4. b8
 a8. fis16 \grace e8 d2
 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 3/8
  \grace f8 e8. d16 c8
 \grace a'8 g8. f16 e8
 c' b a 
 a32 f c' a g8 r8
 
 
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 6/8
  \partial 16
 g16 c4 e8 d4 f8
 e4. f4 g16 a
 g8 e c d4 e16 f e4.
  
 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Menuetto"
  \time 3/4
   g2. f2. \grace f8 e4 d c
  a8. d16 \grace c8 b2
  \grace b8 c2 g4
 a8 cis d e f a,
 g b c d e g,
 f4
   
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Largho"
  \time 3/4
   d4 d, r8 r16 fis'16
  e4 a, r8 r16 g'16
 fis8 fis e e d d 
 cis d16 e d4 r4
 
 
 
 
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro non molto"
  \time 4/4
  \partial 8
  a8 d16 cis fis e d cis b' a a g g4 e8
  fis16 e32 fis g16 b, b8 cis a'16 g g4
  
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Giga"
  \time 6/8
   \partial 8
  d8 d a fis' fis d a'
  \grace a8 g4. fis
  e8 g fis e d cis
  d fis a b fis a 
 \grace a8 g4. fis
 e8 b' a g fis e d a fis d4 r8
   
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Cantabile"
  \time 4/4
  a2 cis4. d8
 \grace fis8 e4 e2 \times 2/3 {fis8 cis e}
 \grace e8 d4 d2 \times 2/3 {e8 b d}
 cis4 cis2
 
 
 
}
 
\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Larghetto"
  \time 3/8
  \partial 8
 a8 \grace g fis8 e d
 \grace d8 cis4 d8 
 \grace cis8 b8 a g 
 \grace g8 fis4 g8
 a b \grace fis'8 e
 \grace d8 cis4 d8
 \grace a'8 g8 fis16 e fis g
 e8
  
 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Menuetto"
  \time 3/8
   a4 b16 gis
 a4 b8 cis4 d16 b
 cis4 d8 e4 a8
 \grace gis8 fis8 e r8

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  g4 d \grace c8 b4 r8 d8
 dis4 e8 g cis,4 dis8 g
 gis4 a16 a, b c \grace c8 b4 r8
 
 
 
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante"
  \time 2/2
 e4 e \times 2/3 {e8 g fis}  \times 2/3 {e8 b g'} 
 fis4 fis4~ \times 2/3 {fis8 a g} \times 2/3 {fis8 b, fis'} 
 g4 gis4~ \times 2/3 {gis8 f e}  \times 2/3 {d8 c b} c4
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Menuetto"
  \time 3/4
  d4 c8 b a g
  \grace b8 a4 \grace g8 fis4 g
 \grace d'8 c4 \grace b8 a4 b
 e4 \grace {d16 [e]} d4 r4

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Allegro pastorale alla Calabrese"
  \time 6/8
  \partial 8
 g8 e4 c8 g4 e'8
 e4. d4 e8
 f4 d8 g,4 f'8
 f4. e4 g8
 \grace f8 e4 \grace d8 c8 e8. d16 e8
 d8. e16 fis8 g8. fis16 g8
 a8. g16 fis8 e8. d16 c8 c4.
 
 
 
}
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio Villanella"
  \time 6/8
  \partial 8
 d8 es4 c8 d4 g,8
 c8. d16 b8 c4 d8
 es4 c8 d4 b8 c4. r8 
 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Presto"
  \time 3/8
  g16 a g f e8
 a16 bes a g f8
 f16 g f e f8
 g16 a g f e8
 e16 f e d e g
 \grace g8 f4 e8
 \grace e8 d8 c d
 \grace d8 e4 c8

}