 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Andante"
  \time 3/2
    d2 a f' e a1
    d,2 g1
    cis,2 a d2.
    e4 cis2. d4
    d2
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Allegro"
  \time 4/4
  a'8 f16 g a8 a a f16 g a8 a 
  a d, g g g f16 e f8 f
  f b, e e e d16 cis d4~ d cis d8
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Affetuoso"
  \time 4/4
  c4 r8 c d d d es16 d
  c4 r8 c f f f g16 f
  e4 r8 a d,4 r8 g
  e c f2 e4 f4
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "4. Vivace"
  \time 3/8
    d8 a cis d a cis
    d e16 d cis d
    e8 a, a
}