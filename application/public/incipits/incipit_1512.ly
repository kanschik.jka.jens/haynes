 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/4
    g4 fis16 g a b c b b4.~
    b4 a16 b c d e d d4.
}
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. o.Bez."
  \time 3/4
    d4 d d
    d8. c16 bes4 es
    d8. c16 bes4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. o.Bez."
  \time 3/4
   \override TupletBracket #'stencil = ##f
   \times 2/3 {g8[ b d]} \times 2/3 {g[ fis e]} \times 2/3 {d[ c b]}
   \grace { c16 d} e4 d r
}