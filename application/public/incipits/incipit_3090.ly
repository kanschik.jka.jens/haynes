\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Ach senke doch den Geist der Freuden [BWV 73/2]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key es\major
   \tempo "o. Bez."
   \time 4/4
   r4 r8 es8 d16 es d c c d c bes
   bes8 as16 g as8 c bes16 as g f es8 g'
   f16 g f es es f es d

   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key es\major
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
   R1*4 r4 r8 es8 d16 es d c c d c bes
   bes8 as16 g as8 c bes16 as g f es8
     
     
}



