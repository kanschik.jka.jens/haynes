\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  <f, c' c'>2 <f c' a'>
  <f a f'> 
  \times 2/3 { r8 a'[ g]}
  \times 2/3 { f8[ e d]} 
  c4 c c d8 e
  f4 f,
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Alegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*63 
    r2 r8. c16 c4
    f8 c a'2 \grace { bes16[ a]} g8 f
    \grace g16 f8 e e4 r8. c16 c8 e
    g8 c, bes'2 a8 g
    gis4 a r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Polonaise"
  \time 3/4
   c8 c r16 c \grace e16 d16 c f8 a
   \grace a16 g8 g r16 c, \grace e16 d16 c g'8 bes
   a a16 g f8 f16 e d8 d16 fis
   g a bes g f e g e c8 r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo. All. non troppo"
  \time 2/4
  \partial 8
  c16 bes
  a8 a16 bes g8 g16 a
  f8 f r g16 a 
  bes8 bes16 c a8 a16 bes
  g8 g r
}