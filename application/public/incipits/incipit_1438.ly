 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 6/8
  \partial 8
  d,8
  <fis a>4. <fis b>
  cis'~ cis4 fis8
}
