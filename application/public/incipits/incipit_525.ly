 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Sonata"
   \time 4/4  
    c4 r8 c16 bes a8 c f a
    g4 r8 g g8. g16 g8 f16 e
    f4 r8
}

 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
   \time 4/4
           c8 c c c  d d d d
           c c c c  bes bes bes bes
}

 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Gigue"
   \time 6/8
    c4 c8 c8. bes16 c8
    a8. c16 d e f4 f8
    f8. f16 c8 d8. d16 c bes a8
}
