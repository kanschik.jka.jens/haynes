\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Christen müssen auf der Erden [BWV 44/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\minor
   \tempo "o. Bez."
   \time 3/4
   es4 c4. b16 c
   d2 g,4
   \times 2/3 {as8 f' es} \times 2/3 {d8 c bes} \times 2/3 {as8 g f}
   \times 2/3 {g8 c b} c4. d16 es \times 2/3 {f,8 d' c}
   
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key c\minor
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
    R2.*12 g4 f8 es d c
    as'4 f2 g4 c, es8 d
    b c d2
}



