\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  g8 c4 d e8 f4 d8 e f4 d8 e f16 d e c d b
  c8 c, f4
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Andante [Tutti]"
  \time 3/4
  a4 e' d c b a b gis a
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Andante [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*25 
   e2.~ e~ e~ e4 d8 c b a gis2 a4
}




\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Tutti]"
  \time 3/8
  c8 c, c c c c 
  c d16 e f g a4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Solo]"
  \time 3/8
   \set Score.skipBars = ##t
   R4.*47 
   c4. g e c8 e g c e g
   \grace {f16[ g]} a4. g
}