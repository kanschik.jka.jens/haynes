 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Air"
  \time 6/8
  \partial 8
  g8
  c d es es d c
  b c d g,4
}
