\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ie. Duo " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Ie. Air marqué"
  \time 2/4
  a4 cis8. d16
  e4 a,8. e'16 
  fis8.[ fis16 e8. d16]
  e4 cis8. d16 e8.[ e16 d8. cis16]
  b8.[ e16 gis,8. a16]

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Pr. Menuet"
  \time 3/4
  e4 e8 d d cis
  cis d e2
  a8 fis e cis d b
  cis4 b8 cis a4

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Ie. Air Léger"
  \time 2/4
  \partial 4
  e8 d
  cis b16 a b8 a16 gis 
  a8[ b cis d]
  e[ a cis, dis] e4

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe. Duo " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Marche"
  \time 2/2
  d2 e
  fis8 g a4 fis d
  e8 fis g4 e cis
  \grace e8 d4 cis8 b a4 g
  fis4 g8 a b2

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegretto. Moderato"
  \time 2/4
  d16 e fis g a g fis e
  d8[ fis e d]
  b' a4 g16 fis
  g4 fis
  e16 a, a' a, g' a, fis' a,
  fis'8[ a g fis]

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Minuetto"
  \time 3/4
  d8 fis fis e e d
  d2 e4
  fis8 a d, fis e d
  cis4. b8 a4
  \times 2/3 {fis8 g a} a2

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Premier Tambourin"
  \time 2/4
  d4 fis16 e d cis
  d8[ b' a g]
  fis a16 g fis8 e16 d
  cis8 e16 d cis8 b16 a

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIIe. Duo " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. allegretto. moderatissimo. Gratioso"
  \time 2/4
  a8 e'16 d \grace d8 cis8 b16 a
  b4 \grace {b16[ cis d]} d4
  cis8 b16 a b8 a16 gis
  a16 e fis gis a gis a b
  cis8 e16 d cis8 b16 a

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Ie. Menuetto"
  \time 3/4
  a8 cis e4 d
  cis8 e a4 gis
  a8 fis \grace fis8 e4 d
  \times 2/3 {cis8 b a} a2
  

}
  
  \relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Ie. Gigue"
  \time 6/8
  \partial 8
  e8 cis4 a8 e'4 a8
  gis4.~ gis4 b8
  d, gis b d, gis b
  cis,4 b8 a4 e'8 cis4
  

}
  


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IVe. Duo " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Air Gay gracieux"
  \time 2/2
  \partial 2
  a4 d
  cis b8 a b4 cis
  d a d8 fis e g
  fis4 g8 a g4. fis8 e2

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Menuet"
  \time 3/4
  fis4 fis fis
  \times 2/3 {fis8 g a} g2
  \times 2/3 {fis8 a g}
   \times 2/3 {fis8 a d,}
    \times 2/3 {e8 a cis}
    d,2 a4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Gigue"
  \time 6/8
  \partial 8
  d8 a' g fis fis g a
  d, a d fis d fis
  a g fis a g fis
  e cis a e' cis a

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Ie. Tambourin"
  \time 2/4
  d8[ a16 d] fis8[ fis16 a]
  g4. e16 g
  fis8[ d16 fis] e8 cis16 e
  d a fis a d4

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ve. Duo " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "1. Legerement"
  \time 2/2
  \partial 2
  a4 a,
  b4. d8 cis b a gis
  a4. b8 cis e gis, b
  a cis e cis fis d b a 
  gis4 \grace fis8 e4

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Menuet"
  \time 3/4
  cis8 e cis a e' a 
  fis2 e4
  d8 gis a b b, d 
  cis b a gis a4

}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "3. Gigue"
  \time 6/8
  \partial 4.
  a8 e a
  e a e cis e cis
  a cis a e a e
  a,4 a'8 b4 cis8 cis4 b8

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Ie. Tambourin"
  \time 2/4
  cis16 d e8 e a,
  d16 e fis8 fis fis
  e[ a e d]
  cis4 b
  cis16 a d b e a cis, d
  

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VIe. Duo " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Air. marquer"
  \time 2/2
  e2 gis4 e
  fis a gis8 fis e dis
  e2 gis8 e b gis
  a4 fis'8 e dis cis b a 
  gis2 e gis

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Ie. Menuet"
  \time 3/4
  \times 2/3 {gis8 a b} b4 b
  \times 2/3 {a8 b cis} cis4 cis
  b8a' gis b dis, fis
  e b a gis fis e

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "3. Ie. Gigue"
  \time 6/8
  \partial 4.
  b4 e8
  e gis e b gis e
  a4 gis8 cis4 b8
  \grace a8 a8 gis b b a gis
  fis4.

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "4. Rondeau gracieusement"
  \time 2/2
  \partial 2
  b4. e8
  e dis dis cis cis b b a
  gis4. fis8 gis b e, gis
  fis a gis fis b4. e8
  dis4 b 

}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "5. Ie. Tambourin"
  \time 2/4
  gis16 fis e8 b' e
  dis[ e fis e]
  a4 gis16 fis e dis
  e8[ b gis e]
  a'4 gis16 fis e dis e4 e

}
