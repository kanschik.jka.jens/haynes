 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 6/8
  \partial 4.
    \grace { fis16 g} a4 g8
    \grace g8 fis4.  \grace { g16 a} b4 a8
    \grace a8 g4. \grace { e16 fis} g4 fis8
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Andante"
  \time 3/8
  \partial 8
    d16 e 
    fis e g fis e d
    cis b d cis b a
    g cis e g fis e
    a4
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Presto"
  \time 6/8
  \partial 4.
  <fis d'>4 <fis d'>8
  <fis d'>4 b8 \grace b8 a4 g8
  \grace g8 fis4.
    <fis d'>4 <fis d'>8
  <fis d'>4 a8 \grace a8 g4 fis8
  \grace fis8 <a, e'>4.

}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Allegro"
  \time 2/4
  \partial 4
    e8 e
    e8. d32 e f8 f f r
    d d d8. c32 d e8 e e4
}
\relative c'' {
  \clef treble
  \key c\major	
   \tempo "5. Andantino"
  \time 3/4
  \partial 8
    g8 c4 e g
    g4. a16 g \grace g8 f8 e16 d
    dis8. f16 e4 r
}
\relative c'' {
  \clef treble
  \key c\major	
   \tempo "6. Allegretto"
  \time 2/4
  \partial 8
    g16 c
    c b b d d c g e'
    e d d f \grace f16 e8 g16 f
    \grace a8 g16 f \grace g16 f16 e
}
\relative c'' {
  \clef treble
  \key f\major	
   \tempo "7. Allegro"
  \time 6/8
  \partial 8
    r8
    f4 f8 g4 g8
    a4 r8 r4 c8
    \grace c8 b4 a8 g4 a8
    g4 g8 g4 r8
}
\relative c'' {
  \clef treble
  \key f\major	
   \tempo "8. Allegro"
  \time 2/4
  \partial 8
    f16 g a8 f bes a g f e d
    c bes a bes c bes a
}
\relative c'' {
  \clef treble
  \key g\major	
   \tempo "9. Contredanse"
  \time 2/4
    d4 b8 c d4. dis8 e fis g e
    d8. c16 b4
}
