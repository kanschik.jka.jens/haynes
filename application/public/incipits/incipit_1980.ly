\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 2/2
  d4 d8 g fis g16 a d,8 c
  bes16 a g8 r16 d' c d es[ d c bes] a f' es f d c bes8 r16
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 4/4
   d4 d16 c bes a bes a g a bes8 c16 bes
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 4/4
  r4 bes es2~
  es4 d g2
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 12/8
  g8 a bes  a bes c  bes a g  bes a g
}
