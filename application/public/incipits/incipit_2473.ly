 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key bes\major
   \tempo ""
  \time 4/4	
  d8. es16 f es d c bes4 r
  bes8 c16 d es8 d16 c d8 es16 f g8 f16 es
  f8 f4 g16 f es8 es4 f16 es
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key bes\major
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
     R1*4
     d8. es16 f8 d16 c32 bes bes4 r
     bes8 c16 d es8 d16 c d4 r
}