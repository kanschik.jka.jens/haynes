\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. All. Maestoso [Tutti]"
  \time 2/2
  <g e' c'>2 g'
  f8 e e e   e4 r8 c16 d32 e
  f2 d
  c8 b b b b4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. All. Maestoso [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   R1*45
   \grace { g32[ a b c d e f g a b]}
   c2 g f8 e e e e4 r
   \grace { c16[ d32 e]}
    f2~ f8[ g16 a] g f e d
    c8 b b b b4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Largo assai [Tutti]"
  \time 2/4
  d8. c16 b8 b d4 c8 r
  a8 b16. d32 \grace d16 c8 b16. a32
  g4 b8 c16. a32
  fis8 g16 r32 b a8 g16 r32 b
  a8 g g r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Largo assai [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*6
   d8. c16 b8b
   d4 c8 r
   a8. b16 c16. b32 e16. d32
   d8. c16 b8 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Poco Allegretto"
  \time 6/8
  \partial 4.
  g'4 f8
  e4. \grace e8 d8 c d
  c4. \grace d8 c8 b c
  a b c d e f e4 d8
}