\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.S."
  \time 2/2
  \partial 8
  c8
  a'16 f8. bes32 g16. e32 bes16. bes16 g'8.
}
