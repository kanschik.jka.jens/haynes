\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 8
  c8 
  f2 a
  \grace a8 g f f4 r8 a, c f
  a g f e d c bes a
  c4. b8 bes4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Rondo"
  \time 6/8
  \partial 4
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \times 3/2 { c16[ d e] }
  f8. g16 f8  e[ f g]
  a8. bes16 a8 g[ a bes]
  c a f bes4 a8 a g g g4
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
  bes'2~ bes8 a16 bes c[ bes a bes]
  f2~ f8 e16 f g[ f e f]
  d2~ d16 es e f f[ es c a]
  c8 bes bes4 r8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Polonaise"
  \time 3/4
  d16 c bes4 bes16 c d es e f
  f c c4 es16 c bes a g f
  bes8 bes16 c d8 d16 es f8 f
  f2.
}

\pageBreak

\markuplist {
    \fontsize #3
    "Quartetto III " 
    \vspace #1.5
}


\relative c' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/2
  c'4 r8 \times 3/2 { e32[ d c] }  b8 g a b
  c e g4. a8 f d
  c4 r8 \times 3/2 { e32[ d c] }  b8 g a b
  c4 r
}


\relative c' {
  \clef treble
  \key c\major
   \tempo "2. Rondo"
  \time 2/4
  g''4 a16 b c a
  g4 f16 e f g
  e8 e d16 c d e
  c
  b c d dis e f fis
}
