\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
  \set Staff.instrumentName = #"Oboe"
   \tempo "Amoroso"
  \time 3/4
  g4 \times 2/3 {g8[ b a]} \times 2/3 {g8[ b a]}
  g2 b4
  \times 2/3 {b8[ a g]} \times 2/3 {g8[ fis g]} c4 c a2
}

\relative c'' {
  \clef treble
  \key g\major
  \set Staff.instrumentName = #"Soprano"
   \tempo ""
  \time 3/4
  R2.
  b2 d4
  \times 2/3 {d8[ c b]} \times 2/3 {b8[ a g]} \times 2/3 {g8[ fis g]}
  a4 c4
  \times 2/3 {c8[ b a]} \times 2/3 {a8[ g fis]} \times 2/3 {fis8[ e d]} 
}

