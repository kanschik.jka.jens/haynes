
\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Andante un poco Adagio"
  \time 4/4
    
   \set Score.skipBars = ##t
   R1*4 
    cis4. d16 b a4 b
    cis d cis b
    cis4. d16 b a4 d
    cis b a r
  
}



