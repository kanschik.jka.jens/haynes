 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro con brio"
  \time 4/4
  fis4 r a r
  d r r8 d cis b
  a g g g g e' cis a
  g fis fis fis fis
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. "
  \time 2/2
  \partial 2

}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. "
  \time 2/4
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}