 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. [Allegro]"
  \time 4/4
    f8. bes16 d,8 es f4 r8 g
    a,8. bes16 bes8. a32 bes c4  r8 d
    es d g f a bes r
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Grave"
  \time 3/4
   r4 d bes' a f8 es d4
   r g f es2 d4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Allegro"
  \time 4/4
  bes16 c d es   f es f c  d8 bes r4
  bes16 c d es  f es f c d8  bes' c, a'
  bes,
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}