\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro moderato"
  \time 4/4
  a'4. g8 f4 f
  \grace g16 f8 e e2 d4
  c16 d e d c2 bes4
  \grace c16 bes8 a16 bes a8 a a bes16 c d e f g
  a4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Rondo. Allegretto spirituoso"
  \time 4/4
   a'4. g8 f4 g
   a a8 bes g4 g8 a
   f4 g a fis
   bes4. a8 g4 r
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
  e8 f
  a g g4 r8 c e, f
  a g g4 r8 c e, f a g f e d c e c b a a4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondo. Allegro moderato"
  \time 6/8
  \partial 4
   g'8 g
   g e f d4 e8
   c4. d4 c16 d
   e8 f16 e d e f8 g a
   e4. d8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro grasioso"
  \time 4/4
  g'4 a16 g fis g b4. g8
  d4 d r8 b' g d
  b4 c16 b a b c4. a8 d8. b16 g4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Rondo. Allegretto grasioso e moderato"
  \time 2/4
   d8. e16 d8 g
   fis r fis16 g a b
   a g fis e d8 c
   b r
}



  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato"
  \time 4/4
  \partial 4
  f8. g32 f
  es4 d c d
  es8 c a2 bes4
  c8 d es d  f es d c
  \grace d8 c8 bes bes4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Rondo. Allegretto scherzoso"
  \time 3/4
   f8. g32 f bes8 a g f
   es d c bes a g
   f r g r a r
   bes8. c32 d c4 r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro affetuoso"
  \time 4/4
  d2 es4 d
  g fis a es
  d2. c4
  \grace { bes16[ c d]} c4 bes r8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Presto ma non tanto"
  \time 2/4
   d8 d d d
   d es es d
   d g g fis
   fis4 r
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro affetuoso"
  \time 4/4
  \partial 4
  c8. c16
  f4 r r16 c b c d c b c
  g'4 r r16 c, b c  d c b c
  a'2 bes8 a g f
  e8. f32 g f4 r8
  }

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Rondo. Allegretto moderato"
  \time 2/4
 \partial 4
   c8 c
   f8. g16 e8. f16
   d8 e16 f g8 a
   bes a g f e r
}

