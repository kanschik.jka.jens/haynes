\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Angloise I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "Nr. 1"
  \time 6/8
    a4.  fis4 g8
    a fis d  d' b g
    a4. fis4 g8
    a fis d  e cis a
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Angloise II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "Nr. 1"
  \time 3/8
    bes4 es8
    es d c c bes as
    g8. as16 bes8
    es, es es
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Angloise III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "Nr. 1"
  \time 3/8
    e16 cis
    a4 a'16 e
    cis4 cis16 e
    d8 d16 e cis d
    b8 e,
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Angloise IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Nr. 1"
  \time 2/4
    d8 es f f
    g bes a g
    f d es c bes d c4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Angloise V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Nr. 13"
  \time 2/4
  \partial 8
  b16 c
  d8 b c d e c c g'
  d b b d
}
