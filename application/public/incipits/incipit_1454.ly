
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Tutti]"
  \time 2/4
  d,4 f8. g16 a8 a, r8 bes'8~bes a4 gis8 a a, r8 a'8~ a g4 fis8 g g, r8

 

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*20 r2 d16 e f g a8 d, \times 2/3 {cis 16 d e} d4 f8 \times 2/3 {e16 f g} f4 a8 \times 2/3 {g16 a bes} a4 g8 f16 e d8 r8
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Largo [Tutti]"
  \time 4/4
  \partial 8
  f,8 \grace d'8 c32 b c8. \grace d8 c32 b c8. \times 2/3 {f16 e f} f,8 r8 f8 g d' r16 d16 c16. bes32 a g f8. r8

 

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Largo [Solo]"
     \time 4/4
     \partial 8
   \set Score.skipBars = ##t
   r8 R1*4 r2 r4 r8 f8 c16.[  d32] c b c c c16.[ d32] c b c c \grace g'8 f8 e16 f f8 f g32 a bes8.~ bes16 g e bes \grace bes8 \times 2/3 {a16 g f} f4 
   


}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro [Tutti]"
  \time 3/4
  d4 d,8 d' \grace f16 e8 d16 cis d4 d,8 f' \grace a16 g8 f16 e f4 f,8 a' \grace c16 bes8 a16 g a4



}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro [Solo]"
  \time 3/4
 \set Score.skipBars = ##t
   R2.*32 d4. cis16 d \grace f16 e8 d16 cis d8 a f'4. e16 f g8. f32 g a8. g32 a bes8 d, cis16 b a4 r8



}

