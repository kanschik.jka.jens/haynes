\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/2
  \partial 8
  r8
     \set Score.skipBars = ##t
      R1*10
  r2 r4 r8 g'
  bes bes bes bes  c16[ d, c d] bes'  d, c d
  bes'8[ a16 g]
}
