\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/2
g2 b'
b a4 fis8 d
a' fis e d a'4 a
}
