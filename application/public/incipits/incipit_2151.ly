 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Andante e Spiccato"
  \time 4/4
  \partial 8
    r16 e
    a8. a16 a8. cis16 b4 r8 r16 e,16
    b'8. b16 b8 cis16 d cis b a8 r
}


\relative c'' {
  \clef treble
  \key a\major	
   \tempo "2. Allegro"
  \time 3/8
  r8 a16 b cis d
  e8 e e fis d16 cis d b
  e8 cis16 b cis a d8 b4
  a
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "3. Giga. Allegro"
  \time 12/8
  \partial 8
  e8
  cis b a fis'4 a,8 e'4 a,8 cis b a
  d b a gis e' d cis b a
}
