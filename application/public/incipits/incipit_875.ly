\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allegro [Tutti]"
  \time 2/4
  r8 e16 fis g8 fis
  e dis e b
  c4 a b r8
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allegro [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*26 
   r8 e16 fis g8 f
   e4. dis8 e4. dis8
   e4. c'8
   b a16 g fis8 g16 e
   dis8 fis b, ais
   b fis' b, ais
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key b\minor
                      \tempo "2. Adagio"
                      \time 3/4
                      % Voice 1
                      r2.
                      r4 fis4. fis8
                      b8. g16 e4. fis8 
                      fis4 r r
                  }
\new Staff { \clef "treble" 
                     \key b\minor
                        % Voice 2
                        r4 b,8. cis16 d8. e16
                        fis4 fis, r
                        r2.
                        r8 cis' d8. cis16 d8. cis16
                  }
>>
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Allegro"
  \time 3/8
   g'16 a b8 dis,
   e fis16 dis e8
   c b a
   g fis16 g e8
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Vivace"
  \time 6/8
  c8 b16 a e'8  e f e
  c b16 a f'8 f4.
  b,8 a16 g d'8 d e d
  b a16 g e'8 e4.
}