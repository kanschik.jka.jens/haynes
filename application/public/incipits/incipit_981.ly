 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allegro ma poco"
  \time 2/2
  e,8.[ fis16] \times 2/3 { g16[ fis e]} \times 2/3 { b'16[ a g]}
  e'8 b r e
  fis16[ e dis c]  b a g fis
  g8 \grace fis8 e r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Largo"
  \time 3/4
  r8 e, a c b16 a b gis
  a8 e \grace { fis16[ gis]} a8 \grace { a16[ b]} c8
  b16 a \times 2/3 { b16[ a gis]}
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Allegro"
  \time 2/4
  e,4 g
  b8 e, g b
  e b e g
  fis b, r4
}
