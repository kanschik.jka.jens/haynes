\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro ma non presto"
  \time 2/2
    r8 c d[ g,] es' d16 c d8[ b]
    c g' g, f' es g g, f'
    es[ c]
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 2/2
      es,4. es8 <bes' g>4. bes8
      es16 es32 f g8 f8. es16 d c bes8 r4
      f'8 bes,4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro"
  \time 2/2
    r4 c, es g c, c' b bes
    a as g c~ c b c4.
}
