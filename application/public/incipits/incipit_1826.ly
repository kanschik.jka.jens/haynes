\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef bass
  \key a\minor
   \tempo "1. Fantaisie"
  \time 3/4
  \partial 8
    a,8
    c b a b gis b
    a \clef alto e a b c d
    e e, f4 d'
    e,8 e' e f g c,
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef alto
  \key a\major
   \tempo "1. Prélude"
  \time 2/2
  a2. r16 e cis a
  <gis b~>2 b4. cis8
  \clef bass <a, e' a cis>4. \clef alto e''8 fis4 gis
  \grace gis16 a2. a8 gis
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef alto
  \key f\major
   \tempo "1. Prélude"
  \time 4/4
      r4 r8 f,, c'4. d8
      \grace {d16[ c] } bes4. 
       \override TupletNumber #'stencil = ##f
       \times 2/3 { bes16 a bes } bes4. a8
       \clef bass
       <f, a c f a>4 r8
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. "
  \time 2/2

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef alto
  \key d\major
   \tempo "1. Prélude"
  \time 4/4
  r4 fis,8. gis32 a a4. d,8
  \grace d16 e4 gis2 \grace { a16[ gis fis gis]} gis8. fis16
  <d fis>4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef alto
  \key bes\major
   \tempo "1. Prélude"
  \time 4/4
  r2 r16 bes16 a32 g f es d8. d16
  <bes d>4 \grace { d32[ es] } f4 \grace { es32[ d c bes a]} bes4. c8
  <f, a>8 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef alto
  \key g\minor
   \tempo "1. Prélude"
  \time 2/2
  r8 r16 g[ f d  es c d bes c a bes g a fis]
  \clef bass
  <g, d' g>2
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef alto
  \key g\major
   \tempo "1. Prélude"
  \time 4/4
  r8 d, g8.  \override TupletNumber #'stencil = ##f
  \times 2/3 { fis32 e d } d8 c32 b a b  b8. a32 g
  \clef bass <g, d' g>4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef alto
  \key c\major
   \tempo "1. Prélude"
  \time 4/4
  r2 e,4 r16 e f g
  d4 r16 g32 f e d c b c4. d8
  <g, b>4 
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef alto
  \key c\minor
   \tempo "1. Prélude"
  \time 4/4
  es,4 r8 es16 d c4 \grace { d16[ c b c]} d8. es16
  <g, b>2 g'4. d8
}

