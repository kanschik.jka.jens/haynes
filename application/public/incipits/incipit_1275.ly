 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key g\minor
   \tempo "Adagio"
  \time 4/4
  r4 r8 d16. c32 bes8 g c es
  es d r
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Zenobia"
  \key g\minor
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
     R1*3
     r2 r4 r8 d16 c
     bes8 a16 g c8. es16 es8 d
}