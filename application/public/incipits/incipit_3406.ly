 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 4/4
    es8 g16. as32 bes16 bes~ bes32 g f es es8 d16. c'32 c4~
    c16[ bes] bes32 d es c \grace bes8 as4
    g16[ bes8 b c16]
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Allegretto"
  \time 4/4
  es16 bes g'8~ g16 f es d es es8 f32 g \grace g16 f8 es16 d
  es16 es8 d32 c c16 bes f' as, \grace as8 g8. as16 \grace as8 bes4
  
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Allegro"
  \time 3/4
   bes4. c16 d es f g as
   bes4 g es16 bes8.
   \grace bes4 c2 es16 c8.
   \grace c4 bes2 bes8 es
   es4 d8 as' as g
   g4 f r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}