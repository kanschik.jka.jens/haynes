\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Starkes Lieben, das dich großer Gottessohn [BWV 182/4]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe/Violine"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key c\major
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                 r4 c16 e d f e d d e f e a c,
                 b8 a16 g g'16 a32 bes e,16 f g cis, d e a, bes' a32 bes g16
                 f16 e d e 
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Viola 1 "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key c\major
                     \set Score.skipBars = ##t
                        % Voice 2
                 r4 g,8 g g b a a 
                 g d g bes a g a e
                 d f f a g f a d
                 c4
                      
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Viola 2 "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key c\major
                     \set Score.skipBars = ##t
                        % Voice 3
                 r4 c8 b c d c e 
                 d b cis d e d e cis
                 a a b c d c d b
                 g4
                      
                  }                  
            
>>
}


\relative c {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key c\major
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
   R1*4 r4 c16 e d e e d d8 g8. a16
   g a f g a g f g32 e d8[ g,] 
   
   
   

}