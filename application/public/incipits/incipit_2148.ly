\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "o. Bez."
  \time 4/4
  f8. es32 d d es f es d c bes64 c a32 bes16 a bes8 r
}
