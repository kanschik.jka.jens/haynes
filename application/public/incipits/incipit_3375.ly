 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  \partial 16.
  g32 a b
  c4 e c g
  e c r   \grace d'16 c8 b16 c
  g4 \grace f'16 e8 d16 e c4 c
  d4. b8 c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \partial 16.
  r16.
   \set Score.skipBars = ##t
   R1*21 
   e32[ f g8.] g8 g g g g g g4 e8 r r2
}


\relative c' {
  \clef bass
  \key f\major
   \tempo "2. Larghetto"
  \time 2/2
  r8 c a bes c8. bes32 c d8 c
  c bes g4 r8 g a bes
  c a r a c bes a g
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Allegretto"
  \time 6/8
  e4. \grace e16 d8 c d
  c4. e8 c g'
  a f c' c b a
  a fis g g4 r8
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. "
  \time 3/4
  \partial 4
  
}