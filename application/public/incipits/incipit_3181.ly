\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Liebt, ihr Christen, in der Tat [BWV 76/12]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key e\minor
                      \tempo "o. Bez."
                      \time 9/8
                      % Voice 1
                      r4. r4. b8 e d
                      c b a b a g fis g a
                      g e fis g b a
                      b4.~ b4 a8~a4 
                     
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Viola da gamba"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                      b8 e dis
                      e g fis g4.~
                      ~g4 fis8~ fis4 e8 dis e fis
                      e4. r4. 
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key e\minor
   \tempo "o. Bez."
  \time 9/8
     \set Score.skipBars = ##t
    R8*9*12 r4. b4.~ b4 c8
    dis,4.~dis8 e fis a g fis
    g4. r4.
   
    
    
    }