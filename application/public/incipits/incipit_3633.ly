\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  \partial 4
  g4 c c2 e8 c
  c b b2 d8 b g4 g'4. f8 f f
  g16 f e f e4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \partial 4
  r4
  \set Score.skipBars = ##t
   R1*63
  r2 r4 g4
  c c4. d16 e d8 c
  \grace d8 c8 b b2 c8 cis
  d4 g2 fis8 f
  g16 f e f e4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Romanza. Poco Adagio [Tutti]"
  \time 2/2
  f4.. c16 c4 r8. f16
  g4.. c,16 c4 c8 c'
  c4 c8 bes16 r bes8 a16 r a8 g16 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Romanza. Poco Adagio [Solo]"
  \time 2/2
  \set Score.skipBars = ##t
   R1*2
   r2 r4
   c8 f
   a2 c8 a bes g
   g f f4. f8 g a
   bes4. g8 bes a g f a2 g4 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Allegretto moderato"
  \time 3/8
  g'8 e c'
  a8 f d b g' f e g c,
  g' e c' a f d b g'16 f e d c4 r8
}