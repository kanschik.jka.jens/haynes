\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. o.Bez."
  \time 3/4
    b4 b4. a16 b gis'4 fis r
    b,4 b4. a16 b
    a'8 fis16 a gis4 r
}
