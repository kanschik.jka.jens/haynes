 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante"
  \time 2/2
  c4~ c16 es d es32 c  d4~d16 f es d
  es8 \grace d8 c g'4~ g8 as16 g f[ es d c] b4
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegro"
  \time 2/2
  \partial 8
  c8 
  g' g g g es d16 es c8 d
  es d16 es f8 d es d16 es f8 d es
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Adagio"
  \time 2/2
  bes4. es16 d c8 d16 es d[ es f8]
  es8 bes es4~ es16[ g f es]
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Vivace"
  \time 6/8
  c8 c c c16 b c d e f
  g8 as16 g f as g8 f16 es d c
  b32[ c c8. b32 c] d8 d,
}