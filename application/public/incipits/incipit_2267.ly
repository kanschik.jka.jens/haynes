 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Intrada"
  \time 2/2
    g'16 c b a g f e d  c c' b a g f e d
    c4 r8 e16 f g4
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Ouverture"
  \time 2/2
  g'4. g8 a4~ a16 c b c
  b4~ b16 c a b c4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Allegro"
  \time 2/2
  g'4 r8 g e16[ d c d] e f g a
  g8 c, g'4.
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Aria"
  \time 2/2
  \partial 4
    g'4 a g f e f2 e4 f8 e
    d4 c b
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "5. Minuet"
  \time 3/4
  c4. d8 b4 a f' f
  f g8 f e f d4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "6. Gavotta"
  \time 2/2
  \partial 2
  c4 g a2 d4 a b2 g'4 d e4. f8 e4
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "7. Gigue"
  \time 6/8
   g'4 e8 c d e
   f4. f4 a8
   f4 d8 b
}
