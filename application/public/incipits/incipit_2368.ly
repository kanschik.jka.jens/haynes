\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I" 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key a\minor
   \tempo "1. Ouverture "
  \time 2/2
  a4. f8 e4. d8
  c2 r8 c8 d e
  f4. f8 f e f g
  e4 c
  
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Passepied "
  \time 3/8
  \partial 8
  e8 e c16 d e f
  e8 a, e'
  a a b
  gis16 a gis fis e d
  
  
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Entrée "
  \time 2/2
  \partial 4
  e4 a,4. gis8 a4 b
  c4. b8 c4 f
  e d8 c b4 c8 d
  c4. b8 a4
  
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Chaconne "
  \time 3/4
  \partial 2
  c4 a
  b e4. e8
  a, gis a b c4
  b b e
  c c a
  b
  
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "5. Giquée "
  \time 6/8
  \partial 8
  a8 e'8. f16 e8 e8. f16 e8
  c8. b16 a8 a4 b8
  c8. d16 c8 b4 e8
  c8. d16 c8 d4
  
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Ouvertur "
  \time 2/2
  f4. d8 es4 f
  bes,2.~ bes16 d es f
  g4. f8 es4 g
  a,8. c16 d8. c16 d8. c16 es8. d16

}

\relative c' {
  \clef treble
  \key bes\major
   \tempo "2. Bourée "
  \time 4/4
  \partial 8
  f8 bes f bes c bes16 a bes c bes8 bes
  c d es f d c16 d bes8 c
  

}

\relative c''' {
  \clef treble
  \key bes\major
   \tempo "3. Gavotte "
  \time 2/2
  \partial 2
  bes8 d, c a'
  bes d, c a' bes c, c d16 es
  d4 c 

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. March "
  \time 4/4
  bes8. f16 f8. a16 bes8. c16 d8. bes16
  g'8. f16 es f es d c bes a g f8 c'
  

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "5. Saraband "
  \time 3/4
  bes4 f'4. g8
  es4. f8 d4
  g f es 
  d4. c8 bes4
 
  

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "6. Menuet "
  \time 3/4
  bes8. c16 c4. bes16 c
  d4 r r 
  d8. es16 es4. d16 es
  f4 r4 r4

}

\relative c' {
  \clef treble
  \key bes\major
   \tempo "7. Menuet "
  \time 3/8
  \partial 8
  f8 bes bes bes
  bes4 c16. bes64 c
  d8 d d 
  d4 es16. d64 es
  f8 bes, g'

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Ouvertur "
  \time 2/2
  c4. as'8 g4 b,
  c4. b8 c4 d8 es
  f4. g8 as g f es
  d4 g c,
  
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Gavott "
  \time 2/2
  \partial 2
  es4 c
  d g g, c
  b2 c4 d8 es
  d4 es8 f d4 es8 f
  es4 c
  
}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "3. Rondeau "
  \time 3/4
  \partial 2
  g4 as
  f8 d f as g f
  es c es g f es
  d4 
  
}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "4. Rigadon "
  \time 4/4
  r4 r8 g8 g,4 c
  b4. c8 d c d g
  es d16 es c8
  
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "5. Sarabande "
  \time 3/4
  g4 c b
  c4. d8 es4
  d f d
  b2.
  
}

\relative c' {
  \clef treble
  \key c\minor
   \tempo "6. Menuet Alternativement "
  \time 3/4
  c4 es g
  c2 r4
  c,4 es g c2 r4
  g'4 c, g'
  as d, f
  
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "7. Concert (Solo-Violine) "
  \time 3/8
  es16 g f g es g
  d16 g f g d g
  c,16 g' f g c, g'
  b,16 g' f g b, g'
  
  
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV" 
    }
    \vspace #1.5
}

\relative c {
  \clef bass
  \key c\major
   \tempo "1. Prélude "
  \time 4/4
  r8 c8 e g e c e g
  e c a'16 b a b g a g a f g f g
  e8 c g' g, c4 r4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Ouverture "
  \time 2/2
  c4. c8 c4. c8
  c2 r4 c4 
  d4. d8 d4. d8 d2 r8 d8 e f
  e4. e8 a2~ a8

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Aria. Adagio "
  \time 4/4
  g4 r8 b8 c4 r8 c8
  d4 r8 e16 f e4 r8 g8
  c,4 r8 b16 a d4 r8 c8
  b e a,8. g16 g2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. March"
  \time 4/4
  c8 b c d c4 g
  c e d g
  c,8 d e4 d8 e f4
  e c g' c, a

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Menuet Alternativement "
  \time 3/4
  c4 e8 d c4
  c2.
  e4 g8 f e4
  e2.
  e4 g8. f32 g a4
  d,4

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "6. Bourée "
  \time 2/2
  \partial 4
  g4 e f d g
  c,8 b c d c4 g
  c a' g f
  e8 d e f e4 

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "7. Saraband "
  \time 3/4
  c4 e g
  g4 f8 e d c
  f4 a g
  e2 d4

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "8. Giquée "
  \time 6/4
  \partial 4.
  g8 f4
  e4. d8 c4 g'4. g,8 b4
  c2.~ c4. g'8 e4
  c4.

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Ouvertur "
  \time 2/2
  f4. a8 bes g e f
  g8.[ a16] g a g a g8 e cis e
  f4~ f32[ a, bes c]

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Rondeau "
  \time 3/4
  r8 a8 d e16 f e8 d16 e
  f8 a, d e16 f e8 d16 e
  f8[ a,]

}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Bourée "
  \time 2/2
  \partial 4
  d8 cis
  d4 d, f a 
  d \times 2/3 {d,8 e d} \times 2/3 {f8[ g f]} \times 2/3 {a8 bes a}
  d4 f e d cis

}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Menuet "
  \time 3/4
  a4 d2~
  d4 cis8 b cis4
  a f'2~
  ~f4 e8 d e4
  a, f' e
  g, f' e

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "5. Aria "
  \time 4/4
  d4 r4 cis4 r4
  d4 r8 g16 e cis4 r4
  d4 r4 e4 r4
  e8 f16 g f8 e16 d d4 d,

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "6. Rigadon "
  \time 2/2
  \partial 8
  d8 f,4 e d8 f a d
  f,4 e d8 f a d
  f,16[ g a8] e a f16[ g a8] e a

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "7. Giquée "
  \time 6/8
  \partial 8
  a8 
  d f d cis a cis
  f a f e cis a'
  a d, a' a a e f4 r8 e4 r8

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Simphonie "
  \time 3/8
  fis8 r8 r8 e8 r8 r8
  fis16 a, d32 e fis16 d32 e fis16
  e a, e'32 fis g16 e32 fis g16
  fis8 a fis
  g e8. g16 
  a8[ fis8.] 
  

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Aria "
  \time 4/4
  a4 fis8 e d4. a'8
  b a b g16 fis e d cis b a4 
  

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Gavotte "
  \time 2/2
  \partial 2
  d4 a
  d fis d a
  d a d cis
  d cis d e8 fis
  e4 a,8 g 

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Rondeau "
  \time 3/4
  \partial 2
  fis4 fis
  fis e8 d e fis
  g4 g g 
  g fis8 e fis g
  a4 a fis

}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Menuet "
  \time 3/4
  fis4 e d 
  a'2 a,4
  b g' fis
  e8 d cis b a4

}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "6. Aria. Hautb. Solo"
  \time 3/4
  d4 f8 e f e 
  d4 f8 e f e 
  e4 bes'8 g e d 
  cis4. b8 a4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "7. Giquée "
  \time 6/8
  \partial 4
  fis8 g
  a b a e fis g
  fis e fis d fis a
  b a b e, fis g
  fis e d b'4.~ b8 gis e a4 fis8

}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. Ouvertur "
  \time 2/2
  f4. f8 f4~f
  f2 r4 c4
  des4. c8 c bes as g
  as bes as bes c des16 c bes8 c
  des4.

}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "2. Gavott "
  \time 2/2
  \partial 2
  c4 f
  c8. bes16 as8 bes c8. bes16 as8 bes
  c4 f, c' f 
  es bes8 c des4 c8 bes
  c4 as

}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "3. Sarabanda "
  \time 3/4
  c4 c4. c8
  c4. c8 f4
  des des4. es8
  c4. bes8 as4

}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "4. Menuet "
  \time 3/4
  c4 as f 
  des'2. c4 f g
  as g8 as f4
  
  

}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "5. Chaconne "
  \time 3/4
  \partial 2
  c4 c
  c c c 
  c f, bes~ 
  bes8 as g as bes c
  as4

}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "6. Giquée "
  \time 6/8
  \partial 8
  c8 as f c' bes g' f
  e4. f8 c e
  des bes g e c' bes
  as4.

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Ouverture "
  \time 2/2
  f1~
  ~f8. f16 e8 d
  c bes a g
  f4. f8 f4 g8 a
  g4. g8 g4 a8 bes
  g4. g8 g4

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Concert. Adagio "
  \time 3/8
  c8 r8 r8
  c8 r8 r8 
  a4 r8
  r32 f'32 e f c[ f c f] a, c a c
  f,[ a' g a] f[ a f a] 
  

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Sarabanda "
  \time 3/4
  c4 d4. d8
  d c c4. c8
  bes4 bes4. bes8
  bes a a2

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Gavott en Rondeau "
  \time 2/2
  \partial 2
  f4 c
  a4. g8 f a g bes
  a4. g8 f a g bes
  a bes g bes a bes g c
  a4 f

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. Menuet "
  \time 3/4
  f4 f,8 g f4
  f f8 g a4
  g g8 a bes4
  a8 bes c d e c
  f4 f,8 g f4

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "6. Giquée "
  \time 12/8
  \partial 8
  c8 f g f c a c d es d a f a
  bes d bes g c bes a g a f4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Ouvertur "
  \time 2/2
  bes4. g8 es'4. es8
  es4. d8 d g, a bes
  c4. c8 c es d c
  bes4 g

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Courante "
  \time 3/2
  d4 g a8 bes a4 fis4. g8
  a4 d,~ d8 d g f es4. d8
  c4. bes8 a4 d bes g

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "3. Sarabanda "
  \time 3/4
  g4 g, a bes2.
  c4 d4. es8
  c2 bes4
  c d4. es8
  fis,2 g4

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Aria "
  \time 3/8
  d8 bes c
  a16 g a bes a8 
  d d g
  f8. es16 d8
  es c f
  d8. c16 bes  a bes8 a g d'4.

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "5. Menuet "
  \time 3/4
  bes4 d bes
  g g' f
  es d c
  bes a8 bes g4

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "6. Bourée Alternativement "
  \time 4/4
  \partial 8
  d8
  d bes16 c d8 g d bes16 c d8 d,
  g c bes a bes bes16 c d8 a

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Ouvertur "
  \time 2/2
  d4. d8 b4 e
  a,8. a16 b8 c
  d2~
  ~d8 g, a b c4. c8
  c4. 

}


\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Gavott "
  \time 2/2
  \partial 2
  g4 d
  d b8. c16 d4 c8. b16
  a8. c16 b8. c16 d4

}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Air. Adagio "
  \time 3/4
  \partial 4
  d4 d d b8. c16
  d4 d b8. c16 
  d4. c8 b c 
  a4 a 

}


\relative c''' {
  \clef treble
  \key g\major
   \tempo "4. Menuet alternativement "
  \time 3/4
  g4 fis2
  g4 d8 c b4
  c8 d e4 d
  b2 a4 

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Chaconne "
  \time 3/4
  \partial 2
  d4. d8
  g4 d4. d8
  e4 b4. c8
  d e a,4 b8 c
  b4 
   

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Intrada "
  \time 4/4
  r8 a16 bes c8 d16 c c8 d16 c c8 d16 c
  c8 f c f e c g' bes
  a16 f, f g a a a bes 

}

\relative c {
  \clef bass
  \key f\major
   \tempo "2. March "
  \time 4/4
  r8 f8 f f f f, f' f,
  f16 f' a f d' bes g bes c a c a d bes g c 
  a4 r4

}

\relative c' {
  \clef bass
  \key f\major
   \tempo "3. Rigadon "
  \time 4/4
  r4 r8 f8 c16 f e d 
  c d c32 d bes16
  f8 f16 e f8 e f bes c c,
  f4.

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Menuet "
  \time 3/8
  f4 r8
  c4 r8
  a16 g f g a bes
  a8 f g 
  a16 g a g a g 
  a8 f' g,

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. Duett alternativement "
  \time 3/8
  c8 f c
  c a16 bes g bes
  a g a bes g bes
  a8 c f 
 

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "6. Sarabanda "
  \time 3/4
  \partial 4
  c4 a bes4. c8
  a4 f c'
  d e4. f8
  e4 c8 g' f e
  a4 d,4. d8 d4

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "7. Giquée "
  \time 4/4
  f16 g f8 e16 d e8 f4 r4
  r2 f16 g f8 e16 d e8
  f4 g a16 bes a8 g a16 f
  g8 e16 d c8 g'
  

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto XII " 
    }
    \vspace #1.5
}

\relative c {
  \clef bass
  \key c\major
   \tempo "1. Prélude "
  \time 4/4
  r8 c16[ c c8 c] c[ a c c16. b64 c]
  d8[ d16 d d8 d] d[ g, d']

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Ouverture "
  \time 2/2
  g4. a8 f4. g8
  e4. f8 d4. e8
  c4~ c32 g a b c d e f  g4. g8 
  e4 g

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Sarabanda "
  \time 3/4
  g4 g4. c8
  b4. c8 d4
  c f4. g8
  e4. d8 c4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Menuet alternativement "
  \time 3/4
  e4 c g'
  e2 r4
  g4 e c' g2 r4
  g4 e4. d16 e
  f4 d4. c16 d
  e4 c f

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Chaconne "
  \time 3/4
  \partial 2
  e4 f
  g g, c
  b g c
  r4 d4 e8 f
  e4 e f 
  g g, c 

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto [XIII] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Ouverture "
  \time 4/4
  c4. c8 c8.[ g16 c8. es16]
  c4. c8 c8.[ bes16 c8. es16] 
  d4.

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Gavott "
  \time 2/2
  \partial 2
  c4 b
  c g es g
  es c c' d 
  es b c d 
  b2 

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Bourée "
  \time 2/2
  \partial 4
  c8 b
  c4 g d' g
  es d8 es c4 d8 g
  es g d g es g c, f
  g f g as g4

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Menuet alternativement avec le Trio "
  \time 3/4
  c8 g c es d c
  d4 g, g'
  es d c
  b8 c b a g4
  

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "5. Chaconne "
  \time 3/4
  \partial 2
  c4. c8
  d4 g,4. g8
  c4 c8 b c d
  es4 d8 es f d
  es4 

}
