 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Affetuoso"
  \time 4/4
        << {
      % Voice "1"
        r2 f8 c d8. es16 
        c8 d16 c bes8. a16 g4
           } \\ {
      % Voice "2"
    f'8 c d8. es16 c4 r8 bes
    a bes16 a
    } >>

  }


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Vivace"
  \time 3/4
    r8 f,[ a c] g a16 bes
    a8 f a c g[ a16 bes]
    a8
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Grave"
  \time 3/2
  f2 g4 f e d
  e2 a,2. a4
  d1.~ d2 cis
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allegro"
  \time 2/2
    f8[ g] a g16 f g8[ f16 e] f e d c
    f8[ g] a g16 f g8 c, r
}