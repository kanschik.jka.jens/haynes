 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8.
  c16 d e
  f8 <a, f'> <a f'> <a f'>
}
 