\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 6/8
      d8 fis16 e d8  e a, a'~
      a g4 fis8 e16 fis d8
      fis fis16 e d8 e4 a8
      b e, gis a4
}
