\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Ich esse mit Freuden mein weniges Brot [BWV 84/3]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\major
                      \tempo "o. Bez."
                      \time 3/8
                      \partial 8
                      % Voice 1
                   d8 g a b
                   c b a
                   b g' b, 
                   c[ b]
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \set Score.skipBars = ##t
                     \key g\major
                        % Voice 2
                  d,8 
                  g16 d a' d, b' d,
                  c' d, b' d, a' d,
                  b' d g d b g
                  c d, b' d, a' d,
                 
                   
                       
                        
                
                  }
>>
}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key g\major
   \tempo "o. Bez."
  \time 3/8
  \partial 8
     \set Score.skipBars = ##t
    r8 R4.*23 r8 r8 d8 g a b
                   c b a
                   b g' b, 
                   c[ b]
                   
   
    
    
    }