\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante"
  \time 2/2
        << {
      % Voice "1"
      d'8 bes r a bes f r a
      bes f r d' es es16. d32 es16. d32 c16. bes32
         } \\ {
      % Voice "2"
      bes,8 d16. es32 f8 f d bes r a
      bes d16. es32 f8 f g bes16. as32 g16. f32 es16. d32
        } >>
}

  
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Presto"
  \time 2/2
  bes2. bes4
  f' c es2
  es4 d8 c d4 c8 bes 
  c4 f f f
}

 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Cantabile"
  \time 3/2
  es2 bes4 es f es
  d c8 bes r4 g' f8 es d c
  as'4. g8 g f f es es d d c
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 2/4
    bes8 bes d bes
    f'4 r8 f
    g f f bes,
    g' f f bes,
}