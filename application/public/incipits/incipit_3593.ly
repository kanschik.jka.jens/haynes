\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro Spirituoso"
  \time 4/4
  g'4 \times 4/6 { fis16 e d c b a} 
  \times 2/3 { g16[ a b] } a8 r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Tutti]"
  \time 2/4
  g'4. a32 f e d
  \grace c8 b16. d32 f,8 r16 g, f'16. e64 d
  c16 g g'16. f64 e d16. a'32 g16. b,32
  \grace b8 c8 g r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*12
   g'4. a32 f e d
  \grace c8 b16. c32 d8 r16 d32 e f16 e32 d
  c16. g32 \grace a'8 g16 f32 e d16. a'32 g16. b,32
  \grace b8 c8 g r
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
  g'4. g,8 b d
  g16 a b a g4 r8. d16
  e c8. c'16 a8. a16 fis8.
  g16 a b a g4
}