\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt + Tenor): Wohl mir, Jesus ist gefunden [BWV 154/7]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key d\major
                      \tempo "Duett"
                      \time 4/4
                      % Voice 1
             r4 d16 e fis g a d, fis d b' d, a' g
             a d, fis d b' d, a' g a d, d' cis b cis d8
             fis,8[ e]
                 
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key d\major
                     \set Score.skipBars = ##t
                        % Voice 2
             r4 a,8 d16 e fis8 d g fis16 e
             fis8 d g fis16 e fis8 a g d
             d[ cis]
                      
                  }
                  

                  
>>
}

\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Alt"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key d\major
                      \tempo "Duett"
                      \time 4/4
                      % Voice 1
         R1*6 r4 d16 e fis g a8 fis b a16 g
         a8 g16 fis b8 a16 g a8 d b a16 g 
         \grace fis8 e4 r4
                 
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Tenor"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key d\major
                     \set Score.skipBars = ##t
                        % Voice 2
             R1*6 r4 d16 e fis g a8 fis b a16 g
         a8 g16 fis b8 a16 g a8 d b a16 g 
         \grace fis8 e4 r4
                      
                  }
                  

                  
>>
}

