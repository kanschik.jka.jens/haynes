 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 2/2
  es4 bes es g bes g es r
  \grace a,4  bes2. as8 g
  es'1
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Andante"
  \time 2/4
  \partial 4
  c4
  g'8. \times 2/3 { f32[ es d]} c8 c
  b c16 r b8 c16 r
  b8 c16 r g'4
  g16 f es d
}

 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Rondo"
  \time 2/2	es2 \grace g16 f8 es f g
  es4 r bes'2
  as4 as8 f g4 g8 es
  d es f g f4 bes, 
}
