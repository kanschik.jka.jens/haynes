\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Recueil de Brunettes" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. o.Bez."
  \time 3/4
  d4 \grace d16 cis2
  d e4 f \grace e16 d2 \grace e16 e2 e4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "II.e Suitte de Brunettes" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "I.er Recueil"
  \time 2/2
  \partial 8
  d8
  d4. e8 cis4. d8
  \grace d16 e2 e4 fis
  g2 g4. fis16 e
  e2. r8
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "III.me Suitte" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Gay"
  \time 3/4
  a4 a b c2 b4
  c4 d2 e2.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IV. Suitte des Brunettes" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1.er Recueil"
  \time 2/2
  \partial 2.
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
 
  d4 e e
  f4. \times 2/3 { e16[ d e]}  f4 e8. d16
  \grace d16 cis4. 
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "V.e Suizte" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Gay"
  \time 3/4
  fis2 g4 a2 a4
  b \grace a16 g2
  fis4. g8 a4 a gis2 a2.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VI.e Suitte" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Pr. Recueil"
  \time 2/2
    \partial 2
    g'4 g8 a
    fis4 g a b
    \grace a16 g4 \grace fis16 e4
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VII.e Suitte" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Tendrement"
  \time 2/2
  \partial 2.
  a4 d e
  fis e8. fis32 g fis4 e8. d16
  \grace d16 cis4. d8 e4.
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VIII.e Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo ""
  \time 2/2
  \partial 2
  c4 g'
  g2 f es4. f8 g4 f g2 f es2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IX.e Suitte des Brunettes " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo " "
  \time 3/4
  g'4. fis8 g4
  d bes'2
  \grace bes16 a2 g4
  g fis 
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "X.e Suitte" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. "
  \time 3/4
  a'4. g8 a4 fis4. g8 a4
  g \grace a16 g4 \grace fis16 e4
  \grace e16 fis2 fis4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "XI.e Suitte" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1.er Recueil "
  \time 2/2
  \partial 2
  a4 e'
  
  cis \grace b16 a4 cis4. d8
  e2 fis8 gis
  a4 e fis fis4. e8 e2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "XII.e Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo ""
  \time 2/2
  \partial 2
  d4 e
  cis4. d8 b4 cis d d d e
  cis4. d8 b4 cis d2
}

