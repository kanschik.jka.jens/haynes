 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Soave"
  \time 4/4
    fis16. g32 a16. b32  a16. b32 g16. a32  fis16. g32 a16. b32 a16. b32 g16. a32
    fis16. d32 e8 r16
}
 

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro [Tutti]"
  \time 2/4
    d8 d, d d
    d' r e r
    fis d fis d
    d d, d d
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
    R2*35
    a4 g8. a32 b
    a4. d8 cis b a g
    fis e d d'
}
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Largo"
  \time 4/4
    r8 cis d cis16 b r8 d e d16 cis
    r8 d16. cis32 b16. cis32 d16. b32 r8
}
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. o.Bez. [Tutti]"
  \time 12/8
  \partial 8
  a8
  d a fis d fis a  d a fis d fis a  
  d a d  e a, e' fis d a  d, fis a
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. o.Bez. [Solo]"
  \time 12/8
   \set Score.skipBars = ##t
    R1.*16
   d,4. d4 d8 d4. r4.
   e4. e4 e8 e4. r4.
   fis4. fis4 g8 a4. b
}