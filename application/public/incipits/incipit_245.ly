\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key g\minor
    \time 4/4
    \tempo "1. o.Bez."
      r4 g16 a bes g
      a8 d,16 d' bes8 a16 g
      fis8 a   d c16 bes a4 d16 d d d
      d c c c   c c c c    c bes bes bes     bes bes bes bes
      bes bes bes a a8. g16 g4 r
}

 