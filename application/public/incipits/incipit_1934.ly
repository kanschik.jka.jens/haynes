\version "2.16.1"
         #(set-global-staff-size 14)
         
         \relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 3/4
    f,4 g a  bes c d
    r es d
    \grace d8 c2 bes4
}
