 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
  e2 e8 e f e
  \grace e8 d2 d8 d e d
  \grace d8 c2 c8 c d c
  \grace c8 b2.
}
