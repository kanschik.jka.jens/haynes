\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 2
    f4 bes
    d, r bes'8 r bes r
    \grace { a16 bes} c4 r
    es, es8. d32 es
    c'4 r f,8. g16 f8. es16 d4 r
}
