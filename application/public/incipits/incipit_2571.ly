 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Largo"
  \time 4/4
  \partial 8
  b8
  e16. fis32 g4 fis8~ fis16. dis32 e8 r c16. b32
  b8 a16. g32 fis16. c'32 b16. a32 g16. fis32 e8 r4
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. Allegro"
  \time 4/4
  e,4 b'4. c8 b a
  b e, e'4. d16 c b8 a
  g16 b a b  g b a b  fis b a b  fis b a b
  e,8 g16 fis g8 a b4 r
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Grave"
  \time 3/2
  d4. g8 fis4. g8 d2~
  d4. g8 fis4. g8 d2~
  d4 c2 b c8 a
  b a g4 r
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "4. Vivace"
  \time 3/8
    r16 e, g b a g32 fis
    g16 b e8 dis
    e16 b c32[ d b c]  d[ e c d]
    
}