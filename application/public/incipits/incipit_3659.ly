 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 4/4
  r4 e f e
  d d d8 f e d
  c4 \grace b8 a4 r8 c d c
}
