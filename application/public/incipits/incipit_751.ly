\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro non molto [Tutti]"
  \time 4/4
  \partial 4
  d4
  <b d g>4 d8. c16 b8 d d16 c b a
  b8 g, b d b g d''8. c32 d
  e8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro non molto [Solo]"
  \time 4/4
  \partial 4
  r4
   \set Score.skipBars = ##t
   R1*35 
  r2 r4 d4g2 \grace e8 d4 \grace c8 b4
  \grace a8 g2. \grace e'8 d8. c32 d
  e8 c g2 g'8 e
  e16 d c b \grace a8 g2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 4/4
  c8 \times 2/3 { e16[ f g] }   \times 2/3 { g16[ f e] }   \times 2/3 { d16[ c b] }
  \grace d8  c16 b c4 c8
  c8 a' \times 2/3 { g16[ f e] }  \times 2/3 { d16[ e f] }  
  \times 2/3 { e16[ d c] } c4 c8
  c8 a' \times 2/3 { g16[ f e] }  \times 2/3 { d16[ e f] }  
  \times 2/3 { e16[ d c] } c8 r4
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro [Tutti]"
  \time 2/4
  g8 g4 a8
  \grace g8 fis4 d
  c'8 c4 a8
  \grace a8 b4 g
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
    R2*58
    g4. b16 g
    \grace g8 fis4 d
    c'4 fis16 e d c
    b4 g
}
