 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  g'8
  \grace f8 es8 \grace d8 c8
  \times 2/3 { as'16[ g f] }
  \times 2/3 { es16[ d c] }
}
