
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto B-Dur "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key bes\major
  \tempo "1. Allegro Moderato"
  \time 4/4 
  R1*2 r4 es16 d cis d g4 f
  f8. es16 d2 c4 d r4
 
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 2/4
  c8. a16 c8 r16 f16
  e8 e e32 g bes16 r16 bes16
  a16. f32 d16. g32 f8 e f4 r4
 
 
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegretto"
  \time 2/4
  \grace es16 d8 c d es
  f f \grace g16 f8 es16 d 
  \grace d16 c8 bes c d
  es32 f g8. r8

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto Es-Dur "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key es\major
   \tempo "1. Allegro Moderato"
  \time 2/2
 bes4 bes2 bes16 as g as
 g2 f es4. d16 c bes4 bes8 es
 \grace d8 c4 bes r8
 
  
}
 
\relative c' {
  \clef bass
  \key es\major
   \tempo "2. Adagietto"
  \time 3/4
  c2. c16 d es d d4 c
  bes8. c16 bes8 d c bes
  \grace c16 bes8 as as4


 
}

\relative c''' {
  \clef treble
  \key es\major
   \tempo "3. Allegretto"
  \time 6/8
  g4 bes8 es, es es
  f4 as8 d,4 f8
  es es es \grace f16 es8 d c
  c4. bes8 r8 r8
 
  
 
}