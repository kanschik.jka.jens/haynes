 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Andante"
  \time 2/4
  \partial 8
    c8 g'4. as8
    b, c as'32 g f16 es[ d]
    \grace d8 es4 f32 es d16 c[ b]
    \grace b8 c8 as
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
   g'8
   \grace f8 es8. as16 g fis g g,
   \grace d'16 c b c8 r c
   as16 g as f' f d bes as
   g f g es'  es c as g
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Allegro"
  \time 3/4
  c2. d
  es8 f g4 g
  as2. g4 c, g'
  \times 2/3 { as8[ g f] } es4 d
  c
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}