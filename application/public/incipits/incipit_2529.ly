 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. o.Bez."
  \time 4/4
  \partial 8
  g8
  c d16 e c8 e16 f e8 c r g'
  g16 d d g g d d g  g8 c, r f
  f16 c c f f c c f b,8 g r
}


