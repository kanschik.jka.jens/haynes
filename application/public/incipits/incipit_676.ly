
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 3/4
r8 g8 g g g a16 b
c8 c c c c16 d e f
g8 g g g g a16 b c4 

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 6/8
  r4 r8 r8 r16 es16 f8 g4 g8 g8. f16 g8
es4 es8 es8. d16 es8 
f8. g16 as8 g8. f16 es8 d4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Largo"
  \time 3/4
 d8 e e2 
f2 \grace e8 d4
e c2
e4 c2
d2 \grace c8 b4
\grace a8 g4

}

\relative c' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 6/8
r4 r8 es8. d16 c8
b4. g8. g16 c8
b4. g8. g16 c8
b8. c16 d8 es4 es8 d4.   

}
