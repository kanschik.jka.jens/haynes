\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/2
  \set Score.skipBars = ##t
  d8 b16 a g[ b d g] fis4 r16 d d16. c64 d
  e16[ g d g] c,8. b16 b4 r16 g a b
 
}
