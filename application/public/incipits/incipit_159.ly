 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Concerto"
  \time 2/4
  bes8 bes4 \times 2/3 { d16[ es f]}
  bes,8 bes4 \times 2/3 { es16[ f g]}
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 4/4
  g'8 d16 es d bes a g es'4 r
  es8 es d16 c bes a bes8 g
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Vivacett"
  \time 3/4
  bes'4 f g
  g8 es f4 r
  f,8 f a a c c
}
