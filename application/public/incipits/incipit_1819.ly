\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
    c,4. c'8 c4. b16 c
    cis4 d r2
    g,,4. d''8 d4. c16 d 
    dis4 e r2
}
