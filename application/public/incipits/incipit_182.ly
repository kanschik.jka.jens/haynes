\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 2/2
    R1 r2  f,16[ g a bes] c d e d
    f8 f f16[ g f e32 f] g8 g g16[ a g f32 g]
}
