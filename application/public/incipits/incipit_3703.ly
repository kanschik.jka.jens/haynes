\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 2/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \partial 8
  bes32 a bes c
  \times 2/3 { d16 bes f} f8 r16 bes d32[ bes f' d]
  \times 2/3 { es16 c a } f8 r16
}
