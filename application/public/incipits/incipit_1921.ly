\version "2.16.1"

#(set-global-staff-size 14)

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Diverses Pieces [20 pieces] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Bourée"
  \time 2/2
  \partial 4
  fis8 g
  a4 d, d d 
  d2 e
  fis4 e8 fis g4 fis
  e a, a
  

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Fanfare"
  \time 6/8
   \partial 8
   a8 fis4 e8 fis g a 
   fis e fis d4 a'8
   fis4 e8 fis e d 
   e4 a8 a4
  
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Marche"
  \time 2/2
  \partial 4
  a4 d a8 d e4 a,8 e' 
  fis4 e8 fis d4 e
  fis 

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Menuet"
  \time 3/4
  d4 d8 d d d 
  a4 a8 a a a 
  fis4 fis8 fis fis fis
  fis4 e8 fis d4

}

