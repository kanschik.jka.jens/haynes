\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 3/4
  \partial 4
  d4
  es4. d8 c4
  d d bes
  c4. bes8 a4
  bes2
}
