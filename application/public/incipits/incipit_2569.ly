 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Affetuoso"
  \time 4/4
  r4 r8 g c4 r8 g
  c4 r16 c d c b8 c r16 g' as c,
  b8 c
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante e Stacato"
  \time 12/8
     \set Score.skipBars = ##t
  \partial 4.
  r4 r8
  R1. * 3
  r2. r4 r8 r g' f
  es f g c, d es as,4. r8 f' es
  d es f b c d g,4.
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Largo"
  \time 4/4
  bes4. c16 d es8 bes r16 d es f
  g8 es r bes16 es as,8 as16 as as f' f as, as f g8 r   }

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Allegro"
  \time 4/4
  r4 c16. b32 c16 g c8 g r16 g c16. g32
  es'8 g, r16 g d'16. g,32 es'8 d
}
\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "5. Grave"
  \time 4/4
  c8 es c as des4 r
  d8 f d bes es4 r
}
\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "6. Allegro"
  \time 2/4
  \partial 4
  g'8 g
  g f16 es d es d c
  b8 g
}