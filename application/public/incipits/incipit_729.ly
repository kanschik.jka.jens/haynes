\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo ""
  \time 3/4
d2 a4 b2 c4
d4. e8 c4 b2 c4
d g, a b
}
