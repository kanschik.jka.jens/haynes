\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Gerechter Gott, ach, rechnest du? [BWV 89/5]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key bes\major
   \tempo "o. Bez."
   \time 6/8
   \partial 8
   f8 bes16 c d es f8 f4 g8
   a,16 bes c d es8 es4 f8
   d4 c8 bes8 g' f
   a, es' d
   
   
   
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key bes\major
   \tempo "o. Bez."
   \time 6/8
   \partial 8
   \set Score.skipBars = ##t
     r8 R2.*7 r4 r8 r4 f8 bes d f f4 g8
     a, c es es4 r8
}



