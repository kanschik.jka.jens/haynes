\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Babiole " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Rondeau. Gracieusement"
  \time 2/2
  \partial 2
  g8 f g a
  g4 f8 e d c f4
  d2 \grace {c16[ d]} e8 d e f
  e4 d8 c f e d c
  b4 \grace a8 g4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Bourée"
  \time 2/2
  \partial 4
  e4 e d8 c d4 g8 f
  e f e d c4 g'
  a g8 a f4. e8
  d c d2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Fanfare"
  \time 6/8
  \partial 8
  c8 e f e d4 c8
  g'4. e4 g8
  g a g f4 g8
  e4. c4

}


\relative c''' {
  \clef treble
  \key c\major
   \tempo "4. Menuet I"
  \time 3/4
  c8 b c4 g
  e8 d e f e d
  c4 a'8 g f e
  d c b a g4

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe. Babiole " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gracieusement"
  \time 3/4
  e4. f8 d e
  c4 g'8 a f g
  e d e f d e
  c2 g4
  d'4. e8 c4
  f4. g8 e4

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Menuet I"
  \time 3/4
  g4 c g
  a g r4
  c b8 a g f 
  e f e d c4
  
  

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Fanfare"
  \time 6/8
  \partial 8
  g8 g c, g' a b c
  g f g e4 g8
  g f e d4 c8
  b4 g8 g4

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "4. Premier Sauteuse"
  \time 2/4
  \partial 8
  g8 e16 f g8 f16 g a8
  g4 c,8 g'
  e16 f g8 f16 g a8
  g4.

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "5. Chaconne"
  \time 3/4
  r4 g8 f g4
  c, e8 d e4
  d g8 f g4
  g f8 e f4 
  f e8 f d f
  e4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIIe. Babiole " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Gracieusement"
  \time 3/4
  \partial 4
  g4 a g8 a f a
  g4 c, e
  f e8 f d f
  e2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Sauteuse I"
  \time 2/2
  c8 d e4 d8 e f4
  e8 f g4 f8 g a4
  g4 c, d8 e f4
  e2 d

}
  
  \relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Gigue I"
  \time 6/8
  \partial 8
  g8 e4 d8 e4 f8
  g4 c8 b4 a8
  g a g f4 g8
  e4. \grace d8 c4

}
  
  \relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Musette "
  \time 2/2
  \partial 2
  e8 d e c
  g' f g e a g f a
  g a g f e d e f
  g4 c, f e 
  d2

}

 \relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Rondeau I. Gracieusement "
  \time 3/8
  \partial 8
  e8 e16 d d8 f
  f16 e e8 g~ 
  g f16 e d c
  b8 \grace a8 g8 

}

 \relative c''' {
  \clef treble
  \key c\major
   \tempo "6. Menuet I "
  \time 3/4
  g4 f8 e d c
  a'4 g r4
  c4 b8 a g f
  e2 d4
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatriéme Babiole " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gracieusement"
  \time 3/4
  e4. f8 d4 \grace {c16[ d]}
  e8 f f4. e16 f
  g4 f8 e d c
  b4. d8 e f
  g g, g4

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Villageoise I"
  \time 2/4
  \partial 4
  g8 a
  g4 c8 b16 a
  g4 c8 b16 a
  g8 f16 e f8 g
  e[ c g' a] g4

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Musette en Rondeau"
  \time 6/8
  \partial 4.
  g4 b,8 \grace {a16[ b]}
  c8 g c d16 e f8 g
  e4 d8
  g4 b,8 \grace {a16[ b]}
  c8 g c f e8. f16
  d4.

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Fanfare"
  \time 6/8
  \partial 8
  c8 e g e d e f
  e d e c4 e8
  g a g f g a
  g f g e4 
  

}


\relative c''' {
  \clef treble
  \key c\minor
   \tempo "5. Sarabande"
  \time 3/4
  g4. c8 b a
  g4. as8 f as
  g4 f4. g8
  es4 \grace d8 c8 es f g
  as4
  

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "6. Menuet I"
  \time 3/4
  e8 f g c, b c
  a'4 b c
  b8 c b a g f
  e2 d4
  

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquiéme Babiole " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Gracieusement"
  \time 2/2
  \partial 2
  b8 g a fis
  g4 d8 e d b c a
  b4 \grace a8 g4 b'8 g a fis
  g fis g a g b a g
  fis a g a b4 b
  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Premiere Contredanse en Rondeau"
  \time 2/4
  \partial 4
  b16 c d e
  d8 g d16 e d c
  b8 \grace a8 g8 b16 c d e
  d8[ g c b]
  a4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Premiere Polonoise. Gravement"
  \time 3/4
  d4 g16 a b8 a c
  b4 g16 a b8 a c
  b a16 g fis8 e d c
  b g'16 fis g8 d b \grace a8 g8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Rondeau. Gracieusement"
  \time 6/8
  b8. c16 d8 e \grace fis8 fis4 \grace {e16[ fis]}
  g16 a a4 \grace {g16[ a]} b4 a8
  c4 b8 a8. b16 g8
  a \grace g8 fis8 \grace g8 g8 \grace {fis16[ g]} a16 g fis e d c

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "5. Menuet I"
  \time 3/4
  g8 fis g d c d
  b a b d e fis
  g fis g b a c
  b2 a4

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixiéme Babiole" 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Lentement"
  \time 3/4
  g8 a g a g a
  g d e fis g a 
  b c b c b c 
  b fis g a b c
  d4 d d

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Reprise. Légerement"
  \time 3/8
  d4 d8
  e16 d e fis g e
  fis e fis g a fis
  g a a8. g32 a
  b16 a g a b8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Musette. Gracieusement"
  \time 2/2
  \partial 2
  b4 c8 a
  b d e fis g4 a8 fis
  g fis g a b4 c8 a
  b a g fis e fis g a
  fis e d c 

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Premiere Contredanse"
  \time 2/2
  \partial 2
  d4 d
  g8 a b4 d,8 e f4
  e8 fis g4 b,8 c d4
  c8 d e4 d c
  b g 

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Menuet I"
  \time 3/4
  d4 e8 fis g4
  fis4 g8 a b4
  c8 b a g a fis
  g2 d4
  e8 d e fis g4
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "6. Chaconne Polonoise. Gravement"
  \time 3/4
  g8. b16 a4 g
  a8. fis16 g4 d
  b8. d16 c4 b
  c8. a16 b4 g

}
