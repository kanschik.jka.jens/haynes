
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andantino [Tutti]"
  \time 2/4
 \partial 16
 d,16 g4 a16 g c b
 a8. b32 a g8 r16 d16
 b'4 c16 b e d
 c8. d32 c b8 r16


}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andantino [Solo]"
  \time 2/4
 \partial 16
   \set Score.skipBars = ##t
   r16 R2*32 r4 r8 r16 d16 g4 a16 g c b
 a8. b32 a g8 r16 d16
 b'4 c16 b e d
 c8. d32 c b8 r8
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Solo]"
  \time 4/4
   g8. e32 f g16 g g g g8. a32 e f16 r16 r8


}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Rondeau [Solo]"
  \time 2/4
   d8 d r8 b8
 c c r8 a8
 b b e,8. a16
 \grace g16 fis8. e16 d8 r8



}

