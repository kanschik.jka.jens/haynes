 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/2
  g'4 fis8. e32 fis g4 r8 d
  e16 fis g8 d[ g] c,4 b8 d
  e16 g fis g  d[ g fis g]
  c,4 b8 d
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. Largo"
  \time 2/2
  r4 e8 e e[ d16 e] fis8 fis
  fis[ e16 fis] g8 g
  g16[ fis fis a] \times 2/3 { a16[ g fis]}
  \times 2/3 { fis16[ g e]}
  dis8 b16[ b']
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Allegro"
  \time 3/4
   g'4 d r8 c
   b c16 d g,4 a
   b16 a g8 d' c16 b a8 g
   fis16 g a8 d, d' d16 c d8
}
