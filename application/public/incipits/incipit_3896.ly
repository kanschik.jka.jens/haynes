 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Violin"
  \key es\major
   \tempo "Adagio"
  \time 2/2
  bes8 bes bes bes  bes bes bes bes
  bes8 bes bes bes  bes bes bes bes
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key es\major
   \tempo "Adagio"
  \time 2/2
     \set Score.skipBars = ##t
     R1*11
     es4 g, r2 R1
     r2 es'4 g,
     r2 es'4 g,
     r8 bes8 as g f[ f16 g] as as g f g8 es
}