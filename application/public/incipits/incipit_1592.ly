\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef bass
  \key bes\major
   \tempo "1. o.Bez."
  \time 3/4
    d,8 a16 g f8 f f f
    f f16 es d8 d d d
}
