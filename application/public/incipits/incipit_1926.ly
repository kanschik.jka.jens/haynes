\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "1re. Sonate " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. Gravement"
  \time 4/4
  a4~ a16 a b a g4~ g16 g a g
  fis8 d fis16 g a b e,8 fis16 g e8. d16 
  d8 fis16 e fis8 fis

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Allemande. Legerement"
  \time 4/4
  a4 r8 fis16 g a8 d, e cis
  d8 e16 fis g8 e fis g16 a b a g fis
  e8 

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "3. Premier Air en Rondeau. Gracieusement"
  \time 3/8
  \partial 8
  a8 g \grace fis8 e8 a
  fis \grace e8 d8 a'
  b16 a g fis e d 
  cis8 \grace b8 a8

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe. Sonate " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Musette. Gracieusement"
  \time 3/4
  b8 a g a fis a
  g4 b,8 c d4
  e d c
  b8 c d e f g
  e4 fis g
  fis \grace e8 d4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Legerem.t"
  \time 4/8
  \partial 8
  d8 g[ g g g] 
  fis e16 fis d e fis d
  g8[ b, c d] b g r8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Premier Air en Rondeau"
  \time 3/8
  \partial 8
  d8 c16 d c b d a
  b8 \grace a8 g8 b' 
  a16 b a g a fis
  g8 d16 e f8
  e fis g
  fis4 

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Premier Menuet"
  \time 3/4
  d2 e4 d e8 fis g4
  fis g a 
  b2 a4 c2 b4
  

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "5. Premier Giga"
  \time 6/8
  g8 a g fis g a
  g fis g d e f
  e f e d e c
  b a b g4.
  

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIIe. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Lentement"
  \time 4/4
  a4 a'2 g4~
  g f2 e4~
  e d2 c4~
  c b e2~
  e4 d4. c4 b8 c4


}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Gayement"
  \time 3/4
  r4 a4 e'~
  e d8 c d4~
  d c8 b c4
  b e8 d c b
  a4 d8 c b a 
  gis4. gis8 a4 b

}
  
  \relative c''' {
  \clef treble
  \key a\minor
   \tempo "3. Lentement"
  \time 3/4
  r4 c4 c
  c b b 
  b a8 gis a b
  gis4 e a~
  a gis2
  a4 a a

}
  
  \relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Legerement "
  \time 2/2
  r4 a4 f'2~ 
  f4 b, e2~
  e4 a, d2~ 
  d4 c b c8 d
  c4 e e e 

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IVe. Sonate" 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Musette. Modérém.t"
  \time 3/8
  g8 g, g'~
  g fis16 e d c
  b8 g g'~
  ~g fis16 e d c
  b8 e16 d c b
  a4 a8

}



\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Premiere Paysanne"
  \time 2/2
  \partial 2
  b4. a8
  g4 d' e fis
  g2 b,4. a8
  g4 d' c b
  a2 b4. a8
  g4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. La Justine. Legerement"
  \time 4/8
  \partial 8
  b16 c
  d8[ d d d] 
  e[ d d d] 
  e[ d e fis]
  g fis16 e d c b c
  d8[ d d d]
  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Premier Air en Rondeau"
  \time 3/8
  \partial 8
  d8 d e fis
  g4 d8 
  e d c 
  b[ g] 

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "5. Premier Menuet"
  \time 3/4
  g8 a b c a b
  g a b c a b
  g4 fis8 e d c
  b4. a8 g4
  

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ve. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Lentem.t"
  \time 4/4
  d8 d, r8 d'8 d e16 d cis8 d16 e 
  f4 r8 f8 e16 f g e a g f e 
  f8 d bes'4. a16 g

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allemande. Gayement"
  \time 4/4
  \partial 8
  d8 
  a'4 r8 g8 f e d d'
  cis a b cis d4. cis8
  b g a b c4. 

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Lentement"
  \time 3/4
  d8 cis d4 d
  d cis cis
  f8 e f4
  f
  f e e 
  a8 g a4 a
  a g g

}

\relative c''' {
  \clef treble
  \key d\minor
   \tempo "4. Legerement"
  \time 3/8
  \partial 8
  a8 f d f
  e a, a'
  f d f e4 a8
  f d f
  e a, cis
  d16 cis d f e d
  cis8 e a

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VIe Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Musette. Moderement"
  \time 2/2
  \partial 2
  e4 d f e d g8 f
  e4 d f e
  d e c d
  b g 

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Premiere Gavotte"
  \time 2/2
  \partial 2
  g4 f
  e d8 c d4 b
  c g g' f
  e f8 g f4. e8 d2

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Allemande. Gracieusement"
  \time 4/4
  \partial 8
  r16 g16 g4~ g16 f e d e8 c e f16 g
  a8. g16 f e d c b8 g' d e16 f
  e8 

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "4. Premier Menuet"
  \time 3/4
  g4 c b
  c2 g4
  f8 e d e f d
  e4. d8 c4
  f2 e4 a2 g4 f4 e2 d2.

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "5. Sarabande"
  \time 3/4
  g4. a8 g a
  f4 f4. g8
  e d e f e f
  d4 g8 d e f
  e4 

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Premier Gigue"
  \time 6/8
  \partial 4.
 es8 f g 
 f es d es f es
 d4. g8 f g
 es f es d es c
 b a g

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Caprice " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. Lentement "
  \time 2/2
  \partial 2
  a4 g
  fis e d cis
  d e fis cis 
  d e8 fis g4 fis
  e2

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Lour. Gracieusement "
  \time 6/4
 d2.~ d4. a8 d4
 cis4. b8 a4 b cis2 \grace {b16[ cis]}
 d4. e8 fis4 e a g
 fis d fis e d cis 
 

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Vivement "
  \time 4/8
  d8[ e fis g] 
  a[ d, b' d,]
  a'[ d, b' d,]
  a' g16 fis e8 d
  cis a e'16 d e fis
  e8 a,

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Mineur. Gracieusement "
  \time 6/8
  d4 a'8 bes a g 
  f4 g8 a g f
  e4 f8 g f e
  f g a e f g
  cis,[ a] 

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Sarabande "
  \time 3/4
  d4. e8 fis4
  g e a
  fis d a'
  b4. a8 g fis
  e4 a e 
  fis8 g fis e d cis b4
  

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "6. Premier Tambourin "
  \time 4/8
  d8[ a' a g]
  fis[ e d e]
  fis[ d g fis]
  e16 d cis b a8 cis
  

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "7. Premier Menuet "
  \time 3/4
  a2 g4 fis e8 fis d4
  b'8 a g fis e d 
  cis4 b8 cis a4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "8. Chaconne "
  \time 3/4
  r4 d8 e fis g
  e4. e8 a4
  a g4. g8
  g4. e8 a[ g]
  fis4 

}
