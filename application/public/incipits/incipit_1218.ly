\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 4/4
  f4. f,8 a4 g8. f16
  c'4. c8 c4 d16 e f8 
  e4. e8 e4 d
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 3/8
  a32[ bes c16 a g f g]
  a32[ bes c16 a g f g]
  a32 bes c16  a32 bes c16  a32 bes c16  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 4/4
  r8 a a a  d d d e
  f4. f8 f e e f 
  d d16 cis d8 e cis e16 d cis b a g
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. [Menuet] alternative"
  \time 3/4
  f4 c4. bes16 c
  g'4 c,4. bes16 c
  a'4 g8 a bes g
}
