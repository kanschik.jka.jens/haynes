
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

 
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante Maestoso"
  \time 3/4
  c2 f16 e d c b4 a d16 c b a
  \grace a4 g2 g16 c e g g2 f4 f4 e16 c g a a g g f
 
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro assai"
  \time 4/4
   c2 c e16 d c8 c4 r4 c4 e16 d c8 c4 r2
 R1 r4 b8. d16 g,4 r4 r4 e8. g16 c,4 r4
  
 
}

\relative c'' {
 << 
\new Staff { \clef "treble"   
  \key g\major
   \tempo "3. Andante amoroso"
  \time 2/4
 \partial 8
 % Voice 1
  r8 r16 d16 e d d4
  r16 d16 e d d4
 r16 e16 f e e4
 r16 d16 e d d8 g16. b,32
 b16 a c b e d8 c16 b e d8 r8
}
  
\new Staff { \clef "treble" 
% voice 2
g,8 d'4 r8 g16 d d4 r8 d32 b a g c4 r8 g'16 e
 \grace e8 d4 r8 g16 b, b a c b e d8 c16 b16 e d8 r8

}
>>
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Minuetto"
  \time 3/4
    c4 c c c2 c16 d c b
 \grace b4 a2 a16 g c a g4 f e d4~d8. f16 \times 2/3 {e8 c g'}
 g4 f e g \times 2/3 {c8 b a} \times 2/3 {g8 f e} e4 d r4
  
 
}

