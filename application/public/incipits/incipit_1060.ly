\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  c4. c8 d4. d8 e4 c, r8 e' f fis
  g4 g2 a8 e
  g f f2 g8 d
}
