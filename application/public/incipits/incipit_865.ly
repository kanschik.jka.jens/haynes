\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro "
  \time 2/4
  d8 d d d
  d b16 c d8 g
  d d d d 
  d b16 c d8 g
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Largo"
  \time 4/4
  b8 c b a g fis g a
  b c b a g fis g a b4 e8 d c4
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
      d4 d d d d d
      d c8 d e fis g2.
}

