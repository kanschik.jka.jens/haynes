\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key bes\major
    \time 2/2
    \tempo "1. o.Bez."
        f,4 bes a r8 bes
        c d es d c4 r8 d
        es c16 d es8 d16 c  d8 bes16 c d8 c16 bes
        c8 a
}
