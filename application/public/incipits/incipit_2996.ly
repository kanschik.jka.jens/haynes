 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "[5.] o.Bez."
  \time 4/4
      d4 f e
      d f8 e d cis
      d4 f e d2 a'4
}
