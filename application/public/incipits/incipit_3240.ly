\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran, Alt, Tenor + Bass): Liebster Gott, wann werd' ich sterben? [BWV 8/1]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key e\major
                      \tempo "o. Bez."
                      \time 12/8
                      % Voice 1
              e16 fis gis a b8~ b16 e, gis b d cis cis4.~ cis16 e, a cis e dis
              dis e fis gis a fis dis4.
                 
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\major
                     \set Score.skipBars = ##t
                        % Voice 2
             r2. a,16 b cis d e8~ e16 a, cis e gis fis 
             fis4.~ fis16 b, dis fis a gis 
                      
                  }
                  

                  
>>
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Sopran"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key e\major
                      \tempo "o. Bez."
                      \time 12/8
                      % Voice 1
              R1.*12 r2. r4. b8 cis b
              e4. b cis~ cis4 b8
              a4.~ a8 cis b \grace a8 gis4. g fis2.
                 
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Alt"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\major
                     \set Score.skipBars = ##t
                        % Voice 2
             R1.*13 r4. gis4 gis8 a4.~ a4 gis8
             fis4 b,8 fis'4.~ fis8 e dis cis4. dis2.
                      
                  }
                  
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Tenor"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\major
                     \set Score.skipBars = ##t
                        % Voice 3
             R1.*13 r4. e'4 e8 e4.~ e4 e8
             dis4 fis8 \grace e8 dis4. \grace cis8 b4. ais
             b2.
                      
                  }
                  
                  \new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Bass"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\major
                     \set Score.skipBars = ##t
                        % Voice 4
             R1.*13 r4. e,,8 fis e a4. e
             fis4.~ fis4 b,8 e2.
             b
                      
                  }
                  

                  
>>
}

