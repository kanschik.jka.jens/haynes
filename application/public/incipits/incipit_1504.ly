 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo ""
  \time 3/4
e4. d8 c4
d g, g'
a4. g8 fis4
}
