 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Andante"
  \time 6/4
  << {
      % Voice "1"
      cis8 d cis d e cis  d cis d fis e d
      cis8. b16 cis8. e,16 fis8. e16
      b'8.
         } \\ {
      % Voice "2"
      a8 b a b cis a  b a b d cis b
      a gis a cis, d cis gis'
      } >>

}


\relative c'' {
  \clef treble
  \key a\major	
   \tempo "2. Vivace"
  \time 4/4
  a16 e a e  a e a e  a8 cis e, e
  b'16 a gis fis e8 e  b'16 a gis fis e8 e
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "3. Siciliana"
  \time 6/8
  \partial 8
  << {
      % Voice "1"
      e8
      e8. fis16 e8 e8. fis16 e8 
      e8. cis16 e8 a,4
         } \\ {
      % Voice "2"
      cis8
      cis8. d16 cis8  cis8. d16 cis8
      b8. a16 b8 e,4
      } >>

}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "4. Allegro"
  \time 3/8
    a16 b b8. a32 b
    cis16 a e8 e
    b'16 cis cis8. b32 cis
    d16 b e,8 e
}