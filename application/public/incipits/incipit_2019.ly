 \version "2.16.1"
         #(set-global-staff-size 14)
         
     \relative c'' {
  \clef treble
  \key  bes\major
 \tempo "1. o.bez."
  \time 2/2
    \override TupletNumber #'stencil = ##f
  \override TupletBracket #'stencil = ##f
      R1 r2 bes4 r8 es8
      c4 c8. bes16 bes4 r8 es~
      es d d c16 bes
      f'4 r8 bes,8
     }
