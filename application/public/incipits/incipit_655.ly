
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ier. Divertissement"
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. Ouverture"
  \time 2/2
r8 a8 a[ a] fis4 a
d, d8 d  d4 e 
fis g8 a g4 fis
e4
 
}
 


 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe. Divertissement "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Entrée"
  \time 2/2
c4. d8 e4. f8
g2 g4 g
a a8 g f g a f
g2 e8 f g4
c, d g, c d2
  
}


