 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Violin"
                     \set Staff. shortInstrumentName = #""
                     \key b\minor
                      \tempo "o.Bez."
                      \time 4/4
                      % Voice 1
                          r4 fis8 b, g' g fis8. e16
                          d b' d, b'   d, b' d, b'   cis, b' cis, b' cis, ais' cis, ais'
                          fis b fis b   d, b' d, b'  d, b' d, b' cis, a' cis, a'
                          a, d a d  a a' a, a' d, fis d fis a, e' a, e'
                          a, fis' a, fis'  b, fis' b, fis' fis, fis' fis, fis'  cis fis cis fis d4 r r2
                          
}
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #""
                     \key d\major
                      % Voice 2
                         R1 r4 fis8 b, g'8 g fis8. e16
                         d4 fis8 fis g g e8. e16
                         fis4 fis8 g a d, cis8. d16
                         d4 d8 e fis b, ais8. ais16 d4 r r2
}
>>
}

