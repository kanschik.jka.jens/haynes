\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Andante"
  \time 4/4
      e4 r16 gis fis e b'8 b, r16 b cis dis
      e dis e8 r16 gis fis e dis cis b8
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Larghetto"
  \time 3/4
  \partial 4
  cis4
  \grace cis8 b2 a4 gis d' cis 
  \grace cis8 b2
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Larghetto"
  \time 3/4
    fis,4 a b8. cis32 d
    g,4. fis16 g fis4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 3/4
    g4 b d e g
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo"
  \time 3/4
    d4 \grace es8 d16 c d8 \grace es8 d16 c d8 
    es8. d16 es2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
    bes'8 f16. es32 \grace es8 d8 c16 bes g'8 f r bes,
    a16 bes8 c16 es g8 f16 \grace es8 d \grace c8 bes8
}
