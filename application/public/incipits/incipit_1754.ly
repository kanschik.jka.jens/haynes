\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Aria. Largo"
  \time 4/4
  c4.~ c16 d  b8 c r16 c d c
  b a g8 r16 g' a g f e d8 r16 f g f
  e d c8
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Aria. Cantabile"
  \time 3/8
      a8 b4 c4.
      f16 e d c b a
      gis8. fis16 e8
      a b4  c8. b16 a8
      b gis4
      a4.
}
