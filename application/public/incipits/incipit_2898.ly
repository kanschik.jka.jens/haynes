\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "[voce]"
  \time 3/4
  r4 d8 bes g es'
  d4 g, r
  bes8 a bes c d es
  d2 bes4
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "[Oboe]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*10 
   r4 e8 c a f'
   e4 a, r
   c8 b c d e f
   e2 d4
}
