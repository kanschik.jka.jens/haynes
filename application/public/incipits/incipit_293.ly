 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  es,2 g16 es8. bes'16 g8.
  es'8. bes16 bes2 d16 f8.
}
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
    es2 es8 g g bes
    bes2 as4 g 
    \grace g8 f4 es
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 2/2
  f4 f2 bes4
  bes16 a8. a4 r bes
  \grace a8 g4 g8. g16 fis8 g bes g
  \grace a8 g4 f r 
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Tempo di Minuetto"
  \time 3/4
  <es, bes' bes'> 2.
  g'2.
  es4 f g
  \grace g8 as4 \grace g8 f2
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. "
  \time 3/4
  \partial 4
  
}