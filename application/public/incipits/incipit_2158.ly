 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 4/4
   \override TupletNumber #'stencil = ##f
  d4. \times 2/3 { d16[ e fis]} g8 d16 c b8 g'16 fis
  e8 d16 e c8. b16 b8 g d c'
  b b16 a g8 b16 a
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 2/4
  r8 d b16 a b c
  d8 c16 d e8 d16 c
  d8 g b,16 a b c d8 c16 d e8 d16 c
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Adagio"
  \time 4/4
  g'4. a8 g c, b16 c d g
  e d c8 r c16 d e f g a d,8 b
  c16 e f e d c d c b8[ d]
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 3/4
  g'8 g, d' g fis8. e32 fis
  g8 g, d' g fis8. e32 fis
  g8 d e16 d c b a8 d
  b4 r8
}