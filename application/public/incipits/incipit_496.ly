\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "Allegretto"
  \time 2/4
    a8 a d4 b8 b e4 cis8 cis fis4 d8 d g4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "Polonaise"
  \time 3/4
  cis4~ cis16 d fis e  d cis b a
  a8 gis gis2
  d'4~ d16 e eis fis  e d cis b
  bis8 cis cis2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "Adagio"
  \time 3/4
    b4 e gis
    b, fis' a
    gis8 fis e dis cis fis e2 dis8 r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\major
   \tempo "Allegretto "
  \time 4/4
  fis2 e8 dis e fis
  fisis2 gis4 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key fis\major
   \tempo "Adagio"
  \time 4/4
  cis1~ cis
  cis8. fis16 fis2 e4 e8 d d d4 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key cis\major
   \tempo "Poco Adagio"
  \time 12/8
  r4 r8 gis'4. eis cis bis4 r8
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key ais\minor
   \tempo "Vivace"
  \time 2/2
  ais2 ais' cis ais, gisis bis'
  dis
  gisis,, ais ais'
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key dis\minor
   \tempo "Adagio"
  \time 4/4
  ais2 dis4 fis
  ais,4. b8 ais dis fis ais
  ais8. gis16 fis2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key gis\minor
   \tempo "Andante"
  \time 9/8
  gis'4. gis8 dis b' ais cis fisis,
  gis4. gis8 dis b' ais cis fisis,
  gis dis gis b ais gis fis e dis
  fis4 e8 dis4 r8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key cis\minor
   \tempo "Largo"
  \time 4/4
  e2 dis4 gis cis,2 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key fis\minor
   \tempo "Allegro moderato"
  \time 6/8
  \partial 8
  fis8 eis4 fis8 e4 fis8
  cis4. d8 r d
  bis4 cis8
  gis4 a8
  fis4 r8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "Allegro moderato "
  \time 3/8
  \partial 8
  fis,8 
  b16 d fis e d cis b d fis e d cis d8 r r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. XIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Allegro moderato"
  \time 2/4
  \partial 8
  d16 es
  f g f d  f es d c
  bes8 bes bes

}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. XIV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "Scherzando"
  \time 2/4
  bes4 es
  g8. fis16 g8. f16
  es8. d16 es8. c16
  bes4 bes

}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. XV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key as\major
   \tempo "Adagio"
  \time 2/4
  c,8 es as c
  c8.. bes32 as8 r

}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. XVI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key des\major
   \tempo "Poco Adagio"
  \time 4/4
  as4.. bes32 c des4 des
  des4. es16 f es4 des

}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. XVII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key ges\major
   \tempo "Andantino"
  \time 6/8
  des4 des8 es4 es8
  ces8. bes16 ces8 des4 des8
}
  
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. XVIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key ces\major
   \tempo "Andante"
  \time 3/4
  \partial 16
  ges16
  ces4 es ces
  ces bes r8. ges16
  ces4 des es ges2 fes8 r

}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. XIX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key as\minor
   \tempo "Allegretto"
  \time 2/4
  \partial 8
  r8
  r es d es
  ces4 bes
  as8 es' d es
  ces4 bes
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. XX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\minor
   \tempo "Andante"
  \time 6/8
  \partial 4.
  bes8. ces16 bes8
  ges'4 ges8 \grace ges8 f8. es16 f8
  es4 r8
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. XXI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\minor
   \tempo "Adagio"
  \time 4/4
  bes'2 f4 des
  bes4. des16. c32 bes8 r r4
  a4 c f a
  c c16 des es des c8 r r4

}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. XXII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "Allegro"
  \time 4/4
  \partial 16
  c,16
  as'4.. g16 f4 r8. c16
  c'4.. bes16 as4 r8

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. XXIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "Allegro assai"
  \time 2/4
  \partial 4
  r4 R2 r4 g'8 g
  as16 g fis g f es d c
  c b a g fis g a b
  c b c es d c d f
  f es d c
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Nr. XXIV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "Lento ma non troppo"
  \time 2/2
  g'2 d bes' a g4.. es16 es2
}
