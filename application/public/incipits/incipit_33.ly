\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "Aria 1. Allegro"
  \time 4/4
  r4 r8 c16 g c8 d16 e d8 g16 f
  e8 c r g'16 f e8 d16 c b8 b16 c
  d8 g16 g g8 f16 e f8 f f e16 d 
  e8 f16 e f e f e d8 d r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "Aria 2."
  \time 4/4
  c4 d8 e d4 e8 f
  e4 f16 e d c d4 g,
}
