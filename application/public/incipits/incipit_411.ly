 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Amoroso"
  \time 3/4
    f2 es4
    \grace f8 es8. d16 d4 d
    d8. es16 es4 d
    d8. es16 es4 r
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Minuetto con moto"
  \time 3/4
  \partial 4
    f8. es16 d4 f8. es16 d8 r
    d2 c8. d16 es4 c8. d16 es8 r
}
