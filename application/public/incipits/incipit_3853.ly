\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Largo"
  \time 2/2
  r8 b' g e dis4 r8 fis
  fis fis16 g a8 a a g r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 2/2
  e8 g16 a b8 e, fis16[ e fis g] fis a g fis
  e8
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Adagio"
  \time 4/4
  e2. dis4
  g2.
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 6/8
  e4. fis8 b, b'
  g16 a g fis e8 e4 dis8
}

