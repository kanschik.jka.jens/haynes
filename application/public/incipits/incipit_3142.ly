\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Auch die harte Kreuzesreise [BWV 123/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key fis\minor
                      \tempo "Lento"
                      \time 4/4
                      % Voice 1
                  r4 a8 gis16 fis bis cis dis fis a8 gis16 fis
                  eis gis cis, e g8 fis16 e dis fis b, d fis8 e16 d
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key fis\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                     R1 r2 r4 d,8 cis16 b
                     eis fis gis b d8 cis16 b
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key fis\minor
   \tempo "Lento"
  \time 4/4
     \set Score.skipBars = ##t
    R1*4 r4 a8 gis16 fis bis cis dis fis a8 gis16 fis
    eis fis32 gis cis,8 r4
    
    
    }