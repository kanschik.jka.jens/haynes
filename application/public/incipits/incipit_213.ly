\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Staccato"
  \time 3/2
    g2 g g
    a a d
    b e e
    e d d
    g c, c
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  g8 d'16 c b c d8 e
  d4 c
  b r8
  
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Adagio"
  \time 3/4
  g8 a g fis e d
  e2 fis4 g8 fis g b a g
  fis4 g g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Tempo di minuetto"
  \time 3/4
  d8 b c a b g
  g'4 fis e
  d8 e c2 b2.
  }