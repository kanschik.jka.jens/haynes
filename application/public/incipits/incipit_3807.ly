\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 4/4
  g'2~ g4. g16 d
  e8 g, c c c b b a16 g
  fis8 a d c b d g a
  fis4 r8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/2
  g4 a b8 d4 e16 fis
  g8 fis16 e d8 c b d4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Largo"
  \time 3/2
  R1. r2 b1~ b g'2 fis4. dis8 e2. fis4 dis2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Vivace"
  \time 3/8
  \partial 8
  d8 g16 fis g a g d
  e d e g d g
  c, b c e d c
}

