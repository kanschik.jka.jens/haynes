\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4		
  g'8 c,16 d  e g f e d8 g, r g'
  a f16 e  d8 e16 f g4 r8 c,
  f d16 c b8 c16 d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 2/2		
  c4 d e f
  f f8 e d g, r g'
  a16 g a g  f[ d e f]  g f g f  e[ c d e]
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Largo"
  \time 3/4
  a'8 gis a4. b8
  gis8 fis e4. a8
  f8 e f4 g e8 d e4.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 3/4	
  g'4 e8 f g a g2 c4
  g e8 f g a g2 c4
}

