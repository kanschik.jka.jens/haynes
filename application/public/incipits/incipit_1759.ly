\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. o.Bez."
  \time 6/8
  c8 d16 c b8   c8 d16 c b8
    c8 d16 c b8 c8 g c
    g' f16 es d c  b8 a g
}
