\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 3/4
  c4 c c
  d8. e32 f f4. f8
  \grace g8 f4 e r
  d d d
  e8. f32 g c,4 b
  \grace b8 c2 r4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro [Tutti]"
  \time 2/4
  \partial 16
  g16
  c8. a'16 g f e d
  c8. a'16 g f e d
  c8 c,16. d32 e8 f16. g32
  c,8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro [Solo]"
  \time 2/4
  \partial 16
  r16
   \set Score.skipBars = ##t
   R2*62
   r4 r8 g
   c8. a'16 g16. f32 e16. dis32
   \grace dis8 e8. f16 e16. d32 c16. b32
   \grace d8 c16. b32 c8 r4
}

