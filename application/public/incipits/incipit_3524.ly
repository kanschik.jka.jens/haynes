\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 3/4
    r4 c f e2 f4
    bes, g8 a bes4
    bes2 a4
}
