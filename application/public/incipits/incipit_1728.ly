\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Largo"
  \time 4/4
  a4. b8  c b c d
  e d e f e a16 gis a8 b
  gis4.
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro"
  \time 2/2
  a4. a'8 g f16 e f8 e16 d
  e8 gis, a d c d16 c b4
  a8 b c d e b e,4 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Adagio"
  \time 3/4
  r4 fis e
  dis e2
  e4. e8 d c
  b4 a gis a b a gis e'2
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Vivace"
  \time 3/4
  e4 a e c2 b4
  c8 d e4 d c b8 c a4
}
