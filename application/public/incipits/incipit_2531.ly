 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "Introduction. o.Bez."
  \time 2/2
  a'4. a8 fis4 a
  d,2 r8 fis g a
  b4. a8 g4 fis8 g
  e4 e e
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "Air. Gay"
  \time 4/4
  \partial 8
  a8 d16 cis d e  d fis e d  e d e fis  e g fis e
  fis8. e16 d8 a' g a16 g  fis8 e16 d
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "Air. Tendrement"
  \time 3/4
  a'4 g8 f e d
  cis4. d8 cis d
  e f16 g f4. e8
  f[ e d]
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "Air. Gay"
  \time 3/4
  a4 a a
  d d8 e fis4
  e a8 b a g
  fis e d fis e d
  e4 a, a'
  a4. b8 gis4
  a2 r4
}
