\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  \partial 4
  g4 c2. e8 c
  f2. d8 b
  c4 r8 r16 c, e8[ r16 e] g8[ r16 g]
  c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \partial 4
  r4
   \set Score.skipBars = ##t
   R1*3 
  r4 r8 r16 c16 e4 g
  c2 c8 g a e
  g f f4 r d8. e16
  f4 f4. g16 a g8 f
  \grace g8 f4 e r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Tutti]"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Solo]"
  \time 4/4
   \set Score.skipBars = ##t

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 4/4

}