 \version "2.16.1"
         #(set-global-staff-size 14)
         
     \relative c'' {
  \clef treble
  \key  a\major
 \tempo "1. o.bez."
  \time 3/4
    \override TupletNumber #'stencil = ##f
  \override TupletBracket #'stencil = ##f
  \partial 4
     a4 b8. cis16 d4 e
     d b2 cis4 a8 b cis a b4 a2
     }
