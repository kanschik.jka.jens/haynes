\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Allegro"
  \time 2/4
      r8 b' b, cis
      d4 cis b4. cis8
      ais16 fis gis ais  b cis d e
      fis8 e d e
      cis fis, r4
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Largo"
  \time 3/4
      d,4 a'4. d8
      e4 a,4. fis'8
      g4 g4. a8
      fis4 b,4. d8
      e4 a,4. cis8
      d4
}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Allegro"
  \time 3/4
   r8 b' b, cis d e 
   fis d16 e  fis8 d16 e  fis8 d16 e
   fis8 b b, cis d e
}
