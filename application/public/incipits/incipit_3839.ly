\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "bass"   
                     \key g\minor
                      \tempo "o.Bez."
                      \time 4/4
                      bes,,4 d g2~
                      g4 fis a2~ a4 g d'2~ d4 g,8. a16 bes2
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key g\minor
                     % Voice 2
                     r8. g16 bes8. g16 d'8. g,16 bes8. g16
                     r8. a16 c8. a16 es'8. a,16 c8. a16
                     r8. bes16 d8. bes16 f'8. b,16 d8. b16
                     r8. c16 es8. c16 g'8. cis,16 e8. cis16
                  }
>>
}
