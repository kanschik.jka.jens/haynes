\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8
  a'8
  a a a a
  a g16 f e a g f
  e a g f e d c b
  c8 a r e'
}
