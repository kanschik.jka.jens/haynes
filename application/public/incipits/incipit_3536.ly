\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
     f8 f f2 \grace f8 es8 d16 es
     d8 d d2 \grace d8 c8 bes16 c
     bes8 bes
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Soprano]"
  \time 4/4
    d2. \grace f8 es4
    d2. \grace d8 c4
    bes2. a4
    bes4. d8
    f,4 r
}
