 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\major
                      \tempo "1. "
                      \time 4/4
                      % Voice 1
                      a'4 fis d a fis' d a fis
                      R1 R
                  }
\new Staff { \clef "treble" 
                     \key d\major
                        % Voice 2
                        a4 d fis4. fis8 d4 fis a4. a8
                        a8. b16 a8. b16 a4 g8 fis
                        e4
                  }
>>
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2."
  \time 3/2
  fis2. a4 g a
  fis2. a4 g a
  b2 b4 a g fis e2.
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. "
  \time 6/8
  d,8 d16 d d8  d d d
  fis fis16 fis fis8 fis fis fis
  a a16 a a8 a a a
}
