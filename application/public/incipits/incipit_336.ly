\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [tutti]"
      \set Score.skipBars = ##t
  \time 3/4
  g8 g g g g g
  d' d d d d d
  g4 r e
  fis r d e
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Oboe]"
      \set Score.skipBars = ##t
  \time 3/4
  R2.*15
  g16 fis g a   g fis g a   g a b c
  d cis d e   d cis d e   d e fis d
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 4/4
  g'8 fis16 e d c b a b8 fis g a
  b16 fis g a b a b cis d2~ d1
  d4 cis
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
         \set Score.skipBars = ##t
  \time 2/4
  g16 a b c d8 d
  d e16 fis g8 b,
  c16 b c a b8 g
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Air. Affettuoso"
         \set Score.skipBars = ##t
  \time 3/8
    g16 fis g a b c
    d8 g4
    fis16 g a8 d,
    c b4
}