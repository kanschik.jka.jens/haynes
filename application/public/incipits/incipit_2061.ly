\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 6/8
  \partial 8
  d8
  g8. a16 g8  d'16 b fis g d f
  \grace f8 e4 d8 r16 d c b a g
  r e' d c b a r c' a fis d c
  c b e d d8
}
