\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. allegro spirituoso"
  \time 4/4
    c8 r r4 e8 r r4
    a,8 r r4 r8 a b c
    d r r4 f8 r r4
    \grace { d,16 g} b1
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 2/4
    d,8. e32 fis g8 a
    b a r4
    c8. d32 c b8 a
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Variaziona con moto"
  \time 2/4
  \partial 8
    g8
    c8. g'16
    f e d c
    a'8 a r
}
