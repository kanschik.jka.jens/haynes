 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. [Allegro]"
  \time 2/2
  c,16[ e g e]  c e g e  
  c16[ e g e]  c e g e  
   c8 e g f b \grace a8 g r4
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Andante"
  \time 3/4
  g4 c d
  \grace { c16[ d] } e2 d4
  \grace { e16[ f] } g4 c,8 b c4
  d2.
  
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Ritternelle"
  \time 12/8
  \partial 8
  g8
  c d e e d c b c d g,4 g
  c b c a g a d c d b a b
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Allegro"
  \time 2/4
g16 c b c  e c b c
g c b c  f c b c
g c b c  g'8 e 
}