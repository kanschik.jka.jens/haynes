\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
   \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
   \partial 8
   \times 2/3 { c,16 d e}
    f4 r8 c a'4 r8 f
    c'4. d16 e f8 f f f
    f,2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. o.Bez."
  \time 2/4
    c8. e16 g8 g 
    g8. f16 d8 r
    e4 g16 f e d
    c8. \grace e16 d16
    c b a g
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. o.Bez."
  \time 2/4
    f4. c8 a'4. f8 c'4. bes16 a
    g4 r8
}

