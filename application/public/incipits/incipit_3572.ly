\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4	
      g'2 d b g
      g, \grace b8 a4 g8 fis
      g4 a b c
      d g,
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Solo]"
  \time 4/4	
      g'2 d b d
      g, \grace b8 a4 g8 fis
      g a4 b c d8~
      d4. g,8 \grace b8 a4 g8 fis
}



\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Andante [Tutti]"
  \time 3/4
    fis,8. g16 a8 a a a
    a8. d16 a4 r8 r16 d16
    b4 a g g fis r
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Andante [Tutti]"
  \time 3/4
    fis8. g16 a8 a a a
    a8. d16 a4 d,16 fis a d
    b4 a
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Rondo"
  \time 2/4
       g'4 d, e d
       g' d, c b
       
}