 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 4/4
  r8 e, g b c, c16 d32 e c8. b16
  b8 b' a16 g fis e
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "bar 5"
  \time 3/4
    b,8 fis'4 e16 dis e dis cis b
    e fis e b g b e fis g fis e fis
    g8 c,
}
