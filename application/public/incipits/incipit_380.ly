\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato [Tutti]"
  \time 2/4
  \partial 16
  f,16
  bes8. c16 c4
  d8. f16 es16. d32 c16. b32
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato [Solo]"
  \time 2/4
  \partial 16
  r16
   \set Score.skipBars = ##t
   R2*55 
    r4 r8. bes16
    f'4 f
    f32 bes16. c4 a32 g16.
    e32 f16. f4 fis32 g16
}