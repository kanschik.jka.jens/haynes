
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "La Steinquerque"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key bes\major
   \tempo "1. gayement"
  \time 4/4
r8 f16 f f8 f f d f f 
f4 r4 r2
r8 d16 d d8 d d4 r4 
}

\relative c'' {
\clef "treble"   
  \key d\minor
   \tempo "2. gravement"
  \time 4/4
d4. c16 bes c4 d
b c8 b16 a b4 a8 g16 f
g4. f16 e fis8 d g4~ g16 f e f fis8. fis16 g4


}

\relative c'' {
\clef "treble"   
  \key d\minor
   \tempo "3. Legerement"
  \time 4/4
r8 d8 bes' d, c e a c,
bes[ d] g bes, a g' g8. fis16 g4 r4 r2 

 
}


\relative c'' {
\clef "treble"   
  \key bes\major
   \tempo "4. mouvement de fanfares"
  \time 2/2
g2 r2 r1 r2 r4 f'4
f8 es f g f g f es
d c d es d es d c
bes a bes c bes c bes c
d c d es d4 f




}

\relative c'' {
\clef "treble"   
  \key bes\major
   \tempo "5. Lentement"
  \time 3/2
bes1 bes2
f' f f 
f2. es4 d c
d c d es d es 
c2 c 


}

\relative c'' {
\clef "treble"   
  \key bes\major
   \tempo "6. gravement"
  \time 2/2
bes2 r2 r1 f'4 es8 d c d c d 
es2. es4 


}

\relative c'' {
\clef "treble"   
  \key bes\major
   \tempo "7. gayement"
  \time 3/4
f4 es8 d c d 
es4 d8 c bes c
d4 es f
c2 bes4


}

\relative c'' {
\clef "treble"   
  \key bes\major
   \tempo "8. Lentement"
  \time 2/2
f4 es8 d c4 d
es1~ es2 d4. c8
d2 c4. bes 8 bes1

}







