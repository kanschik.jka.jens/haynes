\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/2
  d8 d d d   d16 cis b a g fis e d
  e'8 e e e   e16 d cis b a g fis e
  d8 fis a d
}



\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 3/4
  a4 d8 e16 fis fis8 e16 d
  e2 a,4
  b8 a16 g b8 cis16 d e8 fis16 g
  g2.
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3.Allegro"
  \time 2/2
  d8 d, d d d16 e fis g a[ b cis d]
  e8  e, e e   e16 fis g a b[ cis d e]
  fis8
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Rigaudon"
  \time 2/2
  \partial 4
    a4
    a d8 cis d4 a
    a e'8 d e4 a, a g'8 fis g4 fis
    e8 d cis b a4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Menuet altern."
  \time 3/4
  d4 a d8 e16 fis
  e4 d8 cis b a
  e'4 a e8 fis16 g
  fis4 e8 fis d4
}

