\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro tempo moderato"
  \time 4/4
    c4 c e16 d c4 d16 e
    f8 f f f \grace g8 f8 e r4 e e
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 3/4
 
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro finale con spirito"
  \time 6/8
 
}
