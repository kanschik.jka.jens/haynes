\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [tutti]"
  \time 4/4
  c2 r4 r8 g16 a32 b
  c2 c4. c8
  d2 r4 r8 g,32 a b c d2 d4. d8
  e2 e,
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [solo]"
  \time 4/4
  c2 r8 g a b
  c2 r8 g e' d
  \grace e8 d8 c c4 r8 g b c
  d2 r8 a f' e
  \grace f8 e d d4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Romanze Cantabile"
  \time 2/2
f4 c a r8. c16
d4 bes r2
a2 c g r
b f' e r
g2 f8 e d c a'2
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondeau: Polon."
  \time 3/4
  c4. d16 e c8 c
  b d g,4. a16 b
  c4. d8 e fis
  g16 fis g fis g fis g fis g e f d
  c4.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [tutti]"
  \time 4/4
  c4. bes8 a4 bes c f a r
  bes4. a8 g4 a bes e g r
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [solo]"
  \time 4/4
  c4. d16 e f8 g a bes
  a2 g4 r
  c4. \times 2/3 { d16[ e f] } g8 a bes c
  bes2 a4 r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio [tutti]"
  \time 3/4
  d8 a r d f d
  d cis r cis16 a e'8 e
  e8. cis16 a8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio [solo]"
  \time 3/4
  d8. a16 f'4 r8 d
  \grace e8 d8 cis cis4 r
  cis8. e16 g4 r8 e
  \grace f8 e d d4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondeau"
  \time 2/4
  \partial 4
  c8 c
  f4 a8 f c'4 a8 f
  bes bes16 g a8 a16 f g8
}

