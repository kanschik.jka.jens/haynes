\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo I " 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key f\major
                      \tempo "1. Allegro assai"
                      \time 4/4
                      % Voice 1
                      f1 e
                      \grace c8 bes'1 a
                      c4 c2 d8 c
                      c bes bes2 a4
}
\new Staff { \clef "bass" 
                     \key f\major
                        % Voice 2
                        r8 f,, a c bes a g f
                        r c e g f e d c
                        r c e g f e d c
                        r f a c bes a g f
                        a f a a   a a bes a
                        a g g g e c f f,
}
>>
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 6/8
  f8. g16 f8 bes4 f8
  fis g g g4 r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 2/2
  r8 c a bes c d e f
  f e e e e4 r
  bes'2~ bes8 a16 g a[ g f e]
  g8 f f f f4 r
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  r8 c16 b c8 g e' c g' e
  c4 r r2
  r8 c16 b c8 a f' c a' f
  c4 r r2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 2/4
 r8 c bes a
 f'8. e32 d c8 r16 r32 a
 g8. a16 bes d c bes
 \grace c8 bes8 a
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Moderato con Variazione"
  \time 3/4
  \partial 4
  c8 d
  e2 f4 e d8 c d e
  f4 f g \grace f8 e2
}