 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/2
  g'4. d8 d4 d8 c
  b4. c8 d4 c8 b
  a4. b8 c b c d b4 g d' g
}
