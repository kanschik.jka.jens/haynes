\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key g\major
  \override TupletBracket #'stencil = ##f
    \time 4/4
    \tempo "1. o.Bez."
      g'2~  \times 2/3 { g8 b a}  \times 2/3 { \grace a8 g8 fis g}
      \times 2/3 { fis8 dis e }  \times 2/3 { e8 e e} e4 r

      c'2~  \times 2/3 { c8 e, fis }  \times 2/3 { \grace a8 g8 fis e}
      e4
}



