\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro "
  \time 4/4
    d4. d8 a'4. a8
    fis8 fis fis fis e16 d cis d e fis g a
    fis e d fis e a g a fis e d fis e a g a
}

  
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Largo"
  \time 4/4
  fis8 fis fis fis g16 e dis e b e dis e
  cis a a cis cis e e g fis  d cis d  a d cis d
}

 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Menuet"
  \time 3/8
  d8 a fis'
  fis e g
  fis e d
  e16 d cis  b a8
}