\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Solo I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 4
  c4 
  g'4 g16 fis g a  g8. f16 e8. d16
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Solo II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
   c8
   g'4 \times 2/3 { g16[ e g] } \times 2/3 { f16[ d f] }
}


\pageBreak

\markuplist {
    \fontsize #3
    "Solo III " 
    \vspace #1.5
}


\relative c' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 4
  d'4 d4. b8 d4 r8 g
  fis4. a8
}

