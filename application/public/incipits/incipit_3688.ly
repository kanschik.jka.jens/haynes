\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
  d,16 f bes8 bes bes  c bes bes bes
  f'16 d bes8
}
