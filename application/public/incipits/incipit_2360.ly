\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  b8
  g fis16 e b'8 e16 fis dis8 cis16 b r16 e fis g
  a fis dis fis a b g fis g[ fis32 e e16]
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Vivace "
  \time 3/4
  b4 e fis g a8 g fis4
  b4 a8 g fis e dis4. cis8 b4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Giga. Vivace"
  \time 6/8
  \partial 8
  b8
  e4 dis8 e4 b8
  g a b e,4 b'8
  e4 fis8 g fis e
  d4 c8 b4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  d8 d16 g, g d' c b a8 d, r16 a' b fis
  g8 g32 a b16 a8. g16 fis8 e16 d r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allemande. Vivace"
  \time 4/4
  b8 d b a16 g g4 r8 d'
  e e16 d d c c b
  a8 d,~ d b'16 c
  d8 g, b e cis b16 a r8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
  \partial 4
  g4
  d' c8 b a g fis2 e'4
  d c b a2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga. Vivace"
  \time 6/8
  \partial 8
  d8
  b8. c16 d8 g,4 g'8
  fis8. g16 a8 d,4 g8
  e4 d8 c4 b8 a4. d,4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  r8 d, fis8. g16 a8 e r16 a b cis
  d8 d d d d cis r e
  fis32 a16. fis32 a16. e8 a,
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allemanda. Allegro"
  \time 4/4
  \partial 8
  a8
  d16 cis d e d8 a fis e16 d r8 a'
  d e16 fis e8 fis16 g fis8 e16 d r8
}
  
  \relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Vivace"
  \time 3/4
  a4 d cis d2 a4
  d8 e fis4 e fis2 e4 fis8 g a4 b a g8 fis e d
}
  
  \relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Presto"
  \time 4/4
  \partial 4
  a4
  d8 e d4 d cis d a fis d
  \afterGrace d'2( e8)
   e2 fis2.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  e16 a, a8 f'16 e e8 a16 b gis8 fis16 e r e fis e
  d c b c d b e b c8 b16 a
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allemanda. Allegro"
  \time 4/4
  \partial 4
  r16 e d e
  c e d e b e d e c d c b a e' f g
  a b, c d d8. e16 e4 r16
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Corrente. Vivace un Poco"
  \time 3/4
  \partial 8
  r16 a
  a4. r16 b c8. d16
  b8.[ c16 d8. b16 e8. b16]
  c8.[ b16 a8.] 
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Largo e Affetuoso"
  \time 3/4
  e2 f4 e a, f' e d c b8 a b c b4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "5. Giga. Allegro"
  \time 6/8
  \partial 8
  e8 a,8. b16 c8 b8. c16 d8
  c4 b8 a4 e'8
  f4 e8 a4 b8
  gis4. e4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allemande. Largo"
  \time 4/4
  \partial 8
  g'8
  c16 e,32 f g8 c, e16 d d8 g, r16 d' c b c a fis a d, c' b a b8 a16 g r16
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allemanda. Allegro"
  \time 4/4
  \partial 8
  g'8
  c b16 a g f e d e8 g, b g
  c b16 a d e f a, b8 a16 g r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Corrente. Vivace"
  \time 3/4
  \partial 8
  r16 c
  c8.[ g16 c8. d16 e8. f16]
  g4 c, a' g g4. f8 e4. r16
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Rondeau. Largo"
  \time 3/2
  \partial 2
  g'2
  e2. f4 g2
  a, b c f d2. c4
  b2 \grace a8( g2)
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Allegro"
  \time 4/4
  \partial 8
  r16 g
  c8.[ e16 g,8. b16] c4 g8. g'16
  a8. a16 g8. f16 e4. r16
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  g16 a b c d8 d, g4 r8 g'16 fis e16. g32 d16. e32 c16. b32 c16. d32 b8 a16 g r8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  b16 a b c d8 g,16 g' g d e d d8 g,16 g'
  g d e d d8 e16 d c b a g fis8 e16 d
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Vivace"
  \time 3/4
  g4 b8 a b c d4 g, g'8 fis
  e4 fis8 e d c
  b2 a4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Postillon. Allegro"
  \time 6/8
  \partial 8
  d8 g4 d8 b c d
  g,4.~ g4 d'8
  e d c e d c
  d4 g,8 g a b
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Menuet"
  \time 3/4
  d8 e d c b a
  g2 d'4 e4 c4. d8
  b2 a4
}

