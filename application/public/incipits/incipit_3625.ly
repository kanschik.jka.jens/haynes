 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 2/2
    f,4 \times 2/3 { a8[ bes a] } c4. bes16 d
    c4. bes16 d  c[ d c d]   c d c d
    c8[ a16 bes] c8 a f4
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Adagio"
  \time 4/4
  fis2. fis16 a fis es  
  es16. d32 d16. c32
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 3/4
  c4 f, f'
  f8 e4 c es8
  es d4 bes d8 d c4 bes
}
