\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez. [Tutti]"
  \time 2/2
    r8 cis16 d  e8 a
    cis, e a, cis
    r b16 cis d8 b' d, gis b, d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez. [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   R1*16
    c,4~ c16 e d e  \grace d8 c4~ c16 e d e
    c d e f  g c b a g4~ g16 b a b
    \grace a8 g4
}
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Adaio"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*4
      b'2.~ b~
      b8. cis32 b  ais16 b cis ais \grace g8 fis8. e16
      d4. r8
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Allegro ma non tanto [Tutti]"
  \time 3/8
  \partial 8
  e8 a8. gis32  e d cis b
  cis8~ cis32 b cis d cis b a gis
  a8. gis32 fis e d cis b
  a4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro ma non tanto [Solo]"
  \time 3/8
  \partial 8
  r8
   \set Score.skipBars = ##t
   R4.*23
    r4 g'8
    \grace f8 e4.~ 
    e8. f32 g f e d16
    \grace d8 c4.~
    c8. c32 d e f g16
    a b b32 a b16 c8
}