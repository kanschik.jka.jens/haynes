
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\major
  \tempo "1. Adagio"
  \time 4/4
  \partial 8 
   d8
   a'16.[ b64 g] fis16.[ g64 e] d8 d'
   \grace { cis64[ b a] } b8 a r d
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\minor
  \tempo "1. Adagio"
  \time 3/4
  e4.~ e32 d e f e8 c
  a8. gis16 a4 r8 e'
  f4 e8. a16 \grace e8 dis8. e16
  e2 r4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III"
    }
    \vspace #1.5
 }
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  a8
  d4~ d16. fis32 e16. d32 \grace d16 cis8 b16. a32 d8 a
  b16. g32 a16. d32 g,8.g16 g8 fis~ fis16 r32 d' fis16. gis32
}
 
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV"
    }
    \vspace #1.5
 }
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 8.
  d8 c
  \grace c8 b4 a8.[ b32 g] a8[ e' d c]
  \times 3/2 { b32[ a g] } g8.~ g4 r g'8~ g32[ fis e d]
 }
 
 
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\major
  \tempo "1. Adagio"
  \time 4/4
  e4~ e16[ gis]  fis16. gis64 e  fis 8 b, b cis16. dis32
  e16. dis32 e8 e8. e64 dis cis b \grace b8 cis8 b16. b'32 \grace b8 a8. gis16
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\major
  \tempo "1. Adagio"
  \time 4/4
    d8 cis16. d32 fis,16.[ g64 a] g16. a64 b a16. b64 gis a8 r d
}

