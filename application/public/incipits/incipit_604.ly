
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ire. Suite "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
  \tempo "1. Pastourelle. Gracieusement"
  \time 3/4
  \partial 4 
  g4 e8 d c b a g
 c b c d e f 
g4 f e d d  
 
}

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe. Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Musette"
  \time 2/2
\partial 2
 c8 d e d
d4 f e d 
c g c8 d e c
d4 d8 e16 f e4 d c2
 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIIe. Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. La Faconniere. Gracieusement"
  \time 3/4
\partial 4
e8 f
 g4 f e 
d2 d8 e
f4 e d 
c4. b8 c d e4 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IVe. Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Rondeau le delicieux"
  \time 6/8
\partial 4.
 e4 f8
 e8 d c b c d 
g,4. a8 b c 
b\grace a8 g c d e8. f16 d4. 
 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ve. Suite "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Sarabande"
  \time 3/4
g4 d4. g8
fis4. e8 d c
b c c4. b8 
a2 g4 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VIe. Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. L'Infortuné. Lentement"
  \time 3/4
\partial 4
 d4 g4. fis8 g4
d4. es8 d4
c d8 c bes a 
bes4 \grace a8 g4
 
 
}



