\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Gelobet sei der Herr, mein Gott, der ewig lebet [BWV 129/4]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key g\major
   \tempo "o. Bez."
   \time 6/8
   \partial 8
   d8 g a b b c d
   e4.~ e4 d8
   c b a b16 c d8 g,
   fis e fis
   
  
   
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key g\major
   \tempo "o. Bez."
   \time 6/8
   \partial 8
   \set Score.skipBars = ##t
    r8 R2.*23 r4 r8 r4 d8
    g a b b c d
    e4.~ e4 d8
    c b a b16 c d8 g, fis e fis d4 r8
    
}



