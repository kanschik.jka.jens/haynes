\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key f\major
    \time 6/8
    \tempo "1. o.Bez."
      f8 a, a  a f r
      f' a, a  a f f
      c' a f' e c r
}

 