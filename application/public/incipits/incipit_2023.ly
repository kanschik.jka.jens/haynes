 \version "2.16.1"
         #(set-global-staff-size 14)
         
     \relative c'' {
  \clef treble
  \key  bes \major
 \tempo "1. Adagio"
  \time 2/2
    bes4~ bes16. c32 d16. a32 
    bes4~ bes16. c32 d16. f,32
    g4
     }
