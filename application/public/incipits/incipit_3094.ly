\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Die Liebe führt [zieht] mit sanften Schritten [BWV 36/3, 36c/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key b\minor
   \tempo "o. Bez."
   \time 3/8
   \partial 8
   d8 cis d16 cis b ais
   b8 e8. fis32 g
   fis8 e16 d cis b
   ais8[ b32 cis] fis,8 
  
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key b\minor
   \tempo "o. Bez."
   \time 3/8
   \partial 8
   \set Score.skipBars = ##t
    r8 R1*15 r8 r8 d8
    cis d16 cis b ais 
    b8 e8. fis32 g
    fis8 e16 d cis b
    ais[ b32 cis] fis,8 r8
}



