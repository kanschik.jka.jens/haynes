\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble 
  \key bes \major
   \tempo "1. Allegro  B-Dur  4/4"
  \time 4/4
   \partial 4
  bes4 
  f'4. g16 a bes8 a g f
  f8. c16 c4~ c8. es16 d8. f16
  es8 g, g4~ g8. bes16 a8. c16 \grace c8 bes4
}
  

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Rondo  B-Dur  3/4"
  \time 3/4
  d2 \grace f8 es8 d16 es16
  f8. bes16 d,4 es
  f bes8 g f d
  c4. d16 es d4
}

