\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Zum reinen Wasser er mich weist [BWV 112/2]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key e\minor
   \tempo "o. Bez."
   \time 6/8
   \partial 8
   b8 g fis e b'4
   b,8 e16 fis g8 fis dis4 fis8
   g16 fis e fis g e dis e fis gis a b
   
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key e\minor
   \tempo "o. Bez."
   \time 6/8
   \partial 8
   \set Score.skipBars = ##t
    r8 R2.*11 r4 r4 r8 b8
    g fis e b'4 b,8
    e16 fis g8 fis8 dis4 r8
    
}



