\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4	
  \partial 4
  d4
  d'2 b8 d c a
  a g g2 d4
  c'2 b8 a d c
  c4. a8  b4
 
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Rondo. Andante"
  \time 2/2
  \partial 4
 d,4 b'2 c8 b a g
 fis4. g8 a4 d,
 c'2 d8 c b a
 g4. a8 b4
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
   \time 4/4
   \partial 4
   c4
   g'2. a8 g~ g2 g8 c g e
   d4 d d8 f e a
   g4. e8 g4 c,
}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio non tanto"
   \time 2/2
   c'4 g8. g16 es4 c g4. as8 g4 r
   c2 es4 d d8 c c2 r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro ma non presto"
   \time 3/8
   \partial 8
   g'8
   g c g g c g
   a a a fis g g
   e f f dis e e \grace a8 g f e d4 r8
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
   \time 2/2
   es,2 es8 bes es g
   f4. as8 d,4 r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro vivace"
   \time 4/4
   \partial 4
   bes'8. g16
   d es8. es2 g16 es8.
   a,16 bes8. bes2
}
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Rondo grazioso. Un poco allegretto"
   \time 2/4
   \partial 4
   bes'4
   bes g8 es
   \grace es8 f16 es f g es8 bes
   c d \grace f8 es8 d16 c
   \grace d8 c8 bes
}