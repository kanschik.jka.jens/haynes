\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [tutti]"
  \time 4/4
  \partial 8
    f8
    bes, g' f a, bes16 c d es f8 a,
    bes g' f a, bes16 c d es f8 bes16 a 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [solo]"
  \time 4/4
  \partial 8
    r8
   \set Score.skipBars = ##t
   R1*14 
   bes4~ bes16 c d es f8 f f g16 a
   bes4~ bes16 a g f g8 f es4 d4 r16
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [tutti]"
  \time 3/4
  g'8. fis16 g8 d16 c d8. g16
  a8 g a4 r8 d,
  bes'8. a16 bes a g fis g8. a16
  fis8. e16 d4 r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [solo]"
  \time 3/4
  \set Score.skipBars = ##t
  R2.*3
  d2.~ d~ 
  d8 c16 bes c8 es16 d c8 bes16 a
  fis'4. fis8 g8. d16
  c8. bes16 a2
  g4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 6/8
      bes8 c d c f16 es d c
      d8 c bes f'4.
      f8 bes f  g bes g
      f bes es, d4.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  a'8
  fis d e d16 cis d a b cis d e fis g
  a8 d, e d16 cis d a b cis d e fis g
  a d, e fis g a b cis d8[ a]
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Adagio [tutti]"
  \time 3/4
  fis4 fis e
  d4. e8 fis4
  e e d cis2 r4
}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Adagio [solo]"
  \time 3/4
     \set Score.skipBars = ##t
   R2.*3 
   r8 cis fis e16 d cis8 b
   ais b cis d e d16 cis
   d8 fis b a16 g fis8 e
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 6/8
  r8 r a'
  a g fis
  e d16 e fis g a8 g16 fis e d
  cis8 d16 e fis g a8 g fis
}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [tutti]"
  \time 4/4
  \partial 8
  f8
  f f, a c  f f, a c f g16 a bes8 a g[ c,] r16 c d c
  f c d c g' c, d c
  a'8 g r16
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [solo]"
  \time 4/4
  \partial 8
  r8
     \set Score.skipBars = ##t
   R1*7
  r2 r4 r8 f
  f f, a c  f4 g8. f32 g
  a8 g16 f bes8 a g c, r4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio"
  \time 4/4
  bes4 a8 g fis4 a~
  a g2 f4~
  f e dis4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 3/8
  f8 bes, a
  d c f
  bes, a f'
  e16 f g8 c,
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [tutti]"
  \time 4/4
  c8 e16 f g8 g c,16 d e f g8 g
  c,16 d e f g8 a16 b c8 g r a16 b
  c8 g r a16 b c8 b16 a g8 f
  e16 f g8 r16
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [tutti]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*1
  c8 e16 f g8 g c,16 d e f g8 g
  c,16 b c d  c d e f  d c d e  d e f g  
  e8 c  r4 r2
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio [tutti]"
  \time 4/4
  e8 e e e   e e e e
  e16 cis ais cis  e cis ais cis  fis cis ais fis'  e cis ais e'
  d8 d d d
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio [solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*4
   r2 e16 c a c  e c a e'
   a e cis a'  g e cis g'  fis d a d  fis d a fis'
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/8
  c8 e16 d c8 g' g, g'
  c,16 d e d c8 g' g, r
}

