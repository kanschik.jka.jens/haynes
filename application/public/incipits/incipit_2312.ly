\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  f4 e f r8 c
  d c16 d bes8. a16 a8 c f4~
  f8 e a4. g8 c4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  c8
  f e f e  f e f e
  f e16 d c8 bes a g f d'
  g[ fis]
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio"
  \time 3/4
  d4 d c
  b8 g c4 b
  c es8 d cis4 d~ d cis d8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 2/4
  f4 e 
  f8 c f a
  g c, g' c
  a4 f
  a8 f g a
}

