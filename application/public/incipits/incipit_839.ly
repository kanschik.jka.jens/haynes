 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 2/2
    r8 c16[ d]  c bes a g a8[ c] f16. g64 a g8
    e16[ g f e] d16.[ e64 f] e16. f64 g
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 2/4
  c8 c c c d d c c
  c bes16 a g g a bes
  a c f e f e f e f c f e f e f e f4 r8
}

\relative c'' {
  \clef bass
  \key d\minor	
   \tempo "3. Largo"
  \time 3/4
  r4 f,8. e16 d4~ d cis8. d16 e8. cis16
  d4 a f'4~
  f8. f16 e8. f16 g8. e16
  \grace d8 cis4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allegro"
  \time 3/8
    f8 f e16 f g8 f e
    f g16 f e d c8 d16 c bes c
    a bes c8 c
}