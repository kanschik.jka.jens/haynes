
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
 \partial 8
 g8 g4. f8 e4 r8 g8
 g4. f8 e4 r8 d8
 c4 c8. c16 d4 e8 g
 g f e4


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
 \partial 8
   \set Score.skipBars = ##t
   r8 R1*45 r2 r4 r8
 g'8 g4. f8 e4 r8 g8
 g4. f8 e4 r8 d8
 c4 c8. c16 d4 e8 g
 g f e4


}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Solo]"
  \time 2/4
   f4~ f8. e32 d
   c8 c4 f8
   \grace e16 d8 d \times 2/3 {d16[ f e]}  \grace g16 \times 2/3 {f16 e d}
  d8 c r16


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondeau [Solo]"
  \time 6/8
   \partial 4.
  e4 e8 g4 f8 d d d
  f4 e8 g, c bes
  a f' e d a' c,
 \grace c8 b8. a16 g8


}

