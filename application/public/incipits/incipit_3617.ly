\version "2.16.1"

#(set-global-staff-size 14)

  \relative c'' {
     \clef "bass"
     \tempo "1.Grave Andante [Basso]"
     \key g\major 
       g,8 g'16. fis32 g8 g, fis8 fis'16. e32 fis8 fis, 
 e8 e'16. d32 e8 e, d d'16. c32 d8 d, 
 g8 g' 16. fis32 g8 g, fis8 fis'16. e32 fis8 fis,
  }

\relative c'' {
   \clef "treble"   
                     \key g\major
                      \tempo "1.Grave Andante [Oboe]"
                      \time 4/4
\set Score.skipBars = ##t
                      % Voice 1
 R1*2 r4 d4 d2~ d8 c16. b32 c4~ c8 b16. c32 a8 d
 b g r8 d'8 e4~ e16 g fis e fis4~ fis16 a g fis g4 g16 b a g a8 b16 a g fis e d cis4 r4
                  }
                  
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allemante allegro"
  \time 2/2
 \partial 4
   r16 d16 c d b g b d g8 [d] b16 g b d g8 [d]
 b16 [g b d] g  b a g fis8 d r8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. ohne Satzbezeichnung"
  \time 12/8
    r8 r8 b8 g'4 g8 g8. fis16 e8 c'8. d16 c8
 c4 a,8 fis'4 fis8 fis8. e16 d8 b'8. c16 b8 
 b4 g,8 e'4 e8 e8. d16 c8 ais'4.~ ais8. g16 fis8 e8. d16 cis8 d4
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "4. ohne Satzbezeichnung"
  \time 6/8
    g4 d8 g4 a8 b8 a g a g fis 
 g4 d8 g4 a8 b a g a g fis g4 d8 d4 g8 e8 c d e d c d4.~ d4
}