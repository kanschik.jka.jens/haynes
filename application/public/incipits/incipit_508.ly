 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4  
      bes16 bes bes bes   bes bes bes bes
      d d d d   d d d d
      f f f f  f f f f 
      bes bes bes bes a[ a a]
}
