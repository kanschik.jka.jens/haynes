\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cassatio I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  g'2. f8. e16 d4 e2 f4
  \grace g8 f4 e r2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. o.Bez."
  \time 3/4
   e2~ e8. f16
   \grace e8 d2 e8 g \grace g8
   f4 e d c r r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cassatio II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
    f2. e4
    d c d e
    \grace g8 f8 e16 d c4 r c8 a'
    g2~ \times 4/6 { g8 f e d c bes}
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cassatio III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 3/4
    g'4 f8 e f g
    a b16 c g2
    g8 gis a e f g
    \grace f8 e4 d4. r8
}

  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cassatio IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 3/8
    a'4 b8
    \grace a8 g4 a8
    \grace g8 fis4 g8
    \grace fis8 e4 d8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cassatio V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8
    e8
    e4 a16 gis fis e
    e4 fis16 e d cis
    cis8. d16 b8 e16 cis
    a4 b cis r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cassatio VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 3/4
    a'4 g8 fis e d
    \grace d8 cis2 d4
    d' cis8 b a b
    a4 g fis
}
