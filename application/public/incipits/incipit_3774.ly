\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gracieusement"
  \time 3/4
g'2.
a8 g f e d c
\grace e8 d4  \grace c8 b4   c
\grace e8 d4  \grace c8 b4   c
d8 c  f e  a g f2 e4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allemande"
  \time 2/4
    \partial 16
    g'16
    g4 a8 g16 f
    g8 e d c
    f4. e16 f
    d c d e d e f g
}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Sarabande"
  \time 3/4
    es4 d f
    b,4. c8 d4
    g, f' es8 d
    es2 \grace d8 c4
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Premier Menuet"
  \time 3/4
    g'2 bes4
    a8 g f e d c
    f2 a4
    g8 f e d c b
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. La Burlesque"
  \time 2/4
    g'4 a
    g8 c, b c
    g'4 a
    g8 c, b c
}