 \version "2.16.1"
    #(set-global-staff-size 14)
    
\relative c'' {
  \clef treble  
  \key a\minor
    \time 2/2
    a4. b8 c b c d|
    e d e f e a16 gis a8 b|
    gis4. a8 b e,16 d c8 d |
    b4
  }