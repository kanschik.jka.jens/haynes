\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                    \set Staff.instrumentName = #"Oboe 1"
                     \key b\minor
                      \tempo "o.Bez."
                      \time 4/4
                      R1 R1
                      b'4 r8 b b a r a
                      a g r g g fis r fis
                      g e r4 b4. b8
                  }
\new Staff { \clef "treble" 
                    \set Staff.instrumentName = #"Oboe 2"
                     \key b\minor
                        % Voice 2
                      R1 R
                      g'4 r8 g g fis r fis fis e r e e4 r8 dis
                      e4 r r2
                  }
>>
}
