 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro moderato"
  \time 2/2
    es2 \times 2/3 { d8[ c b] } \times 2/3 { b8[ c d] }
    d2 c4 r
    g'2 \times 2/3 { f8[ es d] } \times 2/3 { d8[ es f] }
    
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Amore vole poco lento"
  \time 6/8
  bes8. c16 bes8  as g r
  g'8. as16 g8 \grace g8 f8 es r
  c4~ c16 as'32 f \grace es8 d4~ d16 bes'32 g
  \grace f16 es8 as g g f r
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Allegro assai"
  \time 2/4
  \partial 8
  g8
  es' es4 d8
  \grace d8 c4. g8
  f' f4 es8
  \grace es8 d4.
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Menuett"
  \time 3/4
  \partial 8
  
}