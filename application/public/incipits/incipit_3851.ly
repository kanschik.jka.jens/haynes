\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}





\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
  r8 f es d es4. d16 c
  d8 g16 f f8 es16 d c4. f16 es d8 c16 bes a8. bes16 bes8. c16
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 4/4
  bes4. d8 c f f c d4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 3/2
  r2 d~ d4 es8 d
  d2 a4 bes c bes
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 3/8
  f8. es16 d8 es8 c f
  bes, g'4
}


