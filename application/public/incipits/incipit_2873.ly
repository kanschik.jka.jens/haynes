 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

 

 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro [Tutti]"
  \time 4/4
    e8 c16 b a8 a e' c16 b a8 a
    b gis16 fis e8 e b' gis16 fis e8 e
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \set Score.skipBars = ##t
    R1*13
    r2 r4 r8 e,
    a16 c b a  b d c b  c b a gis  a b c d
    e a, b c  d e fis gis a8 e
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 4/4
   \override TupletBracket #'stencil = ##f
 
    c4 g'8. a16 \grace c,8 b4~ b8~ 
    \times 2/3 { b32[ d c]} \times 2/3 { b32[ a g]}
    f8 f' f~ f32 e f g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Tutti]"
  \time 4/4
       c4 bes a r8 d
       b g a b c d e fis
       g4 f e4. a8
       fis d e fis g g a b
       c4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*13
    r2 r4 r8 c16 b
    a g c b  a g c b a8 g r a'16 g 
    f e a g  f e a g f8 e r
   }


