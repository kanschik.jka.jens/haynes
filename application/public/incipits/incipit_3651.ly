\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 4/4
    es8 bes bes g'  g4 f
    f,8 d16 es  f es d c bes4 r
    
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. o.Bez."
  \time 4/4
    g'4~ g16 es d c f,8 as' b,, d'
    es16 b c8~ c16 g es c
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. o.Bez."
  \time 3/8
    es16 d es d es d
    es16 d es d es d
    es16 d es d es d
    es16 d es d es8
    
    
}

