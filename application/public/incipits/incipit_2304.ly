\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro assai"
  \time 4/4
    d8 d, d d   d d d d
    a'' a, a a   a a a a
    g' a, a a   g' a, a a 
    fis' d, d d   d d d d
}

