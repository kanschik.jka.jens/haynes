\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  g8 g, b d  g g, b d
  g16 a b c d8 g fis d, fis a 
  d16 e fis g  a8 d b g, b d  
 }


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Andante"
  \time 4/4
  e,8 g'16 e  b' g e g fis b fis dis b 'a g fis 
  g b, c a  a' cis, dis b b' dis, e fis, g e  b dis 
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 3/8
  r8 g16 a b c
  d8 d, d'
  g g, g'
  a fis16 g  a8
  b g d
}
  
