 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef tenor
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  d,8 g,16. fis32 g8 g' e g,16. fis32 g8 g'
  d8 g,16. fis32 g8 g'
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 2/4
  g4 g'~
  g8 fis16 e d8 c~
  c b16 c d8 g~ 
  g
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Largho"
  \time 3/4
  b4 e,16 g b e g8 e
  e4 dis fis8 b
  c,2. b
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Allegro"
  \time 3/8
  d16 b a g fis g
  e'8 d4
  fis8 g4
  \times 2/3 { c,16[ b a]} c8[ b]
 }