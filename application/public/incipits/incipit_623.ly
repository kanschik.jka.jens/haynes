\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Pantomime " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Presto"
  \time 4/4
  \partial 4
  r8 c
  c c c c \grace d8 c c c c'
  c, c c c \grace d8 c c c c'
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Seconde Pantomime  " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 6/8
  r4 r8 r4 g'8
  c8. d16 c8 c8. d16 c8
  b8. a16 g8 r4 
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisieme Pantomime " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Staccato"
  \time 6/8
  \partial 4.
  r16 c d e f32 g a b
  c8. d16 c8 b4 a8
  g4 g8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatrieme Pantomime " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Staccato"
  \time 2/4
  \partial 16
  g16
  c4 d8. c32 d
  e4. r16 f
  e8. d32 c g'8 g, c4.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquieme Pantomime" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 2/4
  \partial 4
  c16 d e8
  e e e e
  e4 e16 f g8
  g g g g g4
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixieme Pantomime " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro "
  \time 4/4
  \partial 4
  r16 d e fis
  g8 g, g'8. g16 g8 g, g'8. g16
  g8 g, g'8. g16 g8[ g,]
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Septieme Pantomime " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 4/4
  \partial 4
  r8 g
  c8. d32 e f8 d e8. d32 c d8 b
  c8. d32 e f8 d e8. d32 c d8 b
  c8.
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Huitieme Pantomime " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Mouvement de Chaconne"
  \time 3/4
  \partial 2
  r4 r8. \times 2/3 {g'32[ a b]}
  c4 c,8. e16 d8. f16
  e8. f16 g4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Airs " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Marche"
  \time 2/4
    e8 e e d
    e4 e8 e
    d e f e d4 r8
}
  

