 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro ma non troppo"
  \time 2/2
    bes4 r a r
    bes8 r c2 r4
    c4 r b r
    c8 r d2 r4
    es4. f8 g g g g
    g f f2 es4
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Adagio"
  \time 3/4
  bes2 as4 g r8 g g g
  g2 f4 es r8
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Menuetto"
  \time 3/4
  f,4 bes bes 
  \grace d8 c4 c2
  f8 e f g f es
  \grace cis8 d4 d2
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Andantino"
  \time 2/4
  \partial 8
  d16 es 
  f8 f g16 f es d
  f8. es16 c8 d
  bes bes es16 f g es
  d4 es8
}