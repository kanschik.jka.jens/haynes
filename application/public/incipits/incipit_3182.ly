\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Ich will auch mit gebrochnen Augen [BWV 125/2]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Traverso"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key b\minor
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                      r4 r8 r16 fis16[ \grace fis8 e8. d16]
                      \grace d8 cis4. r16 d16[ \grace d8 cis8. d16]
                      \grace d8 e4
                      
                     
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key b\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                      r4 r8 r16 d16[ cis8. b16]
                      \grace b8 ais4. r16 b16[ \grace b8 ais8. b16]
                      \grace b8 cis4
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key b\minor
   \tempo "o. Bez."
  \time 3/4
     \set Score.skipBars = ##t
    R2.*12 r4 r8 b8 \grace b8 ais8. b16
    \grace a4 g4. fis8 g8. e16
    ais8. b32 cis cis4 r8
   
    
    
    }