
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
  \tempo "1. Adagio assai"
  \time 4/4
  \partial 8
  g'8
  es b c16 g as' c, \grace d8 c8 b r16 g' as16. bes32
  \grace f8 e8 f16. g32 c,16 g' e bes \grace c8 bes8 as r16
}

\relative c'' {
  \clef treble
  \key c\minor
  \tempo "2. Allegro assai"
  \time 3/4
  \partial 4
  g4
  es16 d es g  c b c es  g es d c
  \grace c8 b4. g16 a b c d es
}
\relative c'' {
  \clef treble
  \key g\minor
  \tempo "3. non tanto adagio"
  \time 3/4
  g8. a16 bes8. c16 d8. g16
  a8 g16 fis \grace es8 d4. c8
  \times 2/3 {bes8[ d g]}  \grace d8 cis4. d8
}
\relative c'' {
  \clef treble
  \key c\minor
  \tempo "4. Presto"
  \time 12/8
  c8 b c  g f g  es g c  es d c
  g' f es   d es c b g16 a b c d8 b16 c d es
}