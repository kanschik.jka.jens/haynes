 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
    c,4. c8 e4 g
    c16 d e d c4 bes'4. bes8 
    bes a gis a e4.
}
