 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Andante"
  \time 6/8
  e8. f16 e8  e a r
  e8. f16 e8 e b' r
  d,8. e16 d8 d b' r
  c,8. d16 c8 c4 cis8
}


\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  a8 e'8. d32 e f8 gis,
  \grace gis8 a4. e'8
  e e4 e8
  gis a r e
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Menuetto"
  \time 3/4
  a'2. e
  c8 b16 a gis8[ a gis a]
  \grace gis8 f4 e2
}
