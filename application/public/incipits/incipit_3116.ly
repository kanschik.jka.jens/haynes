\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Und was der ewig guetig Gott [BWV 86/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key fis\minor
                      \tempo "Choral"
                      \time 6/8
                      % Voice 1
                      cis8 fis,16 eis fis gis a b a gis a b
                      cis8 d d d8 cis16 b cis d e8 e e
                      
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key fis\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                     r4 r8 cis8 fis,16 eis fis gis
                     a b a gis a b e,8 e16 d e fis
                     b,8 b'16 ais b cis
                  }
>>
}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key fis\minor
   \tempo "Choral [Melodie: Kommt her zu mir, spricht Gottes Sohn]"
  \time 6/8
     \set Score.skipBars = ##t
     R2.*8 r4 r8 fis4.
     fis fis cis' b cis a gis r4.

}