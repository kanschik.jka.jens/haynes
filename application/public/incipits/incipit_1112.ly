\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 2/2
   es,2~ es8 f16 g as bes c d  es8. es16 es8. es16
   es4 d
} 
