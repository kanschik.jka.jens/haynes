\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
   "Aria (Sopran): Ich will auf den Herren schaun [BWV 93/6]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key g\minor
   \tempo "o. Bez."
   \time 4/4
   r4 g16 fis g a bes a bes c d8 g
   g fis a16 fis e d c es d c a' es d c 
   bes a g fis
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key g\minor
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
     R1*8 r4 g8 as bes c d g
     g fis r4 r2
  
}



