\version "2.16.1"

#(set-global-staff-size 14)


      
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "Grave"
  \time 3/4
  r4 g es' d fis g
  cis, d8 c bes a
  g4 g'4. a8
  fis4 d r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "Aria 1. Allegro"
  \time 4/4
  \partial 4
  d8 c16 d es8 d c f d c16 bes c8 d16 es
  d8 c16 bes a bes c a bes8[ g]
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "Aria 2. Allegro"
  \time 4/4	
  \partial 2
  d4 g
  fis4. g8 a4 bes
  g4. f8 es d c bes
  a g16 fis g8 a
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "Aria 3. Tempo di Minuetto"
  \time 3/4
  d4 d d  d d d
  g bes8 a g4 g2.
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "Aria 4. Allegro"
  \time 2/4
  r8 g bes d
  c16 d c bes a bes c a bes8 g bes d
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "Aria 5. Tempo giusto"
  \time 4/4
  \partial 8
  g16 a bes8 bes bes bes   bes a16 g a bes c8
  d, c' c c c bes16 a bes c d8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "Aria 6. Allegro assai"
  \time 6/8
  d8 g fis g d r
  c d16 c bes a g8 a r
  bes4 a8 bes a r
}