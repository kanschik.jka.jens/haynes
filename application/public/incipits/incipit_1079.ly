 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
    \override TupletBracket #'stencil = ##f
    bes16 bes8. bes16 bes8. bes'16 a g f es d c bes
    bes bes8. bes16 bes8.
}
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Largo cantabile"
  \time 4/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
     R1 R1
     bes'4~ \times 2/3 {bes16[ f es]}   \times 2/3 {d c bes}
      g'4~ \times 2/3 {g16[ bes a]}   \times 2/3 {g f es}
      \grace d8 c f
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. o.Bez. [Tutti]"
  \time 3/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
            bes'8 bes,4 bes' bes,8
            f' f,4 f' f,8
            bes bes,4 bes' bes,8
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. o.Bez. [Oboe]"
  \time 3/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
   \set Score.skipBars = ##t
        R2.*3
        r4 r8 c16 d \grace { c16 d} es8 d16 c
        d4 r8 d16 es \grace { d16 es} f8 es16 d
        c4
}

