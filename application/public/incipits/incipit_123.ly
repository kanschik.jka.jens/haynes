\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. [Allegro]"
  \time 2/2
  \partial 8
  b8
  e16[ dis e dis]  e dis e dis    e8 e, g b
  g e g b e b e g
  fis[ b,]
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Adagio"
  \time 2/2
     \set Score.skipBars = ##t
   R1*3
   r2 g4 b16 c32 b a16 b32 g
   d'8 g g[ fis16 e] e8[ a] a fis16 a

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Allegro"
  \time 3/4
  r4 e,8 fis g a
  b a b c b4
  e8 dis e fis e4
  fis b,
}
