\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Ach, schlage doch bald, selge Stunde [BWV 95/5]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key d\major
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                      d8[ cis d fis] cis e
                      d[ cis d fis] cis e
                      d cis e d cis b
                      cis16 e g8 g4 r4
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key d\major
                     \set Score.skipBars = ##t
                        % Voice 2
                     a,8[ g a d] a cis
                     a[ g a d] a cis
                     a g b a g fis
                     e e' cis4~ cis16 e g8
                      
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key d\major
   \tempo "o. Bez."
  \time 3/4
     \set Score.skipBars = ##t
    R2.*15 r8 d8 e16 cis d8 fis a
    fis16 d e8 g4 r4
  
   

}