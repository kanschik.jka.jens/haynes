 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Affetuoso"
  \time 4/4
    r4 e8 fis  gis, a r16 d c16. b32
    c16. b32 a8 r16 c d16. e32 f16. d32 d16. c32 b16. d32 g16. fis32
    e16. d32 c8 r16
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro"
  \time 3/8
  r8 c d e e e
  e e e e a, b
  c d16 c b a
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Affetuoso"
  \time 3/4
  g'4 b,2 c4 d4. c16 d
  e8 f g4 a
  b,4. a8 g4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Allegro"
  \time 2/4
  a16 b c d e8 gis, a e r e'
  f16 g e f d e c d
  b4 r16
}


 
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Largo"
  \time 4/4
    r4 b'8 c  dis, e r16 a g16. fis32
    g16. fis32 e8 r16 b e16. d32 c16. e,32 a16. g32 fis16. a32 d16. c32
    b16. a32 g8 r16
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 2/4
   e,16 fis g a  b8 b g e r b'
c16 d b c  a b g a
fis e fis8 r16
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Affetuoso"
  \time 3/4
  d4 fis,2 g4 a4. g16 a
  b8. c16 d4 e
  fis,4. e8 d4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 3/8
 r8 g' a b b b
 b b b  b  e, fis
 g a16 g fis e b'8 e, fis g a16 g fis e dis4 r8
}