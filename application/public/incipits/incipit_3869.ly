 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
    d4. fis16 e e8 g~ g16 e fis g
    fis8[ d]
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Allegro"
  \time 2/2
  \partial 8
  a8
  d16[ fis e d] a' fis e fis   d[ fis e d] d' fis, e fis
  d[ fis e fis] d fis e d cis4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Ariosa"
  \time 3/8
  d16. e32 f8 e f8. g16 a8
  d, g bes
  \grace d,8 cis4 r8
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4. Presto"
  \time 6/8
  \partial 8
    a8 
    d cis d  d cis d
    d cis d  d cis d
    e d e e a cis, d4.
}