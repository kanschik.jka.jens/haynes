\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Mein gläubiges Herze, frohlokke [BWV 68/2]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key f\major
   \tempo "Presto"
   \time 4/4
    \set Score.skipBars = ##t
    R1*53 f8 c a' f g c, bes'4
    a,16 c f e f c a c
   
   
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Violine"
  \key f\major
   \tempo "Presto"
   \time 4/4
    \set Score.skipBars = ##t
   R1*52 f8 c a' f g c, bes'4
    a,16 c f e f c a c
   
   
}

\relative c {
  \clef bass
  \set Staff.instrumentName = #"Violoncello piccolo"
  \key f\major
   \tempo "Presto"
   \time 4/4
   f8 c a' f g c, bes'4
   a16 f c f a8 f g c, bes'4
   
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key f\major
   \tempo "Presto"
   \time 4/4
   \set Score.skipBars = ##t
      R1*3 r2 r4 r8 c8 c a16 bes c8 f e d c c
      c a16 bes c8 f e d c r8
}



