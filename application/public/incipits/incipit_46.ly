\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allemande"
  \time 4/4
  \partial 8
  g'8
  bes a g f g d r bes16 c
  d8 c bes a bes g r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "2. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. moderement"
  \time 4/4
  \partial 8
  a8
  d fis16 g a8 d, r4 r8 a
  d fis16 g a8 d, r a' b cis 
  d a b4.
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "3. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Gracieusement"
  \time 3/4
  \partial 4
  a4
  e'4. f8 d f
  e4 a, b
  c4. d8 b d
  c4 \grace b8 a4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "4. Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allemande"
  \time 4/4
  \partial 8
  d8
  g b a g fis4 a
  d,8 c16 b c8 d b g r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "5. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allemande"
  \time 4/4
  \partial 8
  g'8
  fis b, b e d4 r8 g
  fis b dis, e b4 r8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "6. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allemande"
  \time 4/4
  \partial 8
  r16 a16
  a4 r16 e' d e cis d e8 gis,8. a16
  a b cis d e8. d16 cis8 d16 e fis8. e16
  d8  \grace cis8[ b]
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "7. Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Gravement"
  \time 3/4
  \partial 4
  fis,4
  b2 cis4 d8 e fis4 ais,
  b4. d8 cis e
  d4 \grace cis8 b4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "8. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Lentement"
  \time 2/2
  f2 g4. a8
  g4. f8 e4. f16 d cis4. b8 a2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "9. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Doucement"
  \time 3/4
  a4 d cis d4. e8 fis4
  e fis g fis4. e8 d4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "10. Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Legerement"
  \time 4/4
  g'8 g g g  g fis16 e  d8 c
  b g16 a b c d b  e8 e16 d c8 b a4 d,
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "11. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allemande"
  \time 4/4
  \partial 8
  r16 e
  e8. g,16 fis g a b g8 e16 e' e8. dis16
  e8 b e fis g fis16 e fis8 fis
  fis4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "12. Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. moderement"
  \time 4/4
  \partial 8
  cis16 d
  e8 b cis16 b a b  gis8 \grace fis8 e r cis'16 d
  e8 e e d16 cis fis8 e16 fis d8. cis16 b4
}

