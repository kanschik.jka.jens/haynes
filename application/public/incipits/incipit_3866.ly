 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. Adagio"
  \time 2/2
  \partial 8
  c8 f4~ f16 des c bes   as8 g16 f c'8 f
  e8 des16 c r as' g f e8 f16 g c,4~
  c16[ as' g f]
}


\relative c'' {
  \clef treble
  \key f\minor	
   \tempo "2. Allegro"
  \time 2/4
  f,4 c'8 f e4 f
  bes,8 c16 des c8 bes as g16 as f8 c'
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Adagio"
  \time 2/2
  c4~ c16 es d c b4 b16 d c b
  c8[ g'16 d] d8 es16 f es8 c r
}

\relative c'' {
  \clef treble
  \key f\minor	
   \tempo "4. Allegro"
  \time 3/8
  f8 c4 as8 g4 f16[ e32 f] f8.[ e32 f]
 g4 c,8
}