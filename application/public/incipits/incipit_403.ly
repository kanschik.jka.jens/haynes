\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
      c4 c  c16 b a g  f e d c
      d'4 d d16 c b a  g f e d
      e'4 e e16 d c b a g f e
}
