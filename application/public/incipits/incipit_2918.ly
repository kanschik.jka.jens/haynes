 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
    a'8
    d,16 e f g a8 bes4 a16 g a8 a
    d,16 e f g a8 g4 f16 e f8 a
    d,16 e f g a8 bes es, d16 cis d8[ f,]
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Tutti]"
  \time 3/4
    f8 f, f f f f'
    f4 e8 d e4
    c8 c, c c c bes'
    bes4 a8 g a4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
     R2.*11
     r4 r g'
     a4. g8 a bes
     g4. c8 a e
     f a g4 f8 e16 f e8 d c4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro [Tutti]"
  \time 3/4
  d'16 d d d  f, f f f  e e e e
  d d d d  f, f f f e e e e
  d e f g  a b cis a  d e f g
  a b cis a d8 f, e4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*4 
    d16 e f g  a bes a g  f g f e
    d e d cis d8 c16 bes a bes a g
    f g f e  d8 e16 f g a b cis
    d e f g a8 a, d4
}
