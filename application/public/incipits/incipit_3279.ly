 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "Nr. 1."
  \time 2/4
  \partial 8
    es,8
    \times 2/3 { g,8[ bes es ] } \times 2/3 { g8[ f es ] }
    \times 2/3 { g,8[ bes es ] } \times 2/3 { g8[ f es ] }
    \times 2/3 { f8[ as g ] } \times 2/3 { f8[ g es ] }
}

 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "Nr. 2"
  \time 2/4
  \partial 8
    g'16 f
    e8 g d f e g d f
    e16 g c b a g f e 
    d8 g, g
}
 
\relative c'' {
  \clef treble
  \key e\major
   \tempo "Nr. 3"
  \time 2/4
  \partial 8
    b8
    e, e e gis fis fis fis b
    fis fis fis a gis gis gis
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "Nr. 4"
  \time 2/4
  \partial 8
    a8
    e'8. fis16 e8. a16
    a4 gis8. fis16
    e8. d16 cis8. d16
    d4 cis8
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "Nr. 5"
  \time 2/4
  \partial 8
    a'16 d
    a8 a a fis
    d d d e16 fis g8 g fis fis e a, a
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "Nr. 6"
  \time 3/8
    cis8 \grace d16 cis16 b cis d
    e8 cis a b4 a8
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Nr. 7"
  \time 2/4
  \partial 8
    f8
    f d bes' g g4 f8 d c d es es
    es4 d8
}
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "Nr. 8"
  \time 2/4
    bes8 g as f
    es4 es
    \grace g8 f8 es f g as4 as
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "Nr. 9"
  \time 2/4
  \partial 8
    g'8
    e e e16 f g e
    c8 c c e
    d8 d d c16 d
    e8 c c
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "Nr. 10"
  \time 2/4
  \partial 8
    a8
    \times 2/3 { d8[ e d]} \times 2/3 { d8[ e fis]}
    g4 fis
    \times 2/3 { e8[ fis e]} \times 2/3 { d8[ e d]}
    \times 2/3 { cis8[ e cis]} a8
        
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "Nr. 11"
  \time 3/8
  \partial 8
    a'8
    gis16 a b a g e
    fis d cis d cis d
    g a g e fis d
    e cis b a
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "Nr. 12"
  \time 2/4
  \partial 8
    a8
    fis'16 g fis e d8 a
    \grace cis16 b8 a b cis
    d16 e fis g a8 b
    e,4 e
}
