 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 2/4
  \partial 8
  g8
  e' e e e
  e~ e32 f e d e8~ e32 f e d
  e4~ e16. g32 d16. g32
  c,8. b32 a g8 a
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  g'8
  c,8 c~ \times 2/3 { c16[ e d] } \times 2/3 { d16[ f e] }
  \times 2/3 { e16[ a g] } g8 r32 g[ fis g] a g fis g
  g16 f f4 e8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Allegro"
  \time 3/4
  g'4.  a8 b c
  \grace a8 g4 f e
  \times 2/3 { d8[ f e] }
  \grace g16 \times 2/3 { f8[ e d] }
  \grace e16 \times 2/3 { d8[ c b] }
  \grace d8 c4 r8
}
