 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\minor
   \tempo "1. Larghetto"
  \time 6/8
    f8. ges16 f8 des'8 c bes
    bes4. a8. es'16 des c
    c8 bes des des c \times 2/3 { c16[ des es] }
    \grace es4 des4. c4 r8
}


\relative c'' {
  \clef treble
  \key bes\minor	
   \tempo "2. Allegretto"
  \time 2/2
  \partial 4
  bes4
  f' \grace ges16 f es ges f
  f8 bes, a c
  c bes bes4~ bes8. bes16 bes'8. bes16
  bes16[ c, c es] es ges ges es
  des[ c bes' a] g f c' es,
  des8. es16 f4
}

\relative c'' {
  \clef treble
  \key bes\minor	
   \tempo "3. Vivace"
  \time 3/4
  f2 \times 2/3 { es8[ f ges] }
  ges2 f4
  \times 2/3 { ges8[ f es] } es4. des16 c
  c2 des4
}
