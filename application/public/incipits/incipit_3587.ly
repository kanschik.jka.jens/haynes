 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegretto"
  \time 2/2
  f2. a8 f
  c' r a r   f r c r
  \grace cis4 d2. f8 f
  bes2 bes8 g e bes
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Andante"
  \time 2/4
  r16 d es f  r bes, c d
  r c d es r a, bes c
  r d es f g g a bes
  bes a a g  g[ f]
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Presto"
  \time 3/8
  f,4 \grace a16 g8
  f g a bes c d
  c r r
  a'4 bes16 c bes8 r r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}