\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
    d8
    g d32 c bes a
    g8 g'
    fis d r16 d d8
    es c a16 es' es8
    d bes
}
