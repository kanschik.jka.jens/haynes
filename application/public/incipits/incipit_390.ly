\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
     f4 c f8 e16 d  c bes a g
     f4 r8 c' d bes16 a  g a bes c
     a8 f a c  f16 a f a f a g f
     g f e f d8. c16 c4
}
