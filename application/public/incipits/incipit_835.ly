\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 4/4
  f,2 f8 bes16 c a8 g16 a
  f2 f8 bes16 c a8 g16 a f8 d'16 es c8 bes16 c a8 f d' c 
  b4 r8
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  c8
  f f f e16 f
  g8 c, c16 d e f
  g8 g g f16 g
  a8 c, c16 e f g
}


\relative c' {
  \clef tenor
  \key f\major
     \tempo "3. Largo"
  \time 3/4
  r8 r16 f e8. f16 e8. f16
  cis4 \grace { d16[ e f] } g8. g16 f8. e16
  d8. es16 d8. c16 bes8. a16
}



\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 3/4
  c8 a16 bes c8 d16 e f4
  e2 d4
  c8 a16 bes c8 d16 e f4
  c2 bes4
}