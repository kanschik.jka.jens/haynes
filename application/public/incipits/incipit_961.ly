\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 4
  bes4
  bes8. g16 es4~ es8. g16 f8. as16
}
