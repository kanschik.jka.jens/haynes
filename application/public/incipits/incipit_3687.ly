\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
<< {
      % Voice "1"
      r1
  r4 c'16 bes a a  a g f e  d c bes a 
  g4 r r2
  r4 bes'16 a g g  g f e d c bes a g
  f4 r r2
         } \\ {
      % Voice "2"
  c'2. \grace e16 d8. c16
  c4 r r2
  d2. e8. d16
  d4
      } >>

} 

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. o.Bez. [Oboe]"
  \time 4/4 
  \set Score.skipBars = ##t
   R1*8 
   r2 a'4. g8
   \grace g8 f4. e8
   d e16 f e8 d
   b c c4 r2
} 
