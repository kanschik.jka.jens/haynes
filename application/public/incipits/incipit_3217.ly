\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Ich habe meine Zuversicht [BWV 188/1]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key f\major
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
   a8 g16 a bes8 a g f
   bes16 a bes d c2
   a8 g16 a bes8 a g f f'16 e d f
   e2
  
 
 
                                                                                   

}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key f\major
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
R2.*14 a8 bes16 a bes8 a g f
bes16 a bes d c2
  
       
}



