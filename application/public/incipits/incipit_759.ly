 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez. [Tutti]"
  \time 4/4
  <g, d' b' g'>8[ g''] r16 a g16. fis32
  <g,, d' b' g'>8[ g''] r16 a g16. fis32
  g8 d a' c, b16 a g8 r4
}

 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez. [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*26 
   r4 r8 g8 d'4~ d16. g32 fis16. e32 d4~ d16. e32 d16. c32 b8 fis g e'
   e d r
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. Adagio"
  \time 4/4
  \partial 8
  b16 g e8 e'4 g8 g32 fis e16 fis8 r b,16 fis dis8 fis'4 a8 a32 g fis16 g8 r
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Allegro Assai"
  \time 3/8
  g16 b8 d g16
  e8 fis, g
  d16 fis8 a c16
  b8 a r
}
