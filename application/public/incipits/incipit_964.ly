 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro moderato"
  \time 4/4
  f,4. g16 a g4 r
  g bes a r
  f8 d' d2 e4
  f8 c \grace e16 d8 c c2
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 4/4
  g8 g4 c8 \grace c16 b a32 g g4 \times 2/3 { c16[ d e]}
  a,8 \times 2/3 { d16[ e f] } c8 b~ \times 2/3 { b16[ d c] } c8 r16
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 6/8
  c8. d16 c8  c a bes
  c f a, bes8. c32 d c8
  c8. d16 c8 c a bes
  c f a, g8. a32 bes a8
}

