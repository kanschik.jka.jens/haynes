\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Domine Fili unigenite [BWV 235/4]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key bes\major
   \tempo "o. Bez."
   \time 3/8
   f8 bes d~
   ~d c es,~ es8 d r8
 
   
  
   
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key bes\major
   \tempo "o. Bez."
   \time 3/8
   \set Score.skipBars = ##t
   R4.*17 r8 r16 f16 g a
   bes a bes c bes c
   d8 c es
   d c bes
   a bes g
   a16 bes bes8. a32 bes
   c4 r8
    
}



