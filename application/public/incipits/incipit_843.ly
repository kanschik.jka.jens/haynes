\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Poco allegro"
      \set Score.skipBars = ##t
  \time 2/4
  \partial 8
  a'8
  d,4 r
  r r8 bes'8
  a g f e 
  d4 r8 bes'
  a g f e
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Largo"
  \time 4/4
  r8 f, c' c c g c c
  c f, bes bes bes a g4 f8 a d4
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro"
  \time 3/8
  a4.
  b4.
  c8 d16 c b a
  d8 b e
  c a b
  cis d16 cis b a
  d4. g,4.
  
}
