\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. [Allegro]"
  \time 2/2
 r4 r8 g'  g g g g
 g16[ f e d] c8 g  c[ d]  e16 g f e
 d4r8 g16 f e8 d
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Duetto"
  \time 3/2
  r2 es,2. es4
   as2 as bes
   c1 r2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Air. Allegro"
  \time 2/2
  c16[ d b d] c e d f e8 d r4
  c16[ d b d] c e d f e8 g r4
  }


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Stringilo"
  \time 2/2
  r2 c8 d16 e d8[ g]
  e d16 e c4 f8 g16 f e8[ f]
  d4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Quanto peno"
  \time 2/2

}
