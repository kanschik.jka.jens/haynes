 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "o.Bez."
  \time 4/4
  g4 g a b  
  c c b8 c16 d   e d c b a8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "Allegro"
  \time 3/4
  e,2 r4
  r8 g g g g g
  g a16 b c8 g g g 
  g16 f g a g8[ f]
}
