 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. Allegro"
  \time 4/4
  c2 bes4 des
  e, c'4. bes8 as g as g f4
}


\relative c'' {
  \clef treble
  \key f\minor	
   \tempo "2. Largo e piano"
  \time 6/8
  \partial 8
  c8 f,4 f'8 es8. f16 des8
  c2.~ c2.
}

\relative c'' {
  \clef treble
  \key f\minor	
   \tempo "3. Vivace"
  \time 3/8
  c16 bes c des c8
  c des c
  c16 bes c des c8
  as bes g
  as c r
}
