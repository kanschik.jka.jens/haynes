\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 2/2
     d4 fis16 d fis d e4. g16 e
     \grace d8 cis4. d16 e d4. e8
     fis4. e16 d
     cis8 b a b
     a4 g fis
 
} 
