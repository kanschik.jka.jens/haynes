\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
  f4~ f8 g64 f es d c bes c a  bes16 a bes8 r bes
  c
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Vivace"
  \time 2/2
  f4. es8 d c d bes
  c f, f'2 es4~ es d8 es16 d c8[ d16 es] f es d c
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 4/4
  d8.[ d64 es d c bes a g fis g a bes c d]
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 2/2
  \partial 4
  f,4 bes f f bes
  c f, f c'
  d f, f d' c2.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4
  g'2~ g4~ g16 as32 g f g g f64 g
  as8 \grace g8 f \grace es8 d g es c r 
}



\relative c'' {
  \clef "bass" 
    \key c\minor
     \tempo "2. Allegro [Bass]"
    % Voice 2
    r8 c,, es c d es16 f g8 g
    c, c' des bes g e c c'
    as f as f b d16 c b[ a g f]
    es8 c c' es fis, g c, d
  }
  
\relative c'' {
  \clef "treble"   
  \key c\minor
   \tempo "2. Allegro [Solo]"
   \time 2/2
      \set Score.skipBars = ##t
   R1*4
   r8 g bes g a bes16 c d8 d
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Poco Largo"
  \time 3/4
  bes4 es,4. es'8
  d2 es4
  c8 d bes es d es
  f,2 es4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Allegro"
  \time 3/4
  c4 d b
  c b8 a g4
  es' f d
  es d8 es c4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. Adagio"
  \time 4/4
  c8 as16. g32 \times 2/5 {f32[ g as bes c  d e f g as] }
  e4 f8.[ e32 f]
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "2. Vivace"
  \time 2/2
  f,2 g
  as4. bes8 c bes as g
  f4 f'2 es4~
  es des2 c4~ c
}
  
  \relative c'' {
  \clef treble
  \key f\minor
   \tempo "3. Largo"
  \time 3/4
  f4. g8 e4
  f c2
  as'4 f e8 f e2.
}
  
  \relative c'' {
  \clef treble
  \key f\minor
   \tempo "4. Presto"
  \time 12/8
  \partial 8
  c8
  f4 f8 f8. e16 f8 g4. c,4 c8
  g'4 g8 g8. f16 g8 as4. \grace g8 f4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
  g4~ g8 a16 bes a4~ a8 bes16 c
  bes8 g d' c16 bes a8[ d]
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Vivace"
  \time 2/2
  r8 d bes g  es' g16 f  es[ d c bes]
  a8[ c] a f  d'[ f16 es] d c bes a
  g8 bes g es
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Andante"
  \time 3/2
  es,4 f g as bes2
  es, es' d
  g f es d1 c2
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 2/2
  g4 a bes c d g, d' g
  fis g a fis
  g2.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 4/4
  bes4~ bes16[ bes32 c d c d d c d]  es4. bes8
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 2/2
  r8 es d c  bes as g f
  es4 f g a
  bes2 c
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Adagio"
  \time 3/2
  g'2 es d8 c b c
  c2 r r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Presto"
  \time 12/8
  \partial 8
  bes8 
  es d c  bes c as  g4 f8 es4 bes'8
  c as bes c d es  bes as g r4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  r8 c f4~ f8 e16 d c8[ d64 c bes a bes]
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Vivace"
  \time 2/2
  r4 c d c8 bes
  c f, f'2 e4 f r r f~
  f e d2
  c4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio"
  \time 3/2
  r2 a d~
  d cis e~
  e d f~
  f e g
  cis, a
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 12/8
  c4 a8 f4 d'8 c4 a8 f4 c'8
  d4 bes8 g4 d'8 c4 a8 f4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
  f4~ f16 g32 f es d c bes  bes4 r8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Vivace"
  \time 2/2
  r8 f f f  d g g g
  c, f f f  bes, c16 d es[ d c bes]
  a8 d d d  g, c c c 
  a[ f]
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 3/2
    d1 d32[ c bes a g a bes c d c d]
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Presto"
  \time 12/8
  \partial 8
  f8
  d c bes f bes a bes4. r8 r f
  f' g f f es d c4. r8 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Largo"
  \time 4/4
  bes4. c32[ d es f g]  d4.
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 2/2
  \partial 4
  es8 f
  g as g f es4 bes
  es2 r4 es8 f g as g f es4 bes es2 r4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Adagio"
  \time 3/2
  g2 c1
  g2 d'2.
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Allegro"
  \time 3/4
  bes4 g es
  bes' es2 d4 g2
  f4 d bes
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 4/4
  f8 es16 d bes8 g'16 f f8 d16 es f8 g16 d
  es d c d es32 f g16 f es d8 f bes,4~
  bes8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Presto"
  \time 12/8
  \partial 8
  f,8 bes4 f8 d' c bes c4 f,8 es' d c
  d c bes f' es d c4. r4
}


  \relative c'' {
<<
\new Staff { \clef "treble"   
                     \key g\minor
                      \tempo "3. Adagio"
                      \time 3/2
                      d1.~ d~ d~ d4 es8 d es4 es8 d c bes a g fis2
                      % Voice 1
                  }
\new Staff { \clef "bass" 
                     \key g\minor
                     r4 g,8 fis g4 g g, g'
                     a, a'8 g a4 a a, a'
                     bes, bes'8 a bes4 bes bes, bes'
                     c, c'8 bes c4 c c, c'
                     d,
                        % Voice 2
                  }
>>
}
  
  \relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 2/2
  \partial 2
  f,4 bes
  a d c bes
  a f'~ f8 d es4~
  es8 f d es c4. bes8 bes2
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Largo"
  \time 4/4
  es8 d16 c bes8. as16 g8 es r bes'
  c c f8. es16 d8 bes r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 12/8
  bes8. c16 bes8 bes c d  es4. es,4 es'8
  f d bes as bes f g f es r r
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Largo"
  \time 3/4
  c8 b c4 g
  d'8 c d4 g,
  f' es d es c8 b c4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Allegro"
  \time 3/4
  es8 c g es es' g
  f bes, bes2
  f'8 d bes d f as
  g bes, es2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 3/4
  d4 es d
  c4. bes8 a8. g32 a
  bes4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Vivace"
  \time 2/2
  \partial 4
  d4
  g f8 es d c bes a
  g a bes c  d e fis d
  g fis g a  g bes a g fis2.
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 6/8
  d4. g
  fis r8 r a
  d, c bes a bes c
  bes a g
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 3/4
  g'4 c, \grace es8 d4
 es4. d8 c4  
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Vivace"
  \time 2/2
  \partial 4
  g4 c d es f
  g c, as' g
  f es d c b2.
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Largo"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Andante"
  \time 3/8

}


