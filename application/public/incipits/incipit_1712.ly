\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 16
  es,16
  es2. r8. g16
  g2. r8. bes16
  bes2. c8 d
  es4 g,2 a4
  c2 bes4 r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 4/4
  r2 es8 d c bes
  bes4 bes bes bes  bes2. r4
  r2 f'8 es d c
  bes4 bes bes bes bes2. r4
}

