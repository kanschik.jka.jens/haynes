\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
      c'4. e,8 f d r bes'
      e, c r a' d,4 g8 f16 e
      f4 g8 bes a16 g a bes g8. f16
      f8 a g f
} 
