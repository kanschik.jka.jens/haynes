\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 6/8
     c4 c'8 b c a
     g a f e f d
     c4 e8 d e c
     b c a g a f
     g, g'' g
     d, g' g
     c,, g'' g  b,, g'' g
     a,, g'' g
     g,, g''g
}
