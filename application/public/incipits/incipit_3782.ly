 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 3/4
  r8 f, g a bes c16 bes
  a8 f f4 r R2.
  r8 f g a bes c16 bes a8 f f4 r
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Andante"
  \time 2/2
  d4. e8 cis4 d
  e8[ e] e16 f d e f8 d r f
  e16 d e cis d8[ a'] a4
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Vivace"
  \time 3/8
  c8 f16 g a8
  c,8 g'16 a bes8
  c, a'16 bes c8
  g4 a8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}