\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. "
  \time 2/4
  \override TupletBracket #'stencil = ##f
  d4 \times 2/3 { d16 e fis  g a b}
  \grace a16 g8 fis r16 g b, cis
  d8 a \times 2/3 { d16 e fis  g a b}
  \grace a16 g8[ fis]
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 4/4
  \partial 8

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. "
  \time 2/4
  \override TupletBracket #'stencil = ##f
  d4 g16 d c b
  \grace b8 c4 c16 a g fis
  \times 2/3 { g16 b a} g16 b \times 2/3 {a16 c b} a16 c 
  b8 d e fis g
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. "
  \time 2/4
    fis16 d cis b ais b b' fis
    \grace a16 g8 fis4 e8
    \grace e16 d8 cis16 b
    ais b b' fis
    \grace a16 g8 fis4 e8
}

  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
    c8 g32 c e g \grace g16 f8 d16 b
    c8 g32 c e g \grace g16 f8 d16 b
    c c' c, c  \grace { cis16 d e} d4
    e16 c' e, e \grace { e16 f g } f4
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. "
  \time 2/4
  \grace fis16 e16 dis e8 \grace fis16 e16 dis e8 
  e8 a16 fis g e fis dis
  \grace fis16 e16 dis e8 \grace fis16 e16 dis e8 
  e8 a16 fis g e fis dis
  e8 e,
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. "
  \time 6/8
      g'8 b, b b4 a8
      a' c, c c4 b8
      g' d d  e c c
      b16 a c' a g fis  \grace fis8 g4 r8
}
