\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
  r8 d g4. a16 bes a8. g16
  fis8 d a'4. g16 a bes4
  bes8 a16 bes c4 c16 d bes c  a bes g a
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 2/2
  r4 g8 a   bes c a bes  g bes a bes
  c bes a bes  c d bes c  a c bes c
  d4 bes8[ c]
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 4/4
  bes2 f' f8 g16 f  es d c bes a c f es d c bes a
  g4 es'4. d16 c d es f8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 3/8
  d g fis g a16 g fis8
  g bes a bes c16 bes a8
}

