
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Andantino"
  \time 4/4
  \partial 8
 r8 r4 r8 b8 b cis16. dis32 \grace fis16 e8. dis16 
 \grace dis16 cis8 b r8 r16 b16 b8 fis16. b32 b8 gis16. b32 
 \grace b16 a8 gis r8
 
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Allegro assai"
  \time 4/4
  e4 gis e r16 dis16 cis b
 e4 gis e r16 dis16 cis b
 cis4 cis8. cis16 a'4 gis16 fis e dis e4 gis e
 
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Andante cantabile"
  \time 2/4
  \partial 8
 e8 \grace b32 a16 gis32 fis e8[ e e]
 e2~ e~ e~ e e4 r8 e8 f16 fis gis a ais b cis d 
 \grace d32 \times 2/3 {cis16 b a} a8 r8
 
}
\relative c' {
  \clef treble
  \key e\major
   \tempo "3. Minuetto"
  \time 3/4
   e4 gis b
 b4. cis8 b4 b8 e e dis dis cis 
 \grace cis8 b4. a8 gis4
 
}