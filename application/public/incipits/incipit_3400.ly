\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. ?"
  \time 4/4
      c4 r8 g'  e c g' g,
      c16 b c d  c8 c d16 c d e  d8 d
}

