
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonatille Premiere. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Lentement"
  \time 3/4
\partial 4
g4 c4. d8 e4
d4. e8 f4
e8 d c d e f 
g4 c2~ c8 bes a g f e 
f4. g8 a4 g
 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Gavotte en Rondeau. Moderement"
  \time 2/4
\partial 4
g8 c
c16 b a g bes a g f 
e8 d e16 f g a 
g f e d f e d c
b8 \grace a8 g8
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondeau. Gracieusement"
  \time 3/8
c8 c g
c8. d16 e f
g8 f e 
d4 c8
e16 d e f e f
d c d e d e
c d d8. c16 b8. a16 g8
 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "4. Legerement"
  \time 2/4
\partial 8
g8 e d16 e f e d c
d c d e d8 g
f16 e d c e d c b
c b c d c8 e
e16 f g4 d8 e4


 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonatille Deuxieme. "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Rondeau. Tendrement"
  \time 3/4
g4. f8 g4
e4. f8 e d 
c d d4. c8
b4. a8 g4
c4. d8 e4
d \grace c8 b4
 
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gayement"
  \time 2/4
c4 d
e16 f e d c d e f 
g8 g g g 
e c4 d8
e4 f
g16 f e d f e d c
g' f e d f e d c 
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Musette. Tendrement"
  \time 2/2
\partial 2
c8 b c d 
g,4 c d e 
d c d8 e f d
e4 d8 c b c d b
c2
 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "4. Legerement"
  \time 3/8
c8 b16 a g f
e8 d e 
c16 b c d e f 
g8 f16 e d c 
b8 a b
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonatille Troisieme. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Tendrement"
  \time 3/4
\partial 4
c4 g'4. as8 g f
es4 \grace d8 c4 es
d8 es f es d c
b\grace a8 g4

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "2. Allemande "
  \time 2/4
\partial 16
c16 c8. g16 as g f es
f es d f g f es d 
es d c es f es d c
b8 \grace a8 g8

}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "3. Rondeau"
  \time 3/8
g4 as8
g f es
d f16 es d c
b4 \grace a8 g8
c d es
d16 c d es f d
g8 es8. d16 d4.

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Gigue "
  \time 6/8
\partial 8
c8 g' f es es f es 
as4. f
es8 d es es d es 
g4. d
c8 b c d c d 
es d es f es f


}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonatille Quatrieme. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Moderement"
  \time 2/4
\partial 4.
c8[ c c] 
g'[ a b c]
f,[ as g f]
es[ d es f]
g[ c, c c] 
g'[ a b c]
f,[ es d a]
b[ c d es]
 d 

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Rondeau"
  \time 3/8
c8 g' g
g f16 es d es
f8 f f 
f es16 d c d 
es8 es es 
es d16 c b c
d8 es f 
es4 d8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Iere. Ariette"
  \time 6/8
\partial 8
g8 c4 d8 es4 f8
g4. c
g8 as g f4 g8
es4 d8 c4 g8
c4 d8 es4 f8
g4. c4.


}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "4. Rondeau. Gayment"
  \time 2/4
c4 b
c4. bes8
as[ g f es]
d4 c
es8[ f g as]
g[ f es d ]
c4 d
b \grace a8 g4

}





\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonatille Cinquieme. "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Gracieusement. sens lenteur"
  \time 3/8
\partial 8
g8 fis4 g8
d4 e8
d c b 
a g a 
b16 c c8. b32 c
d8. e16 fis8
g e a
fis[ \grace e8 d8]
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Moderement"
  \time 2/4
g4. b16 c
d c b a g a b c
d e fis g e8 fis
g d4 

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Rondeau"
  \time 6/8
\partial 2
b8 b c d 
d4 g,8 c4 b8
a4 b8 b c d 
e fis g fis4 g8
a4
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Legerement"
  \time 2/4
\partial 8
d8 d16 c b a g8 d'
e d4 d8
e16 fis g8 e a
fis8. e16 d8 a'
b e,4 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonatille Sixieme. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Lentement"
  \time 3/4
g4 bes d
bes4. a8 g4
d' g4. a8
fis4. e8 d4
g8 a a4. g16 a
bes4. a8 g4
 
 
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "2. Allemande"
  \time 2/4
\partial 16
g16 g4 r16 d16 c d
bes8 a16 bes g a bes c
d8 d d g
fis8. e16 d8 g16 a
bes a g a bes a bes g
 
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "3. Ariette. Gracieusement"
  \time 3/8
g8 a bes
a4 g8
fis4 g8
d4.
c8 d es
d c bes
a bes d
bes8. a16 g8
 
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "4. Gayement"
  \time 2/4
g4. f8
es[ d c bes]
es4. d8
c[ bes a g]
d'4 g
fis \grace e8 d4
 
 
}


