\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro assai"
  \time 4/4
     c2~ c8 e d c
     c b b b b4 r
     d2~ d8 f e d
}
