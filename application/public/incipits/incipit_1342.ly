 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
      c4 g e c
      g'' d b g
      e16 c d e  f g a b c4 r
}

 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 3/4
    g2. g
    g16 c, d e   f g a b c4~
    c16 e, f g  a b c d e4	
}

 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto"
  \time 4/4
    c4 c c r8 e
    d c b a g4 f
    e16 c d e  f g a b c4. e8
}

