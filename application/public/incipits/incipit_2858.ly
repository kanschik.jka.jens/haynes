\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro non molto [Tutti]"
  \time 4/4
  c1~ c2~ c4~ c16 b32 a g f e d
  c4 r r2
  g''1~ g2~ g4~ g16 fis32 e d c b a 
  g4 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro non molto [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*17 
  c1~ c2~ c4~ c16 b32 a g f e d
  c4 r r2
  d'1~ d2~ d4~ d16 c b a g f e d4 r
  
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Larghetto [Tutti]"
  \time 4/4
  g'4 r8 g fis4 r8 fis
  e4 r8 e dis4 r
  g,4 r8 g fis4 r8 fis
  e4 r8 e8 dis4 r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Larghetto [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*8 
   r4 r8 b e16 g8 e16 e b'8 g8
   fis16 a8 b,16 b a'8 fis16 g b8 e,16 e b'8 e,16
   dis8 b r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Minuetto"
  \time 3/8
  e16 f g8 g
  g f e
  d16 e f8 f f e d
}