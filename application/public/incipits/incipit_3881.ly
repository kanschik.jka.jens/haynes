\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro Maestoso"
  \time 2/2
  d2 d4~ d8. d16
  \grace e8 d4. cis16 d a'8 a, b cis
  \grace e8 d4. cis16 d a'8 a, b cis
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Cantabile"
  \time 4/4
  \partial 4
  g4
  d'4. c32 b a g g'4 fis8 e
  \grace fis8 e4 d r8 \grace a8 g32 fis g a
  b8 g d b
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Tempo di Menuetto [Tutti]"
  \time 3/4
  a'4. b8 cis d
  \grace a8 g2 fis4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Tempo di Menuetto [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*12
   d4 a'16 b a g fis g a, g'
   \grace g8 fis8 e16 d a' b a g fis g a, g'
}
