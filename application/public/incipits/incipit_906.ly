\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/4
    f8 a r c,
    g' bes r c,
    a' f g e f
}
