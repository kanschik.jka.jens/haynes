\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 2/2
    \partial 4
    e,4
    e e e e
    d2 e f2. e4
    a f e2
    d8 d d e d g, a b
    c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   \partial 4
   r4
   R1*83 
  g'1~ g~
  g2. a8 g
  f e d2 r4
  e2~ e8 f16 e d8 e
  \grace g8 f2 r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Romance [Tutti]"
  \time 2/2
        <a f'>4 <c g'> r8 c c c
        <c a'>4 <bes bes'> r d'8. c16
        c4 c8. d16  f,4 \grace a8 g8. f16
        f8 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Romance [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
        R1*3
        f4. c8 c4. a8
        a c f a c r r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Allegro"
  \time 6/8
      \partial 8
      g8
      g c c \grace d16 c8. b16 c8
      d4 e8 c c16 d e f
      g8 g g
      \grace a16 g8. fis16 g8
      g,4.~ g8
}