 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 4/4
    es4. es8 d4. f8
    es4 es r2
    c4. d16 c bes4 d es es r2
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Andantino"
  \time 4/4
  f4 bes
  d,2 es4 g c,2 bes4 c8 a bes d f bes f es es d
  d4 c
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Menuetto. Allegro"
  \time 3/4
  \partial 4
  bes4
  c8 bes a bes c d
  es4 es f
  d2 bes4
}

\relative c'' {
  \clef alto
  \key es\major	
   \tempo "4. Allegretto"
  \time 6/8
  \partial 8
    bes8
    bes as g bes as g
    c4 c8 c4 as8
    as g f  as g f bes4 bes8 bes
}