\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  \partial 8
  e,16 f
  g8 g g g g d d g
  g c, c' d16 c b8. a16 g8 g
  a c f4  g,8 b e4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \partial 8
  r8
  \set Score.skipBars = ##t
   R1*24
  r2 r4 r8 e16 f
  g8 g g g g d d g
  g a16 b c8 a b8. a16 g8 g
  \times 2/3 { a16[ g fis] } fis4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  r4 r8. r32 a a16. gis32 gis8 r8. r32 b
  b16. a32 a8 r8. r32 e' e4 d16. c32 b16. a32
  gis16 e' d cis d b c a b
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio [Solo]"
  \time 4/4
  \set Score.skipBars = ##t
   R1*4
   a8 a'4 e8 c' e gis,8. a32 b
   a4 e8. fis16 d cis d8~ d16 b' gis d
   c b a4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro assai [Tutti]"
  \time 3/8
  \times 2/3 { e16[ f g]} f8 e
  d c r
  \times 2/3 { e,16[ f g]} f8 e
  d c r
  
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro assai [Solo]"
  \time 3/8
  \set Score.skipBars = ##t
   R4.*43
  \times 2/3 { e16[ f g]} f16 e d c
  b4 c8 
  \times 2/3 { f16[ g a]} g16 e f d
  e c d b c8  
}
