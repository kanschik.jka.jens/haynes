 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante"
  \time 2/2
    bes8 d16 es f8[ g] a, f r bes
    es8 es es g16. es32 
    d4 r16 d g16. d32 
   
}


 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/4
      bes4 \grace { c16 d} es4
      d8 g f es
      d g f es
      d4 c bes2~ bes
   
}

 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Larghetto"
  \time 3/4
       r4 g8 a bes c 
       d4 c8 bes a c
       bes4 a8 g d'4 g g4. a8 fis4 r
}

 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Vivace"
  \time 3/4
    f4 d8 c d es  f4 bes r
    es, c2 d4 g f es c2 d4 bes r
   
}

