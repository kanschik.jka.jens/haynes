 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Spirituoso"
  \time 2/4
  \partial 8
  c8
  g'8 \grace a8 g16 f32 g c8 a
  g16. g32 \grace a8 g8 c f,
  e c \times 2/3 { d16[ e f]} \times 2/3 { d16[ e f]}
  e8 g r4
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Tempo giusto"
  \time 3/4
  g'4 e16 f e f g8. g16
  g4 c r8 r16 g
  g f8 r16
  \grace g8 f16 e8 r16 e8 r
  e16 d8 r16
}
