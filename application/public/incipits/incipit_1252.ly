 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key f\major
   \tempo ""
   \time 4/4
   f4. a8 g c,4 bes'8
   a c, f2
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alto"
  \key f\major
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
     R1*11
     r2 r4 r8 c,
     f4. a8 g4. bes8 a bes c4 r r8 g
     a bes c4 r r8 bes
     a g f e d4 r8
}