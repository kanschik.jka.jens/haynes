 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Andante"
  \time 3/4
      r4 d es 
      es2 d4
      r g a
      fis4. e8 d4
      r es c d a bes~
      bes8 c c4. bes16 c d4
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro"
  \time 2/2
    d4. d8 g, es'4 es8
    d c bes a g bes'4 d,8
    c bes' a c, bes a' g bes,
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Adagio"
  \time 3/2
    r2 g1
    g1.
    g4 a a2. bes4 bes1.
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Allegro"
  \time 2/4
    g2 a bes4 r8 bes
    c4 d g,8 g' d e
    f a, d f,
    g d' cis d16 e
    a,8 a' d,4
}