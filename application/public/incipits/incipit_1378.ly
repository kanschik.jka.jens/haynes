 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1.Allegro "
  \time 2/2
      g4 d bes8 g r4
      g' d bes8 g r d''
      bes' bes bes bes bes g r c, a' a a a 
      a f r
}


\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key g\minor
                      \tempo "2. Largo"
                      \time 4/4
                       r1 g'2~ g8 a16 bes a8. g16
                       f4 r r8 g c8. bes16
                       a8 c, f8. es16 d4 r
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key g\minor
                        % Voice 2
                       c2~ c8 d16 es d8. c16
                       bes4 r r2
                       r8 d g8. f16 es4 r
                       r8 a, c8. a16 bes4 r
                  }
>>
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. [Vivace]"
  \time 6/8
    bes16 d g d bes d  es g es g es g
    d g d g d g
    c, g' bes, g' a, fis'
    g,8 bes g c es c bes g bes a d d,
}
