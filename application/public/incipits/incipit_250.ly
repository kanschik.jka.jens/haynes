\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key g\minor
    \time 4/4
    \tempo "1. o.Bez."
        r8 d bes g  d'8. es,16 d8 g
        f4. es8  d f' g f
        es d c bes a bes16 c  fis,8
}

 