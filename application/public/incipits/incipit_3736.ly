\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante [Tutti]"
  \time 2/2
  <c e,>4 <c e,>2 c8 g <e' c>4 <e c>2 e8 c
  g'8. f16 e4 g8. f16 e4
  d4 d8 d d4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante [Solo]"
  \time 2/2
  r2 r4 g4
  c2. g4
  e'2. c4
  g'8. f16 e4 r2
  d4 d8 d d4. f8 e4 e r2
}
