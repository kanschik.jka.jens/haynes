 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro moderato"
  \time 4/4
    f,4. c8  a' f c' a
    f4 c' c c
    c4. d16 e f4. e8
    \grace e8 d4 c r2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Menuetto"
  \time 3/4
  \partial 4
    c4 f f f
    c'2 bes16 a g f
    g4 a bes
    \grace bes8 a4 f
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Adagio"
  \time 4/4
  \partial 8
  c8 g'4~ g16 f e f \grace f8 e4~ e16 d c d
  c8 c'~ c16 bes a g \grace bes16 a8 g r4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Menuetto"
  \time 3/4
    c'2. a4 bes g
    f bes a g r r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. Rondo"
  \time 2/4
    c8. bes16 a8 g f f'4 e8~
    e d4 c8 \grace c8 bes4 a8
}

