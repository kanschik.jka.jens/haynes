\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio [Violine]"
  \time 4/4
    c8 c d d  es es f f
    g g f f  f es es d
    d c f f d d c c
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio [Oboe]"
  \time 4/4
   \set Score.skipBars = ##t
      c1~ c4 c16 d es f g2
      g8 g as g16 as as8 g r
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro"
  \time 4/4
    c8 g d' g, es' as16 g f es d c
    b8 g d' g, f' g16 f g  f es d
    es8 g, d' g, es' g, r4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Adagio [Violine]"
  \time 3/4
   \set Score.skipBars = ##t
     r4 bes es as, as as g es r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Adagio [Oboe]"
  \time 3/4
   \set Score.skipBars = ##t
     bes2.~ bes~ bes4
     es8 d f es
     es4
}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Allegro"
  \time 4/4
    g8 g c c  b16 a b c d es c d
    es f d es f g es f d es c d es f d es
    c d b c d es c d b8 g r4
}