 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
  d8. e16 d8 c
  c4 b8 a g r \grace b16 a8 g16 a
  b4 r
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Andante moderato"
  \time 2/4
  g'4 \times 2/3 { g16[ e c] } \times 2/3 { c'16[ b a] }
  g8 r r \times 2/3 { c,16[ g' e]}
  
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Menuetto"
  \time 3/4
  \partial 4
  g'4
  fis g e
  d fis g
  a b c
  \grace c4 d2
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Thema La Fantasy. Moderato"
  \time 3/4
  d8 b g'4. fis16 e
  d4 c b
  a8. b16 c4 c
  c8 b16 c b4 r
}