\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Partita I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  f4 f8. f16 f4 g
  a bes8 g g f f e
  f4 a,8. a16 a4 b
  c r r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/4
   f4 g a c16 bes a g
   f4 g a r8 c,
   d c r a bes a r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Menuetto"
  \time 3/4	
   f4 g a
   bes2 a4
   g g8 bes e, g
   f4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Finale. Allegro"
  \time 6/8
  a8 f'16 e f g f4 r8
  r8 f16 e f g f4 r8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Partita II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Larghetto"
  \time 3/4
  c4 bes a a g a
  bes8 d d c c bes
  \grace c8 bes8. a32 bes a4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/4
  	a4 bes
        c8. d16 c8 r
        f a,4 c8
        \grace c8 bes8 a r4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Menuetto"
  \time 3/4
  \partial 4
   c4
   a bes c d2 bes4
   g a bes a r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Finale. Presto"
  \time 3/8
  \partial 8
  f,32 g a bes
  c4 bes8 a4 g8
  a bes g a r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Partita III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
  c8 c4 d8 c4 r
  e8 e4 f8 e4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Menuetto"
  \time 3/4
   g'2 a8 g
   \grace g8 f4 f e
   \grace g8 f2 d8 b
   c2 r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Adagio"
  \time 2/4
   g'4 f16 e a g
   g f e4 d8
   e f16 d d c d b
   c8 c16 e c8 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Finale"
  \time 3/8
  c8. d16 c8
  d8. e16 d8
  e8. f16 e8
  f8. g16 f8
}

  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Partita IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
  g'2~ g
  g16 a g f e d c b
  c4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 2/4
   a4. bes16 c
   c8 bes4 r8
   g'4 bes32 a g f  e d c bes
   bes8 a r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Menuetto"
  \time 3/4
   g'4 f g  a2 f8 e
   d4 e f
   f e r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 2/4
  g'4. a8
  \grace f8 e4. d8
  e c b d
  c4 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Partita V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
  g'4 d8 e
  d16 c c4 b8
  b16 d g b  b a g fis
  fis8 g r4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Menuetto"
  \time 3/4
   d2 b8 r
   e2 c8 r
   a'2 fis4
   fis g r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Adagio"
  \time 2/4
   c4. e8
   e16 d d8 r4
   f16 a g f   e d c b
   b8 c r4
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Presto"
  \time 3/8
  g4.
  d
  e fis
  g8 b a g fis e
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Partita VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
        << {
      % Voice "1"
        d2~ d d d
         } \\ {
      % Voice "2"
        b8 b16 a a8 c16 b
        b8 c16 b b a g a
        b8 g16 fis fis8 a16 g
        g8 a16 g g fis g a
      } >>

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Menuetto"
  \time 3/4
   g4. a16 b   c d e fis
   g4 g \times 2/3 { b8[ a g]}
   \grace g8 fis4 fis \times 2/3 { g8[ fis e]}
   d4 r r 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Finale"
  \time 2/4
   d4 fis g  c16 a fis d
   g4 fis g
}
