\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8
    b16. c32
    d8. b16 c8. e16
    d16. b32 g8 r d'
    fis, g c b b a r4
}

