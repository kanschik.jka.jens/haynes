\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Erleucht auch meine finstre Sinnen [BWV 248v/5]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key fis\minor
   \tempo "o. Bez."
   \time 2/4
   \partial 8
   cis8 fis4 fis8. gis32 a
   gis8[ fis eis fis]
   b b16 cis32 d cis8 b
   cis16 b a gis fis8 
   
 
  
  
   
}

\relative c {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key fis\minor
   \tempo "o. Bez."
   \time 2/4
   \partial 8
   \set Score.skipBars = ##t
   r8 R2*23 r4 r8  cis8 fis4 fis8. gis32 a
   gis8[ fis eis fis]
   b8. cis32 d cis8 b cis a fis
   
}



