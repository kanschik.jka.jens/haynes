 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "o. Bez."
  \time 6/8	
  c4. bes
  a8. bes16 a8 d4 g8
  e8. f16 e8 e4 f8
  f e g g f bes
  bes4 a8
}
