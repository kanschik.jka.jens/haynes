\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key d\major
    \time 4/4
    \tempo "1. ?"
 r2 r4 r16 a b cis
 d8 fis, g a d,4 d'
 r8 d fis d e16 d e fis e fis g a
 fis e d cis d8

}
