 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Symphonia"
  \time 3/4
     r4 bes c
     d4. es8 d4
     d8 es d es d bes
     c4 f, r
}
 