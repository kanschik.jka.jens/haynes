\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Recitativ (Tenor): Die Männer aber [BWV 246/41]" 
    }
    \vspace #1.5


}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
   c8 c' es c f, c' es c
   f, c' es d as f g g 
   g'4~
   ~g16 es d c as' g as8~as16 f d f
 bes, d f, as bes d f g es d c8
  
  
   

}


}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor [Evangelist]"
  \key c\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
 R1*9 r2 r8 g8 c c 
 c c16 c c8 es c c r8
  
   

}