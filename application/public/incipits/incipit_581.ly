\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\major
                      \tempo "Cantate"
                      \time 2/2
                      \partial 4
                      a4
                      d e8 fis e4 d8 e
                      fis4. e8 d2
                      R1
                      r2 r4
                  }
\new Staff { \clef "treble" 
                     \key d\major
                      r4
                      R1
                      r2 r4 a4
                      b a b cis d8 cis d e d4
                  }
>>
}
