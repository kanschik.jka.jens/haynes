\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  bes8
  es es es f16 g f es d c bes8 g'
  f16 es d c bes8 as' g[ es]
}
      
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Largo"
  \time 3/4
  g'4 es c
  b8. c32 d g,4 r
  g' es c 
  b8.
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro"
  \time 3/8
    g8 es c'
    bes8 es4
    as,8 f bes
    g4 f8
}

