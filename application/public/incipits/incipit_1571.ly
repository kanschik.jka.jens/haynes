\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quadro I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez. "
  \time 4/4
  \partial 8
  bes16 d
  f8 f4 f32 g f es d8 d4
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quadro II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
  <a, c f>8 f'32[ e f g] a8 a32[ g a bes]
  << {
      % Voice "1"
        c8. d16 c8
         } \\ {
      % Voice "2"
        a8. bes16 a8
      } >>

}
