\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VIIIe Suite" 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. Ouverture "
  \time 2/2
  a8. a16 fis8. fis16 d8. d16 a8. a16
  fis8. fis16 d8. d16 d'4 fis
  
  
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. 1er Rondeau "
  \time 2/4
  \partial 4
  d8 fis
  e[ d cis d] 
  g[ fis b a] 
  g [fis e d]
  e4 
 

  
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Loure "
  \time 3/4
  \partial 16*5
  d16 a4
  f a8. g16 f8. e16
  d4 r8 a''8 a a16 g32 a
  bes16 a g a bes g bes g
  

  
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Passepied "
  \time 3/8
  d4. fis a d
  fis,16 g a b a g
  fis g a b a g
  

  
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "6. 1er Rondeau. Gracieusement "
  \time 6/8
  \partial 8
  d8 fis,4 g8 a4 b8
  e,4. d8 d' cis
  d a fis' e a g
  fis e d e4 

  
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "8. 1er Rigaudon "
  \time 2/2
  \partial 4
  d4 fis e a, g'
  g fis a2
  d,4 e8 fis g4 fis
  e8 d cis b a4
  

  
}

\relative c' {
  \clef treble
  \key d\major
   \tempo "10. 1er Menuet "
  \time 3/4
  d4 d8 d d d 
  fis4 a d
  fis e8 d e cis
  d cis d a d e
  

  
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "12. Ariette "
  \time 3/8
  \partial 8
  a8 fis e d a'4 d8
  cis d e
  fis e fis 
  \grace fis8 g4 fis8
  e4 d8
  
  
  
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "13. 1er Gavotte. Gracieusement "
  \time 2/2
  \partial 2
  fis4. g8
  e4 d g8 a fis g
  e4 d fis8 g a fis
  b4 a g4. fis8 e2
 
   
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "15. 1er Tembourin "
  \time 2/4
  d8[ a fis d]
  fis'4 d
  a'8[ d, g fis]
  e16 d cis b a b cis a
 
  
   
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "17. Chaconne "
  \time 3/4
  \partial 2
  d4 a
  d, d'8 e fis4
  e g g 
  g a8 g fis e
  fis4 
 
 
  
   
}

