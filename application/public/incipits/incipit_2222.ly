\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4 
    \partial 8
    c8
    g'4 f16 e d c  \grace c8 b4. c8 a a4 b8 c4
}
