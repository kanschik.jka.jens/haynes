\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Molto Adagio"
  \time 4/4
    r16 g' e c  a'16. c,32 b16. f'32 \times 2/3 { e32 d c } c8.~ c32 e d8.
    d32 d f e e8~ e16 a fis8   g16 d f e  d32 b b d d c c e
    c16 b r8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Alla breve"
  \time 2/2
    r2 c
    g'2. g4
    e c c c
    a'1
    g4 g, g'2~
    g4 c, f2~
    f e d1
    c'4
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegretto"
  \time 3/4
      g'4 g4. f16 e
      \grace e4 f2 e4
      a4. f8 e d
      \grace c4 b2 c4
}
