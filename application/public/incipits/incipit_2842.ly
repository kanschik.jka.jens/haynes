 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

 

 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Largo"
  \time 2/2
  c16[ d e f] g8 g, f4 r8 c'
  d16[ e f g] a8 c, c b r4
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 2/2
  \partial 4
  c8 c
  g' c, c c  d c d c
  g' c, c c d c d c
  e f16 g f8 e d g, r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Largo"
  \time 3/8
  e16 f g8 e
  d16 e f8 d
  e16 f g8 e
  e4 d8
   }

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 3/4
  c8 g'4 e d16 c
  d8 g4 d c16 b
  c8 g'4 e d16 c
  d g f g
}
