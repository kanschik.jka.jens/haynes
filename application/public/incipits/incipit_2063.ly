 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegretto"
  \time 12/8
    g'4. r8 as b, \grace b8 c4. c'4.
    as g4 c,8 \grace g'8 f4. es4 g,8
    as c as  g c g' \grace g8 f4. es4
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Andante"
  \time 2/4
  \partial 8
  bes8 es16. bes'32 bes4 d,8
  es16. g32 \grace f8 es4 bes8
  c16. as'32 as4 c,8
  c16. bes32 bes8 r
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Allegro assai"
  \time 3/8
  g'16 es d c b c  c' g f es d c as'8 g r
  g16 es d c b c c' g f es d c f8 es r
}
