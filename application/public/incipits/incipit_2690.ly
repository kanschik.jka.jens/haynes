 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. o.Bez."
  \time 4/4
  e16 e fis fis  g g fis fis  e b g b  e b g b
  e e fis fis b, b dis dis e
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Andante"
  \time 4/4
  \partial 8
      << {
      % Voice "1"
      r8 r2 r4 r8 g'16 fis
      e d c b a8 b16 c32 d c16 b a b g8
         } \\ {
      % Voice "2"
      d'8
      c16 b a g d'8 g fis16 e e d d8 g,
      g g g fis  g fis g
      } >>

}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Menuet"
  \time 3/8
  b8 e fis g16 e b'4 a8 g fis
  g16 e \grace e8 fis4
}
