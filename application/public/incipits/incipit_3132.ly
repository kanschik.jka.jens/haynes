\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Recitativ (Alt): Ach Golgatha, unselges Golgatha [BWV 244/69]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe da Caccia 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key as\major
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                      es4~ es16 ges es des es4~ es16 ges es des
                      es4~ es16 ges es des 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe da Caccia 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key as\major
                     \set Score.skipBars = ##t
                        % Voice 2
                        c4~ c16 as c bes c4~ c16 as c bes
                        c4~ c16 as c bes
                       
                        
                
                  }
>>
}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key as\major
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
     r4 r8 es8 es8. ges,16 ges4
     r8 ges8 \grace {ges16[ as]} bes8. ges16 c8. c16 c4
}