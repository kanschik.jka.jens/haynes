\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 4/4
  es8 bes c bes16 as bes8 es, r4
  g'8 f16 es d c bes as g8 es4 bes'8
  es,[ g']
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro [Vn]"
  \time 2/2
  es,4. f8 g es as f 
  g es as f g es as f
  g f g
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro [Ob]"
  \time 2/2
  bes4. c8  d bes es c 
  d bes es c  d bes es c
  d bes es2 d4 
  es8
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Largo"
  \time 3/4
  c8 d es d es d
  es f g f g f
  es4 g8 f es d es4 c es
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Allegro"
  \time 2/4
  bes8 as16 bes c8 c
  bes4 es
  bes8 as16 g as8 f
  g f es4
}