\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro "
  \time 6/8
     \partial 8
     g'8
     g8. a16 g8  c e, g
     g4 f8 e4 e8
     d f d' \grace c8 b a16 g a b
     c8 g e c4 r8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 3/4
   \partial 4
   c4
  c2 f4 
  f2 a4
  g16 f g a  bes a bes c d8 f,
  \grace f8 e4
}
