\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\major
                      \tempo "1. Largo"
                      \time 3/4
                      \partial 4
                      r4
                      % Voice 1
                      r r d \grace e4 d cis r
                      R2.
                      R2.
                      d4 e e16 fis g8
                        fis2.
                  }
\new Staff { \clef "treble" 
                     \key d\major
                        % Voice2
                        d4
                        \grace e4 d cis r
                        R2.
                        d4 e e16 fis g8
                        fis2.
                  }
>>
}
