 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 6/8
  \partial 8
    c8
    a'8. bes16 a8 a4 a8
    a8. bes16 a8 a4 g8
    f16 e f4~ f8. c'16 e,8
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 2/4
  \partial 16
  c16
  f2 g
  a16 g f e f8 c
  d g~ g16 a bes d,
  c8 f~ f16 g a c,
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 3/4
  c4 a f
  e f a
  g a c
  c d e
  f2.
}

