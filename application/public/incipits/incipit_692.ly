\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "La Sultanne" 
    }
    \vspace #1.5
}

\relative c {
  \clef bass
  \key c\major
   \tempo "1. ohne Satzbezeichnung"
  \time 2/2
 d2 e4. d16 e
 f2 d
 bes'2. a8 bes
 a1~ a2 g~ g
 f4 e8 f e2

}

\relative c {
  \clef bass
  \key c\major
   \tempo "2. Air Tendrement"
  \time 3/4
 fis4 r8 f8 g4
 a \grace g8 f4 g
 \grace g8 b4 f bes
 a g4. f8
 e4. f8 g4
 a f g
 

}


\relative c''' {
  \clef treble
  \key d\major
   \tempo "3. Legerement"
  \time 4/4
  a4 a8. g32 a b8 a16 b
  g b a g
  a8 g16 a fis8 e16 fis g8 fis16 g e g fis e
  
  
  

}


