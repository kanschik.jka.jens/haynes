 \version "2.16.1"
    #(set-global-staff-size 14)
    
\relative c'' {
  \clef treble  
  \key bes\major
    \time 2/2
    \tempo "1. Allegro"
   f4. f8 bes a bes a|
   bes4 f8 f g f es16 d es f|
   d4. f8 bes a bes a|
   bes4
}

\relative c'' {
  \clef treble  
  \key bes\major
    \time 3/4
    \tempo "2. Adagio"
    f4 f8. es16 d8. c16
    bes4 f f
    c' c8. d16 es8. f16
    d8. c16 d8. c16 bes4
  
}
\relative c'' {
  \clef treble  
  \key bes\major
    \time 12/8
    \tempo "3. Allegro"
    bes4. r8 r f' d4. c 
    d r8 r f, g (bes g) a (c a) 
    bes (d bes) c (es c) d (c bes) a4.
    
   
}