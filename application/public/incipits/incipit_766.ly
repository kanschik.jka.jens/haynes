\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Lentement"
  \time 2/2
r2 r4 d'8 c
bes4 a g f
g bes a g
f d r a'8 g
f4 e d cis d
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Fugue. gay"
  \time 2/2
r4 a b cis
d e f g
a1~ a2 g2~ g f
e4 a2 g4 f
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Lentement"
  \time 3/2
  \partial 2
f4. f8
fis2. 
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Air. gay"
  \time 3/8
\partial 8
  a'8
  d, f16 g a8
  cis,4 a'8
  g16 f e f g a
  f8[ d ]
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. gai"
  \time 3/4
  a'8 a  fis fis  d d
  a2. fis'4 d fis e2.
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Prelude. lent"
  \time 3/2
  r2fis4 e fis2
  b, g'4 fis g2
  ais,2. ais4 b2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prelude. un peu lent"
  \time 3/4
  r4 d g~
  g fis b
  a8. d,16 d4 e fis g2
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1.Prelude. un peu animé et marqué"
  \time 2/2	
  r2 r4 b
  e1~ e~ e~ e2 dis
  e8 gis gis a b4 a8 gis fis4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Prelude. lentement"
  \time 2/2
  r2 r4 e,
  c'2. fis,4 b2. e,4
  a4. b8 b4. a8
  gis4 b
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Vivement"
  \time 4/4
 \partial 8
 fis8
 b, d16 e fis8 b16 ais b8 fis r16 b ais b
 g fis e d cis fis e fis d8[ b]
}
