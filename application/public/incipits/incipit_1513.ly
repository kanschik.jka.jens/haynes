\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "'Gelobet seist Du'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key bes\major
   \tempo "Andante"
  \time 4/4
     \set Score.skipBars = ##t
     R1*5
     r2 r4 bes
     bes bes c bes
     es8. f16 f4 es2
}

 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Hauptmanual"
                     \key bes\major
                      \tempo ""
                      \time 4/4
                      r2 r16 g' f es bes'8 d,
                      es4 r16 g f es d8 bes' bes bes 
                      bes \grace bes8 a16. g32 g4 f r
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oberwerk"
                     
                     \key bes\major
                        % Voice 2
                     r16 d c bes f'8 a, bes4 r16 d c bes
                     g'8 g, a4 bes8 d d d
                     e8 f4 e8 f16 a, g f c'8 e,
                  }
>>
}
  
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "'Wie schön leuchtet der Morgenstern'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key bes\major
   \tempo "Vivace"
  \time 6/8
     \set Score.skipBars = ##t
     R2.*12
     f,2. c' R f,2. c' R2.*2
     f,2. c' a \grace g8 f4. c' d d c2.
}

 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Hauptmanual"
                     \key f\major
                      \tempo ""
                      \time 6/8
                      R2. r8 c d e f e
                      d d e f g f
                      e g, a bes c bes a4.c~ c bes
}
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oberwerk"
                     
                     \key f\major
                        % Voice 2
                     r8 f g a bes a
                     g4. c~ c b
                     c8 e f g a g f c d es f es d2.
                  }
>>
}
  

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "'Ach Gott von Himmel sieh darein'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key d\minor
   \tempo "Adagio"
  \time 2/4
     \set Score.skipBars = ##t
     R2*6
     r4 a bes a g d' d8 c bes4 a2
}

 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Hauptmanual"
                     \key d\minor
                      \tempo ""
                      \time 2/4
                      g16 d' d c fis, c' c bes
                      d, bes' bes a d, a' a g
                      g d' c bes es8. d16
                      c c bes a  d bes a g
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oberwerk"
                     
                     \key d\minor
                        % Voice 2
                        R2 g16 d' d c fis, c' c bes
                        bes bes a g c8. bes16
                        a a g f bes d c bes
                  }
>>
}
  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "'Herr Christ der einge Gottes Sohn'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key bes\major
   \tempo "Andante"
  \time 4/4
     \set Score.skipBars = ##t
     R1*6
     r2 r4 bes
     bes d c bes a2 g4. r8 r4 d' es c d c bes2
}

 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Hauptmanual"
                     \key bes\major
                      \tempo ""
                      \time 4/4
                      r16 f es d c a' g f es c' bes a g es f g
                      a2~ a16 d, g f es4
}
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oberwerk"
                     
                     \key bes\major
                        % Voice 2
                       r4 r16 f es d c a' g f es c d es 
                       f c f es d es d c b4 c
                  }
>>
}
  

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Herr Gott dich loben alle wir" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key bes\major
   \tempo "Largo"
  \time 4/4
     \set Score.skipBars = ##t
     R1*4
     r2 bes bes a g f bes c d1
}

 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Hauptmanual"
                     \key bes\major
                      \tempo ""
                      \time 4/4	
                      r2 r8 f16 es d8 f
                      bes, a g a16 bes a8 bes16 c d4~
                      d8 c16 b c4~ c8 d16 es b4 c r
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oberwerk"
                     
                     \key bes\major
                        % Voice 2
                        r8 c16 bes a8 c f,4 f'~
                        f es f4. es16 d 
                        es4~ es16 g f es d4. es16 f
                        es8
                  }
>>
}
  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "'Du o schönes Weltgebäude'" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key d\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
     R1*5
     d2 a d4 d c c
     bes2 a
}

 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Hauptmanual"
                     \key d\minor
                      \tempo ""
                      \time 4/4
                       r8 d f d e a, a' g f4 e8 a4 gis16 fis gis a fis gis
                       a4. g8 f a, d4~
                       d cis d8
}
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oberwerk"
                     
                     \key d\minor
                        % Voice 2
                        R1 r8 a c a b e, e' d
                        c a b cis d4 r8 f,
                        g f e4 d
                  }
>>
}
  