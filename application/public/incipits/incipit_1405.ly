\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "Non troppo lento, piu tosto Andante"
  \time 3/4
  c4 b c
  a d f
  \grace {e16[ g]} f4 \times 2/3 {f8[ e d]} \times 2/3 {d8[ c b]}
  \grace b4 c2 r4
  a4 \times 2/3 {a8[ b c]} \times 2/3 {b8[ c d]}
  \grace d8 c8 b16 c g4~ g16 a32 b c d e f64 g
  a4
  
}
