\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Vivace"
  \time 3/4
  r8 f, f a a c
  c f f f f f
  f e16 d  c8 c16 bes a8 a16 g
  a8
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Scherzando"
  \time 4/4
  a8 d~ d16 cis d e f8 d r cis
d f,16 g32 a g16 f e f d4 r8  
}



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key f\major
                      \tempo "3. Bourée"
                      \time 4/4
                      % Voice 1
                      \partial 4
                      r4
                      R1 r2 r4 d
                      c8 d e f g a bes g
                      a4 g8 a f4 r
                  }
\new Staff { \clef "bass" 
                     \key f\major
                        % Voice 2
                        \partial 4
                        c,4
                        c bes bes a
                        a bes8 a bes4 bes
                        a g8 f e4 c f c f,
                  }
>>
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Menuett"
  \time 3/4
  f,16 g a8 a16 bes c8 c16 d e8
  f4 c2
  a4 c8 bes a g a4 g8 a f4
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "5. Loure"
  \time 3/4
    \partial 4
    a4
    d4. f8 e4
    f d a f4. g8 e4
    f4 d
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "6. Gigue"
  \time 6/4
  \partial 4
  a4
  a f a  c a c
  f c f  a f a
  g2 c,4 bes'2.
  a2 c,4 f2.
}