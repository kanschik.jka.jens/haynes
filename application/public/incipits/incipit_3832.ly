 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key es\major
                      \tempo "Allegro"
                      \time 3/4
                      es,16 g bes es  g, bes es g  bes, es g bes
                      f bes f d  bes f' d bes f bes f d
                      es g bes es  g es bes g  bes es g bes
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key es\major
                        % Voice 2
                      g,8 g'4 g g8 f4 as,2 g8 g'4 g g g8
                  }
>>
}



\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Largo"
  \time 12/8
  c,8 d es  g,4 g'8
  fis f e  g,4 f'8
  e es d  g,4 c8 f4 es8 d g g,
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Allegro"
  \time 3/8
  es,8 g16 f g as
  bes8 c d es4.
  d8 es f g4.
}
