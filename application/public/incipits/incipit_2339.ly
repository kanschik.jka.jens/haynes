 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  a'8
  f8. e32 d e8 cis d a r g'~
  g f16 e f8 bes cis, d
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Allegro"
  \time 4/4
  r8 d d, d' d4 cis
  r8 e a, g' g4 f
  r8 a d, a' bes4 a
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Allegro"
  \time 3/8
  d4 e8 f4 r8
  e4 f8 g4 r8 r f16. a32 d,8
}

