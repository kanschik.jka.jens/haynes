 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4		
  d8 d d d  d d d d
  d8. e16 d8. e16 c8. d16 c8. d16
  \grace c8 b4 a8 g fis4 g
  a b c
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Rondeau. Allegretto"
  \time 2/2
  \partial 8

}
