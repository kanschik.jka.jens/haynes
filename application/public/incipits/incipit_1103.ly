\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 4/4
  a4. \times 2/3 { a'16[ gis fis] } e4. d8 
  cis4 a r8 e fis gis
}
