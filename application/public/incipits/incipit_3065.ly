\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Genügsamkeit ist ein Schatz in diesem Leben [BWV 144/5]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key b\minor
   \tempo "o. Bez."
   \time 4/4
   r4 r8 fis8 b8. cis32 d d16 cis b ais
   b fis d fis b8 fis
   cis'8. d32 e e16 d cis b
   cis ais fis ais cis8 fis,
  
   
 
   
   
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key b\minor
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
    R1*6 r4 r8 fis8 b8. cis32 d d16 cis b ais 
    b4 r8 fis8 cis'8. d32 e e16 d cis b cis4 r8
}



