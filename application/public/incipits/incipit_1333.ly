 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Andante"
  \time 2/4
  bes8. bes16 es g, as8
  bes8. bes16 es g, as8
  bes16 c as8~ as16 bes f as
  g c as8~ as16 bes f as
}

