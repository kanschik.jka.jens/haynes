 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "o.Bez."
  \time 4/4
  R1
  r8 f a, d \grace c8 bes8. a32 bes c8 bes
  a4 f'2 e4
  f8 c a'4~ a8 d,
}

