\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro Spirituoso"
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \time 2/2
       \grace { a16 c}  bes4 bes2 es8 d
       \grace d16 \times 2/3 { c8 bes c} bes2 g'8 f
       \grace f16 \times 2/3 { es8 d es} d2 bes'8 f
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Andantino"
  \time 3/4
      g,8 g'4 g16 bes r bes d g
      g8. bes,16 a8 a16 c r c fis a
      a8. c,16
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Presto"
  \time 3/8
      bes8 r16 bes bes a32 bes
      d8 r16 bes bes a32 bes
      f'8 r16 f f e32 f
      a8 r16 f
}
