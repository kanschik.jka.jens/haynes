\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  g'2 \grace g16 a2
  g4~ g8. f16 e4 \grace g8 f4
  e8. g16 c,8. d16 d2
  c4 g g g
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante"
  \time 3/8
  \partial 16
  d16
  g8. a16 g8
  d'8 d,4 e8 d fis
  \grace a16 g32 fis g a b8 r16
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegretto [Tutti]"
  \time 2/4
  c'8 b32 a g f e16[ a] g32 f e d
  c8 b32 a g f e16[ a] g32 f e d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegretto [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*12
   e4 \grace f16 e8 d16 c
   g'4 c16 b a g
   g f f4 e8
   \grace e8 d4
}
