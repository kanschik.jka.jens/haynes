 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "Nr. 1"
   \time 4/4
       \partial 16
       c16
       c4 r8 \grace d8 c16 b32 c e8 \grace f8 e16 d32 e g8 \grace a8 g16 fis32 g
       c4 r8 \grace d8 c16 b32 c g8 \grace a8 g16 fis32 g e8 \grace fis8 e16 d32 e
       c4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "Nr. 2"
   \time 4/4
    c,8 c c c  c c \grace d8 c8 b16 c
    a8 f' f f   f f \grace g8 f8 e16 f
    d8 g g g  g f e d
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "Nr. 3"
   \time 4/4
       \partial 8
         r32 a' b cis
         d8 r16 a b8 r16 g fis4 r8 d
         \grace fis8 e8 cis
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Nr. 4"
   \time 3/8
       \partial 16
       d16
       g16. d32 d8. d16
       e16. c32 c8 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "Nr. 5"
   \time 2/2
       \partial 16
       d,16 d4 a''8 a fis r r4
       r4 g8 g e r r4 r c'8 c a
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Nr. 6"
   \time 2/2
       \partial 2
       d4. c8 b4. g8 c4. b8 a4. fis8 b4. a8 g4. g8 a8. g16 a8. b16 g4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "Nr. 7"
   \time 2/2
   a'4. d8 cis a g b
   a4. fis8 \grace a8 g8 fis g e
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "Nr. 8"
   \time 2/4
      f4~ f16 f  \grace g8 f16 e32 f
      a4 a
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "Nr. 9"
   \time 4/4
       \partial 4
       d8. c16
       b4 r r8 d16 cis d cis d b
       g4 r r8 g' fis e
       \grace d8 c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "Nr. 10"
   \time 3/8
       c8 r16 c' c c
       c b32 a g16 g g g
       g8 r16 e f g
       a g32 f e16 e e e
       e4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "Nr. 11"
   \time 2/2
       a'2. gis16 a g e
       d4 r r d
       d8 g g2 fis4
       fis fis16 a g fis
       d4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "Nr. 12"
   \time 4/4
      c16 e d c c8 r  c16 e d c c8 r
      d16 f e d d8 r d16 f e d d8 r

}

