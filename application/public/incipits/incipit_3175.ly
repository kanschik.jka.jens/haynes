\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Ruhet hie, matte Töne [BWV 210/4]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key e\major
                      \tempo "o. Bez."
                      \time 12/8
                      % Voice 1
                   gis4 fis8 \grace gis8 fis4 e8 e gis b d4.~
                   ~d8 cis b cis16 d e8 cis8 cis4 b8 b4.~ b8 a gis
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\major
                     \set Score.skipBars = ##t
                        % Voice 2
                    b,4 a8 a4 gis8 gis4.~ gis8 b e
                    b' a gis a16 b cis8 a8 a4 gis8 gis4.~
                    ~gis8 fis e
                       
                        
                
                  }
                
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key e\major
   \tempo "o. Bez."
  \time 12/8
     \set Score.skipBars = ##t
    R1.*12 gis4 fis8 fis4 e8 e4. d'4.~d8 cis b cis16 d e8 cis8 cis4 b8 b4.
   
    
    
    }