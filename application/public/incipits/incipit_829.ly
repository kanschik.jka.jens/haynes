\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 2/2
  f8[ g16 a]
  bes,8 bes bes a f'4
  \times 2/3 { e16[ c d] }  \times 2/3 { e16[ f g] }
}
