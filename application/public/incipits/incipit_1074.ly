\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio [Tutti]"
  \time 4/4
  \partial 8
  << {
      % Voice "1"
      r8
      r8 b b'4 r8 b, a'4~
      a8 g16 fis g4. fis16 e fis8. a16
      dis,8 b e2 dis4 e
         } \\ {
      % Voice "2"
      b8 g'4 r8 b, fis'4 r8 fis
      b,4 r8 e, c'4 r8 a16 c
      fis,4 g fis2
      e8
      } >>

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio [Solo]"
  \time 4/4
  \partial 8
   r8
   \set Score.skipBars = ##t
   R1*3 
   b2~ b8 r16 b[ b8. b16]
   e2~ e8. dis16 dis8. e32 fis
   e8 b g'4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro tempo di capella"
  \time 2/2
  e,4 fis g a
  b2 b c1 b2 e~ e dis e1
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Adagio"
  \time 4/4
  r4 b8 e, e' e e e
  e dis16 cis dis8 e16 fis b,8 b g' g
  g fis r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 3/4
  e8 g b g e g
  fis b dis, fis b, b'
  g fis e g fis e
  dis4. cis8 b4
}