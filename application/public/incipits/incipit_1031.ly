\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. All. Moderato"
  \time 4/4
  \partial 8
  g'16 a
   g8 g g c  \grace b8 a4. g16. c,32
   f8 f~ f16 d g f e4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 4/4
    c16. g'32 g16.[ f64 g] a8 c, \grace c8 b4 r
    g16. c32 c16.[ b64 c] d8 f, e4 r
  }

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Menuetto"
  \time 3/4
  c8. e16 g4 c
  \grace b8 a2 g4 f4. d8 e4 \grace e8 d2 c4
}