\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Mein Jesus will es tun, er will dein Kreuz küssen [BWV 72/5]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\major
   \tempo "o. Bez."
   \time 3/4
r8 g8 a16 g f g f'8 e
d g, a16 g f g g'8 f
e bes' a c, b f'
e16 d e g d2 r4
 
   
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key c\major
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
  R2.*16 r8 c8 d16 c b c f8 e
d2 r4

  
}



