
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ire. Gentillesse "
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key c\major
   \tempo "1. Gayement"
  \time 6/8
\partial 8
c8 g'4 g8 g4 g8
b,4. c
f4 f8 f e d 
e4 d8 c4 e8
d e f g4.
d8 e f g4.

 
}

 \relative c'' {
\clef "treble"   
  \key c\major
   \tempo "2. Gracieusement"
  \time 3/4
c8 e d c b a 
g4 f'2
\times 2/3 {e8 d c} d4 b
c c'2~ c4 b8 a g f
e4 d g \grace f8 e4
 
}

\relative c'' {
\clef "treble"   
  \key c\major
   \tempo "3. Mouvement de tambourin"
  \time 2/4
c4 e
d16 c b a g a b c 
d4 f
e16 d c b c d e f 
g4 a
g8 a16 b c8 e,

 
}


 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    " IIme. Gentillesse "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Gayement"
  \time 2/4
g4 c,
g4. f'8 
f16 e d c e d c b 
c4~ c16 d e f 
g8 g g a16 b
c b a g f e d c 
f4 e


 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Gracieusement"
  \time 2/2
\partial 2
es4 b8 d
c4 as'4 g f 
es d es d
g, g'2 f4
g4. d8 d4. c16 d
es4 d c bes a

 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gayement"
  \time 3/8
c8 d8. e16
b4 c8
d g4
e d8
e16 f g8 g
g d d
e16 f g8 g
g d d

 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIIme. Gentillesse"
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Rondement"
  \time 3/4
c4 c, c
c a'2
g8 f e d c b 
c4 a'2
g4 a8 b c f, 
f e a b c e,
e d e f g c,
b4. a8 g4


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement"
  \time 2/2
\partial 2
c4 g e' d g8 c, b a 
g2 g'8 c, b d
c f e d d4. c8 c2 



}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Mouvement de Gavotte"
  \time 2/4
\partial 4
e8 e
e d f f 
f e g g
g f16 e d8 c
g'4 

}





\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IVme. Gentillesse"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Gayement sans vittesse"
  \time 2/4
\partial 8
c8 g'4 c
g4. f8
es d c b
c4. es8
d c b as 
g4. c8 f4 es bes4.

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Moderement"
  \time 3/4
g4. f8 e4 
d4. c8 d4
e4. d8 c4
g2 r4
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gayement"
  \time 3/8
e8 d c 
g16 a b a g8
f'8 e d 
g,16 a b a g8 
g'8 f e 
a16 g f e d c
b4  

 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Vme. Gentillesse "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Gayement"
  \time 2/4
\partial 8
d8 d g, g g'
fis8. e16 d8 e
c8. b16 a8 d
b8. a16 g8 b16 c
d8 g, b d
e d g4

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Gracieusement"
  \time 3/4
\partial 4
b4
b a8 c b a 
b4 \grace a8 g4 g'
g fis8 a g fis
g4 d d 
e8 d d c c e
d c c b b d 

 
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gayement"
  \time 3/8
d8 g b
a16 g a8 d,
e16 d c b a g 
a4 g8
b8. c32 d a8
c b16 a b8
 
}






\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VIme. Gentillesse"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Rondement "
  \time 3/4
\partial 4
b4 b c d 
g, g'8 fis e4
d c4. b16 c
\override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
b2 \times 2/3 {b8 c d}
d4 a \times 2/3 {a8 b c}
c4 g

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Gracieusement "
  \time 2/2
\partial 2
b8 c d4
d g r2
r2 b,8 c d4
d c8 b a b c a 
b4 \grace a8 g4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gayement "
  \time 6/8
\partial 8
g8 d'4 d8 d4 e8
d4.~ d4 g8
d c b c b a 
b4 a8 g4 

}


