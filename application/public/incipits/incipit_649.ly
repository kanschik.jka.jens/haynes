\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
r8 g
c2~ c16 e d c
d8 g, r d' e d e fis
g[ g,]
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Largo"
  \time 3/4
    c4. es8 d c
    g'2 g8 d
    es4 as8 g f es
    d2 g,4	
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Pastorale. Vivace"
  \time 4/4
      r2 g'4. f8
      e c d g,
      c4. f8 
      e c d g, c d e d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 3/8
      g4 g8
      c4 c8 e d c
      f e d g d g
      e4 e8
}
