 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 4/4
    d,1 r8 d' b g e2 r4 c'8. a16 b4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. o.Bez."
  \time 3/2
  b2 g e
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. o.Bez."
  \time 6/4
    g'4 b,16 a b c d4 g, r r
}
