\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 3/4
  << {
      % Voice "1"
          R2. R f4 f f
          R2. bes4 bes bes
         } \\ {
      % Voice "2"
        bes,,4 d g e f r
        R2.
        bes4 bes bes
        R2.
      } >>

}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Allegro comodo"
  \time 2/2
  bes2. a8 bes
  \grace a8 g2. c4
  f,2. g8 a
  bes4 c d r
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Larghetto Sostenuto"
  \time 6/8
  \partial 8
  << {
      % Voice "1"
        r8
        R2.
        r8 g' bes g4 r8
        R2. r8 f as f4 r8
         } \\ {
      % Voice "2"
        bes,8
        a bes bes a bes bes
        bes4 r8 r r bes
        g as as g as as as4 r8 r bes bes
      } >>

}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key bes\major
                      \tempo "Vivace"
                      \time 6/8
                      R2. r4 r8 r r bes'
                      a g f es d c
                      bes bes bes bes4
                      % Voice 1
                  }
\new Staff { \clef "alto" 
                     \key bes\major
                     % Voice 2
                     f8 f f  g g g
                     f f f  f f f 
                     f f f f f f
                     f f f f4
                  }
>>
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro Moderato"
  \time 2/2
  \partial 4.
  c8 d e
  f g a bes c4 bes
  a8 g f e f4 es
  d g f e
  f r
}

\relative c'' {
  \clef alto
  \key bes\major
   \tempo "2. And.no con moto"
  \time 2/2
  \partial 2
  \grace { bes,16[ c] } d4. c16 bes
  bes4 a8.. c32 \grace { c16[ d] } es4. d16 c
  c4 bes8.. bes32 \grace { d16[ es] } f4. es16 d
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Thema con Variacioni. Allegro"
  \time 2/4
  \partial 4
  c8 c
  c a c f 
  c r f f
  g g \grace f16 e8 d16 c
  c8
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 4/4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto III " 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key es\major
                      \tempo "1. Larghetto Sostenuto"
                      \time 2/2
                      R1 R1
                      r4 bes'2 d,4
                      es8 g f es  d es \grace f16 es8 d16 c
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key es\major
                        % Voice 2
                       bes2. b4
                       c2~ c8 c d es
                       es es, g2 f4 es r r es
                  }
>>
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "All. Vivace"
  \time 2/2
  R1 r4 g'8 as bes4 g
  as c as f  d f d bes
  es r r2
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. And. gracioso con moto"
  \time 3/8
  \partial 8
  << {
      % Voice "1"
      bes'16. g32
      f8 r r
      r r g16. es32
      c8 r r
         } \\ {
      % Voice "2"
      r8 
      r d d d r r 
      r c c 
      } >>

}
  
  \relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Finale. Allegro MOderato"
  \time 4/4
  \partial 4.
  bes,8 a as
  g bes es g bes4. as16 f es4 r es'4. e8
  f g as a bes2
}
  
  \relative c'' {
  \clef treble
  \key es\major
   \tempo "4. "
  \time 6/8
  \partial 8

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro Moderato"
  \time 3/4
  \partial 4.
  f8 f f
  f bes, bes bes bes bes
  bes g  g g  g g
  g c, c c c c
  
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key g\minor
                      \tempo "Andantino"
                      \time 2/4
                      \partial 8
                      % Voice 1
                      r8
                      d2~
                      d8. fis16 a8 r
                      R2
                      r8 fis16. a32 c8 r
                  }
\new Staff { \clef "treble" 
                     \key g\minor
                        % Voice 2
                       \partial 8
                       d,8
                       d g r g
                       g16. fis32 a8 r es~
                       es es es es
                       es r r es
                  }
>>
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Finale. Allegretto"
  \time 3/4
  \partial 4.
  c8 cis[ d]
  bes4 r8 bes bes[ b]
  d c r c es g
  f4~ f16 a c bes a g f es
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Largo Sostenuto"
  \time 2/2
  c,2 \grace e8 d4 c8 d
  e2 c4 c8 e
  g4 g g g f2.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "Allegro Moderato"
  \time 2/4
  c,8 r c' r
  c' r r b,16 c d8 c4 e8
  a, f'4 d8
  \grace c8 b4 r
}
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key c\major
                      \tempo "2. And. Con moto Espresivo"
                      \time 2/2
                      R1
                      r4 r8 d16 b
                      g'8 g g g
                      g4 r4 r2
                      r4 r8 g16 e c'8 c c c
                      c4
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key c\major
                        % Voice 2
                       r4 r8 g,16 e c'8 c c c
                       d1
                       c4. e16 c g'8 g g g
                       g4 g2 c,4
                  }
>>
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Thema con Variacioni. And.no con moto"
  \time 2/4
  \partial 4
  c8. d16
  e8 e e16 g f d
  c8 c c16 e c b
  a f' d c b d b g
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro Moderato"
  \time 4/4
  \partial 4
  c8 f
  f2 a,4 a8 d
  d2 g,4 g8. fis16
  g4 bes e, g
  d2 c4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante con moto"
  \time 2/4
  \partial 8
  bes'8
  f f f bes
  bes, bes bes es16[ d]
  c bes a g f8 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Thema con Variacioni. And.no"
  \time 2/4
  \partial 8
  f16 a
  g f e d c8 c
  c4. a16 c
  \grace c16 bes4.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Andante"
  \time 3/8

}

