\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Tritt auf die Glaubensbahn [BWV 152/2]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key e\minor
   \tempo "o. Bez."
   \time 3/4
   r8 b8 a g fis e
   dis16 cis b cis dis e fis g a g a fis g8
   
   
  
  
   
}

\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key e\minor
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
   R2.*10  r8 b8 a g fis e dis4 r4 r4
   
}



