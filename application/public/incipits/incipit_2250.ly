\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro Moderato"
  \time 2/2
  \partial 4
      a8 g
      \grace g8 fis4 fis fis fis
      g4. a8 b4 a16 g fis e
      d8 cis cis4 cis cis
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio molto"
  \time 3/4
      d4 c b
      b8 a a a a a
      a4 b c
      b2.~
      b16 a c a g4 fis
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Rondeau. Allegretto"
     \override TupletBracket #'stencil = ##f

  \time 2/4
  \partial 8
  \times 2/3 { a'16 b cis}
  d8 d, a' a,
  fis' d, r d'
  cis d cis d
  fis e d d
}