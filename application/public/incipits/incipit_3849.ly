\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}




\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI [IX] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 2/2
    e4. e8 e dis r fis
    fis[ b,] e16 d c b c4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 4/4
  b4. b8 g c c c
  fis,4 b e,
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Adagio"
  \time 3/4
  g4 b e8 d
  d g, g4 r
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 3/8
  b8 e4~
  e8 dis16 cis dis8
  e8 g4
}
