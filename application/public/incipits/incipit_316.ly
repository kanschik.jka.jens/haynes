 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

   \tempo "1. Moderato"
   \time 2/4
   \partial 8
      \grace c8 b16 c
      d8 \grace c8 b16 c \grace e8 d8 b16 c
      d4 r16 g fis e
      d g d b  \grace d8 c8 b16 a
      \times 2/3 { b16 c d} d8
}

\relative c'' {
  \clef treble
  \key g\minor
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

   \tempo "2. Adagio"
   \time 2/4
   \partial 8
     d8 g,2~ g
     r r
     r4 r8 f'
     bes2~ bes~
     bes4 f8 f
    
}
\relative c'' {
  \clef treble
  \key g\major
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

   \tempo "3. Andantino"
   \time 3/4
      d2. \grace fis8 e2. d
      b4 d b a c a
      \grace c8 b8 c d4 r
}
