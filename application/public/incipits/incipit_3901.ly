\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
  \set Staff.instrumentName = #"Oboe"
   \tempo "Amoroso"
  \time 3/4
  g'4~ \times 2/3 {g8[ d g]} \times 2/3 {b8[ a g]}
  g4 b c
  b \grace d8  \times 2/3 {c8[ b a]} \times 2/3 {g8[ fis e]} 
  d4 a' a
}

\relative c'' {
  \clef treble
  \key g\major
  \set Staff.instrumentName = #"Soprano"
   \tempo ""
  \time 3/4
  R2.
  g4  d' e
  d \times 2/3 {e8[ d] c } \times 2/3 {b8[ a] g } 
  fis4 c' d
  \grace c8 b4 
}

