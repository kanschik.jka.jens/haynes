\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio non molto [Tutti]"
  \time 4/4
  f,1 a a8 bes bes2 bes4
  c8 bes bes2 \grace c8 bes8 a16 g
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio non molto [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*12
   c'1~ c~ c~ c~
   c2. \grace c8 bes a16 g
   f2 r4 a8 f
   c'2. b8 a g4 r r8 \grace a8 g16 f g8 a g8 f f2
  
}

