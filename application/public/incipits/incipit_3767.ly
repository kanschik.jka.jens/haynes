\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 3/8
  f8. es16 d8
  d32 c es c bes8 a
  bes8. c32 d c8
  es4. d
  d32 c es c bes8 a

}
