\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 4/4
  \grace f16 es8. d32 es  bes8 r
  \grace as16 g8. f32 g es8 r
  \grace g'16 f8. es32 f bes,8 r
  \grace g'16 f8. es32 f bes,8 r
  g'8 bes es, g \grace g16 f4 es8 d
  g8 bes es, g \grace g8 f4 es8 d
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Andante"
  \time 3/4
  \partial 16*5
  c16 es16. f32 g16. g32
  g4. c,8 \grace es16 d8 \grace c16 b8
  \grace b8 c4.
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegretto"
  \time 3/8
    es8 es,16. es'32 es16 d32 es
    f4 g16 as32 bes
    \grace bes16 as8 as g
    \grace g16 f8 \grace es16 d8 \grace c16 bes8
    es
}
