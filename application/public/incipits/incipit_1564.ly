\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
   R1*5
   r2 e16 d e f  e f e f
   d c d e  d e d e
   c8 b b4 a4 r r2
}
