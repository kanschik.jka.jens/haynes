\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}




\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII [VIII] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 2/2
  r8 f, c' c a f r f'
  d bes g g'16 f e8 c f4~
  f16 g f a g4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 4/4
  r8 f f c g'4. f16 e
  a8 a,
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Adagio"
  \time 3/2
  f,1 e2 f2. e8 d cis2
  d
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 6/8
   f8 c f  f a16 g f8
   g c, g' g bes16 a g8
}


