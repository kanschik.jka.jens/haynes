\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Hört, ihr Augen, auf zu weinen [BWV 98/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\minor
   \tempo "o. Bez."
   \time 3/8
   g8 c8. d32 es
   d8 c16 b c8
   g16[ b d f] es d
   es d d c c b
   b c d es f g
   as8 g16 f g8~ g16
 
   
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key c\minor
   \tempo "o. Bez."
   \time 3/8
   \set Score.skipBars = ##t
    R4.*16 g8 c8. d32 es
    d8 c16 b c8
    g16[ b d f] es d
    c[ b a b] g8
}



