\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegretto con vivacita"
  \time 2/4
   \partial 8.
   r32 d32 e16. fis32
   g16. a32 b16. c32 d8 d
   d c c d,16. e32
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegretto"
  \time 6/8
  \partial 4
  b8 c
  cis d b cis d b r4 r8 d4.
}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andantino"
  \time 3/4
        << {
      % Voice "1"
      c2. c bes'
         } \\ {
      % Voice "2"
      r8 c,, bes a bes c a4 r r
      r8 e' f e g f
      } >>
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Tempo di Minuetto"
  \time 3/4
  \partial 4.
  c8 f a
  c, f4 \times 2/3 { g16[ f e]} f8 a
  \grace a8 g8 f4
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegretto"
   \time 2/4
   d8 d4 cis16. e32
   g8 g4 fis8
   b8 b,4 cis8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Tempo di Minuetto"
  \time 3/4
  fis,8 a a d d fis
  fis fis16 e d8 d16 e fis e fis d
 
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Andantino "
  \time 6/8
   \partial 4
   e8 e
   e cis cis cis a a
   a4. fis8 fis' fis fis[ d]
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegretto"
  \time 2/4
  \partial 4
  e16. fis32 e16. d32
  cis8 cis[ cis cis]
  \grace e8 d cis d16. e32 d16. cis32
}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Andantino lento"
  \time 2/4
  es4.~ es16 r
  g4.~ g16 r
  a,4. c8~
  c fis a c
  b2
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Minuetto con moto"
  \time 3/4
  \partial 4
  g'4
  g2 as4 as2 g4
  g f8. es8 f8. g16
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegretto comodo assai"
  \time 2/4
  R2
  d4 d8 d 
  e32 d cis d e16. f32 g8 g
  g4 f8 r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Minuetto"
  \time 3/4
   \partial 4.
   cis8 d cis
   e d cis a bes8. a32 bes
   a4.
}