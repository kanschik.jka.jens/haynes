\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Grave"
  \time 4/4		
  r4 r8 e16. f32 g8 g, g a16 b
  c b c8 r16 g c16. b32 a16. b32 c16. a32 f8. e16 e4
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 2/4	
  \partial 8
  g8
  c c c c
  c16 b c d  c8 c
  d d d8. c32 d
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Largo"
  \time 12/8
  e8. d16 c8 b4 a8 gis4 a8 r r a
  b8. c16 d8 c4 b8 c8. b16 a8 r4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 6/8	
  c8 d e  d4 g,8
  c b c r r g
  c b a g4 f8
}

