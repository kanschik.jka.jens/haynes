\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Concerto"
  \time 4/4
  a'8 g16 f e d c b a a' e c a c b a
  gis e gis b e e, gis b e d e f e d c b
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Largo"
  \time 2/2
  g'8 g g g g4 r
  g8 g fis fis g4 r
  g8 g g g a4 a16 f g a
}
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro"
  \time 3/4
  a8 a, a a' a g
  f4. e8 d f
  e d e4 e a,8 a' a g g f
}