\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  c2 d4. b8
  c2 g4 c8 d
  e2. f4 e2 d4 r
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Moderato Variatione"
  \time 2/2
  c4 e8. d16  c8. d16 e8. c16
  d4 g8. fis16  g8. f16 e8. d16 c4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  c2~ c8 d16 c bes8 a
  g f f4 r8 f[ g a]
  bes4. c8 d4 d
  g,2 r
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Grazioso Variatione"
  \time 4/4
  \partial 4
  a8. bes16
  c4 c8. bes16 a4 a8. g16
  f4 f'2 e8 d
  c4. a8
}


\pageBreak

\markuplist {
    \fontsize #3
    "Quartetto III " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. All. Vivace"
  \time 6/8
  g4. a4 b8
  c4. g8 r g
  f'4 f8 e g e e d d d4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Grazioso Variatione"
  \time 4/4
    \partial 4
    e,4
    a e'8. d16 d8 c b c
    a4 e'8. d16 d8 c b c
    a8.
}

