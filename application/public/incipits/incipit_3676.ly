 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Larghetto"
  \time 12/8
    bes8. c16 bes8  bes es c  bes g' es bes4.
    c8. d16 c8 bes16 es d f es g \grace bes,8 as4. g4 r8
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Allegro ma moderato"
  \time 4/4
  es16 bes bes bes bes8 \grace d16 c16 bes32 as
  \times 2/3 { g16[ as bes]} bes16[ bes] bes8 \grace d16 c16 bes32 as
  \times 2/3 { g16[ bes es]} es16[ es]
  \times 2/3 { d16[ f as]} \times 2/3 { as16[ g f]} 
  \grace f8 g8 f r16
  
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Allegro assai"
  \time 3/8
  es4. \times 2/3 {d16[ c bes~]} bes8 r
  f'4~ \times 2/3 { f16[ g as]}
  \times 2/3 { g16[ f es~]} es8 r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}