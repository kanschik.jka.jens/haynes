\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Grave"
  \time 3/4
  e,4 \grace a8 g4. a8
  b2.
  b4 a8 g a fis g4
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/2
  g16[ fis g a]  g fis g a g8 fis r d
  e16[ d e fis]  e d e fis e8 d r 
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Andante"
  \time 12/8
   b8. e16 d8 e b c  b8. e16 d8 e b e
   g,4 fis8 g4 a8 g4 fis8 g4 a8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Andante"
  \time 3/4
  d4 d d 
  d c8 b c4
  fis,8 g a b c d
  c4 b8 a b4
}




