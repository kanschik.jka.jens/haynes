 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prelude"
  \time 4/4
  d8 b b g  d' a a d
  g,4 e' d8 g c8. b16
  b4
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Gavotte"
  \time 2/2
  \partial 4
  d4 e g8 fis g4 g,
  c e8 d e4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Menuet"
  \time 3/4
  b8 c d4 d
  d b8 a g4
  c8 d e4 c
  a2.
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Passepied"
  \time 6/8
  \partial 4
  d8 c
  b4 d8 g, c c
  c d16 c b a b8 g' fis e4
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "5. Sarabande"
  \time 3/4
  \partial 4
  d8. d16 e4 d g8 d
  e4 d e8 b c4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "6. Minuet"
  \time 3/4
  d4 g g
  g fis8 e fis g a4 c, c 
  c
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "7. Gigue"
  \time 6/8
  \partial 8
  g'8
  g4 e8 d e c b4. c
}
