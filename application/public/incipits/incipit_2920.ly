\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro moderato [Tutti]"
  \time 4/4
  \partial 8
  d8
  \grace fis16 g8 g \times 2/3 { g16[ b a]} \times 2/3 { g16[ fis e]} \grace e16 d8 d4 c16 b
  \times 2/3 { c16[ d e]} d8 \times 2/3 { g16[ b a]} \times 2/3 { g16[ fis e]} \grace e16 d8 d4 
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
 
   \partial 8
   r8
     R1*13 
  r2 r4 r8 d8
  \grace fis16 g8 g, \times 2/3 { g'16[ b a]} \times 2/3 { g16[ fis e]} \grace e16 d8 d4 c16 b
  \times 2/3 { c16[ d e]} d8 \times 2/3 { g16[ b a]} \times 2/3 { g16[ fis e]} \grace e16 d8 d4 
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 2/4
  \partial 8
  g16 a bes8 bes16 c d8 d 
  \grace { c16[ d]} es8 d4 d8
  bes16 d g, bes a c fis, a a4 g8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. [Solo]"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 2/4
  g8 b32 c d c b16 g b d
  g8 g32 a b a g4
  d8. g16
  d c b a
}