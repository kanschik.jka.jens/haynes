\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Douxiéme Suitte" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Prelude. Tres lentement"
  \time 4/4
  r8 r16 fis16 a8. a16 a8 d,~ d8 cis16 d
  cis8 fis~ fis16. e32 d16. cis32 b8 e~ e16. d32 cis16. b32
  a8 d~ d16. cis32 b16. a32
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allemande. Pas trop viste"
  \time 4/4
  \partial 8
  r16 d16
  d4. d8 \grace {cis16[ d]} e8 fis g a16 g
  fis8 fis, fis' a cis, a b cis
  d fis~ fis e~ e16 fis d8~ d16 e cis d
  b8 cis16 d e8 e,

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Sarabande. Tres proprement"
  \time 3/4
  d4 a4. g16 a 
  \grace a8 b2 b8. cis16
  \grace cis8 d4 b4. e8
  cis4 \grace b8 a4

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. La Parisienne. Tres legerement"
  \time 6/8
  \partial 4
  f8 g
  a bes a d,4 a'8
  bes a g a g f
  e f d cis4 d8
  a4 b8 cis4 d8 e4.~ e8 d e 
  f e d g f e 

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Chaconne. Piqué"
  \time 3/4
  r4 d8. cis16 d4
  a fis'2
  e4 a8. g16 a4
  e g2 
  fis4 d8. e16 fis8. g16
  a8. g16 fis8. g16 a8. fis16
  b8. a16 g8. fis16 

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "6. Premier Couplet. La Chasse"
  \time 6/8
  r4 a8 a4 d8
  d4 a8 a4 fis8
  fis4 d8 d4 fis8
  fis d a' a fis b
  a a a a a a 
  

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "7. Fanfare de P. Philidor"
  \time 6/8
  \partial 4.
  fis4 g8
  a4. e8. fis16 g8
  fis4 e8 fis4 g8
  a4. a8. g16 fis8 e4.

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "8. Menuet en suitte du même"
  \time 3/4
  fis2. e4 d e 
  fis g8 fis e fis
  d4 a d,
}






