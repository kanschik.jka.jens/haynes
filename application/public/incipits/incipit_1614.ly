 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "Largo"
  \time 4/4
  r8 b  c16 b a g  d'8 g4 fis8
  b, e4 d8~ d cis16 b cis4
  d
}

