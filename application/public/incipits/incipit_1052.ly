\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4	
    f2 a
    \grace a16 g8 f f4. c8 f g
    a4.. f16 c'4.. a16
    \grace c8 bes8 a a4 r2
} 


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Cantabile Adagio"
  \time 6/8
    g'4 e8 \grace f8 e8 d e~
    e g f d4 d8
    c d e f16 g a g f e
    \grace e8 d8 c16 b a g g4 r8
} 


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegretto"
  \time 2/4
  \partial 4
  f16 a c, f
  a,8. bes16 c d e f
  \grace e8 d8 c  f,16 a c f
  e d bes' a  a g a f
  \grace f8 e8 d16 c
} 


