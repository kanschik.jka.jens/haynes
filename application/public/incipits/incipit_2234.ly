 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Andante"
  \time 2/2
  e,4 e e e
  e e e e
  dis dis dis dis
  g g g g
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 2/4
  e8 e, e e
  e4. b'8 
  c b b e
  dis e e
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Andante"
  \time 2/2
  r16 d[ e fis] b,8 g' fis8.[ e16] fis e d c
  d[ d e fis] b, fis' g b,
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Gavotte"
  \time 2/2
  \partial 2
  e4 e,
  g a b c8 a
  b2 e4 fis g fis8 g a g fis e 
  dis4
}
