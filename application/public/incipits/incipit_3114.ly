\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Ruhig und in sich zufrieden [BWV 204/2]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\minor
                      \tempo "o. Bez."
                      \time 3/8
                      % Voice 1
                      d8. es16 d8
                      a16 bes bes8.[ a32 bes]
                      c16 d es8 d
                      bes a16 bes g8
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                       bes8. c16 bes8
                       fis16 g g8.[ fis32 g]
                       a16 bes c8 fis,8 g fis g8
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key g\minor
   \tempo "o. Bez."
  \time 3/8
     \set Score.skipBars = ##t
     R4.*12 d8. es16 d8
     a16 bes bes8.[ a32 bes]
     c16[ d es8]

}