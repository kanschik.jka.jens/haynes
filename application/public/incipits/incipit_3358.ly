 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 4/4
    a16. bes32 c16. d32  c16. d32 c16. d32 c16. a32 bes8 bes16. g32 a8
    a g r bes bes a r d 
    d c
}
