\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro brillante"
  \time 2/2
  \partial 4
  b8. a16
  a g fis g  b[ a g a]   c b a b d[ c b c]
  d4. e8 d4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Adagio"
  \time 2/4
  b8 g16. a32  b16 g' fis e
  d16. c32 c8 r16 c c c
  c8
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. poco Moderato"
  \time 2/4
  d4 \grace a'8
  g16 fis g e
  d4 e16 d c b
  a c b d c8 c c4 b8 r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro con giusto"
  \time 2/4
\partial 8
a16. a32
d4 e8 f
e8. cis16 d8 d16. a32
bes8 bes bes d16 bes
bes a a8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio"
  \time 2/2
  d4. a8 d4 d
  d4. b16 g fis4 r
  e8 g4 b e8 g b cis,4
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Prestoo"
  \time 6/8
\partial 8
  a'8
  a bes a a bes a
  a g f e f g
  f e d cis d e
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato ma non troppo lento"
  \time 3/4
  c4. b16 c d c e d
  g,4. c8 e g
  g16 f e d c b d c b a g f 
  f e a g g4 r
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 3/4
  f,8.. g32 g4 r
  bes8.. a32 a4 r
  f' e d c~ c16 cis d bes a4
}
  
  \relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Grazioso"
  \time 2/4
  \partial 8
  g'16. e32
  c8[ c c] g'16. e32
  f8[ f f] f16. d32
  b8 d16. b32 g8
  }
 
