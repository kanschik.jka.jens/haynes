 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key c\major
                      \tempo "1. Allegro"
                      \time 4/4
                      % Voice 1
                      c'4 r c r
                      r8 c b a g f e d
                      c4 r r2
                      r8 a' g f e d c b
                  }
\new Staff { \clef "treble" 
                      % Voice 2
                      c4. e16 d c4. e16 d
                      c4 r r2
                      c4. e16 d c4. e16 d
                      c4 r r2
                  }
>>
}



\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Andante"
  \time 4/4
  g'4 d \grace c8 b8. c16 d d g b
  a fis g b a fis g b  d,8. b32 c c4
  b r8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Menuetto"
  \time 3/4
  \partial 4
      g'8 e
      c4 c cis
      cis d8. f16 e8. g16
      f4 \times 2/3 { a8[ g f] } \times 2/3 { e8[ d c] }
      c4 b
}



\relative c'' {
  \clef "treble"   
    \key c\major
    \tempo "4. Presto"
    \time 2/2
    c2. d8 b c4 r r2
    e2. f8 d e4 r r2
    c4 d8 e f g a b
    c b a g f e d c
}
