\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
     g2 b 
     d c'8 b a g
     fis fis g g fis fis g g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 2/4
  \partial 8
    g16 c
    c4 b8 d16[ f]
    f4 e8 c'16[ b]
    \grace b8 a8. g16 g f f e
    e8 d r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Tempo di Minuetto"
  \time 3/4
    g'4 fis g
    \grace g8 a4 g8 fis e d
    d4 d d 
    d4. c8 b4
}
