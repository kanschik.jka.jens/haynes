\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/2
  c'2 c,4. c8
  c4 c c c
  c2~ c8 c' b a
  \grace a8 g4 f8 e
  \grace g8 f4 e8 d
  c2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante maestoso"
  \time 2/4
  \partial 8
    c8
    f f~ f16.[ \grace { g16[ f e] } f32] \times 2/3 { a16[ g f]}
    c'8 c, 
    \times 2/3 { c16[ e g]} \times 2/3 { bes16[ a g]}
    
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/8
  c4. d e f
  \grace a8 g16.[ f32] g16[ e f g]
  a16.[ g32] a16 bes c d]
  \grace g,8 f4 e8
}
