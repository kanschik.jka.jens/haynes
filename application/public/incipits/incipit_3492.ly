\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 12/8
    c8. d16 c8  g'4 e8 
    c8. d16 c8  g'4 e8 
    c8. b16 c8 r r c d4 e8  f8. e16 d8
}
