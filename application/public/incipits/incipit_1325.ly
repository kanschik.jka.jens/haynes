\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Scherzando I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/4
    f4 f,16 g a bes  c8 c c d
    bes8. a32 bes c8 bes
    a c f a
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Menuetto"
  \time 3/4
  \partial 4
  c4 bes8 a g f d'4
  \grace c4 bes2 a4
  a8 g bes g f e
  f4 c
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "3. Adagio"
  \time 2/4
  \partial 8
    c8
    f4 as16 g f e
    f8 c16 des des8 des8
    des c bes8. c16
    bes as g f as g f e
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Presto"
  \time 3/8
  \partial 8
  f,8
  f g a bes c d
   e,4 c'8 a f r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Scherzando II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 3/4
    <g, e' c'>4 r \grace a''16 g8 f16 g
    a4 f, r
    f'2 e16 d c b
    c4 c, r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Menuett"
  \time 3/4
  \partial 4
    g4 c e g, f8 e f4 g f8 e d c f4
    \grace e4 d2
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Andante"
  \time 2/4
    c8 es r es
    d g r g
    f16 es d c  es d c b
    c8 g r
}


  \relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Presto"
  \time 3/8
  \partial 8 
    e,16 f
    g8 g a16 c g4 g'8
    g, f' f
    \grace f8 e4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Scherzando III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
  a8
  fis d a g'
  fis b a d
  cis e a, g'
  g4 fis8 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Menuett"
  \time 3/4
    a'2 g8. fis32 g fis4 b d,
    cis8 d e fis g b
    a4 g fis
}
  
  \relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Andante"
  \time 2/4
    d,4. f8 e4. g8 \grace g8 f8 e16 d cis8 d
    a' a, r4
}
  
  
  \relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Presto"
  \time 3/8
    fis4 a16 fis
    d8 a fis
    e fis g
    g4 g8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Scherzando IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
    g,8 a16 b  c d e fis
    g8 g g g  b g g g
    fis g e g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Menuetto"
  \time 3/4
  d2. dis8 e e4 e
  e8 fis fis4 fis
  g b d,
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 2/4
    d4. es8 d d d d
    d c es8. d32 c
    bes8 g a fis g d r4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Presto"
  \time 3/8
    g,16 b d g b d
    c b c a g fis
    g b d g b g d4 r8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Scherzando V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Scherzo. Allegro"
  \time 2/4
    e4 r gis r
    e gis e r
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Menuetto"
  \time 3/4
    \partial 4
    e gis b b, cis e b
    a a' a8 gis
    \grace gis8 fis2
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Andante"
  \time 2/4
    e,8. fis16 g8 fis e dis e fis
    b, b' cis dis, e a16 fis g e fis dis
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "4. Presto"
  \time 3/8
  e16 gis b gis e8
  b8 cis dis
  e16 gis b gis e8
  b4 r8
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Scherzando VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 3/4
  a,4 cis e
  a a'4. gis8
  \grace gis8 fis4 e4. a8
  e4 \grace e16 d8 cis16 d cis4
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Menuetto"
  \time 3/4
  \partial 4
  e,8. a16
  fis4 b gis8. e16
  a4 cis e e8 d d cis cis b
  \grace b4 cis2
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Adagio"
  \time 2/4
  d4 fis8 a
  a4 \grace b8 a16 g32 fis \grace b8 a16 g32 fis
  e32 d cis d d4 a8
  b16 cis \grace e8 d16 cis32 b a8 gis
  fis4 r8
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Finale. Presto"
  \time 3/8
  a,8 cis e a r e'
  d cis r
}

