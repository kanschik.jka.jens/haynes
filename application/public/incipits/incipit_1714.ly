\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Moderato [Violin]"
  \time 4/4
  c,4 r8 r32 g a b c4 e
  c4 r8 r32 g a b c4 e
    g4. f16 e f8 g a b c4 r 
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Moderato [Oboe]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*3
    r2 c4. b8
    \grace b8 a4 a
    f'4. e8
    \grace e8 d4 d d8 d e f
    g e c e \grace e8 d4 c8 b c4 r 
}

\relative c'' {
  \clef alto
  \key c\minor
   \tempo "2. Adagio cantabile"
  \time 3/4
  g2.~ g
  g8 \grace f16 es16 d c8[ es g c]
  \grace b16 as8. g32 as g2
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo"
  \time 6/8
  c8. d16 c8 g c d
  d4 e8 d4 e8
  \grace g8 f4 e8 \grace e8 d4 c8
  e4. d8
}

