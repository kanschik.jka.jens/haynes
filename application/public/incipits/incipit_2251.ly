\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro assai"
  \time 4/4
  \partial 4
    bes16 as g f
    es4 es2 es'4
    es8 d d2 c4 c8 bes bes2 as4
    \grace bes8 as8 g16 as g4 r2
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio"
  \time 3/4

   g'8 es r \grace f8 g8  \grace f8 g8  \grace f8 g8 
   g8 d r g16 as g f es d
   \grace d8 c4 r8
}

\relative c'' {
  \clef treble
  \key ees\major
   \tempo "3. Rondo. Allegretto"
  \time 2/4
  \partial 4
  bes4
  es8 g bes es
  bes4 g8 es
  d f as as
  \grace bes8 as4 g8 r
}