\version "2.16.1"
         #(set-global-staff-size 14)
         
         \relative c'' {
  \clef treble
  \key es\major
   \tempo "1.Allero [Tutti] "
  \time 4/4
  es,4 g8. as16 bes4 c8. d16 es8-. bes'-. bes-. bes-. bes-. bes-. bes-. bes-. es,,4  g8. as16 bes4 c8. d16
         }
            
         \relative c'' {
  \clef treble
  \key es\major
  \tempo "1.Allero [Solo] "
   \time 4/4
   es2~ es16 (c8.) g'16 (f8.) f2 (es16) (bes8.) bes'16 (g8.)
   f16 g8. as16 g8. \grace {bes8} as4 g8 f
         }
 \relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante [Tutti] "
  \time 3/4
  bes4 d16 (c) c (bes) r4 d f16 (es) es (d) r4 bes'32 (a bes) c-. bes8-. bes-. bes-. bes-. r
 }

\relative c'' {
  \clef treble
  \key bes\major
     \tempo "2. Andante [Solo] "
  \time 3/4
\grace {bes16} d2 f32 (g f es) es (f es d) \grace {g8} f4 bes,8 d f bes \grace {a} g2 \trill \acciaccatura {f16 g} f4
}
 \relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro assai [Tutti]"
  \time 2/4
  es,2 g4 bes es g bes g f8 bes,, bes bes bes4 r 
 }
  \relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro assai [Solo]"
 \time 2/4
es,2 g4 bes es2 es8 g bes g es \grace {es} d es g es4 es
  }