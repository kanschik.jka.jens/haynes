\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key es\major
                      \tempo "1. Allegro"
                      \time 3/4
                      % Voice 1
                      es2. es d es4 r8
}
\new Staff { \clef "treble" 
                     \key es\major
                        % Voice 2
                        g,4 as bes
                        es, e f
                        as bes as
                        g 
}
>>
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante ma Allegretto"
  \time 2/4
  f8 es es d 
  d4. f8 d f g es
  c4 r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Rondo. Allegro moderato"
  \time 6/8
  \partial 8
  g'8
  g as f f g es
  es4. f
  bes,8 bes bes bes c d
  es es es es r
}

