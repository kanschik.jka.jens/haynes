\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
      g'2 fis16 g8. a16 g8. g2 f16 e8. d16 c8.
      b8. c32 d c4 r e8 d
      d4 f8 e e4 a8 g
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Tutti]"
  \time 3/4
      \partial 8
      c8
      f4 e f
      \grace e8 d4 c r8 c
      f4 e f
      \grace c8 bes4 a r8
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Solo]"
  \time 3/4
      \partial 8
      r8
      \set Score.skipBars = ##t
   R2.*16 
    r2 r8 c
    f4. a16 g f e d c
    cis8 d d4. d8
    d16 bes f'8   f16 d bes'8  bes16 f e d
    d c b c c4. 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Tutti]"
  \time 3/4
    \partial 8
    g8
    c16 c e, e  f f g g  a a b b
    c4 e d
    e16 e g, g a a b b c c d d
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Solo]"
  \time 3/4
    \partial 8
    r8
    \set Score.skipBars = ##t
   R2.*55
   r2 r8 c
   g'2.~ g
   g8. a16 f8. g16 e8. f16
   d8. b16 c4 r8 
}
