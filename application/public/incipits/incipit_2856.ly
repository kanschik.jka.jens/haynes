 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [tutti]"
  \time 2/2
  bes'8[ bes,] bes16 c d es f8 f, f f
  d'16[ c d bes]  es d es c  f8 g16 f f8[ f,]
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [solo]"
  \time 2/2
     \set Score.skipBars = ##t
   R1*3 
  bes'8[ bes,] bes16 c d es f8 bes a f
  g16[ f g es]  f es f d  es c es d c8[ c ]
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Largo"
  \time 4/4
  r16 d bes g  d'4 r16 g fis g a8 d,
  bes'4~ bes16 d, e d  cis8 a bes' cis,
  d16 a bes' cis, d8 g16 a f4 e8. d16
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/8
  g'16 d g d a' d,
  bes' d, bes' d, a' d,
  g fis g bes a g
  fis a d, c bes a
}
