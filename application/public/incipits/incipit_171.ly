\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 3/4
  
    c4 e g~ g f8 e d c
    b4 d f~ f e8 d c b
}
