 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
    bes4~ bes16 bes d es f bes, bes8 r16 f' g a
    bes16. f32 d16. f32  bes,16 d c bes es d d8 r16 d16 c bes
    g' f f8
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegro"
  \time 4/4
  f2 d8 bes \times 2/3 { bes8[ c d] }
  d8 c r4 f2
  c8 a \times 2/3 { a8[ bes c] } c8 bes r4 
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Largo"
  \time 12/8
  r4. d4. d8. es16 d8  bes'8. a16 g8
  bes8. a16 g8 fis4 g8  c,2.
  c8. d16 es8  es d c
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Allegro"
  \time 2/4
    bes16 c d es  f8 bes bes a a4
    bes,16 c d es f8 as as g g4
    
}