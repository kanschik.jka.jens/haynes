\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 3/4
  r4 c4 f
  f e bes'~ 
  bes a2~
  ~a4 g2~
  ~g4 f r8 c8
  d2. c bes a
  a4 g

}

\relative c' {
  \clef treble
  \key f\major
   \tempo "2. Fuga presto"
  \time 4/4
  f8 c a'4~ a8 g c bes
  a4 f2 e4 
  f r4 r8 g8 a g
  f c a'4~ a8 g c bes 
  a8 c d4.

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Siciliana"
  \time 12/8
 d4 d,8 f8. a16 d8 \grace d8 cis4. r4.
 f4 d8 a'8. f16 d8 cis8. d16 cis8 r8 r8 d8 
 cis8. d16 cis8 r8 r8

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro assai"
  \time 3/8
  a16 g a bes a8
  a bes4~
  ~bes16 a bes c bes8
  a4 c8 bes4 g8
  f a g f4 d8 c4 r8


}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
  f8 bes,16 a bes8 g' \grace g8 f4 r8 bes,8
  a f16 e f8 d' \grace d8 c4 r8 c8
  d16 bes g f e d' c bes bes8 a r4

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro presto"
  \time 3/4
  bes8[ f bes d] c bes16 a
  bes8[ f bes d] c bes16 a 
  bes8 c d es f g
  a,4 f8 c' bes a16 g a4 r8
 

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Aria andante"
  \time 2/4
 d8 d es16. d32 es16. c32
 d8 g, r8 bes8 
 a a bes16. a32 bes16. g32 
 a8 d, r8

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro Assai"
  \time 2/4
  bes8[ d c a]
  bes16 c d es f8 f,
  g[ bes a f]
  g16 a bes c d8 d,
  es[ g f d]
  es16 f g a bes8 g
  a4.

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Siciliana"
  \time 12/8
 d8. es16 d8 es2.~ es8 d c
 c4 bes8 r4. r4. c8. d16 c8
 a'2.~ a8. fis16 d8 a' g fis g4. r4.

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Fuga Allegro"
  \time 4/4
  r8 d8 d d es4 fis,
  g4. g8 a bes16 c bes8 a
  bes4 g2 fis4
  g fis g a bes f g a d bes g a
  d,

}
  
  \relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Aria Largo"
  \time 3/4
  f4 g4. f8
  es4 f4. es8
  d4 es4. d8 c4 d r4
  d4 d d d8. c32 d c2

}
  
  \relative c''' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro Assai"
  \time 3/8
  g8 g, g
  g16 a bes4~
  ~bes8 bes bes 
  bes16 c d4
  es16 d es f es f
  es d es f es f
  d8 bes g g4 r8
  

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IIII. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Grave"
  \time 3/4
  a8 b c[ d16. c32 d16] e4
  f e r4
  e8 fis gis[ a16. gis32 a16] bes4~
  ~bes a2~
  ~a4

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro è un poco Presto"
  \time 9/8
  e8 a e c e c a c a 
  b e b gis b gis e fis e 
  c e c a c e a c a
  gis b gis e4. r4.

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Affettuoso"
  \time 4/4
  r8 e16 f g8. a16 \grace c,8 b4 r4
  r8 c'4 b a gis8
  a4 r8 d,8 c4 r8 b8
  a4  r8 g8 f4 e8g'~
  ~g4 fis g r4

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Vivace"
  \time 2/4
  a8 f' e16 d c b
  c8 a a4~
  ~a8 c b16 a gis f
  gis8 e e4~
  ~e8 gis4 b8~
  ~b d4 f8~
  ~f e4 d8 c a r4

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Grave"
  \time 3/2
  d4 g fis e d c
  b e d c b a 
  g2 g g 
  g fis r2
  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro Assai"
  \time 6/8
 g8 d'16 c b a g8 d' g
 fis4. r8 d8 fis
 g4. r8 fis,8 g 
 d4. r4.
 d'8 d16 c b a g8 b d 
 e2.

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
  \time 4/4
  r8 b4 c8 c b r8 b8
  e fis16 g fis g a e \grace e8 dis16 c b8 r8 b8~
  ~b a16 g d8 a' a g r16

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Menuet 1"
  \time 3/4
  g8 a b c d e
  a,2 g4~
  ~g8 e c'4 b 
  a g8 fis g4
  g8 a b c d e
  a,2
  

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Largo"
  \time 3/4
  c8 d es f g as
  b,2 c4~
  ~c b8 c d4~
  ~d c r8 es8~
  ~es4 d8 c d4 r4

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Fuga Allegro"
  \time 4/4
  r8 c8 g c~ c b16 c d es f g
  es8 c as'4~as8 g4 f8~
  ~f es d4 c8 d16 es d c bes a 
  g4

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Aria Sarabande"
  \time 3/4
  es2.~
  ~es4 d8 c d4
  es d4. f8
  g4 f r4
  g,4 g4. g8 f2.

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Vivace un poco"
  \time 3/8
    \set Score.skipBars = ##t
   R4.*4
   c8 g'4
   c,8 as'4~
   ~as8 g16 f es d 
   es8 c r4

}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "5. Fuga Allegro Assai"
  \time 2/4
  c4 es
  d8 g, d'16 es f g
  es4 c~
  ~c b c r4

}

