\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro [Tutti]"
  \time 2/4
  g'4 g,8 es'
  d16 c bes a g8 es'
  d c16 bes a8 bes16 g
  fis8 e16 fis d8
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*7 
    r4 r8 a
    bes16 a bes c  bes8 d16 bes
    a g a bes a es' d c
    bes a bes c  bes8 d16 bes
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio [Tutti]"
  \time 3/2
  f2 f f f1 r2
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio [Solo]"
  \time 3/2
   \set Score.skipBars = ##t
    R1.*2
    as'4. g8 f4. es8 d4 es8 f
    es2 es es es1.
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 3/8
  g'4 a8 bes g fis
  g4 a8
  bes g fis
  g16 fis g bes a g
  f e d8 r
}