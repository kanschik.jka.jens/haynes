\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 4/4
  g4 b8 d g4 b,
  \times 2/3 { a8 b c }  b2.
}
