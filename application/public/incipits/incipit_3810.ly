\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allemanda"
  \time 4/4
  \partial 16
  a16 a4 r16 cis a gis  a b a gis a d a gis
  a b a gis a e' a, gis a b a gis a d cis b
  cis8[ a]
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. o.Bez."
  \time 4/4
  \partial 8
  e8 a e e e a e e e
  a e fis d e a, a e'
  cis a
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Affetuoso"
  \time 6/4
  \partial4.
  e8 a4
  g4. e8 fis4 e cis d
  cis2 a4 a2
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Giga"
  \time 6/8
  \partial 8
  cis16 d
  e8 cis d e4 a8
  e cis d e4
}

