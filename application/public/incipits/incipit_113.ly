\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Aria Vivace"
  \time 2/2
  f4 es16 f g8 f4. bes,8
  c d es f d4 r
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Aria"
  \time 3/4
  r4 r8 f f f
  f es16 es d8 f es d 
  c16 bes c d c8 
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Aria"
  \time 3/4
    \partial 8
    f8
    f4 bes, r8 f'
    g es16 f g es es, g'
    f d bes4 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Aria Allegro"
  \time 3/8
      r8 r f g16 f es f g8
      f16 es d es f8
      es16 d c d es8
      d16[ c bes8]
}
