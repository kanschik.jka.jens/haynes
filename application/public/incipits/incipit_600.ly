 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Ouverture"
  \time 2/2
        << {
      % Voice "1"
      R1 
      r4 g'4 d es
      f2 es4. f8
      d8 d es f g4. f8
         } \\ {
      % Voice "2"
      r4 d g, a bes2. g4
      d' a bes c
      c bes8 a bes4. bes8
      } >>
  }

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Menuet"
  \time 3/4
  g'4 a bes
  a8 g a bes a4
  d, g4. a8
  fis4. e8 d4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Passepied"
  \time 3/4
  g'2 d4
  g8 fis g a bes4
  a bes g
  fis2 g4
}
