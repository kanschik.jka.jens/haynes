 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
    d4 r16 e fis g  a8 a a b16 cis
    d8 d, r16 d' cis b cis cis b a b b a g
    a a g fis g g fis e fis8 d a' a,
}


\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "2. Largo"
  \time 4/4
  fis8 fis fis g16 fis e8 e e fis16 e
  d8 d d e16 d cis2
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Presto"
  \time 2/2	
  a'4 b a8 fis b g a fis b g a fis g e
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4. Affetuoso"
  \time 3/8
    d8 fis16 e cis8
    a' a, a'
}