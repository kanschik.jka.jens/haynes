 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Ouverture"
  \time 3/4
  r8 a[ a b cis d]
  e2 d4 cis8[ cis cis cis d e]
  fis fis fis e d cis
  b4 e d
}
 
\relative c'' {
  \clef treble
  \key a\major	
   \tempo "2. 1.er Rondeau"
  \time 3/4
  r4 cis d
  e8 d cis b a gis
  fis4 b cis d8 cis b a gis fis
  e4 a8 gis a4
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "4. 1.er Rondeau"
  \time 2/4
  \partial 4
  e8 a a e e a16 gis
  a8 e cis d
  e fis16 e d cis d b
  cis8 a e' a
}


\relative c'' {
  \clef treble
  \key a\major	
   \tempo "6. 1.er Menuet"
  \time 3/4
  cis8 d d4. cis16 d
  e8 a e cis a e
  a b b4. a16 b
  cis8 d cis b cis a
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "8. 1.er Rigaudon"
  \time 2/2
  \partial 4
  e4 a e e d cis2 b4 cis b a b gis
  a8 b cis d e4
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "10. 1.er Air"
  \time 6/8
  r4 e8 cis4 d8 e4 a,8 d cis b
  cis \grace b8 a8 e' a4 e8
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "12. 1.er Passepied"
  \time 3/8
  \partial 8
  cis16 d e8 a e cis a b
  cis d16 cis b cis
  a gis a b cis d
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "14. 1.er Gavotte"
  \time 2/2
  \partial 2
  cis4 b a e' fis e8 d
  cis4 b cis d e fis8 e d4. cis8 b2
}



\relative c'' {
  \clef treble
  \key a\major	
   \tempo "16. 1.er Tembourin"
  \time 2/4
  a16 b cis d e8 a,
  fis'4 e a8 a e e
  cis b16 cis a8 e
}



\relative c'' {
  \clef treble
  \key a\major	
   \tempo "18. Chaconne"
  \time 3/4
  r4 a cis e a, e'
  a e a,
  d8 cis b a b gis a4
}