 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Affetuoso"
  \time 4/4
        << {
      % Voice "1"
        c8 a16. bes32 c8 c f,4 r
        f'8 d16. es32 f8 f bes,2
           } \\ {
      % Voice "2"
      r2 a8 f16. g32 a8 a
      d,4 r d'8 bes16. c32 d8 d
    } >>

  }


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 2/2
  << {
      % Voice "1"
      c4 r8 f g c,16 d c[ bes a g]
      a8 f r f' g c,16[ d] c
           } \\ {
      f,8 a c f e4 r
      f,8 a c f e4
    }
  >>
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Affetuoso"
  \time 3/4
  d8 e f e d4 e8 f g f e4 bes' a8 g f e f e d2
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Presto"
  \time 2/2
  f,4 f'2 e4 f g c,2 bes2.
}