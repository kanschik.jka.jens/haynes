 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Vivace"
  \time 2/4
  e,8 e4 fis16 gis 
  a gis a8 r a
  fis' e4 d8
  cis8[ a]
}

