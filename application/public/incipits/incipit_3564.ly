\version "2.16.1"
         #(set-global-staff-size 14)
         
         \relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 4/4
  
   c8 g4 a16 b16 c8 g4 a16 b 
     c8-+ e16. c32 d8 f16. d32 e (d e f) g8 r8 e,32 (d e f)  
}


    \relative c'' {
  \clef treble
  \key e\minor
   \tempo "2.Adagio [Tutti] "
  \time 4/4
  
 b16-. (b-. b-. b-.) b4:16 b4:16 b4:16 
 b16 fis'(g) dis e (b) c (b) \grace {b} ais4 b16 fis b a
    }
  \relative c'' {
  \clef treble
  \key e\minor
  \tempo "2.Adagio [Solo] "
     \time 4/4
 b'1 b16 g (fis e) dis (e) b (d) \grace {d8} c8 b  r e~e dis16 e fis8 a \grace {b}
  
     }
 \relative c'' {

\clef treble
  \key c\major
   \tempo "3.[Tutti]"
  \time   2/2
  g'2 c,
  a'4 g2 e4
  a g2 e4 f4. e16 f g4 f 
  e c a d
 }
  \relative c'' {
  \clef treble
  \key c\major
   \tempo "3.[Solo]"
  
  \time 2/2
  \override TupletBracket.bracket-visibility = #'if-no-beam 
  
 \tuplet 3/2 {g16 (b a)}
   \tuplet 3/2{g( b a)}
  \tuplet 3/2{ g b c }
   \tuplet 3/2{d g d}
   
   \grace {c16 d} e8 d g4~ g32 fis g a
   g16 g g bes g 
 }