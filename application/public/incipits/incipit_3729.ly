\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key es\major
    \time 4/4
    \tempo "1. o.Bez."
    \partial 8
    \override TupletBracket #'stencil = ##f
    bes8
    bes16 as g4 f8  es16 d d8 r es'16 g
    e f es d   c bes as g \times 2/3 {g16[ c bes]} bes4 b8
}


