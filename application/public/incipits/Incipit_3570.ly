 \version "2.16.1"
         #(set-global-staff-size 14)
         
 \relative c'' {
  \clef treble
  \key f \major
   \tempo "1. Allegro  [Tutti]"
  \time 4/4
 \chordmode  { f8^5/a } r f,16 e f g a4 r \chordmode  { f8^5} r a16 g a bes c4 r \chordmode  {f'8^5/a } f,32 g a bes c8-. a-.
 }
  \relative c'' {
  \clef treble
  \key f \major
   \tempo "1. Allegro  [Solo]"
  \time 4/4
  f,2~f8 e f g a2~ a8 \trill g a bes c f4 es8 d8. c16 d8. e16 f8. \trill e16
  }
  
  \relative c'' {
  \clef treble
  \key c \major
   \tempo "2. Arioso [Tutti]"
  \time 3/8
  e,16 (f) g8. a16 g32 c b a \grace{a8} g8. a16 g32 ( c b a) g (c b a) g (a f e)
  }
  
   
  \relative c'' {
  \clef treble
  \key c \major
   \tempo "2. Arioso [Solo]"
  \time 3/8
  e16 (f) g8. a16 g32 c (b a) \grace{a8} g8. a16 g32 ( c b a) g (c b a) g (a f e)
  }
  
  \relative c'' {
  \clef treble
  \key f \major
   \tempo "3. Vivace"
  \time 3/4
  f2. f,16 (g a g) f 4 r a'2. a,16 (c bes g) a4 r d2 e4
  }