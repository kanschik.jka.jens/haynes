\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
    g8
    c c4 c8~ 
    \override TupletBracket #'stencil = ##f
    \times 4/6 { c16 a' g f e d} c8 b
    c8 g
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. o.Bez."
  \time 4/4
  \override TupletBracket #'stencil = ##f
  c4 \times 4/6 { c16 d es  es f g}  \grace c,8 b4
  \times 4/6 { b16 d c b a g} 
  f'4 \times 4/6 { f16 b f d b g}
  
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/8
    c4. g e8 d c
    e' d r
    g,4. d b8 a g
}
