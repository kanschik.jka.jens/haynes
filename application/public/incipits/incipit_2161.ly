 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
    d4. e16 d d4. e16 d
    d8 c16 b c8 b16 a b4 r
    r g'2~ g8 fis
    g
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 2/4
  g'4 fis
  g8 fis16 e d c b a
  b8 e d c
  b4 a 
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Adagio"
  \time 4/4
  r4 b e
  dis b r
  r b g'
  fis8 g g a a fis g4 b, b
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Allegro"
  \time 6/8
  \partial 8
    d8
    d b16 c d8 d a d
    d b16 c d8 d4 g8
    fis16 g a8 d, c4 b8
}