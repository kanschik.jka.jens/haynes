\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 2/4
  \partial 8
  e16 f
  fis g g8 r f16 g
  gis a a8 r d,16 e
  f8. e32 f g8 f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 6/8
  c8. d16 c8 c a f'
  c4 bes8 a r f'
  e4 f8 e4 f16 d
  c8 bes' a a g r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Tempo di Minuè"
  \time 3/4
  g'2 \grace { a16 g } f16 e d c
  c4 b c d d e
  g f d
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Moderato"
  \time 4/4
  f2~ f8 e16 f g8 f
  e d d4 r2
  \grace c4 bes2 \grace c16 bes8 a16 bes c8 bes
  gis a a4 r2
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 6/8
  \partial 8
  f,8 d'4 d8 d es c
  a4 c16 bes bes8 r f
  f'4 f8 f g es
  c4 es16 d d8 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Finale"
  \time 2/4
  c8
  f f f f
  f f32 e f g f8 r
  f8 f f f
  f f32 e f g f8 r
  
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Moderato"
  \time 3/4
  d2 es8. c16
  f8. d16 bes4 r
  bes'2 a8 g
  \grace a8 g4 f r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Andante"
  \time 2/2
  \partial 2
  d4 d
  d4. c8 bes4 a g r d' d
  bes' g d8 es16 d c8 b
  \grace b16 c4 r
}
  
  \relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro non troppo"
  \time 2/4
  \partial 8
  d8 d4 f16 es d c
  bes a g f   e f g a
  bes8. c16 d8 es
  d4 c8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  g'4 g8. g16 g4 g
  a16 g fis e d8 d d4 r
  e4 e8. e16 e4 e
  f16 e d c b8 b b4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andante"
  \time 2/4
  e4 g16 f e d
  c8 c d r
  e e g16 f e f
  g8. a16 g8 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 6/8
  \partial 8
  d8
  g4 g8 g b g
  d4. b4 r8
  c4 c8 \grace d8 c8. d16 e8
  a,4 r8 r4 r8
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 2/2
  d2 f d r f g a r
  a a c g4 a bes1 e,2 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Minuetto"
  \time 3/4
  fis4 g a d, cis d e g fis8 d fis e d cis b a
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Finale"
  \time 2/4
  d4. d8 \grace f8 e4 d8 r
  a'4. g8 \grace g8 f4 e8 r
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Moderato"
  \time 2/4
  \partial 8
  f16 g
  a8 f16 c c8 c
  d e16 f c8 r16 f
  f e g f bes g e bes
  a8 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Cantabile Adagio"
  \time 2/2
  g'2~ g8 a f d
  c4 b8 r r a a a
  a g g4. b8 d g
  f4. dis8 e4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo"
  \time 6/8
  f4 f8 f a f 
  e4 g8 c,4 r8
  g'4 g8 g a bes a f r r4 r8
}



