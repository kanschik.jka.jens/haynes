\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  f,2 f'4. f8
  cis,1 d2 d'4. d8
  a,1 bes4. c8 d4. e8
  f \grace g8 f16 e f8 g a \grace bes8 a16 g a8 bes
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Grazioso con Variationi"
  \time 2/4
  \partial 8
  f16. a32
  c8 c c c
  bes4 a
  g8 bes16 g f e d e f4 r8

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Tutti]"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Solo]"
  \time 4/4
   \set Score.skipBars = ##t

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 4/4

}