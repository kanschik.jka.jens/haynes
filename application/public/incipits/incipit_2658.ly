 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 3/4
  a8. b32 cis d4 e
  d8. cis32 b a4 b
  a8. b32 cis d4 e
  fis8 cis d2
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. o.Bez."
  \time 2/2
  a16[ g fis g] a g fis g a[ g fis g] a8 d,
  a' d4 cis8 a fis' e4
  a, d4 cis8 a fis' e4
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. o.Bez."
  \time 3/8
  d8 e d cis4 d8
  d8 e d cis16 b a4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}