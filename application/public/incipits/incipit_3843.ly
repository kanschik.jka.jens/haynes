\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}




\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4
  d4 r8 d e4 a, d4. fis8 g2~
  g4 fis g2
  
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/2
    d8 g16 fis g8 d c g'16 fis g8 c,
    b g'16 fis g8 b, a
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Adagio"
  \time 3/2
  r2 r g'
  fis b1~
  b2 a1
  
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 12/8
  g8 a g g a g  g a g g a g
  a b a
}
