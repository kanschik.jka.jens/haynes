 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro con spirito"
  \time 4/4
  c2. d8. b16 c2. g'8. b,16
  c2. d8. b16
  c b c d  e d e f g4 r8
}


\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key c\major
                      \tempo "2. Minuetto"
                      \time 3/4
                      % Voice 1
                      R2. R
                      g'2 \grace g16 f8 e16 f
                      \grace f4 e2 d4
                      
                  }
\new Staff { \clef "treble" 
                     \key c\major
                     % Voice 2
                     c2.~ c8. b32 c  d8 c b a
                     g4 a b
                     \grace b4 c2 b4
                  }
>>
}

