 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
    f4 e f8 c a f
    f'16 e d c  bes a g f c'8 c, r a''
    a a4 g16 f g8 c,16 b c8
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Adagio"
  \time 3/4
  r4 a' d,
  cis2 d4 e8 f g16 a bes8 bes a16 g
  f8 e d4. f8
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 3/8
  r8 f g e4 f8
  r c' d
  bes4 a8 g bes16 a g f
  
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}