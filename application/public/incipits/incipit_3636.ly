
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
  \tempo "1. Allegro moderato"
  \time 2/2
  \times 2/3 { g16[ a bes]} \times 2/3 { bes16[ c d]}
  d8[ r16 g] \times 2/3 { fis16[ g a]} \times 2/3 { fis16[ g a]} 
  c,8[ r16 d]
  \times 2/3 { bes16[ c d]} \times 2/3 { bes16[ c d]} g,16 bes a g
  \times 2/3 { fis 16[ g a]} g8 r4
}


\relative c'' {
  \clef treble
  \key bes\major
  \tempo "2. Adagio"
  \time 4/4
  f4 f16[ bes32 a g f es d]  es4 es16[ c'32 bes a g f es]
  d16 d es f  f g g bes, a f es'8. d32 es   f es d c
  d8 bes r
}

\relative c'' {
  \clef treble
  \key g\minor
  \tempo "3. Allegro un poco"
  \time 3/8
  d,8 es r
  e f r fis g r g' bes, bes bes a r
}
