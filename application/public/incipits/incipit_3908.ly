\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}





\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "Affettuoso"
  \time 2/4
  \partial 8
    a8
    cis32 d e8.~ e8 \grace d8 cis8
    \grace b8 a4 r8 e
    fis gis16 a b cis d8
    d cis r
}
