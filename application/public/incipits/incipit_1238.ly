 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio e  Piano"
  \time 3/4
    f4 a, bes c2. d4 bes2 a2.
    f'4 f e16 f g8
    c,2.~ c4 a f
}
