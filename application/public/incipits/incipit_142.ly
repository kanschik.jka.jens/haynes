\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Ouverture [Trumpet]"
  \time 2/2
  es,4. es8 es4 es   
  es4. es'8 es4 es
  c4..
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Ouverture [Oboe]"
  \time 2/2
    \set Score.skipBars = ##t
 R1*2
 r4. c8  c16[ d es d]   d es d c
 d4.
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Gigue"
  \time 2/2

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Aria. Vivace"
  \time 2/2

}
