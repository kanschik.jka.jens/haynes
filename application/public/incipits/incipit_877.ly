\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  bes32 c d es
  f8 d bes bes,32 c d es f8 d bes bes'32 c d es
  f8 d bes bes' \grace bes8 a4. bes16 f
  g8 bes16 es, f8 bes16 d,
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Andante"
  \time 2/4
    g'8 g,4 bes32 a g16 g'8 g,4 bes32 a g16
    g'8 g,4 bes'32 a g16 g8 fis r4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/4
  bes8 f'4 d bes8
  a16 c bes4 d8 es f
  a,16 c bes4 d8 es f
  a,16 c bes8 bes'4 g
}
