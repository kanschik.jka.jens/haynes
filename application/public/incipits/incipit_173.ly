\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 2/2
      c8 c, r b' c c, r b'
      c e, d b' c e, d b'
      c e, d b'  c c, r e'16 f
      g a g a  g[ a e f]  g a g a g[ a e f]
      g[ a e f]  g a e f  g a g a g[ f e d]
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Menuet Alternat."
  \time 3/4
      c8 e g e c g'
      a4 f f
      g8 e c g' f g
      a4 f f
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Meneut"
  \time 3/4
        g8 a b g d' e16 fis
        g4 fis g
        e b c
        c2 b4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Echo"
  \time 3/4
  \partial 2
      e4 d 
      e r r r e d
      e2 f4 g a8 g f e d4 r r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Giga"
  \time 2/2
  \partial 8
      g8
      \times 2/3 { c8 b c}
      \times 2/3 { d8 c d}
      \times 2/3 { e8 d e}
      \times 2/3 { f8 e f}
      
      \times 2/3 { g8 a g}
      \times 2/3 { f8 g e}
      d4 g,
      
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "6. Gavote"
  \time 2/2
  \partial 2
         g'4 g,
         c4. c8  c d d8. c32 d
         e4 c  g' g
         a g f e
         d d 
}