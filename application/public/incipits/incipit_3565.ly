\version "2.16.1"
         #(set-global-staff-size 14)
         
         \relative c'' {
  \clef treble
  \key c\major
   \tempo " 1. Allegro [Tutti] "
  \time 4/4
  c,2 e 
  g4. a16 b c8 g e c 
  a2 a''4. g16 f 
  e8 d c b c2
         }
           \relative c'' {
  \clef treble
  \key c\major
   \tempo " 1. Allegro [Solo] "
 \time 4/4
  c2 e g4. a16 b c8 g e c a2
  \grace {f'16 g} a4. g16 f
  e8 d c b c16 g a b
         }

\relative c'' {
  \clef treble
  \key c\major
   \tempo " 2. Poco Andante [Tutti]"
  \time 3/4    
  a8 a,16. gis32 a16. gis32 a16.gis32 a16. b32 c16. d32
  e16. dis32 e16. fis32 e16. dis32  e16. fis32 e8
}
  \relative c'' {
  \clef treble
  \key c\major
   \tempo " 2. Poco Andante [Solo]"
 \time 3/4
  \grace {dis16 e f} e4 \grace {dis8} c8 b16 a \grace {a8} a'8 g16 f 
 \slashedGrace  {g8} f e16 f e4 r16 a,32 b c d e f \slashedGrace {e8} d8 c16 b
 }
 \relative c'' {
  \clef treble
  \key c\major
   \tempo " 3. Presto"
  \time 2/2 
  c2 e d g \grace {g8} f4 e8 f g4 f e d c2 r2 c~ c
 }
 