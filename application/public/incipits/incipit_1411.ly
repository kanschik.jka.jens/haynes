\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "1. Air" 
    }
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo ""
  \time 4/4
    g'2 e a2. g4
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "2. Air" 
    }
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo ""
  \time 3/4
  g'2 f8 e e4 e e

}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "3. Air" 
    }
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo ""
  \time 6/8
    d8. e16 d8  b'4 g8
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "4. Air" 
    }
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo ""
  \time 2/2
  g'4 c8 r  g4 c8 r
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "5. Air" 
    }
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo ""
  \time 2/2
  g'2. b4
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "6. Air" 
    }
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo ""
  \time 3/4
  d2 g4 g b d
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "7. Air" 
    }
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo ""
  \time 2/4
    g'8. a32 g e8 r16 e

}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "8. Air" 
    }
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo ""
  \time 6/8
  b'8. c16 b8 d4 fis,8
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "9. Air" 
    }
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo ""
  \time 4/4
  d2 \grace g16 fis4 e8 d
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "10. Air" 
    }
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo ""
  \time 3/4
  a'2 b8 g

}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "11. Air" 
    }
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo ""
  \time 2/2
  g'4. a16 g b4 e
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "12. Air" 
    }
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo ""
  \time 2/2
   fis4 a8 r fis4 a8 r
}
