\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.bez."
  \time 3/4
      bes2. a2 r4
      bes4 c d  es4. f8 d4 c2 c4
}
