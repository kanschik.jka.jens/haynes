\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef alto
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  r8 c16. bes32 a16. bes32 c16. d32 e,8 f16. d32 c4~
  c16. d32 e16. f32 g8. a64 bes a g f8. d16 c4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro ma non troppo"
  \time 3/4
  r8 c d c16 bes c8 f
  \grace f4 e2.
  r8 f f e16 f g8 f
  e8. d16 c4 f
  }

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Moderato"
  \time 2/2
  c4 c2 d4
  \grace c4 bes2 a4 f f bes2 a4 g2 f
  }