\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 2/2
  \partial 8
  c8
  f4 f f16 a g f e8[ g]
  g a4 bes8
}
