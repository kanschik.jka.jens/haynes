\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Letzte Stunde, brich herein [BWV 31/8]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\major
   \tempo "o. Bez."
   \time 3/4
   r4 c8 g a f
   e g c g a f
   e g a g a b
   c g a g a b
   c b c d e d
 
   
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key c\major
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
  R2.*15 r4 c8 g a f
  e2 f4 
  r4 c'8 g a f 
  e2 r4
  
}



