 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key fis\minor
   \tempo "1. Largo Cantabile"
  \time 3/4
    cis4 d8. eis16 fis gis a d,
    bis4 cis r8 b
    a16 cis fis a a gis b8~ 
    \times 2/3 { b16[ a gis]} 
    \grace b16 \times 2/3 { a16[ gis fis]} eis4 fis r8
}


\relative c'' {
  \clef treble
  \key fis\minor	
   \tempo "2. Allegro non troppo"
  \time 4/4
  fis4. cis'8 \grace cis4 d4. fis,8
  b b4 a16 gis \times 2/3 { a16[ b cis]} cis4 fis,8
  b8 b4 a16 gis a8 gis r
}

\relative c'' {
  \clef treble
  \key fis\minor	
   \tempo "3. Allegretto"
  \time 3/4
  cis'4 d cis
  \grace cis4 b2 a4
  gis4. a16 b a8 gis
  \grace gis4 a2 gis4
}

