 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Affetuoso alla Siciliana"
  \time 12/8
  r2. e4 b'8 g8. fis16 e8
  c'4. b8 r r b8. c16 b8 e16 b a g fis e
}
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro ma moderato assai"
  \time 4/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      \partial 8
      b'8
      e b r16 b fis a \times 2/3 { g16 fis e} e8 r
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Vivace"
  \time 3/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      b'4 b~ b8. c16
      c2 b4
      r8 a g4 fis fis2 g4
}
