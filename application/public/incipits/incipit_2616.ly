\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 12/8
  g8 d g b g b d b16 c d8 d4.
  g8 d g g d g a fis16 g a8 a
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Grave"
  \time 3/4
  r4 b g
  d' c4. b16 c 
  b2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 4/4
    r8 g g g d' d d e16 fis
    g8 d g fis16 g a8 d, a' g16 a
    b8
}
