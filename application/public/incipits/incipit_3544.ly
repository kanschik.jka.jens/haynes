\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro assai"
  \time 4/4
    f,8 a16 g f8 e
    d f16 e d8 c
    bes d'16 c bes8 a g f e d c4
}
