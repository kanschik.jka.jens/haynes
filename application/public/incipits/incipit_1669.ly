\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. o.bez."
  \time 3/4
    c2 d4
    \grace d8 es2 d4
    es d4. es8
    c b c es d c
    b2 g4
}
