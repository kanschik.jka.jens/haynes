 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
    d8 r d r d r d r
    b g g'4~ g8 d g4~
    g8 fis16 g a g fis e fis8 d r
}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Larghetto"
  \time 3/4
  b4 e2 e4 dis fis
  fis b8 a g fis g4. fis8 g a
  fis4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 4/4
  r2 r8 d b c
  d4 r r2
  r8 g g fis fis e e d
  d e e d d c c b b4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Entree"
  \time 2/2
  \partial 8
  d8
  d8.[ b16] c d e fis g4. g,8
  g[ a16 b] c d e fis g4. d8
  e4. g,8 fis4. c'8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Loure. Cantabile"
  \time 6/4
  \partial 4
  g4 d'4. e8 d4 d4. e8 fis4
  g2. g,2 d'4
  e4. fis8 g4 \grace d8 c2.
  b b2 b4
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "6. Trio. Tempo di Menuet"
  \time 3/8
   d'8 c b
   a4 b8
   c b a b8. c16 b c
   d8 c b
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "7. Air Italienne. Allegro"
  \time 2/2
  \partial 8
  d8
   b4 \times 2/3 { c8.[ d16 e8] } d4. c8
   b8 g  \times 2/3 { c8.[ d16 e8] }  d4. g8 
}
