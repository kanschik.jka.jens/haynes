\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  d4 d2 e16 fis g e d4 d2 e16 fis g e
  d8 d d d   c b a g a4. b16 c b4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Rondo. dolce"
  \time 2/4
  \partial 4
  d8 d
  d4~ d16 e c e
  b8 b b b
  \grace c16 b8 a16 g a8 b
  d c c c
  c4
}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
  g4 g8 c c2 b8 a
  g c c2 b8 a
  \grace a8 g4 f8 e d f e a g f f2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Minuetto"
  \time 3/4
  g'8. a32 g f4 e
  d e f g8 gis a f d a c2 b4
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  \times 2/3 {d8[ es f] } bes,2 a4
  \times 2/3 {bes8[ c d] } d2 es8 e
  f d bes bes'
  bes a a g g f f f f4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Minuetto"
  \time 3/4
  f2 es16 d c bes
  a4 bes c
  d8 bes a bes g' f
  f4. es8 d4
}
