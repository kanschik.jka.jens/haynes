\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
    bes4. c8  d8. es16 f8. g16
    a,4. bes8 c8. d16 es8. f16
    g4. f8 g8. g16 a8. bes16
    a4. g8 f4. es8
}
