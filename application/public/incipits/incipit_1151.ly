 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 4/4
    cis4. d8 \grace cis8 b4. cis8
    \grace b8 a4 fis a cis
    \grace cis8 d4. e8
    \grace d8 cis4. d8
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Gavotte en Rondeau"
  \time 4/4
    \partial 2
    e4 \grace { fis16 gis} a4
    \grace a8 gis4 a8 gis a gis fis e
    a2
}

