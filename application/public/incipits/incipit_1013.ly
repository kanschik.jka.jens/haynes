\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 4/4
  r8 bes es f
  d4 r8 es
  c d16 es f es d c d8 bes es4~
  es d es8 g g g
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Largo [Tutti]"
  \time 3/2
  d2 d d d c r
  es es es es d r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Largo [Solo]"
  \time 3/2
   \set Score.skipBars = ##t
   R1.*7
   bes1.~ bes~
   bes4 c8 bes a4 bes8 c f,4 c'
   d es8 f bes,1
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro"
  \time 6/8
  es16 d es f es f d8 es r
  g f16 g f es d8 bes' bes,
  es16 d es f es f d8 es r
  d c16 bes c a bes8 r r
}