 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "o. Bez."
  \time 4/4
  f,4 bes2 a8 f g4 c2 bes8 g
  a4 d2 c8 bes a c
}
