 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Ouverture"
  \time 2/2
    e4. e8 g g b b
    dis,4 fis b,4. c8
    a4. a8 a4 g8 fis
    g4 \grace fis8 e4
}
 
\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. 1. Rondeau. Gracieusement"
  \time 3/8
  r8 e g
  a, dis16 e fis8
  g, c16 b a g fis8 b4
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "4. La Mariniere. Marque"
  \time 6/8
  e4 dis8 e4 fis8
  g4. fis
  b,4 a8 g4 fis8 e4
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "5. Sarabande"
  \time 3/4
  b'4 b,4. e8
  dis4. c8 b4 a g4. fis8
  \grace fis8 g2 \grace fis8 e4
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "5. Rigaudon"
  \time 2/2
  \partial 4
  b4 g e g b
  e2 g
  fis4 e fis dis e fis8 g a4
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "7. Gavotte"
  \time 2/2
  \partial 2
   e4 fis g b, c b dis e a, c
   b a8 g fis4 b
   g \grace fis8 e4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "9. Menuet"
  \time 3/4
  e8 g fis e fis dis e4 e,8 fis g4 fis b2
}
