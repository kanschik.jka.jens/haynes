\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Entrée"
  \time 6/8
\partial 2
  c8 c d es
  d4 g,8  c d es
  d4 g8 es d c
  d4 g8 es d c
}
