\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
  bes16[ c d es] f8 f g f f f
  bes f f f  d[ c16 d] bes8 c
  d[ f,16 g] f8[ d']
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 4/4
  r2 g'4. g,8
  es'4 d bes8. a16 g bes a bes c4 bes a r
}
  
  \relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Presto"
  \time 4/4
  bes4 a bes
  c f, d'8 es
  f4 es8 d c bes g'2 
  }
