\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. [Vivace]"
  \time 6/8
  d,8 d d d d32 e fis g a8
  d,8 d d d d32 e fis g a8
  d,8 d d d r r
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Andante"
  \time 4/4
        << {
      % Voice "1"
      r1 r4 a~ a4. a8
      d4 r8 d e g e b'
         } \\ {
      % Voice "2"
      a,8 a a a  a a a a
      a a a a    a a a a
      b b d d d d r d
      } >>
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 4/4
  fis4 e a8 fis g e
  fis4 e a8 fis g e
  fis e e e   e e e e
  fis e e e   e e e e
}
  
