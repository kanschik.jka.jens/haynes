 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  c,2. c'4
  c16 b d c b2.
  g'8 f f2 e8 d
  d16 c e d c4 r2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 2/4
  \partial 8
  g8
  d'16 e fis g g fis e d
  d c c4 b8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondeau"
  \time 3/4
  g'2 f4
  g8 f e d c b
  c4 f e
  \grace e8 d4 c8 b a g
}
