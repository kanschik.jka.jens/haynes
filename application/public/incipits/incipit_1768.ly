 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "5. Air"
  \time 3/8
  \partial 8
   e8 
   f d e c c f
   e a b gis4 e8
}
