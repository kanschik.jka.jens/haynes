\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Gott schickt uns Mahanaim zu [BWV 19/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\major
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                      r4 r8 d8 b a b g
                      e'8. f32 g e4 d8 g, c4
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\major
                     \set Score.skipBars = ##t
                        % Voice 2
                     \set Score.skipBars = ##t
                     R1*2 r2 r4 r8 g8
                     fis e fis d b'8. c32 d b4 a8 d g4.
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key g\major
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
     R1*13 r4 r8 d8 b a b g
     e'8. f32 g e4 d8

}