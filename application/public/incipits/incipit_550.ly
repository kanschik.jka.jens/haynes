\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/2
    bes4 d f r8 bes
    a bes bes, bes'  a bes bes, bes'
    a g f es d16 c bes8 r4
    r2 r4 r8 f
}
