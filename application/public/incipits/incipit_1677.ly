
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Prima"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
\partial 8.
d16 e fis
g8g, g16 a b c 
d8 d~ d c16 b 
\times 2/3 {c16 d e} d8~ d fis16 g
a g fis e d c b a b a g8
 
}

\relative c'' {
\clef "treble"   
  \key g\major
   \tempo "2. Adagio"
  \time 2/4
g8 b b8. a32 g
fis8 c' c8. b32 a
b8 d d8. c32 b
a16 b a g \times 2/3 {fis16 e d} d8
g8 b

 
}

\relative c'' {
\clef "treble"   
  \key g\major
   \tempo "3. Allegro"
  \time 3/8
g4 a8
\times 2/3 {b16[ a g]} \times 2/3 {d'16[ c b]} g'16 b
d,4 c8
\times 2/3 {b16 a g} g4

 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Seconda "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
\partial 8
a8 a4 g
fis16 g a8 r8 d,8
e16 cis g' e b' g fis e
fis g a8 r8


 
}

\relative c'' {  
\clef treble
  \key d\major
   \tempo "2. Andante"
  \time 2/4
\partial 8
g8 d' d~ d16 cis b a 
g fis \grace fis8 g4 d'8
e16[ e32 fis] g16 fis32 e d8 cis
cis b r8


 
}

\relative c'' {  
\clef treble
  \key d\major
   \tempo "3. Presto"
  \time 6/8
\partial 8
a8 d fis e d fis e 
d a fis d4 d'8 fis a g fis a g
fis d a fis4



 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Terza "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 2/4
a16 a' a8 cis, b
a16 a' a8 cis,8 b \grace b8 a4 r8 e'8
fis16 gis a a fis gis a a e8 a, r8

 
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio"
  \time 2/4
\partial 8
a8 e'[ e  e a]
\times 2/3 {gis16 a b} e,8 r8 e8
\times 2/3 {d16[ e f]} \times 2/3 {b,16[ c d]} e,16 f' e d
\times 2/3 {c16[ b a]} a8 r8

 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Minuetto"
  \time 3/4
a4 a'2
\times 2/3 {gis8 fis e} e2
b4 cis d
\times 2/3 {cis8 b a} a2 
b8[ gis] d'[ b] fis' d
\times 2/3 {cis8 b a} a2


 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Quarta "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
g8 \times 2/3 {b16 a g} d8 c
b \times 2/3 {b'16 a g} b,8 a
g \times 2/3 {b'16 a g} d8 c
b8 \times 2/3 {b'16 a g} b,8 a g4 r8

 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 3/4
c8. e16 \grace e8 d8 c16 b \grace b8 c4
e8. g16 \grace g8 f8 e16 d \grace d8 e4
g8. a16 g8 f e d 
\grace f8 e8 d c4 r4
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 3/8
g8 g16 fis g d
b'8 b16 a b g
d'8 d16 c e b
g'8 g, g
\times 2/3 {e'16 d c} g'8 g
\times 2/3 {d16 c b} g'8 g
 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Quinta "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
d4 fis8. g16
a8 d4 cis16 b
a8[ g fis e]
fis16 e d8~ d cis16 b
a8[ g fis e]
fis16 e d8 r4

 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 3/4
d4 g, \times 2/3 {c8 d es}
fis,4 g r4
\times 2/3 {c8 d es} es8 d c bes
r8 a8 bes c bes a
\times 2/3 {bes8 c d} d8 c bes a 
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Minuetto"
  \time 3/4
d4 \times 2/3 {a'8[ g fis]} \times 2/3 {e8[ d cis]}
\grace cis8 d2 a4
b4 \times 2/3 {d8[ cis b]} \times 2/3 {d8[ cis b]}
\grace b8 a2 fis4
 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Sesta "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
\partial 8.
d16 e fis
g8[ g, g g]
g'16 fis e d c b a g
g'8[ g, g g]
g'16 fis e d c b a g
g'8[ g, g g]


 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Adagio. Ma non tanto"
  \time 12/8
\partial 8
b8 g8. fis16 e8 b'8. c16 e,8 e4 dis8 dis4 e8
fis8. g16 a8 c b a g fis e e4 

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Aria. Moderato"
  \time 2/4
g4 b16 a b c
d b g4 e'8
e d4 c8
b16 a g4 e'8
e d4 c8 
b16 a g8 r8



 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "4. Capricio"
  \time 3/4
g8 b4 a g16 fis
g8 b4 a g16 fis
g8[ b a c]~ c b16 a
b8 g d b g4

 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "5. Minuetto"
  \time 3/8
g4. d
\times 2/3 {a'16[ b c]} b8 a
g16 fis \grace fis8 g4
\times 2/3 {a,16[ b c]} b8 a
g16 fis \grace fis8 g4
 
}




