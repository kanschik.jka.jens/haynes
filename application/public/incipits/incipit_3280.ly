 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/4
    \grace c8 bes8 a16 bes f8 d'
    \grace es8 d8 c es4
    \grace es8 d8 c16 bes g8 a
    \grace a8 bes4 \grace f'8 es4
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. o.Bez."
  \time 4/4
  g8 bes16. d32 g8 bes a16 cis, d4.
  f,8 a16. cis32 d8 a' g16 b, \grace b8 c4.
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Allegro ma non tanto"
  \time 3/4
  bes4. f8 bes d
  d c c2
  \grace cis8 d8 f es d c bes
  \grace bes8 a2 bes4
}
