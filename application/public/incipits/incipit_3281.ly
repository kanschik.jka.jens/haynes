\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Affettuoso [Oboe]"
  \time 3/4
  c4~ c8 b16 c  d d e f
  \grace f8 e4~ e8 d16 e f f g a
  a fis g4 g8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Affettuoso [Soprano]"
  \time 3/4
    b4 b4. \times 2/3 { e16[ d c]} 
    c4 b r
    e8. d32 e f8 d b a
    a g r4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Tutti]"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Solo]"
  \time 4/4
   \set Score.skipBars = ##t

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 4/4

}