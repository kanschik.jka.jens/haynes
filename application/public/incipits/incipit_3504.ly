 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
    g'4 g g4. fis16 e
    d8 b c d
    g,4 r
    g g g4. fis16 e
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Andante"
  \time 3/4
  d4 es d c d r
  bes c bes bes a r
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Allegro"
  \time 2/2
  g'2 g4 g g2 b4 a
  g2 b4 a g fis8 e d4 g
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}