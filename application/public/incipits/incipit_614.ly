
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
  \tempo "1. Vivement"
  \time 3/4
  g8 c, c c c a'
 g c, c c c a'
 g c b a g f
 e d16 e c g a b c d e f 
 g8
 
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement"
  \time 2/2
 \partial 2
  e4 \grace d16 c4
 f4. e8 f e d c
 b4 c8 d e4 c
 d4. c8 b c d b
 c4 g
 
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gayment"
  \time 2/4
 c8 [g g e'] d16 c b a g4 d'8 [g, g f'] e4 d 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gayment"
  \time 2/4
 g4 c b8 [a g f']
 e4 d c g e' g
 e8 [d c g'] f4 e d2
 
 
 
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Gracieusement"
  \time 3/4
 \set Score.skipBars = ##t
   R2.*1  
  g4 c8 b c d
 es4 d4. c8 b2 b4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Legerement"
  \time 3/8
 c16 b c d c8 g c d 
 e d16 c d b c4 
 
 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Fuerement"
  \time 2/4
 c4 r16 d e f g8 c, r8 g'8 a [a g f] e4 d c 
  
 
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement"
  \time 3/4
\set Score.skipBars = ##t
   R2.*1 
 g4 b8 a b c a4 b8 c d b c2 g4
  
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Gayment"
  \time 6/8
 c4. e d8 c d d b g d'4. f
 e8 d e c e f g4 
 
 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto IV "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o. Bez."
  \time 3/8
 c16 d e8 d c g c d16 e f8 g
 e4 d8
 
 
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Gracieusement"
  \time 6/8
 \partial 4.
 c4 d8 e d c b a b c4 g8 c d e b a b c d8. c16 b4. 
 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Tambourin"
  \time 2/4
 g8 f16 e d8 e c4 g c8 [g d' g,] e'4
 
 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Gracieusement"
  \time 3/4
 g4 a fis g fis8 e b4 e d c b2 \grace a16 g4 g8 fis g a b4 
 
 
 
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Tendrement"
  \time 6/8
  r4. r4 b8 b a g d' c d b g b c d e d g fis e4 d8 d4
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gayment"
  \time 1/2
  g8. a16 b8. c16 d4 g,8. d'16 e4 fis
 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Gayment"
  \time 2/4
 g4 fis g4. fis16 e d8 [c b a] g8. a16 b c b c d8 g,
 
 
 
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Gracieusement"
  \time 2/2
  \set Score.skipBars = ##t
   R1*1 r2 g4 d b4. a8 g [fis g a] b4 c8 b a4. g8 fis2
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Gayment"
  \time 3/8
  g8 b, d g, b d g a fis g16 fis g a g8
 
 
}
