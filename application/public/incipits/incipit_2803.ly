\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Violin]"
  \time 4/4
  c4 r16 d e f g8 g g a16 b
  c8 c, r16 c'16 b a b b a g a a g f
  g g f e f f e d e8 c g' g,
  c

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Oboe]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*3 
    c16 d e f g a g a g8 c, a'16 g a f
    g f g e  f e f d e d e c a' g a f
    g f g e f e f d  e d c d e f g f
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Largo"
  \time 4/4
      << {
      % Voice "1"
      e8 e e f16 e d8 d d e16 d
      c8 c c d16 c b2
      g'8 g g a16 g f8 f f g16 f
         } \\ {
      % Voice "2"
      c8 c c d16 c b8 b b c16 b
      a8 a a b16 a gis2
      e'8 e e f16 e d8 d d e16 d
      } >>
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto"
  \time 4/4
  g'4 a g8 e a f
  g e a f  g e f d
  e g f e d f e d
  e g f e d f e d
  e fis g a fis4. g8
  g

}
