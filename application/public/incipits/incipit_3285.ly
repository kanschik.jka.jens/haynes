\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante"
  \time 2/4
    f8. d16 bes8. f'16
    f es d c c4
    bes g'8 f
    \grace f8 es4 d
}
