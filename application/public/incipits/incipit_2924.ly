\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/2
  f2 a c d8 c bes a
  \grace a8 g4 g g g
  g4. c8 d c bes a
  a g  bes g g f f e 
  f2 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andante grazioso"
  \time 6/8
  \partial 8
  g8
  c8. d16 e8 e r16 d32[ e]  f e d c
  d8. e16 f8 f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Menuetto. Allegretto"
  \time 3/4
  \partial 4
  c4
  f f f8 a
  c4 c a a8 g g f g e
  f4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Adagio con espressione"
  \time 2/4
  f16..[ d64] bes'8 r32 bes[ a bes]  c bes a as
  fis16 g g8 r16 g g ges
  g f f8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. Menuetto. Poco Allegretto"
  \time 3/4
  a'8 a bes bes c c
  d,4 bes'8 a g f
  f e d c d e
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "6. Rondo. Allegretto"
  \time 2/4	
  a'8 a c c
  f,4. a8
  a16 g g fis g8 e
  f a c4
}