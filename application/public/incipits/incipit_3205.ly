\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Weichet nur, betrübte Schatten [BWV 202/1]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key g\major
   \tempo "Adagio"
   \time 4/4 
   \set Score.skipBars = ##t
   R1*2 r2 d2~
   ~
   d8. c16 b a g32 fis g16 g8. fis16 e32 fis g a b c a b
   c8~ c32 b a b 
                                                                                   

}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key g\major
   \tempo "Adagio"
   \time 4/4
   \set Score.skipBars = ##t
  R1*4 r2
  d4~ d16 c32 b a16 g
  g4 r8 g8 es'4~ es16 d cis d
  cis8 b16 a r8
       
}



