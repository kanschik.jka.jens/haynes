 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 3/4
      \partial 4
      a4
      b2 cis4 gis2 gis4
      e'4. fis8 e4
      d4. e8 cis4
      cis b2 a
}
 