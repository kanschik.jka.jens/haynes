\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
  g8 b4 c8 d g4 e8
  c4 b
  g8 b4 c8 d g4 e8
  c4 b
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Siciliana"
  \time 6/8
  d8. e16 d8 d8. b16 g8
  d'8. e16 d8 d8. b16 g8
  g'8. fis16 e8 d8. e16 c8
  b8. g16 d'8 a4 d,8
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
  g8 a16 b c d e fis g8 fis16 e
  d8 c b4 a
  g16 a b a g8 d' b g
  e' d d c c b
}
