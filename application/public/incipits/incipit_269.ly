\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Vivace"
  \time 4/4
  d4 fis16 d cis e \grace e8 d4 fis16 d cis e
  \grace e8 d8. e16 fis fis gis a a8 a, r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Andantino"
  \time 3/4
  r8 g' fis g a bes
  d, d' c bes a g
  g4 fis8 g a c
  bes g fis g a fis
  g4 r
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prélude. Adagio"
  \time 2/4
  c8 g' \grace f8 e \grace d8 c8
  \grace b8 a4 \grace c8 b8 \grace a8 g8
  c8. d16 e c b c
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Cantabile"
  \time 3/4
  e8 g16 r b,8 e16 r g,8 b16 r
  \grace {e16[ fis]} g2 r4
  fis8 a16 r dis,8 fis16 r b,8 dis16 r
  \grace {fis16[ g]} a2 r4
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Adagio"
  \time 4/4
  a8 cis16 e \grace dis8 e a
  a \grace {gis16[ a]} gis8 r a
  fis32 d16. e32 cis16. d32 b16. e32 d16. 
  \times 2/3 { cis16[ b a] } a r16
}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Amabile"
  \time 2/2
  f4. e16 d c4 d
  d4. e16 f c4 r
  bes8 a g f e4 \grace {d16[ e]} f4
}

