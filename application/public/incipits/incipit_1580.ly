\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo ""
  \time 4/4
  r4 r8 c a8. g16 f4
  c' r16 c d e f8 f16 g e8. d16
  c bes a g  f a bes c  d8 d c8. bes16 a8 f' d g e16
}
