\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Der Ewigkeit saphirnes Haus [BWV 198/8]" 
    }
    \vspace #1.5
}
 
 
\relative c''' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Traverso"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key e\minor
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                    g8. e16 b4~ b16 e fis g
                    fis e fis b b,4~ b16 b c d
                    e g fis e a b a g fis e dis e dis4 r4
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                   b2.~b~b8 dis e  c a4~
                   ~a8 fis dis c' b a
                   g4 r4
                      
                  }
                  
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Viola da Gamba "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 3
                  e'8[ e, e e'] e d16 c
                  d8[ d, d d'] d c16 b
                  c8[ c, c c'] c b16 a
                  b8[ b, b b']
                      
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key e\minor
   \tempo "o. Bez."
  \time 3/4
     \set Score.skipBars = ##t
   R2.*20 r4 r4 b4
   b2.~b~b8 dis e c a4~
   ~a8 fis'8 fis16 g fis e dis c b a
   g4 r4
   

}