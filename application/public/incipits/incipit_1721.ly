\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  g'8 d b d b g r4
  g'8 d16 c b8 d e16 fis g d c8. d16
  b
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 3/4
  g4 b16 c b8 d16 e d8
  g4 d16 e d8 b16 c b8
  g4 b16 c b8 d16 e d8
  g4 d16 e d8 b16 c b8
  g4 d' d d c c
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Adagio"
  \time 4/4
  g'8 g g g fis4 r
  fis8 fis fis fis e4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 2/4
  \partial 8
  d8
  g b,16 c d8 e 
  d b16 c d8 e
  d b c a 
  b a16 b g8
}