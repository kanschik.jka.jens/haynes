 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Vivace"
  \time 2/4
    f8 f f g16 a bes8 d, d es16 f
    g8 bes, f' bes, es d16 c d8 c16 bes c8 f, r
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Siciliana"
  \time 6/8
  \partial 8
  d8
  g,16 a bes a g8  g4 es'8
  a,16 bes c bes a8 a4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Allegro"
  \time 12/8
   f4 bes,8 g'4 bes,8  f'4. es
   d8 es d  c d bes a bes g f4.
}

