 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 3/8
  bes16 d f8 es
  d c4
  bes8 a bes c16 d es8 r
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegretto"
  \time 4/4
  \partial 8
  f32 es d c
  bes8 bes4. c16 d es4.
  \times 2/3 { d16[ c bes] } \times 2/3 { f'16[ es d] }
  bes'8 g e f \grace { c16[ d]} es4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Vivace"
  \time 3/8
  f,8 d'32 bes16. es32 c16.
  d32 bes16. c4
  d16 f, g bes f bes
  \grace f8 es4 d8
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Menuett"
  \time 3/4
  \partial 8
  
}