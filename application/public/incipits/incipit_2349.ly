\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 2/4
    f4 c8. a16
    d bes g8 r4
    bes16 g d4 e8 e4 f8 r
}
