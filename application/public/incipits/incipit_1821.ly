\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4
  d4~ d32 e d c  b a g fis g16. fis32 g8 r16 e' d16. c32
  d16 g, g' e c8. b32 c b4 
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/8
  d8 b16 c d8 g,16 fis g a b c
  a g a b c d
  b4 g8
}
