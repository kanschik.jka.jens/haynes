 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Andante"
  \time 4/4
  \partial 4
    r8 g8
    d' d4 c32 d es16 d8 d4 c32 d es16
    d8 es d c bes a16 bes g8
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Largo e dolce"
  \time 6/8
  bes8. d16 f8 \grace as8 g8 f r
  \grace bes,16 a32 g a16~ a32 bes c16~ c32 d es16
  \grace es16 d32 c bes16 bes4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Presto"
  \time 2/2
  g2 bes a4 es' d g, fis c' bes g a d, r
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. bar 84"
  \time 12/16
  \partial 16
  d,16
  g bes a~ a es d~
  d c' bes~ bes f e~
  e d'c~ c g fis~
  fis es' d r
}