\version "2.16.1"

#(set-global-staff-size 14)



\relative c''' {
<<
\new Staff { \clef "treble"   
                     \key g\major
                      \tempo "Air gai "
                      \time 4/4
                      % Voice 1
                      \partial 4
                      g8 fis
                      e2 e8 d e fis
                      g2 d4 c8 b
                      e4 \grace d8 c4
                      e g
                      d2~d8 e b d
                      c d a c b c g b
                      a4. d,8
                  }
\new Staff { \clef "treble" 
                     \key g\major
                        % Voice 2
                        \partial 4
                        b'8 a 
                        g2 c 
                        b b'4 a8 g
                        g4 e \grace d8 c4 e
                        b2~ b8 c g b
                        a2
                        d,4 d'8 g
                        fis2 r4
                  }
>>
}
