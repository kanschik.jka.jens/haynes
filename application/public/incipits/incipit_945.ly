\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite I] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Ouverture"
  \time 2/2
  f4. f8 d4 f
  bes,4. c8 d4. c16 bes g'4. c,8 c4 a
  f'4. bes,8 d4 bes
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite II] " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Ouverture"
  \time 2/2
  g,4. g8 g4 g
  c4. c8 c4. d16 e
  d4. d8 d4 d
  g4. e8 e4 g
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "]Suite III] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Ouverture"
  \time 2/2
  c2. r8 c
  f4. f8 a,4 bes8 c
  d4. d8 c4 d
  bes4. c8 d4. d8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite IV] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Ouverture"
  \time 2/2
   c4 a   a'4. a8
   a2 r8 e8 d c
   d4. d8 d4. e8
   c4 a
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite V] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Ouverture"
  \time 2/2
  d4. e8 d4. c8
  b4. b8 d4 d
  c4. c8 b4 c
  a4. a8 a4. a8
  g4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite VI] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Ouverture"
  \time 4/4
  d4. c8 c4 d
  bes g g'4. a8
  fis2 fis8 fis
  g4 bes a g
}
