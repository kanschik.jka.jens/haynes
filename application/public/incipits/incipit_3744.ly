\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Andante"
  \time 2/2
    f4 e8. f32 g  f4 a16 cis,8.
    d8 a4 g' \grace g16 f8 e r
}




\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  a'8
  d,2~ d
  cis8 g' f e
  \grace e8 d8 cis r
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Vivace"
  \time 3/8
    a8 d cis d8. e32 f e8
}
