\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt + Tenor): Wie selig sind doch die, die Gott im Munde tragen [BWV 80/7]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe da Caccia"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\major
                      \tempo "Duetto"
                      \time 3/4
                      % Voice 1
                   r4 r4 d4 e4. d8 e f
                   d4 g2~
                   ~g8 e fis d b' g
                   \grace fis8 e4
                 
           
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\major
                     \set Score.skipBars = ##t
                     % Voice 2
                       R2. r4 r4 g4 b4. a8 b c
                       a4 d2~ 
                       d8 b cis a fis' d
                       b4
                       
                
                  }
                  
                  
>>
}


\relative c' {
	<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Alt"
                     \key g\major
                      \tempo "Duetto"
                      \time 3/4
                      % Voice 1
                  \set Score.skipBars = ##t
                    R2.*16 r4 r4 d4 e4. d8 e fis
                    d2 r4
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Tenor"
                     \key g\major
                     \set Score.skipBars = ##t
                        % Voice 2
                R2.*16 r4 r4 b'4 c4. b8 c d
                b2 r4
                        
                
                  }
>>
}

	
	
