\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Stumme Seufzer, stille Klagen [BWV 199/2]" 
    }
    \vspace #1.5
}
\transpose c d{
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\minor
   \tempo "o. Bez."
   \time 4/4
   r4 g8 es'16 d b8 as'16 g g f32 es d16 es32 c
   b16 d g,8 g16 bes des c
   as f f'8~ f16 as, c bes
   g bes es,8 
   
 }    
   
}

\transpose c d{
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key c\minor
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
   R1*7 r4 g8 c, as'16 g c b c8. d16
   es d d8 r8
}
}


