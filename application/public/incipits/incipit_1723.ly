\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Vivace"
  \time 4/4
  c8 g a b  c c, r d'
  e f16 e d e f g  e8 c r g'
  c, d16 e f8 e16 f  d8 e16 f g8 f16 g
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio"
  \time 4/4
  e4. d16 c b4. c8 d e f e16 d  c8 a16 gis a8 b16 a
  gis8 a e4 r8 a8 a g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Vivace"
  \time 3/4
  c8 d e f g a
  g4 c, a' g f4. g8
  e2 d4
}
