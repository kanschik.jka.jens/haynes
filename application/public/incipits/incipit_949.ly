 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Ouverture"
  \time 2/2
    c4. g'8 es4 f8 g
    b,4 g c d8 es
    d4 g
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo ""
  \time 3/4
  r8 g' f g d f
  es4 e4. d16 e
  f8 f es f c es
  d g f g d f
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Entree"
  \time 2/2
  \partial 8
  f8
  es2 es4. es16 f
  g4 c,2 as'8 g
  f es d c b4. c8
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Menuet"
  \time 3/4
  g'4 es c g'2 b,4
  c g es'
  d8 c d es d4
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Gigue"
  \time 6/4
  \partial 4
    g4
    d'2 es4 d2 g4
    es4. f8 g4
    es4. d8 c4
    g'2.
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "5. Gigue"
  \time 6/4
    c2. c,2 es'4
    g2 es4 c2 g4
    c4. d8 es4 es f8 es d c
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "6. Gavotte"
  \time 2/2
    g'4. b,8 c4 d
    es4. f8 g4 as
}