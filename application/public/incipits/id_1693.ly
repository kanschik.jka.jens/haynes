\version "2.16.1"

#(set-global-staff-size 14)

\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto 1 (d-moll) " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro [Solo]"
  \time 2/2
  \partial 8
   \set Score.skipBars = ##t
   r8 R1*47 r2 r4 r8 a d4. a8f'4. e8 \grace f8 e8 d d d d4 r8
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Gracioso [Solo]"
  \time 6/8
   \set Score.skipBars = ##t
   R4.*8 f4 f8 \grace {f16 g} a8. g16 f8 f4. e4 d8 c g' e c d bes bes4. a4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Rondo.Allegro [Solo]"
  \time 2/4
\partial 8
a'8 fis[ fis fis a] e[ e e a] d,[ d d g] fis4 e8 e b[ b b g'] e[ e e b'] a4 cis, d r4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto 2 (g-moll) " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \partial 16
   \set Score.skipBars = ##t
   r16 R1*63 r2 r4 r8. d16 bes'4 r8. d,16 fis4 r8. d16 g4 r4 r8 d g f \grace f8 es4 d8 c c4
  
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [Solo]"
  \time 2/2
  \partial 2
  f8 bes d, f bes, f d' bes f' es d c bes r8 bes'bes g4 \grace c8 bes8 a16 g f4
  
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Rondo Allegro [Solo]"
  \time 6/8
  \partial 4
  bes'8 a g4. fis8 g a d,4.~d8 bes' a g4. a8 bes c d4.~d8 d c b4. c8 d es fis,4.
  
}
  \pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto 3 (C-Dur) " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key C\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \partial 4.
   \set Score.skipBars = ##t
   r8*3 R1*82 r2 r8 g'8 a b c d e4 r8 e,8 f g a b c4 r8 c8 b a \grace a8 g4 f8 e d4 
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio [Solo]"
  \time 2/4
  \partial 8
  \set Score.skipBars = ##t
  r8 R2*7 r4 r8 c8 c4 a'8. f16 \tuplet 3/2 { e[ d' c] } c4 \grace c8 bes16 a32 g f8. fis16 g8. a32 bes a16 bes b c 
       
}

\relative c'' {
  \clef treble
  \key C\major
   \tempo "3. Allegretto [Solo]"
  \time 6/8
  \partial 4
  e8 f a g g r8 b b c4 g8 r8 e8 f a g g r8 b8 b c4 g8 c b c b4. a
  
}

  \pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto 4 (B-Dur) " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 16
   bes16 bes4 r8. c16 c4 r8. d16 d4
  
}

\relative c'' {
  \clef treble
  
   \tempo "2. Adagio"
  \time 4/4
  
}

\relative c'' {
  \clef treble

   \tempo "3. Rondo.Allegro "
  

}

  \pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto 5 (C-Dur) " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key C\major
   \tempo "1. "
  \time 4/4
   c,4 c8. c16 c4 r4 f4 f8. f16 f4
  
}
    \pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto 6 (F-Dur) " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. "
  \time 3/4
   f,4 r8 f8 a f c'8[ c c c] a f c4
  
}
  
  