\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "1er. Concert " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. La Francoise. Gay"
  \time 2/4
r8 e8 b' a
g fis16 e dis8 e16 fis
b,8 cis16 dis e8 fis16 g
fis8 fis16 g a8 g16 fis 
g8 e

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Les Tourterelles. Tendrement"
  \time 3/4
\partial 4
b4 e4. fis8 dis8. fis16
e8. b16 b4 fis'
g4. a8 fis8. a16
g8. fis16 e4 b'
b4. c8 a4
\grace a16 b2 b,4

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. L'Allemande"
  \time 4/4
\partial 16
e16 e4~ e16 b cis dis e dis e fis e fis g e
fis e fis g fis g a fis g8 fis16 g e g fis e
dis cis b8~ b16 fis' g a b


}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. L'Angloise. Air"
  \time 2/4
r4 r8 e16 dis
e8 fis g16 fis e g
fis8 b, e16 d c b 
c b a c b a g fis
g8 e 


}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "5. La Mantoiiane. croches egales"
  \time 3/4
\partial 8*3
e8 b'[ a]
gis fis16 gis e8 e b'[ a]
gis fis16 gis e8 fis gis[ a] 
b gis16 a b8 cis fis,[ ais] b4.
 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "6. La Genoise. Movement de Gigue"
  \time 6/8
\partial 2
b8 e b e
fis b, fis' g fis e
dis4 b8 e4 dis8
c b a d4 c8
b a g c4 b8
a b16 c d8 g, a fis g4

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "7. Sommeil des Festes de l'Eté. Lentement"
  \time 3/4
b4 c b
e4. fis8 g4
fis a8. g16 fis8. e16
dis4. cis8 b4 
b c d 
a4. b8 c4
g a b a2.
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "8. Le Poitevin. Menuet"
  \time 3/4
b4 b b 
e e e 
fis fis8 gis a4
gis
fis8 gis e4
b b b 
e dis8 cis b a 
gis a a4. gis16 a b2


}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "9. Le Breton. Passepied"
  \time 3/8
\partial 8
e8 g fis e
b' b, e
dis e16 fis g e
fis4 b,8
g' fis e b' b, e
dis e16 fis g e fis4


}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "10. Le Parisien. Air dans le goût des Brunettes. Lentement"
  \time 3/4
e4 b'2
gis4. fis8 gis4
\grace fis16 e4 a4. gis16 a
fis2 fis4 
e b'2
gis4. fis8 gis4\grace fis16 e4 gis4. ais8
\grace a16 b2.


}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "11. L'Auvergnate. Badine. Legerement"
  \time 2/4
\partial 8
b8 e b b e
fis4 r8 fis8
g fis16 g e g fis e
dis8 b4 b8
e b b16 c d e
fis4.

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "12. La Florentine"
  \time 6/8
e16 b e b e b fis' b, fis' b, fis' b,
g' b, g' b, g' b, a' b, a' b, a' b,
b'8 a16 g fis e c' b a g fis e
dis4.

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "13. L'Espagnol. Prelude. Lentement"
  \time 2/4
b4~ b8. b16
b4~ b8. b16
e8. fis16 fis8. e32 fis
g4 fis8. b16
e,8. e16 a16. g32 fis16. e32
dis4


}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "14. La Milanoise. Gay"
  \time 2/4
\partial 8
b8 e e e dis16 e
fis8 fis fis b,
fis' fis fis e16 fis 
g8 g g fis16 g
e8 c'16 b a g fis e
dis4

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "15. La Venitiene. Rondeau. Gay"
  \time 3/4
\partial 4
b4 b2 a4
g2 b4
e2 fis4
dis2 b4
e2 fis4
g fis e 
dis8. e16 e4. dis16 e
fis2

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxiéme Concert " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Prelude. Lentement"
  \time 2/4
r8 r16 c16 es8. g16
\grace es16 d8. d16 d8.[ g16]
c,8. c16 d8.[ f16]
b,8 b16 b8.[ d16]
g,4

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisiéme Concert " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Ouverture"
  \time 4/4
c4. b8 c d e f
e4 c g'4. g8
g4 f8 e d e c d
b4 g d' e f4. f8 f4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatriéme Concert " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. La Melancolique. Lentement"
  \time 4/4
\override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
r2 d4~ d8. es16
\grace d16 c2 es4~ es8. f16
\grace es16 d2 f4~ f8.[ \times 2/5 {as32 g f es d]}
\grace d16 es4~ es8. f16
es4. d16 es c2



}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquiéme Concert" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Marche"
  \time 2/4
\partial 8
d8 d d16 d d8 d16 d
fis8 d16 d d8 d
d d d8. d16
d8. d16 d4
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixiéme Concert " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Rondeau"
  \time 2/2
\partial 2
g4 d
g a8 b a b c a
b4 g b d
g, a8 b c b a g
a4 d, 

}

