\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Grave"
  \time 2/2
  a'4. a8 g c, f8. g16
  e4. f8 d c bes4
  a8 f'4 e8 f d16 c c[ bes bes a]
  a8 c4 b8 c
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/2
  f,8 a16 bes c8 c a f r4
  f8 a16 bes c8 c a f r f'
  d c r f d c r
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Adagio"
  \time 3/2
  r2 r a'
  g1 a2
  f1 a2
  d,1 c2 bes1
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Gavotte"
  \time 2/2
   \partial 2
   a'4 bes 
   c bes8 a g4 a8 bes a4 f
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "5. Aria"
  \time 2/2
   \partial 2
   f4 c'
   as g8 f e4 f8 g
   f4 c as' g8 f
   g4 c, 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "6. Allegro"
  \time 2/2
   \partial 2
    a'8 g f a
    g c, d e f a g f
    e a, bes c d c bes d
    c
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo"
  \time 3/4
        << {
      % Voice "1"
      R2.
      g'4 d8. f16 e8. g16
  f4 f4 f4~
  f e8 d e f g4

         } \\ {
      % Voice "2"
      d4 g,8. bes16 a8. c16 bes4 bes bes
      bes a8 g a b
      c4 c c
      } >>

  
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 2/2
  d4. g8 fis d r g
  es d d c d4 r8 a
  bes b c d es4 r8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 3/2
  f2 d bes
  g' f1
  es2 d2. es4 c2 f f
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Poco Allegro"
  \time 3/8
  g8 bes16 a g8 d' g4 fis4.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Grave"
  \time 3/4
  g'4 es c as' g4. f8
  es4 c c'~
  c b2 c
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Poco Largo - Adagio - Andante"
  \time 2/2
  \partial 16
  g'16
  g4 r16 g f es d4 r16 d es f
  es8 c as'4~ as8 g4 f8
  g4.
}
  
  \relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro"
  \time 2/2
  g8 c c b c g es'4
  d8 c16 b c8 d b4. g'8
  as f f e f f, r
}
  