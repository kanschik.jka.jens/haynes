\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
     bes4. es8 \grace f16 es8 d r4
     f~ f16 g as as,  \grace bes16 as8[ g]
}
  
