 \version "2.16.1"
         #(set-global-staff-size 14)
         
     \relative c'' {
  \clef treble
  \key  bes\major
 \tempo "1. o.bez."
  \time 2/4
    \override TupletNumber #'stencil = ##f
  \override TupletBracket #'stencil = ##f

    g'4. b,8 c4. d8
    es g4 b,8 c4. d8
    bes4 r8 d
    es es es \times 2/3 { c16 d es}
     }
