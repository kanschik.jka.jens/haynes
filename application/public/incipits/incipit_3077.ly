\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Von der Welt verlang ich nichts [BWV 64/7]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key g\major
   \tempo "o. Bez."
   \time 6/8
   r4 r8 d8 g16 fis g8~ g16 fis e d b'8 a4.
   b8 c16 a b8~ b16 e cis8. d16 fis,4 e8
    
   
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key g\major
   \tempo "o. Bez."
   \time 6/8
   \set Score.skipBars = ##t
     R2.*12  r4 r8 d8 g16 fis g8 
     r16 fis16 e d b'8 a c r8
     
}



