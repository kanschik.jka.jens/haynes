 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Aria"
  \time 6/8
      r4. r4 g'8
      c,8. d16 c8 a'8 fis c
      bes8. a16 g8
      bes'4.~
      bes8. a16 g8 f es16 d es8 d4. r8 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Aria. Allegro"
  \time 4/4
    r8 bes16 a bes8 c  f,4 bes
    r8 es16 d es8 c d bes f'4
    r8 bes,16 a bes8 bes g' e f bes,
    a4 g8. f16 f8
}

