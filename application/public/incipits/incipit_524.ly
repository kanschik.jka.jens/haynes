 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'Quel è quel empio core' " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef bass
  \set Staff.instrumentName = #"Basso"
  \key g\minor
   \tempo "o.Bez."
  \time 3/4
    g,4 g fis
    g4. g8 a4 g4 fis
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \key g\minor
                      \tempo "o.Bez."
                      \time 3/4
                      \set Score.skipBars = ##t
                      R2.*7 
                      d4 c4. c8 
                      c4 bes r8 d
                      g fis g4 a8. g16
                      fis4
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 1"
                     \key g\minor
                      \tempo "o.Bez."
                      \time 3/4
                      \set Score.skipBars = ##t
                      R2.*7 
                      % Voice 2
                      g4. a8 fis fis
                      fis4 g8 a d fis,
                      g4 c2
                      d4
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key g\minor
   \tempo "o.Bez."
  \time 3/4
     \set Score.skipBars = ##t
     R2.*16
     r4 r d4
     g, r8 g' f8. g16
     es4 d
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'Povero il mio Giesù' " 
    }
    \vspace #1.5
}
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key c\minor
   \tempo "o.Bez."
  \time 3/4
     es4. d8 c4
     es4 d4. c8
     b2 r4
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'Contro di te mio bene' " 
    }
    \vspace #1.5
}
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"2 Obs unissoni"
  \key c\minor
   \tempo "o.Bez."
  \time 4/4
    g'8 c, d g, c4. c8
    d es f g es8. d16 c4
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key c\minor
   \tempo "o.Bez."
  \time 4/4
  c4 d8 es d4. g8
  es d c4
}