\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio [Tutti]"
  \time 2/4
  c'8. c16 g16. e32 g16. e32
  c8 r r4
  b4 b8 b b r r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*8 
  g'4 g16 a32 g f[ e d r64 e]
  c4. f16 e
  cis16 d  g f  dis e a16. g32
  g8 g32 f e f e8 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegretto"
  \time 3/4
  e8 c4 
   \override TupletNumber #'stencil = ##f
   \times 2/3 { e16 g e } c8 e 
  f8 d4 
   \times 2/3 { f16 g f } d8 f
   e8 e4 f16 g a g f e
   d8 g4 g fis16 f

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Solo]"
  \time 4/4
   \set Score.skipBars = ##t

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 4/4

}