\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
    c8 
    \grace g'8 f4. e16 f \grace g8 f4. e16 f 
    f8 a c4 r c,
}
