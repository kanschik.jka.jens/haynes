\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato"
  \time 2/2
  bes4 bes4. c16[ d] es f g a
  bes8 f f f   f es es es
  c4 c4. d16[ es] f g a bes
}
