\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Andante"
  \time 4/4
      \partial 8
      a'8
      \grace g16 f8 e16 d bes'8 a g16 fis g8 r g
      \grace a16 g8 f16 e a a8 g16 f e d8 r
}

\relative c'' {
  \clef alto
  \key d\minor
   \tempo "2. Allegro"
  \time 3/4
  r8 d, d e f g
  a bes g2~
  g8 a f2~
  f8 d' bes a g f
  e g c,4 e
  f8
  }

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Menuetto"
  \time 3/4
  a2 d4~
  d cis d
  a2 a'4~
  a cis, d
  f2.~
  f4
  }