\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Divertimento I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
    es2 g
    f8 d es4 r bes8 b
    \grace d8 c4 c4. es8 d f
    es g bes,4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Divertimento II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 3/4
    es8. f16 g4 \grace as8 g8 f16 es
    es d f d es4 r
    f8. f16 g8. g16 as8. as16
    \grace as8 g4 f r
}
