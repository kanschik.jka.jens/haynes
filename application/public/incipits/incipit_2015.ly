
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
  \tempo "1. Largo"
  \time 4/4
  r8 d8[ c16 b a g] fis8 g r g
  a a16 b32 c c8 b16. a32 b16 c b c b c32 d c16. b32 a8 d, r4
}


\relative c'' {
  \clef treble
  \key g\major
  \tempo "2. Allegro"
  \time 4/4
    r16 d d d  d d e fis g g, g8 r16 g' g fis32 g
    a16 a g a  d, a' a g32 a b16 g g8 r16
}

\relative c'' {
  \clef treble
  \key e\minor
  \tempo "3. Adagio"
  \time 3/4
  b4 b b fis'2.~ fis4 e8 dis e4.
  fis8 fis4. e8 dis2 e4
}

\relative c'' {
  \clef treble
  \key g\major
  \tempo "4. Allegro"
  \time 12/8
  b8 c16 b a g a8 d, d' b c16 b a g a8 d, d'
  b c16 b a g b8 c16 b a g c8 d16 c b a  c8 d16 c b a
}

\relative c'' {
  \clef treble
  \key g\major
  \tempo "5. Gavotte"
  \time 2/2
  \partial 2
  g'4 fis
  g d b' c8 a
  b4 g  b, c8 a b4 c8 a b4 a8 g fis2
}


\relative c'' {
  \clef treble
  \key g\major
  \tempo "6. Menuet"
  \time 3/4
  g4 d'8 c d4
  g, a b c d8 c b a
  b4 a8 g b4
}
