\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 3/4
      bes8. g16 g4 r
      es'8. bes16 bes4 r
}
