\version "2.16.1"
         #(set-global-staff-size 14)

         
         \relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 2/2
  \partial 8
  a'8
  a4. g8 f4. e8
  f2. c4
  d4. d8 \times 2/3 { d4 c bes} a2.
}
         