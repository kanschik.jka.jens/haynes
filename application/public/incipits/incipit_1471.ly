
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonate 1.re"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key g\minor
   \tempo "1. Prelude. Lentement"
  \time 3/4
g8.[ a16 g8. a16 bes8. c16]
a8.[ bes16 a8. bes16 c8. d16]
bes4 \grace a8 g4 d' d2.~ d4 c c c bes a


 
}

\relative c'' {
\clef "treble"   
  \key g\minor
   \tempo "2. Fugue. gay"
  \time 4/4
r8 g8 bes g a a c bes16 a
bes8 es es es a, d d d 
g, c c c fis, g16 a bes8 d es2


 
}

\relative c'' {
\clef "treble"   
  \key g\minor
   \tempo "3. Grave"
  \time 3/2
bes2 c d
es2. f4 es d
c2 c f
d d es4 d
c2 c


 
}

\relative c'' {
\clef "treble"   
  \key g\minor
   \tempo "4. Gigue. Vivement "
  \time 6/8
bes8 c d a bes c
bes c d a bes c
bes g g' fis4. g4 r8 fis4 r8


 
}





\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonate II.e "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key d\major
   \tempo "1. Prelude. Gravement"
  \time 2/2
r2 a8. g16 fis8. e16
fis4 a g fis
e2 a~ a4 g8 fis g2~ 
g8 g fis e fis2~ fis4.

 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Courante. Legerement"
  \time 3/4
\partial 4
r8 d8
d4 fis d
a'8 g fis e d cis
d cis d e fis d
e d e fis g e 
fis4 d
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Grave"
  \time 3/2
r2 fis1
r2 e1
r2 d2. cis4
d2 g1
r2 fis1
r2 e1
fis1 fis2~ fis

 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Gigue"
  \time 6/8
\partial 8
\set Score.skipBars = ##t
r8 R2.*1 r4. r8 r8 e8 
e d d d cis cis
d b e cis a cis
d4 d8 e a, a'
b g e fis4 

 
}





\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonate III.e "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Prelude. Gravement"
  \time 2/2
fis2. fis4
g2. g4
fis2. fis4
e2. d8. cis16
d4 fis e d cis2 r2


 
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Fugue. gay"
  \time 4/4
b8 d fis d b e g e
a, cis e cis a d fis d
g, b d b g cis e cis
ais4 fis

 
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Grave. Gracieusement"
  \time 3/4
r4 b4 d
e4. fis8 cis4
d b d8 e
fis4. fis8 b4
ais2.

 
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "4. Vivement, et croches égales"
  \time 4/4
fis4. fis8 e d cis e
d4 b g'4. g8
g4 cis, fis4. fis8
fis4 e8 d cis4


 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    " Sonate IV.e "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Gravement"
  \time 2/2
\partial 2
r2 r4 e8. e16 b4 b8. d16
\grace d4 c4 a8. b16 c4 cis
dis e e dis
e2 
 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Fugue"
  \time 2/2
r4 r8 b8 a g fis d
dis e fis b g e e' e
e4 dis8 b g' fis e dis 
cis b b ais b4 r8 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Grave"
  \time 3/4
e4 d4. c8
b2.
c8 b a4. g8
fis fis b a g fis
g4. a8 b4
c8 e d b a g 
fis g fis4.
 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Gigue"
  \time 12/8
r8 e8 b e b e g g e g e g 
b4.~ b r8 b8 g b g b 
a fis a a g fis g4.~ g4 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    " Sonate V.e "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Prelude. Lentement"
  \time 4/4
e4 r16  e16 a cis, d4 r16 d16 cis b
cis4 r16 cis16 d e b8. b16 b8. a16
a4. 




}


\relative c''' {
  \clef treble
  \key a\major
   \tempo "2. Courante"
  \time 3/4
\partial 4
r8 a8
a4. gis8 a b
gis a gis fis e d
cis a cis b cis dis
e gis fis gis a fis 

 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Sarabande"
  \time 3/4
e4. e8 a e
fis e fis a gis fis
gis fis gis b a gis
a gis fis e d cis
fis g fis e d cis 
b cis b4. a8 a2.

 
}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "4. Legerement"
  \time 3/8
r8 a8 a
gis16 fis e d cis b
a gis a cis b d
cis8 b a e'4 r8
 
}




\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    " Sonate VI.e "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prelude. Lentement"
  \time 3/4
b4. c8 b c
d4. e8 d e
d4. c8 b d
c4. b8 a c b a b c b c 
a2

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Fugue. Gay"
  \time 2/2
r8 g8 b c d e fis d
g2 fis8 g a fis
b a g b a2
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Grave"
  \time 3/2
g1 e4. d8
c1 b4. a8
g4. a8 b2 e~ e dis dis
e1
 
}


\relative c''' {
  \clef treble
  \key g\major
   \tempo "4. Vivement"
  \time 2/4
g4 d
b g
d'8 e fis d
g4. fis8
e fis g e a4. 
 
}










