\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Largo e affettuoso"
  \time 3/4
  a4 c d e
  gis,2 e'4
  d c b8 c
  b4 gis a
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allemanda"
  \time 4/4
  \partial 16
  a'16
  a8 e a, e' a gis16 fis e d c b
  c8 a f' f f e4 e8
  e d4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Poco allegro"
  \time 3/4
  c4 c c
  g'8 a g f e g
  f g f e d f
  e4. f8 g4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Allegro"
  \time 2/4
    \partial 8
    e8 e a4 g8~
    g f4 e8~
    e d4 c8 f e d c b2 a4.

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
  g'8 bes a g fis a c, a'
  bes, g' a bes a fis d fis
  g bes, c bes c a d d,
  g
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Largo e affettuoso"
  \time 3/4
  g'4 d bes'
  fis g2
  d8 f es4 d
  c8 es d4 c
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. fuga"
  \time 4/8
    \partial 8
    d8
    bes g a fis
    g16 d e fis g a bes c
    d4 c
    d8 es d c
    b4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Un poco largo"
  \time 6/8
    
    a'4 b8 a4 g8
    fis4 e8 \grace { e16[ fis]} g4 fis8
    e4 d8 e

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Fuga. Allegro e Spiccato"
  \time 4/8
  d4 a'~ a b~
  b a g fis
  e8 a, fis' a
  fis a cis, a
}
  
  \relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Sarabande"
  \time 3/4
  a'4. bes8 a4
  \grace c,4 bes2.
  g'2 \grace f4 e4
  cis2.
}
  
  \relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Allegro ma non presto"
  \time 4/8
  a'8 fis b cis
  d d, g g
  cis,  a' g fis16  e
  fis8 d b' b
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Adagio"
  \time 3/4
    b'4 fis8 g e fis
    \grace e4 d2 cis4
    b2 cis8 b16 cis d4. b8 cis d
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Corrente. Allegro"
  \time 3/4
  \partial 4
  b'4
  fis8 e d g fis e
  fis b, cis g' fis e
  fis ais, b d cis b
  ais b16 cis fis,8
}
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key b\minor
                      \tempo "3. Grave"
                      \time 4/4
                      % Voice 1
                      R1
                      \times 2/3 { fis8[ gis fis] }
                      \times 2/3 { gis8[ a gis] }
                      \times 2/3 { ais8[ b ais] }
                      \times 2/3 { b8[ cis b] }
                      fis8 d'4 cis b a8
                  }
\new Staff { \clef "treble" 
                     \key b\minor
                        % Voice 2
                        b,16 ais b cis  d cis d b  cis b cis d  e d e cis
                        d8 d d d  cis fis16 e d8 b
                      \times 2/3 { ais8[ fis' d] }
                      \times 2/3 { e8[ fis e] }
                      \times 2/3 { d8[ e d] }
                      \times 2/3 { cis8[ d cis] }
                        
                  }
>>
}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "4. Giga. Allegro"
  \time 12/8
  r4 b'8  fis4  e8 d4 cis8  b4 ais8
  b4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Grave e affettuoso"
  \time 6/4
  es2 d4 c2 b4
  c2 d4 es2 f4
  g2 f4 es2 d4
  c4. b8 c4 b2.
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Fuga di Capella. Vivace"
  \time 4/4
  r4 g' c, as'
  b, g'2 f8 es
  f4 es d c~
  c bes2 a8 g fis4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Minuetto"
  \time 3/4
  g4 c es
  d g2 es d4 c g c
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. ALlegro"
  \time 4/8
  \partial 8
  g'16 f
  es8 g c, es 
  as f,4 f'16 es
  d8 f bes, d
  g es,4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 3/4
  e2 fis4 g2 fis4 e4. dis8 e4 b2.
  g'2 a4 b2 a4 g4. fis8 g4 fis2.
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Corrente. Allegro"
  \time 3/4
  \partial 8
  b'8
  b16 b, cis dis  e fis e fis g a g a
  b8 e, c'4. b16 a
  b8 c, b'4. a16 g
  a8
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Adagio"
  \time 4/4
  b4 fis' g fis8 e
  d b b d cis b ais fis b4 g'2 fis4~ fis
  e2
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 4/8
  e4 fis g a8 b
  dis, b b'4~
  b a~
  a g
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "5. Giga"
  \time 6/8
  \partial 8
  b'8
  fis4 a8 dis,4 fis8
  a,4.~ a8 b a g a g fis4
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 3/4	
  d8  a  a d f e16 d
  e8 f d d e f16 g
  cis,2 d4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allemanda. Andante"
  \time 4/4
  d4 a8. g16 f8. a16 g8. bes16
  a4 d,8 d' cis a d e
  f e16 d e8 d16 cis d8 e16 f e8 d cis
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Gracioso e un poco allegro"
  \time 3/8
  a4 d8 cis4 d8
  e4 fis8
  e a, e'
  g4 fis8 e4 d8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. 1.e Minuetto"
  \time 3/4
  d2 e8 d16 e f2 g8 f16 g
  a2 bes4 a2 d4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. 2.e Minuetto"
  \time 3/4
  d2 a4  fis'2 e4
  a4 g8 fis e d
  cis4. b8 a4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Andnate"
  \time 3/4
  e4 a gis
  a4. b8 a4
  gis4 a b
  cis a cis
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Corrente. Allegro"
  \time 3/4
  \partial 8
  b'8
  b16 b, cis dis  e fis e fis g a g a
  b8 e, c'4. b16 a
  b8 c, b'4. a16 g
  a8
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Allegro"
  \time 4/8
  a'8 a,16 b cis8 a 
  b b16 cis d8 b
  cis cis16 d e8 cis
  d d16 e fis8 d
  e
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Muzetta. Largo"
  \time 2/2
  \partial 2
  e4 fis
  e fis8 gis a4 gis8 fis
  e2 cis8 d  b d
  cis4 b8 a
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "5. Allegro ma non presto"
  \time 4/4
  r8 a' a a  gis gis gis gis
  fis fis fis fis  e e e e
  d d d d  cis cis cis cis
  b16
}
