 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 2/2
    es8 g f es d16[ c bes8] r g
    as as f' as, as[ g] r f16 es
    es8 es' c bes as f bes4~
    bes8[ c] c8. bes16
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Allegro"
  \time 2/4
  es8 bes g es
  d es4 g16 bes
  es8 bes g es d es4 g16 bes
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Adagio"
  \time 3/4
  c4 d4. es8
  b2 c4 d es4. f8
  d2 c4
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "4. Allegro"
  \time 2/4
    es,8 g4 bes8 bes c16 d es8 d16 es
    f8 as,8 as as
    g f16 es r4
}