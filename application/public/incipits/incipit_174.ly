\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  d,8
  fis fis fis e16 d a'4. fis'8
  e fis g fis e d cis r
  
}
