\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 3/4
    c,4 e16 d8. f16 e8.
    \grace g8 f2 d8 b
    c4 d e d r r
}
