\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key e\minor
    \time 4/4
    \tempo "1. Ouverture"
      b4. a8 g4 e
      c'4. c8 c4 d
      b4. b8 g'4. g8
      ais,4
  }
