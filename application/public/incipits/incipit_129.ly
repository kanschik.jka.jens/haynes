\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. [Allegro]"
  \time 2/2
  d8 a fis' d a' fis d a
  d a fis d  d'[ e16 fis] e8 fis16 g
  fis e fis e d4 r2
}



\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Largo"
  \time 2/2
        << {
      % Voice "1"
      r1 r1
      fis1~
      fis16[ g fis e] d8 e16 cis d8 b r
         } \\ {
      % Voice "2"
      r8 b16 cis d8[ b] fis' b, ais fis
      r8 b16 cis d8[ b] a b e, fis b,
      } >>

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 3/8
  d8 a d  e a, e'
  fis d16 e fis g
  a4.
}

