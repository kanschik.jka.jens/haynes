 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #""
                     \key a\minor
                      \tempo "Andante"
                      \time 4/4
                      \partial 8
                      % Voice 1
                      r8
                      r4 r8 e  a, b16 c b8 c16 d
                      c8 d16 e  d8 e16 f  e8 d16 c d8 c16 b
                      c b c d  b8. a16 a4 r
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob"
                     \key a\minor
                      \partial 8
                      % Voice 2
                      e'8
                      a, b16 c  b8 c16 d  c4 r8 e
                      a,8 b16 c b8 c16 d  c8 b16 a b8 a16 gis
                      a gis a b gis8. a16 a4 r
                  }
>>
}


\relative c'' {
  \clef "treble_8"
  \set Staff.instrumentName = #"Tenor"
  \key a\minor
   \tempo "Andante"
  \time 4/4
     \set Score.skipBars = ##t
      \partial 8
      r8
      R1*2
      r2 r4 r8 e,8
      a,8 b16 c b8 c16 d  c4 r8 e
      a, b16 c b8 c16 d c8 e f e
      d e c8. b16 b4
}