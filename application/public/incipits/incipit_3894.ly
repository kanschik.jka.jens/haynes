 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key a\minor
   \tempo "Andante"
  \time 2/2
    e4. \times 2/3 { d16[ c d] }  e4. \times 2/3 { d16[ c d] }  
    e8[ d16 c] d8 c16 b c8 a r16
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key a\minor
   \tempo "Andante"
  \time 2/2
     \set Score.skipBars = ##t
     R1*7
      r2
    e4. \times 2/3 { d16[ c d] }  
    e8 a, r f' e d c4
    b r r2
}