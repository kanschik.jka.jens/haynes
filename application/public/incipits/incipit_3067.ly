\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Erfüllet, ihr himmlischen, göttlichen Flammen [BWV 1/3]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe da Caccia"
  \key bes\major
   \tempo "o. Bez."
   \time 4/4
   \partial 8
   f8 d8 bes16 a bes8 d es bes16 a bes c d es
   f8 bes,16 a  es8 f' g[ f] f
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key bes\major
   \tempo "o. Bez."
   \time 4/4
   \partial 8
   \set Score.skipBars = ##t
     r8 R1*7 r2 r4 r8 f8
     d bes16 a bes8 d es bes16 a bes c d es
     f8 bes,16 a bes8
}



