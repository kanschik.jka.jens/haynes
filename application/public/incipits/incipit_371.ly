\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
      \time 2/4	
        g'4 r8 
          \override TupletBracket #'stencil = ##f
          \times 2/3 { g16 g g}
          d8 c b a
          g'4 r8 
          \override TupletBracket #'stencil = ##f
          \times 2/3 { g16 g g}
          d8 c b a
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Andante"
      \time 3/4	
\override TupletBracket #'stencil = ##f
      d4 d d
      \times 2/3 { e8 fis g}  g8 g g g
      \grace a8 g4 fis r
      e e e
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Tempo di Menuet. Allegro"
      \time 3/4	
      g'4 g4. a16 b
      a4 cis, d
      e e4. fis16 g
      fis4 ais, b cis
}
