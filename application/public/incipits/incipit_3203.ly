\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Bereitet die Wege, bereitet die Bahn [BWV 132/1]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key a\major
   \tempo "o. Bez."
   \time 6/8
   \partial 8
   e8
   a8. cis16 b8 cis8. e16 e,8
   a8. cis16 b8 cis a cis
   fis8. a16 gis8 a8. fis16 b8
   
  

 
   
   
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key a\major
   \tempo "o. Bez."
   \time 6/8
   \partial 8
   \set Score.skipBars = ##t
  r8 R2.*17
  r4. r8 r8  
   e8
   a8. cis16 b8 cis8. e16 e,8
   a8. cis16 b8 cis16 b a b cis d
  
}



