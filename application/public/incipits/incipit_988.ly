\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
  d,8 d' d, d'
  d,16 a' b cis d8 a'
  d,16 a b cis d8 a
  d,16 a' b cis d8 a'
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Largo"
  \time 3/4
  fis4 d b
  fis' g2
  e4 fis4. cis8
  d4 b'8 ais b cis
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 2/4
  d8 a b a
  e' a, b a
  gis' a b a16 gis
  a8 a, b a
 } 

  