 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 2/2
  bes16[ es, d es]  bes' es, d es c'8 bes r4
  f'16[ bes, as bes] f' bes, as bes g'8 f
}
 

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro [tutti]"
  \time 2/2
  \partial 8
  bes8
  es,16 f g as bes8[ es] d es r bes
  es,16 f g as bes8[ es] d es r f
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro [Oboe]"
  \time 2/2
     \set Score.skipBars = ##t
  \partial 8
    r8 R1*8
    r2 r4 r8 bes
    es,16[ f g as] bes8 es d es r c
    bes4 as g16[ f es d] es8 c'
    bes16[ d f es] d c bes as g4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Adagio staccato [tutti]"
  \time 3/2
  g2 g g   g g g
g g f g1.  
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Adagio staccato [Oboe]"
        \set Score.skipBars = ##t

  \time 3/2
  R1.*4
  c1.~ c1
  d2 g4 as g f
  es2. d4 c2
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Allegro"
  \time 2/4
  bes8 bes4 \times 2/3 { c16 bes c }
  bes8 bes4 \times 2/3 { d16 c d}
  es16 d es bes  f' es f bes,
  g'8 f16 g es8 bes
}
