 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andantino amoroso ma non largo"
  \time 2/4
  d8. cis32 d g8. r32 b,
  c8. b32 c a8. r32 a
  b8 g4 fis8 fis4 g8
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Minuetto"
  \time 3/4
  \partial 4
  c16 d c b
   a8 a4 a a8 a b
   b c c4
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Finale"
  \time 2/4
  \partial 4
  g'16 g, fis' d, g' g, g' g, g' g, fis' d,
  g' g, g' g,
}

