\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key g\major
                      \tempo "1. Andante"
                      \time 6/4
                      % Voice 1
                      r1.
                      r2. c4 c c 
                      c b8 a b4
                  }
\new Staff { \clef "bass" 
                      % Voice 2
                       g,4 g g g fis8 e fis4
                       e e e a a a 
                       b b b
                  }
>>
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Vivace"
  \time 6/8
    d8 g,16 a b c d8 fis16 e fis d
    g8 fis16 e d c b8 g b'
    g e16 fis g e a8
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Largo"
  \time4/4
  \partial 8
  g8
  d'1~
  d8 b g d' g2~
  g b g d
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Vivace"
  \time 3/4
    <g g'>8 d' b  g d' g,
    fis d' a fis d' fis,
    g b d4. g8
    a fis d4.
}
