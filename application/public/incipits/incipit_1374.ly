 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
    r4 r8 f, c' c c c
    c a16 bes c8 f, c' c c c
    c a16 bes
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 3/4
  d4 d d
  d c r
  es es es es d r
  f f f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro assai"
  \time 2/4
  \partial 16
  c16 c4 bes
  a r8. a16
  a4 g f r8 c'
  f c g' c,
}

