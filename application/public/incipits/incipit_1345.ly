 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o,Bez."
  \time 2/2
  \partial 4
  bes4
  es4. g8 f4 f16. es32 d16. es32
  d4 bes
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 2/2
  \partial 8
  g16 as
  bes[ as g as] bes8 c bes[ c16 d] es8 bes
  c bes16 as g[ f g as] bes4
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Adagio"
  \time 3/4
  c4 g'2
  g4. f8 es f16 d
  c4 r8 c d es
  f2.~ f4. es8 d c
  b2 g4
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "4. Boure"
  \time 2/4
  \partial 8
  es16 f
  g8 f16 es d8 es
  f bes,4 es16 d
  c8 bes as bes16 f
  g8 es4
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "5. Allegro ma non presto"
  \time 3/8
  es8 g f
  es d c
  bes c as g4 es8
  g16 f es f g as
}
