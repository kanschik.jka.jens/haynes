\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 2/2
   es4 bes'8. as16 g[ as bes as] g f es d
   es4 bes8. as16 g[ as bes as] g f es d
   es[ d es g]
}
