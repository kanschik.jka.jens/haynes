 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro non tasto"
  \time 4/4
  \partial 8
    es8
    es4 d8 g g4 fis8 g
    es4 d8 g g4 fis8 g
    a16 g fis e d c bes a bes8[ fis g]
}

\relative c'' {
  \clef alto
  \key g\minor	
   \tempo "2. Largo [Tutti]"
  \time 3/4
  g8 g g g es' g,
  fis fis f f d' f,
  e e es es c' es
  d d d d c' d,
}  


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Largo [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*5
   r4 d g a8 fis \grace es8 d2
   g8 e \grace d8 c2
   c8 fis a c, bes a bes fis \grace fis8 g4 r
  
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Allegro non Molto"
  \time 3/8
  d8 d es d d es d16 a' bes g a fis
  g a, bes g a fis g8 d' es
}

