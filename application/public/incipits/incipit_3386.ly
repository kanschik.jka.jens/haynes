      \version "2.16.1"
         #(set-global-staff-size 14)
         
\relative c'' {
  \clef treble   
  \key bes \major
   \time 4/4
  \partial2 c8 b c d|
   es es f es d d es d 
}