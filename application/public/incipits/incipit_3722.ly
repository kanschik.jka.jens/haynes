\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Tempo Giusto [Tutti]"
  \time 4/4
  c,8 c' r16. c,32 c'16. b32 a8 a, r16. a32 a'16. g32
  f16. e32 d16. c32 b8 f'' f4 e8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Tempo Giusto [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*12 
    c2~ c4. cis8
    d4 \time 4/6 { d16[ f e d e f] } f4 e8
    \times 2/3 { f16[ g a] }
    g8 c, \times 4/6 { c16[ b c d e f] }f4 e8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio "
  \time 3/4
  g4. \grace c8 b16 a32 g d'8 e
  g,4 fis8 r16 d g16. b32 d16. g32
  g16.[ fis64 g] e16.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Tutti]"
  \time 2/2
  c2 c,4. c'8
  d2. c8 d
  e2 c,4. e'8
  f2. e8 f
  g4. c,8 c'2
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Solo]"
  \time 2/2
     \set Score.skipBars = ##t
   R1*63
     c2. d4 d2. c8 d
     e2. f4 f2. e8 f
     g1~ g2 \times 2/3 { a4 b c }
     \times 2/3 { b4 a g }  \times 2/3 { fis4 g a }

}