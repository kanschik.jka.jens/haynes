\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key a\major
    \time 2/2
    \tempo "1. Discretamente"
      a4 e'16 cis a e  fis8 e r a
      d4 b'16 gis e d  d8 cis r8 \times 2/3 { cis16[ d e]}
      cis8 e, e \times 2/3 { cis'16[ d e]}
      b8 e, e fis16[ gis]
      a8. 
      
}

\relative c'' {
  \clef treble  
  \key a\minor
    \time 3/8
    \tempo "2. Larghetto"
      \override TupletBracket #'stencil = ##f
      a8. b16 a8
      c b a
      a' \times 4/6 { g16[ f e  d c b]}
      a8 \times 4/6 { g16[ f e  d c b]}
      a8. b16 a8
      c b a
}

\relative c'' {
  \clef treble  
  \key a\major
    \time 2/4
    \tempo "3. Allegro"
      a32 b cis8.  cis32 d e8.
      fis32 gis a8.   a8 e,
      \override TupletBracket #'stencil = ##f
      \override TupletNumber #'stencil = ##f
      \times 2/3 { fis16[ gis a] }  gis8
      \times 2/3 { a16[ b cis] }  b8
      \times 2/3 { cis16[ d e] }  a,8 r4
}
