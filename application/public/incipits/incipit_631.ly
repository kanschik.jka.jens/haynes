\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Vivent et piqué"
  \time 4/4
  d,16 e fis g a b cis a d8 d, r a''
  b16 a g fis e a g a   fis g fis g a8 a,
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. 1er Air"
  \time 6/8
  \partial 2
  fis8 fis g a
  d, cis d  e d e
  fis4 e8 g4 fis8 \grace fis8 e4 d8
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Sarabande"
  \time 3/4
  a4 d4. e8 cis2.
  e4 a4. r32 g fis e
  \grace { e16[ fis] } fis2 \grace e8 d4
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Fugue"
  \time 2/2
  d1 e2  fis
  g r4 g
  fis fis fis e8 d8 e4 a, r
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "2.eme Sonate " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key d\minor
   \tempo "1. Lentement"
  \time 4/4
r4 r8 a bes g16 a bes8 a16 g
a8 f16 g a8 g16 f g8 e16 f g8 f16 e f8[ \grace e8 d]
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allemande"
  \time 4/4
  d4 r8 a d e f g
  a4 r8 a bes g e c'
  a f r4
}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "3. eme Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Grave et piqué"
  \time 3/4
  r4 g8. a16 b8. c16
  d4. c8 c8. b32 c
  b4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Courante "
  \time 3/4
  \partial 16
  d16
  d2 r8 r16 g
  g2 r8 r16 b b2 r8 r16 d
  d8 c16 b a8 g fis e
  fis4 d r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "4. eme Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Gravement "
  \time 4/4
  r2 r4 d~
  d c2 bes4 a d8 c bes c bes a
  g16 d' e fis g4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allemenade"
  \time 4/4
  g'8 bes16 a g8 fis g d r4
  g8 bes16 a g8 fis g d r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "5. eme Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Lentement"
  \time 4/4
  fis4 b2 a8 g16 fis
  g4. fis16 e fis4 b, ais b cis fis~
  fis e8. fis16 fis4 r
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Fugue"
  \time 4/4
  b2 fis' g fis4 b~
  b ais b8 b b b
  a a a b16 a g8 g g a16 g fis8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "6. eme Sonate" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Gravement"
  \time 4/4
  a'4~ a16 cis, d e fis8 fis fis e16 d
  e4~ e16 a, b cis d8 d d cis16 b cis8 \grace b8 a8 r
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. 1er Gavotte"
  \time 2/2
  \partial 2
  cis4 \grace b8 a4
  e' a gis8 fis e d 
  cis4 b
}

