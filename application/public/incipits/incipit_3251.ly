\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "[3]. Presto Tutti]"
  \time 3/8
    g16 a bes c d8
    bes d fis~
    fis g d
    \grace c8 bes4 a8

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "[3]. Presto[Solo]"
  \time 3/8
   \set Score.skipBars = ##t
   R4.*24
      g16 d g a bes a
      g8 d'4~
      d16 c bes a g fis
      g8 d'4~
      d16 g a bes a g
}
