 
\version "2.16.1"
 
#(set-global-staff-size 14)
 \layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Soprano) - Ich will dir mein Herze schenken [BWV 244/19]" 
    }
    \vspace #1.5
}
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore I"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key bes\major
                      \tempo "o. Bez."
                      \time 6/8
                      % Voice 1
                      bes16 a bes d c es
                      d c d f es g
                      f g as8 g g f es~
                      ~es d16 c d8 a'bes c,~
                      ~c bes16 a bes8
               
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore II"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key bes\major
                        % Voice 2
                        r4. bes16 a bes d c es
                        d es f8 es es d c~
                        ~c bes16 a bes8 c d a~
                        ~a g16 fis g8
                      
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key g\major
   \tempo "o. Bez."
  \time 6/8
     \set Score.skipBars = ##t
     R2.*6
     g4 a8 b4 c8
     d16 e f8 e8 e d c~
     ~c b16 a b8 r8 r8

}
