\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 2/2
  f8 f f f  g4~ g16 f es d
  es8 es es es  f4~ f16 es d c
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 3/4
  bes'8 bes, d f bes bes,
  c4 c4. bes16 c
  d8 f bes d bes,4 d8
  }

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key bes\major
                      \tempo "3. Largo"
                      \time 2/2
                      R1 r2 d4 r8 d
                      fis,4 g c c c bes
                      % Voice 1
                  }
\new Staff { \clef "bass" 
                     \key bes\major
                        % Voice 2
                       d,4 r8 es fis,4 g
                       c c
                  }
>>
}  
  
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 12/8
  \partial 8
  f,8
  bes f d  c' bes a bes4. c
  d8 bes f es' d c d4.
}
