\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Reis von Habsburgs hohem Stamme [BWV 206/7]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key fis\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                      fis16 gis a b cis8 d fis, eis4 fis8
                      b a4 gis8 a16 gis fis eis fis a gis b
                      a8 cis fis a,
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key fis\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                        R1 R1 fis16 gis a b cis8 d fis, eis4 fis8
                      b a4 
                
                  }
>>
}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt [Donau]"
  \key fis\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
     R1*8 fis16 gis a b cis8 d fis, eis4 fis8
     b a4 gis8 a gis fis4
}