\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Ach Herr, was ist ein Menschenkind [BWV 110/4]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key fis\minor
   \tempo "o. Bez."
   \time 3/4
   r4 r4 cis4 a'4. gis8 b16 a gis fis
   d'4. cis8 e16 d cis b
   eis4 fis8. cis16 d8. b16 a4 gis r8
   
   
  
   
   
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key fis\minor
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
   R2.*16 r4 r4 cis 4 a'4. gis8 a8. fis16 d'8. cis16 b4 r8
}



