\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Chor: Ich harre des Herrn [BWV 131/3]" 
    }
    \vspace #1.5
}
 
\transpose g a
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\minor
                      \tempo "Adagio"
                      \time 4/4
                      % Voice 1
               r4 es4 es es8 es
               es4 r4 
                    
                    
                     
                  }
                  



\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Sopran"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\minor
                      \tempo "Adagio"
                      \time 4/4
                      % Voice 1
                      r4 d4 d d8 d8 d4 r4
               
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Alt "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                       r4 es4 es es8 es es4 r8 bes8 es16 d g es g f32 as g bes a16
                       bes2.
              
 }                     
          
 \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Tenor "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\minor
                     \set Score.skipBars = ##t
                        % Voice 3
                       r4 bes,4 bes bes8 bes bes4 r4
              
 }   
 
  \new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Bass "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\minor
                     \set Score.skipBars = ##t
                        % Voice 4
                       r4 es,,4 es es8 es es4 r4
              
 }   
            
>>
}


