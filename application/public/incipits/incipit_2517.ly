 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Alla breve"
  \time 4/4
    e2 g fis b~
    b4 e, a2~ a g
    fis1 e4 fis g a
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Adagio"
  \time 4/4
  <<{
    R1 r2 r4 d
    g2. a8 e
    fis d g4. e8 fis4
    } \\ {
    r2 r4 g,
    d'1~ d4 e8 b c2~
    c4 b16 a b8 a4. b16 c
    }
    
  >>
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Allegro"
  \time 2/4
   r8 e16 fis g8 a
   b b, b' b,
   c e a a,
   b dis e g,
   a c b a
   g
}

