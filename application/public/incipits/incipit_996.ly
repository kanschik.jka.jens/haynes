\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 4/4
  a4 e cis16 d e4 fis16 gis
  a8 e a e cis16[ d]
}
