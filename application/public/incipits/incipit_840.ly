\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 2/2
  f4. bes8 a g16 a f8 d' 
  c f f d16 es f2~f4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/2
    r2 f4 f
    f4. es16 d  c8 d es f
    d c bes c  d es f g
    e4 f2 e4
    f
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 3/4
      d4 d d
      d cis c bes a a bes bes c
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 2/4
      bes8 c16 d  es f g a
      bes8 d, g g
      g a, f' f
      f g, es' es
}
