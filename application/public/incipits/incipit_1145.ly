 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Vivace"
  \time 4/4
    r4 r8. gis'16 a8. gis16 fis8. e16
    dis4 e b cis
    gis4.
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Bergerie"
  \time 6/8
  \partial 4.
  b4 cis8
  e,16 dis e fis gis a  b4 cis8
  e,16 dis e fis gis a b8 cis16. b32 cis8
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "3. Rejouissance"
  \time 4/4
  e4 gis8 e b4 e8 b gis4 b8 gis e2
  \grace { fis32 gis a b cis dis}  e4 gis8 e b4 e8 b
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "4. Loure"
  \time 6/4
    \partial 4.
    b'8 gis4
    b,4. gis'8 e4 gis,4. e'8 b4
    e,4 e8. e16 e8. e16 e2.
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "5. Menuet"
  \time 3/4
    gis'8 e a8. b16 gis8. a16
    b,8 fis' fis4. e16 fis
    gis8 e a8. b16 gis8. b16
}


\relative c'' {
  \clef treble
  \key e\major
   \tempo "6. Tombeu"
  \time 4/4
  b,8 b b b cis4 d
  cis2 b
}


\relative c'' {
  \clef treble
  \key e\major
   \tempo "7. Menuet"
  \time 3/4
    gis'4 e8 gis b gis
    b a a gis gis fis
    a gis cis, cis cis16 dis e cis
}