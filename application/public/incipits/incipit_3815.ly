 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 3/8
  g'4. as8 g f
  es d c
  b4. c4 es8
  \grace es8 d4 g8
  f es d
  c d es
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
   c8
   g'4 as16 g f es
   \grace es16 d8. c16 \grace es16 d8 c16 b
   c8 g16. g32 f'16 d c b
   c8 g16. g32 f'16 d c b
   c8 g
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Allegro"
  \time 3/4
  c4 \times 2/3 { es8[ d c] } \times 2/3 { es8[ d c] }
  b8. c16 d4 r
  d4 \times 2/3 { f8[ es d] } \times 2/3 { f8[ es d] }
  c8. d16 es4 r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}