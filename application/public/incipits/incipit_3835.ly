\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante [Oboe]"
  \time 4/4
  a4. d8 b4 a8 d
  b8. a16 a8. d16
  cis d e8~ e16 g, a g fis32 e d8. r8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante [Tenor]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*7 
    r4 a4 b a8 d
    b8. a16 a8 d cis16 d e4 g,8
    fis16 e d4
}
