\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "I. Sonate " 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \key es\major
   \tempo "1. Allegro assai"
  \time 2/4
  es4 es
  es8 f16 g as bes c d
  es[ es] c c as[ as] bes bes
  


}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegretto. Rondeau"
  \time 6/8
  \set Score.skipBars = ##t
   R2.*2
   es4. d
   es4 c'8 c bes as
   g4. f
   r4 as 8 as g f
   es4. d
 

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    " II. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/4
  f8. f16 a8. a16 c4 r16 c16 c  c 
  cis8. cis16 cis8. cis16 d4 d8 r8
  
 

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegretto. Rondeau"
  \time 6/8
  a4 a8 a g a
  bes4 bes8 bes4 bes8
  e4 e8 e d e 
  f4 f8 f4 r8


}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "III. Sonate " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegretto Comodo"
  \time 2/4
  bes4 bes
  bes r8 r32 f32 g a
  bes4 d8 d bes4 r8 r32 f32 g a 
  bes4 d8 d8 bes4 r8 r32 bes32 c d 
  es4 g8 g es4 r8

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante. Rondeau en Pastoralle"
  \time 6/8
 bes4 bes8 bes a g 
 f4. r8 bes8 a 
 g c bes a c es
 d g f es d c
 bes4 bes8 bes a g 
 f4 r8
 
}


