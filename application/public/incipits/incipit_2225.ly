\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4 	
      bes4 d32 bes16. f'32 d16. bes'8 bes4 f32 g a bes
}
