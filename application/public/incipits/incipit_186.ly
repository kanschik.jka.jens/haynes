\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 2/2
    r4 r8 es16 bes   c[ as f c']  c as f c'
    c8[ as16 bes] r8 d16 g, as[ f d f]
}
