\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
    \override TupletNumber #'stencil = ##f
  \time 4/4	
    \partial 8
    c8
    g' g4 g g g8~ g8 g4 g g g8~
    g8 g4 g g16 a \grace c16 b8 a16 g
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 2/4	
    a8. bes16 c8 c
    c16. cis32 d8 r f16 e
    f e d c c8 bes a8. bes16
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto"
    \override TupletNumber #'stencil = ##f
  \time 2/4	
    <c, e c'>2 <c e 'c>
    e'4 c r g
    a b c d
    e c 
}
