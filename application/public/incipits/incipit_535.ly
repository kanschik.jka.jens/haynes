 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
   \time 3/4  
      d8. e16 e4. d16 e
      f8 e16 d cis4. bes16 c
      d8 c16 bes  a4. g8
      f4. e8 d4
}
