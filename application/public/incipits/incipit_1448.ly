\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Tempo Giusto "
  \time 4/4
  c2 \grace e16 d16 cis \grace e16 d16 cis  d8 r
  e2 \grace g16 f16 e \grace g16 f16 e f8 r
  f4~ \times 4/6 { f16[ e f a g f]}  e8 g32 e16. d32 c16. b32 a16.
  g4 f e r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio molto"
  \time 4/4
c4. f8 e16. f64 g bes,8 r a
\times 2/3 {a16[ fis g] } \times 2/3 {d'16[ bes g] } f8 e
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Menuet"
  \time 3/4
  \partial 4
  g4 c2 \grace e16 d8 c16 d
  c8 r g16 c8. e16 g8. g2 \grace b16 a8 g16 f
  e8 r
}
