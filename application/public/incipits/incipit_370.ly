\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 2/2
  es8.[ d32 es] bes8 r g8. f32 g es8 r
  f'8.[ es32 f] bes,8 r f8.[ es32 f] d8 r
}
