\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8
  d8 
  d g, r d'
  d g, r d'
}
