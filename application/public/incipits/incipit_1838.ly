
\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro moderato  F-Dur  4/4"
  \time 4/4
   f2 a
   c, f
   a,4 c2 bes8 g
   \grace {g16 [a]} g8 f f4 r8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio  B-Dur  4/4"
  \time 4/4
   f1 f2. es4
   d bes c a bes2 r2
   g'1 f2. d8 bes a4 bes es d c2 r2
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Andante con Variazioni  F-Dur  2/4"
  \time 2/4
  \partial 8
   c8 f f f f
   f16 e d c c8 d16 e
   f a g f e d c b 
   c4. cis8 d16 bes' bes bes bes a g f e d c b c8
}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro moderato  B-Dur  4/4"
  \time 4/4
  \partial 8*3
   d8 es g
   f d16 r16 es8 c16 r16 d8 bes16 r16 c8 a16 r16
   bes2~ bes8 c16 bes a8 bes
   c4 c d d es2 r8
}

\relative c''' {
  \clef treble
  \key es\major
   \tempo "2. Andante  Es-Dur  3/4"
  \time 3/4
   g2 f8. d16 es2 \times 2/3 {bes8 es g}
   bes2 as8. f16 g2 \times 2/3 {bes8 g es}
   d8. bes16 \grace bes8 bes'4~ \times 2/3 {bes8 \grace as16 g8 \grace f16 es8}
   d8. bes16 es4 f g \grace bes8 as4 g
   g2 f4
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Poco Allegretto molto scherzando  B-Dur  2/4"
  \time 2/4
  \partial 8
   d16 es f8 f16 g f8 f
   f f16 g f8 f16 g f8 e bes' e,
   f4. fis8 g bes16 r16 f8 bes16 r16 f8 es d4
}




\pageBreak

\markuplist {
    \fontsize #3
    "Quartetto III " 
    \vspace #1.5
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato  C-Dur  4/4"
  \time 4/4
  c16 b c d c8 c c c c c 
  c2. d8. e16 f4 e d c b r4 r2
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andante con Variazioni C-Dur  2/4"
  \time 2/4
  c8 c b g c c
  \grace {d16 [e]} f4
  e8 c a d g, g16 a b c d e
  f8 f f4 e8 e a4 g8 f e d c e c r8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo.Presto  C-Dur  6/8"
  \time 6/8
  \partial 8
  c16 d e8 e e e e e e e e e e e
  f4 r8 e4. \grace {d16 [e]}
  d4 r8 r4
}


