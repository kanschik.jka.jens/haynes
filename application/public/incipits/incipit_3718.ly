 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante"
  \time 3/4
  \partial 8
  f16. d32
  bes8 bes4 bes'8 a g
  e f r f g d
  d4 \grace f16 es8 d32 c16. bes32 a16. g'32 f16.
  \grace f8 es4 d r8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Minuetto"
  \time 3/8
  bes4.~ bes16 a g f es' d
  d4.~ d16 c bes a g' f
  f8 bes r16 f
  \grace f8 es4 d8
}