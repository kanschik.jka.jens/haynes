\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro ma non presto [Tutti]"
  \time 4/4
  c16 g c8 c c c16 es, c'8 c c
  b16 d, as''4 g8 \grace f8 es8 \grace d8 c8 r4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro ma non presto [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*16 
   c16 g c8 c c c16 es, c'8 c c
   b16 g b d  f es f d es8 c r4 
   }

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Siciliano poco vivace"
  \time 6/8
  d8. es16 d8 d4 g8
  es8. d16 c8 d4 g8
  c,8. d16 es8 d8. es16 c8
  bes8. a16 g8 g4 bes8
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro [Tutti]"
  \time 3/8
  c,16 c es es g g es es g g c c
  g g c c es es g8 g, 8 r
  as' f es d c bes
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro [Solo]"
  \time 3/8
   \set Score.skipBars = ##t
   R4.*48
   c16 d es d c b
   c8 g c
   f es d
   es16 d c8 r
}	