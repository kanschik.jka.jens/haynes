\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
    bes'4 r r f8 es
    d4 d es8 f g es
    d2 c4 c
}
