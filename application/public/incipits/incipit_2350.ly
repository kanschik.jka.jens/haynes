\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
    c4 d c8 f e d
    c4 d c8 f e d
    c4 d c8 d16 e f8 g16 a
    bes c, bes c  a' c, bes c a'8 g r
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "2. Largo"
  \time 12/8
    f4 c8 as8. g16 f8  des'4 c8 c4 f8
    bes8. as16 g8 as8. g16 f8 e8. f16 g8
    c,4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 3/8
    f,16 a c8 f
    e4 f8
    g16 e c8 bes
    bes16 g a8 r
    f16 a c8 f
    e4 f8 g16 a bes8 a a g r
}
