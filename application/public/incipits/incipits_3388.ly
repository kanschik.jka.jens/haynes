     \version "2.16.1"
         #(set-global-staff-size 14)

\relative c'' {
  \clef bass   
  \key d\minor
   \time 4/4
   \tempo "Adagio"
 d,,8 e f g a b cis a d c bes a g a bes g
}