 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 3/2
  r2 g d g1 r2
  r g d g1 d2
  c2. d4 c2 b b d g4
}
