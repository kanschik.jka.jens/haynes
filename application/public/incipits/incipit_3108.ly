\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor und Bass): Jesus soll mein alles sein [BWV 190/5]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"(Oboe d'amore)"
  \key d\major
   \tempo "Duett"
   \time 6/8
   fis4 e8 d g8. a32 b
   cis,4 b8 a4.~ a16 b cis d g fis r16 e16 fis g b a r16 
   
   
 } 
 


\relative c'' {
<<
\new Staff { \clef "treble"   
  \set Staff.instrumentName = #"Tenor"
  \key d\major
   % Voice 1
   \tempo "Duett"
   \time 6/8
   \set Score.skipBars = ##t
   R2.*10 cis4 b8 a d8. e32 fis
   e8 d cis b4 
                     
                  }
   \new Staff { \clef "bass"
   \set Staff.instrumentName = #"Bass"
   \key d\major
   % Voice 2
   \tempo "Duett"
   \time 6/8
   \set Score.skipBars = ##t
   R2.*8 fis,4 e8 d g8. a32 b
   a8 g fis e4.~ e4.~ e8 gis a 
   b e, a gis4.
                       
                  }
>>
}


 

