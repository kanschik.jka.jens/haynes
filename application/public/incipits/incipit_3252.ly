\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Wie freudig ist mein Herz [BWV 199/8]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key bes\major
   \tempo "Allegro"
   \time 12/8
   \partial 8
   f8 bes4 d8 c4 es8 d8 c d bes d c
   d es f es f g f es f d bes f'
   f2.~f4.~ f8 es d
   c es d c f bes, as g as f4.~ f2.
 
   
   
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key bes\major
   \tempo "o. Bez."
   \time 12/8
   \partial 8
   \set Score.skipBars = ##t
  r8 R1.*8 r2. r4. r4 f8 bes4 d8 c4 es8 d4. r4 c8
  d es f es f g8 f4. r4
  
}



