\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio de la Chambre " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Symphonie "
  \time 2/2
  \partial 4
  r8 g8 c4. b8 c4. d8
  e4. d8 e4.

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Sarabande "
  \time 3/4
  e4 f g
  d4. d8 e4
  c4 d e8. f16 d2 c4
  
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Menuet "
  \time 3/4
  c4. d8 e4 f2.
  e4. d8 c4
  d b4.

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Dans nos bois "
  \time 3/4
  c4 g'2 e f4
  g a8[ g] f e
  d2 c4

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Symphonie "
  \time 3/4
  c8 b c d e f
  g4. g8 f4
  e4. d8 c4
  b4. a8 g4
  

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "6. Symphonie "
  \time 3/4
  e8 f g4 e4
  f2. 
  e4. d8 e f
  d2 c4
  

}


\relative c''' {
  \clef treble
  \key c\major
   \tempo "7. Symphonie "
  \time 3/4
  \partial 4
  g4 e4. d8 c4
  g'4. f8 f8. e16
  e4

}


\relative c''' {
  \clef treble
  \key c\major
   \tempo "8. Symphonie "
  \time 3/4
  \partial 4
  g4 c, d e
  f g8 f e4
  d4. e8 f g 
  e4 f g
  

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "9. Chaconne "
  \time 3/4
  r4 e8 d e f
  g4 f8 e f g
  a bes a g f e
  d8. c16 b4. c8 c4
 
  

}


\relative c''' {
  \clef treble
  \key g\minor
   \tempo "10. La Jeune Tris "
  \time 3/4
  g4 c, g'8. as16
  f4. f8 g4
  f8. es16 d4. c16 d
  es2 es4
  
  

}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "11. Quesque vous allez "
  \time 2/2
  \partial 8
  c8 c4. d8 es4. f8
  g2. r8 
 
  

}


\relative c''' {
  \clef treble
  \key g\minor
   \tempo "12. Ah quand reuiendrait il?? "
  \time 2/2
  g1
  d4. es8 f4. d8
  es2 d4. c8
  b2.
 
  

}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "13. Symphonie "
  \time 3/4
  a4 d4. e8
  f4 f4. g8
  a4 a bes8. a16
  g4 g a8. g16
  f4 
  

}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "14. Sarabande "
  \time 3/4
  f4 g a8. bes16
  g2 a8. e16
  f2 g8. a16 e2 d4
  
  

}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "15. Symphonie "
  \time 2/2
  r4 a4 d4. e8
  cis4. cis8 d4. e8
  fis4. fis8 fis4. g8
  e4. e8 fis4. g8 a4.
  
  

}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "16. Symphonie "
  \time 3/4
  d8 cis d4 e
  fis8 e fis4 g
  a4. g8 fis4
  g8 fis e fis d e
  cis4 e a,
  

}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "17. Gavotte "
  \time 2/2
  \partial 2
  d4 a 
  d e e4. e8
  g16[ fis8.] e16 d8.
  fis4. g8
  a4
  
  

}


\relative c''' {
  \clef treble
  \key d\major
   \tempo "18. Symphonie "
  \time 3/4
  a2 g4
  fis4. g8 fis4
  e d cis 
  d2 e4
  

}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "19. Menuet "
  \time 3/4
  fis4 e d 
  e2 fis8 g
  a4 b8 a g fis
  e d e fis e4
  

}


\relative c''' {
  \clef treble
  \key g\minor
   \tempo "20. Menuet "
  \time 3/4
  g4. g8 d4
  es4. es8 d4
  c bes a 
  bes4. a8 g a 
  bes a bes c bes c
    

}


\relative c''' {
  \clef treble
  \key g\major
   \tempo "21. Symphonie "
  \time 3/4
  b4 g a
  fis4. e8 d4
  g e fis8 g
  fis4. e16 fis g4
 
  

}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "22. Gavotte "
  \time 2/2
  \partial 2
  d4. e8
  d4 c8 b c4 b8 a
  b4 d 
  
  

}


\relative c''' {
  \clef treble
  \key g\major
   \tempo "23. Menuet "
  \time 3/4
  g4 fis a
  d,2 e4
  d c b a2 g4
  

}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "24. Le Contre faiseurs "
  \time 3/4
  g4 d' c
  b2 a4 g g' a
  b b, c d
  

}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "25. Symphonie "
  \time 3/4
  \partial 4
  r8 bes8
  f'4 f4. es8
  d4. d8 bes'4~
  ~bes a4. g16 a
  bes2 r8
  

}


\relative c''' {
  \clef treble
  \key bes\major
   \tempo "26. Gavotte "
  \time 2/2
  \partial 2
  bes4. a8
  bes4 f es8 d es c
  d4 d es4. es8
  g4 g8 a g4. c8 a2
  
  

}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "27. Chaconne "
  \time 3/4
  r4 bes8 c d es
  f4 f4. f8
  f4 f4. f8
  g8 f f4. es8
  d4
  

}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "28. Symphonie "
  \time 2/2
  r4 e4 b4. c8
  a4. a8 a4. b8
  g4 g a4. b8
  b2 a4. b8 b2
 
  

}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "30. Symphonie "
  \time 3/4
  \partial 4
  b4 e e fis
  g g a
  b a g fis2 fis4
  
  

}


\relative c''' {
  \clef treble
  \key g\major
   \tempo "31. Gavotte "
  \time 2/2
  \partial 2
  g4. g8
  a4 b a4. b8
  g2 
  
  

}


\relative c''' {
  \clef treble
  \key g\major
   \tempo "32. Menuet "
  \time 3/4
  b2 a4
  a2 g4
  g4. a8 fis4
  g8 a g fis e fis
  g2 fis4
  

}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "33. Symphonie "
  \time 2/2
  \partial 2
  b4 e
  dis b' a g
  fis fis8 g a4. a8
  a4 b8 a g4 fis
  g e
 
  

}


\relative c''' {
  \clef treble
  \key g\major
   \tempo "34. Symphonie "
  \time 3/4
  g8 fis g a g a
  b4 a8 g fis4
  a4. b8[ g8. fis16]
  fis2 e4
  

}


\relative c''' {
  \clef treble
  \key g\major
   \tempo "35. Menuet "
  \time 3/4
  b2 b8 a
  g4~ g4. g8
  a b a g a fis
  g8. a16 a4. g16 a
  b4.
  

}


\relative c''' {
  \clef treble
  \key g\minor
   \tempo "36. Sarabande "
  \time 3/4
  bes4 f4. g8
  d4. es8 f4
  bes, c4. d8
  c4. f8 d4
  

}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "37. Menuet "
  \time 3/4
  f4 bes c
  f,2 es4
  d2 es4
  c2 bes4
 

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "38. Rondeau "
  \time 3/4
  d4 d es
  f4. es8 d4
  g f es
  d2 es4
  

}


\relative c' {
  \clef treble
  \key a\minor
   \tempo "39. Symphonie "
  \time 3/4
 r4 r4 e4
 a4. gis8 a4
 b e,4. c'8 b4. b8 c4
 a a4. gis8 a2
  

}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "40. Passacaille "
  \time 3/4
  a4 c4. a8
  e'2 r8 e8 
  fis4. g8 a4
  gis2.
  

}


\relative c''' {
  \clef treble
  \key a\minor
   \tempo "41. Allemande "
  \time 4/4
  \partial 4
  r8 a8
  a2 e4. d8
  c4. b8 b4. a8 a1
  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "42. Chaconne "
  \time 3/4
  b4. c8 d4
  a2 d4
  e4. fis8 g4
  fis4. e8 d c
  

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "43. Gavotte "
  \time 2/2
  \partial 2
  f4. e8
  d4 d c4. bes8
  a4 a 
  
  

}


\relative c''' {
  \clef treble
  \key f\major
   \tempo "44. Sarabande "
  \time 3/4
  g4 a4. bes8
  fis2.
  g8 d es4. d8
  c4 bes a8 g d'2.
  

}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "45. Symphonie "
  \time 3/4
  f4. e8 d4
  cis2 a'4
  a4. g8 d4
  e2 e4
  

}


\relative c''' {
  \clef treble
  \key g\major
   \tempo "46. Gaillarde "
  \time 3/2
  g2 d4. e8 f4. g8
  e2. r8 d8 c4. b8
  a1.
  
  

}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "47. La Boutade "
  \time 4/4
  f1~
  ~f4. f8 e4. d8
  c2 d4. c8
  bes4. a8 bes4. c8
  a4.
 
  

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "48. Chaconne "
  \time 3/4
  r4 e4. f8
  g4 d8 c d e
  f4. e8 f g
  e f d4. 
  

}


