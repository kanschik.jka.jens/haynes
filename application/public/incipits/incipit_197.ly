\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  d8
  bes a16 g a8 bes bes c r bes
  bes a r g fis4 r
  d'32[ e d16] e32[ fis e16] fis32[ g fis16] g32[ a g16] fis4 r
  }

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Moderato [Oboe]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*3 
   r2 c4. b8
   \grace b8 a4 a f'4. e8
   \grace e8 d4 d d8 d e f
   g e c e \grace e8 d4 c8 b c4 r r2
  
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Grave [Tutti]"
  \time 4/4
  a4. d8 cis a r d
  bes g a e f d c'4~
  c8 a fis a d, bes' g, bes'
}
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Grave [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*12
   r2 e4. a8
   gis e r a f d e b
   c a a'4~ a8 fis dis a'
   a gis g4
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Aria Angloise"
  \time 2/4
  \partial 8
  d8
  bes a16 g bes'8 a fis e16 fis d8 d
  d es r e e f r
}
