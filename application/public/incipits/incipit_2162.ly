 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 


\relative c'' {
  \clef treble
  \key es\major
   \tempo "Vivace"
  \time 4/4
  \partial 8
  bes8 es8. f32 g f8. g32 as
  g16 f es8 r

}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "Allegro"
  \time 3/8
  g'8 as16 g as f
  g4 r8
  bes,8 g' f~ f es c'
  f,16 g as8 g
  \grace es8 d4 es8

}