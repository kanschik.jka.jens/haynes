\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key f\major
    \time 4/4
    \tempo "1. Allegro con spirito"
    \partial 4
    c,8. c16 f4. f8 a4 f
    a4. a8 c4 a
    c4. b8 c b c b
    c,4 r
}

\relative c'' {
  \clef treble  
  \key f\major
    \time 6/8
    \tempo "2. Rondo"
    \partial 8
    c,8
    f4 a8 c4 a8
    \grace bes8 a4. g
    f8 f f  g f g
    f4 a8 c4
}

\relative c'' {
  \clef treble  
  \key bes\major
    \time 4/4
    \tempo "3. o.Bez."
     f2 es4 d
     g bes g2
     f16 f8. g8 f es d c bes
     a16 c bes a
}
