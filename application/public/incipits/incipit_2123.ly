\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Ia. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Cantabile"
  \time 6/8
 \partial 4.
 d4 es8
 \grace d8 c4 c8 c8. d16 a8
 bes4 g8 d'4 g,8
 \grace g8 fis4 g8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Rondo"
  \time 2/4
  \grace e8 d16 cis d e
  d8 d
  g4~ g8 g
  a[ b c a]
  \grace c8 b16 a b c b4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \times 2/3 {d8 fis g} d4 d
  d2.
  \times 2/3 {d8 \grace fis8 e8 d} c4 b
  a g r4

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IIa. " 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 3/8
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  f16. bes32
  \grace bes8 a8. c16
  bes16. d32 \grace d8 c8. es16
  \grace es8 d8. \grace es8 \times 2/3 {d32 c d} es32[ c] \times 2/3 {g'32 es c}
  \grace bes8 a8 bes r8

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/4
  f4 es d r4
  d4 c bes r4
  bes4. d8 
  \grace d8 c4. es8
  \grace es8 d8. c32 d es8 c a bes r4

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Minuetto"
  \time 3/4
  f2 d8 bes
  \grace {es16[ f]} g4 f r4
  g8 es c es g, c
  \grace bes8 a4 bes r4

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IIIa. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Largo"
  \time 12/8
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  f8. g16 f8 c4 a'8 a4 \grace fis8 g8 g4 r8
  g8. f16 g8 c,4 bes'8 bes4 a8~ a8. f16 \times 2/3 {g16 a bes}
  c8. bes16 a8 g8. a16 f8 e4 \grace d8 c8

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Fuga. Presto"
  \time 2/2
  r4 f4 c d
  c2 \grace c8 bes2
  a4 f' a, bes
  a2 \grace a8 g2
  f4 f b a
  bes a bes c
  d c d e f1

}
  
  \relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Minuetto"
  \time 3/4
 c4 f a 
 g16 bes g e f4 r4
 f4 a c
 bes16 c bes g a4 r8 f8 f4 d r8 f8 f4 c r8

}
  
  

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IVa. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Largo"
  \time 6/8
  fis8. g16 fis8 fis b, b'
  g8. a16 g8 g b g
  fis8. g16 fis8 fis b b,
  e8. fis16 g8 \grace a8 g8 fis e
  \grace e8 d4 \grace cis8 b8

}

\relative c' {
  \clef treble
  \key b\minor
   \tempo "2. Presto"
  \time 2/4
  \partial 16
  fis16 b8 r8 cis8 r8
  d8 r8 e8 r8
  fis8 r8 b, r8
  r4 fis'8 r8
  e8 r8 cis8 r8 fis,8 r8 e'8 r8 
  d8 r8

}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Minuetto"
  \time 3/4
  fis4~ fis8.[ ais16 b8. g16]
  fis4~ fis8.[ ais16 b8. fis16]
  e8.[ g16 fis8. e16 d8. cis16]
  d8.[ cis16 b8. cis16 d8. e16]
  fis4~ fis8.

}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Va. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  g8 \times 2/3 {d'16 \grace fis8 e16 d}
  d4 g16 d
  \grace d8 c16 b b4 b8
  \grace b8 a8 g \grace b8 a8 g16 fis d' c c4 b8
  \grace b8 a8 g \grace b8 a8 g16 fis \grace fis8 g4 r8 r16

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. ohne Bez."
  \time 3/8
  \partial 8
  g8 g d d 
  c b g'
  g b, b
  a g d' 
  d g d~
  d a' d,~
  d c' b 
  \grace b8 a4 

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Minuetto Commodo"
  \time 3/4
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  d8. \grace fis8 e32 d
  d4 d
  d c b 
  \times 2/3 {a8 c e e d c b a g}
  fis4 g r8

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIa. " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Cantabile"
  \time 3/8
  \grace {fis8[ g]} a8. g16 fis8
  \grace {e8[ fis]} g8 \grace fis8 e4
  \grace g8 fis8. e16 d8
  \grace fis8 e8 \grace d8 cis4
  d16 a d fis8 e32 d
  e16 a, e' g8 fis32 e
  fis8 \grace d8 d16 e fis g
  \grace {g8[ a]} b4 a8
}

\relative c'' {
  \clef treble
  \key d\major
  \tempo "2. La Cacia. Allegro"
  \time 6/8
  \partial 8
  d8 d4 a8 a4 g8
  \grace g8 fis4.~
  fis16[ d32 e fis g a b cis]
  \set Timing.measurePosition = #(ly:make-moment 6 8)
  \bar "|"
  d4 fis,8 fis4 e8
  \grace e8 d4.
}



\relative c''' {
  \clef treble
  \key d\major
   \tempo "3. Minuetto"
  \time 3/4
  \partial 16
  a16 a2 g4
  \grace g8 fis2 a,4
\grace cis8 b8.[ a16 b8 e cis e]
  d4 a' r8 a,8
  \grace cis8 b8.[ a16 b8 cis e b]
  d4 d, r4

}


