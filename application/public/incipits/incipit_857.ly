\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. o.Bez."
  \time 2/2
  \partial 2
  f4 cis
  d r g e
  cis r8 a
  a a a f'
  e a16 g a8 a,
  a a a f'
}
