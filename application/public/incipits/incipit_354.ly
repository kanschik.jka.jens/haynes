\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata B-Dur " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 2/2
   bes16[ c d es] f f, g a
   bes[ a bes8] r f'
   g16[ es d c]
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/2
  \partial 8
  bes8
  f'4 es
  d16[ c bes8]~ bes16 c d es
  f8[ g16 a] bes8[ d,]
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/4
    f4 bes, a
    \times 2/3 { bes8[ c d]} c2
    \times 2/3 { d8[ e f]} bes,4 bes'
    g f r
}
  