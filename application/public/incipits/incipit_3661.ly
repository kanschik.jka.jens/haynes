 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Larghetto"
  \time 6/8
    \override TupletBracket #'stencil = ##f
      f4. es32 d16. c32 bes16. a32 bes16.
      c4 c16 d32 es  \grace es8 d4 r8
}
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegretto"
  \time 4/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      bes'4 \times 2/3 { d16[ c bes]} \times 2/3 { \grace c8 bes16 a bes}
    \grace a8 g4 f
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro assai"
  \time 3/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      bes'2.
      \times 2/3 { \grace c8 bes8 a bes } bes2
      a4 a2
      bes8 bes, c d es f
}

