 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4
    r8 c es g b,4 r
    r8 c es g as2~
    as16 g g f  f es es d  d g d b g4
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Allegro"
  \time 4/4
  c4~ c16 es d c  d c b a  g f es d
  es d c8 c'2 b4 c4
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Andante"
  \time 3/4
  es4 g es
  f2 bes4~
  bes f as~ as g g
  as8 g f4 es d r r
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Allegretto"
  \time 3/8
    c16 es d b c8
    as16 fis \grace fis8 g4
    c32 d es f g16 es f d
    es as g b, c8
}