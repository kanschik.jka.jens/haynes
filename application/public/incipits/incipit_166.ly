\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 2/2
  \set Score.skipBars = ##t
  c4 b c  r8 g
  c d e f g[ f16 e] d8 e16 b
  c8 b16 a d8[ c16 d]
}
