 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/4
  d,8 r f r bes r d r
  f8. g32 a bes8 f
  d c r4
  d,8 r f r bes r d r
  f8. g32 a bes4
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Adagio"
  \time 2/2
     g8 g g g a4 r
     r2 as8 as as as g4 r
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Allegro et presto"
  \time 3/4
  bes,4 r8 bes'16 a bes8 f
  d bes d f bes f
}
