\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo"
  \time 4/4
  r8 g16 bes d8 g g fis r4
  r8 bes,16 d g8 bes bes a r a,16 bes
  a4 a16 a g fis g4 g16 g a g
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 4/4
  r8 d[ g g] g fis16 e d c bes a
  g bes a g a c bes a   bes g a bes a c bes a
}


\relative c'' {
  \clef treble
  \key bes\major
     \tempo "3. Largo"
  \time 3/4
     r4 f, d'
     c8. a16 f4 r
     r d bes' a8. f16 d4 r
     r bes'8. c16 bes8. c16
}



\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 3/4
  g'8 a16 bes a4 fis
  g d2
  es8 f16 g e8 e e e
  f g16 a fis8 fis fis fis
}