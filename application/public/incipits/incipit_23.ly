 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
  bes16[ c d es] f8 bes, c f, a f'
  bes,16[ c d es] f8 bes, c f, r f'
  
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Largo"
  \time 3/4
  es4. g16 as bes8 es,
  f bes, r f' bes bes,
  es4. f16 g as8 c,16 es
  d c bes8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/8
  d16 c d es f8
  g a r
  bes a16 g f es d c bes c d8
  es f r
}

