\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro Moderato"
  \time 4/4
  f2. \grace f16 es8 d
  d2. \grace d16 c8 bes
  bes c16 d es f g a bes8 bes4 a8
  \grace a16 g8 f16 g f4 r8

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Rondo. Allegretto Spiritoso"
  \time 2/4
  \partial 4
  bes8 c16 bes
  a8[ bes c d] 
  es r8 d8 r8 c8 r8 f8 r8 d4 r8
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
  d8. d16
  g4. d8 a'4. d,8
  b'2 a8 g fis e
  d2. c4
  \grace {b16[ c d]} c8 b b2

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Rondo Allegretto"
  \time 2/4
  \partial 4
  g8 g
  fis16 g a g e8 g
  d4 cis16 d e d
  c8[ a' b, g']
  \grace b,8 a4 

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro Moderato"
  \time 4/4
  \partial 8*3
  d8 es d
  g4. a8 bes bes a g
  g fis fis4 r8 d8 es d
  a'4. bes8 c8 c bes a a g g4 r8

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Rondo. Allegretto Moderato"
  \time 6/8
  d8. es16 d8 g4 g8
  f8. g16 f8 es4 es8
  d4 d8 c4 c8
  bes4 a8 g4 r8

}
  
  

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo IV " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  g2 e4. f8
  g2 e8. f32 g f8. g32 a
  g4. fis16 g a8 g f e
  d4 c r4

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Rondo. Allegretto Spiritoso"
  \time 2/4
  g8.[ a16 g8 f]
  e[ d c d]
  e[ f g f]
  e8. d16 c8 r8

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro Espressivo"
  \time 4/4
  \partial 4
  e4 d c r8 c8 e c
  b4 a r8 a8 c a
  e' c a' e c'4. b16 a
  gis4 b r4

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Rondo Pastorale. Allegretto ma non tanto"
  \time 6/8
  \partial 4.
  c8 d e
  f e dis e4 e8
  e4 a,8 c4 b8 d4 c8 b c a 
  c4 b8 c d e 
  f e dis e4 e8


}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro Moderato"
  \time 4/4
  \partial 4.
  c8 d e
  f2 e8.[ d16 c8. bes16]
  a4 a r8 a8 bes c
  d2 c8.[ bes16 a8. g16] c8. a16 f4 r8

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Rondo Allegretto. Grazioso e Moderato"
  \time 6/8
  \partial 4.
  c8 a' f e4 e8 \grace f16 e8 d e 
  f4 fis8 g4 a8
  bes4 bes8 \grace c16 bes8 a bes a4 r8

}

