 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Simphonia"
  \time 3/4
  e8 a, a a  a16 b c d
  e f e f  e f e f e8 e16 f
  e8 a, a a  a16 b c d
}



\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 4/4
  c2~ c16 f, a bes c a g f
  d' f, bes c d bes a g c f, a bes c a g f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Capriccio - Tempo di Gavotta"
  \time 2/2
  \partial 2
  f4 c a~ a32 f g a bes c d e f4 c
  a f a'8 f g e
  f d e c
}

\relative c' {
  \clef tenor
  \key d\minor
   \tempo "4. Aria da Capriccio - Andante"
  \time 3/4
  a8. d16 d8 f
  \times 2/3 {d8[ cis d]}
  d4 g,8 g'16 f e d cis b
  a8. g16 a8
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "5. Menuet"
  \time 3/4
  a8 b c d e f
  e4 b e
  a, f' a, gis8 a b4 e,
}
