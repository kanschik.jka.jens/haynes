\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. [Allegro]"
  \time 2/2
  fis4. e8  d4. e8
  fis2 fis4 b
  fis2 r8 fis e fis
  g8. fis16 e8. d16   e4 fis
  d2
}



\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Andante"
  \time 2/2

}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. "
  \time 2/2

}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "4. "
  \time 2/2

}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "5. Gigue"
  \time 2/2

}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "5. Quanto peno"
  \time 2/2

}
