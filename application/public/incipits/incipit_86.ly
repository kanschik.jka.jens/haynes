\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/2
  \set Score.skipBars = ##t
   f8. bes16 d,8 es d4 r
   f8. bes16 d,8 es
}
