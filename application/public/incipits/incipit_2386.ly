
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
  \tempo "1. Allegro"
  \time 2/2
  \partial 8 
  g8 c4 c~ c8 e d b
 c4. \grace {d32 [c b c]} d8 e4 r8 g,8
 e'4 e~ e8 g f d 
 \grace f8 e4. \grace {f32 [e d e]} f8 g4 r4
 
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Largo"
  \time 3/4
  c4 bes a 
 a,16 es' f es a, es' f es a, es' f es
 bes d f d bes d f d bes4
 
 
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 6/8
 \partial 8
  g8 c c c c4 e16 d
 c8 g' e d4 g,8
 d' d d d4 f16 e
 d8 a' f e4 
 
 
}
 
