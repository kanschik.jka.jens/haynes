 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Andante"
  \time 4/4
    \override TupletBracket #'stencil = ##f
    g'8 g \times 4/6 { g16 g f g f es}  f8 bes, r16 f' f g
    as as8 c, bes' as16 \times 2/3 { g16 f es } es8 r16
}
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro ma non troppo"
  \time 4/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      es4 bes'32 g16. f32 es16. es4 d8 c'8
      c4 bes8 as \times 2/3 { g16 f es} es8 r4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 3/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      bes2 es8 c
      c4 bes g'8 es
      es4 d8. f16 es8. g16
      \grace bes,8 as2 g4
}

