\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  e8
  cis16 d e cis a8 e' d cis r e
  fis32 e d16 e16. a,32 d8 cis cis b r
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 2/4
  r8 a a a
  e' e e e
  gis16 a b4 a16 gis
  a8 gis16 fis e8 d cis16 b a4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Andante"
  \time 2/4
  \partial 8
  e,8
  a16 gis a b c8 b
  c16 b a8 r f'
  e d16 c d c b a
  gis4 r8
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Allegro"
  \time 3/8
  e8 fis a,
  b32 a gis16 a4
  b32 a gis16 a4
  fis'8 e d
  cis16 b a b cis d
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Adagio"
  \time 3/4
  \partial 4
  a'8 a,
  d cis d4 e16 cis a8
  f' e d4 a'
  bes32 a g8. g32 f e8. e32 f g8.
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro"
  \time 4/4
  d2 d
  a'4 a a a
  f8 d f a f d f a
  e4 a, e' a
  f8 d f a f d f a
  e4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Adagio"
  \time 6/8
    \partial 8
    a8
    e'8. f16 e8 e4 d8
    c8. b16 c8 a4 e'8
    f8. e16 d8 c8. b16 a8
    gis8. a16 b8 e,4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro"
  \time 2/4
  d8 a4 d8
  cis16 d e4 g8
  f e16 d bes'8 d,
  cis d r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  g8 b32 a g16 d'16. g,32 e'16. g,32 fis8 g r fis16 g
  a16. d,32 b'16. d,32 a'16. d,32 b'16. d,32 b'8 a r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
  r8 g g4
  r8 d' d4
  r16 g fis e d8 c
  b16 a g8 r
}
  
  \relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Andante"
  \time 3/4
   \partial 4
   d4
   d c16 bes8. a16 g8.
   a16 c es8 fis,2
   g bes4 a8 bes c a f4
}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 6/8
  g4. g
  b4 c8 d c b a4 e'8 d4 c8
  b a g r r 
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  d8 g16 fis g4 a8 bes16 a bes4 c8
  d c16 bes a8 g fis16 g a8 r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 4/4
  g4. g8 d' d d c
  bes a16 g d'8 d, g bes'4 g8
  fis16 g a4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Andante"
  \time 2/4
  \partial 8
  f8 f16 es d c bes8 c
  a bes r f'
  g32 f es16 f32[ es d16] es8 d
  d c r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 3/8
  d4. g
  f8 es d d c bes c bes a bes16 a g8 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  b8
  e16 b e32 fis g16  fis b, fis' b g fis e8 r dis
  e16 b e32 fis g16  fis b, b' a
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 3/8
  e16 b e g fis e
  fis b, fis' a g fis
  g fis e fis g a b8 b, b'
  g16 fis e4
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Largo"
  \time 3/4
  fis4 b,4. cis16 d
  cis8 fis, fis'4. fis8
  b e, e4 e16 fis g e
  a fis d8 d4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Presto"
  \time 2/4
  e8 b4 a'8
  g16 fis e8 r4
  e8 b4 a'8
  g16 fis e8 r8
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  d4. a'16 fis d cis d4 a'32 fis d16
  b'32 a g16 a32[ g fis16] g8 fis fis e r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  d16 cis d4 a'16 fis d cis d4 b'8
  a32[ g fis16] g32 fis e16 fis32 e d16 a'8 fis16 e d8 r 
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. ADagio"
  \time 3/4
  r4 d32 e f8. e32 f g8.
  f8 e d4 cis
  d8 e f4 g32 f e8. f32 e d8. bes'4 a
  gis
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Vivace"
  \time 2/4
  d4 d d8 fis16 e d8 d
  d fis16 e d e fis g
  a8 g16 fis e8 a fis16 e d4
}

