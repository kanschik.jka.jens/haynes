\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. o.Bez."
  \time 2/2
  \set Score.skipBars = ##t
  \partial 8
  b16 ais
  b8 d cis16[ b ais g] fis8 fis r g16 fis
  g8[ b] a16 g fis e d8 d r
}
