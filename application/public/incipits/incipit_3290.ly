\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}
\relative c''' {
  \clef treble
  \key bes \major
   \tempo "1. Allegro  B-Dur  3/4"
  \time 3/4
  bes2  f4 d r8 bes c d es2 c4 a r8 f g a bes2.
}

\relative c''' {
  \clef treble
  \key es \major
   \tempo "2. Poco Adagio  Es-Dur  6/8"
  \time 6/8
  \partial 4.
  bes8 as f es4. d8 es f bes,4. ~ bes8 c d
  \grace f es d es
  \grace g f es f
  g4.
}

\relative c'' {
  \clef treble
  \key bes \major
   \tempo "3. Allegro  B-Dur  2/4"
  \time 2/4
  \partial 4
      f8 f
      g16 f d bes d c a f
      bes8 bes c c d d
  \grace f es16 d es g
    f4  
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro non troppo con espressione"
  \time 2/2
  d2.  \grace {e16[ d cis]}   d8 e
  f2.  \grace {g16[ f e]}   f8 g
  a2~ a8 bes a g
  gis a  bes a a8. g32 f f8. e32 d
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Rondo moderato"
  \time 2/4
  a'4 bes16 a gis a
  f4 d8. dis16
  e8 e f16 e d e
  f g a f e8
}
