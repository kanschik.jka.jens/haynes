\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio  a-Moll  2/4"
  \time 2/4
   \partial 8
   a8
   e'2~ e e4 r8 a,8
   f'8. g16 e8. f16
   d8. e16 c d e8~
   e8 d4 dis8
   \grace dis4 e2
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro moderato  a-Moll  2/4"
  \time 2/4
   \partial 8
   a8
   e'8. f16 e8. f16
   e8 a16 gis b a gis a 
   e8. f16 e8. f16
   e4
}

\relative c''' {
  \clef treble
  \key a\minor
   \tempo "3. Grazioso  a-moll  3/4"
  \time 3/4
  \partial 8
   << 	
       {r8 R2.
        gis4 a r4
        R2.
        gis,4 a r4
       }
      \new Staff {e'8
                  d c b a gis a
                  \grace { d16[ e] } 
                  f4 e r8 e8
                  d c b a gis a
                  \grace { b16[ c] } 
                  d4 c r8
                }
    >>
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Grave e sostenutol D-Dur  4/4"
  \time 4/4
   \partial 8
   fis32 e fis g
   a8 a a4 r4 r16 d,16 fis d
   d8 cis r16 e g e e8 d r4
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Allegro moderato  D-Dur  2/4"
  \time 2/4
   a2 g fis4. e16 d
   cis8 d e fis
   e8. fis32 g fis4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Andante non molto  d-Moll  2/4"
  \time 2/4
  d16 e f4 e8
  e16 d d4.
  e16 f g4 \grace f8 e8
  \grace d8 cis2
  }


\relative c' {
  \clef alto
  \key d\major
   \tempo "4. Allegro non troppo  D-Durl  3/4"
  \time 3/4
   d2. g2 fis4
   \times 2/3 { e8 g b } \grace d,8 cis2 d2.
   \clef treble 
   fis'16 g a b a8 a a a
   a g g2
}
