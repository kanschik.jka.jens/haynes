
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Suite "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\minor
  \tempo "1. Ouverture"
  \time 2/2
  g4. g8 g, a bes c d4. \times 2/3 {g16 a bes} a4. g8
 fis4. \times 2/3 {fis16 g a} d,4.  
 
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Trio"
  \time 3/8
  d4. bes'8 a bes g a bes a bes g fis g a
 
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxieme Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Ouverture"
  \time 2/2
 \partial 2
 r8 d8 d e f f g a g4. f8 e4. e8 a4. \times 2/3 {g16[ f e] } f4.
 
 
 
}
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Trio"
  \time 6/8
  r8 e e f f16 g f e d cis d e f d g a g f e d

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troixieme Suite "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key a\major
   \tempo "1. Ouverture"
  \time 2/2
 r8 e8 a a cis cis e e a4. \times 2/3 {cis16 b a} b4 e,
 
 
 
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Trio "
  \time 3/8
r8 e e e,16 fis gis a b cis d8 e, d' cis16 e d cis b a   
 
}

