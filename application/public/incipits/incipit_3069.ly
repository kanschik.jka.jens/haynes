\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Verleih, dass ich aus Herzensgrund [BWV 177/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe da Caccia"
  \key es\major
   \tempo "o. Bez."
   \time 6/8
   \partial 8
   bes8 \grace as8 g4 g8 es16 g bes8 des,
   c16 es d c bes as \grace as8 g4
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key es\major
   \tempo "o. Bez."
   \time 6/8
   \partial 8
   \set Score.skipBars = ##t
     r8 R2.*15 r4 r8 r8 r8 bes8
     \grace as8 g4 f8 es bes'8. c32 des
     c16 es d c bes as \grace as8 g4
}



