 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 2/2
    b8[ a16 g]  d'8 g, e' d r4
    e8 d c b a g c b
    a d, d'4. e16 d d4
}

 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
    r8 d g, a
    b4 r8 c
    d b16 c  d8 d
    d b16 c d8 d
    e d4 e16 fis
    g fis g d g fis g d e8 d4
}

 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Largo"
  \time 3/4
    e4 b r8. a16
    g4 e r
    e' b r8. a16 g4 fis r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 3/8
  d8 e4
  c8. b32 c d8
  b c4 a8. g32 a b8
  g a4 fis16 d e fis g a b fis g a b c d8 c4
}