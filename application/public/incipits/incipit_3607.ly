\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 4/4
    r8 d g, fis g d' g, fis
g16 bes a g a c bes a bes d c bes c es d c
d8}
      
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 3/2
    f2 d bes
    f' f, r
    r bes1~
    bes2 a bes4 c 
    d2
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 4/4
  g'8 d bes g es' c16 d es8 c16 d
  es8 c a f d' bes16 c d8
}
