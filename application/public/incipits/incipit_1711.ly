\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
    c 2 e g2. a8 b
    c4 e, e e e4. \grace g16 f8 e4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante un poco Adagio"
  \time 3/4 
    a'2 g8. a32 g
    f4 r c
    bes'2 a8. bes32 a
    g4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro assai"
  \time 2/2
    \partial 4
    e8 f
    a g fis g  a g fis g
    e4 c2 c4
    cis8 d e d c b a g
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro assai"
  \time 3/4
    g'4. fis16 e  d c b a
    g4 r r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 2/2
    g'2~ g8 a b c
    e,2~ e8 f fis g
    c,2~ c8 d dis e
    g,4. a16 b c4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Polonaise"
  \time 3/4
    g'8 g4 fis16 g  a g fis e
    d8 d4 cis16 d  e d c b
    a8 a4 c16 b d c b a
}

\pageBreak

\markuplist {
    \fontsize #3
    "Quartetto III " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro giusto"
  \time 4/4
  R1 r4 r8 c e f a g
  f4 r r2
  r4 r8 c fis g bes a g4 r r2
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 3/4
    f8 g16 a
    bes4 f d f d bes
    f f4.. g32 a
     c4 bes8 r r4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegretto"
  \time 2/4
     \partial 8
    c8
    f f a a
    c8. bes16 g8 e
    f g a b
    c4
}
