 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "Allegro moderato"
  \time 2/4
  \partial 8
  a8 d d d d
  d4. a'8
  g e fis d
  \grace fis16 e8 d r4
}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "Adagio"
  \time 3/4
    b,4 b' b
    ais8. cis16 e8 r r8. d16
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "Presto"
  \time 3/4
  \partial 4
a4
d cis b a gis a
ais b g e2
}
