 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 12/8
    g8. a16 g8 es'4 es8 es4 d8 g4.~
    g fis g
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro"
  \time 3/4
  g'4 f8 es d c
  \grace c16 bes4. a8 g4
  d'8 c bes a bes c
  d
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Adagio"
  \time 4/4
    es4 bes8 g' as,2
    f'4 f,8 as g2
}

\relative c'' {
  \clef bass
  \key g\minor	
   \tempo "4. Allegro"
  \time 3/8
    g,16 a bes4
    a16 g fis es d c bes8 a g 
}