 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  a8
  \grace g16 fis8 e16 d d' b8. 
  \times 2/3 { b16[ a g]} a8 r g
  fis32 g a16 b32[ cis d16]
  \times 2/3 { cis16[ b a]} 
  \times 2/3 { g16[ fis e]} fis16 e d8 r
  
}


\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "2. Adagio"
  \time 3/4
  fis,4 d'32 cis b8. cis32 b ais8.
  b4 fis2~
  fis4 d'32 cis b8. cis32 b ais8.
  b4
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Allegro"
  \time 3/4
   d,4 \times 2/3 { fis8[ e d]} \times 2/3 { a'8[ b c]}
   b8 g'4 cis,8 d a
   gis a e'16 d cis b  a g fis e
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}