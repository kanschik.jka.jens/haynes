 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Largho"
  \time 3/4
  a4 d fis
  e16 fis g cis, \grace cis16 d2
  b4 a r
  b' a g
  \grace g16 fis2
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Allegro"
  \time 4/4
  d,4 r16 d' e d cis d e8~ e16 g fis e
  fis g a8~ a
}

\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "3. Largho"
  \time 4/4
  r8 fis16. e32 d8 cis16. b32 b8 ais ais16. fis32 ais16. b32
  cis8[ g'] fis
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4. Non tanto presto"
  \time 3/8
  d8. e16 cis8 d cis16 b a8
  d8. e16 cis8 d cis16 b a8
}