
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Fête Pastorale "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Modérément."
  \time 3/4
\partial 4
e4 d4. c8 e b
c4 g g'
f4. e8 f d 
e4 \grace d8 c4 c'
c4. bes8 a g 
a4 g f
e4. f8 g4

 
}


 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxième Fête. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Lentement "
  \time 2/2
\partial 2
e4. f8
d4. d8 g4 f
e \grace d8 c4
c'2~ c b
c4 bes a g
f e d c
b \grace a8 g4 
 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisième Fête. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gracieusement "
  \time 3/4
\partial 4
c4 g'4. a8 g f
e4. g8 f e
d c d f e d
c4 g c8 d
e4 d c 
f4.


}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatrième Fête. "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Lentement"
  \time 3/4
\partial 2
c8 b c g
a4 g4. f8
e4 e8 d e c
f4 e4. d8 c4 g

}




\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquième Fête. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Moderement"
  \time 3/4
c4 g' g
g8 f as g f es
d4 c b
c4. d8 es f
g f as g b c
f, es f d g c,
b c c4. 
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixième Fête. "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\minor
   \tempo "1. Gracieusement"
  \time 3/4
\partial4
g4 es4. d8 c4
d \grace c8 b4 \grace a8 g4
c4. d8 es4
f d2
es8 d c d es f 
g4 \grace f8 es4  

 
}

