 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
    g,4 g g r16 g' fis g
    g,4 r16 g' fis g g,8 g' d b
    g g' d b g a16 b c d e fis
    g8 d b g d'4 d
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Siciliana"
  \time 12/8
  d8. es16 d8 d4 d8 g4 d8 bes'4 d,8
  d8. es16 d8 d4 d8 a'4 d,8 c'4 d,8
  bes'8. a16 g8 fis4 g8 a4 d,8 r r
  
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Allegro"
  \time 2/4
  g'8 d d g
  fis4 r8 g16 d
  e fis g d c8 a'16 c, b8 g r
}
