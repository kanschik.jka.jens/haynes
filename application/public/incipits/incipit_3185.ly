\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Ich will an den Himmel denken [BWV 166/2]" 
    }
    \vspace #1.5


}



\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key g\minor
   \tempo "Adagio"
  \time 4/4
     \set Score.skipBars = ##t
   d4~ d16 g, fis g es'8 es es16 f32 g f16 es
                       es8 d~ d16 bes a g c8[ c]


}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key g\minor
   \tempo "Adagio"
  \time 4/4
     \set Score.skipBars = ##t
    R1*6  d4 r16 g,16 fis g es'8 es es16 f32 g f16 es
    es d d8 r8
   
    
    
    }