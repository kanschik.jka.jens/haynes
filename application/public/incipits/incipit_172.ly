\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.S."
  \time 4/4
  c4 g'16 f e d c b c4 g8
  c8. b32 a
}
