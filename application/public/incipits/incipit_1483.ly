 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Andante"
  \time 4/4
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
    g'8 g4 f32 e d c  c8 b16. e32 f8 e32 d c b
    \grace b8 c4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 4
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
    g4
    c4. d16 e d4. e16 f
    e8. g16 b,8. d16 c4 a'
    g8 g f e
    f2 e4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro assai"
  \time 3/4
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
   g4 c d
   \grace f16 e4 d r8 g
   g4 c, e
   \grace f16 e4 d r8
}