\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  f4 f2 e8 d d c c2 cis4
  cis8 d d4. e8 f b,
  b2 c4 r
}



\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Menuetto"
  \time 3/4
  \partial 4
  e8 f
  f,2. g2 a4 bes bes' bes
  bes8 g e c a'4
  f,2. g2 a4 c8 bes a f g e f4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 4/4
  f4 f2 es8 d
  c4 bes r8 bes d f
   f es es2 d8 c
   bes4 a 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Tempo di Gavotta"
  \time 2/2
  c4 a8 bes c4 d8 e
  g f e d d4 c
  
}