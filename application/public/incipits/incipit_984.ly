\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/4
    bes32 d f8. f8 f8
    bes,32 d f8. f8 f8
    bes bes, bes' bes,
    bes' bes, r g'
    f32 es d8. es32 d c8. d4
}
