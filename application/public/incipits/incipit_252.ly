
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
  \tempo "1. Allegro; Adagio"
  \time 3/4
  r4 e e
  a a a a g8 f g4 g f8 e f4
}


\relative c'' {
  \clef treble
  \key a\minor
  \tempo "2. Allegro"
  \time 2/2
  r8 e c8. a16  f'8. e16 f8. d16
  e4. r16 e e8. e16 a8. a16
  a8 g16 fis g8. g16
}

\relative c'' {
  \clef treble
  \key a\minor
  \tempo "3. Gavotte"
  \time 4/4
  \partial 2
  a'4 gis
  a4. e8 f4 e d2 c4 b c a
}

\relative c'' {
  \clef treble
  \key a\minor
  \tempo "4. Gavotte"
  \time 4/4
  \partial 2
  a4 b
  c4. c8 d4 e
  a,2 f'4 e f4. e8 d4 c8 d
}
\relative c'' {
  \clef treble
  \key a\minor
  \tempo "5. Adagio"
  \time 3/4
  r4 e4. e8
  a4 a4. a8
  a4 gis b
  e, d8 c b c
}