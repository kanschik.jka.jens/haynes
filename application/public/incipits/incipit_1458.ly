 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. o.Bez."
  \time 2/4
    r4 f8. g16
    a4 d,
    r8 bes'8 a g f4 d
}
