\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 4/4
      bes16 g8.  g32 as16. bes32 c16.  bes16 g8.
}
