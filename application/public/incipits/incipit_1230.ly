\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro [Tutti]"
  \time 2/2
  \partial 8
  g8
    c g'16[ f]
    es d c  b c8 g es c
    c' g'16[ f]
    es d c  b c8 g es c
    c' g' c, as' c, g' c, as'
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Allegro [Solo]"
  \time 2/2
  \partial 8
  r8
     \set Score.skipBars = ##t
     R1*7
     r2 c4 r8 g'
     es c g' g, c4 r8 d
     es8[ d16 es] f es d c b8 g r4
}


\relative c'' {
  \clef alto
  \key g\minor
   \tempo "2. Adagio [Tutti]"
  \time 3/4
  c4 c8c c c
  c c c c bes bes
  g g g g g g
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*3
    f2. 
    f4. es16 d  es4
    f d c
    b g d'
}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro [Tutti]"
  \time 3/4
    c8 c, es g c g
    es16 c' c c es, c' c c  d, b' b b
    c8 c, es g c g
    
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*20
   c8 c16 b   c8 c16 b   c8 c16 b 
   c g es g   c g es g   c g es g
}