\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro quasi molto"
  \time 4/4
  f4 c8. bes16 a4 g
  f r8 f' c a r f' 
  d bes r f' c a r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andante"
  \time 4/4
  e8 c16. b32 a8 a' dis, e
  r16 e32 g bes[ g f e]
  cis8 d r16 d32 f a[ f e d]
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 3/8
  f,4. d'
  c8 e, f 
  r d' bes c e, f
  r a'16 f a f c4
}
