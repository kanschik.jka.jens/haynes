 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Musette"
  \time 4/4
  a'8. g16 b8 a a4 g8 a16 \grace g16 fis
  \time 2/2
  e2 fis4 e8 d
  \time 3/4
  d4 d4. d16 cis
  \grace cis16 d8. a16 b8. cis16 d8. e16
}

