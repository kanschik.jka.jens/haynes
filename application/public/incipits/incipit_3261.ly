\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro moderato"
  \time 4/4
    d2 d,4. d8
    d8. cis16 d8 d d4. f'8
    f4~ f16 e d cis bes4 a8 g 
    f bes a gis a4 r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 6/8
  a8 r a g bes g
  f4 g16 f32 g a8 r16 a, c f
  a8 r16 c, f a c8 c c
  c r
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Menuetto. Allegretto"
  \time 3/4
  \partial 4
  d8. d16
  d4 a f'
  f8 e d cis d f
  a4 a a
  a e
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegretto ma non troppo"
  \time 6/8
  \partial 8
  a8
  d8. cis16 d8 f8. e16 d8
  cis8. d16 e8
  a, r a'16 a
  a8 r a16 a a8 r a16 a
  a4. e8 r
}
