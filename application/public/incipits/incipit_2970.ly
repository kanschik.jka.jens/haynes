 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
    c4
    \grace { f16[ a]} g4 g4.. a16[ d,8. f16]
    \grace f8 e4 e4.. g16[ b,8. d16]
    c16 d e f g e d c b c d e d c b a
    g4 f e r
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 2/4
    \grace b16 c8 \grace b16 c8 d16 c d e
    c8 e g8. g16
    a g f e f e d c
    b c d b g a b g
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Largo"
  \time 6/8
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}