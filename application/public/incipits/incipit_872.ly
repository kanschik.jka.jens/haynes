\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Tutti]"
  \time 2/4
  g8 g g g
  g16 a b c d8 g
  a,16 g fis e d8 c'
  b g r4

}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Largo [Solo]"
  \time 4/4
  r4 e8. g16 fis dis b8 b8. fis'16 
  g8 e r4 r2
  r4 b'8. c16 c8. a16 fis8. a16
  b8 g e8. g16 c8 b4 a16 g
  g4.


}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro [Tutti]"
  \time 3/4
  d8 d d d d d 
  d4 r4 r4
  g8 g g g g g 
  g4 r4 r4
  e8 e e e e e 
  e d16 c d8 b c d

}