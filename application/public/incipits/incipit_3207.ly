\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Meinem Hirten bleib ich treu [BWV 92/8]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key d\major
   \tempo "Andante"
   \time 3/8 
   \set Score.skipBars = ##t
  a16 fis' e8 d16 cis
  d8 a4
  a16 g' fis8 e16 fis32 d
  cis16 d e cis a8~ 
  a16 fis dis e fis a 
  c b gis'4
 
                                                                                   

}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key d\major
   \tempo "o. Bez."
   \time 3/8
   \set Score.skipBars = ##t
  R4.*12 
     a16 fis' e8 d16 cis
  d8 a4
  a8 d, g8 e4.  
}



