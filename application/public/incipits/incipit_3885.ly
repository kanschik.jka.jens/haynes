\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
    g2 \grace b8 a8. g16 a8. b16
    c8. d16 e8. f16 g8 r16 e a8. g16
    f8 r16 d g8. f16 e8 r16 c f8. e16
    d8. cis16 d8. e16 d4 dis e1
}



\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 3/4
    R2. R
    c2 \grace { d32[ c b]} c8 f
    f e d c r8. c16
    c4 \grace {d32[ c b]} c16 f a gis bes a g f
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro vivace"
  \time 6/8
  \partial 4.
  g8 a b
  c e g g, a b
  c4 r8 g a b
  c e g g, a b
  c4 r8
}