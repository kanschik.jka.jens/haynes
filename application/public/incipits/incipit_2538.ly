 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 3/2
      es8 d es f  es d es f  es d es f
      es4 es \times 2/3 { es8[ f g] } f2 es4
      es2 d1
}
