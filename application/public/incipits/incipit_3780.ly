\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "o. Bez."
  \time 4/4
  \partial 4
  c8. des16
  c bes as bes as2 as8. bes16
  as g f g f4. r8 \grace des'8 c8 bes16 as
  as8. g16 g4. as8 \grace c8 bes8 as16 g g8. as16 as4
}
