\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Ouverture"
  \time 2/2
    d4~ d16 c bes a bes4 d8. d16
    g,4 g'8. g16 g4~ g16 g a bes 
    a8 f bes a g[ bes,]
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Air"
  \time 2/2
  \partial 4
    g'8 a 
    fis4 d bes'4. bes8
    a4. bes8 g4. g8
    g4 a
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Menuet"
  \time 3/4
    g'4 a bes
    fis4. e8 d4
    es4 f8 es d c
    bes4. a16 bes g4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Menuet"
  \time 3/4
      d4 es c
      d bes8 c d4
      g, g' a fis8 e fis g fis4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "5. Chique"
  \time 3/4
  \partial 4
  g'4
  fis4. e8 fis d
  g4 d g
  a d, c
  bes8 a g4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "6. Bourée"
  \time 2/2
  \partial 4
    bes8 c
    d4 c8 bes c4 d a
    bes8 c d es d4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "7. Menuet"
  \time 3/4
    g'2 a4
    fis d bes'
    a e a fis8 e fis g fis4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "8. Menuet"
  \time 3/4
      g'8 fis g4 d
      a'2 d,4
      bes8 a bes4 c
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "9. Ouverture"
  \time 2/2
    g'4 r16 b a g fis8 e d c
    b4 g d'4. d8 d4
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "10. Entrée"
  \time 2/2
    d2 e4. fis8
    g2. r16 b, c d
    e4 a,8 g fis4 d'
    b4. a8 b4 g
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "11. Menuet"
  \time 3/4
     b4 c8 d e4 d g b,
     c a d b4. a16 b g4
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "12. Menuet"
  \time 3/4
   d4 b g a d a b e cis d8 cis d e d4
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "13. Rigaudon"
  \time 2/2
  \partial 4
  d4
  b g a fis
  g2. g'4 e d8 c d4 g
  e d c b
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "14. Bourée"
  \time 2/2
  \partial 4
    g'4
    fis g8 a d,4 b
    e d8 c d4 c8 b
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "15. Menuet"
  \time 3/4
  g'4. a8 fis g
  a4 fis d
  e f8 e d c
  b4 g
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "16. Menuet"
  \time 3/4
    g8 a b4 c
    d2 b4 e2.
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "17. Ouverture"
  \time 2/2
      f4. f8 f4 g8 a
      bes4 bes,8 c d4 es8 f
      g4 bes,8 bes bes4 c8 d
      es4.
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "18. Air"
  \time 2/2
      bes'4. bes8 a g f es
      d4 es8 f g4. g8
      f4 g es4. d8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "19. Menuet"
  \time 3/4
    bes2 c4 a8 g f4 f'
    d g f8 es
    d c d es d c
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "20. Menuet"
  \time 3/4
    f2. g4 a bes
    g f4. es8
    d4 bes8 c d bes f'2.
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "21. Chique"
  \time 3/4
  \partial 4
    f4
    g4. f8 g4
    f2 d4
    es8 f g4 c,
    d8 es f4 bes,
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "22. Menuet"
  \time 3/4
    bes'2 f4
    bes a8 g f es
    d4 d8 es f4
    d c8 bes c4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "23. Chique"
  \time 3/4
  \partial 4
    f4
    d4. c8 d4
    c f f,
    bes g a8 bes
    a4 a f
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "24. Menuet"
  \time 3/4
      bes2 f4
      d' bes f
      f' es8 d c bes
      
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "25. Ouverture"
  \time 2/2
    a'4. e8 f4 g8 a
    d,4. d8
    bes'4 g
    es4. d16 c f4 a,
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "26. Air"
  \time 2/2
  \partial 2
    a'4 bes
    g f8 e f4 g
    a2 a4 c
    f, f f g8 a
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "27. Menuet"
  \time 3/4
  d4 a f'
  e d8 cis d4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "28. Chique"
  \time 6/8
  \partial 4
  a'4
  bes8. a16 g8 a8. g16 f8
  e4 a8 f d f
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "29. Sarabandde"
  \time 3/4
   a4 f4. g8 g4. f8 g a
   a4 d4. es8
   c4. bes8 a4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "30. Menuet"
  \time 3/4
    a'4 f d
    a'2 d,4
    g e f8 g
    cis,2 a4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "31. Passepied"
  \time 6/8
  \partial 8
    f8
    e d cis d a g
    f d' cis d4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "32. Suite"
  \time 6/8
  \partial 8
    a'8
    f d a f d f'
    g16 f g8 a f[ d]
}
