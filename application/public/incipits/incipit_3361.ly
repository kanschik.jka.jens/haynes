 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo ""
  \time 2/2
  \partial 4
    \grace { c32[ bes a]} bes8. bes16
    f'1~ f~ f8 bes a g f d es c
    c4 bes 
}
