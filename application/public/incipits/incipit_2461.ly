\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. All."
  \time 2/2	
   bes4 bes2 c8 bes
   bes as g2 as8 bes
   c4. es8 es d d c
   \grace c8 bes4. as8 g4
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 3/4
  d4. es8 d c
  bes4. bes8 c d
  es4. d8 c bes
  a4. bes16 c bes4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Presto"
  \time 2/4	
    es4 es es r8 g
    f4 f f r
    as2 f8 r d r
    es r g r bes4 r
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. And. moderato"
   \time 2/4
   \partial 8
   b,8
   e e4 gis8
   \grace a16 gis16 fis32 gis fis8 r b,
   fis'8 fis4 a8
   \grace a16 gis16 fis32 e e8
   
}
\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Rondeau"
   \time 3/4
   \partial 4
    gis8 fis
    e4 b b8 a gis2 r4
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. All. di molto"
   \time 2/2
   \partial 8
     g8
     g4. as8 as g f es
     d4. f8 es4 r8 bes'8
     bes4. c8 c bes as g
     f4. as8 g4
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. And. moderato"
   \time 2/4
   \partial 16
   f16 f8. bes16 f8 f16 d
   f es d es es8 r16 c'16
   c8. a16 f8 es
   es16 d cis d d8
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Presto"
   \time 2/4
   g'4 g8 g
   \grace as8 g4 f8 es
   es d d4~
   d8 es f g
   as4 as8 as \grace bes8 as4 g8 f f es es4
}
