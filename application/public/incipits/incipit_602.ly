\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Gravement"
  \time 2/2
  \partial 8
  g8
  b2 b4 b8 g d'2. r8 g,
  e'4. e8 e4. d8 d2.
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Lentement"
  \time 2/2
  \partial 2
  g'4 as
  g f es d
  es2. g4 f es d c
  b2 c4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Tendrement"
  \time 3/2
  g'4. f8 es4. d8 es4. f8
  \grace es8 d1.
  
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Prelude"
  \time 2/2
  \partial 2
  f4 g
  g~ g16 a g a a4. g8
  f2.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Vivement"
  \time 6/8
  \partial 4.
  r8 d d
  d f4~ f8 e e e g4~ g8 f4 
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Gravement"
  \time 2/2
  \partial 2
  f4. \times 2/5 { e16[ d c bes a] } bes4. c16 bes bes4. a8
  a2
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Prelude"
  \time 2/2
  a'2~ a8 a b c
  gis2~ gis8 gis a b
  e,4. d8 e d c b 
  c4 a
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Lentement"
  \time 4/4
  r4 r8 c c4 b8. a16
  g4. \times 2/3 { b16[ a b] } c8. \times 2/3 { d32[ c d] } d4
  e8[ c]
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Lentement"
  \time 2/2
  e4 fis g fis
  e dis e fis dis2.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Prelude"
  \time 2/2
  \partial 2
  b'4. fis8
  d4. d8 d4 e8 fis
  cis4. \times 2/3 { d16[ cis d] } d4. cis8
  b4.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Gracieusement"
  \time 2/2
  \partial 2
  d4 cis8 b
  a2 d4 cis8 b
  a4 d, d' e fis e fis d
  e cis
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Lentement"
  \time 2/2
  a4 cis8 d e4 e,
  a2. a4
  b cis d4. e8
  cis4 a
}

