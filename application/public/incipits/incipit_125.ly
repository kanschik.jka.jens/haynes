 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. [Andante]"
  \time 2/2
  g'8. g16 g8. g16 fis4 r
  f8 f f f  \grace f8 e4 r
  es8. es16 d8. c16 bes8. a16 bes8. g16
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Bouree"
  \time 2/2
  \partial 4
  d4 g, g g a bes8 a bes c a4 d
  g, g g bes a2.
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Andante"
  \time 3/4
  f,4 g f f8 es d c bes c 
  d c d es16 f es8 d
  c4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Vivace"
  \time 6/8
  \partial 8
  g16 a
  bes8 a g d' d, d d4.
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "5. Allemande"
  \time 2/2
  \partial 8
d8 d4. g8 bes16 a g fis a[ fis c es] 
d[ c bes a] c bes a g fis g a d, r4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "6. Sarabande"
  \time 3/2
  bes2 d bes f'2. es8 d c2
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "7. Tempo de Menuet"
  \time 3/4
g4 bes d
fis, a d
g, bes d
d, d'8 c bes a
}