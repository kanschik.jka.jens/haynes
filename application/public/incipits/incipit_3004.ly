\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria " 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \key a\minor
                      \tempo "Andante"
                      \time 4/4
                      r4 e2~ e8 d
                      \grace d8 c8. b16 a8 a a8. gis16 gis8. a16 a4 e'2~ e8 d
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key a\minor
                     \set Staff.instrumentName = #"Chalumeau"
                        % Voice 2
                       R1
                       r4 e2~ e8 d
                      \grace d8 c8. b16 a8 a a8. gis16 gis8 a16 b
                     
                  }
                  

>>
}
