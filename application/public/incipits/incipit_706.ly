\version "2.16.1"

#(set-global-staff-size 14)

\relative c' {
  \clef treble
  \key es \major
   \tempo "1. Allegretto  Es-Dur  3/4"
  \time 3/4
  es4 es g es2 r4
  g'4 g es bes'2 bes4 
  bes2 c4 bes2 as4
}

\relative c''' {
  \clef treble
  \key f \major
   \tempo "2. Allegretto  F-Dur  2/4"
  \time 2/4
  r4 c8 c c a f a
  c bes g e f f f16 g a bes 
  g4
}

\relative c'' {
  \clef treble
  \key d \minor
   \tempo "3. Allegretto  d-moll  2/4"
  \time 2/4
  a8 a d f a4 a
    \grace a8 g e d f
    e e16 f e8 e   
}

\relative c'' {
  \clef treble
  \key c \major
   \tempo "4. Polacca  C-Dur  3/4"
  \time 3/4
   \partial 8
   g8
   c c4 e16 d c d e c
   f e d c b4 r8 g
   c c4 e16 g  f e d c b c a b g4
}

\relative c'' {
  \clef treble
  \key g \major
   \tempo "5. Allegretto  G-Dur  2/4"
  \time 2/4
   d4 g8 fis
   e g d4
   d e8 c d g g,4
   b \grace d8 c b 
   a e' d4
}


\relative c'' {
  \clef treble
  \key c \major
   \tempo "6. Allemande  C-Dur  3/8"
  \time 3/8
   \partial 8
   g8
   e' e16 f g e
   d8 d16 e f d 
   c8 \grace d c16 b c e 
   g8. a16 g f 
   e8 e16 f g  e
   d8 d16 e f d
   c8 c16 e d b c4
}

\relative c'' {
  \clef treble
  \key f \major
   \tempo "7. Andantino  F-Dur  4/4"
  \time 4/4
   r2 r4 c8. c16
   f4 a r8 a g f
   \times 2/3 { e8 g bes } bes2 a8 g
   \grace g f4. f8 \grace a g8.[ f16 g8. a16]
   g4 f
}
