 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
    g8 as
    bes4. as8 g4 as
    f g es f g g g bes
    as2. f4 g2. es4
    f2. as4 g
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Adagio maestoso"
  \time 2/4
  es8 es4 es8~ es g4 g8
  f f f g16 as
  bes8. c16 bes as as g g8 f r4
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Minuetto. Allegro"
  \time 3/4
  \partial 4.
  bes8 es bes g4 r8 g' bes g
  es4 r8 bes es bes
  g g bes g es g bes,4 r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}