 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Andante"
  \time 4/4
  \partial 8
    a'8
    \grace g8 \times 2/3 { f16[ e d]} d4 a'8
    \grace g8 \times 2/3 { f16[ e d]} d4 d8
    es16 bes' bes a a cis, cis d d cis d4 
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Presto"
  \time 2/4
  \partial 8
  a'8
  f16 e d4 cis8 d4 e
  f r8 a
  a4 cis, d e
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Allegro"
  \time 3/4
  a'4 f cis
  d4. e8 f g
  a2.~ a
  a4 g2
}

