\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 4/4
  \partial 8
  g8
  c16 d e f  g8 c, f e4 d16 c
  a'8 g4 f8 e16 d c8 r e
  d16 f f e  d f f e d4 r8
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Affetuoso"
  \time 6/8
    e8. f16 e8 d c b
    c16 d e8 r c16 d e8 r
    d c b c16 d e8 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto"
  \time 3/4
  c4 d8 e f g
  a4 g f e8 g f e d c
  d4 c r
}
