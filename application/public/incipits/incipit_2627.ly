\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key d\major
                      \tempo "1. Andante"
                      \time 3/4
                      % Voice 1
                      d,2.
                      d4 cis4. cis8
                      d cis d e fis d
                  }
\new Staff { \clef "treble" 
                     \key d\major
                        % Voice 2
                        r4 d8 e fis d
                        e d e fis g e
                        fis e fis g a fis
                  }
>>
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 2/4
    d8 d16 a fis8 d
    d' d16 a fis8 d
    g'4 fis
    e16 d cis e a4
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Siciliano"
  \time 6/8
   \partial 8
   b8
   e8. fis16 e8 e8. d16 cis8
   d4.~ d4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Presto"
  \time 3/8
  d8 cis16 b a g
  fis a d8 d
  d cis16 b a g fis8 e16 fis d8
}