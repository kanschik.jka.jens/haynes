\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. o.Bez."
  \time 2/2 
    f,8. c'16 c8. f,16 f16.[ c'32 c16. f,32] f16.[ c'32]
}
