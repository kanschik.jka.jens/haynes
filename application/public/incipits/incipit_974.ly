\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  f,8
  bes d c es  d f es g
  c,2. d4
  d8 c es d  f es g f
  \grace f4 es2 d4
}
