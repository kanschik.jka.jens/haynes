
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
  \tempo "1. Adagio"
  \time 4/4
  r8 g b d b16 a g8 r4
  r8 d' g b, a4
}


\relative c'' {
  \clef treble
  \key g\major
  \tempo "2. Allegro"
  \time 2/4
    r8 g'  d c
    b16 d c b a c b a
    g8 g' d c
    b16
}

\relative c'' {
  \clef treble
  \key g\major
  \tempo "3. Largo"
  \time 6/8
  \partial 8
  d8
  g8. fis16 e8 d4 c8 
  c4 b8 r r d
  d8. e16 d8 g4 d8 
  d4 c8
}

\relative c'' {
  \clef treble
  \key g\major
  \tempo "4. Tempo di Minuetto"
  \time 3/4
    g'4 b8 a g fis 
    g4 fis8 e d c
    b4 d8 c b a
    b4
}
