\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 3/4
    f2 r4
    d8 es f g f4
    f2 d4
    bes8 c d es d4
    c2
}
