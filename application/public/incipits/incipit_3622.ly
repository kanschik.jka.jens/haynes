 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Andante"
  \time 4/4
    d2~ d8 c16 b c4~
    c16 d, fis a bes a g8 r16 g b d
    es d c8 r
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro"
  \time 4/4
  g4 r8 g d'4. c16 d
  g,4 r8 g es'4. d16 es
  g,4 r8 fis'4. e16 fis g,4 r8
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Andante"
  \time 3/2
  f2. f
  f4 d8 c bes8. bes16 c8.[ bes16] bes2
  bes8 f' es2 es8 d d4
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Allegro"
  \time 2/4
    d4 \times 2/3 { bes8[ a g] } d'4 g
    fis r8 g a d, d c
    bes a g16 g' fis g a8 d, d c
}