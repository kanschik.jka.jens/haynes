\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran und Bass): Gott, du hast es wohl gefüget [BWV 63/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key a\minor
   \tempo "Duetto. Adagio"
   \time 4/4
   r8 e16 d e b c a gis8 e'16 f32 d e d c b c d gis,16
   a8
 } 
 
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Sopran"
                     \key a\minor
                      \tempo "Duetto. Adagio"
                      \time 4/4
                      % Voice 1
                       \tempo "Duetto. Adagio"
                    \time 4/4
                   \set Score.skipBars = ##t
                 R1*6 r4 e8 a, f'8. a16 g8 f32 e f16
                 e8 d16 c r8
                  }
\new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Bass"
                     \key a\minor
                        % Voice 2
                        \tempo "Duetto. Adagio"
               \time 4/4
                  \set Score.skipBars = ##t
                  R1*7 r4 e,,8 a, f'8. a16 g8 f32 e f16
                 e8 d16 c r8
                  }
>>
}




