\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef bass
  \key g\minor
   \tempo "1. Adagio [Basso]"
  \time 4/4
  r8 g, bes g  d' d, r d' fis, a d, c'  b f' r g,
  b d g, d' es,16 d' c b c8 c
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio [Oboe]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*8 
    g'2~ g8. fis32 g
    fis16. g32 fis16. g32
    a8 a a g16 fis g8 b,16 c d8 g,
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Vivace"
  \time 2/4
  g'4 fis g8 bes4 a16 g
  f4 es d8 f4 es16 d
  c8 d16 es f8 a, bes8 d4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. A tempo giusto"
  \time 6/8
  bes8 a bes~ bes a16 g f8
  f' f, f'  f, f'4
  es8 es, es' es, es'4
  d8 c16 bes a g f8 es' d
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro"
  \time 3/4
  bes4 c4. d8 d4 r r8. c16
  b8. d16 g,8. f16 es8. d16
  c4 c' r8. bes16
  a8. c16 f,8. es16 d8. c16
  bes4 bes'
}