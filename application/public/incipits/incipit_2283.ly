\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key f\major
                      \tempo "1. o.Bez."
                      \time 4/4
                      \partial 8
                      % Voice 1
                      r8
                      r1 r
                      f1 f f2 g8 f e d c4 a'2 g16 f e d
                  }
\new Staff { \clef "treble" 
                     \key f\major
                     \partial 8
                     % Voice 2
                       \override TupletBracket #'stencil = ##f
                       \override TupletNumber #'stencil = ##f
                     s16 \times 2/3 {c,32[ d e ] }
                     f8 c f a c4 r8. 
                    \times 2/3 {f,32[ g a ] }
                     bes8 g e c bes4 r
                     c r c'16 d c bes a8 a
                     a bes bes4 f16 g f es d8 d
                     d c c4 e'8 d c bes a4 c c a

                       
                  }
>>
}
