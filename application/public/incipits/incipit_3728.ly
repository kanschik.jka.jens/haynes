 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo"
  \time 3/4
    \override TupletBracket #'stencil = ##f
      r8 d g g~ g16 a32 bes a16 g
      g8 fis r16 d es d
      g d d16. d64 f 
}
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Fuga alla breve"
  \time 2/2
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 3/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      
}

