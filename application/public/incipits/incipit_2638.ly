 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Prelude. Allegro"
  \time 6/8
    r8 d d  d d d
    d d d cis a cis
    d a d e a, e'
    fis d e fis e d
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Entree. presto"
  \time 2/2
  a'8.[ a16] \times 2/3 { a8[ bes a]} d,4 r
  bes'8.[ bes16] \times 2/3 { bes8[ c bes]} e,4 r
  
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Aria. poco allegro"
  \time 3/4
  f8. f16 f8. g16 e8. g16
  f4 r r
  a8. g16 f8. g16 e8. g16
  f4 r r
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "4. Menuett"
  \time 3/4
    d2 a'4
    f8 e f g a4
    d,2 bes'4 a8 bes a g f e
    d4 a a
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. petite Prelude"
  \time 2/2
  \times 2/3 { d8[ e d]} \times 2/3 { d8[ e d]} d8 a fis' d
  a' fis d a' \times 2/3 { fis8[ g fis]} \times 2/3 { fis8[ g fis]}
  fis4 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "6. Chiacona"
  \time 3/4
  d8 r fis4 g8 a
  cis,4 a d
  b b4. d8
  e4 fis g8 a
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "7. Gigue"
  \time 2/4
  \partial 8
  a8
  \times 4/3 { fis16[ e d]} \times 4/3 { a'16[ b cis]}
  d4 r8 e
  \times 4/3 { fis16[ e fis]} \times 4/3 { d16[ g fis]}
  e4 r8 
}
