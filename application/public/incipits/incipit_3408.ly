 \version "2.16.1"
         #(set-global-staff-size 14)
         
     \relative c'' {
  \clef treble
  \key  c\major
 \tempo "1. o.bez."
  \time 4/4
    \override TupletNumber #'stencil = ##f
  \override TupletBracket #'stencil = ##f
      \partial 4
      g'8. g16
      c4 c c4. \times 2/3 { b16 a g}
      a4 a~ a8 c16 b \grace b8 c8 b16 a
      g f e f f4~ f8 f g a
     }
