\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
    bes4 f'  d16[ bes c d] es f g a
    bes8[ a16 g] f es d c d8 bes r4
    as'4 as as8[ g16 f] g8 es
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 3/4
    f4 bes,4. d8
    c4 f,4. f'8
    bes4 bes, es
    es8. d16 d4 r
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo ""
  \time 3/8
    bes8 f' es
    d bes16 c d es f8 g a
    bes bes, r
}
