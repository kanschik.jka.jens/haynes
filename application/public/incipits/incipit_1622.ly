 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "o.Bez."
  \time 4/4
  r8 g g g d' c d16 c bes8
  a fis16 e fis8 a g fis g d'
  fis, bes16 a bes8 g
}
