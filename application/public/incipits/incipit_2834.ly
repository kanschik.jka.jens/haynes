\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite] I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allemande"
  \time 2/2
  \partial 8
  d8
  d2 r8 d e fis
  g4. fis8 g4 a
  bes4. fis8 g4. a8 fis4. fis8 g4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite] II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allemande"
  \time 2/2
  \partial 8
  d8 d2. e8 d
  c4. c8 c4. d8 b4 g
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite] III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Prelude"
  \time 2/2
  r8 e d e c a b gis
  a b c d e fis g e
  a2 g4. g8
  f2
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite] IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allemande"
  \time 2/2
  \partial 8
  bes'8
  bes4 bes8 a g f es d
  es4. es8 es4. f8
  d4 g8 f es d c bes c4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite] V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Prelude"
  \time 2/2
  r8 g' es8.[ c16] g'2~
  g8[ es] d8. es16 \times 2/3 { c8[ d es]} d8. c16
  b2 r8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite] VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allemande"
  \time 2/2
  \partial 8
  c8 c2 r8 b c d
  e4 \grace d8 c4 e f
  g4. g8 a4 b c4. c8 c4.
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite] VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allemande la Royalle"
  \time 2/2
  \partial 8
  d8 d2 r8 cis d e
  f4 e8 d a'2~
  a4 a g2~
  g4 g f2~
  f8 g[ f e] e4. e8 d4.
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite] VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allemande"
  \time 2/2
\partial 8
  d8 d2 r8 e fis g
  a4. a8 b4 a8. g16 a4. g8 fis e d cis b4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite] IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allemande"
  \time 2/2
  \partial 8
  e8
  e2 r8 e fis g
  fis4. fis8 fis4 g8. a16
  g4 \grace fis8 e4.
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Suite] X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. Allemande la Plainte. Grave"
  \time 2/2
  \partial 8
  c'8
  c4. bes8 as4 g8. as16 g4 as8. g16 f4 e8. f16
  e2 f4
}
