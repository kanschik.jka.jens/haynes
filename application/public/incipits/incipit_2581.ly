 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major	
   \tempo "1. Adagio"
  \time 4/4
  r8 d16 c d c b a  b8  b16 a b a g fis
  g d' e fis g b a g  fis8 d r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
  r8 d b g
  fis d' b g
  fis fis' fis8. e32 fis
  g8 g g8. fis32 g a16
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
  \time 3/4
        << {
      % Voice "1"
      R2. 
      d8 g g16 fis fis e  d c c b
      b8
         } \\ {
      % Voice "2"
      d8 g g16 fis fis e  d c c b
      b4 r r
        } >>
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Allegro"
  \time 2/4
  \partial 8
  g'16 a
  b c d c  b8 c
  c b4 b8
  c b a d 
  b a16 b g8
  }
