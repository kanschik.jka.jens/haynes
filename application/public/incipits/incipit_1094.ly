\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  c2~ c8 d16 e d c b a
  a8 g g2 f'4
  f8 e e2 d4
  g8 e \grace d8 c2 e8 c
  b a a' g g f e d c4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio"
  \time 2/4
  c4 a'16 g fis g
  \grace {fis16[ g as] } g8. f32 es d8 e
  f f~ f16 as32 g bes[ as g f]
  es d c d c8 r4
}


\relative c'' {
  \clef bass
  \key c\major
   \tempo "3. Menuetto"
  \time 3/4
  c,2 e8 c c4 b c
  \grace b4 a2 g4
  \grace g4 f2 e4
  d'2. d4 f e
  a, f'8 e d c
  \grace c4 b2.
}
