\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 4/4
    R1 r2 r4 e
    e2 fis16 e dis e dis e fis e cis8. d16 d4
}
  
