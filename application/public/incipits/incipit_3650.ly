\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Moderato"
  \time 4/4
    c8 g'4 b,8 c g4 f8
    es16 g c8~ c16 es d f
    es d c4 g8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 4/4
  bes4~ bes16 c32 d es16 g, d'8 a16 g r8 g'
  f16 d c bes  es8. d32 es
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. o.Bez."
  \time 3/8
  c8 c c
  g'4.
  c,16 b c b c8
  as'4.
  g,8 a16 b c d
  es[ d]
}

