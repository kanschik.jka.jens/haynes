\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro non troppo"
  \time 4/4	
  \partial 4
  b'8 c
  fis,4 g8 r c,4 e8 r
  cis4 d r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. o.Bez."
  \time 3/4	
  a'8 r bes r c r
  c4. d8 c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Tempo di Minuetto"
  \time 3/4	
  R2. R
  c4. d8 e c
  a4. cis8 d c
  b4. dis8 e d
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
   \time 4/4
   d,4 f8 r  bes4 f8 r
   d4 f8 r bes4 f8 r
   d''2. c8 bes
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Andante"
   \time 3/4
   g2~ g4
   bes8 as as4. g8
   f4. g8 f4
   as2 g4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Rondo. Allegro"
   \time 2/4
   \grace es8 d8 c16 d  bes8 bes
   f'4 bes
   \grace es,8 d8 c16 d  bes8 bes
   c4 f,
}
