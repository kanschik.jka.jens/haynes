\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Un poco andante e grazioso"
  \time 4/4
  \partial 16
  c16
  f4~ f8. g32 f e4 f
  a2 g8
}



\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Tempo giusto"
  \time 4/4
  f4 f \grace g8 f16 e f g a8 f
  f2~ e8 d c bes
  \grace bes8 a16 g a bes c8 f, a g f e 
  f
}


