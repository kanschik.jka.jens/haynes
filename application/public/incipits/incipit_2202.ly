\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. "
  \time 4/4

}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. "
  \time 6/8

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. "
  \time 4/4

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro"
  \time 3/8
  \partial 8

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "5. "
  \time 3/4

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "6. "
  \time 2/2
   
}
