 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez. [Tutti]"
  \time 2/4
  a4 b8. a32 b
  cis8 a' gis8. fis32 gis
  a8 a, b8. a32 b
  cis8 a'16 gis fis e d cis
  d8 cis4 b16 a
}


\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez. [Solo]"
  \time 2/4
     \set Score.skipBars = ##t
   R1*30 

  a4 b8. a32 b
  cis8 fis e d
  cis b16 a b8 gis a4
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "2. Adagio"
  \time 3/4
  e4 a e d b' d,
  cis a' cis,
  cis b r
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. o.Bez."
  \time 2/4
  d4 d' a8 g fis e
  fis e d cis
  d4 d'
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}