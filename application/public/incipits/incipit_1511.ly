\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}




\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Divertimento II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andantino"
  \time 2/2
  d4. cis16 b a4 b a8 g fis e a2
  \grace a8 g4 g

}

\relative c'' {
  \clef treble
  \key g\major
  \tempo "2. Andante molto"
  \time 2/2
  \partial 4
  \grace c16 b a b c
  d8 g d2 \grace c16 b a b c d8 d d b r d d b
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Tempo di Menuetto"
  \time 3/4
  a2 fis4 fis e a g2 fis4 fis e d
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Divertimento IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante expressivo"
  \time 2/2
  a2 g f d' c8 d d2 c8 bes bes a d c c4 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante molto"
  \time 2/4
    \partial 8
    bes16. d32
    f8 a, bes \grace d16 c8
    bes4 a8 f'16. f32
    f16 bes8 d,16 es es8  d16 d c c4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Tempo di Menuetto"
  \time 3/4
  f,2 \grace a16 g8. f32 g a4 bes r8 c,
  f2 \grace a16 g8. f32 g a4 bes r
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Divertimento VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andantino"
  \time 2/2
  f2 es8 d c bes bes4 bes2 \grace c16 bes8. a32 bes 
  c8 a bes4 r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante con moto"
  \time 6/8
  c,8 c c c r f
  e r bes' a4 r8
  c8 c c c a bes c c d a4 g8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Tempo di Menuetto"
  \time 3/4
  f8 r g2 e2 f4 es2 d4 c a bes
}

