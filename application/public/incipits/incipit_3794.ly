 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Ouverture"
  \time 2/2
  \partial 2
  b4. b8
  e4. e8 e4 fis8 g
  fis4. fis8 b4 dis,
  e4. fis8 fis4. e16 fis
  g4 \grace fis8 e4
}
 
\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. 1.er Rondeau. Legerement"
  \time 3/4
  \partial 2
    b4 e
    dis e fis
    g b, e
    g,8 a a4. g16 a b4 b e
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "4. 1.er Rigaudon"
  \time 2/2
  \partial 4
  b4 b e e fis 
  g8 a g fis e4 e, g2 a
  b8 a g fis e4
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "5. Sarabande"
  \time 3/4
  b4 e8 fis fis e16 fis
  g4. a8 b4
  a g4. fis8
  \grace fis8 g2 \grace fis8 e4
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "6. 1.er Badine"
  \time 2/2
  \partial 2
  e8 fis g a g fis e dis e fis g a
  g4 \grace fis8 e4
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "7. 1.er Menuet"
  \time 3/4
  b2 c4 b e dis
  e8 fis fis4. e16 fis
  g8 fis g a g4
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "8. 1.er Gavotte"
  \time 2/2
  \partial 2
  b4 e
  dis e8 fis \grace fis8 gis4. a8
  fis4 e
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "9. 1.ere Sicilienne"
  \time 6/8
  \partial 8
  b8 b4 e8 dis8. c16 dis8
  e4. b4 a8 g4 fis8 g4 a8 b4. e,4
}



\relative c'' {
  \clef treble
  \key e\major	
   \tempo "10. Chaconne"
  \time 3/4
  r gis'8 fis e gis
  fis4 b b, e a a
  a8 gis fis4 b8 a gis4 gis8 fis e gis fis4 b
}