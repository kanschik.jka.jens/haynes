\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 3/4
    bes4 a8 bes  f4
    bes4 a8 bes f4
    es' d8 es c4
    d c8 d bes4
}
