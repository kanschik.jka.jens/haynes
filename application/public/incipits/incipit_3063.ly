\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Ich nehme mein Leiden mit Freuden auf mich [BWV 75/5]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key a\minor
   \tempo "o. Bez."
   \time 3/8
   \partial 8
   e8 a16. b32 c16 b a8
   d16 e32 f e8 c
   a8. c16 b8
   e,4
   
   
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key a\minor
   \tempo "o. Bez."
   \time 3/8
   \partial 8
   \set Score.skipBars = ##t
     r8 R4.*15 r8 r8 e8 a16. b32 c16 b a8
     d16 e32 f e8 c
     a8. c16 b8 e,4 r8
}



