 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Cantabile"
  \time 3/4
  r4 d g c,4. b8 c d
  b a g a b c
  a2 a4
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  d8
  b16 g a b  c d e fis  g8 d g d
  b16 g a b   c d e fis  g8 d g d
  e16 g fis e d e d c
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Largo"
  \time 4/4
  b'4. b8 a4 a16 c b a
  g8 dis e fis16 e dis8 b e4
  e8 fis16 e dis8. e16 e4.
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Moderato"
  \time 3/4
  g8 a b4 c d2 g4
  fis e d c2 b4
}