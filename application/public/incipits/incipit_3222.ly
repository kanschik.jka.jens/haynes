\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Ich habe genug [BWV 82/1]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\minor
   \tempo "o. Bez."
   \time 3/8
   R4. r8 r8 g8
   es'8. d16 c8
   c4.~ c4 r8
  
   
   
   
}

\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key c\minor
   \tempo "o. Bez."
   \time 3/8
   \set Score.skipBars = ##t
 R4.*32 R4. r8 r8 g8
   es'8. d16 c8
   c4.~ c4 r8
   
   
}


