\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro Moderato"
  \time 2/2
  f2 c a'2. f16 g a bes
  c4 c c c
  a f r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegretto"
  \time 2/2
  \partial 4
  f,8. a16
  c4 c c f
  f2 e4 d8 c
  f r g r a r bes r
  a2 g4 r
}

