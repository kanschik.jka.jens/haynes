\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 4/4
  c,8 c' c c, c4 r
  c8 c' c c, c4 r
  c8 d e f g4 r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio [Tutti]"
  \time 4/4
    c4 r8 c cis cis cis cis
    dis4 r8 dis d d d d
    cis4 r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
    R1*7
    r2 e8 c16 b a8 f'
    f2 f8 d16 c b f' e d
    c8 b16 a r4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 2/4
   c8 e g c,
   b4 r8 c
   d e f e16 d
   e8 c c4
}
