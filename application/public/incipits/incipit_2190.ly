 
\version "2.16.1"
 
#(set-global-staff-size 14)


\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Fanfard Nouvelle " 
    }
    \vspace #1.5
}
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. gaym.t"
  \time 2/2
  r8 a8 a a a a a a 
  d d d d d d d d 
  fis fis fis fis fis fis fis fis
  a a a a a a a a 
  fis a fis d fis a fis a
  
}


\relative c''' {
  \clef treble
  \key d\major	
   \tempo "2. Marche"
  \time 2/2
  a2 fis8 g a b
  a2 fis8 g a b
  a4 g8 fis e4 d
  cis a cis e

}

\relative c''' {
  \clef treble
  \key d\major	
   \tempo "3. La loiiison"
  \time 2/2
  \partial 4
  a4 a g8 fis g4 fis8 e
  fis4 d a d
  fis e8 d e4 d8 cis
  d4 a fis

}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4. La nicolle"
  \time 2/2
  \partial 4
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "5. L'imitée"
  \time 2/2
  \partial 4
  a4 d2. d4 
  fis2. fis4 a2. e4
  fis d e a, 
  fis' d e d,
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "6. La pillarde"
  \time 6/8
  \partial 8
  a8 d e fis fis g a
  fis e d fis g a
  e fis g d e fis
  e4 d8 a4
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "7. L'henrielte"
  \time 6/8
  \partial 8
  fis8 fis4 d8 e4 a,8
  d4. a4 d8
  d4 a8 a4 a8
  d4. a4
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "8. La dijonnoise"
  \time 2/2
  \partial 4
  fis8 g
  a4 g8 fis e4 d8 e
  fis4 e8 d a4 a
  a'8 g a b
  a4 a,
  
}

\relative c''' {
  \clef treble
  \key d\major	
   \tempo "9. La Villageoise"
  \time 2/2
  \partial 2
  a4 g8 a
  fis4 e fis e8 fis
  d4 a
  
}

\relative c''' {
  \clef treble
  \key d\major	
   \tempo "10. La babillarde"
  \time 6/8
  \partial 8
  a8 fis d d d fis a
  e a, a a e' a
  fis d d d fis a 
  e4.~e4
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "11. La rôsette"
  \time 6/8
  \partial 8
  a8 d d d d d d 
  fis fis fis fis fis fis
  a a a a a a 
  fis e d a a' g 
  fis e d a
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "12. La mignône"
  \time 6/8
  \partial 4.
  fis8. e16 fis8
  d4 d8 a'8. g16 a8
  fis4 fis8 fis8. e16 fis8
  d4 d8
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "13. La Nannette"
  \time 3/4
  \partial 4
  d4 a' a a 
  fis8 e fis g fis g
  a g a b a g
  fis d fis a d, fis
  
}

\relative c''' {
  \clef treble
  \key d\major	
   \tempo "14. Menuet"
  \time 3/4
 a4 fis fis fis e r4
 a4 e e e d r4
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "15. Menuet"
  \time 3/4
  d8 d fis fis a a 
  fis4 d a
  d8 d fis fis a a 
  fis4 d a
  a'8 fis a fis a fis
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "16. Le Vaché de bagnolet"
  \time 3/4
  fis4 d fis
  fis d fis 
  fis d fis
  fis2 \grace e16 d4
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "17. La bêrgere"
  \time 2/4
  \partial 4
  d16 e fis g
  a8. g16 fis8 e
  d a'16 g fis8 e 
  fis a16 g fis8 e
  d a
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "18. Le berger"
  \time 2/4
  \partial 8
  fis8 
  fis[ a a e]
  fis4 e8 fis
  fis[ a a e]
  fis4 e8 fis
  fis[ a a e]
  fis[ a a d,]
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "19. Menuet"
  \time 3/4
  d8 fis a d, fis a
  d,[ fis a a,] d fis
  d4 a d
  fis2 d4
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "20. Menuet"
  \time 3/4
  d8 e fis g a e
  fis4 e d
  a fis'8 g a e
  fis4 e d 
  
}

\relative c''' {
  \clef treble
  \key d\major	
   \tempo "21. Le peiisant"
  \time 2/4
  \partial 8
  a8 a[ g g a] 
  fis e16 fis d8 fis
  fis[ e e fis] 
  d a fis 
  
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "22. La peiisanne"
  \time 2/4
  \partial 8
  fis16 g
  a4. fis16 g
  a4. fis16 g
  a8 g16 fis e fis g e
  fis8 e16 fis d8 
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "23. La Toulousaine"
  \time 6/8
  \partial 4.
  a8 a a 
  d e d a a a 
  e'4. a,8 a a 
  fis' e fis g4 fis8
  e4 d8
  
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "24. La Montauban"
  \time 6/8
  \partial 4.
  a8. a16 a8
  d8. e16 d8 e8. fis16 e8
  d8. e16 d8 a8. a16 a8
  d8. e16 d8 e8. fis16 e8 
  d4.
   
  
  
}
