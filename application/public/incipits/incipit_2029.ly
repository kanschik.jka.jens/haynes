\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}



\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Aria [1] allegro"
  \time 2/4
 \partial 4
 bes4
 d f
 d8 bes bes' bes
 bes a4 g16 f 
 g8 c, c' c
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "Aria [2] dolce"
  \time 3/8
  bes8 c bes
  d16. es64 f d8 c
  bes c bes
  es16. f64 g es8 \grace d8 c8
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "Aria [3] allegro "
  \time 2/4
  f,4 d'16 e f d
  f8 c r bes'
  a4 g16 f e bes'
  a8 g r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "Aria [4] a Tempo giusto "
  \time 3/8
  f,8 d' d32 e f16
  f,8 d' d32 e f16
  c8 f f32 g a16
  g f e d c8
}
