\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
    c8 c32 b c d  c8 e16 c  f8 f f4
    e8  e32 d e f  e8 g16 e  a8 a a4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Andante"
  \time 4/4
 
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto non tanto"
  \time 6/8
 
}
