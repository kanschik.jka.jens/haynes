\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
 \override TupletBracket #'stencil = ##f
  bes4 es2 f4 g g2 \times 2/3 {f8 g as}
  g16 f es4. as4 g f e r2
} 

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Siciliano"
  \time 6/8
  bes8. c16 d8 d8. es16 f8
  es16. f32 g8 g
  g16 f f4
  c16. d32 es8 es  es16 d d4
} 

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro"
  \time 3/4
    es,4 g bes
    es g4. as16 bes as4 g f g f r
} 
