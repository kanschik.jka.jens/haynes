\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Soave [Tutti]"
  \time 6/8
  g8 g16 a b c  d8 g, e'
  d g c, b4.
  fis8 d'16 cis d8 r d16 cis d8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Soave [Solo]"
  \time 6/8
   \set Score.skipBars = ##t
   R2.*9 
   r8 g16 a b c  d8 g, e'
  d g c, b4.
  a8 d16 cis d8 b d16 cis d8
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
  r8 g b a
  g b d4~
  d8 c16 d e8 d~
  d g,[ b a]
  g
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Adagio"
  \time 4/4
  r2 r4 b~
  b4. c8 dis,4 r8 e
  a b16 c b8. a16 g fis e8 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Vivace [Tutti]"
  \time 12/8
  g4. d g r8 r a32 b c d
  e8 g, e' e g, e' e4. d
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Vivace [Solo]"
  \time 12/8
     \set Score.skipBars = ##t
   R1.*3 
   r2. r8 d, d  d d d
   fis d d  d d d  g d d  d d d
   b' d, d  d d d  fis d d  g d d
}