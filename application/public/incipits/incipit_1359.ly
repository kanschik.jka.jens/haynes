 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef "treble"   
                     \key bes\major
                      \tempo "1. Allegro [Oboe]"
                      \time 2/2
                         \set Score.skipBars = ##t
                         R1*9
                      r2 bes8 bes bes bes
                      bes16[ f d f] bes f d f c'8 c c c
                      c16[ a f a] c a f c'
}
\relative c'' {
\clef "alto" 
                     \key bes\major
                      \tempo "1. Allegro [Gambe]"
                        % Voice 2
                      bes4 f bes, r8 es'
                      d d d d d16[ c bes c] d c bes d
                      es8 es es es es16[ d c d] es d c es
                  }

 
 

 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 2/2
   \override TupletNumber #'stencil = ##f

    b8 b b b  
   \times 2/3 { b16[ d b]  }
   \times 2/3 { g16[ b g] }  
   \times 2/3 { d16[ g d] }  
   \times 2/3 { b'16[ d b] } 
   c8 c c c c32 d e d c b a g
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Vivace [Oboe]"
  \time 3/4
    r4 r f,
  bes8 bes16 a bes8 bes16 c d8 c16 bes
  c8 c16 bes c8 c16 d es8 d16 c d8
}

\relative c'' {
  \clef bass
  \key bes\major
   \tempo "3. Vivace [Gambe]"
  \time 3/4
  \partial 8
  f,,8
  bes bes bes bes bes bes
  bes a16 g f g f es d es d c
  bes8 f'
}

