\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante"
  \time 6/8
    f8. g16 f8  f4 a,8
    bes f d' c16 d32 es d8 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 4/4
  f4. g16 f bes8 d, es f
  f4. as16 g bes8 es, f g 
  f4. g16 f bes8 f es d
  }

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. All. Moderato"
  \time 2/2
  bes2 c d r4 bes4
  es2 d c bes c2.
}