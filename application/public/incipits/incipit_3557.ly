\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
      \time 2/4	
    d4 d8. d16
    d8. d,16 fis8. g16 a4 a8. a16
    a8. a16 b8. cis16
    d8. cis16 b8. a16
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
      \time 4/4	
      g8 b16. d32 g8 r  g,8 c16 e g8 r
      g,8 b16. d32 g8 r r16 a b a r a8 c,16
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Presto"
      \time 3/8	
          a'4. cis, d4 a8
          fis d e fis4 a'8 fis, d e d4 a'8
}
