 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "1. [Allegro]"
  \time 2/4
  \partial 8
  c8
  a16 c f4 g8
  a16 g f e f8 bes
  a16 g f e f8 c
  bes g'16 bes, a8 f'16 a,
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. [Largo]"
  \time 4/4
   d16. a32 f16. d32 f'8 f f e r4
   r8 a bes a16 g a8 f g f16 e 
   f8
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. [Allegro]"
  \time 6/8
  \partial 8
  c8
  c a f c'16 bes a g f8
  d'16 e f8 d c4
}
