\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. All. maestoso"
  \time 4/4
  d'2 a4 fis
  d2 cis
  b2. cis8 d e4 e8 fis \grace a8 g4 fis8 e a fis d4 r
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Rondo. Allegro Brioso"
  \time 2/4
  a'8.[ b16 a8 a]
  fis a r a
  \grace a8 g[ fis g a]
  g4 fis8 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro affectuoso"
  \time 4/4
  \partial 4
  d4
  d8.[ es16 c8. d16]  bes8.[ c16  a8. bes16]
  g2~ g8[ bes a c]
  bes8[ d c es]  d[ bes c a]
  g2
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Presto"
  \time 2/4
  \partial 4
    d8 d
    g4 g8. bes16 a4 a8. fis16
    g4 g8. bes16 d4 d8. c16
    bes4 bes8. a16 g4 g8. f16 d8 es es4
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro amabile"
  \time 4/4
  \partial 8
    c8 f4 g a bes
    a g2 a4 \grace c8 bes4 a8 bes c4. bes8
    gis a a4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andantino"
  \time 2/4
  \partial 4
  f8 f f4 \grace a16 g f g a
  g4 g16 e g bes
  \grace bes8 a g16 f bes8. a16 g16. e32 c8
}
  
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro con Gratioso"
  \time 4/4
  \partial 16
  r16
  bes2. es4
  d c bes4. a8
  bes4 es8 d c bes f'4 f2 f,4 r
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante con molto"
  \time 4/4
\partial 2
f8. g16 f8. g16
f4. bes8 d,4. g8
f es es2 e4 f4. es8 es4 d8. c16
\grace d8 c bes bes4
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro con Vaghezza"
  \time 4/4
  \partial 4
  d4 \grace e8 d8 c16 b b2 c8 d
  e fis16 g g2 fis8 e
  \grace e8 d4 c8 b a b c d
  c2 b4
}
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante con molto e Grazioso"
  \time 3/4
  d2 g4 fis r a
  c,4. e8 d c b4 r8 d e d a4 r8 d e d
  g,4 
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro con Gratioso"
  \time 4/4
  g'2 a4. f8 e4 r r2
  g4 gis a4. f8 e4 r r2
  c16 d e f   e f g a  g8 a b c
  \grace d8 c b b2 a4
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondo. Allegro Gratioso"
  \time 2/4
  \partial 4
  g'8. f16
  e8[ g d g]
  \grace f8 e8 d16 e \grace g8 \times 2/3 { f8[ e d] } c8 c d8. b16
  g4
}
