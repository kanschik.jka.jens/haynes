\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro Maestoso"
  \time 2/2
  \partial 16
  d,16
  g2~ g8. a16 b8. c16
  d4 d d d
  d2. e8. d16
  d2. g8. d16
  d8 c c b \grace d8 c4 b8 a
  \grace c8 b16 a b c d4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio [Tutti]"
  \time 4/4
    r16 a' fis d r a fis d
    r a'' g e r a, g e
    r a' fis d a d fis a g fis e d cis b a g
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Adagio [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
    R1*2
    a'1~
    a8 \grace g16 fis16 e32 d
    d8 d d4. dis16. e32
    e8 e~ e16 e fis g
    fis16. g32 a8 d,16. e32 fis8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegretto"
  \time 3/4
  g'4. fis16 g \grace a16 g8 fis16 e
  d4 d d
  d4. d8 \grace e16 d8 cis16 b
  c a b c d e fis g a4
}
