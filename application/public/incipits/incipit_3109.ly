\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran und Alt): Gottes Wort, das trüget nicht [BWV 167/3]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe da Caccia"
  \key a\minor
   \tempo "Duett. Andante"
   \time 3/4
   r4 r8 r16 e16 a gis a b
   gis4~ gis16 e fis d cis e g fis
   fis4~ fis16
   
   
   
 } 
 


\relative c'' {
<<
\new Staff { \clef "treble"   
  \set Staff.instrumentName = #"Sopran"
  \key a\minor
   % Voice 1
   \tempo "Duett. Andante"
   \time 3/4
   \set Score.skipBars = ##t
   R2.*6 r4 r4 e8. f16 d4 r8 r16 d16 f8. e16 c4 r4 r4
                     
                  }
   \new Staff { \clef "treble"
   \set Staff.instrumentName = #"Alt"
   \key a\minor
   % Voice 2
   \tempo "Duett"
   \time 3/4
   \set Score.skipBars = ##t
   R2.*6 r4 r4 c8. d16 b4 r8 r16 a16 gis8. a16 a4 r4 r4
                       
                  }
>>
}


 

