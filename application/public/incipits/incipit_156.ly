\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 2/2
      a4 r8 e'  cis a e' e, 
      a16[ gis a b] a8 a 

      b16 a b cis  b8[ b]


      cis4 r16 fis fis fis
      b,[ b cis d] e e e e
      a,[ a b cis] d d d d
}
