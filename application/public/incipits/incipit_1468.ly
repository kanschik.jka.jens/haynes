
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key b\minor
   \tempo "1. Gravement"
  \time 2/2
r4 b4 b8. cis16 d8. b16
cis4 cis cis8. d16 e8. cis16
d4 d8. b16 fis'2~ 
fis e~ e4

 
}

 
\relative c'' {
\clef "treble"   
  \key b\minor
   \tempo "2. Gay"
  \time 4/4
  b8 fis' d fis b, b'16 a b8 fis
  g e g fis16 e fis2
 
}

 
\relative c''' {
\clef "treble"   
  \key b\minor
   \tempo "3. Allemande"
  \time 4/4
  \partial 8
  r16 b16
  b8 a16 g fis e d cis d8 b d fis
  b, fis' b fis g g16 fis e d cis b
  ais4 fis r8
 
}

 
\relative c'' {
\clef "treble"   
  \key b\minor
   \tempo "4. Rondeau. Tendre. Gracieusement"
  \time 2/2
  \partial 2
  b8 fis' e d
  cis d e cis d4 cis8 b
  ais4 fis b r4

 
}

 
\relative c'' {
\clef "treble"   
  \key b\minor
   \tempo "5. Rondeau. Gay"
  \time 2/2
  \partial 4
  fis4 fis b, b b'8 ais
  b4. a8 g fis e d
  cis4 fis8 e d cis b cis 
  ais4 fis fis


 
}

 
\relative c'' {
\clef "treble"   
  \key b\minor
   \tempo "6. Gigue"
  \time 6/8
  \partial 2
  d8 d e fis
  cis fis cis cis d e
  d b b' b4.~
  ~b4. ais b4

 
}

 
\relative c'' {
\clef "treble"   
  \key b\minor
   \tempo "7. Passacaille"
  \time 3/4
  b4 d4. cis16 b
  cis4 fis, fis'
  b, e4. e8
  e d cis d e cis
  d b d4. cis16 b
  cis4 fis,


 
}

 
\relative c'' {
\clef "treble"   
  \key b\major
   \tempo "8. Carre"
  \time 3/4
  b4 dis8 e fis gis
  fis4. gis8 ais fis
  b ais gis fis e dis
  gis fis e4. dis16 e
  dis4


 
}

 
\relative c'' {
\clef "treble"   
  \key b\minor
   \tempo "9. Mineur"
  \time 3/4
  b8 fis b cis d e
  cis4. d8 e cis
  d b g'2~
  ~
  g8 fis e4. d16 e
  fis8 fis, b cis d e 
  cis4.


 
}


