\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro con brio"
  \time 4/4
  c4. e16 g c4 c \grace c8 b4 a8 g \grace g8 f4 e8 d
  c4 c16 d32 e f g a b c8 c c c
}
      
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 3/4
  g4 b a g e' d
  r8 d16 c r8 c16 b r8 b16 a
  g b' a g r g, g g r b' a g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 4/4
  g'4 r8 d, g4 b d r8 g, b4 d f4. e8 d4. c8
}
