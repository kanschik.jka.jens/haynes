\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Moderato [Tutti]"
  \time 2/2
  \partial 8
  f8
  d32 es f8. d32 es f8. d16 es f4 bes8
  f16[ d es c] d bes bes'8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Moderato [Solo]"
  \time 2/2
   d32 es f8. d32 es f8. d8 \grace c8 bes4 \times 2/3 {es16[ f g]}
    g8 f4 es8 es d r 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [Tutti]"
  \time 3/4
  g,8 es'' d g d,16 c bes a
  g8 es'' d fis, g bes
  a[ cis d e, fis]
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio [Solo]"
  \time 3/4
  g8 es' d c bes a
  bes g g'4 fis
  g8 es d c bes a
  bes g r bes a c
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro assai"
  \time 2/4
  \partial 8
  f,8
  bes bes bes bes
  \grace c8 bes32 a bes8. \grace c8 bes32 a bes8.
  \grace c8 bes32 a bes8. g'8 f es d
}