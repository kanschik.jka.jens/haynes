\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "Aria 1"
  \time 3/4
  r4 bes g d'4. c8 bes a
  bes2 r4
}
