
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
  \tempo "1. o.Bez."
  \time 4/4
  g'4~ g16 b a g fis8 d r4
  d4~ d16 d e fis g2~
  g8 g g16 e cis a
}


\relative c'' {
  \clef treble
  \key g\major
  \tempo "2. Allegro"
  \time 2/4
    d8 g, r d'
    d fis, r d'
    d c16 b c8 b16 a
    b a g a b  a b cis
}

\relative c'' {
  \clef treble
  \key g\major
  \tempo "3. Largo"
  \time 3/4
        << {
      % Voice "1"
      R2. 
      r8 b e g  fis e
      fis4 r r
      r8 b fis a g fis g4
         } \\ {
      % Voice "2"
      r4 b, b
      e2 r4
      r b b
      fis'2 r4
      r
      } >>
}

\relative c'' {
  \clef treble
  \key g\major
  \tempo "4. Allegro"
  \time 3/8
    \partial 8
    d8
    b g16 a b c
    d8 g d
    d a' d,
    d b'
}
