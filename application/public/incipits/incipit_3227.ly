\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Jesus nahm zu sich die Zwölfe [BWV 22/1]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                      \override TupletBracket #'stencil = ##f
                      \override TupletNumber #'stencil = ##f
                      r8 d8 g g g16 a fis g a4~ 
                      a16 bes g a bes4~ bes16 bes a bes c a bes g
                      fis8 a16 bes32 c es,8 d r8
                 
                 
               
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine 1"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \set Score.skipBars = ##t
                     \key g\minor
                        % Voice 2
                 bes4~ bes16 c a bes c4~  c16 d bes c
                 d4~ d16 es c d es8 g fis g a,4 r8
                       
                
                  }
                  
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \set Score.skipBars = ##t
                     \key g\minor
                        % Voice 3
                 g8 g16 a bes4. a16 bes c4~
                 ~
                 c8 bes16 c d4. c16 bes a8 g
                 d'4 
                       
                
                  }
                  
                 
>>
}


    
    \relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Tenor"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                      \override TupletBracket #'stencil = ##f
                      \override TupletNumber #'stencil = ##f
                    R1*6 r8 d4 es8 a, bes16 c d8 d,
                    g16 b a b c4~ c16 bes a bes bes8. a16 a4 r4
                 
                 
               
                      
                     
                  }
                  
\new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Bass"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \set Score.skipBars = ##t
                     \key g\minor
                        % Voice 2
               R1*8 r4 d,2.~
               ~d16 c32 bes a bes g16 fis4 r8
                       
                
                  }
                  
                 
                                   
>>
}