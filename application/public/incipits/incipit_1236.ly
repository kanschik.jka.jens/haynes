 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 2/4
    g8 c, c'4
    c8 b16 a b8 c
    d g, as' g16 f
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Allegro"
  \time 2/4
  \partial 4
  r16 c es c
  as' as as as  as as as as
  as32 g f es d8 r16 d f d
  g g g g g g g g
  g32 f es d c8 r16
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "3. Adagio"
  \time 2/4
  r4 g16 as as g
  g bes bes g g as as g f8 es r4
}

\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "4. Tempo di Menuet"
  \time 3/4
    c4 b c d2 g,4
    d' c d es2 g,4
}