\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key f\major
  \override TupletBracket #'stencil = ##f
    \time 3/4
    \tempo "1. o.Bez."
      a'4. g8 f4
      e2 d4 g2 f4
      f e e
}



