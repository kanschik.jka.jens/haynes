\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Recitativ (Bass): Und da du, Herr, gesagt [BWV 171/5]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key b\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                      \set Score.skipBars = ##t
                      r1 
                      \time 3/8
                      R4.*10
                      \time 4/4
                      fis2 a~ a4 dis, e2~e1~ e4 ais,
                      
                 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key b\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                     \set Score.skipBars = ##t
                      r1 
                      \time 3/8
                      R4.*10
                      \time 4/4
                      a2 c fis, g~ g cis~ cis
                   
                       
                        
                
                  }
    
\new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Bass"
                     \key b\minor
                     \set Score.skipBars = ##t
                        % Voice 3
                     \set Score.skipBars = ##t
                      r4 r8 d,,8 fis g a d,
                      \time 3/8
                    c'4 r8 
                    b16 c d8 b 
                    g d4 e8 c' a
                    fis e d
                   
                       
                        
                
                  }
                  
>>
  }
