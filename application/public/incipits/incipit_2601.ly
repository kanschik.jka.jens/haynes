 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 2/2
 \partial 8
d8 g d bes g r d' g d bes g r d' bes' d, d16 cis r8 
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Largo"
  \time 2/2
      << {
      % Voice "1"
      r2 a'8 g16 fis bes4
      a4. a8 d,
         } \\ {
      % Voice "2"
      r4 es8 d16 c fis4 g
      g8 f16 es f2
      } >>
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Allegro"
  \time 12/8
  \partial 8
  d8 g fis g g fis g es4. d
  c bes4 a8 g4.
}
