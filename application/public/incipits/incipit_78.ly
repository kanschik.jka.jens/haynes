\version "2.16.1"
         #(set-global-staff-size 14)
         
\relative c'' {
  \clef treble  
  \key c\major
    \time 4/4
    \tempo "1. Largo"
    c16 b c d c b c e d c d e d a' g f e d c8 a'4~ a16 a g f g4~ g16
}

  

\relative c'' {
  \clef treble  
  \key c\major
    \time 4/4
    \tempo "2. Allegro"
    c16 b c d e d e c g'4 r8 g e16 f e f g8 f16 e d8 g, r16
}
  

\relative c'' {
  \clef treble  
  \key c\major
    \time 3/2
    \tempo "3. Grave"
    e2 a,2 a'~ a2. fis8 g s2 a2 e a~ a g4 f g a f1 
       
}



\relative c'' {
  \clef treble  
  \key c\major
    \time 6/8
    \tempo "4. Allegro assai"
    r8 c c e g, g' e c g' g fis8. e32 fis g8. g16 a g f e d4 
}


