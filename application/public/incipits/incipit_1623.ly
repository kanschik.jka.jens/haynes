 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "o.Bez."
  \time 3/4
  r4 r8 e, fis gis
  a gis a b c d
  e4 d c
  b8 e gis, a16 b e,8
}
