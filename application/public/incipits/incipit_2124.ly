\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allemande. Allegro"
  \time 4/4
    a'4 r16 a gis fis  e8 a cis, d16 e
    d8 fis b, cis16 d
}


\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Aria. Andante"
  \time 6/8
    a16 e a32 b cis16 cis32 d e16 e4 r8
    a,16 e a32 b cis16 cis32 d e16 e4 r8
}



\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Gigue"
  \time 12/8
    e8 fis e  e fis e e4 r8 r r e
    a b a gis a gis
}



\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Sarabande"
  \time 3/4
    a'4 gis4. gis8
    a4 e4. e8
    fis a e4. d8
}



\relative c'' {
  \clef treble
  \key a\major
   \tempo "5. Gavotte"
  \time 4/4
  \partial 4
  e8 d
  cis4 e a, a'
  gis fis8 gis e4 d cis
}



\relative c'' {
  \clef treble
  \key a\major
   \tempo "6. Menuet"
  \time 3/4
    e4 cis a cis8 d cis d e4 fis gis a
}

