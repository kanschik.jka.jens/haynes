
\version "2.16.1"

#(set-global-staff-size 14)

\relative c''' {
  \clef treble
  \key a\major
   \tempo "1. Allegro moderato"
  \time 2/2
  \partial 4
  a8 fis fis e e2 fis8 d
 d cis cis2 e8 cis
 cis b d b b a a gis \grace gis8 a2 r2 

}

\relative c''' {
  \clef treble
  \key e\major
   \tempo "2. Adagio"
  \time 2/4
  b4 a16 gis fis e 
 e dis dis8 dis16 e fis gis
 \grace b8 a4 gis16 fis e dis
 dis e e8 gis16 e b d
 dis16 cis cis8 a'16 fis e dis e16

}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "3. Rondo Allegretto"
  \time 2/4
 a8 a a16 gis fis e
 fis8 fis16 a e4
 d8 d d16 cis b a 
 gis8 dis'16 e e,4
  

}

