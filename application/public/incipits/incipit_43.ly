\version "2.16.1"

#(set-global-staff-size 14)

\relative c' {
  \clef violin
  \key c\major
   \tempo "Larghetto"
  \time 2/2
   \partial 8
    g'8
    c4 d8. b16  e4 f8. d16
    g4. f8 e4
}
