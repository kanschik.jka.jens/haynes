 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Vivace"
  \time 3/4
  r8 f,, a c f a
  c c c c c16 bes a g
  a8 f r4 r
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Adagio"
  \time 2/2
  f8 d cis d  g d cis d
  a'[ g16 f] e8 d cis[ b] a4
  d8 a g a bes4 r
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Allegro"
  \time 3/8
  f,8 a f a c16 bes a8
  a f c c4 r8
}

