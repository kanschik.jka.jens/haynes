 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 12/8
    r4 r8 r r f8 bes4 f8 bes8. a16 g8
    f8. es16 d8 d4 f8 g4 f8 g8. f16 es8
    f8. es16 d8 d4 bes'8 bes4 a8 a4 g8
    f1.
}
