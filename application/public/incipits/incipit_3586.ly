 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 4
    bes4
    es4. g16 f es8 d c bes
    \grace c16 bes8 as as4 r as
    d8 es f g as as, as as
    \grace bes16 as8[ g]
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Andante"
  \time 3/4
  g'4 es f g2 as8 g
  f4 d es
  f2 g8 f
  es8. f16 g4 as
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Rondo"
  \time 2/4
  \grace as16 g8. as16 bes8 bes
  \grace bes16 as8 as f r
  \grace g16 f8. g16 as8 as
  \grace as16 g8 g es r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}