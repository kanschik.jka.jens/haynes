\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. o.Bez."
  \time 2/4
  \set Score.skipBars = ##t
  b16 gis8.  a16 fis8.
  gis8 e'4 d16 cis
  b e, d e a8 gis
  gis fis
}
