\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. [Vivace]"
  \time 2/2
  g8 fis g a  bes a bes c
  d4 g, g'4. g8
  fis4. d8  g d es f
  es4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Rondeau"
  \time 2/4
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Aria Andante"
  \time 2/4
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Arietta"
  \time 2/4
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "5. La Sperenza"
  \time 2/4
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "6. Menuett altern"
  \time 2/4
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "7. Duetto"
  \time 2/4
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "8. [Duetto]"
  \time 2/4
}
