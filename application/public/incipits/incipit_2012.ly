\version "2.16.1"
         #(set-global-staff-size 14)

         
         \relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. o.Bez."
  \time 4/4
    r8 e e e  e c16 d e8 b
    c16 b c d  b e d e  c b c d e d e f
    g8 g g g   g e16 f g8 d
}
         