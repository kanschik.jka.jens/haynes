\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro "
  \time 4/4
  f4. f8 g f es d
  d4. d8 es d c bes
  bes a a4. c8 es a, c4. d16 c bes4 r
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Rondo. Allegro"
  \time 2/4
  \partial 4
  bes'8 bes
  bes16 a g f g8 a
  bes f f f
  g16 a f g es f d es
  c4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro molto"
  \time 4/4
  \grace { b16[ d]} g4. \grace { a32[ g fis g]} a8 b fis g e
  cis d cis4. e16 d c b a g
  fis8 g a b c d e d
  d4. c8 b4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Rondo. Allegro"
  \time 2/4
  \partial 8
  g'8
  \grace b16 a8 g g g
  \grace b16 a8 g g g
  c c b b a16 g fis e
  d8 g
}


\pageBreak

\markuplist {
    \fontsize #3
    "Duo III " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio espressivo"
  \time 3/4
  f2 es8. d16 d4 r d8.[ \grace {es32[ d cis d] } f16]
  es4. bes'8 g es
  d8. es32 d cis4 r8.
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro assai"
  \time 2/2
  \partial 4
  a4
  d8 f e g f a g bes
  a2. \grace a16 g8 f16 g
  f4 \grace { e32[ d cis] } d4 e \grace { d32[ cis b] } cis4
  d4 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Variatione. Tempo giusto"
  \time 6/8
   \partial 8
   d8
   e4 g8
   cis,4 e8
   d4 fis8 a d b
   a g e g fis d fis4. e8
}
