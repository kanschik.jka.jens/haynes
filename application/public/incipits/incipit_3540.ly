 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
  bes16 as g f
  es2. g4 \grace g8 f4 es r g
  \grace g8 f4 es r
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Aria. Cantabile"
  \time 2/4
  f4 es16 d c bes
  \grace d8 c8 bes4 a8
  bes8. c16 d8 r16 
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Menuetto"
  \time 3/4
  \partial 4
  bes4 es2 g8 f
  f es es4 es
  f2 as8 g g f f4 
}



\relative c'' {
  \clef "treble"   
    \key es\major
    \tempo "4. Moderato Variat"
    \time 2/4
    \partial 8
    bes'16 g
    es8 es4 bes'8
    c c,4 d8
    es8. f32 g as8 c bes as g
}
