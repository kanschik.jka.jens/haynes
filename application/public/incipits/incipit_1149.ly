 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Ouverture"
  \time 4/4
      gis'4 r8. b,16 e4 r8. e,16 cis'4 r8. b16 \grace b8 cis4 r8. d16
      \grace cis8 b2
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Courrante"
  \time 3/2
  \partial 8
    b8 b4. e8 dis4 e  \grace cis8 b4. a8
    gis2 r4 b cis4. d8
    \grace d8 cis4. b8
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Loure"
  \time 6/4
  \partial 4.
    e,8 fis4
    \grace fis8 g4. a8 b4 \grace a8 g4. fis8 e4
    b'2. b 
}
