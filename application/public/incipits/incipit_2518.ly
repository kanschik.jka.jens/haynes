 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #""
                     \key g\major
                      \tempo "o.Bez."
                      \time 3/4
                      \set Score.skipBars = ##t
                        R2.*4
                      % Voice 1
                      r16 d c d b c a b  g a fis g
                      e4 r r
                      r16 fis' e fis d e c d  b c a b
                      g4 r r
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violins"
                     \set Staff. shortInstrumentName = #""
                     \key g\major
                      \set Score.skipBars = ##t
                        R2.*5
                      % Voice 2
                      r16 e' d e  c d b c  a b g a
                      fis4 r r
}
>>
}


\relative c'' {
  \clef "treble"
  \set Staff.instrumentName = #"Soprano"
  \key g\major
   \tempo "o.Bez."
  \time 3/4
     r4 r d
     c4. d8 a8. c16
     fis,4 d g
     a8 d, c'4. d8
     b4 r r
}