 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro Con Gracioso"
  \time 4/4
  bes2 f
  d'2~ d8 es c d
  bes4 bes \grace c8 bes4 a8 bes
  g4. es8 g4 r4
 
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Rondo Allegretto"
  \time 2/4
  \partial 4
   f8 f
   f8. g16 c,8 d
 \grace f16 es8 d d16 c c bes
  bes a a bes c16. d32 es16 d
 \grace d16 c4
}