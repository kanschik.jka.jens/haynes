\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.bez."
  \time 3/4
  \partial 4
    g4
    c4. b8 c4
    d4. c8 d4
    e c
}
