 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  c,4 e2 g4~
  g d2 e4 f
  a,2 b4
  c16 b c d c8 c c4 r
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio"
  \time 2/4
  \times 2/3 { a,16[ a a]} \times 2/3 { a16[ a a]}
  \times 2/3 { a16[ c b]} \times 2/3 { a16[ e' c]}
  \times 2/3 { b16[ b b]} \times 2/3 { b16[ b b]}
  \times 2/3 { b16[ d c]} \times 2/3 { b16[ f' d]}
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Rondeau"
  \time 3/4
  e4 e8 d f e
  g4. \times 2/3 { a16[ g f] } e8 r
  \grace e8 d4 d8 c d e
  c16 d c b c g a b c b c d
}

