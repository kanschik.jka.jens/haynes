\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. Adagio"
  \time 3/4
  r4 bes2~ bes2.
  bes8 c a4. f'8 f2.
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "2. Allegro"
  \time 4/4
     r8 bes bes bes f c' c c
     a f' f f bes,4 r8
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "3. Sarabande"
  \time 3/4
    f4 bes, bes8 c
    a4. a8 bes4
    ges' c, c8 es
    a,4. g8 f4
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "4. Giga"
  \time 3/8
  \partial 8
    f8
    bes, c des c4 f8
    bes,8 c bes a4 f'8 bes,4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo"
  \time 2/2
  g8. g'16 f es d c bes8. es16 d c bes a
  g8
}

\relative c' {
  \clef bass
  \key bes\major
   \tempo "2. Allegro [Bass]"
  \time 4/4
  bes8 bes, bes bes' a g16 a f8 d
  
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro [Oboe]"
  \time 4/4
    \set Score.skipBars = ##t
   R1*3 
   r2 r8 bes d f
   bes,4 r8 d16 es f8 f
 
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Adagio"
  \time 4/4
   g8 a bes a16 g es'2~
   es8 d16 es c8. d16 bes4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Vivace"
  \time 3/4
    r8 bes bes bes bes bes
    c c c c c c
    d bes d f d bes
    es c es g es c
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "5. Menuet"
  \time 3/4
  bes4 d f
  bes a8 g f es
  d es f4 g
  c,8 bes c d c4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "6. Allegro"
  \time 6/8
  \partial 8
    f8
    bes, c d bes c d c4. f,4 c'8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "7. Gavotta"
  \time 2/2
  f8 es d c d4 r8 f
  bes, c16 d c8. bes16 a4 f
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 4/4
  es2~ es8 f16 g as,8 g16 f g4. es'16 f d4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Vivace"
  \time 3/4
     r4 bes bes es2. es4 f8 es d f
     g4 as8 g f es d4
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Adagio"
  \time 4/4
   c4 as' b, f'
   bes, g' as, r
   f'8 bes,16 c bes8 g'
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Vivace"
  \time 4/4
     \set Score.skipBars = ##t
   R1*2
   r2 r4 r8 bes
   es bes g bes es4 r8 f
   g16 f es f  g as g f
}


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "5. [Giga]"
  \time 12/8
 \partial 8
   bes8
   es4 g8 es4 g8  bes, c bes bes4 f'8
   g4 bes8 g4 bes8
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "6. Allegro"
  \time 2/4
   bes4 es
   d8. c16 bes8. as16
   g8 g' f es
   d4 bes
}
  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 3/4
  a4 c e a, a'2 a8 g g2 f8 e e4.
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Vivace"
  \time 4/4
   r8 e e e  f f f e16 d e8 f16 g  a g f e
   d8 e16 f
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro"
  \time 4/4
 \partial 8
     e8 e16 f e d  c d c b  a4 r8 e'
     a c, d c16 d e4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Adagio"
  \time 4/4
     e4 r8 e f4 r8 f8
     fis4 r8 fis g4 r8
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "5. Giga"
  \time 12/8
 \partial 8
     e8 e4. d c8 d c  b c d
     c4. b c8 d c b c d
     c d e
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 3/4
  b2.~ b8 e e4 dis
  e fis g g fis8 e dis4 e e4. dis8 e4 r r
}

\relative c'' {
  \clef bass
  \key e\minor
   \tempo "2. Vivace"
  \time 4/4
   e,,4. b'8 a g a fis
   \clef treble
   e''4. b'8 a g a fis
   g e r g fis b e, a
   d,4
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Adagio"
  \time 4/4
   b4 e8 d c4. e8
   a4 c,8 d
   b4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 4/4
      e,8 fis g a  fis g a b
      g4 e' e4. dis8
      e fis g a fis g a b
      g[ a]
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "5. Allegro"
  \time 2/2
 \partial 8
   b8
   e g g e fis4 r8 fis
   g b b g a4 r8 a
   b8.[ g16]
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  a'2~ a16 b a b a g fis e
  fis8 g a b16 a a4 r8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Vivace"
  \time 4/4
    \set Score.skipBars = ##t
   R1*2
   r2 r4 r8 a8
   d16 cis d e  fis e fis g  a g a b  a b a b
   a8[ d,]
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Adagio"
  \time 3/4
 \partial 4
   fis4
   g a b dis,2.
   e4 fis g
   g fis8 e dis4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Allegro"
  \time 3/8
 \partial 8
   d8
   d fis a
   d,4 r8 d fis a
   d,4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Adagio"
  \time 4/4
   dis4~ dis16 fis g fis e2
   a4. g16 fis g8 e16 d cis8 b
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "6. Bourée"
  \time 4/4
 \partial 8
   a'16 g fis8 g a g16 fis  e d cis b a8 e'
   fis[ g]
}
