 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  g4~ g16 b a g  d' c d e d8 c16 d
  b a b c b c b c a g a b a d c d
  g, fis g a g b a g fis8 d r
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 2/2
  g8 g g16[ a b g] d'8 d d[ e16 fis]
  g8[ g,]
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Adagio"
  \time 4/4
  b8 e16. dis32 e8. fis16 dis4. fis8~
  fis e16 dis e fis e fis e8 dis r
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. o.Bez."
  \time 6/8
  g16 fis g a b g  d' cis d e fis d
  g8 g g fis e16 fis d8
}

