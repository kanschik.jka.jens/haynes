 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  f8 e f e f e f e f
  g16 f e g  a g f a  g f e g  a g f a  
  g4 r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 6/4
  a'8 g a bes c4 c bes a
  g8 f g a  bes4 bes a g
  a8 g a bes c4 c bes a
}
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. o.Bez."
  \time 4/4
  \partial 8
  c8 f16 f, f f    a f f f   c' f, f f   a f f f
  f'8 g16 a g f e s e8 c r4
}
 