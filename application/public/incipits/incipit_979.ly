\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.S."
  \time 4/4
  \partial 8
  f16 d
  bes8 bes4 bes8 \grace {a16[ bes c]} bes4  f'16[ d]

}
