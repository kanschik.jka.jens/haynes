\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Brilante"
  \time 2/2
    a'4. gis16 fis  e8 d cis b
    a4 a e r
    a cis8 r b4 e8 r
    \grace d8 cis4. b8-. a4 r
}


\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Andante"
  \time 2/4 	
  \partial 8
    e,16 a cis8 cis b b
    a8. gis16 a b cis d
    e8 e fis fis b,4
}

