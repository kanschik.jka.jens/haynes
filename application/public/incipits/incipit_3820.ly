 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Andante"
  \time 3/8
  e4.~
  e
  e16 e e d d c
  c c c gis gis a
  a4 b8
}


\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "2. Allegro"
  \time 6/8
  \partial 8
  e8
  e a e  \grace g8 f8 e f
  e4.~ e4 e8
  e a e \grace g8 f e f
  e a e
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "3. Allegro"
  \time 3/4
  e4 \grace d8 c4 \grace b8 a4
  \grace a'8 gis2 a4
  b e, d
  \times 2/3 { c8[ b a]} a2
}

