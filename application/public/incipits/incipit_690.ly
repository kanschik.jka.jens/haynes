
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Apothéose de Lulli"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key g\minor
   \tempo "1. Gravement"
  \time 2/2
d4 c bes a
bes a g fis
\grace fis8 g2 a
fis1

 
}

