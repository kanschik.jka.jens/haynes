\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4		
   R1
   g'2 f8 e d e
   g f16 r f8 e16 r e8 d16 r d8 c16 r
   c8 b d fis, g4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 3/4
  c8 a' g f e d
  c4~ c16 bes a bes a8 r
  d fis g a bes g f4 e

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Andantino"
  \time 2/4
  \partial 8
  g'8
  g a f e
  d4 e8 d16 e
  g8 f16 r f8 e16 r
  e4 d8
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 6/8	
  f4. f8 g a
  bes4 a8 g4 f8
  g4 a8 bes4 g8
  f8. g16 f8 e4
 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Romance Andante"
  \time 2/2
  \partial 2
    bes8 r bes r
    bes4. c8 d4 a c bes a8 r bes r
    c d es d c4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo. Allegretto"
  \time 2/4
  c4 bes16 a bes g
  a8 g f g a4 c16 bes a g
  f4 e8
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Duo III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4	
  \partial 4
   f, f' es d c
   es8 d d4 r8 bes bes' g
   f4 es d c es8 d d8
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio"
  \time 3/4
  g'4 f g bes as g
  f8 es d es f g
  g4 f

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Rondo. Allegretto"
  \time 2/4
  \partial 8
    d8
    f es c a
    g' f es d
    c d es c d16 c d es d8
}
