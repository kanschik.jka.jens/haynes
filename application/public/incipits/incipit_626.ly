
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Prima"
    }
    \vspace #1.5
}
 
\relative c'' {
<<
\new Staff { \clef "treble"   
  \key e\minor
   \tempo "1. Adagio"
  \time 4/4
% Voice 1
\set Score.skipBars = ##t
   R1*2 r2 r8 r16 e16 e8 dis16 c
b8 a16 g a8 b g[ \grace fis8 e8]

}

\new Staff { \clef "bass"
\key e\minor
 % Voice 2
r8 r16 e16 e8 dis16 c b8 a16 g a8 b
g \grace fis8 e8 r16 b'16 a b g g fis g dis8 b
e16 b c a b8 b, e4 r8 r16

                  }
>>

 
}



\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 2/4
\partial 8
e8 dis e b g'
g fis r8 b16 a g8 fis e dis e4 r8
 
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Affettuoso"
  \time 3/8
\partial 8
e8 g fis16 g a8
g fis e
dis8. cis16 b8
e e16 d c b 
c8. b16 a b 
g a a8. g32 a 
b4
 
}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Giga. Allegro"
  \time 6/8
\partial 8
e8 
g fis e dis e fis
b,4.~ b4 c8
b a g fis g a 
g fis g e4
 
}
 

 

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Seconda "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Moderato"
  \time 3/8
\partial 8
e8 cis8. b16 cis a
b8 e, a 
b16 cis d b e d 
cis8 \grace b8 a8 

 
}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 2/2
\partial 8
a16 gis 
a8 e16 fis e8 a16 gis
a8 e16 fis e8 a16 gis 
a8 a a a 
 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Aria Gracioso"
  \time 3/8
cis16 b cis d cis d
e4. 
fis16 e fis gis a b 
gis8. fis16 e8
d16 cis b a gis fis e8
 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Presto"
  \time 2/4
e4 a
gis a8 b
e,4 d 
cis b d8 cis d cis
d cis b a cis4

 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Terza la Telles d'acosta"
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Moderato"
  \time 3/8
\partial 8
g16 a
b8 c16 b a g
a8 d, g8~ g fis16 e d c
b8 a

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
b4 c8 d
g,4 a8 b
a4 b8 g
fis4 d
e fis8 g
d e c e 
d4 c b a 

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 2/4
d4 bes'8 a 
g4 a8 bes
a4 g8 fis
g4 d
es c8 es
d [fis g d]
d [c bes a] bes4 g

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 3/4
b16 c d8 d d d d 
e4 d r4
c4 b r4
a4 g r4
g'16 a b8 b b b b 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonate IV."
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio"
  \time 4/4
\partial 16
e16 e8. a16 g f e d c8. b16 c d e a,
gis8[ \grace fis8 e8] 

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro"
  \time 2/4
\partial 8
a8 e'[ a, gis a]
c16 b a gis a8 a
e'[ a, gis a] 
c16 b a gis a8 c16 d
 e8[ e e e] 

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro presto"
  \time 2/4
a4 c
b4. f'8
e4 d 
c b 
c8[ e c e] 
b[ e b e] 
a,[ c b a]
gis4 e



}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Quinta "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Moderato"
  \time 2/4
\partial 16
g16 g8. c16 b[ a g f]
e8 d16 g f[ e d c]
b8[ \grace a8 g8]

 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 6/8
\partial 2
e8 e f d
e f d e f d8
e d c g' f e 
a b c f,4 e8 d4

 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 6/8
\partial 2
es8 es d c
g' f as g c b
c4. as8 g f
f es d es d c 
b4
 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "4. Gavotta"
  \time 2/4
\partial 4
g8 c,16 g'
a8 c,16 a' g8 c,16 g'
a8 c,16 a' g e f d
e c d b c d e f 
e8 d
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Sesta "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Moderato et Gracioso"
  \time 3/4
\partial 4
d4 g bes8 a g fis
g2 es8 d
d4 c8 bes c d 
bes4 \grace a8 g4

 
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Un poco Allegro"
  \time 6/8
\partial 2
d8 d c d 
g,4 es'8 es d es 
c4 d8 d c d 
es d c bes a g 
fis4
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Un poco Allegro"
  \time 6/8
\partial 2
b8 b a g 
c4 b8 b c d 
fis,4 d8 g d g
a d, a' b a g a4
 
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "4. Allegro presto"
  \time 2/4
g4 fis
g4. es8 
d[ c bes a]
bes4 \grace a8 g4
bes16 c d8 d d 
bes16 c d8 d d  
fis,4 g a2
 
}


