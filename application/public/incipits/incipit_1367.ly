 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
    g4 r8 b, c4 d
    g,16 a b c  d e fis d   g a b c  d e fis d
    g8 fis16 e d8 c   b g16a b8 c
    d e16 d c8 b a d, r
   
}

 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \set Score.skipBars = ##t
  R1*10 r2 r4 r8 d
  b16 c b a  g d' e d  c b a b  c e d c
  b g' fis g
   
}

 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Adagio"
  \time 3/4
    R2. e2 r4
    b2 g'4~ g fis4. e8
    dis4 b2
   
}

 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
    g4 b d g8 d4 g8 g8. fis32 g
    a8 d,4 a'8 a8. g32 a
    b8 d,4 g8 g16 a b8 a 
}

 