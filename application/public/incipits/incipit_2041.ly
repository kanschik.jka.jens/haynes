\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatriéme Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Lentement"
  \time 4/4
  a8 c d e16. d32 a8 \grace b8 a8 d16. f32 e16. g32
  f8 \grace e8 e4

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Air en Musette. Rondeau. Gracieusement"
  \time 6/4
  \partial 2.
  a2 cis8 d 
  e2 a,4 fis'2 a,4
  e'2 a,4 a' gis fis
  e d cis b4. cis8 \grace b8 a4
  b2 e,4

}

\relative c''' {
  \clef treble
  \key a\minor
   \tempo "3. Courante"
  \time 3/4
  \partial 4
  r8 a8 a8.[ d,16 e8. d16] c8. b16
  c8.[ e16 d8. c16 b8. a16]
  gis8.[ f'16 e8. d16] c8. b16
  \grace b8 c4

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Gavotte"
  \time 2/2
  \partial 2
  a4 e'
  e \grace d8 c4 d e
  f \grace e8 d4 \grace c8 b4 \grace a8 gis4
  a4. b8 b4. a8 a2

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "5. Sicilienne. tres lentement"
  \time 6/4
  \partial 4
  a4 c b a b a gis
  a2 r8 b8 b2. \grace {a16[ b]}
  c4 b a 

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "6. Paysanne. Gayment"
  \time 2/2
  \partial 4
  a4 e c' b a
  e'2 d8 e f d 
  f e d c d e f d
  f e d c 

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquiéme Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Tres lentement"
  \time 4/4
  \partial 8
  r16 b16 b8. g'16 g8. fis32 e
  fis8. b16 dis,8 \grace c8 b8
  fis'8. b,16 a8. g32 fis \grace fis8 g8 e16. fis32 g16. a32 g16. a32
  \grace a8 b8.

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixiéme Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Lentement"
  \time 3/2
  fis2 ais,2. b4
  cis4. d8 e4. fis8 g4 fis8. e16
  fis2 fis2. fis4
  fis2 \grace g8 e2. e4 e2. d4 cis b ais2

}


