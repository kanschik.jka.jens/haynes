 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Adagio"
  \time 4/4
  r4 r8 bes f' f f es
  d16. c32 bes8 r f' g f16. bes32 f8 es16 d32 es
  d4
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegro"
  \time 4/4
  bes'8 bes, r16 f' g a bes8 bes, bes' bes,
  bes' bes, bes' bes, bes' bes, r f'
  bes a16 g f8 es d16 c bes8
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Largo"
  \time 4/4
  d4~ d16. g32 fis16. g32 d4~ d16. g32 fis16. g32
  c,4~ c16. es32 d16. c32 bes16. a32 g8
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Allegro"
  \time 2/4
  \partial 8
  f8 d4 es f bes,8 bes'
  a g f es \grace es8 d4.  g8
  f es d c
  bes
}