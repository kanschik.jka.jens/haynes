 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. o.Bez."
  \time 3/4
  f8 f4 f f8
  e8. f32 g f4 r8. c16
  c4. bes'8~ bes as16 g
  as4 g r
}
