\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 2/2
  \partial 16
  c16
  f4. g16 a f4 r8 r16 c
  g'4. a16 bes g4 r8 r16 c,
  bes'16 a8. g16 f8. a16 g8. f16 e8.
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Minuetto. Allegro"
  \time 3/4
c2 f16 e f g
a4 e f
c2 f16 e f g a4 e f
d2
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Larghetto"
  \time 2/2
  a1 cis2. fis16 e d cis
  cis b8. b4 r d
  cis8. d32 cis e8 r16 a, b8. cis32 b d8 r16 gis,
  b a gis a a4
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Presto"
  \time 3/8
  a4 \times 2/3 { a16[ b cis] }
  \grace cis8 b4 r8
  b4 \times 2/3 { b16[ cis d] }
  \grace d8 cis4 r8
  
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Spirituoso"
  \time 2/4
  a'4. fis16 a
  a e e4 g8
  fis \grace b8 a16 g32 fis e8 \grace a8 g16 fis32 e
  d4 c
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Moderato"
  \time 3/4
  a'2 \grace g8 fis4
  \grace e4 d2 r4
  b~ b8. cis16 \grace e8 d8 cis16 b
  a4 d r
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. All. moderato"
  \time 4/4
    \partial 8
    es,8
    bes'4. c16. bes32 bes4. c16. bes32
    es8 as,4 g8 f8. c'16 bes16. as32 \grace as8 g16. f32
}
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Minuetto. Gratioso"
  \time 3/4
  bes2. c8. d32 c es4 c
  bes es es
  f es8 d es8. bes16
  bes'4 as g
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante"
  \time 2/2
  \partial 16
  f16
  f8 es es d d4 r8 r16 d
  d8 c c bes bes4 r8 r16 bes16
  c4. d16 es
  g,4 a bes bes2
}
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Minuetto. All. assai"
  \time 3/8
  bes4 \times 2/3 { d16[ c bes] }
  f'8 \times 2/3 {f16[ g e] } f8
  g4 \grace bes8 \times 2/3 { a16[ g a] }
    bes8 f r
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trio VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Largo e Sostenuto"
  \time 4/4
  a'4 e2 cis8. a16
  d8. e16  b8. d16  e,4 d'
  \grace e8 d4 cis r
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 3/8
  a4. \times 2/3 { cis16[ d cis]} e8 a
  e,4 d8 cis r r
  r e a
  r fis d'
  gis,16 b e d cis d \grace d8 cis4 r8
}
