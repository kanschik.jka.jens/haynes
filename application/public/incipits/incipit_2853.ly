 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 4/4
    \partial 8
    bes16 c
    d8 bes16 c d8 d es d4 c16 bes
    es8 d4 c16 bes es8 d4 e16[ fis]
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Largo"
  \time 12/8
  d8. es16 d8 d e16 fis g a  bes8. a16 g8 g,4 g'8
  c,8. d16 c8 c16 es d c bes a bes8. a16 g8 g4.
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Allegro"
  \time 3/8
  d8 d8. d16 d8 d8. d16
  g8 g8. a16
  fis8. e16 d8
}

