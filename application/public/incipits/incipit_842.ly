\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef bass
  \key f\major
   \tempo "1. Largo"
  \time 4/4
      r16 c,, f e  f g a bes c8 f, r16 c' f es 
      d c bes a bes a g f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/4
    \set Score.skipBars = ##t
   R2*3
   c16 bes a bes  c8 f
   c16 bes a bes  c8 f
   d16 es c d  bes c a bes
   g8 c, r4
}

\relative c'' {
  \clef bass
  \key d\minor
   \tempo "3. Adagio"
  \time 3/4
    r8 d, a g f e
    d4 bes'4. g8
    a cis d4. d,8
    g4 f4. e8 f4 r
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 3/4
     r8 c d c d e
     f e f e f g
     a g a2~ a4 bes2~ bes4
     c2~ c4
}
