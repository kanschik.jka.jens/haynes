 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4  
    c4 r8 c,16 d  e8 c f d
    g e r g a f b g
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4  
    c2 d16 c b c  d c b c
    a8 a' a2.	
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio poco andante"
  \time 4/4  
     d8 c bes c  d d d 
     es2 d8. fis fis8. e32 fis	
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Allegretto"
  \time 2/4  
      g'4 f16 e f g
      a8 g f e f4
}
