\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
      \time 2/4	
    \partial 8
    f8 bes4 bes
    bes bes8. bes,16 bes'4 bes8. bes,16
    bes'4 bes bes4. bes,8
    bes8. c16 d8. es16
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
      \time 3/4	
      es8 es, es'4 es
      es8 es, es'4 es
      es2 e4 f es16 d8. c16 bes8.
      f'4
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
      \time 4/4	
      bes2 f'8 es d c
      bes4 bes, c2 d f'8 es d c
      bes4 bes, c2 d2
}
