\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Ein Fürst ist seines Landes Pan [BWV 208/7 - Originalversion von BWV 68/4]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key c\major
                      \tempo "o. Bez."
                      \time 4/4
                      \partial 16
                      % Voice 1
                    g16 c4. r16 c16 d4. r16 d16 
                    e8. d16 e8. f16
                    d8. g32 f
                    g8. 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key c\major
                     \set Score.skipBars = ##t
                        % Voice 2
                    e,16 g4. r16 g16 b4. r16 b16
                    c8. d16 c8. d16 b4. r16
                       
                        
                
                  }
                  \new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Taille"
                     \set Staff. shortInstrumentName = #"Ob 3"
                     \key c\major
                     \set Score.skipBars = ##t
                        % Voice 3
                c,16 e4. r16 e16 g4. r16 g16 g8. g16 g8. a16 d,4. r16
                       
                        
                
                  }
>>
}


\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key c\major
   \tempo "o. Bez."
  \time 4/4
  \partial 16
     \set Score.skipBars = ##t
    r16 R1*9 r2 r4 r8 g8 c4 b c g
    a g8 a16 f e4 r4
   
    
    
    }