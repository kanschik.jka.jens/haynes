\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro Maestoso [Tutti]"
  \time 2/4
  \partial 16
  c16
  f4 a8. f16
  c'8 c, r c
  c8.[ bes16 a8] c8
  c8.[ bes16 a8] c8
  d c \grace c16 bes8. a32 bes
  bes4 a8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro Maestro [Solo]"
  \time 2/4
  \partial 16
  r16
   \set Score.skipBars = ##t
   R2*52
   r4 r8 r16 f
   c'2 c4 c
   c8 a bes g
   e f b, c
   \grace e16 d4 c8 bes gis a r4
  
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adaio [Tutti]"
  \time 3/4
  \partial 16
  g,16
  c8. e16 g8. c16 e8. g16
  b,4 c r
  a'2. g 
  \grace {f16[ g] } a8 g f e d c
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Solo]"
  \time 3/4
    \partial 16
    r16
   \set Score.skipBars = ##t
   R2.*11
   r2 r8 r16 g
   c2. f
   e4 f8 fis g gis
   a2 \grace g16 f8 e16 d
   \grace c8 b4.
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegretto [Tutti]"
  \time 3/8
  \partial 16
  r16
   \set Score.skipBars = ##t
   R4.*34
   r4 r16 c
   f4 f8
   f32 g a8. r8
   a4 a8 a32 bes c8. r8
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegretto [Solo]"
  \time 3/8
  \partial 16
  c16
  f4. c
  a8 c a
  f r16 c d e f e f g a bes
  c d e f g a bes4 r8
}