\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Suitte de Trios " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Simphonie. Grave"
  \time 2/2
  d2~ d8. d16 e8. fis16
  \grace fis8 g4. b,8
  a4. g8
  e'4 r16 fis16 e fis fis4. e16 fis
  g8.

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "2. Sarabande en Rondeau"
  \time 3/4
  g4 bes4. a8
  g4 d \grace d8 es4
  d g a
  \grace a8 bes2 a4

}



\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Dance de Village. Rondeau"
  \time 2/2
  \partial 2
  d4 e
  d g fis g
  a d, b' a
  c b a g
  a8 g fis e d4 e

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Premier Menuet"
  \time 3/4
  bes2 c4 \grace {bes16[ c]}
  d2 g4 fis g a
  \grace a8 bes2. 
  a4 g f
  e \grace d8 cis4 d


}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "5. Fugue"
  \time 2/2
  g4 d g a8 g
  fis e d e fis g fis g
  a4 a a a 
  a c8 b a g fis e
  d2
  
}






\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIe. Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Air en Fugue"
  \time 3/8
  \partial 8
  e8 b'4 b16 a
  g8 a b
  e, fis g
  fis16 g fis e dis cis 
  b c b a g fis
  g8[ e]

}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "2. Air en Suitte"
  \time 2/2
  r4 g8 a b4 e,
  fis g a g8 fis
  g4 \grace fis8 e4 b'2
  a4 g fis e
  dis \grace c8 b4

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Rigodon"
  \time 2/4
  \partial 8
  b8 b e16 fis g8 fis16 g
  e8[ b' b a] 
  g fis16 g a8 g
  fis16 e fis g fis8 b,

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Passacaille"
  \time 3/4
  \partial 2
  e4 b'
  \grace a8 g4 fis4. g8
  \grace g8 a4 e8.[ dis16 e8. fis16]
  \grace fis8 g4 fis4. b8 g4 \grace fis8 e4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IIIe. Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Ouverture"
  \time 2/2
  \partial 8
  c8 c8. d16 e8. f16 g4. g8
  a4. a8 g4. f8
  e2 e~
  ~e4 d8. c16 b4. c8 c4 e

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Suitte. Legerement"
  \time 2/4
  r8 g8[ g g] 
  c4 c~
  c8 d16 c b8 c16 d
  e8 e e f16 e
  d8. c16 d e f d
  e f e f
  

}
  
  \relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Symphonie tendre"
  \time 3/2
  r2 c2 es
  g g1~
  ~g2 f2. es8 d
  \grace d8 es2 es2. d8 es
  d2

}
  
  \relative c''' {
  \clef treble
  \key c\major
   \tempo "4. Allemande "
  \time 4/4
  \partial 8
  r8 r2 r4 r8 r16 g16
  g4~ g16 a g f e8 g c,8. d16
  b8. b16 c8. d16 e fis e fis g8. a16
  e8

}

  
  \relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Air "
  \time 3/4
  r4 g4 c
  b b e c4. c8 d e
  f g f e f d
  e4 e f g2.~g4

}

  
  \relative c'' {
  \clef treble
  \key c\major
   \tempo "6. Sicilienne "
  \time 6/4
  \partial 4
  c4 c b c d2 e4
  \grace e8 f2. e2 f4
  g a g g f e
  d2.
}

  
  \relative c'' {
  \clef treble
  \key c\minor
   \tempo "7. Sarabande "
  \time 3/2
  es2 f2. g4
  \grace g8 as2 as2. bes4
  \grace as8 g2 f2. g4
  es1 d2

}

  
  \relative c''' {
  \clef treble
  \key c\major
   \tempo "8. Ouverture "
  \time 2/2
  g2 c,4. c8
  f2 f~
  f4 e8 d e4. f8
  d4. c8 d e f d
  e4 c
 

}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IVe. Suitte " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key a\minor
   \tempo "1. Simphonie. tres Proprement"
  \time 3/2
  a2 e2. fis4
  \grace f8 g2. f4 f g
  e2 c'2. b4
  \grace b8 a2

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Bourée. Pas trop vîte"
  \time 2/2
  \partial 4
  cis8 d
  e4 e e fis8 gis
  a4 e a gis
  fis e d cis8 d
  cis4

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Muzette. tres Lentement"
  \time 6/4
  \partial 2.
  a2 cis8 d
  e2 a,4 fis'2 a,4
  e'2 a,4 a' gis fis
  e d cis b4. cis8 \grace b8 a4
  b2 e,4

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Fugue. Dacapella, un peu piquez lourez"
  \time 2/4
  \partial 4
  a4 c d8. c16
  b4 e4~
  e f8. e16
  d8.[ c16 b8. a16]
  gis4 r4

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ve. Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Ouverture"
  \time 2/2
  d4. d8 bes'4. c8
  a4. a8 g4. a8
  \grace g8 f4 a2 g4~
  g f8 e f4 e8 d
  cis4. a8

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Air"
  \time 3/4
  r4 a4 g
  fis g a
  cis,2 d4
  \grace d8 e2 fis4
  g fis e
  fis \grace e8 d4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Gigue"
  \time 6/8
  \partial 4.
  a8. b16 a8
  d8. e16 d8
  a4 a8
  fis'4. fis8. g16 fis8
  d4 cis8 d4 d8
  g4.

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Les Echos"
  \time 3/4
  r4 fis8 g a4
  a g fis
  e4. e8 fis4
  g fis e
  fis d8 e fis4
  g fis e
  fis \grace e8 d4

}

\relative c''' {
  \clef treble
  \key d\minor
   \tempo "5. P.er Bourée"
  \time 2/4
  a4 \grace a8 bes4
  g4. a16 g
  f8[ e f g] \grace {f16[ g]}
  a16 g a bes a8 d,
  a'4 \grace a8 bes4
  g4.
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "6. Chaconne. Rondeau"
  \time 2/2
  \partial 2.
  d8. e16 fis4 b,~
  b e8. fis16 g4 cis,~
  cis fis8. g16 a4 d,~
  d

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VIe. Suitte " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Rondeau. Marche"
  \time 2/2
  \partial 2
  g4 d
  \grace d8 e4. e8 d e d c
  b4 g8 a b c b c
  \grace c8 d4. d8 e4 fis8 g
  fis2

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Gigue"
  \time 6/8
  \partial 8
  d8 bes'4 a8 g a fis
  g g, a bes c a
  bes4. \grace c8 c4. \grace {bes16[ c]}
  d4 b8 b a b  
  c4 bes8 a g a bes4.~ bes4
  

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Air"
  \time 4/4
  g4 fis \grace {e16[ fis]} g4 fis8 e
  d2 d4 e 
  d c b a
  b g8 a b c d e
  d4 e8 fis \grace fis8 g4. a8
  fis4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. P.er Passepied"
  \time 3/8
  \partial 8
  d8
  b16 a g a b c
  d8 d d 
  e16 d e fis e fis
  g8 d d 
  g16 fis g a g a
  b8 a16 b c8

}

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "5. Fugue"
  \time 6/8
  g8 d e c a d
  b b' g e a16 g fis e
  d8 d b c16 d c b a c
  b a b c d8~ d c c

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Marche du Regiment de la Calotte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Rondeau. P.er Partie"
  \time 2/4
  g16 a b c d8 e16 fis
  g4 d g8 b a g16 fis
  g fis g a g4

}
