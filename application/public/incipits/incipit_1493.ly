\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Largo cantabile"
  \time 3/4
  fis4 d b
  b ais r
  g' fis e
  d b
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Allegretto"
  \time 4/4
  \partial 4
  b4
  fis' fis2 g4
  \grace fis8 e1
  e4. fis16 g fis4  e
  d8 cis d2
  }

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Vivace assai"
  \time 3/4
  fis8 d cis b b b
  b4 g'2
  g8 fis fis4. g16 e
  \times 2/3 {  d8 cis b } b4 
}
