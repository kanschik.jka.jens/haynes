\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}


\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key es\major
                      \tempo "1. Allegro"
                      \time 4/4
                      \partial 4
                      r4
                      R1 g'2 f
                      es8 bes es g \grace g8 f4 es8 d
                      es4 r r2
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key es\major
                        bes,8. bes16
                        es4 bes8. bes16 f'4 bes,8. bes16
                        g'2 as
                        g8 bes es bes \grace bes8 as4 g8 f
                        es4
                  }
>>
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro Moderato"
  \time 2/4
  g'16 f g a g8 g
  f es es es 
  bes16 a bes c bes8 bes
  as g g g
}


\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4	
  f4
  \grace d'8 c4 c2 bes8 a
  g f f2 es4
  cis8 d d d  d'4. e,8
  f c c2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Tempo di Minuetto"
  \time 3/4	
    f4 g a
    c,2 bes4
    e f g
    bes,2 a4
}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 4/4
  e2 fis
  e4 r r2
  e2 fis8 gis a fis e4 r4 r2
  e2 e8 fis gis a \grace b8 a gis ges2.
  gis8 a b a gis fis e d
  d cis fis e e2
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Tempo di Minuetto"
  \time 3/4
  e4 cis8. a16 a'8. fis16
  e8 r r4 r
  b8 r cis r d8 r
  cis4 r r 
}
