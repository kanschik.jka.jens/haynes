\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}



\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tutti"
  \key bes\major
   \tempo "Allegro"
   \time 4/4
   bes4. d16 bes f8 f f r
   bes bes d d f4 r
   f2 es8 d \grace d8 c8 bes
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key bes\major
   \tempo ""
   \time 4/4
   \set Score.skipBars = ##t
   R1 * 40
   f2 es8 d c bes
   bes4~ bes16 f g a bes f g a bes d c es
}

