 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "Cantabile"
  \time 4/4
  \partial 8
  c,8
  f e g f bes a r f
  g f bes a d c r f
  f4 e16 d c b
}
