\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro "
  \time 4/4
    c,8 r e r g r e r
    f r d r b r r g'
    c c c c c16 d c b c8 d e4. f8 d4 r
}

\relative c'' {
  \clef bass
  \key c\major
   \tempo "2. Menuetto. Allegretto"
  \time 3/4 
  \partial 4
  g,,4
  c c c
  c c c
  c c c c r f8 e
  d c b a g fis g4 r r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Andante"
  \time 6/8
   \partial 8
  c8 f4 f8 \grace a8 g8. f16 g a
  bes4 a16 g g8 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro con Variazioni"
  \time 2/4
  g'2 a fis8 fis fis fis g2 e8 e e e f4. e8 d d \grace e8 d8 c16 d c4 r
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Moderato"
  \time 2/2
   \partial 4
    f4 f f f f
    f4. g8 f4 f
    bes es, es es
    d4. es8 d4 d g c, r c f bes, r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Menuetto"
  \time 3/4
  \partial 4
     \override TupletBracket #'stencil = ##f
 \override TupletNumber #'stencil = ##f

  f,4
  bes2 c16 bes a bes
  b8 c c4. cis8
  d4~ \times 2/3 {d8 es f}  \times 2/3 {fis g es}
  \grace d8 c4 bes8 a g f
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Adagio"
  \time 3/4
  es8 r es 8 r es r
  d8. f16 as4 r
  g as e
  f2 es4 d es a, bes r r
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro Canone"
  \time 2/4
    bes'2
    bes,8 c d es f4. es8
    d f a, c bes4. f8 bes c d es f4. es8 d f a, c bes4
}

\pageBreak

\markuplist {
    \fontsize #3
    "Quartetto III " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 3/4
    bes2 c4~ c bes2 a4 c f
    d es e
    f2. es8 r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 3/4
    g'2 f4
    \grace f8 es4 es8 d es4
    as2 g4 f2.
}

\relative c'' {
  \clef bass
  \key es\major
   \tempo "2. Andante poco Adagio"
  \time 2/2
    c,,4 d es e f fis g g, c bes a2 g r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Rondo"
  \time 2/4	
    es4. g8 bes a f d
    es4. g8 bes as f d
}

\pageBreak

\markuplist {
    \fontsize #3
    "Quartetto IV " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 3/4
    bes2 a4 a8 g g4 r
    as2 fis4 g8 d d4 r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 4/4
    g8 bes g bes g bes g bes
    d4. es8 d4 d
    es1 d2 r4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Andante"
  \time 2/2
     bes2 a c b
     c4 c8. c16 c4 bes
     bes8 a a4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 2/4
    \partial 8
    d8
    g g g g
    \grace g8 fis8 e16 d e8 fis
    g a bes c d4. c8 bes bes r g a a r fis g4 r
     
}

\pageBreak

\markuplist {
    \fontsize #3
    "Quartetto V " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
    a4 d2 fis8 d fis d
    cis4. e8 d4 r
    fis8 a fis a d4 r
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Menuetto"
  \time 3/4
    r4 r a'
    g fis b
    a g fis8 e16 fis
    g8 g e d16 e a8 a
    fis e16 fis d'2
}
 

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Adagio"
  \time 3/4
         d4 e fis g8. e16 cis4 r
         e fis g a8. fis16 d4 r
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 2/4
       d2 cis b a g~ g fis4 r
}
\pageBreak

\markuplist {
    \fontsize #3
    "Quartetto VI " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 2/4
    f8. f16 f8. e16 e4.
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro assai"
  \time 2/4
  \partial 16
  a'16
  a8. a16 a8. g16
  g8. fis16 fis8. e16 e8. d16 d8. d16
  d4

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Menuetto. Allegretto"
  \time 3/4
    \partial 4
  f8 g
  a4 c8 a bes g
  f4 f g8 a
  bes4 bes8 a bes d
  bes2 a4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Andante"
  \time 3/8
    f8 d es16 f32 g
    g16 f f8 r32 bes64 a g f es d
    d32 c r c d c r c fis g es c
    bes8 a32 f g a bes c d es
    d8 r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Rondo. Allegro"
  \time 4/4
    f4 g bes a8 f
    d4 e8. d32 e
    f4 r8
}

