\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro Comodo [Tutti]"
  \time 4/4	
    c4 g8. e'16 e4 c8. g'16
    a8 f d e  f c b d
    \grace f8 e d16 c  \grace e8 d8 c16 b c4 r8

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro Comodo [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*17
   c4 g8. e'16 e4 c8. g'16
   a8. b32 c  d,8 e f8. g32 a \grace a8 g8 f
   \grace f8 e8 d16 c \grace e8 d8 c16 b
   \grace b8 c4 r
  
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Largo cantabile"
  \time 2/2
      e4. a8 \grace a4  gis2
      
  \override TupletBracket #'stencil = ##f
  \times 2/3 { a8 gis a } a,4 r e
  e2~ e8 gis b d
  d4 c r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro assai"
  \time 3/8
    \partial 8
    g8
    c4 g'16 f
    \grace f8 e4 c8
    \grace e8 d8 c b
    \grace b8 c4

}