\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Suitte " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Gracieusement"
  \time 2/2
  \partial 2
  d4 fis
  e d e cis
  d4. cis16 b
  a4 g 
  fis b b a 
  d4. e16 fis g4 fis
  \grace fis8 e2 

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Gaiement"
  \time 2/2
 a2 g
 fis4 fis8 fis fis4 fis 
 a2 g
 fis4 fis8 fis fis4 fis
 e8 cis e g fis a fis a
 e cis e g fis a fis a
 e4
 
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "3. Menuet"
  \time 3/4
 a8 g fis4 e
 d e8 fis g4
 fis8 a g fis e d
 cis4 b8 cis a4
 
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "4. Legerement"
  \time 2/2
 a8 g fis4 fis e
 d a'2 b4
 a8 g fis4 fis e
 d b2 a8 g
 fis4 b2 a8 g fis4

}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "5. Lentement"
  \time 3/4
f4 e d
\grace d8 cis2 d4
e f g 
\grace g8 f4. e8 d4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "6. Gaiement"
  \time 2/2
  a2 b
  a4 d cis b
  a2 b
  a4 d cis b
  a d cis e
  d fis e g

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "7. Vivement"
  \time 3/8
  r8 fis8 g
  a a g fis fis g
  a a g
  fis b a 
  g g fis
  e a g 
  fis cis16 d g8 
  fis a g


}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "8. Premiere Gavotte"
  \time 2/2
 \partial 2
 fis4 a
 a g8 fis
 e4 d8 e
 \grace e8 fis4 \grace e8 d4 g fis
 b a \grace a8 g4. fis8
 \grace fis8 e2
 
 }
 
 \relative c'' {
  \clef treble
  \key d\major
   \tempo "9. Villageoise"
  \time 2/2
  \partial 2
  d8 e fis g
  fis g a b a g fis e 
  fis e d4 g b
  a b8 a g fis e d 
  cis b a4
  
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "10. Premier Menuet"
  \time 3/4
  d4 a g
  fis8 g a4 b
  a b8 a g fis
  \grace fis8 e2.
  d8 d' e, d' fis, d'
  g,4 fis e

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "11. Gaiement"
  \time 2/2
  fis2 e4 d
  d2 r2
  a'2 g4 fis
  fis2 r2
  b8 a g4 g e
  a8 g fis4 fis d


}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "12. Tres vitte"
  \time 2/4
  \partial 2
  d8.[ fis16 e8. d16]
  cis8.[ b16 a8. g16]
  fis8.[ g16 a8. b16]
  a8.[ d16 e8. d16] g4 fis
  \grace fis8 e2
 
  


}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxieme Suitte" 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Gracieusement"
  \time 2/2
  \partial 2
  b4 g
  g fis8 e d4 c
  b8 g b d  g4 d
  e d e d
  e g d c8 b
  \grace b8 a2
  

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Legerement"
  \time 2/2
  b8 c d4 d e
  d g fis g
  b,8 c d4 d e d g fis g
  c,2 b
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Gracieusement"
  \time 3/8
  d16. bes32 g8 g
  es' d g
  c,16 d32 es
  d8 g
  bes,16 c32 d
  c8 es
  es16 d c bes a g
  fis16. e32 d8 r8
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "4. Gaiement"
  \time 2/2
  g4 d b g
  g' d b g 
  d' a fis d
  d' a fis d
  g' d b g
  g' d b g 
  b b8 b d4 c 

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. Menuet"
  \time 3/4
  b4 b a
  g8 a b c d e
  d4 c b
  \grace b8 a2 g4
 

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "6. Contre danse"
  \time 6/8
  \partial 8*3
  g8 a b
  d,4 g8 d4 c8
  b4 g8 b4 c8
  d e d c4 b8 a4.
 

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "7. Legerement"
  \time 4/4
  d8 e fis g e4 e
  d g d8 c b a 
  g1


}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "8. Badinage"
  \time 2/4
  \partial 8
  b8 
  b g c a
  b g a fis
  g b16 d g8 b,
  b g c a
  b g a fis
  g b e d
  cis d fis, e d4. r8
 

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "9. Gaiement"
  \time 2/2
  g4 fis8 e d4 c
  b e e d 
  g fis8 e d4 c
  b e e d
  g2 fis
  e4 d8 cis d4 g
  fis2 e d r2
 

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "10. Legerement"
  \time 2/4
  g8. b16 a16. g32 fis16. e32
  d8. g16 fis16. e32 d16. c32
  b8. e16 d16. c32 b16. a32 
  g8 c b a
  g a b g


}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "11. Fanfar"
  \time 6/8
  \partial 8
  d8 g g g g4 b8
  g g g g4 d8
  e4 d8 c4 b8
  a d c b g d'
  g g g g4 b8
  

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "12. Legerement"
  \time 2/2
  \partial 4
  b8 g
  \grace fis8 e2 d8 fis g d
  \grace d8 c2 b4 b'8 g
  \grace fis8 e2 d8 fis g d
  \grace d8 c2
  

}


