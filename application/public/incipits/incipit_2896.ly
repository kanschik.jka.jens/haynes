\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Vivace [voce]"
  \time 4/4
  r4 e,8 e16 e  a b cis d cis d e fis
  e4 cis8 b16 a e'8 cis b a b4 r r2
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Vivace [Oboe]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*10 
    r8 e16 f g8 b, c d16 e g, c b c
    a8
}
