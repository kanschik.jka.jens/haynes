\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro molto [Tutti]"
  \time 4/4
  c16 d e8 g, g a16 b c8 e, e
  f16 g a8 d, d e16 g c b c g c b
  c g c b  c g c b c b c b  c b c b
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro molto [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*13 
    c16 d e8 g, g a f' d c
    b a' g f e c c4~
    c8 d16 e f8 e16 d e8 c c4~
    c8 f16 g a8 g16 f e d c8 r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Largo [Tutti]"
  \time 4/4
    f8 r e r r d c b
    c r b r r f' e d
    c a a' r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Largo [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*6
   r2 r8 a e'4~
   e8 f e4~ e8 f e4~
   e8 f e16 d c b c8 a
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 4/4
  \partial 8
  g8
  a f'16. a,32 b8 g'16. 
  b,32 c8 c16 d e f g8
  a, f'16. a,32 b8 g'16. b,32 c8 c16 d e f g8
}