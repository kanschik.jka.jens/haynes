 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Tutti]"
  \time 3/4
  bes16 bes bes bes  bes c d es d es f g
  f es d c bes bes' a bes bes, bes' a bes
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Solo]"
  \time 3/4
     \set Score.skipBars = ##t
   R2.*27 r4 f bes,
   es8 g f4. es8
   d4. c8 bes4
   r g' bes, a f' d
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Largo [Tutti]"
  \time 4/4
  g,16. g'32 fis16. g32 bes,16. d'32 c16. d32  c,16. g'32 fis16. g32  c,16. es'32 d16. es32
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Largo [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*5 
   g'1 a
   d,8 r g r c, r fis r
   g d g2 fis4
  
}
\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Presto"
  \time 4/4
  \partial 4
  f,4
  bes c  d es
  f8 es d c bes4 g'
  g f r bes
  es, es
}
