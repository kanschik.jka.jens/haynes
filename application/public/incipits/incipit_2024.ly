
\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
  \tempo "1. Gravement"
  \time 3/4
  c4 g4. es'8
  d2 as'4
  g f8 es d c
  b2 c4
}


\relative c'' {
  \clef treble
  \key c\minor
  \tempo "2. Allegro"
  \time 2/4
  \partial 8
  d32 es f16
  es8[ es es d]
  c g'4 d32 es f16
  es8 es es d
  c g'4 f16 es
  d8 c4
}

\relative c'' {
  \clef treble
  \key es\major
  \tempo "3. Adagio"
  \time 3/4
  es8 bes4 f'8 bes,4
  g'8 es f g es bes'
  bes as g g f4
}

\relative c'' {
  \clef treble
  \key c\minor
  \tempo "4. Vivace"
  \time 3/8
  es8 d8. es32 f
  es16 d c b c8
}
