\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 3/8
    g8 g8. a32 b
    a8 c r
    a a8. b32 c
}
