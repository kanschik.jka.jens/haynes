\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Suite" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gracieusement"
  \time 2/2
  \partial 2
  e4 g
  c, g f' e8 d
  e4 \grace d8 c4 d e8 c
  d4 e 8 f e d c
  b4 \grace a8 g4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxieme Suite" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Gracieusement"
  \time 3/4
  c8 d es f g f
  es4 \grace d8 c4 d
  es f8 es d c b2
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisieme Suite " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gracieusement sans lanteur"
  \time 2/2
  \partial 2
  g'4 c
  b \grace a8 g4 a g8 f
  e4. f8 g4 c,
  d c f e d2
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatrieme Suite " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gracieusement"
  \time 2/2
  \partial 2
  c4 g'
  f4. e8 d e f d
  e4 \grace d8 c4 e f8 e d4 g c,4. d8
  b4 \grace a8 g4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquieme Suite " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allemande. Gayement"
  \time 4/4
  \partial 8
  r16 c'
  c4 r16 c, e c g' f e c a' g f a 
  g8 a16 b c b a g f e d c b8 c
  d
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixieme Suite " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Gracieusement "
  \time 3/4
  c4 es8 f g g,
  c4 as'8 g f es
  d4 es f
  es2 \grace d8 c4
}
