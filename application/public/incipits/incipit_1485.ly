 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  g'8 b,16. c32 d8 e \grace d8 c b r16 c a g
  fis8. a16 a g b d b8[ a]
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro moderato"
  \time 4/4
  \partial 16
  g'16
  b, b8 c16 d8 e16. fis64 g  g16 b,8 c16 d8 e16. fis64 g
  g8 fis16. a,32 a8 b16. d32  \grace d16 c8 b r 
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Vivace"
  \time 3/4
  d4 d2
  e4 d r
  g8 fis fis g g a
  \grace b16 a8 g16 fis g8 b, a b
}

