 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

 

 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Largo"
  \time 2/2
  r8 bes16[ d] f8 f f[ c] es8. f16
  d8 r8 r4 r2
  r8 f d[ c16 bes] c8
}



\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/4
  bes8 d f c
  d bes r g'
  f d es c
  d bes r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Largo"
  \time 3/4
  r4 d4. d8
  fis,4 es'8 d es c
  d4 f,4. bes8
  bes4 a8 bes c a
  fis4 r r
   }

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 3/4
  f,8 g a bes c g
  a4 r r
  bes8 c d f e d
  c4 d2~
  d4 c2~ c4 bes2~
  bes4
  }
