\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  d4 r8 d16 d d8 a r fis'16 fis
  fis8 d r a'16 a a8 fis r fis16 g
  a8 fis b g a fis b g
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*9 
   fis4 r8 fis16 g a8 fis b g
   a fis b g a fis16 g a8 fis16 g
   a8 fis16 g a8 b16 a g8 e16 fis g8 e16 fis
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Grave"
  \time 3/2
  f2 f4. e8 f4. g8
  e1.
  g2 g4. f8 g4. a8
  f1.
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 12/8
  fis4. e fis r8 fis g
  a fis g  a fis g  a fis g a b a
  g e fis  g e fis g e fis g a g
  fis4. a a r4 r8
}
