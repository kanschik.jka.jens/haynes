 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key g\major
                      \tempo "1. Andante"
                      \time 4/4
                      % Voice 1
                      R1 r2 g'4~ g16 b a g
                      fis8 d g b cis,16[ e32 a8]~ a16[ g fis e]
                  }
\new Staff { \clef "treble" 
                     \key g\major
                     % Voice 2
                       d4~ d16 e d c b8 g c e 
                       fis,16. a32 d8~ d16 c b a b b c d g, d' c b
                       a g fis e d16.[ d'32 g8]~ g fis16 d e c d e
                  }
>>
}




\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 4/4
  g4 a b r8 d16 c
  b8 g g a b4
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Largo"
  \time 3/2
  r2 b1~ b2. a4 g fis
  g2 b1~ b2. a4 g fis
  g2
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Presto"
  \time 6/8
    d4. r8 b c
    d e d d g d
    e fis e e g e d
}