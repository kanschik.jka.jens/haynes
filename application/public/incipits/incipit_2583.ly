 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Largo"
  \time 2/2
  r8 b' e, fis g4~ g16 fis g a
  fis4~ fis16 e fis g
  e4~ e16 dis e fis
}


\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "2. Allegro"
  \time 2/2
    e8[ e16 dis] e8 fis g g g a
    fis[ fis16 e] fis8 g  a a a b
    g g g c  fis, fis fis b
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Affetuoso"
  \time 3/4
  g'2 d4 e d c
  b8. a16 g2
  g4 fis4. e16 fis g4 g'2 g4
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "4. Allegro"
  \time 3/8
    b16 e, e b' c[ b32 a]
    b16 e, e b' e[ d32 c]
    d16 c32 b  d16 c32 b32 c16 b32 a
    b16 a32 g  b16 a32 g a16 g32 fis
    g16 e e fis fis8
}