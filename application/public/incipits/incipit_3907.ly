\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto [IV] " 
    }
    \vspace #1.5
}


\relative c' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro Moderato  B-Dur  3/4"
  \time 3/4
   \partial 16*5
    f16 g8. a16
    bes4 r8. bes16 c8. d16
    es4 r8. es16 f8. g16
    bes,2 a4 bes4
}

\relative c' {
  \clef treble
  \key bes\major
   \tempo "2. Menuetto  C-Dur  3/4"
  \time 3/4
  \partial 4
    f4 \grace a8 bes4 \grace c8 d4 \grace a8 bes4
    g'4 r8 fis g es 
    c4 c8 d c bes
    \grace bes8 a2
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Rondo poco Andante  B-Dur  2/2"
  \time 2/2
  \partial 4
  d8 es
  f4 f g g f2 bes8 a g f
  es4 es d d c8 f,8 g a bes c d es
  f4 f g g f2 bes8 a g f 
  g4. c8 bes4 a bes
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "Allegro  6/8"
  \time 6/8
  bes4 d8 bes4 r8
  d4 f8 d4 r8
  bes'8 bes bes bes4 a16 g
  f4 es8 d8
}

