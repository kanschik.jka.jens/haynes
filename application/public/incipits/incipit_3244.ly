\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Zerfließe, mein Herze [BWV 245/63]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Traverso"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key f\minor
                      \tempo "o. Bez."
                      \time 3/8
                      \partial 8
                      % Voice 1
                   c8 f32 g as16~ as16[ g32 f e16 f]
                   f8~ f32 e f as f[ e f as]
                   f g as16~ as[ g32 f e16 f] 
                   es4.~ es8 
                    
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe da Caccia "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key f\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                  f,32 g as16 as32 bes c16~ c[ bes32 as g16 as]
                  as4.~ 
                  as32 bes c16~ c[ bes32 as g16 as]
                  a8~ a32 g a c a[ g a c]
                  f,8~ f32
                      
                  }
                  
            
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key f\minor
   \tempo "o. Bez."
  \time 3/8
  \partial 8
     \set Score.skipBars = ##t
     r8 R4.*15 r8 r8 c8
     f32 g as8 g32[ f e16 f]
     f8 c as 
     f16 es' des c bes des
     ges es a,8 r8
   
   

}