 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Ouverture"
  \time 2/2
    a'4. gis16 fis e8 d cis b
    a4. gis16 fis e8 d cis b
    a8 a' cis a e a cis a
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo ""
  \time 3/8
  r8 e e
  e a, d~
  d cis16 b cis8
  fis16 e d cis b a
}


\relative c'' {
  \clef treble
  \key a\major	
   \tempo "2. Rondeau. gracieusement"
  \time 3/8
  e8 d4
  cis16 b a gis a8
  fis'16 gis a8 e
  d4 cis8
  e16 gis gis fis32 gis a16 r
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "3. Air"
  \time 2/4
  \partial 4
  a8 cis
  e4 fis8 e d cis b a
  gis a b cis
  b e, a cis
  e4
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "4. Forlane"
  \time 6/8
  \partial 2
    cis8 cis4 b8
    a8. cis16 d8 e4 fis8
    e8. fis16 gis8
    a4 a,8
    d4 e8
    cis4.
}


\relative c'' {
  \clef treble
  \key a\major	
   \tempo "5. Rigaudon"
  \time 2/2
  \partial 4
  e4 a, cis e a
  gis2 a
  e8 d e fis e4 d
  cis b8 cis a4
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "6. Gavotte"
  \time 2/2
  \partial 2
  a'4 e
  \grace d8 cis4 fis8 e d4. cis8
  b2
}
\relative c'' {
  \clef treble
  \key a\major	
   \tempo "7. Menuet"
  \time 3/4
  a4 cis e
  gis,2 a4
  d b2
  a8 b cis d e4
}
\relative c'' {
  \clef treble
  \key a\major	
   \tempo "8. Badine"
  \time 6/8
  \partial 8
  a8 cis b cis  a cis a
  b4 e,8 e4 b'8
  d cis d b e d
  cis4 a8 a
}
\relative c'' {
  \clef treble
  \key a\major	
   \tempo "9. Tembourin"
  \time 2/4
  a8 cis cis e
  e4 a
  a,8 cis cis b
  a16 b cis d e8
}
\relative c'' {
  \clef treble
  \key a\major	
   \tempo "10. Chaconne"
  \time 3/4
  r4 cis8 d e fis
  e4 a e
  cis b d
  cis fis8 e d cis
  b4
}