\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 3/4
  es4 es2 es4 es2
  es4 es2 \grace d8 c4 bes r
  r2 bes16 a g f bes4 d, es
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio ma non troppo"
  \time 2/4
  g8 es'4 d8~ d b4 c8
  as' g4 f8 \grace f8 es4 d
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro ma non troppo"
  \time 3/8
  g8 as bes
  bes4.  bes8 es bes bes4.
  bes8 es d d f16 es d c bes8 bes as
  \grace { g32[ bes]} as8 g r
}