 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Poco largo e cantabile"
  \time 3/4
    f4 bes4. f8
    \times 2/3 { f8[ as g] } g4. bes,8
    bes4 \times 2/3 { a8[ d c] } \times 2/3 { es8[ d g] }
    \grace f4 es2 d4
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allabreve"
  \time 2/2
  f1 bes4 bes,8 c d4 es
  f c f es
  d c8 bes g'2~ g
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Lento"
  \time 3/4
  d4 es r8. g16
  g4 cis, r
  fis8 g4 gis8 a c,
  bes4 a r
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Vivace"
  \time 2/2
  \partial 4
    bes4 f'2. \times 2/3 { bes8[ a g] }
    \grace f4 es2~ \times 2/3 { es8[ d c] } c8 d
}