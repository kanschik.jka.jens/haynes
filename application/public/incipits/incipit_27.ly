\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro [tutti]"
  \time 2/4
  \partial 8
  f16 g
  a8 d, a' cis,
  d d,32 e f g a8 d
  e a,32 b cis d e8 a
  f4 e8 f
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro [solo]"
  \time 2/4
  \partial 8
   r8	
   \set Score.skipBars = ##t
    R2*19 
  r4 r8 f16 g
  a8 d, a' cis,
  d a d8. e32 f
  e8 a, e'8. f32 g
  f4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio [tutti]"
  \time 3/4
  bes16 d f bes  bes, d f bes bes, d c bes
  f a c f f, a c f f, a g f
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio [solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*7 
   f2.~ f
   bes2 f4
   g8. bes16 es,2
   d r4
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro"
  \time 6/8
  d,8 f a d4 e8
  f16 g a bes a g f e d8 a'
  a d cis d cis16 bes a g
  
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  c8
  g' f16 e d8 c  g' f16 e d8 c
  c'8 b16 a g8 f e d c16 d e f
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio"
  \time 3/4
  a4 c8 a a'4~
  a8 gis b2
  e,4. e8 c a
  d4~ d8 f e d
  cis2 r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/8
  c8 e16 f g8 f d4
  e16 g c8 c,
  f d4
  e16 f g 8 c,
  b16 c d e f e
  
}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  d8
  g bes a fis g d c a
  bes16 d g bes a8 fis g d c a
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 3/4
  r8 f es d r g
  r es d c r f
  r d c bes r es
  r c bes a r d
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 2/4
  g'4 bes16 a g fis
  g8 d16 c bes8 a
  g g'16 a bes8 a
  g d16 c bes8 a
  g
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  f8
  bes, d16 f bes8 bes bes4. bes8
  c16 bes a g f8 es es d r bes'
  c16 bes a g f8 es es d r
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 4/4	
  g'16. fis32 g16. a32 g16. fis32 g16. a32 g4. g8
  a16. c32 bes16. c32  a16. c32 bes16. a32 bes16 a g8 r
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/8
  d16 es f8 f
  f bes g f bes es,
  d bes' g
  f bes es,
  d16 bes c bes f bes
  
}

