\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key g\major
                      \tempo "Andante Moderato"
                      \time 4/4
                      \partial 2
                      r2
                      R1
                      r2 c4 b8 a 
                      b8. c16 d4
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key g\major
                        % Voice 2
                       g,8 b a c
                       b c d4 \grace d8 c4 b8 a
                       b8. c16 d4 r2 r
                  }
>>
}
