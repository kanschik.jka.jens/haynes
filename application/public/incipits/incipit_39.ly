\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
  bes'4. f8 d4 r
  f4. d8 bes4 r
  bes'1~ bes~ bes~
  bes2~ bes8 c16 bes a bes c bes
  \grace bes8 a4 g8 f \grace f8 es4 d8 cis
  d4 r r2
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Larghetto"
  \time 3/4
  r2.
  bes'8. a16 g8 g fis g
  fis~ fis32 g a bes c4 r
  c c16 bes a g \grace g8 fis16 es d es
  d2.
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Andantino con brio"
  \time 2/4
  \partial 4
  f8. bes16
  d,8. bes16 \grace f'8 es16 d es f
  d8. bes16 \grace f'8 es16 d es f
  d8 r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro maestoso"
  \time 4/4
  c4. e8 g4. e8
  c4 r8 g c g c g c4 r r g'8. a16
  \grace g8 f4 e e8 r g8. a16
  \grace g8 f4 e e8 r
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Largo con espressione"
  \time 2/2
  c8. g16 c8. es16 g8. g16 es8. c16
  g'4 g8. g16 g4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Andantino con espressione"
  \time 2/4
  \partial 4
  c8. e16 d8 d \grace e8 d16 c d e
  c8 g'16 r e8 g16 r
  d8 d \grace e8 d16 c d e c8 r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro maestoso"
  \time 4/4
  f2. c8 a
  c'2. bes8 a
  g2. e8 c
  bes'2. a8 g
  f4 r
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "2. Larghetto"
  \time 3/4
  r2 r8 f32 g as bes
  c8 r16 c as8 r16 as f8 r16 f
  e8. g16 c,4 r
}
  
  \relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro ma non tanto"
  \time 3/4
  \partial 4.
  f4 g8
  e8[ c e] f4 g8
  e r8 a4 g8
  \grace g8 f8[ e f] g4 f8
  e4 r8
  
}
  