\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trietto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  d4. d8 g,4. g8
  e'4. e8 c a a c
  d, fis'16 e fis8 g r16 g fis e d8 c
  b16 a b c  b c b cis
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Grave"
  \time 3/2
  d2 e d
  d c r
  c d c c b r
  b'4. a8 g2 fis
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 4/4
  \partial 4
  d4
  g8 a g a g a g a
  g a g a  g4 d
  g d e b
  c8 d e4 b
  c8 d e2
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Scherzo I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 2/4
  cis8 cis16 d b8 b16 d
  cis8 cis16 d b8 b16 d
  cis8 e b e
  a,4 cis8 cis16 a
  e'4 r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Moderato"
  \time 3/2
  c4 b8 c a2 b
  c4 e8 dis dis2 e
  d4 cis8 d f4 d e8 d c b
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Allegro"
  \time 3/4
  cis16 b cis d  e8 e fis e
  a16 gis a8  e16 d e8 cis16 b cis8
  a16 gis a b  cis8 b a gis
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trietto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Vivace"
  \time 3/8
  r16 d fis a d,8
  r16 d g b d,8
  r16 d fis a d,8
  r16 fis a, d fis, a
  d,8 d' d
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Andante"
  \time 4/4
  g2 g'
  fis4. e8~ e d16 e cis8 fis16 e
  d8 cis16 d b8 e16 d cis8 b16 cis a8 d16 cis
  b8
}
  
  \relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Vivace"
  \time 6/8
  \partial 8
  a'8
  fis a e fis a cis,
  d4.~ d8 fis a,
  b d fis, g b e, fis a d, d'4
}
  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Scherzo II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Vivace"
  \time 4/4
  e4 e e   r8 gis16 a32 b
  e,8 gis16 a32 b  e,8 gis16 a32 b e,8 b gis e
  fis'4 fis fis r8
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Largo"
  \time 6/4
  e4. gis16 b,  e4 e8. dis32 cis b4 r
  a4. fis'16 dis a4 a8. gis32 fis gis4 r
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "3. Vivace"
  \time 6/8
  \partial 8
  b8
  e4 b8 gis'4 b,8
  fis'4 b,8 a'4 b,8
  gis' b16 a gis fis e8 gis16 fis e8 
  dis fis16 e dis cis b4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Trietto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro"
  \time 4/4
   \override TupletNumber #'stencil = ##f
   d4. \times 2/3 { d16[ e f]}
   e4. \times 2/3 { e16[ f g]}
   f8 d bes'8. bes16 bes8. cis,16 a'8. a16
   a8.
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Largo"
  \time 6/4
  \partial 4
  d4
  g4. a8 bes4 g4. fis8 g4
  d2.~ d2 g,4
  c4. d8 es4 d4. es8 c4
  bes a g r r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Allegro"
  \time 3/4
  d8 e16 f e8 d cis d
  a d f4 e
  f16 g a8 g f e f
  cis d a2
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Scherzo III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  fis4 g8 fis16 e a4 fis8 e16 d
  e4 cis8 b16 a d8 a fis d
  fis'4 g8 fis16 e a4 fis8 e16 d
  e4 cis8 b16 a d8 a  d,4
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Largo"
  \time 3/2
  r4 fis8 d b4 b b d
  r cis8 ais fis4 fis fis cis'
  r4 d8 b b fis fis' d d b d b
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 3/4
  d4 e8 d fis e
  g fis b a4 g8~
  g a16 fis g e fis d  e cis d b
  cis a d b  e cis fis d  e8 a,
}
