\version "2.16.1"
         #(set-global-staff-size 14)

         
         \relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/2
  bes'4. f8 g4. f16 es
  d4. d8 d4. es8
  f4. d8 bes4. bes8
  es2~ es8 es d c
  d4. f8 bes4.
}
         