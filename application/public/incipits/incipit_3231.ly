\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Auf, Gläubige, singet die lieblichen Lieder [BWV 134/2]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1 (+Vl.1)"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key bes\major
                      \tempo "o. Bez."
                      \time 3/8
                      \partial 8
                      % Voice 1
                     f8 bes d f
                     d16 c bes c d es
                     f es f g f g 
                     f8[ f,]
                    
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2 (+Vl. 2)"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key bes\major
                     \set Score.skipBars = ##t
                        % Voice 2
                      r8 r8 r8 f8
                      bes d f
                      d bes a
                      bes16 c d es f g
                      a,8[ c] 
                      
                  }
>>
}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key bes\major
   \tempo "o. Bez."
  \time 3/8
  \partial 8
     \set Score.skipBars = ##t
    r8 R4.*23 r8 r8 f8
    bes d f
    d8. c16 bes8 R4.
  
   

}