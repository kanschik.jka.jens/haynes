 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. All. mod."
  \time 2/2
  d4 d2 fis8 d
  \grace b'8 a4 a2 fis8 d
  cis b g'4 \grace a16 g fis g a b8 b,
  b4 a r2
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Tempo di Minuetto"
  \time 3/4
  a'2 fis4 d2 a'4
  b b d
  a2 r4
  
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "Minore [Trio]"
  \time 3/4
  a'4. bes16 a  g f e d
  d8 cis cis4 r
  g'4. a8 a16 g f e e8 d d4 r
}
