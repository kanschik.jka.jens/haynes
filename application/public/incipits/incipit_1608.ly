\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  f,8
  c'16. d32 c16. d32 c8. bes32 a bes16. c32 bes16. c32 bes8. a32 g
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro moderato"
  \time 2/2
  c8. d16  \grace c8 bes4   a8 a' g f
  bes,8. c16  \grace bes8 a4 g8 g' f e
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Vivace"
  \time 3/8
  c8 f8.[ g32 a]
  \times 2/3 { g16[ f e] } \times 2/3 { d16[ e f] } e8
  c8 g'8. a32 bes
}
