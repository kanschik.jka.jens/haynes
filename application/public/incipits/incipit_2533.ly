\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Prima " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\majpr
   \tempo "1. Siciliana"
  \time 12/8
  g8. a16 g8 g,8. a16 b8 c4.~ c4 g'8
  a16 b c8 a f4. e4.~ e4 g8 
  e8. c16 d8 f8. d16 e8
  a4.

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  g8 e[ c g' a~]
  ~a g16 f g8 a16 b
  c8 b16 a g8 f
  e16 d e f d8 g,
  c[ g e' g,] d'[ g, f' g,]

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Minuetto Allegro"
  \time 3/8
  g8 f16 e d c
  g'8 a16 b c8
  b c a g4.
  c8 e, f
  c' d, e
  d16 e f8 a
  b,4.

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "4. Corrente Allegro"
  \time 3/4
  \partial 8
  g8 g c, e g a f
  g e d c g' e
  f g, b g f' g,
  e' g, c g e' g,

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Seconda " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 4/4
  b4 e,8 fis dis4 r8 e8
  c b a b g8.[ fis16 e8 g']
  fis b, g' fis16 e d8 b e d16 cis
  d8 e fis g ais,4 r8

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allemanda Allegro"
  \time 4/4
  \partial 8
  b8 e g fis e16 dis e8 b4 c8
  a g fis b g fis16 g e8 b'
  c b e b c b16 c a b c a

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Sarabanda"
  \time 3/4
  d4 g4. a8
  fis4. e8 d4
  e a,4. d8
  b4. a8 g4
  a d8.[ c16 b8. a16]
  b4.


}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "4. Minuetto"
  \time 3/8
  g8 fis e
  b' b,16 cis dis b
  e8 fis dis
  e16[ dis e fis] fis16. e64 fis
  g8 fis e
  
  
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Terza " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 4/4
  d8 a fis g16 a d,4 r8 fis'8
  e d16 cis d8 e cis8.[ b16 a8 d]
  e d cis d g fis16 e fis8 a,

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  d8 a' a b d, a' a b
  cis,16 d e fis e fis g a fis e fis g e8 a,

}
  
  \relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Minuetto"
  \time 3/8
  d16 cis d8 a
  fis'16 e fis8 d
  a'16 g a8 b
  cis,4 d8

}
  
  \relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Presto "
  \time 2/4
  d4 a8 fis'
  fis e16 d e8 cis16 d
  e4 a,8 g'
  g fis16 e fis8 a 
  b g  g fis16 e 
  a8 fis

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Quarta " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  d4 g8 a fis4. g8
  e d c16 fis a c, b8.[ a16 g8 b]
  a d g,16 cis e g, fis8 e d a'
  b a c b e[ d]

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  d8 b8. c16 c8 b16 c
  d c b a g8 d'
  e8. fis16 fis8 e16 fis g4.

}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "3. Minuetto"
  \time 3/8
  g8 d e
  c4 b8
  a16 b c8 e
  fis,4 d8
  d g16 a b8
  a g16 fis e d
  g fis g a b c 
  b8 a r8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga Allegro"
  \time 6/8
  \partial 8
  d8 g fis g g fis g
  fis a16 g fis e d8 g b,
  c d e a, d c
  b d16 c b a g4 a8

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Quinta " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "1. Siciliana"
  \time 12/8
  g4 c,8 b8. c16 d8 c4. r8 r8 g'8
  f8. g16 es8 d8. es16 c8 b4. r8 

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allemanda Allegro"
  \time 4/4
  c4 g'8 d es d16 es c8 es
  d c16 b \grace b8 c8 e g,4 r4

}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "3. Sarabada"
  \time 3/4
  g8 f16 g d4. es8
  es4 d8. es16 bes4
  c8. as16 f4. bes8
  g4. f8 es4

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Corrente Allegro"
  \time 3/4
  c4 g' f
  es8 d es f d es
  c b c es d c
  b2.

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Sesta " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key es\major
   \tempo "1. Adagio"
  \time 4/4
  g4 d8 es \grace c8 bes4. c16 d
  es8 f16 f as8 g16 f g8 es16 d
  es4
  g8 es es d16 c f8 d d c16 bes es8  d16 c

}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 4/4
  es4 f8 es16 d es8 d16 c bes8 c16 d
  es4 f8 es16 d es8 d16 c bes8 c16 d
  es8[ es,] as g g f r4

}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Minuetto"
  \time 3/8
  es8 d16 es c d
  bes8 bes'16 g as f
  g8 f es \grace es8 d4 r8

}

\relative c' {
  \clef treble
  \key es\major
   \tempo "4. Allegro"
  \time 2/8
  es8[ g16 es]
  bes'8[ d16 bes]
  es8[ f]
  d[ c16 bes]
  es8[ bes16 g]
  c8[ as16 f]
  d8[ bes']

}

