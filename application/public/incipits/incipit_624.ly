
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Sonate"
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Allemande. Legerem.t"
  \time 4/4
\partial 16
g16 g4 r8 c16 b c8 g g c16 b
c b a g a g f e f8 d4 g16 f
e8 a16 g f e d c b8 g4

 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. La Brune. Gracieusem.t"
  \time 3/8
c8 c g'
e8. f16 g8
d8 f16 e d c
b8. a16 g8
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. La Blonde"
  \time 3/8
c8 g' f
e8. d16 c8
e16 d e f g as
g4.

 
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "4. Le Boiteux. Tres viste"
  \time 2/4
g4 c,8 a'
g4 c,8 a'
g[ f e d] 
e4 d
g g,8 f'
e4


}
 

 

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxieme Sonate "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Gracieusem.t"
  \time 4/4
\partial 2
c4 g
g'4. as8 g4 f8 g
es4 d f es8 d
b4. c8 d4 es d2

 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Vivement Et Marqué"
  \time 2/4
c8 b16 a g f e d 
c8 g g c16 d
e8[ d e f] 
g a16 g f e d c
b8[ g g' g] 
g[ f f f]  

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Les Delices "
  \time 3/8
e8. f16 e8
d c g'
a f8. e32 f
g4.
f16 g e f d e 
b8. a16 b g
d' e e8. f16 d4.
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Gavotte. Legerem.t"
  \time 2/4
\partial 4
e8 d16 c
g'8 g f16 e d c
b8 g g' g 
g f16 e f8 e
d4
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisieme Sonate"
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\minor
   \tempo "1. Prelude. Lentem.t"
  \time 4/4
\partial 16
g16 g8. c16 bes as g f es8. d16 es f g c,
b8[ \grace as8 g8] g'8. g16 c bes as g f es d c 
f8 


}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Le Caprice. Legerem.t"
  \time 2/4
\partial 8
c8 g'4. c,8
g'4. c,8
g' c16 bes as g f es
d8 g16 f es d c b
c8. d16 d8. c16


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Menuet"
  \time 3/4
e8 f g4 g
e2 d4
f e d c d b
c2 g4
e'8 f g a b c 
e, f g a b c 
f,4 e2 d2.

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Sarabande a deux Chalumeaux"
  \time 3/4
e4. f8 g4
b,4. c8 d4~ d8 d16[ e f]
e4. d8 
e4. d8 c4
e4.


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Gigue"
  \time 6/8
\partial 4.
c4 g'8
c,4 g'8 e4 d8 
e d c d e f 
e d c  b4 a8 g4.

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatrieme Sonate"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. La Thetis. Un peu legerem.t"
  \time 2/4
d4 d
g4. fis16 g
a8[ d, d c]
b4 a
b16 c b a g a b d 
d8 c16 b c8 c

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Muzette- Les Amours. Gracieusem.t"
  \time 4/4
\partial 2
d4 g 
fis \grace e8 d4 e8 fis g a
d,2 g8 fis g d 
e d c b c d c b 
a4 g

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Sarabande"
  \time 3/4
\partial 4
d4 g4. fis8 g a 
d,2 es4 
d c4. d8
bes4. a8 g4
bes'8 a g f es d 
cis4 a4


}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "4. Le Postillon"
  \time 6/8
\partial 8
g8 d4 g8 d4 c8
b4.~ b4 e8
d4 c8 b4 a8
b a b g4


}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquieme Sonate "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prelude"
  \time 3/4
r4 d4. c8
b4. a8 b c
a2 g4
d'4. b8 c d 
e4. fis8 g a 
b a g fis e d 
cis4. 

 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. La Justine. Gavotte"
  \time 2/4
\partial 4
g16 fis g a 
fis8 e16 d e8 e
d4 c16 b c d
b8 a16 b c b a g 
a8 g

 
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Gracieusem.t"
  \time 3/8
\partial 8
bes8 g4 es'8 
d g16 fis g8
d c bes 
a4 g8
bes'16 a g f e d 
e8 
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Le Bavarois. Leregem.t"
  \time 2/4
d4. e8
d4. e8
d[ e d c] 
b4 \grace a8 g4
g'4. b8
a4. b8

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixieme Sonate "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\minor
   \tempo "1. La Rivier d'hier. Prelude. Lentem.t"
  \time 4/4
\partial 16
g16 g8. bes16 a c bes a g8 d g16 f es d 
es8. d16 c8 d bes8. a16 bes  c d es
d8


 
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. La Dificile "
  \time 2/4
\partial 8
d8 g4. d8
g4. d8
c[ bes a d] 
bes g d'4~ d8 c16 bes c4~ c8 bes16 a bes8 d g 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Rondeau. Un peu lentem.t"
  \time 3/8
g8 g g 
d'4 g8
fis4 g8
d4.
es8 d c 
d4 es8
c4 d8 
bes8 a16 bes g8
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "4. Les Adieux de Brunoy"
  \time 2/4
g4 b
a4. c8
b[ a g fis]
g4 d
d8 e16 d c8 d16 c
b8 a16 g a8 b 
c4 b a2


}


