 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 3/4
    \override TupletBracket #'stencil = ##f
      f4 e f r8 c d f r d c f r
}
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 12/8
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
     
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 2/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
         
}
