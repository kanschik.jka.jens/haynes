\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro Spiritoso [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   R1*38 f4c8.a'16 f4 r4 g4 c,8. bes'16 g4 r4 r8 f16[ g] f8 f f e d c 
  
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio [Solo]"
  \time 2/2
   d2 \grace f8 es4 es c2 es4 d8 r8 f2 g8. a16 bes8 r16 d,16 f4. es16 d c4 r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondeau [Solo]"
  \time 6/8
  f4 f8 \grace g8 f8 e f g4 c8 c4 r8 f,4 f8 \grace g f8 e f g8. f16 e d c4 r8
}
