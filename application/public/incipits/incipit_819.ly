\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 3/4
  c2. bes~ bes
  a4 c8 bes c bes
  a4
}
