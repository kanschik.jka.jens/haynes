\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Vergib, o Vater, unsre Schuld [BWV 87/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe da Caccia 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                      r4 r8 bes8 bes a r8 c8
                      c a fis g a g16 a32 bes a8 c, c bes r8
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe da Caccia 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                        r4 r8 d8 d c r8 es8
                        es c a bes c bes16 c32 d c8 a
                        a g r8
                        
                
                  }
>>
}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key g\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
     R1*15 r4 r8 bes'8 bes a r8 c8
     c a fis g a g16 a32 bes a8 c, c bes r8
}