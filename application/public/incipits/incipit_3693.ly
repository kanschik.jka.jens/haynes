 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  bes,8
  es es es es es d16 c d es d es
  f8 f f f f es16 d  es f es f
  g8 g g g g f16 es f g f g
}


\relative c'' {
  \clef treble
  \key c\minor	
   \tempo "2. Adagio"
  \time 4/4
  c8 c, r g'' g g g g
  g g, r g g g g g 
  g g, f''4 es8 d16 c b8. c16 
  c8 c, r
}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Allegro"
  \time 3/8
  es8. d32 c bes8
  es8. d32 c bes8
  g8 as bes
  es,4 g'8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}