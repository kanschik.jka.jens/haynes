\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Getrost! es fasst ein heiliger Leib [BWV 133/2]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key a\major
                      \tempo "o. Bez."
                      \time 2/2
                      \partial 16
                      % Voice 1
                      e16 a4. r16 e16 cis'4. r16 a16
                      e'4 r16 e16 d e fis e fis gis a[ cis, b cis]
                   
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key a\major
                     \set Score.skipBars = ##t
                        % Voice 2
                  cis,16 cis4. r16 e16 a4. r16 a16
                  cis4 r16 cis16 b cis d cis d b cis[ e, d e]
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key a\major
   \tempo "o. Bez."
  \time 2/2
  \partial 16
     \set Score.skipBars = ##t
     r16 R1*8 r2 r4 r8 r16 e,16 a4. r16 e16 cis'4. r16 a16 e'8 r8 r8
     e,8 fis16[ e fis gis a8] e8
     fis16[ e fis gis a8] a8 a4 r8
     
}