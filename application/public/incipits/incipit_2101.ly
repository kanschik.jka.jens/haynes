\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro vivace"
  \time 4/4
  c'2 c4. c8
  c2 r4 c,8.c16
  d4 r e r
  g4. f8 e r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Adagio"
  \time 3/4
  c2.~ c2.
  c4 bes a
  a16 g bes d f,4 e
  f8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Allegro"
  \time 2/4
  c16 d e f g8 g
  \grace g8 f e16 f d4
  e16 g c b a g f e d8 g,
}
  
