\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Sehet, Jesus hat die Hand [BWV 244/70]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe da Caccia 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key es\major
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                      bes4~ bes8. c16 bes8. c16 bes8. c16
                      bes8. bes,16 bes as bes des des c des g g f g bes
                      as1  ~
                      ~as8.
                    
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe da Caccia 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key es\major
                     \set Score.skipBars = ##t
                        % Voice 2
                        g4~ g8. as16 g8. as16 g8. as16
                        g1~ 
                        g8. as16 as g as c c bes c es es d es g f4~ f16
                      
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key es\major
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
     R1*7 r2 r4 bes4~
     ~bes16 es d c bes4~ bes16 es d c bes as g as
     as4~ as16
  
   

}