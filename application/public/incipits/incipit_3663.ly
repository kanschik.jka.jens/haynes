 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Alla Ciciliano, ma un poco largo"
  \time 12/8
    \override TupletBracket #'stencil = ##f
    \partial 8
    c8
    \grace bes8 a8. d16 c8  c g8. a32 bes \times 2/3 { a16 g f} r8 r
}
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Fuga. Allegro assai"
  \time 2/2
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      \partial 2
      c2
      f4 f f f e8 d c4 r f8 f,
      bes2 bes~
      bes4 c8 d c4 bes
      a c
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Vivace e molto"
  \time 3/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      a'4 g4. a16 bes
      a4 \grace f4 e2
      f4 c d
      \grace c4 bes2 a4
}

