
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Preludes sur tous les Tons por la Flûte Travers"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key g\major
   \tempo "1.er Prelude"
  \time 2/2
\partial 2
r8 b8 a g
 d'2 r16 c16[ b a] b8. c16
 b4. d8 d b16 c32 d d8. c32 d e8. e16 a4 

}
 

 

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Traits pour la Flûte Traversiere "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1.er Trait. fierement"
  \time 4/4
g8. b16 \grace {b16[ c]} d4 g4. b8 d b g16 d8. d16 b8. g16 g8.

 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Preludes pour la Flûte a bec"
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key f\major
   \tempo "1.er Prelude. Moderé"
  \time 2/2
f4 a c8 c a a 
f16 f g a bes c d e f2
 r16 c16[ d e] f g f g g4. f16 g a4 r16

}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Traits pour la Flûte a bec"
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key f\major
   \tempo "1. Premier Trait"
  \time 6/8
f8 a c f a c
f4.~ f16 e32 d c16 d c bes
a bes c bes a g f g a g a e


}

