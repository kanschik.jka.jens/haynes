\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. [Allegro]"
  \time 2/2
  bes16 c d es f4  bes,16 c d es f4
  bes,8 bes' bes4  bes,8 bes' bes4 bes8 a16 g f8[ g16 a]
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 2/4
  bes4 as8. g16
  es'8. d16 d8. d16
  c8 bes a16 g f g
  fis8[ d]
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/4
  bes8 f d' bes f' d
  bes' f d f bes,4
  a8 f c' a f' c
  a' f c f c4
}
