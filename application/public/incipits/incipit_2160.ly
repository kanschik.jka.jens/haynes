 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  g4 a b r8 d16 c
  b8 g g a b4 r8 d16 c
  b8 g g a b g g a
  b c16 d c8 b\grace b8 a4 r8
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Andante"
  \time 4/4
  d4 d16 e d c b8 g c e
  fis,16. a32 d8  d16 c b a b b c d g, d' c b
  a g fis e
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Largo"
  \time 3/2
  r2 b1
  b2. a4 g fis
  g2 e' g fis dis1 e2 g b
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Presto"
  \time 6/8
  d4. r8 b c
  d e d  d g d
  e fis e e g e
  d e d d g d
}