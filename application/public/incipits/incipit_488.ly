\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata prima " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Andante"
  \time 3/4
    \partial 4
    b4
    e b g'
    fis g a
    dis,8 fis e g fis a
    g4 e
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 3/4
  \partial 4
  g'8 fis
  e4 fis dis
  e g8 fis e4
  fis b,
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Gavotte"
  \time 2/2
  \partial 2
  e4 g
  fis8 a dis, fis b, dis fis a
  g4 e  b'8 g e b' 
  c4. b8 a g fis e
  dis4 b
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 2/4
  g'8.[ fis16  g8. a16]
  fis4 b
  e,8.[ dis16 e8. fis16]
  dis2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata seconda" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Andante"
  \time 3/4
  b4 e fis
  g4. g8 a g
  fis4 b8 a g fis
  e4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro moderato"
  \time 4/4
  \partial 16
  e16
  e4~ e16 g fis a
  dis, fis b, dis   fis, b dis, a'
  g8 e r16
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Aria 1"
  \time 3/8
  g'8 e fis
  dis4 e8
  c a b
  g4 \grace fis8 e8
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 3/4
  e8[ b b e] e dis16 e
  fis4 b, r
  
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Andante"
  \time 3/4
  \partial 4
  fis4
  g e2
  fis8 g fis e d fis
  e4 cis2
}
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Allemanda. Allegro"
  \time 4/4
  \partial 8
  r16 b
  b4~ b16 d cis b ais8 cis fis e
  d16 cis b ais b d cis b ais8 cis fis e
}
  
  \relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Largo"
  \time 3/4
  fis4 b, fis' g \grace fis8 e4 \grace d8 cis4
  \grace b8 ais4 b4. cis8
  cis2 b4
  }
  
  \relative c'' {
  \clef treble
  \key b\minor
   \tempo "4. Allegro assai"
  \time 2/4
      fis16 e d cis b8 fis'
  g b, e g
  fis e16 d cis8 b b4 ais
  }

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4
  \partial 8
  d16 c
  b a b c d8 g, g4 r16 d' g f
  e g c b a e fis g fis8 d r
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  \partial 8
  d8 g b16 a g fis e d c8 e16 d c b a g
  fis8 a'16 g fis e d c b8 g r
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gavotta"
  \time 2/2
  \partial 2
  b4 c
  \times 2/3 {d8[ g, a] } \times 2/3 {b8[ c d] } e4 fis
  \times 2/3 {g8[ b a] } \times 2/3 {g8[ fis e] } d4 c
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Giga. Allegro"
  \time 6/8
  \partial 8
  d8
  g d c b a g
  d' a g fis e d
  c' e d c b a
  b a b g4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Quinta " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Largo"
  \time 4/4
  r4 r8 a b4~ b16 b cis d
  cis8 a r d e fis g16 e a g fis8 d
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
    a16 g fis g a8 d cis d r e
    fis16 d e fis g8 fis fis e
}
\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Largo"
  \time 6/8
    fis8. g16 fis8 fis16 b a b g fis
    e8. fis16 e8 e16 ais cis ais fis e
    d fis b fis d b g' fis e d cis b
    b4 ais8 ais4.
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Presto"
  \time 3/4
  d4 e2
  fis4 d8 e fis g
  a4 b cis
  d a8 g fis e d4
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata Sesta" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4
  d4~ \times  2/3 { d16[ c b]} \times  2/3 { a16[ b g]} fis8 g r e'
   \times  2/3 { d16[ e fis]} \times  2/3 { g16[ a b]} c,4 b2
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
 d8 d~ \times  2/3 { d16[ e fis]} \times  2/3 { fis16[ e d]} g16 fis g8
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
  \time 6/8
  d8. e16 d8 g16 a32 b a8 g
  fis8. g16 a8 d,4 b8
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 3/4
  \times 2/3 { g'8[ d c] }  \times 2/3 { b8[ a g] }  \times 2/3 { e'8[ d c] }
  \times 2/3 { d8[ e fis] }  \times 2/3 { g8[ a b] }  \times 2/3 { d,8[ c b] }
}

