\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Gott ist unsre Sonn' und Schild [BWV 79/2]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe [os. Fl.trav.]"
  \key d\major
   \tempo "o. Bez."
   \time 6/8
   fis8 e4 d16 fis a, cis d fis
   g8 fis4 e16 g a, cis e g
   a c, b a b fis' a g g fis fis g 
   
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key d\major
   \tempo "o. Bez."
   \time 6/8
   \set Score.skipBars = ##t
     R2.*12 fis8 e4 d8 cis16 d e fis
     g8 fis4 e4.
     
}



