\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  r16 d e fis g8 d b g16 a b c d c
  b8 g r4 r2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 3/4
  d4 g, e'
  d2 g4 e c d
  b g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/8
    b16 c d8 c
    b g'4
    c,16 d e8 d c c'4
}
