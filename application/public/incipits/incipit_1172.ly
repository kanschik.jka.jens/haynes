 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Andante"
  \time 2/2
    d4. g,8 es'16 d c d  es8 es
    es a, r a d16 c bes c d8 d 
    d g, r 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allegro"
  \time 2/2
    r8 d g fis g d g fis
    g d g f es g c, f6 es
    d8 bes es d c es a, d16 c
} 

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 2/2
  f4 r8 f f4 r8 f
  f4 r8 f, f4 r
  g'4 r8 g8 g4 r8 g
    
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Presto"
  \time 2/2
      g4 a bes c8 d
      es4 d8 c d4 c8 bes
      c4 bes8 a bes4 a8 g d'4 d, g2
} 