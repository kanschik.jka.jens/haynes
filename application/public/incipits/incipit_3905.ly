\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto [II] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro Spirituoso  F-Dur  4/4"
  \time 4/4
  f16 g a g f g a g f4 r8. a16
  c2 a4 f 
  e16 f g f e f g f e4 r8. g16 
  bes2 g4 e
  f16 g a g f8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Menuetto Moderato  F-Dur  3/4"
  \time 3/4
  \partial 4
  c4
  f a c c2 c,4
  f4 f8 e f a 
  \grace a8 g4 g g
}


\relative c''' {
  \clef treble
  \key d\minor
   \tempo "3. Andante  d-Moll  6/8"
  \time 6/8
  a8. bes16 a8 d4 d,8
  d cis cis cis cis4 r8
  g'8. a16 g8 g e cis 
  cis d d d
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro  F-Dur  2/2"
  \time 2/2
  f2 e4 r
  bes'2 a4 r
  c c8 c bes a g f
  e f g e c c'8 c c 
  
}
