\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro molto"
  \time 4/4
  \partial 16
  c16
  f2~ f8 f a f
  f e e e e4 r8. c
  g'2~ g8 g bes g
  g f f f f r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Tutti]"
  \time 3/4
  c8 r d r b r
  c4. g'8 f16 e d c
  c b b8 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*6
   r4 r c16 d e f
   g4 \grace { a32[ g f g] }
   a4 \grace { b32[ a g a] }
   b4 \grace { c32[ b a b] }
   c4. c,8 e g
   a4 a a
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegretto"
  \time 2/4
  \partial 8
  c8 
  f f e f
  a16 g g8 r c,
  g' g f g
  bes16 a a8 r
}