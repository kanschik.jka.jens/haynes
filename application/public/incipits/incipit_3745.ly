\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
    bes4 c16 bes es d  g f f4 d16 bes 
    bes a a4 c16 es, es d d4 d'16 bes
}
