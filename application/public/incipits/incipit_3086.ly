\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Können nicht die roten Wangen [BWV 205/7]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key fis\minor
   \tempo "o. Bez."
   \time 4/4
   a16 b cis4 b8 a gis16 fis gis8 eis
   fis16 cis fis a gis fis eis fis gis cis, gis' b a gis fis gis
   a8 b32 cis d16~ d8 cis
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt (Pomona)"
  \key fis\minor
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
    R1*9 a16 b cis4 b8 a gis16 fis gis8 eis
    fis16 eis gis8 cis4 r2
}



