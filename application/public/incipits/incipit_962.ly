\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
    c8
    \override TupletBracket #'stencil = ##f
    \times 2/3 { a16 g f} f4 f'8 \times 2/3 { e16 f g} c,,4 g'8
    \times 2/3 { e16 d c} c4 bes'8

}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 2/4
  \partial 8
  f,8
  \override TupletBracket #'stencil = ##f
  bes8 bes, \times 2/3 { bes16 d f} \times 2/3 { bes16 d f}
  \grace f8 es8 d r
  
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 3/8
     f16 c \grace bes8 a4
     d16 bes \grace a8 g4
     a16 c f c bes a
     \grace a8 g8 f16 e d c
}
