 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. "
  \time 4/4
  bes'4 a bes r8 f
  g f16 g es8. d16 d4 r8 c
  d bes16 c d8 e f f, r f'
  g es16 f g8 a bes c, r
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Largo"
  \time 3/2
  f2 f f
  f8 f, f g  g b b d  d f f as
  as2 as as
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Allegro"
  \time 12/8
  \partial 8
  f8
  bes4 f8 d es f bes,4 f8 bes4 bes8
  c a f f'4 es8 d c bes 
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}