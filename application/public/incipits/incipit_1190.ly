\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. All. [Tutti]"
  \time 4/4
  f4 r8 c32 d e16 f8 f f f
  f4 g a e
  f r8 c8 f2
  f4 g a e f r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. All. [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*36 
   g'2. f8 e
   d c c2 d8 e
   f2 e8 d g f
   d4. dis8 e4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andagio"
  \time 3/4
    e2 f4
    g2 f16 e d c
    d4 d e
    f4. e16 f g f e d
    c4 c d
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo"
  \time 3/4
  \partial 4
  c'8 a
  \grace g8 f4 f g8 a
  c bes g4 g8 f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Rondo"
  \time 2/2
  c'2 a8 g a bes
  g4 g g a
  f4. a8 g4. f8
  \grace f8 e4 e8 d c4 c
}