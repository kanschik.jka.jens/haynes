\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  \partial 8
  d,8 fis a d4 a8 g fis d
  fis a d4 fis,8 e d fis'
  e d16 cis d8 e cis b a16 d e fis
  e8 d16 cis d8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \partial 8
  r8
   \set Score.skipBars = ##t
   R1*15 
   r2 r4 r8 a
   d4 e fis16 e d cis  d e fis g
   a8 d, b' d, cis d r a'
   b16 cis d4 \grace cis8 b8 \grace a8 g4. a8
   fis16 e d  e fis g a fis fis8 e r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Largo"
  \time 4/4
  d8 f16 g a8 f f e r a,
  e' g16 a bes8 g g f r
}



\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro [Tutti]"
  \time 12/8
  d4 r8 e d cis d4 r8 b4 r8
  a4 r8 g4 r8 fis e d r d' e
  fis e d fis e d a'4 r8a,4 r8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro [Solo]"
  \time 12/8
   \set Score.skipBars = ##t
   R1.*15 
   r2. r4. r4 a8
   d fis e d cis b  a d cis b a g
   fis4 g8 a4 b8 a4 d,8 r4
}