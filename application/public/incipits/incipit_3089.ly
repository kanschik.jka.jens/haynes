\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Hochgelobter Gottessohn [BWV 6/2]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe da Caccia"
  \key es\major
   \tempo "o. Bez."
   \time 3/8
   g16 bes es8 f,
   g16 es d es as es
   bes' c32 des c8 es,
   d16 bes as bes es bes
   f' g as f f'8~ f16
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key es\major
   \tempo "o. Bez."
   \time 3/8
   \set Score.skipBars = ##t
    R4.*16 g16 bes es8 f,
    g16 f es f g as
    bes c32 des c8 as f4 r8
}



