 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
  \time 2/2
    fis8 d16 e fis8[ g ] fis[ fis16 g] a8 b
    a[ g16 fis] e8. d16 cis4 r8
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Allegro"
  \time 3/4
  r8 a' fis a d, fis
  e a, e' fis g a
  fis4 d a'
  a gis4. a8
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Rigadon"
  \time 2/2
  \partial 4
  a'4
  g2 fis
  e8 d e fis e4 fis
  e2 cis
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4. Menuett"
  \time 3/4
    fis4 g8 fis g fis
    e4 fis8 e fis e
    d4 e8 d e d
}