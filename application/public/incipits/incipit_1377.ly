 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Vivace"
  \time 3/4
  c8 g r16 c, es g  c es d c
  d8 g, r16 d g b d f es d
  es8 c r16 g' es c  g' es c g'
}
 