\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Allegro"
  \time 4/4
  e4 gis a4. a8
  a g g f f e r e,
  a4 gis a b 
  c16 e d c
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Grave"
  \time 3/2
  b2 r b
  a a r
  g r g
  g a r 
  a a1 a1.
}


\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Allegro"
  \time 12/8
   a4. e'8 c a  f' b, a gis e' d
   c b a e' c a  f' a g a b a
   g fis e
}

