\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite in F " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Prelude"
  \time 2/2
  r4 c4 a f
  bes2~bes8 a bes c
  a4. g8 a4. bes8
  g4. f8 g4 a
  bes2. bes8 bes 
  bes4 a g4. f8 f2

}

\relative c''' {
  \clef treble
  \key f\major
   \tempo "2. Sarabande"
  \time 3/4
  a4 a4. bes8
  g4. a8 bes4
  a g4. f8
  g2 c,4
  c' a f 
  d g 

}

\relative c''' {
  \clef treble
  \key f\major
   \tempo "3. Gavotte"
  \time 2/2
  \partial 2
  a4 c
  f, g8 a bes4. c8
  a4 f a f
  d e8 f d4. c8 c2

}

\relative c'''{
  \clef treble
  \key f\major
   \tempo "4. Bouree"
  \time 2/2
  \partial 4
  c4 a4. g8 f4 bes
  g f8 g e4 f
  g a bes a8 g
  a4 g8 a
  f4 a
  a g f e

}

\relative c'''{
  \clef treble
  \key f\major
   \tempo "5. Menuet 1"
  \time 3/4
  a8 bes c4 c
  a4. g8 a4
  f g a
  g8 f g a bes g
  

}

\relative c''{
  \clef treble
  \key f\major
   \tempo "6. Rigaudon"
  \time 2/2
  \partial 4
  f8 g
  a2 bes
  a4. g8 f4 g
  a f bes a
  g8 f g a g4 
  

}

\relative c''{
  \clef treble
  \key f\major
   \tempo "7. Loure"
  \time 6/4
  \partial 4.
  a8 bes4
  c4. d8 es4 d4. c8 bes4
  c4. d8 es4
  d4. c8 bes4
  c a d c bes a g2.
  

}
\relative c''{
  \clef treble
  \key f\major
   \tempo "8. Petite Chaconne En Rondeau"
  \time 3/4
  r4 f8 g a bes
  c4 c c 
  c bes bes 
  bes8 a g a bes c
  a g f g a bes
  c4 c c c

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite in a " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Prelude"
  \time 2/2
  r2 c4 a 
  f'2 f4. g8 
  e2 e4. e8
  e2 d4. d8
  d4 e8 b c2~
  c b4. b8
  b4. c8 a4 gis8 a
  gis2

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite in Bb " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Prelude. Lentement"
  \time 2/2
  r4 f4 f es8 d
  es2. es4
  es d8 c d4. es8
  c4. c8 f4. f8
  f4. g8 e4. f8
  f4. f16 g as4 g8 as
  g4. g8 g4.

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite in A " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "1. Prelude"
  \time 2/2
  a2 e4 a8 gis
  fis2~ fis8 fis gis a 
  gis 2 a~
  a gis4. a8 a2 r2

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite in e " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key e\minor
   \tempo "1. Prelude. Grave"
  \time 2/2
  r2 b4. b8
  a2 a~ 
  a g4. g8
  g2 fis~
  fis fis4. fis8 g4. a8 fis4. e8 e1

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite in b " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Prelude"
  \time 3/4
  r4 fis8 e fis cis
  d4 cis4. b8
  ais2.
  b4 ais4. b8
  cis2 cis8 cis
  fis, fis' e4. e8
  e4. fis8 d4 \grace cis8 
  cis4.

}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite in f " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\minor
   \tempo "1. Prelude. Lentement"
  \time 2/2
  f2 c4 as'
  e2 f2~
 f e4. e8 f2 r2
 r2 g4. g8
 g2 f4. c8
 des2 des4. des8
 e4. f8 e4. f8 f2

}

