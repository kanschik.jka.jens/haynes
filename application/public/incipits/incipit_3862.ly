\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata 2-IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio"
   \time 4/4
   d8 e16 fis e8. d16 cis8 d r d
   g16 e fis g  cis, d d8 e4 r8 a
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
   \time 4/4
   d4~ d16 fis e d
   cis b a8 r e'
   fis g a g16 fis e d e8 r
}
\relative c'' {
  \clef treble
  \key fis\minor
   \tempo "3. Adagio"
   \time 3/4
   r4 e, a a gis r
   a b c c b r
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Allegro"
   \time 12/8
   d4. e8 a, e' fis4. r8 a a,
   b g' fis e4 d8 cis4. r8
}
