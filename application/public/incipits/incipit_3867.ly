 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio"
  \time 2/2
  c4~ c16 a g f d' bes c8 r f16 a,
  bes8 g16 a bes[ d c bes] a g f8 r
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 2/4
  f,16 g a bes  c d e c
  f8 f, r f'
  g16 f e d c bes a g a8 f
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Adagio"
  \time 3/4
  d4 a f'
  f e a d, g8 f e d
  cis4.
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Vivace"
  \time 3/4
  c4 f e f f,8 g a bes
  c4 d c
  bes2 a4
}