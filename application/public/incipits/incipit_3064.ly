\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Es halt es mit der blinden Welt [BWV 94/7]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key fis\minor
   \tempo "o. Bez."
   \time 4/4
   \partial 8
   cis8 a4 fis8 fis16 eis fis8 cis4 fis16 gis32 a
   gis8 cis,4 gis'16 a32 b a8 fis4
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key fis\minor
   \tempo "o. Bez."
   \time 4/4
   \partial 8
   \set Score.skipBars = ##t
     r8 R1*7 r2 r4 r8 cis8
     b16 a gis fis
     d'8 cis16 b cis8 a fis cis'
     gis a16 fis b8 a gis a16 fis gis8
}



