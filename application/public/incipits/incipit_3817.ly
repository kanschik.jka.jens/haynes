 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Andante"
  \time 4/4
  bes4 es d g
  f8 bes es, c d4 g
  f8. g32 a bes8 bes bes16 c, c8~ c \grace { c16[ d]} es8
  \times 2/3 { d16[ c bes]} bes8
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Allegro"
  \time 3/4
  f8 f~ f16.[ f32] \grace g16 f32 es f16 bes f bes f
  f es es8~ es16.[ es32] \grace f16 es32 d es16 g es g es
  cis d d8
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Allegro"
  \time 3/4
  f8. g16 f4 es
  \grace es4 d2.
  c8 es c a f es'
  es4 d d
}
