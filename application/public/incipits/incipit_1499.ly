 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  g'8. fis32 e d16 b' a g
  g8 fis r c'
  c32[ b e d] d16. g,32 g8 fis64 e d32 a' c, c8 b r4
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegretto"
  \time 4/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

  g'16. d32 d8 \times 2/3 { r16 b a} \times 2/3 { b16 c d}
  \grace d16 c8 b \times 2/3 { r16 b a} \times 2/3 { b16 c d}
  c8 b e4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Vivace"
  \time 3/4
  d4 g fis \grace fis4 g2 d4 e d b'
  \grace d,4 c2 b4
}

