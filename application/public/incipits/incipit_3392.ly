 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Ouverture"
  \time 4/4
    es,8. g16 g8. bes16 bes8. es16 es8. g16
    es,4 r8. es16 es4. d16 es f4.
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Pastorelle"
  \time 6/8
  bes4.~ bes4 es,8
  \grace { es'16 d} c4.~ c4 es,8
  bes'16 c bes g  as8  g16 as g es f8
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. La Vivacità"
  \time 4/4
    es,4 g16 f es8 bes'4 g16 f es8
    c'8 c c c  c bes r4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. La Morosità"
  \time 6/4
    \partial 4
    es,4
    bes'2 g4 es'2 bes4
    g'2 as,4 g2
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Menuet"
  \time 3/4
    es,4 es16 f g8 g f
    g es bes'2
    c16 d es8 bes16 es8. es8 d
    es d
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "5. Air en Sarabande"
  \time 3/2
    r4 d, es f g
    bes8. g16 g8. bes16  bes8. g16 g8. bes16  bes8. g16 g8. bes16 
    es'4 r
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "6. Gavotte en Rondeau"
  \time 4/4
  \partial 2
  es4 d
  \grace d8 es2 bes
  g4 es bes' bes 
  es d8 es f4 es8 f
}