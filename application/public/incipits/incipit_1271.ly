 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Largo e staccato [Vns unisono]"
  \time 3/4
  e4 a,4. r16 e'
  f4 gis,4. r16 f'
  f4 g,4. r16 e
  dis4 fis,4.
}


\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "1. Largo e staccato [Petrus]"
  \time 3/4
       \set Score.skipBars = ##t
   R2*9
   r4 e2~
   e8 f e4 c a2 r4
   f'2 d4 b2 b4 b c d c a r
}
\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "1. Largo e staccato [Oboe]"
  \time 3/4
       \set Score.skipBars = ##t
   R2.*10
   r4 e2~
   e8 f e4 c a2.
   a a2 gis4 e' c a
}
