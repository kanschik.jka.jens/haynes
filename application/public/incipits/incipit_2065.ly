\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
  c8
  f4~ f32 e d c bes a g f
  \grace e'16 d8 c4 \times 2/3 {f16[ g a] } \grace a16 g8. f16
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 3/4
  bes4. f'8 \grace es16 d8 \grace c16 bes8
  es8. f32 g f8 r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 3/8
  \partial 8
  c8
  f16 a r c bes g
  a f r a g e
  f c r c bes g a32 f g a bes[ c d e] f8
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
  b16. c32
  d8 d~ d g16 e
  e d d8 r g16 e e d d8 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Andante"
  \time 3/4
  a'2 fis8. a16
  \times 2/3 { a8[ fis g] } g4 r8 r16 e
  \times 2/3 { fis8[ e fis] } \grace a8 g4 \grace fis8 e4
  \grace fis8 d4 d 

}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/8
  \partial 16
  d16
  d8. b32 c d16[ d]
  d8. b32 c d16[ d]
    d16. g32 d4
    \times 2/3 { e16[ d c] } b8 a
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegretto"
  \time 2/4
  c8 c16. c32 c8 c
  d8. e32 f e8 g
  a,[ \times 2/3 { d16 e f]} e8 d
  e32 g f a g4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 2/4
  \partial 16
  c16
  f8 f,16. f32 f16. a32 g16. bes32
  \grace bes16 a8 a16. a32 a16. c32 bes16. d32
  c8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Presto"
  \time 2/2
  g'2 e8 g f a
  g2 e8 g f a
  g2 e4 c' g2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro ma non tanto"
  \time 2/4
  \partial 8
  g'8
  g8. fis32 e d8 e
  d \grace a'16 \times 2/3 {g16[ fis e] } d4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Grazioso"
  \time 3/4
  \times 2/3 { e8[ f e] } g4 \grace f8 e4
  \grace d4 c2.
  \times 2/3 { f8[ g f] } a4 f
  \grace g8 f4 e
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
  d4. g8 g d e4 d r
  \grace fis16 e8 d16 c b4 a g'8 d d4 r
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro non presto"
  \time 6/8
  \grace d16 c16 b c4 \grace d16 c16 b c4
  c16 b d c e d  f e g f e d
  \grace d16 c16 b c4 \grace d16 c16 b c4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegretto"
  \time 3/4
    g'2 f4
    \grace f16 e8 d16 c b4 c
    a8. c16 g8. c16 f,8. d'16
    e,8. b'16 \grace b4 c2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1.Allegretto "
  \time 2/4
  \partial 8
  bes8
  bes16 es es8 r g
  g16 f f8 r bes,
  bes16 f' f8 r as
  as16 g g8
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Andante"
  \time 3/4
  c4 c c
  des8. c16 des2
  as'4 \grace c,4 b2
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Allegro ma non tanto"
  \time 3/8
  es8 g bes
  bes4. c8 bes as
  \grace as8 g4.
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
