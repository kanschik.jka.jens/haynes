\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
    d16 cis b a  g fis e d
    a'8 b4 g8
    a d cis b
}


\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Siciliana. Affetuoso"
  \time 6/8
  \partial 8
  cis16 d
  e4 fis8 e8. a16 d,8
  d4 cis8~ cis4 cis16 d
  e4 fis8 e8. a16 cis,8
  cis4 b8~ b4
}


\relative c'' {
  \clef treble
  \key d\major
 
   \tempo "3. Allegro"
  \time 3/4
    a8 fis d a' b g
    a cis d4 r8 b
     a d cis b a g
  fis e d4 r
}
