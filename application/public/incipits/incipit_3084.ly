\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Schäme dich, o Seele, nicht [BWV 147/3]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key a\minor
   \tempo "o. Bez."
   \time 3/4
   r4 b4 e dis2 g4~ g f8 g a f
   dis4 b'8 e, c' a gis4 b d,4~ d8
   
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key a\minor
   \tempo "o. Bez."
   \time 3/4
   \set Score.skipBars = ##t
     R2.*9 r4 e4 f8 d 
     e2 c'4~ c b8 c d b gis2 r4
     
}



