\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio [Oboe]"
  \time 3/4
  bes8 c16 d  es4 f
  d es bes
  as'2. g4 es4. bes'8
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Adagio [Alto]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*14 
    bes8 g es4 f 
    d es bes
    as'2. g4 r r
}
