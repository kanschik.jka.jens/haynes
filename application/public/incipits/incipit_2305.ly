
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata [1] a Oboe Solo con il Basso"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key c\major
   \tempo "1. Andante"
  \time 4/4
r4 r8 g8 g g g g 
c16 b c8 r8 g'8 c, g' e c
a' c,16 a' g8 c,

 
}

\relative c'' {
\clef "treble"   
  \key c\minor
   \tempo "2. Adagio?"
  \time 3/4
c4 g es'
c b r4
d f, f'
f8 d es2
fis4~ fis8 es d c
f4~ f8 d b g 
des'4.
 
}

\relative c'' {
\clef "treble"   
  \key c\major
   \tempo "3. Allegro assai"
  \time 2/4
r4 c4
r4 c4
r4 c4
r4 \times 2/3 {g'8 e c}
a'4 c,
\times 2/3 {e8 g f} e8. d16
c8. b16 c4~ 
\times 2/3 {c8 e f} \times 2/3 {g8 e c}
a'8 fis 
 
}







\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata [2] a Oboe Solo e Basso "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. ohne Bez."
  \time 4/4
f2~ f8 g16 f es4~
es8 f16 es d4~ d8 es16 d c8 bes
a4

 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/2
bes2~ bes4 a8 g
f2 f'~ f4 es8 d c4 bes
a2~ a8 bes c d
es4 es4~ es8 d c bes
a g f2.

 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Andante"
  \time 12/8
\partial 8
d8 d4 d8 es4 b8 c4. r8 r8 c8
des8 c des as'4 c,8 b4. r8 r8 b8
c4 g8 es'4.~ es8 d c as4.~ as4

 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Allegro"
  \time 3/4
bes4. c8 d es
f4 f, f'
bes bes4. c8
a4. g8 f4

 
}




\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata [3] Oboe solo Con il Basso "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
f2 bes,4~ bes16 c d es
f8[ g] a, bes es, d r4

 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante"
  \time 3/4
d4 es8 g bes es,
cis d r8 a8 f' as
b, c r8 g es' g
a, bes r8 bes8 a bes
es4~ es16 es f g f8 es
d4~ d8 fis g bes c,4
 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/4
bes4. c8 d es
f4 f, f'
bes bes4. c8 
a4. g8 f4

 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata [4] Oboe solo Con il Basso "
    }
    \vspace #1.5
}
 
\relative c' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 2/2
es1
\times 2/3 {f4 g a} bes2~ 
bes4. as8 g4. f8
g4 f es r4
as2 c
f as
g, bes es g

 
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Andante"
  \time 4/4
g4 r8 es'8 as,4 r16 f16 e f
d'4~ d16 g, fis g e'4. g,8
fis4 r8

 
}

\relative c' {
  \clef treble
  \key es\major
   \tempo "3. Allegro"
  \time 3/8
es8 g bes
es16 d es g bes8
as g f 
es bes4
g'16 f g as bes8
as, g f f'16 es f g as8 g, f es


 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata [5] Oboe solo Con il Basso "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegretto"
  \time 4/4
g4 r8 g8 d' d d d 
bes32 a g16 g4 d'8 es32 d c16 es4 g8
d32 c b16 d4 

 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Andante"
  \time 3/4
d4 g,8 es' d cis
d a4 es'8 d c
bes a g d' es g,
a16 g f8~ f es' d a
g16 f es8~ es d' c g
 
}

\relative c' {
  \clef treble
  \key d\major
   \tempo "3. Rondeau"
  \time 2/4
\partial 8
d8 
d4 d8 e 
fis e d fis
fis4 fis8 g
a g fis a 
a4 a8 d 
b b' a, a'
g, g' fis, fis'
e16 d cis b a g fis e 
d4



 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 2/2
\partial 4
d4 g,8 a b c d4 e
d8 fis \grace fis8 g2 d4
e4. d16 c d4 g,
c b r4

 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata [6] Oboe solo Con il Basso "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 2/4
g4 d16 bes' a g 
g4 fis g r4
d'4 \times 2/3 {g,8 bes d}
g8. g16 a8. bes16
\grace g8 fis2
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Andante"
  \time 4/4
bes4~ bes16. f'32 d16. bes32 g'8 f~ f16 d c bes
g' e f8~ f bes, bes a4 c8
c8 bes4 d8 d c4 


 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 12/8
g4. d bes' g
d'8 bes g d'4 g8 fis4. g
a4 c,8 d,4 a''8 bes,8 a bes g'4.
es4 a,8 c,4 


}


