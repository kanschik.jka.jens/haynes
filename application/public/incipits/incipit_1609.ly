\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
  r8 r16 f16 d8. bes16 bes'8. f16 d8. bes16
  d4 a
}
