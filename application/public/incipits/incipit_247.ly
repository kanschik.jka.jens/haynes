\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key bes\major
    \time 4/4
    \tempo "1. o.Bez."
      r4 r8 d16. es32 f8  bes,16 d  f8 g16. a32 
      bes8 bes, r8 f'16. bes,32  g'8 f16 g es d es f
      d8
}

 