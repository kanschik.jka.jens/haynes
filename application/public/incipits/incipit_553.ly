\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Trio II] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
  b8 c
  e d d2 e8 fis
  a g g2 fis8 g
  b a g fis \grace e8 d4. c8
  c2 \grace {a16[ b]} b4 r4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Tempo di Menuetto"
  \time 3/4
  d4 b8 g' fis e
  d4 b8 g' fis e 
  d e fis g a b
  c4. a8 fis4
  g2 \grace b16 a8 g16 fis
  g2

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Trio IV] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  c2 c8 d e f
  a g g2 g4 
  g8 f e f f4. f8
  b4. e,8 e2
  e8 d cis d cis d cis d
  g4. c,8 c2

}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Rondeau. Allegretto"
  \time 2/4
  \partial 4
  g16 e f d
  c8 c \grace d8 c8 b16 c
  d8 r8 e16 f e c
  f g f d g8 a
  \grace e8 d4

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "[Trio VI] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
  d8 d
  g b, b2 cis4
  d8 a a2 a8 b
  \grace d16 c4 b8 a e g fis a
  g4. a16 b a4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Tempo di Menuet"
  \time 3/4
  d8. e32 d
  c4 b
  a2 b4
  c16 d e d c8 d e fis 
  g d d2

}
  
  
