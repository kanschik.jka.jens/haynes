\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
    \partial 4
    g8. g16
    g4 c8. c16 c4 e8. e16
    e2 c4 g'
    g e8. a16 g8. f16 e8. d16
    e16 d c8 c4 r
  }

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*25
   r2 r4 g'
   g2. f8 e
   e2. d8 c
   c4. d16 e d8 c b a
   a4 g r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  c16 e c e  c e e e  c e c e  c e e e
  d f d f  d f f f  d f d f  d f f f
  e g g g a f f f g e e e f d d d e8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*9
   g'2. f16 e d c
   c16. e32 d8 d2 r4
   f4. g16 a \grace a16 g8. f16 \grace f16 e8. d16
   f e d c c2 r4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegretto"
  \time 2/4
  c2 d
  e8. f32 g f8 e
  e4 d8 r
  g4 f16 e d c
  \grace c8 d4. e8
  f4 e16 d c b
  d c b a g4
}