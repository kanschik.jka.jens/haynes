 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Larghetto"
  \time 4/4
    \partial 8
    bes8
    es es4 es8 es32 d16. c32 bes16. c4
    bes16 bes8 es32 c
    bes16 as8 g32 f
}
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegretto"
  \time 4/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
    bes'4~ bes16 g f es \grace es4 d4. es8
    c'8 c4 bes8 \grace bes4 as2 g4 r
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Vivace"
  \time 3/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      bes2 es4
      \grace es4 d2.
      es4 bes c
      \grace bes4 as2 g4
}