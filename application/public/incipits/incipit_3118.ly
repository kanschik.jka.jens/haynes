\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Er richt's zu seinen Ehren [BWV 107/5]" 
    }
    \vspace #1.5
}
 
 
\relative c' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key b\minor
                      \tempo "o. Bez."
                      \time 12/8
                      \partial 8
                      % Voice 1
                      fis8 g fis16 e fis8 b16 cis cis8. b32 cis d8 cis16 d b8 r8 r8 
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key b\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                   d,8 e d16 cis d8 r8 b'8 ais b e,16 fis d8 r8 r8 
                  }
>>
}


\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key b\minor
   \tempo "o. Bez."
  \time 12/8
  \partial 8
     \set Score.skipBars = ##t
     r8 R1.*4 r2. r4. r8 r8 fis8 b16 cis cis8. b32 cis d8 cis16 b e8 cis16 d d8. cis32 d e8 d16[ cis]

}