\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Ich bin ein guter Hirt [BWV 85/1]" 
    }
    \vspace #1.5


}


\relative c''' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
     g2~ g16 f as g f es d c
     b8. c16 c8. b32 c d16 es d es es8. d32 es
     f2~ f16 g e f f8.
  
  
   

}


}


\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key c\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
  R1*6 r8 c8 b c es,8. f16 f8. es32 f 
  g4 r4
  
   

}