\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Andante [Tutti]"
  \time 4/4
  r16 b b b  b a32 g fis16 b  g8 e'4 dis8
  e16 g g g fis e32 d cis16 fis d g g g fis e32 d cis16 fis
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Andante [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*5 
    r2 b4. c16 b
    b4. e16 b b8 c16 b b8 e16 b
    b8 a16 g fis8. b16 g8 e r4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro molto [Tutti]"
  \time 4/4
  r8 e,16 fis  g a b c d c b d  c b a c
  b a g b  c b a c  b a g b  c b a c
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro molto [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*7
   r2 r8 e,16 fis  g a b c
   d c b d  c b a c  b a g8 r4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Largo"
  \time 3/2
  r4 e b c b c8 a
  b e, e'4 b c b
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 2/4
  r8 b a fis
  g4 b e,8 e' d b
  c4 e
}