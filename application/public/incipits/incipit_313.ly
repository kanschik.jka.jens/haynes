\version "2.16.1"
         #(set-global-staff-size 14)
         
         \relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  g4 b16-. a-. b-. c-. d4 g fis16 e fis g a8. a16 a4 r4 a4 c,~ c8 a es' c
         }
         

         
         \relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro [Solo}"
  \time 4/4   
  d2 \tuplet 3/2 {g8( d g)} \tuplet 3/2 {bes (a g)} \grace {g} fis2~ fis8 (g) fis (g) a4 c, c8 (a) es' (c)
         }
  

         \relative c'' {
  \clef treble
  \key g \minor
   \tempo "2. Largo [Tutti]"
  \time 2/4
 \partial 4 bes16 a (bes c)  \tuplet 3/2 {d16( bes f)} f8 r16 bes d32 (bes) f' (d) \tuplet 3/2 {es16( c a)} f8 r16 c' es32 (c) g' (es)
 }
 
     \relative c'' {
  \clef treble
  \key g \minor
   \tempo "2. Largo [Solo]"
  \time 2/4
\partial8 f,8 bes4 d16 (bes) f' (d) \grace {f16} es16. \trill d32 es8 r16 c g' es 
 \tuplet 3/2 {d bes f'} \tuplet 3/2 {g es c} \tuplet 3/2 {f d bes} \tuplet 3/2 {es c a}
  }
   \relative c'' {
  \clef treble
  \key g \minor
   \tempo "3. Allegro molto [Tutti]"
  \time 6/8
  g8 bes a g4 r8 bes d c bes4 r16 g' g4. (fis4) g8 a a, bes c bes a 
  }

\relative c'' {
  \clef treble
  \key g \minor
   \tempo "3. Allegro molto [Solo]"
  \time 6/8
 \partial 8 d,8 g4. bes d g g (fis4) g8 a fis d c bes a 
  }
 