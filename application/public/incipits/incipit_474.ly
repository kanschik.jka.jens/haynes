 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o. Bez."
  \time 4/4
  \partial 8
  g8 c e16 d c8 g c e16 d c4
  f8 e16 d e8 d16 c d4 r
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 2/2
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Largo"
  \time 6/8
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}