\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro"
  \time 4/4
  c'2 a4 f
  e f8. e32 f g4 r
  bes2 g4 e
  f g8. f32 g a4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Andante"
  \time 3/4
  e4. f8 e4 e g8 f e d
  d2 c4 c r r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo"
  \time 2/4
  c8 f f16 e d c
  f8 a a g
  c,16 f a c  bes g a f
}