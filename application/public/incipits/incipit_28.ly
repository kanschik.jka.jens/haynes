 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

 

 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
  d16[ c bes a] g8 fis g16[ a bes c] d8 g,
  es'8 g, fis es' es d
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio pizzicato"
  \time 2/2
    r4 g d'4. c8
    bes16[ g bes16. d32] bes16 g bes16. d32 d8[ g,] 
    es'32 d c bes a bes c g
    fis8[ e16 d]
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Vivace"
  \time 3/8
  g8 d g
  a16 g a8 r
  bes16 c d8 d~
  d16 bes g d' bes g a[ bes c8]
}

