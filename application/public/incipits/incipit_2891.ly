\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  <g e' c'>2 
  <g d' b'>2 
  <g e' c'>2 r4 g'8 e
  c4 c2 e4 d4. b8 c4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*89 
  r2 r4 g'8 e c4 c2 e4 d4. b8 c4 r
  r4 e2 g4 f4. d8 e4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante [Tutti]"
  \time 3/4
  c8. bes16 a8 a d b
  c4 a r
  c16 bes r8 bes16 a r8 a16 g r8 f4 r r 
}

\relative c'' {
  \clef bass
  \key f\major
   \tempo "2. Andante [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*15 
   c,2. c
   c16 d e f g f e f f es d cis
   d8 e16 f c4 r8 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo"
  \time 2/4
  g'4 a16 g f e
  d8 d16 f e4
  a8 g f e d16 c b c d e f fis
}