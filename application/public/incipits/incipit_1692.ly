\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
   \partial 16
   c'16 c4 r8. a16 a4 r8. f16 f4 r8. c16 a4 c f,1
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \partial 16
   \set Score.skipBars = ##t
   r16 R1*53 r2 r8 f8 g a bes2 a8 g f e c'4. a8 f4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Solo]"
  \time 3/4
   g'2 \grace f8 e8 d16 c \grace d8 c8 b b4 r4 g4 d''8 \grace c8 b8 \grace a8 g8. f16 f e a g g4.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondeau. Allegretto [Tutti/Solo]"
  \time 2/4
  f4 a8 f \grace g8 f8 e e r8 f4 a8 f d'c c r8
  }
