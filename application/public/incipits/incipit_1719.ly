 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
    f2 d 
    bes r8 a bes c
    d2  bes f  r8 f g a
    bes2. g'4 es c a f'
    bes,
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*137 
   r2 r4 f
   f4. d8 es4. \grace { f16[ es d]} g8
   f8. bes16 bes4. a8 g f
   f8. es16 d2 c4
   c cis d r
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Rondo con Spirito"
  \time 3/4
  bes8 bes16 c bes8[ d f d]
  es8 es16 f g8 g16 a bes8 g
  f f16 g f8[ d bes d]
  c16 bes a g f8[ f g a]
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Largo"
  \time 6/8
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}