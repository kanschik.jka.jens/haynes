 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. [Allegro]"
  \time 2/4
  \partial 8
    a'8
    d, d d e
    a, a a b16 cis
    d cis d e  fis e fis gis
    a8 e4 fis16 gis a8 e4
}


\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "2. Largo"
  \time 4/4
  \partial 8
    fis16 d
    b8 b b g' g16 b, ais8 r fis'16 b,
    g'8 e e c' c16 e, dis8 r
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Allegro"
  \time 3/8
    a'16 g fis e d8
    d b' a cis, g' fis
    e fis16 e fis8
    e fis16 e fis8
}
