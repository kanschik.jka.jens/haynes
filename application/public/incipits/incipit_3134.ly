\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Sende deine Macht von oben [BWV 126/2]" 
    }
    \vspace #1.5
}
 
 
\relative c''' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key e\minor
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                     r4 g16 fis e dis e8 b b b 
                     a' b,16 cis dis e fis g  a g fis e b'8. e,16
                     dis b dis fis
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                      r4 b,16 a g fis g b e b a g fis e
                      dis e fis8~ fis e16 dis e fis g8~ g16 fis g a 
                      b8[ b ]
                       
                        
                
                  }
>>
}


\relative c''' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key e\minor
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
     R1*6 r4 g16 fis e dis e8 b b b 
     a' b,16 c b a g fis g e g b \grace dis8 e8. fis16 dis4 r4
}