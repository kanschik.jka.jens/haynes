 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4	
  \partial 4
  d8 d
  es es d d es es d d
  es8. f16 g4 r c,8 c
  d d c c d d c c
  d8. es16 f4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro [Solo]"
  \time 4/4	
  \partial 4
  r4
  \set Score.skipBars = ##t
   R1*49 
   bes'2 f \grace es8 d4 c8 bes bes4 bes
   a8 bes c d  es c f es
   \grace es8 d4 c8 bes bes d c bes
}




\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Rondeau. Allegretto"
  \time 2/4
  \partial 4
    f16. g32 f16. g32
    f4 es16 d c bes
    a8 bes c d
    es8 es16. c32 d8 d16. bes32
    \grace d8 c8 bes
}
