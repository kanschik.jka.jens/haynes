\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Aria"
  \time 4/4
  c4 a8 f g'4 e8 c
  bes2 a
  r4 r8 c d f e g
  f4 r8 c d e16 f e8 f16 g 
  f4
}



\relative c'' {
  \clef bass
  \key f\major
   \tempo "2. Aria "
  \time 12/8
    \partial 8
    f,,8
    c'4 bes8 a8. g16 f8 d'4 d,8 d' c8
    bes8. a16 g8 c4 c,8 f,4.
}

\relative c'' {
  \clef bass
  \key f\major
   \tempo "3. Duetto [Bsn]"
  \time 2/2
  \partial 4
  r8 bes,16.  a32
  bes8 f16. es32 d8 c16 bes f'[ f,]
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Duetto [Bsn]"
  \time 2/2
     \set Score.skipBars = ##t
  \partial 4
  r4
  R1*5
  r2 r4 bes8. c16
  d c c8 f32 g f16 es d c8 f, c' c
  c2~ c8 bes16 a g8 a16 f
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Aria"
  \time 3/8
    f8 c e
    f8. c16 c8
    f16 c c c c c
    
}
