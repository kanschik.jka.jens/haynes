 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Molto allegro"
  \time 4/4
  \partial 8
  a8
  d fis e g fis8. e16 d8 a
  d e fis g a4 r8 a,
  d fis e g fis8. e16 d8 fis b, e
  d b16 cis16 d4 r
}


\relative c'' {
  \clef treble
  \key d\major	
   \tempo "2. Menuetto"
  \time 3/4
  d2 fis4
  \grace fis16 e4 d r
  e2 g4
  \grace g16 fis4 e a,
}

\relative c'' {
  \clef treble
  \key a\major	
   \tempo "3. Andantino"
  \time 2/4
  e4 e a16 b cis a e4
  e8. cis16 e8 e
  a16 b cis a e4
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "4. Menuetto"
  \time 3/4
  \partial 4
    a'8 g
    fis4 g8 fis e fis
    \grace e16 d4 cis8 b a4
    a' g fis e8 d e fis g e
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "5. Rondeau. Allegro assai"
  \time 2/2
  d4 d \grace d8 cis4 b8 a
  d4 d \grace d8 cis4 b8 a
  d4 e fis g
  \grace b8 a4 g8 fis fis4 e
}