 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
    \override TupletBracket #'stencil = ##f
      c8. c,16 c'8. c,16  c4 r16 c' b a
      g f e d c8. c16 c4 r16 c' b a
}
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio"
  \time 4/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
      b16 d d b  b d d b  c e e c  c e e c
      b d d b
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
        g'16 e c8
        c,8 c c c
        a''16 f c8 c, c c c
        g''16 e c8 c,[ c]
}

