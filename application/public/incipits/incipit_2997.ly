 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio ma non troppo"
  \time 4/4
    c4. a16 g f4~ f16 f a c
    d8 c \grace c8 bes8. a16 a8 g16 f d'4~
    d8 c16 b c4. bes16 a bes8 bes bes a
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 4/4
  c2 d
  c4. a8 bes4 c
  f, r8 c' bes g16 a bes8 bes a f r
}

\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "3. Larghetto"
  \time 3/2
  f8. e16 d8 cis d bes  g fis g2~
  g8 a16 bes a8 g f e   f8. d16 f8 a d e16 f
  b,8. g16 b8 c d16[ b as g]
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allegro assai"
  \time 3/4	
   c8 f \times 2/3 { f8[ e f] } d8 f
   c e16 d e8 g4 e
   c8 g' \times 2/3 { g8[ f g] } c,8 g'
   
}