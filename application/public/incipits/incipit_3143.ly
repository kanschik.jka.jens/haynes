\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Capital und Interessen [BWV 168/3]" 
    }
    \vspace #1.5
}
 
 

}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore 1+2"
  \key fis\minor
   \tempo "o. Bez."
  \time 3/8
    r8 a16 b cis a 
    fis8 fis' e
    d cis16 b e8
    b4 a8  r8
    
    
    }


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key fis\minor
   \tempo "o. Bez."
  \time 3/8
     \set Score.skipBars = ##t
    R4.*24  r8 a8 cis
    fis, fis' e
    d cis16 b cis a
    cis8 b a r8
    
    
    }