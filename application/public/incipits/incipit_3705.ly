 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "Siciliana"
  \time 6/4
  \partial 4
  g4
  d'2.~ d2 e4
  d2.~ d2 g4
  d4. e8 c4  b4. c8 a4
  g2.
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "Aria 1. Allegro"
  \time 4/4
  \partial 8
    d8
    g d e16 d e fis  g8 d c16 b c d
    e8 a, d16 c b a b8 g b d
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "Aria 2. Allegro"
  \time 6/8
  \partial 8
     d8
     d b16 c d8 d d d
     d b16 c d8 d d d
     e16 fis g8 d  c16 d e8 b
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "Aria 3. Vivace"
  \time 2/4
  d8 e4 d8 
  c b4 c8~ c b16 c d8 e~
  e fis16 e fis8 d
}



\relative c'' {
  \clef treble
  \key g\major	
   \tempo "Aria 4. Affettuoso"
  \time 6/4
  \partial 2.
  b8 c d c b4
  g2. c8 d e d c4
  a2.
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "Aria 5. Presto"
  \time 4/4
\partial 4
  b4
  e a, d g, c b2 a8 g
  a b c d e fis g e
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "Aria 6. Tempo di Minuetto"
  \time 3/4
  b4 a8 b c a
  c b a g d'4
  c8 b a g d'4
  b2 a4
}