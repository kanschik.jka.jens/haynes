\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 2/2
     e2 g c,2. b8. c16
     a4 b8. c16 a4 b8. c16
     a8 b c cis  \grace e16 d8 c d e
     c2 a g4 c c
}
