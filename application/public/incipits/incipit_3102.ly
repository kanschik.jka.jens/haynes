\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Ich lasse dich nicht [BWV 197a/6]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key d\major
   \tempo "o. Bez."
   \time 6/8
   \partial 8
   d8 a'8. b16 cis8 d8 r8 d,8
   cis8. d16 b8 a4 a'16 b32 c
   b16 g fis e a fis g e d cis b'a
 
  
  
   
}

\relative c {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key d\major
   \tempo "o. Bez."
   \time 6/8
   \set Score.skipBars = ##t
   R2.*7 r4 r8 r8 r8 d8 a'8. b16 cis8 d r8 d,8 cis8. d16 b8 a4  
   
}



