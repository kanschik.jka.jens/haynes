\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key a\major
    \time 4/4
    \tempo "1. o.Bez."
       \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
    e4. \times 2/3 { d16 cis b}  cis8. b16  a8. gis16
    a4 e b'8. cis16 b8. a16
    gis4
      
}

\relative c'' {
  \clef treble  
  \key a\major
    \time 3/4
    \tempo "2. Arioso"
       \partial 4
       cis8 d
       e4 a e
       e2 d4
       cis8 d e d cis b
       cis4 a
}

\relative c'' {
  \clef treble  
  \key a\major
    \time 2/2
    \tempo "3. Rigadon"
   \partial 4
   a'8 gis
   a4 e a e
   cis2. cis4
   b e, fis gis
   a8 gis a b a4
}

\relative c'' {
  \clef treble  
  \key a\major
    \time 3/4
    \tempo "4. Saraband"
     a4 e4. e8
     fis4. fis8 e4
     a8 b b4. a16 b cis4. b8 a4
}

\relative c'' {
  \clef treble  
  \key a\major
    \time 3/4
    \tempo "5. Air"
    e8 a, a cis b cis16 d
    cis8 e a, cis b cis16 d
    cis8 e a,
}

\relative c'' {
  \clef treble  
  \key a\major
    \time 3/4
    \tempo "6. Menuet"
     a'4 gis fis
     e4. d8 cis d
     e4 e, gis a8 gis a b a4
}


\relative c'' {
  \clef treble  
  \key a\major
    \time 3/8
    \tempo "7. Passepied"
   \partial 8
     a16 b
     cis a cis a d a
     e' a, e' a, fis' a, e' d e fis e d
}
