\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Air [Voix]"
  \time 3/4
  a2 a4 d2 e4
  fis d e 
  fis \grace e8 d4 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Air [Oboe]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*3 
   r4 r cis
   d8 cis b d cis d16 e
   d4 \grace cis8 b4 r8 b
   a4. b8 g a
   fis4. g8 a4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Tutti]"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. [Solo]"
  \time 4/4
   \set Score.skipBars = ##t

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 4/4

}