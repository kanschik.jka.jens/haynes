\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
  c'4 c c4. b16 a g8 c e, g
  c,4. b16 a
  g8 c e, g c,4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio"
  \time 3/4
   g'4 g as as d, g
   g c, f f bes, es es a, d
   d g, c
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/8
   c'8 c, c c16 d e f g8 a b4 c8 c, r
   c'16 b a g f e
   a g f e d c
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
  c4 c d r8 d
  d16 c b a g8 f'
  e c r g'
  a a a a
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio"
  \time 3/4
  c4 e8. c16 g'8. c16
  c4 b8 c b c
  a8. a,16 c8. a16 e'8. a16 
  a4 g8[ a g a]
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro "
  \time 3/8
   c16 c c c c c
   g' g g g g g
   c c c c c c
   c8 b r
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
    c4. e16 f g8 c, r e16 f
    g8 c, c'4
    b8 g r4
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio"
  \time 3/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
   g'8.[ \times 2/3 { f32 es d] } c8. g'16 g8. f32 g
   as2 r4
   f8.[ \times 2/3 { es32 d c] } bes8. f'16 f8. es32 f
   g2 r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/8
   g'8 g g
   g16 c, e c g' c,
   a' g f e d c
   b8. a16 g8
   c16 e g e c g'
   c,16 e g e c g'
}

 
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
    g'4 c,
    b8 c d e d g, f' e16 d
    e8 c16 d e f g e
    a8 d, cis d
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Aria I"
  \time 6/8
 \partial 4.
 e4 d8
 c g a' g4 f8
 e d f e4 d8
 c g f' e16 g f e d c
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Aria II"
  \time 6/8
 \partial 4.
 es4 f8
 g4. f8 es d
 es4 c8  as'4 g8
 f4 es8 d4 c8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 3/4
  c4 e8. f16 g8. a32 b
  c8 c, c c' c b
  a4 g4. f8
  e4. d8 c4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 3/4
  g'8 g, b g d' g, 
  g'4 g2 \grace { fis16[ g] }
  a4 a2 \grace { g16[ a] }
    b4. a8 g4
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 3/4
  r8 r16 g g8. g16 g8. g16
  bes8. bes16 bes8. bes16 bes8. bes16
  d8. d16 d8. d16 d8. d16
  g4 g, g'~ g fis2
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 2/4
   g'16 d b'4 a8
   g16 g, a b   c d e fis g d b'4 a8 
   g16 b a b  g b d, f
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 3/8
  g'8 b,16 c d8
  g, b d
  g b,16 c d8
  g,4 d'8
  e e8. d32 c d8 d8. c32 b
  c8 c8. b32 a
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 3/4
   bes'4 a g
   g fis r
   es d c c bes r
   es8 g c es, es es
   es4 d r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 2/4
   g'4 d
   b16 c d8 g, d'
   g4 a b8 g4 b8~
   b a d, g~
   g fis b, e
}
