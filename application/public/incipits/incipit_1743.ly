\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allemanda Poco Largo"
  \time 2/2
  \partial 8
  e8 e4~ e16 e fis g fis4~ fis16 fis g a
  g8 e r16
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Allegro"
  \time 2/2
  r8 g g g fis g16 a b8 b e,4 e'2 dis4 e8
}




\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Poco Largo"
  \time 3/4
  fis4 d b
  fis'2 cis4
  d8 cis b d cis b ais4 fis2
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Vivace"
  \time 2/2
  \partial 2
  fis4 cis
  d cis b8 cis16 d
  cis8. b16 ais4 fis
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. Allemanda. Vivace"
  \time 2/2
  \partial 8
  b8 b4. cis8 d4. fis16 e
  d8[ cis] b16 d cis b ais8 fis b4~
  b8[ cis16 d] cis8. b16 b4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Largo"
  \time 3/2
  d2 a a'
  fis1 e2 d e2. cis8 d
  cis1 a2
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "5. Poco Allegro"
  \time 6/8
  \partial 8
   fis,8
   b cis b b cis b
   b4. r4 b8
   cis d e d4 cis8
   d cis b
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Vivace"
  \time 2/2
  d8[ e] f e16 d e8[ f] g f16 e
  f8 d a'2 g4 a
}



\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Affettuoso"
  \time 12/8
  d4 a8 d8. e16 fis8 e4 a,8 a'4 cis,8 d8. e16 fis8 
  e4 d8 cis4. r8 r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allemande. Allegro"
  \time 2/2
 \partial 8
 d8 d4 ~ d16 a' g a fis8. e16 d[ fis e d] cis8[ a]
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 3/4
  g8 fis g a g a
  b a b c b c
  d2 g4 fis2 d4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/2
  g8 b b d d a a d
  d g, g g' fis4 r8
}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Largo"
  \time 4/4
  g4~  g16 a bes c bes8 g d'4~ 
  d16 c bes c c8 bes16 c d4 r8 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Allemanda. Vivace"
  \time 2/2
  \partial 8
  d8
  d g16 fis g8[ d] bes g16 a bes8[ c]
  d bes16 c d4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 3/4
  \partial 4
  bes4
  f' f8 g f4
  f \grace es8 d4 f
  g f4. es8
  d4 bes c d2
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Andante"
  \time 12/8
  g8 a bes c d es d4 g,8 g4 d'8
  g a bes a bes g fis4 d8 d4 d8
  es d es  c d es d4 bes8 bes4
}

