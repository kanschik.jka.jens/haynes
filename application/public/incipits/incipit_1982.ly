 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
    g4 d' b8 g r d'
    d d d d  d d d d
    d e16 d c8. b16 a4 r
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Adagio"
  \time 3/4
  r4 b a8 g d'2 r4 r d g8 a
  fis4. e8 d4
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Vivace"
  \time 4/4
    g4 g d' d
    g a fis2
    d4 g8 b, c4 c
    c b a d
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Presto"
  \time 6/8
    g8 b d  fis, a d
    g, b d fis, a d
    g, b d g a fis
}