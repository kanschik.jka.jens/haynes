\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Adagio  a-Moll  2/4"
  \time 2/4
   \partial 8
   a8
   e'2~ e e4 r8 a,8
   f'8. g16 e8. f16
   d8. e16 c d e8~
   e8 d4 dis8
   \grace dis4 e2
}


\relative c''' {
  \clef treble
  \key a\minor
   \tempo "2. Grazioso  a-moll  3/4"
  \time 3/4
  \partial 8
      << {
      % Voice "1"
      g4
         } \\ {
      % Voice "2"
      d4      
      } >>
}
