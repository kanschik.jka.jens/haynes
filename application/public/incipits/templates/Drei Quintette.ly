\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4	
  \partial 4
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. "
  \time 2/2
  \partial 4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. "
  \time 2/2
  \partial 4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. "
  \time 2/2
  \partial 4

}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4	
  \partial 4
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. "
  \time 2/2
  \partial 4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. "
  \time 2/2
  \partial 4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. "
  \time 2/2
  \partial 4

}

\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4	
  \partial 4
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. "
  \time 2/2
  \partial 4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. "
  \time 2/2
  \partial 4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. "
  \time 2/2
  \partial 4

}