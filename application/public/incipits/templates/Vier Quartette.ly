\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. "
  \time 3/8

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. "
  \time 3/4
  
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. "
  \time 2/4

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key bes\major
   \tempo "1. "
  \time 3/8

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. "
  \time 3/4
  
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. "
  \time 2/4

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. "
  \time 3/8

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. "
  \time 3/4
  
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. "
  \time 2/4

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. "
  \time 3/8

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. "
  \time 3/4
  
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. "
  \time 2/4

}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. "
  \time 2/4

}
