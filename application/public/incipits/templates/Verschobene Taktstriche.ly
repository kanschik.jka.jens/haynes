\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key es\major
   \tempo "Adagio"
  \time 4/4
   \set Staff.whichBar = ""
   % hier können jetzt mehr als 4 viertel hinein
   % an [] denken!
  bes2~ bes8[
  g'32 f es d
  c bes as g   f es f g
  as g f as
  g]
  \unset Staff.whichBar
  \bar "|"
  f4
 
}

