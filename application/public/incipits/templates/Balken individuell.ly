\version "2.16.1"

#(set-global-staff-size 14)

\relative c' {
  \clef treble
  \key d\major
   \tempo "1. ohne Satzbezeichnung"
  \time 2/2
  fis'8[ d16 e] fis g fis g fis[ e d e] fis g fis g
  e[ fis e fis] d e d e cis4
  a4 
  \autoBeamOff g8 g8 \autoBeamOn
  
}
