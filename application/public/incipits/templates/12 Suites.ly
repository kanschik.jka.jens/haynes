\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. "
  \time 2/2
  \partial 4
  
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. "
  \time 2/2
  \partial 8

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. "
  \time 4/4
  \partial 4
  
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. "
  \time 2/2

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. "
  \time 3/2
  \partial 4

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. "
  \time 4/4
  \partial 4

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. "
  \time 2/2
  \partial 4

}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. "
  \time 2/2
  \partial 4

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. "
  \time 2/2
  \partial 4

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 2/2
  \partial 4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. "
  \time 2/2
  \partial 4

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. "
  \time 4/4
  \partial 4

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. "
  \time 3/2
  \partial 4

}

