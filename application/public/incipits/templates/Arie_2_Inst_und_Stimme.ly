 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Violin"
                     \set Staff. shortInstrumentName = #"Vn"
                     \key d\major
                      \tempo "3. Allegro"
                      \time 4/4
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob"
                     \key d\major
                        % Voice 2
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key d\major
   \tempo "3. Allegro"
  \time 4/4
     \set Score.skipBars = ##t
     R1*5

}