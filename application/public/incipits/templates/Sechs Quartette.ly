\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Largo"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Andante"
  \time 3/4
  \partial 4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Giga"
  \time 6/8

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Largo"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Corrente. Allegro"
  \time 3/4
  \partial 8

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Largo"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Allegro"
  \time 4/4

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Largo"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Allegro"
  \time 4/4

}
  
  \relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Largo"
  \time 3/4

}
  
  \relative c'' {
  \clef treble
  \key es\major
   \tempo "4. "
  \time 6/8
  \partial 8

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Andante"
  \time 3/4

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Andante"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Andante"
  \time 3/4

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "4. Allegro"
  \time 6/8

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Largo"
  \time 3/2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Andante"
  \time 3/4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Presto"
  \time 2/4

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quartetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 3/4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Largo"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Andante"
  \time 3/8

}

