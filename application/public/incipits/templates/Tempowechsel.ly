\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 12/8
    d4.~ d8. e16 d8 d8. e32 fis g8 d8. e16 c8
    d4.
   \tempo "Allegro"
  \time 4/4
    r8 a a a b 
}
      
