
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
   \times 2/6 {e16[ f g a bes c] }
   \times 2/7 {d16[ e f g a bes c] } d8[ e,8]
