 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\score { 
  << 
    \time 2/2
    \new Staff { 
      \set Staff.instrumentName = #"Oboe"
      \clef "treble"
      \key bes\major
      \new Voice {  
        \relative c'' { 
          r1 r1 r2 bes4. d8
          f2 g16 f8. es16 d8.
        }
      }
    } 
    \new Staff <<  
      \clef "bass"
      \set Staff.instrumentName = #"Bass"
      \key bes\major
      \new Voice {  
        \voiceOne
        \relative c' { 
        r2 d4. es16 c
        bes4 a8 bes \grace a16 g4. a16 f
        bes2 f4. bes8 d2 es16 d8. c16 bes8.
        } 
      }
          \new Staff <<  
      \clef "bass"
      \set Staff.instrumentName = #"Basson"
      \key bes\major
      \new Voice {  
        \voiceOne
        \relative c' { 
        r2 bes4. a8
        g4 f8 g a2
        g2 r es r  
        } 
      } 
      >>  % end of LH staff

      >>  % end of LH staff
  >>  % end of simultaneous staves section
}  % end of single compound music expression





