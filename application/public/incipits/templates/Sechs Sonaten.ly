\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Grave"
  \time 3/2
 \partial 8
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 4/4
 \partial 8
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 4/4
  \partial 8

}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Grave"
  \time 3/2

  \partial 8
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 4/4
  \partial 8
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 4/4
  \partial 8

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Grave"
  \time 3/2
 \partial 8
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 4/4
  \partial 4
 
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 4/4
  \partial 8

}

  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Grave"
  \time 3/2
 \partial 8
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 4/4
 \partial 8
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 4/4
  \partial 8

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Grave"
  \time 3/2
 \partial 8
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 4/4
 \partial 8
 
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 4/4
  \partial 8

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Grave"
  \time 3/2
 \partial 8
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Presto"
  \time 4/4
  \partial 4
 \partial 8
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 4/4
  \partial 8

}
