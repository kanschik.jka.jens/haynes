
\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
  \tempo "1. "
  \time 4/4
 \override TupletNumber #'stencil = ##f
  g'4. \times 2/3 { f16 es d } 
  
}
