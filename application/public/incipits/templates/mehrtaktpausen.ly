
\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Menuetto"
  \time 3/4
  
  
   \set Score.skipBars = ##t
   R2.*4 a4 a a a
  
  
}



