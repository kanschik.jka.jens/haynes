 
\version "2.16.1"
 
#(set-global-staff-size 14)
 

 

 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
  c4. g'16. es32 c4. g'16. c,32
  f8 d16. es32 f8 f f16. d32 es8 r16 es f16. es32
  d4~ d16. d32 es16. d32 c4
}



\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  g'8
  es16 c b c g c b c
  f8 es4 d8
  es16 c b c g
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 3/2
  \partial 2
  bes2
  es4. d8 es4 es16 f g8 f4. es8
  f4. bes,8 bes'4. f8 g4. as8
  g4. f8
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Vivace"
  \time 3/4
    es4 f d
    es8 d es f g4
    c, d b
    c2.
}
