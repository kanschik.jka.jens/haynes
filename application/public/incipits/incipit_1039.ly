\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 3/4
    r4 r bes~
    bes8. d16 f4 r
    r r bes,~
    bes8. es16 g4 r
    r4 r16 es d es f es d es
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro assai"
  \time 4/4
  es2. d4
  c bes as2
  g4 r r2
  }

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Allego"
  \time 3/8
  R4. r8 r es~
  es d es c bes r R4.
  r8 r es~ es d es as, g r
}