
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef "treble"   
  \key es\major
  \tempo "1. Andante"
  \time 4/4
  \partial 8
  bes 8 bes \grace d32 c16. bes32 bes8 as as g r8 g8
 g16 f as g bes as c bes bes8 bes32 as g as g8 g
}
 

\relative c' {
  \clef treble
  \key es\major
   \tempo "2. Allegro assai"
  \time 3/4
 es2. g bes es des bes c d es~ es4 d2 es8 g es2
}
 
 
\relative c'' {
 \clef "treble"   
  \key es\major
   \tempo "3. Minuetto"
  \time 3/4
g8. as16 bes8 bes bes bes
 bes4 c8. bes16 es8. bes16 bes 4 c8. bes16 es8. bes16 bes4 as g
 c2. bes c8 bes as g f g \grace g4 f2.
}
 
