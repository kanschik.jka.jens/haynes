\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 2/2
  \set Score.skipBars = ##t
  R1 r2 g'4 g16 b a g
  fis8 d g b   cis, e16 a~ a[ g fis e]
}
