 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
     bes4 bes bes bes16 c d es
     f8 f f f  f g16 a bes8 bes,
     f' f f f  f g16 a bes8 g
     f16 es d f es d c es d8 bes4
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Adagio"
  \time 2/2
  g'4. g16 fis g4. es8
  es d d16 c bes a bes8 g bes d
  f4. g16 d es4 d
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Allegro"
  \time 3/8
    f8 bes, c d16 es f4
    f16 bes, es8 es
    es d4
    g16 e f8 f
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}