\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 2/2
  c'2 g8. fis32 g c8. g16
  e2. g8. e16
  c4 e8. c16 g'4 e8. c16
  g'4 g,
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   R1*72
     \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

  c,2. \times 8/13 { d32[ e f] g[ a b c] d[ e f g a b]}
  c2 g4 c8. g16
  e2 r4 g8. e16
  c4 e8 c16 g'4 e8. c16 g'4 g, r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante"
  \time 3/8
  f4 e8 f4 g8
  a bes16 a g f e f g c, d e
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Allegretto"
  \time 2/4
  g'4 f16 e d c
  \grace e8 d8 c  \grace e8 d8 c 
  g'4 f16 e d c
  d8. e32 f e8 r
}