 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. o.Bez."
  \time 2/2
    <es, g>4 <d f>2 <f as>
  \grace <g bes>8 <f as>4 <es g> r <es c'>

}
 