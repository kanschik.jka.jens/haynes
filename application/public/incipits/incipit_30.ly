\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [tutti]"
  \time 4/4
  \partial 8
  c8
  f f f f  f16 c a c f8 a
  g16 e c e g8 c a16 f a, c f8 a
  g16 e c e g8 c a f r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [solo]"
  \time 4/4
  \partial 8
  r8
  \set Score.skipBars = ##t
   R1*16
   r2 r4 r8 a'
   a16 f c f a8 g a16 f c f a8 g 
   a bes16 c bes8 a g c, c16 e f g
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio [tutti]"
  \time 12/8
  a'8. g16 a8 bes4 a8 g4. r8 r g
  f8. e16 f8 g4 f8 e4. r4 r8
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Adagio [solo]"
  \time 12/8
  r4 d8 d8. e16 f8 g4 f8 e8. f16 g8
  f4 a8 g4 f8 e8. f16 g8 a4 g8
  f4.
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 3/8
  \partial 8
  c8
  a f16 g a bes c8 f f
  f a a a f c
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  d8
  g16 fis g a b8 a g d'
  d, c
  b g' b, a g g'
  d c
  b[ g']
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Adagio"
  \time 3/4
  g'8 fis g a b4
  a b r
  g8 fis g a b g
  fis4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Allegro"
  \time 3/4
   g'4 b,8 d g4
   a8 d, fis a fis d
   b'4 c b a2 r4
}




\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 8
  g8
  c c, e g c c, e g
  c c,16 d e8 f g g, r g''
  g16 e c e  a bes a g f g a f  d e f d
  
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio"
  \time 3/4
  a'4 c b c a e
  e8 c a c b e
  c a a'4 c, b e b gis b e
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/8
  \partial 8
  g'8
  e c g c e g
  c b16 a g f
  e d c8 g'
  
}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 16
  a16
  d4 a d,8 fis16 a d8 a
  d, fis16 a d8 a d, fis'16 g a b g b a d cis b  a g fis e
  
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Adagio"
  \time 4/4
  b'8 b fis fis g g cis,32[ e d cis] e g fis e
  ais8 ais fis fis fis b  b,32[ d cis b] d fis e d
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 3/8
  a'8 g fis e d16 e fis g
  a8 g fis
  e d16 e fis g
  a8 b cis
  d cis16 b a8
}

