
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Suitte "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Amusement Champetre. Prelude. Tres lentement"
  \time 2/2
\partial 2
g4. f8
e4. f8 d c d e 
c4 g g' c
e,4. d8 e f g a 
g4.


}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxiéme Suitte "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Lentement"
  \time 3/4
e4. f8 g4
d4. f8 e f 
g c, b4. c8
d g g e f d 
e d c4 
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisiéme Suitte "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Lentement. Gracieusement"
  \time 2/2
\partial 2
c8 d e f 
g4 d8 e f e d c 
b4 \grace a8 g4



}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatriéme Suitte. L'Ile de Beauté "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Légérement sans vitesse"
  \time 2/2
\partial 2
 e4. f8 
g4 c g a 
e d8 e f e d c 
b4 \grace a8 g4

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Ciquiéme Suitte. Le Presidente"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gracieusement"
  \time 2/2
\partial 2
e4. d8
c4 g' e4. d8
e4 \grace d8 c4 e8 d e f 
d4. c8 d4 b c g
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    " Sixieme Suitte. Litaliene"
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
c16 g e g c, e d e c f e d64 e f d e16 c d b
c g' e c g' e c e 

 
}

