 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  g'8 d g16. a32 b16. c32 d8 d, e16. fis32 g16. b,32
  c8 e'16. d32 c b a g fis e d c \grace c8 b16. a32 g8 r4
  
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Allegro"
  \time 2/4
    g'8 d~ d e16 fis
    g b a g d'8 d,
    e d
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Adagio"
  \time 3/4
  b'4 g8. b16 e8. b16
  c4 c,4. c8
  c b e e e e 
  e
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Presto"
  \time 3/8
  g'16 fis g8 d
  a'16 g a8 d,
  b' a g c b4 b8
}