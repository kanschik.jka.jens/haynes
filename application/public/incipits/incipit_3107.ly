\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt und Tenor): Sein Allmacht zu ergründen [BWV 128/4]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key b\minor
   \tempo "Duett"
   \time 6/8
   \partial 8
  fis8 b cis d b b16 ais b8
  e,4. d4 fis'8 d cis b fis16 gis ais b cis d e4~e16 fis32 g fis4 
   
   
 } 
 


\relative c'' {
<<
\new Staff { \clef "treble"   
  \set Staff.instrumentName = #"Alt"
  \key b\minor
   % Voice 1
   \tempo "Duett"
   \time 6/8
   \partial 8
   \set Score.skipBars = ##t
   r8 R2.*13 r4 r8 r8 r8 g8 b cis d cis b16 ais b8 e4. d4
                     
                  }
   \new Staff { \clef "treble"
   \set Staff.instrumentName = #"Tenor"
   \key b\minor
   % Voice 2
   \tempo "Duett"
   \time 6/8
   \partial 8
   \set Score.skipBars = ##t
   r8 R2.*15 r4 r8 r8 r8 b8 fis' gis a gis fis16 eis fis8 
   b,4. a4
                       
                  }
>>
}


 

