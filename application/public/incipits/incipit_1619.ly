 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major	
   \tempo "Andante"
  \time 3/8
    f,16. g64 a f8 r
    g16. a64 bes g8 r
    a16. bes64 c bes8 a
    g16. a64 bes a8 r
}
