 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "o.Bez."
  \time 12/8
  b8  c16 b a g  a4 d8 \grace c16 b8 c16 b a g
  \grace g8 a4 d8~
  d g, c~ c b d~ d g,
}

