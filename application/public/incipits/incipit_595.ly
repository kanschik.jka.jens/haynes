\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Andante"
  \time 3/4
  \partial 8
  d8
  d4 fis g
  a fis a
  b a4. g8 fis4 \grace e8 d4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 4/4
  d16 e fis g a8 a, d4 cis
  d16 e fis g  e fis g a  fis e fis d   g fis g e
  a16 fis8. g16 e8. fis16 d8. e16 cis8.
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Arietta"
  \time 6/8
  f16 e d8 a a4.
  g'16 f e8 a, a4.
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Gavotta"
  \time 2/4
  \partial 8
  a8
  d fis16 e d8 fis 
  e a, a e'
  fis a16 g fis e d fis
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key d\minor
   \tempo "1. Largo"
  \time 4/4
  a8 d,16. f32 a16 g f g32 a bes4 r
  g8 c,16. e32 g16 f e f32 g a4 r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Allegro"
  \time 3/4
  \partial 8
  r16 d16
  d4 r r
  bes' r r
  a g8 f e d cis e4 d8 cis4
  
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3.Arietta "
  \time 12/8
  f16 g a8 d, d4 d8 bes'4 a8 g4 f8
  e16 f g8 c, c4 c8
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Gigue"
  \time 6/8
  \partial 8
  d8
  a'4. r4 r8
  bes4. r4 r8
  a8 g f e f d
  cis a d f, e cis'
  d4.
}
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Adagio"
  \time 4/4
  \partial 8
  r16 e
  e4 \grace fis8 fis4 \grace { e16[ fis] } g8 b a16 g fis e
  dis8 \grace c8 b8
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Corrente all."
  \time 3/4
  \partial 8
  e8
  e b' a g fis e
  dis cis b a g fis
  g b e g, fis dis' e,4.
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Sarabande"
  \time 3/4
  e,4 b'8 c a b
  g4 e b'
  e fis8 g e fis dis2 e4 
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Gavotta Allegro"
  \time 2/4
  \partial 8
  b'8
  b e, a c
  b4 r8
  e,
  a c fis, a
  g
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "1. Largo"
  \time 4/4
  r8 b gis' fis16 e cis'8 cis cis dis16 e
  a,8 b16 cis
  dis, fis b a gis4 r8
}

\relative c'' {
  \clef treble
  \key e\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  b8
  e16 fis gis a b8 a
  gis fis16 e cis'8 cis
  dis,4 \grace fis8 e4
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Siciliana"
  \time 12/8
  \partial 8
  b8
  e8. fis16 g8 fis16 g a b c e, dis8. e16 fis8
  a, dis fis
  g,4 e'8 fis, a dis
  e,4.
}


\relative c'' {
  \clef treble
  \key e\major
   \tempo "4. Gigue"
  \time 6/8
  \partial 8
  b8
  e b e e4 b8
  fis' b, fis'
  fis4 b,8
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "1. Largo"
  \time 4/4
  b8 g'16 fis e d cis e d8 \grace cis8 b r8
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Allegro"
  \time 3/4
  b4. b16 cis d8 cis16 b
  cis8 d16 e fis8 g fis e
  d[ b]
}


\relative c'' {
  \clef treble
  \key b\minor
   \tempo "3. "
  \time 3/4
  b8. b16 b8. b16 b4
  \grace {cis16[ d]} e2 r4
  d8. d16 d8. d16 d4
  \grace {e16[ fis]} g2 r4
  
}


\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Gigue"
  \time 6/8
  \partial 8 b8 
  fis' b, b  g' b, b
  fis' b, b  g' b, b
}
\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Largo"
  \time 4/4
  \partial 8
  d8
  a'4 r16 d, fis d b'8 d,4 cis8
  d16. e32 fis16 g32 a b8.
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 8
  a d16 cis d8 r e
  fis16 e fis8
  r
  g
  a16 g a8 r
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Arietta"
  \time 6/8
  \partial 8 d8
  a'4 g8 fis16 g a8 bes
  a4 g8 fis4 e
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Minuetto"
  \time 3/4
  d,4 fis8 a d cis
  d2 e4 fis16 e fis g a4 g fis2 e4
}
