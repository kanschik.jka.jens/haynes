\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  \partial 8
  a8
  d16 cis d fis d a fis a d, a' fis a d, a' fis a
  d16 cis d fis d a fis a d, a' fis a d a e' a,
  fis'8 e g fis e d a'4
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \partial 8
  r8
   \set Score.skipBars = ##t
   R1*7
   r2 r4 r8 a
   d,16 e fis8 fis16 g a8 a16 b cis8 d d32 e fis16
   e8 d cis b a16 gis a gis a e' fis d
  
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Siciliano [Tutti]"
  \time 12/8
  d8. cis16 b8 b8. cis16 d8
  d,4. e'4.~
  e8. fis16 g8 g8. fis16 e8 d4 cis8 b4.
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Siciliano [Solo]"
  \time 12/8
   \set Score.skipBars = ##t
   R1.*6
   fis,8. g16 fis8 fis8. d'16 cis8 b8. ais16 b8 b4.~
   b16 g' fis cis d b e,8. g16 fis8 d8. cis16 b8
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro [tutti]"
  \time 3/8
  r8 d, fis a d,16 e fis g
  a8 d fis a g16 fis e d
  cis a e'8 es
}


\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro [solo]"
  \time 3/8
  \set Score.skipBars = ##t
  R4.*18
  a16 b cis d e fis
  g, fis g8 e'~
  e16 d cis b a g
  fis e d e fis g
}