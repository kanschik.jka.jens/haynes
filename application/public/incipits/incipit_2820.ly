
\version "2.16.1"

#(set-global-staff-size 14)

\relative c' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4
 f4. g8 bes a c a 
 bes8. a32 bes d8 bes f4 r4
 g4. a8 c bes d f 
 es8. d32 es g8 es c4 r4

}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio"
  \time 2/4
 g4 as8. g32 as
 bes8. c16 bes8 bes16. bes32
 bes16 as as16. [as32] as16 g g16. [g32] f8 es r4
  

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegretto"
  \time 2/4
 f8 f f8. es16
 d8 d \grace {c16 [d]} es8 d16 c
 bes f d f bes d f d 
 c es g, c \grace bes8 a4
   

}

