\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. [Tutti]"
  \time 4/4
  d4 d,8 d' d4 r8 d
  bes4 a8 cis d4 e
  f8 e d4 cis4 r8
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*2 r2 r4 r8 a
   f'8 f f f  f8. e32 f g4~
   g8 e a16 g f e f e d c b8. a16 a4
}
