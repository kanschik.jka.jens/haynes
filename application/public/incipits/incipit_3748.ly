\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Largo"
  \time 6/8
  d,8. e16 f8 e d r
  e8. f16 g8 c, f
}
      
\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Vivace"
  \time 4/4
  r8 d cis d f4 e
  d r8 cis d a d cis
  d c b c e4 d 
  c
}
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Andante"
  \time 4/4
      << {
      % Voice "1"
      r16 e e e     e e e e    a, f' f f    f f f f
      gis, b b b     b b b b    c, a' a a     b, gis' gis gis
      a,
         } \\ {
      % Voice "2"
      c8 b a c c d d d
      b e e gis, a a f e
      e
  } >>
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "4. Allegro"
  \time 2/4
  r4 f8 e16 d
  e8 a, d4
  cis b'8 a16 g
  a8 d, g f16 e f8
}


