\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  \partial 4
  g4
  c c,8. c16 c8. d16 e8. f16
  g4 g8. g16 g8. a16 b8. g16
  c4 c8. c16 c8.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   \partial 4
   r4
   R1*61
  r2 r4 g'16 f e d
  c4 c c c
  e4. f8 g4 r8 g
  a g f g a b c a
  fis g g4 r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio [Tutti]"
  \time 4/4
  \partial 8
  a16 c e8 e e c16 a b d f8 f e16 d
  c a e'8 d c b16 gis e8

}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   \partial 8
   r8
   R1*3
   r2 r4 r8 a16 c e8. f16 e c' b a \grace a8 gis4. a8 
   b16 g e8 

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo"
  \time 6/8
       c4 e8 g e c
       a' f d d e f
       g e c \grace d16 c8 b c
       e d c d b g
}