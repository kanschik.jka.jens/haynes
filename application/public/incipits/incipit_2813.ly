 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Moderato"
  \time 2/4
  \partial 8
  bes8
  f'8. g16 f es d c
  d8. es16 d c bes a
  bes c32 d es[ f g a] bes c d c bes[ a g f] f8[ es d] r16
}


\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Cantabile"
  \time 2/2
  \partial 4
  c4
  f1 g \grace bes8 a4 g8 f f4 f
  \grace es8 d4 bes'8 g f4 es
  c'8 a g f f4 f
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Menuetto I."
  \time 3/4
  \partial 4
  f4
  bes2 f8 d g2 es8 c
  f4 d8 bes c a bes4 r r
}



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key bes\major
                      \tempo "4. Allegro"
                      \time 4/4
                      % Voice 1
                        \partial 4
    f4 bes8 bes, d bes f bes es, bes'
    d,4 r  r2
    r8 f a f c f bes, f'
                  }
\new Staff { \clef "treble" 
                     \key bes\major
                        % Voice 2
                        r4
                        bes'4 r r2
                        r8 d, es f f es es d es4 r r2
                  }
>>
}
