\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran): Liebster Jesu, mein Verlangen [BWV 32/1]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key e\minor
   \tempo "Adagio"
   \time 4/4
  r4 e4~e8 fis32 e dis e fis8. b,16
  a16 g fis e c' b b c c b b cis
  d cis cis d
 
   
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key e\minor
   \tempo "Adagio"
   \time 4/4
   \set Score.skipBars = ##t
  R1*8 r4 e4~e8 fis32 e dis e fis8. b,16
  g8 fis16 e r4 r2
  
}



