\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key c\major
    \time 2/2
    \tempo "1. ?"
        c,8 e e g g16 f e8 g16 f e d
        c8 e g c b g r4
        c,8 e e g g16 f e8 g16 f e d
        c8 e g c b g r4
        
}
