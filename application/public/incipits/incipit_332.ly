 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro spirituoso [Tutti]"
  \time 4/4
    bes4 bes8. bes16 bes4 bes
    bes4. d16 c bes8 f d f
    bes4. d16 c bes8 f d f
}

 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro spirituoso [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*54 
   r2 r4 bes8 d
   f4. d8 f4 f
   f2~ f8 g16 f es d c bes
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Andante"
  \time 3/4
  g'4 g g
  g4. as8 bes4
  es, as g
  g8 f es d c bes

}

\relative c'' {
  \clef treble
  \key es\major	
   \tempo "3. Rondo"
  \time 2/4
  \partial 8
  bes16 c
  d8. es16 f8 g
  c, d \grace f8 es4
  d8 bes g c
  \grace bes8 a8. g16 f8
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}