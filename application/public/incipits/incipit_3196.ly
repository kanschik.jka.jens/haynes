\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor + Bass): Ich lasse dich nicht, du segnest mich denn [BWV 157/1]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Traverso"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key b\minor
                      \tempo "Duett"
                      \time 4/4
                      % Voice 1
                    r8 d16 e32 fis e16 d cis b fis'2~ 
                    fis8 b, cis d16 e ais,8 fis ais cis
                 
           
                     
                  }
                  
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key b\minor
                     \set Score.skipBars = ##t
                     % Voice 2
                        r2 r8 cis16 d32 e d16 cis b ais
                        b4 r8 g'8~ g16 fis e d cis d e cis
                       
                
                  }
                  
                  
                                    
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Violine "
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key b\minor
                     \set Score.skipBars = ##t
                     % Voice 3
                        fis,2~ fis8 cis'16 b b8 e,16 d 
                        d e fis g32 a g16 fis e d cis2 
                
                  }
>>
}


\relative c'' {
	<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Tenor"
                     \key b\minor
                      \tempo "Duett"
                      \time 4/4
                      % Voice 1
                  \set Score.skipBars = ##t
                   R1 *8 r2 cis4 fis~ fis8 e16 dis e8 fis g r8 r8 
                      
                     
                  }
\new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Bass"
                     \key b\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                R1*8 fis,,4 b~ b8 ais b cis
                d r8 r8 
                        
                
                  }
>>
}

	
	
