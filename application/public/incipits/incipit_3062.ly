\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'Fl√∂sst, mein Heiland, fl√∂sst dein Namen' [BWV 248iv/4]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key c\major
   \tempo "o. Bez."
   \time 6/8
   c4.~c4 g8
   a f g a f g
   c4.~ c8 e d
   e c d e c d
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Sopran"
  \key c\major
   \tempo "o. Bez."
   \time 6/8
   \set Score.skipBars = ##t
    R2.*18 c4.~c4 g8
    a f g r4 r8
    c4.~c8 e d
    e c d r4 r8
    
}



