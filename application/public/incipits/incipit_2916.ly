 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef bass
  \key g\minor
   \tempo "1. Adagio [Basso]"
  \time 4/4
    g,8 g16 a bes8 g  fis d16 e fis8 d
    g g,16 a bes8 g es' c d d,
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio [Oboe]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*2 
     d1~ d2 c8 es d8. c16
     bes a g8 r g a4 r8 a
     bes d g2 fis8. g16
     g4~ g16 a bes g a8 d, r4
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Rondeau"
  \time 3/8
     d16 c bes8 a
     g es' d
     bes' a g fis16 g a8 d,
     g fis8. e32 fis
     g16 es d c bes a
     bes d c bes a g
     fis4.
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Sarabande"
  \time 3/4
     g8 a bes4. g16 a
     bes8 d fis,2
     g8 a a4. g16 a
     bes8 d16 g b,2
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "4. Gigue. Presto"
  \time 6/8
    g8 a fis g bes a
    bes c a bes d c
    d es c d g fis
    g a fis g bes a
    bes4 bes8 bes a g
    fis g a d,4.
}
