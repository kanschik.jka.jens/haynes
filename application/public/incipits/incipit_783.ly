\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio [Tutti]"
  \time 2/4
  \partial 16
  c,16
  f16 r32 c a'16 r32 f c'8 r16 f,
  e32 d cis d d8~ d16 bes'32 a bes[ g f e]
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio [Solo]"
  \time 2/4
  \partial 16
  r16
   \set Score.skipBars = ##t
   R2*15 
   r4 r8 r16 c
   f2~ f f
   \grace { g16[ a]}
   g32 f e f e[ f g f]
   g a bes a bes[ g e bes]
   \grace bes16 a16 g32 f f8 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/4
  \partial 4
  c'8 bes
  bes a a g
  g f f e
  d bes'16 a g8 f \grace f8 e c
}


