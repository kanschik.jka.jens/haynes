 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
    r8 g16. b32 d8 g,16. b32 g'8 g,16. b32
    d16 c32 b  a16 b32 g
    \grace g8 fis4 r r8 fis
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Vivace"
  \time 2/4
  \partial 8
  d8
  b g' fis e d4 c
  b16 a b c b8 b
  g e' d c
}

\relative c'' {
  \clef treble
  \key e\minor	
   \tempo "3. Adagio"
  \time 3/2
  r2 fis g~
  g4 cis, e g fis2~
  fis4 b, d fis e2
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "4. Tempo di minuetto"
  \time 3/4
    d4 b8 c  c8. b32 c d4 g fis e d c b
}