\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 3/8
  \partial 8
  c32 b a g
  f8 f  c'32[ b a g]
  f8 f  c'32[ b a g]
  f8 a' a 
  a g r
}
