\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. "
  \time 4/4
  \partial 4
  f8 d'
  d4. c16 bes a4 bes
  b2 c4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. "
  \time 2/4
 
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. "
  \time 2/4

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. "
  \time 4/4
  \partial 4
  d,8. d16
  g8. g16 a8. a16 b8. b16 c8. c16
  d1~ d
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. "
  \time 3/4

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. "
  \time 3/4

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. "
  \time 2/4
  \partial 8

}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto III " 
    }
    \vspace #1.5
}

\relative c' {
  \clef bass
  \key es\major
   \tempo "1. "
  \time 3/4
  es,8 r bes r r4
  es16 d es d  es d es d  es f es d
  c8 r g r r4
  c16 b c b c b c b c des c bes
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. "
  \time 3/4
 
}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "3. "
  \time 6/8
  \partial 8
  
}
  
\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 4/4
  c2. e4 d8 c c4. c8 d e
  g f f2 e4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. "
  \time 3/4

}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. "
  \time 12/8

}



\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. "
  \time 6/8
   \partial 8
   c8 c4 bes8 a4 g8
   f4. g4 g8
   \grace bes8 a g f bes4 a8 g g16 a g a g8
}
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. "
  \time 3/4

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. "
  \time 3/4

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. "
  \time 4/4
 
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sestetto VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. "
  \time 2/2
  d,2 d4 d
  a'4. \times 2/3 { g16[ f e] } f4. \times 2/3 { e16[ d cis] } 
  d4

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. "
  \time 2/4
  \partial 8

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. "
  \time 2/4
  \partial 4

}
