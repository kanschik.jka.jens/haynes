\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio e staccato"
  \time 2/2
  d8 d d cis16 b a[ g a8] r16 d cis d
  e[ d e8~] e16 e fis g  g[ fis e fis] 
}

  
\relative c'' {
  \clef treble
  \key d\major
   \tempo "2. Allegro"
  \time 2/2
  \partial 8
  a'8
  b d, d b'  a16[ g fis e] d8 a'
  g[ fis16 e] fis8 e16 d  e[ d cis b] a8 a
  d4
}

 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Adagio"
  \time 3/4
  fis8. d16 d8. b16 b8. g'16
  ais,8 cis b4 r8 fis'
  g8. fis16 e8. d16 cis8. b16
  ais4 fis r
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "4. Menuett"
  \time 3/4
  a'8 fis g e fis d
  e4 a, a
  d cis8 b a g
  fis4 e8 fis
  d4
}
