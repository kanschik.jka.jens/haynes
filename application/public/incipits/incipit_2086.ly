 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 2/4
  \partial 8.
  g'16 fis g
  d8 d d es 
  es d r c
  a'16 g fis e d c bes a
  bes a g r
}


\relative c'' {
  \clef treble
  \key d\minor	
   \tempo "2. Largho"
  \time 3/4
  d4 a f'
  bes,2. a2 a'4
  g f e
  f a
}

\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "3. Allegro"
  \time 3/8
  d16 g bes8 a
  g d c bes a g
  d d'16 e fis g a8 bes bes
  a
}
