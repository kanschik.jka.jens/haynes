\version "2.16.1"
         #(set-global-staff-size 14)
         
         \relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro spirituoso"
  \time 4/4
  d,2. fis4
  d cis d dis e2. fis8 g
  a4 a a b8 cis d2. fis4
}