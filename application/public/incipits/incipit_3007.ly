 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe/Chalumeau"
                     \set Staff. shortInstrumentName = #""
                     \key g\minor
                      \tempo "3. Aria"
                      \time 3/4
                      % Voice 1
                      R2. r4 r8 a d c
                      bes8. a16 g4 r
                      r g c a2.~ a4 r r
                  }
\new Staff { \clef "alto" 
                     \set Staff.instrumentName = #"Viola"
                     \set Staff. shortInstrumentName = #""
                     \key g\minor
                        % Voice 2
                        r4 d, g g8 fis fis4 r
                        r d g
                        es2.~ es8 g f es d c d2 r4
                  }
>>
}


\relative c'' {
  \clef alto
  \set Staff.instrumentName = #"Alto"
  \key g\minor
   
  \time 3/4
     \set Score.skipBars = ##t
     R2.*13
                        r4 d, g g8 fis fis4 r
                        r d g
                        es2.~ es8 g f es d c d2 r4
     

}