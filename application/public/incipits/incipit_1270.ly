 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key e\minor
   \tempo "o.Bez."
  \time 4/4
  g'4. fis16 e
  dis4 r32 b cis dis e fis g32. fis128 g
  a4. b8 g4 r16 g a b
  c4. c8 c b16 a g8. fis16
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Almira"
  \key e\minor
   \tempo ""
  \time 4/4
     \set Score.skipBars = ##t
     R1*5
     r2 r4 r8 e,
     e'4. d16 c c4 b
     r r8 b a'4 fis8 e dis4 dis r
}