\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef alto
  \key d\major
   \tempo "o.Bez."
  \time 2/2
    a4 d, r2
    a'8 g16 a b8 g a4 d,8 r16 a'
}
