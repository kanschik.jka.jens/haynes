\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 3/4	
    \grace { d,16 g}  b2 c8 d
    e8. fis16 g8 r r d
    c2 b8 a gis4 a r8
}
