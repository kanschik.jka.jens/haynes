\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 4/4 
    bes2 d f2. es16 d c bes
    bes8 es es2 d4
    \grace d8 c4 bes r
}


