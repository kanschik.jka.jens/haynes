\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 4/4
  bes2 d4. bes8
  f'2. f,4
  g16 fis g fis  g fis g fis g4 r
}

