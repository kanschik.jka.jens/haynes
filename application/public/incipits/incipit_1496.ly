 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef alto
  \key c\major
   \tempo "1. Larghetto alla Siciliana"
  \time 6/8
  e,8. f16 e8 e8. f16 e8 
  e8 d r r4 c8
  c4 f8 f8. e16 d8 d c r
}


\relative c'' {
  \clef alto
  \key c\major	
   \tempo "2. Allegro"
  \time 2/2
  \partial 4
  g,4
  c2 c c4 b r g
  d'2 d d4 c
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Vivace"
  \time 3/4
   c4 c2
   \times 2/3 { b8[ c d] } d2
   e4 e4. f8
   \grace e4 d2.
}
