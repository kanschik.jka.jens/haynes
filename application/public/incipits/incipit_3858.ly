\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Largo"
  \time 4/4	
  d4. b8 g4 r8 g'16 fis 
  e8 d c4 b r8 b16 c 
  d8 e16 fis g8 g g4. fis16 e
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 2/2	
  g8 b16 c d8[ e] e[d]~ d16 e32 fis g16 b,
  c8[ c] a' c, c[ b] r e
  e16[ cis d8] g16 e fis8
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Largo"
  \time 3/4		
  e4 b' e,
  e dis8 b dis fis
  a c b a g fis
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 2/2
  d8 c b a g4 g a g2 d'4
  e g c,2
}

