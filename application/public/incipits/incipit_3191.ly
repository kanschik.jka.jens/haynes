\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Jesu, beuge doch mein Herze [BWV 47/4]" 
    }
    \vspace #1.5

}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Violine"
  \key es\major
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    bes16 as g f es4 f8 es d es
    g es d es as8. bes32 c bes4~ bes8
                   
   
    


}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key es\major
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    R1*4 f16 es d c bes4 c8 bes a bes
    d bes a bes es8. f32 g f4~ f8
                   
   
    
    
    
}


\relative c' {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key es\major
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    R1*12 bes16 as g f es4 f8 es d es
    g es d es as8. bes32 c bes4~ bes r4
                   
   
    
    
    }