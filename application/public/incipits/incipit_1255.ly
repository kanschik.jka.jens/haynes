 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'Morirò ma vendicata'" 
    }
    \vspace #1.5
} 

 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tutti"
  \key g\minor
   \tempo "Presto"
  \time 3/4
    g,4 g g
    g4. bes8 d g
    a,4 a a a4. c8 fis a
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Medea"
  \key g\minor
   \tempo "Adagio"
  \time 3/4
     \set Score.skipBars = ##t
     R2.*19
     r4 d2~ d~ d8 bes g2.
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key g\minor
   \tempo "Adagio"
  \time 3/4
     \set Score.skipBars = ##t
     R2.*21
     r4 d2~ d~ d8 bes g2.
}
