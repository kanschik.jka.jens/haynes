\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro spirituoso"
  \time 4/4
    d'4 g, r e
    f e r b
    c b c d
    g,4. a16 b c d e f
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Romance. Andante"
  \time 2/2
   \partial 16
   c16
   a'4. g8 e4 r8. c16
   bes'4. c8 a4 r8. f16
   d4 d g g
   c,2 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Allegretto"
  \time 6/8
  \partial 4.
      r16 c, e g c d
      e4.~ e8 f d
      c4. b4 r8
      c c' c c b a
      a4. g4 r8
}
