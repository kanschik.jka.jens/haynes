\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro assai [Tutti]"
  \time 2/2
      c2 g e2. f4 g2 a4 b
      c2. e8 d d1~
      d4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro assai [Tutti]"
  \time 2/2
   \set Score.skipBars = ##t
   R1*63
    r2 r4 g
    c2 e g2. \grace g16 f8. e16
    e2~ e8[ f16 e] \grace g16 f8 e16 d
    \grace e16 d8 c c4.
}






\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante con Variation [Tutti]"
  \time 3/4
      d,8.. c32 b8 b' b b
      b4 a8 g fis g
      g16 fis e' d  e d cis d  g d c b
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Andante con Variation [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
        R2.*4
        d4. g8 d b
        b4. e16 d  c b a g
        g4. c8 c c
        \grace d8 c4 b r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro molto"
  \time 2/4
     g'8 e e e
     e4. g8
     g f d d
     d4. f8
     e4 g16 e g e
     c4 e16 c e c
     \grace e8  d8 c d e
     d4 c
}