\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro "
  \time 4/4
  d16 e d e   d e d e  fis g fis g  fis g fis g
  a8 d, d a' b a g8. fis16 fis4 r8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata II " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro "
  \time 4/4
  \partial 8
  a8
  e' e e fis e cis16 d e8 fis
  e cis16 d e8 fis e16 d cis b a8 a
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata III " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro "
  \time 4/4
  g'4 d b16 c b c d8 d
  e e e d16 c d c b a g4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IV " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Andante"
  \time 4/4
  e4 \grace fis16 fis4 g \grace a16 a4
  b8 b, e fis16 e dis8 b r
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  d4.. d16 fis8. e16 fis8. d16
  a'4 a,8. a'16 a8. e16 a8. g16
  fis8. e16 d8.
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. Andante"
  \time 3/2
  d1 a2 d e4 d cis2 d1 a2
}
\pageBreak
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 4/4
  a'4 gis a8 gis16 fis e8 e
  a4 gis a8 gis16 fis e8 e
  fis8 d b e cis b16 cis a8
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata VIII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 4/4
  g'8 g, g' g, fis' e16 fis d4
  e8 e e d16 c d c b a g4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata IX " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 4/4
  d8 a d16 e fis g a b a b a b a b
  a g fis e d a' d cis b a g fis e fis g a
  fis
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata X " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
  r8 g bes g d'4~ d16 es d c
  bes8 g g'16 fis g a fis8 d
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 3/4
    r8 g c c c c
    c g e' e e e 
    e c g' g g g
    g c, c' c c c
    c4 g e
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sonata XII " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Adagio "
  \time 3/2
  c1 f,2 d'1 c2 d4 c bes2. a4
  a1.
}

