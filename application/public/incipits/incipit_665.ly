
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto I."
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
d4 d a' r8 b8
a[ g fis e] 
fis[ d fis g] 
a b16 a g8 fis
e8 a16 g fis e d cis
b8 g'16 fis e d cis b a8 

 
}



\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Largo"
  \time 3/4
f4 e8 d cis e
d4 a bes
g4. f8 g a
f4. e8 f g 
e4

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 3/8
d8 d d
a b16 a b cis
d cis d e fis8
g,8 e'16 d cis b
a8 b16 cis d8
fis,8 g16 fis g a 
b8 g a d,4.
 
}
 

 

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 4/4
a8 a a a a b16 cis d e fis gis 
a8 b b a16 b cis4 r8 e,8
fis a d, fis e a cis, e
d fis b, e cis e b e


 
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Adagio"
  \time 3/4
e4 d8 cis b a 
fis'2 e4
r4 e4 fis
d4. cis8 d e
cis4. b8 cis d
b4 e8 d e4~ e d8 cis d4~ d

}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Allegro"
  \time 3/8
e8 e e 
e a4
gis8 a16 gis a8
d,4 cis8
b16 cis d8 cis
b16 cis d8 cis

 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto III."
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
c4 g'
f8 e16 f d e f g
e8 d16 c
d8 g
e d16 c d8 g
g8 a16 g f8 e 
d4.


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio"
  \time 3/4
c4 e4. f8
g4 c, a'
g f4. e8
d2 g,4
g a8 g a b
g4 c8 b c4


}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "3. Gavotta"
  \time 2/2
\partial 2
g4 g
e d8 e f e d c
d4 g, c d
e f8 g f g e f
d2

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 3/8
\partial 8.
g16 a b
c8 c c 
d b g
d' d d16. c64 d
e16 d c d e f
g8 a16 g f e
d4.


}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV."
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Allegro"
  \time 2/4
g8[ g bes8. c16]
d8[ d d g]
fis8. e16 d c bes a 
bes8[ g d' g]
fis8. e16 d c bes a 
bes8 g



}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 4/4
bes8 bes bes bes  c4 r4
bes8 bes bes bes bes4 r4
c8 c c c c4 r4

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 3/4
g4 g g
g bes8 a g fis
g4 g,8 a bes d
d4 c d
bes d d 
es g8 f es d
c4  c c

 

}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Adagio. Fuga. Allegro"
  \time 4/4
d8 d d d d4 r4
cis8 cis cis cis d4 r4
d2 e
fis8 d d fis16 gis a8 e a4~ a gis

 
}

\relative c'' {
  \clef treble
  \key b\minor
   \tempo "2. Adagio"
  \time 3/2
r2 b4. d8 cis4. e8
d4. cis8 d4. fis8 e4. g8
fis4. e8 fis4. a8 g4. b8
a4

 
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "3. Presto"
  \time 2/4
a4 a
a fis8 g
a[ a a b]
 a[ a a b]
a4 fis8 g
a4 fis8 g
 a[ a g fis]
e2
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto VI."
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Allegro "
  \time 4/4
\partial 8
e8 b' b, b b' g fis16 g e fis g a 
b8 b, b b' g fis16 g e8 b
c b b b c b b b 

 
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Largo"
  \time 3/4
d8 c b a g a
fis4 d g
a4. g8 a4
b \grace a8 g4 d'
e8 d e fis g e
fis4 \grace e8 d4


}

\relative c' {
  \clef treble
  \key e\minor
   \tempo "3. Allegro"
  \time 3/8
e8 g b
e g16 fis g8
dis, fis b
fis' a16 g a8
e, g b g' b r8

 
}

