\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key f\major
                      \tempo "1. Allegro"
                      \time 4/4
                      % Voice 1
                      R1 r4 c'8 c c c c c
                      \grace { d32[ c b]} c4 r r2
                  }
\new Staff { \clef "treble" 
                     \key f\major
                        % Voice 2
                        f,4. e8 f e f e
                        f4 r r2
                        g4. a8 g a g a g4 r4 r2
                  }
>>
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Menuetto"
  \time 3/4
  \partial 4
  f8. f16
  f2 a8. a16 a2 c8. c16
  c4 c c
  c2 r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Adagio"
  \time 4/4
  e2 d4 c
  c2 b4 r
  f'2 e4 d
  d2 c4 r
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Polacca"
  \time 3/4
  \grace f8 f'4 f16 e d c c8 c
  c4 d16 c bes a a8 a
  g8 g4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. Thema con Variatzioni. Andante"
  \time 2/4
  \partial 8
  c8
  f e f e
  f8. a16 c8 c16. bes32
  g8 bes16. a32 f8 a16. g32
  e16 g f  d c8
}

