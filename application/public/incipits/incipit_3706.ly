\version "2.16.1"

#(set-global-staff-size 14)


      
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "Adagio"
  \time 3/2
  r2 c es
  d f1~
  f2 es4 d es f 
  g2 f4 es d c b1 r2
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "Aria 1. Presto"
  \time 4/4
  \partial 2
  g'4 as
  g4 f bes2
  es,4 d8 es f4 es
  d c b c8 d
  es4 c
}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "Aria 2. Vivace"
  \time 4/4
  g'4 f16 es d c b8 c16 d c b a g
  c8
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "Aria 3. Vivace"
  \time 6/4
  \partial 2.
  es4. f8 es4
  es f8 es d c d4 d8 es f4
  d2.
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "Aria 4. Allegro"
  \time 3/8
  \partial 8
    c16 d
    es8 d es
    f4 es16 f
    g8 f es16 d
    es[ d c b c d]
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "Aria 5. Allegro"
  \time 2/4
  \partial 8
  g8
  c g c16 d es c 
  d8 g, r g
  d' g, d'16 es f d
  es8 c
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "Aria 6. Presto"
  \time 4/4
  \partial 4
  c8 d
  es4 c8 d es4 d8 es
  f4 d8 es f4 es8 f
  g4 es8 f g f es d
  es4 d8 es c4
}