 
\version "2.16.1"
 
#(set-global-staff-size 14)
 \layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Recitativ (Tenor) - Ach, windet euch nicht so [BWV 245c - Anhang in zweiter Fassung]" 
    }
    \vspace #1.5
}
 
\relative c''' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe I"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key c\minor
                      \tempo "o. Bez."
                      \time 2/2
                      \partial 8
                      % Voice 1
                  g8 es as g f es as g f
                  es g c, d es d d g,
                  es'4 e f fis g8 a fis4 g r8
                   
               
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe II"
                     \set Staff. shortInstrumentName = #"Obdm"
                     \key c\minor
                        % Voice 2
                   es8 c f es d c f es d 
                   c2. b8 d g, des' c bes a es' d c
                   bes c a4 g r8
                        
                  }
                  
              
>>
}


\relative c''' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key c\minor
   \tempo "o. Bez."
   \time 4/4
   \partial 8
   \set Score.skipBars = ##t
     r8 R1*9 r2 r4 r8 g8
     es as g f es as g f 
     es g c, d es d d r8
  
}

