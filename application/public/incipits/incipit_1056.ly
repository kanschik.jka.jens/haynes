\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 6/8
     c,4~ c32 b e d c4 r8
     e4~ e32 d g f e4 r8
     g c e g16 e a8. g16
     f4 g32 f e d
     c16. d32 e16. c32 f16. e32
     d8
} 

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. o.Bez. [Oboe]"
  \time 6/8 
  \set Score.skipBars = ##t
   R2.*6 
   g'2.~ g4.~ g8 a b
    c e, a  g f cis
    d~ d16. f32 e16. g32 f8 r r
} 
