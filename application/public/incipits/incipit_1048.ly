 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Adagio"
  \time 4/4
    c,2 c16.[ c32] e16. d32
    c16.[ c32] e16. d64 c
    g'2 g16.[ g32] b16. a32
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Allegro"
  \time 3/4
  c,8 r g'4 c~ c b r
  c,8 r g'4 c~ c b r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Larghetto"
  \time 3/4
  R2. R
  r4 r g
  \grace g8 f4 e8 r r4
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Allegro di molto"
  \time 4/4
  \partial 4
    g4
    c2. b4 a2. g4 g8 f f4 r f
    f'2. e8 d
}