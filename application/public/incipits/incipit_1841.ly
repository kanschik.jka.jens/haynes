 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 

 
\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key bes\major
   \tempo "o.Bez."
  \time 12/8
       bes8 f bes  bes16 c d8 c
       c f, c' c16 d es8 d
       d f, d'  d16 es f8 bes, a16 bes c8 f, f4.
}

\relative c'' {
  \clef "treble_8"
  \set Staff.instrumentName = #"Soprano"
  \key bes\major
   \tempo ""
  \time 12/8
     \set Score.skipBars = ##t
     R1.*8
       bes,8 f bes  bes16 c d8 c
       c f, c' c16 d es8 d
       d bes d d16 es f8 d  g f es d4.
       c4.
}