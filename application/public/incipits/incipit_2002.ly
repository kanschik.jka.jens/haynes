 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Adagio"
  \time 4/4
    d4. c16 bes a4 r8 d8
    g, c4 bes16 a bes8 g d' g
    f4 es d4.
}


\relative c'' {
  \clef treble
  \key g\minor	
   \tempo "2. Allegro"
  \time 2/2
  r8 d d d  es[ es16 d] es d c es
  d[ c bes a]  g a bes c d4. es8
  f16[ g a g] f es d f  es8[ a,]
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Adagio"
  \time 3/4
  r4 f,4 bes
  a f r
  r c' d es2 d4 d
}

\relative c'' {
  \clef bass
  \key bes\major	
   \tempo "4. Passagaglia [Basso}"
  \time 3/4
  bes,,4 bes'4. bes8
  a4 f4. f8
  g4 d4. es8
  f4 f,4. f8
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "4. Passagaglia [Oboe}"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*4 
   r4 r bes
   f'4. g8 a f
   bes4 f4. g8
   c,4 c d8 es
   d4 bes r
}