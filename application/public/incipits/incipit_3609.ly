\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro moderato"
  \time 4/4
    c8 c4 e16 g g8. f16 e8 c'
    c16 b b a   a g g f  e8 f g4
    a2 g a g f4 e f e
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio [Tutti]"
  \time 3/4
    d4 d8 e16 d d8 e16 fis
    \grace a16 g8 fis16 e d4 c16 b a g
    a8 b c e d c
    c4 b r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Adagio [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
    R2.*4
    d4 d8 e16 d d8 e16 fis
    \grace a16 g8 fis16 e d4 c16 d c b
    a b c b  c b fis' c c' a fis c
    c4 b8 r r4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 6/8
    c,8 e g
    c16 b c d e g,
    a g a b c g
    f8 d g c,4 r8
}