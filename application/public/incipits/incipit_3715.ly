 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Largo"
  \time 4/4
  \partial 4
  \grace as'16 g8 f16 es
  bes'1~ bes
  bes8. g16 es4 r8 es es es
  es8. d16 es4 r8
}


\relative c'' {
  \clef treble
  \key es\major	
   \tempo "2. Allegro con Spirito"
  \time 2/4
  es2 bes g4 r8 \times 2/3 { es16[ f g] }
  as8 bes c d es f g as
  bes4. as8
  g16 f es8 r4
}
