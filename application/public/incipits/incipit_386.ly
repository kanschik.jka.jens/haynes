\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio [Tutti]"
  \time 3/4
  <g g,>4 g16 fis a g  b a c b
  <b d, g,>4 b16 a c b d c e d
  d8 g r a,16 b \grace d8 c8 b16 a
  g8 b r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*8 
    g'2. g g2 d4
    b8. a32 b g4 g
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro [Tutti]"
  \time 2/4
  g4 r8 \times 2/3 { g'16[ g g]}
  d8 c b a 
  g4 r8 \times 2/3 { g'16[ g g]}
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*40
   r4 r8 r16 d,
   g2 b d g
   b4 d d16 c c4 b8
   b16 a a4 g8 g16 fis fis4 e8
}

