\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. o.Bez."
  \time 3/4
      r4 d8. e16 d8. e16
      d4 a' b
      r c,8. d16 c8. d16
      c4 g' a
}
