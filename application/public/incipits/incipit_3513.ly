\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key g\major
  \override TupletBracket #'stencil = ##f
    \time 4/4
    \tempo "1. o.Bez."
      g'8. c16 g8 g g4 \grace g8 fis e16 fis
      e8. g16  e8 e e4\grace e8 d8 c16 d
      c8 e r g fis d r b
}



