 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Ob d'Amore"
  \time 4/4
    f8 c a c  f c a c
    d[ d]
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1."
  \time 4/4
    d a fis a  d a fis a
    b[ b]
}