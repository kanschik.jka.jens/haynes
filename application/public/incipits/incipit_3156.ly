\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Sopran + Bass): O wie wohl ist uns geschehn [BWV 194/10]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key f\major
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                     c4 d8 c bes d
                     c2 bes4~ bes8 c16 d c4 bes8 a 
                     g2. r4
                 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key f\major
                     \set Score.skipBars = ##t
                     a4 bes8 a g bes 
                     a2 g4~ g8 a16 bes a4 g8 f
                     e2. r4
               
                        
                
                  }
>>
}


\relative c'' {
	<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Sopran"
                     \key f\major
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                  \set Score.skipBars = ##t
                     R2.*30  c4 d8 c bes d
                     c2 bes4~
                     bes8 c16 d c4 bes8 a g2 r4
                      
                     
                  }
\new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Bass"
                     \key f\major
                     \set Score.skipBars = ##t
                        % Voice 2
                 R2.*30 a,4 bes8 a g bes
                 a2 g4~
                 ~
                 g8 a16 bes a4 g8 f 
                 e2 r4
                        
                
                  }
>>
}

	
	
