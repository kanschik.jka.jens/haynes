\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro Moderato"
  \time 4/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f

  \partial 8
  c,8
  f a a c c4 f16 es d c
  c8 bes16 g g8 g \grace g16 a8 f r4
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Menuetto"
  \time 3/4
    a'2. bes8 g es c d es
    f4 bes a
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Adagio"
  \time 3/4
      \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \partial 4
    d,16 es d es
    f4 d' c
    c16 bes a bes bes4 r8 f'
    f4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "4. Menuetto"
  \time 3/4
      \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \partial 4
  \times 2/3 { c8 f a}
  bes4 bes \times 2/3 { c,8 e g}
  a4 a \times 2/3 { a,8 c f}
  d4
}


\relative c''{
  \clef treble
  \key f\major
   \tempo "5. Finale. Rondo Allegro"
  \time 2/4
      \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \partial 8
    c8
    f f \grace g16 f8 e16 f
    g8 g \grace a16 g8 f16 g
    a8 f bes a
    a4 g8
}
  
