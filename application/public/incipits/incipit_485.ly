 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro ma non tanto"
  \time 4/4  
      r4 e4. f8 fis g
      g2 b,4 r
      r f'4. fis8 g d
      f2 e4 r
}
