 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 8
  a8
  d16 a8. fis16 d8. 
  b'8 a r d
  cis16 e a,8~ a16 g fis e
  fis8 d r
}


\relative c'' {
  \clef treble
  \key b\minor	
   \tempo "2. Andante"
  \time 2/4
  \partial 8
  fis,8
  b b b16 cis32 d cis b ais16
  b8. a32 g fis8 e
  d fis b cis d cis fis e
  \times 2/3 { d16[ cis b]} b8 r
}

\relative c'' {
  \clef treble
  \key d\major	
   \tempo "3. Allegro"
  \time 3/8
  d16 d, fis d a' d,
  d' a a a fis d'
  fis fis, a fis d' a
  fis' d d d a d
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "4. Menuett"
  \time 3/4
  \partial 4
  
}