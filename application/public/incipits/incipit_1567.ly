\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "o.Bez."
  \time 4/4
  \partial 4
  d8. d16
  d4. g8 g fis fis e
  e d d c b2
  \grace dis8 e4. a,8 \grace e'8 d4 c8[ b]
}
