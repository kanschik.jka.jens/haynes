 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Moderato"
  \time 3/4
    a'4. f8 e d
    \grace d8 c8. d16 e4 r8 c'
    c4 b16 d c b  a gis b d,
    c4 b r
}
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. ALlegro"
  \time 2/2
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  \partial 4
  e4
  \times 2/3 {a8 b c} c2 b8 a
  a2  \times 2/3 { gis8[ b a]}  \times 2/3 {a8 b c}
   \times 2/3 {g8 a b} b2
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Vivace"
  \time 3/4
    \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
    a8 c16 b  c8[ d e f]
    f4 e8 c16 d e8 f
    f4 e8 c16 d e8 f16 e
    d8 d16 c d8 b16 c d8 e16 d
    c8
}