\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro"
  \time 2/4
 
  \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
   s8
   \times 2/3 {g16[ b a] }
   \times 2/3 {b16[ d c] }
   \times 2/3 {d16[ g fis] }
   \grace g8 fis4 r
   \times 2/3 {g,16[ a b] }
   \times 2/3 {b16[ c d] }
    g,16 b'8 a32 g
\grace g8 fis4 r
  
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Andante"
  \time 2/4
  \partial 16.
    g'32 fis g
    d16 d8 c16 bes bes8 a16
    \times 2/3 {g16[ a bes] }
    \times 2/3 {bes16[ c d] }
    \times 2/3 {d16[ c bes] }
    \times 2/3 {c16[ bes a] }

}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Presto"
  \time 2/4
  g16 a b a g8 b
  d g b d
  \grace g,8 fis4 r

}
