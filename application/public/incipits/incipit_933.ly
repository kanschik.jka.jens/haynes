 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1."
  \time 3/4
  r4 d g
  fis fis4. g8
  e4 c4. c8 c4. b8[ c d]
  b4
}


