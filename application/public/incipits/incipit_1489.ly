 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Largo e cantabile"
  \time 3/4
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
   e4 a c
   c b r8. d16
   \grace a8 gis4~ gis8. b16 e,8. e'16
   \grace e8 d2 c4
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Allegro"
  \time 2/2
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
    e4 e2~ e8. e16
    c4 a r e'~
    e8. c16 c4 \times 2/3 { b8 b c} \times 2/3 {d8 c b}
    \times 2/3 {c8 b a} a4 r
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "3. Vivace"
  \time 3/4
 \set Score.skipBars = ##t
  \override TupletBracket #'stencil = ##f
       a4 c b
       c a r
       e8 e4 e' d8
       c4 a r
}