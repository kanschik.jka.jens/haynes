\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Ouverture"
  \time 2/2
  es8. bes16 bes8. es16  es8. g16 g8. es16
  f8. bes,16 bes8. f'16  f8. as16 as8. f16
  g8 bes, es g
}
      
\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Bourree"
  \time 3/2
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Aria Siciliana"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "4. Aria"
  \time 4/4

}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "5. Menuet altern."
  \time 6/4
  \partial 2.

}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "6. Trio"
  \time 3/8
  \partial 8

}

