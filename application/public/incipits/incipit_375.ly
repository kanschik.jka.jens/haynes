 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 2/4
  \partial 16
    g16
    e'8. c16 g'8 f16. d32
    e8. c16
    g'8 f16. d32
    e16 c c' a g32 f g f g[ f e f]
    f8 e
}


\relative c'' {
  \clef treble
  \key g\major	
   \tempo "2. Adagio"
  \time 2/4
  \partial 8
    d
    e32 d c b b8~ b8 g'32 b b, d
    d c b a a8~ a fis'32 a a, c
    c16 b e32 d c b b16 a d32 c b a
    a g fis g g8 r
}

\relative c'' {
  \clef treble
  \key c\major	
   \tempo "3. Allegro"
  \time 3/8
  g'8. a16 g8
  g4 e8 g8. a16 g8
  g4 d8
  e16 f g e d c b d f a g f
}
