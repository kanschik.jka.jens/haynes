\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Poco Largo"
  \time 4/4
  c8 c c16 g' es c c8 b r c
  d d d16 es32 f es16 d \times 2/3 { es16 d c } c4.
}

\relative c'' {
  \clef alto
  \key c\minor
   \tempo "2. Alla breve"
  \time 2/2
  r2 g2
  es c
  as'1~
  as4 d, g f
  es2 e 
  f fis
  g4 f es d8 c 
  d1
  c4
}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "3. Allegro"
  \time 3/4
  r4 g4 g
  as \grace g8 f2
  g8 g16 f g8 c, d es
  f4 \grace es8 d2
  es4
}
