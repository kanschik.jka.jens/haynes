
\version "2.16.1"

#(set-global-staff-size 14)

\relative c''' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 4/4
   a4 a2 cis16 d cis b
  \grace b8 a4 a2 cis16 d cis b
  \grace b8 a4. e8 fis a16 gis \grace b16 a8 gis16 fis
  \grace g8 fis4 e4 r2
}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "2. Rondo Grazioso"
  \time 2/4
   \partial 4
  r4 r2 r4 cis8. d16
  cis b b8 b8. cis16
  b16 a a8 cis,8 \grace e8 d16 cis32 d 
  e cis16. a'32 b16. fis32 e16. d32 cis16.
  cis8 b r4
}


