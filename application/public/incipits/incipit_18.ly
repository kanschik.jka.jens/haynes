\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. o.Bez."
  \time 2/2
  d8 d d d  es es es d
  c c c c  c c es g,
  fis fis fis fis
}
