\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Les Nopces de village " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Ouverture "
  \time 3/4
    g'4. fis8 e4
    d4. e8 c4
    b4. b8 c4
    d4. c8 b4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. 1.er Entrée "
  \time 2/2
    g8 fis g a b4 g
    d' g, d' dis
    e8 d e fis g4 d
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. 2.e Entrée "
  \time 3/4
  g2 d'4
  b2 c8 b16 c
  d4 e a,
  b4 c4. b8 a2 g4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. 3.e Entrée "
  \time 2/2
  \partial 2
  g4 d'
  d2 a8 b c4
  b g g'8 a b4
  a d, g fis
  g2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "5. 4. Entrée "
  \time 2/2
  \partial 4
    b8 c d4 d e8 d e fis
    g4 d2 g8 a
    b4 a a8 b g a
    fis2.
}	

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "6. 5.e Entrée "
  \time 2/2
  \partial 4
    r16 f,16 g a
    bes4. f8 bes4. c8
    d4. bes8 d4. es8
    f2
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "7. 2.e Air "
  \time 4/4
  g2 b4. c8
  d4 d2 g8 fis
  g4 e2 a8 g fis4 fis2
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "8. 6.e Entrée "
  \time 2/2
    d4. d8 g2
    g4. g,8 bes4. c8
    d2 c
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "9. 7.e Entrée "
  \time 2/2
  \partial 4
  r16 d16 e fis
  g4. fis8 g4. d8
  e4. b8 e4. d8
  c4. d8 a4. b8
  a2 g4.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "10. Bourée "
  \time 2/2
  \partial 4
  g4 d' b c8 d e4
  d c8 b a4 a'
  a4. g8 fis e d e
  cis4. d8 d4
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "11. Air pour Barbacola "
  \time 2/2
  \partial 4
  b8 c d4 c8 b a4 b8 c b4 g2 g'8 a b4 a8 g fis4 e8 d a'2.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "12. 8.e Entrée "
  \time 4/4
  g4. a8 b4 g
  d'4. b'8 a4. g8
  fis1
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "13. 11.e Entrée "
  \time 2/2
  \partial 4
  r8 g'
  fis2. g8 d
  es4 d c4. f8
  d4 bes2
}
\relative c'' {
  \clef treble
  \key g\major
   \tempo "14. 13.e Entrée "
  \time 3/4
  g'4 d4. g8
  e4 e2
  d8 e c4. b8 b2.
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "15. 2.e Air "
  \time 3/4
  d4. e8 fis4
  g d e
  b c d
  b4. a16 b g4
}
