 
\version "2.16.1"
 
#(set-global-staff-size 14)


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 1" 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key d\minor
   \tempo "1. o.Bez."
  \time 4/4
     \set Score.skipBars = ##t
     r8 d, f a r16 a[ b cis d cis d8]
     r16 e[ f g a g a8] r16 e f cis
     d a bes g
     a8 d, r4 r2

}
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob"
                     \key d\minor
                      \tempo "1. o.Bez."
                      \time 4/4
                      % Voice 1
                      R1 R
                      r4 d,8 f a4 r16 a b cis
                      d cis d8 r16 d e f g f g8 r16 e f cis
                  }
\new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Bassoon"
                     \set Staff. shortInstrumentName = #"Bsn"
                     \key d\minor
                        % Voice 2
                        R1 R
                        r2 d,,8 f a4
                        r16 a b cis d cis d8 r16 e, f g a g a8
                  }
>>
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 3" 
    }
    \vspace #1.5
}


\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob"
                     \key d\minor
                      \tempo "3. o.Bez."
                      \time 3/2
                      % Voice 1
                      R1.
                      r2 d,8 e f g  a bes g a
                      f4 d r2 r
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Soprano"
                     \set Staff. shortInstrumentName = #"S"
                     \key d\minor
                        % Voice 2
                        r2 d8 e f g  a[ bes] g[ a]
                        f4 d r2 r
                        r2 d8 e f g  a[ bes] g[ a]
                  }
>>
}



\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 5" 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Soprano"
  \key d\minor
   \tempo "5. o.Bez."
  \time 3/2
     \set Score.skipBars = ##t
      \partial 2
      d2 f2. f4 e2
      cis a a bes2. d4 c2
      a f
}
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe"
                     \set Staff. shortInstrumentName = #"Ob"
                     \key d\minor
                      \tempo "1. o.Bez."
                      \time 3/2
                      \set Score.skipBars = ##t
                      \partial 2
                      % Voice 1
                      r2 R1.*7
                      r2 r d2 f2. f4 e2
                      cis a a bes2. d4 c2
                      a f
}
\new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Bassoon"
                     \set Staff. shortInstrumentName = #"Bsn"
                     \set Score.skipBars = ##t
                     \key d\minor
                     \partial 2
                        % Voice 2
                      r2 R1.*7
                      R1.
                      r2 r8 d c d  g, bes a g
                      a2 a, r
                      r r8 g' f g c, e d c
                      f2 f, r
                  }
>>
}

