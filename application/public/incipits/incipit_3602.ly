\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro giustoso"
  \time 4/4
   \override TupletBracket #'stencil = ##f
  
  \partial 8
    d,8
    d16 g r8 g16 b r b \times 2/3 { b16 d c } c4 c8
    \times 2/3 { b16[ c d] }  \times 2/3 { e16[ fis g] }  \times 2/3 { fis16[ e d] }
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Rondo. Tempo di minuè"
  \time 3/4
 
}
