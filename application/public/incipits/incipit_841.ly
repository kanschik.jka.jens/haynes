\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Andante"
  \time 4/4
  a'16 g f g a8 bes bes16 g a8 r4
  c16 bes a bes c8 d d16 bes c8 r4
  d16 c d c bes a bes a
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Allegro"
  \time 2/4	
  a'16 g f g a8 bes
  a16 g f g a8 bes
  a bes16 a g8 f
  e16 d c d e f g f
}


\relative c'' {
  \clef treble
  \key f\major
     \tempo "3. Andante"
  \time 3/4
  a'8. bes16 a4 g
  f16 e d8 e4 r
  g16 f e8 f4 r
  a8. bes16 a4 g
}



\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 3/4
  f4 e8 d c bes
  a4 bes g f r g'
  f g e
}