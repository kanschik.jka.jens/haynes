\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 2/4
  e8 a4 gis16 fis
  e8 a,~ a16 b cis d
  e8 a4 gis16 fis
}


\relative c'' {
  \clef treble
  \key e\major
   \tempo ""
  \time 4/4
    b4 e,8 e' cis4 b~
    b8 e, e' cis cis4 b
    b a8. gis16~ gis8 cis16 bis cis4
}


\relative c'' {
  \clef treble
  \key a\major
   \tempo ""
  \time 3/4
    a4. cis8 b e
    cis a4 b16 cis b8 gis
    a e~ e16 a gis fis e8 d
    cis a r
}
