 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Largo"
  \time 2/2
  g4 r16 d e fis g[ d e fis] g a b c
  d4
}
\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Presto"
  \time 2/2
  d,16[ d d d] e e e e    fis[ fis fis fis] g g g g
  a[ a a a] b b b b c8 b16 a b8 a16 g
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Ritornello"
  \time 6/4
  g2 a4 b a g
  d'2. r4 r d
  g fis g fis2 g
  d c b c b a
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. o. Bez."
  \time 2/2
  \partial 4
  g4
  d' g, d' e8 fis
  g4 fis8 e d4 e 
  d c8 b a b c d
  b4 a8 b g4
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. o. Bez."
  \time 3/4
  g'4 fis2
  g4 d8 c b a 
  g8. a16 a4. g16 a
  b8 c d4.
}
