\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Was unser Gott geschaffen hat [BWV 117/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe d'amore 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key e\minor
                      \tempo "o. Bez."
                      \time 6/8
                      \partial 8
                      % Voice 1
                  b8 g a fis g4 b8
                  e16 fis g e fis dis e4 d8
                  c b a 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe d'amore 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key e\minor
                     \set Score.skipBars = ##t
                        % Voice 2
            g8 e fis dis e4 fis8
            g16 a b g a fis g4 g8~ g fis16 e fis8~ fis
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key e\minor
   \tempo "o. Bez."
  \time 6/8
  \partial 8
     \set Score.skipBars = ##t
    r8 R2.*7 r4 r8 r8 r8 b8
    g a fis g4 b8
    e16 fis g8 fis8 e4 
}