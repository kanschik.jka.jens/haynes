 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro assai [Tutti]"
  \time 4/4
  bes4. r16 f c'4. r16 f, 
  c'4. r16 f, d'4. r16 f,
  es'4 es es d8. es16 c4 f \grace es8 d4. r16
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro assai [Solo]"
  \time 4/4
     \set Score.skipBars = ##t
   R1*16 
     f4~ \times 2/3 { f8[ g as] }
     \grace c,8 b4~ \times 2/3 { b8[ c d] }
     g,4 f
     \times 2/3 { es8[ d c] } g''4~
     g8. es16 c8. bes16 
     \grace bes8 a4
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Adagio ma non tanto [Tutti]"
  \time 4/4
  bes,4 bes'2 a4
  des2 c~
  c8 f, bes2 as4
  ges4. ges8 f2

}
\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "2. Adagio ma non tanto [Tutti]"
  \time 4/4
       \set Score.skipBars = ##t
   R1*9 
     r2 r4 as'4~
     as8 f ges4. es8 f4~
     f8 bes, es4~ es8 c des bes
     ces ges'~ ges16 es ces bes a8 bes r 
}

\relative c'' {
  \clef treble
  \key bes\major	
   \tempo "3. Tempo di menuet"
  \time 3/8
    bes8 c d c16 d es4
    d8 es f a,16 bes c4
}