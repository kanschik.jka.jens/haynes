\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro. Tempo giusto"
  \time 4/4
    f16 d g f f4  f16 d g f f4
    \grace f8 g8 f16 es d c bes a
    \grace c8 bes16 a bes8 r4
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "2. Adagio cantabile"
  \time 3/8
    bes16. es32 bes4
    c16. es32 c4
    bes8 \grace f'8 es8 \times 2/3 { g16[ d es]}
    bes8 as g
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Tempo di Menuetto"
  \time 3/4
    bes2 d8 bes f'2 es16 d c bes
    \grace as8 g8 f g es' c a
    bes4 bes, r
}