 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro assai"
  \time 4/4
  \partial 4
  d4
  g16 a g fis g8 a ais b fis g
  fis e e4 r r
  d16 e d cis   d e fis g  a g fis e  d c b c c8 b b4
}


\relative c'' {
  \clef treble
  \key c\major	
   \tempo "2. Adagio poco Andante"
  \time 3/4
  g'8. a16 g8 r r4
  g8. a16 g8 r r4
  c4 e, fis g4. gis16 a g fis e fis
}

\relative c'' {
  \clef treble
  \key g\major	
   \tempo "3. Rondo. Allegro"
  \time 2/4
  g'8 g r d
  e e r fis
  g g a8. g16
  g fis e d cis d e fis
}
