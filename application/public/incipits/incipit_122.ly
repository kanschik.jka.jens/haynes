\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
  \partial 8
  bes16 a
  bes8 f g d   es c r c'16[ bes]
  c8 g a es f d r d'16[ c]
}


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 2/2
  bes8 g es es'~ es d16 c d8[ es16 f]
  c4~ c8 d16 es32 f
  as,4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/4
  bes8 d f d bes f'
  a,4 a a 
  g8 bes es bes g es'
  f4 f f
}
