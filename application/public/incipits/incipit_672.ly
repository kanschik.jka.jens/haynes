
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto Comique"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key bes\major
   \tempo "1. Allegro"
  \time 2/4
f8.[ g16 f8. es16]
d4. c8
bes4 bes
f'8.[ g16 f8. es16]
d4. c8 bes2
bes8.[ a16 bes8. c16]
d4. es8 f4 f

 
}



\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Andante"
  \time 2/2
d4 d8 d cis4. cis8
d d cis d a a16 bes cis8 cis
f,4. f8 g g a a 
d4 r4


}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegro"
  \time 3/8
f8 f f 
g bes4
a g8
f4.
es4 d8
c d bes
c f4
d c8
f f f 
g a4 e f8 g4.

 
}
 

 

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto II. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/4
\partial 8
d8 g[ g g fis]
g f16 es d8 d
g[ g g f] 
es4 d8 d
g[ g g fis ]
g f16 es d8 d
g[ g g f]
es4

 
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio"
  \time 3/4
d4 d d
d8 g g f d f
es4 es es
es8 a a c c d, 
d4 d d
es8 g g es es bes a4 a a bes2


}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 3/8
\partial 8
d8 g g fis
g f16 es d8
g g f
es4 d8
g g fis
g f16 es d8 g g f
es d f
es16 d c8 es
d16 c bes8 d

 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Margoton. IIIe. Concerto"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Adagio"
  \time 3/4
c4 c c 
g' es8 f g4
b, b b
c c'8 bes c4
bes bes bes
as
f8 g as f
g f es f g es


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allegro"
  \time 2/4
r4 e16 f g8
c,4 d 
e8 d e16 f g8
c,4 d
e e16 f g8
c,4 d
e8 d e16 f g8
c,4 d e

}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "3. Adagio"
  \time 4/4
\set Score.skipBars = ##t
   R1*1 r2 r4 r8 g16 f
es8 d16 es c es d c b8 g16 c d es f d
es f g es f g as f g bes as g f4



}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 3/8
r8 e16 f g8
c, d4
e16 d e f g8
c, d4
e8 e16 f g8
c, d4
e16 d e f g8 c,8 d4 e8 g, d' d


}




\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto IV."
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. Allegro"
  \time 2/4
a8[ a a a]
d[ d d cis16 b]
cis8[ cis cis d16 e]
fis8 fis, b4~b8 a16 gis a4~ 
a8 gis16 fis gis8 a16 b
cis b cis d cis8 

}

\relative c'' {
  \clef treble
  \key fis\minor
   \tempo "2. Adagio"
  \time 3/4
b4 b b cis2 r4
e4 e e fis2 r4
b,4 b b b b b c2 r4




}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Allegro"
  \time 3/4
e4 e2
cis4. b8 cis4
d d cis b2 b4
e e2
cis4. b8 cis4 d b2 a2.
 

}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Concerto V. "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Allegro"
  \time 2/4
\partial 8
d8 d8.[ cis16 d8. a16]
 b8. b16 fis8. fis16
g8. g16 a8. a16 
b8. b16 fis8. fis16
g8. e16 a8. a16 d,4

 
}

\relative c'' {
  \clef treble
  \key a\minor
   \tempo "2. Adagio"
  \time 3/4
f4 f f f2 r4
g4 g g 
cis,8. a16 cis8. e16 a8. e16
f8. d16 f8. a16 d8. a16
bes8. e,16 e4. d8
c4 c c
 
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "3. Allegro"
  \time 2/4
\partial 8
a8 d[ d d d] 
e d r16 a16 b cis
d8[ d d cis16 d]
e8 d4 c8~ c b a g16 fis
e8[ d a' a]
fis d
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Le Plaisir des Dames. VI. Concerto"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Allegro "
  \time 2/4
g4 b8 c 
d4. d8 e[ fis g e]
fis4 d
e8[ fis g e]
fis4 d
e8[ fis g a]
 fis[ g a b]
fis4 e
d2
 
}


\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Adagio"
  \time 4/4
d8 d d d d4 r4
d8 d d d es4 r4
es8 es es es d4 r4
bes8 bes bes  bes bes4 r4

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Allegro"
  \time 2/4
\partial 4
d8 d16 es
d8[ c bes c]
d4 d8 d16 es
d8[ c bes c]
d d16 es d8 c
bes[ a g8. g16] 
g8[ g g g] 
 
}

