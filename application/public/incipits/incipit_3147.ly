\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor): Wohl dir, du Volk der Linden [BWV 119/3]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe da Caccia 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\major
                      \tempo "o. Bez."
                      \time 4/4
                      % Voice 1
                     r2 r8 g8 d8. e16
                     fis8. e16 fis8. g16 e8 a e8. fis16
                     g8. fis16 g8. a16 fis8 
                 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe da Caccia 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\major
                     \set Score.skipBars = ##t
                        % Voice 2
                        r8 d8 g,8. a16 b8. a16 b8. c16
                        a8 d a8. b16 c8. b16 c8. d16
                        b8 
                   
                       
                        
                
                  }
>>
}


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tenor"
  \key g\major
   \tempo "o. Bez."
  \time 4/4
     \set Score.skipBars = ##t
    R1*11 d1~ d16 c b c32 a g8. a16 b8. a16 b8. c16
    a4 d,8 
    
    
    }