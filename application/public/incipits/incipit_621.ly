
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Sonate "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. Legerem.t"
  \time 2/4
\partial 8
g8 
c b16 a g8 f
e4. a8
g f16 e d8 c
b4. c8 d e f f e d g g 
g4

}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Rondeau Le Gracieux. Gracieusem.t"
  \time 3/8
\partial 8
c8 g' c16 b c8
e,4 a8
g f16 e d c 
b4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Le Duc De la Trimouille. Vivem.t"
  \time 2/4
\partial 8
c8 g'4 r8 a16 b 
c8 b16 a g8 f
e g 
f16 e d c 
b8. a16 g8 g


}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxieme Suite "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. La Pagotte. Gracieusement"
  \time 3/8
g4 c8 
c b16 a g8
e4 f8
f e16 d c8
g'4 c,8 
c b16 a g8
c16 b c d e f 

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Fanfare Le Chasseur. Legerem.t"
  \time 6/8
\partial 2
g8 c4 e8
d g, g c4 e8
d g, g g'4 c8
g f e f4 e8
d4 g,8 c4 e8 d g, g 

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. La Coulombe. Gracieusement"
  \time 3/4
e4 f8 e d c 
d4 g, g'
e4. f8 e f g2.
a8 g f e d c 
b4 \grace a8 g4

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Le Chevalier De Guines. Legerem.t"
  \time 2/4
\partial 8
c8 g'4 es8 f16 es
d4 es8 f16 g
c,8 es d c
b8. a16 g8 d'
es f g as
g f es d 

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisieme Sonate "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. La Noce de Bercy. Gracieusem.t"
  \time 2/2
\partial 2
c8 b c g
a4 g f e8 f
e4. d8 c b c g 
a4 f'8 e d4. c8
b4 \grace a8 g4


}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Chaconne"
  \time 3/4
\partial 2
es4 es
d es8 f g4
as8 g f es d c 
b4 es es 

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. La Mariée de Bercy. Legerem.t"
  \time 2/4
\partial 2
c4 d
es8 b c d 
es4 f g8 c, c g'
c bes as g
f es d c
f4 es 
d4. es8 
d


}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatrieme Sonate a deux Muzettes "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Gracieusem.t"
  \time 2/4
g4 c
b r8 r16 c16
d8. e16 f8. g16 
e4 d 
c'8. b16 a8. g16 fis4

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Le Vilers Brulin. Vivem.t"
  \time 2/4
\partial 16
c16 c4 e16 d c b 
c4 e16 d c b 
c8 e d c 
b \grace a8 g8 r8 e'8 g4 e8 \grace d8 c8
g'4 e8 \grace d8 c8

}
\relative c''' {
  \clef treble
  \key g\minor
   \tempo "3. Menuet"
  \time 3/4
g8 c, b c es c
g' c, b c es c
g' f es d c b 
c2 g4

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "4. Sarabande"
  \time 3/4
c4. g'8 f g
es4. d8 c4
d8 es f es d c
b4. a8 g4 

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "5. Branle"
  \time 2/4
c8 c16 d
e8 e d c 
b g e' e16 f
g8 g g f 
e d 

}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquieme Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. La Comtesse de Choyseul. Legerem.t"
  \time 2/4
\partial 8
d8 
g g g g 
g4. fis16 e
d8 e d c
b8. a16 g8 b16 c 
d8 d d d 
d e16 d c b a g 
c4 b a


 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Rondeau Le Comte de Soiiastre. Gracieusem.t"
  \time 3/8
b4 c8
d g, g'
fis4 e8
b4.
c8 b a b c d 
c4 b8
a4 g8
b4

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Gavotte"
  \time 2/4
\partial 4
b16 c b c
d8 d d d 
d4 g16 fis e d 
e8 e d c
b g b16 c b c 
d8 d d d

 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Menuet"
  \time 3/4
g8 fis g fis g fis 
g'2 d4
e8 d c b c a
b4 a8 b g4


 
}



\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    " Sixieme Sonate"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Gracieusem.t"
  \time 3/4
g4 d'8 c bes a 
bes4 \grace a8 g4 es' d g4. a8
fis2
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Branle"
  \time 2/4
\partial 4
d8 d
g, e' d c 
b a b d 
d d c b a4
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Chaconne La Fougere"
  \time 3/4
r4 g4 g 
d' g8 fis e d 
e4 d d 
d c8 b c d 
b4 

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Le Vivomte de Runboval. Legerem.t"
  \time 2/4
d4 g
fis4. g16 fis
e8 fis16 g
 d8 c
b4 a 
b16 c d8 d g
b,16 c d8 d g
 
}





