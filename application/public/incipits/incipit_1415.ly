\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
    bes2 d f2. d4
    bes f d f bes r r f
    bes2 bes16 c d c \grace es16 d8 c16 bes bes4
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key es\major
   \tempo "1. Allegro"
  \time 4/4
   \partial 4
   bes'8 g
   f es es2 c'8 bes
   a bes bes2 \grace as16 g8 f16 es
   d es d es  f g f g  as4 as
   
  
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto VI " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
  \partial 4
  g,4
  g8 c e4. c8 d b
  c e g4. e8 f d
  e c' c4. b8 a g g4 a8 f e4
}

