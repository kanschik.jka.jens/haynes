 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro"
  \time 4/4
    c,2 e4. g8
    c4. b16 c g8 g'16 a g8 g
    g4 f r8 f16 g f8 f
    f4 e r8
   }
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Menuet"
  \time 3/4
       \override TupletBracket #'stencil = ##f
     e,4 e4. f16 g
     f8 d  \times 2/3 { e8 c e }  \times 2/3 { g8 e g} c4 
}

 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Adagio"
  \time 2/4
  \override TupletBracket #'stencil = ##f
  c4. a'8
  a g d8. e32 f
  \times 2/3 { e16 c' g}  \times 2/3 { a16 f d}
  c8 d
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Allegro"
  \time 2/4
    e,4. f16 g
    f4. g16 a
    d,4. e16 f
    e8 g c e,
   }
