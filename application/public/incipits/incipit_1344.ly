 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "1. [Adagio]"
  \time 2/2
  a4 r16 e' cis a a4 r16 e' cis a
  a4 r16 e' cis a  fis' e e a d,8. cis16
  cis4
 }
 
\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Allegro"
  \time 2/2
  a8[ cis16 d] e8 a,  gis b e e,
  a16[ b cis d] e8 a gis e r
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "3. Adagio"
  \time 3/4
  a4 cis e
  gis,2 a4
  fis' e8 gis fis e
  d2 cis4
}
\relative c'' {
  \clef treble
  \key a\major
   \tempo "4. Tempo di Boure"
  \time 2/2
  a4 e'8 fis e d cis b
  a4 b' g2
  fis4 a e a
  d,8 fis e d cis4 a
}


