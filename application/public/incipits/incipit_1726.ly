\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Adagio"
  \time 4/4
  d8 g,16 fis g8 a b g r d'
  e16 d c b a e' d c b8 g r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Allegro"
  \time 4/4
  g4 a b4. d8
  e16 d e d  c e d c  d c d c  b d c b
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Largo"
  \time 3/2
  e2 r g fis r b
  gis r a a gis1
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Allegro"
  \time 2/4
  g8 fis g a b a b c
  d4 c16 d e8
  d4 r
}
