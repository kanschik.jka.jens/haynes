\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Tenor + Bass): Sie werden euch in den Bann tun [BWV 44/1]" 
    }
    \vspace #1.5
}
 
 
\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \set Staff. shortInstrumentName = #"Ob 1"
                     \key g\minor
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                      r2. r2. d4 d d 
                      e g, e'
                      f2.~ f~ f8 d es c bes c a4
                 
                     
                      
                     
                  }
\new Staff { \clef "treble" 
                     \set Staff.instrumentName = #"Oboe 2"
                     \set Staff. shortInstrumentName = #"Ob 2"
                     \key g\minor
                     \set Score.skipBars = ##t
                  g4 g g 
                  a c, a'
                  bes2.~bes~bes8 g as f es f
                  d4 b'4. a16 b
                  c2.~c
                        
                
                  }
>>
}


\relative c'' {
	<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Tenor"
                     \key g\minor
                      \tempo "o. Bez."
                      \time 3/4
                      % Voice 1
                  \set Score.skipBars = ##t
                     R2.*24 d4 d d e g, e' f2.~
                     ~f2.~ f8
                      
                     
                  }
\new Staff { \clef "bass" 
                     \set Staff.instrumentName = #"Bass"
                     \key g\minor
                     \set Score.skipBars = ##t
                        % Voice 2
                    R2.*22 g,,4 g g
                    a c, a'
                    bes2.~bes~ bes8 g as f es f
                    d4
                        
                
                  }
>>
}

	
	
