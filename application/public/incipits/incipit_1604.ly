\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 4/4
  \partial 8
  c8
  bes16 a d c  c8. f16 f e a g g8. bes,16
}
