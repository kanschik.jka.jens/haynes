\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Partita I " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro "
  \time 4/4
    g'2 f8 e d c
    a4 g r \times 2/3 { c,8[ d e] }
    \times 2/3 { d8[ e f] }
    \times 2/3 { e8[ f g] } f2
    e
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Largo"
  \time 3/4 
    d2 \grace c8 b4
    \grace a8 g4 g'2~
    \times 2/3 { g8[ b a] }
    \times 2/3 { g8[ fis e] }
    \times 2/3 { d8[ e c] }
    b4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 3/8
  c8 g e'
  \grace e16 d8 c16 b c8
  e c g'
  \grace g8 f4 e8
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Partita II " 
    }
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. o.Bez."
  \time 2/2
   \partial 4
   c4
   f1~ f
   e8 f g2 bes,4
   bes a r2
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Largo"
  \time 3/4
  c2. es d e
  f8. g16 a8 g bes a
  a4 g f g e f
  a, g
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Vivace"
  \time 3/8
  c8 f e
  \grace e8 f4 c8
  d16 e f8 d
  d c r
}

\pageBreak

\markuplist {
    \fontsize #3
    "Partita III " 
    \vspace #1.5
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "1. o.Bez."
  \time 2/2
  a'2. bes4
  d,2 cis4 d
  e4. f16 g f4 e
  f8. g16 a4 r2
}


\relative c'' {
  \clef treble
  \key d\minor
   \tempo "2. Largo"
  \time 3/4
  f4 e d
  c bes8 a g f
  g8. a32 bes a4 g
  a8 bes b c r
}

\relative c'' {
  \clef treble
  \key d\minor
   \tempo "3. Vivace"
  \time 2/4
     \partial 8
     a'8
     d,4 e
     f16 e d8 r e
     f16 g a4 g8 f16 e d8 r
}
