\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite I" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Musette. gracieusement"
  \time 2/2
  \partial 2
    c4 e g a b4. c8
    g4 \grace f8 e4 d e f4. e8 d4. e16 c
    b4 \grace a8 g4
}

\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite II" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Lentement"
  \time 3/4
  \partial 4
  e4
  e \grace d8 c4 g'
  g4. a8 b4 c g f e4. f8 g4
  d4. e8 f4
}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite III" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Musette"
  \time 2/2
  \partial 2
    c4 e
    d c e4. f8 g2 e8 f e f g a g a g f e d e4 c
}

\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite IV" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude"
  \time 3/4
  \partial 4
  g'4
  e8 d c g' a b
  c b c e, f d
  e f e f f4 \grace {e16[ f]} g2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite V " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. gracieusement"
  \time 2/2
  \partial 2
  c4 d es f g g8 a
  b4 c8 g as4 g f as8 g f4. es8 d2
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Suite VI " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. March"
  \time 2/2
  \partial 2
  d4 b8 d
  g4 b8 g fis4 g
  a g8 a b4 g c8 b a c b a g a
  fis2
}
