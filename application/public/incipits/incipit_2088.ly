 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 


\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Tutti"
  \key c\minor
   \tempo "Adagio"
  \time 3/4
  g'4 c, c' c b4. g8
  as4. f8 bes as
  as4 g8 g g g

}


\relative c'' {
  \clef bass
  \set Staff.instrumentName = #"Basso"
  \key c\minor
   \tempo "Adagio"
  \time 3/4
     \set Score.skipBars = ##t
     R2.*30
  g,4 c, c' c b4. g8
  as4. f8 bes as
  as4 g r

}