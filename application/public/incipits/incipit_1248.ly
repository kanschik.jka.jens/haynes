\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria 'Kommt vermehrt der Thorheit Ruhm'" 
    }
    \vspace #1.5
}

\relative c'' {
<<
\new Staff { \clef "treble"   
                     \set Staff.instrumentName = #"Oboe 1"
                     \key g\minor
                      \tempo "o. Bez."
                      \time 4/4
                      r2 d4 g 
                      fis g a bes8 g
                      a4 d, c d8 a
                      bes4 c a bes8 c
                      bes4 g
                      % Voice 1
                  }
\new Staff { \clef "treble" 
                     \key g\minor
                     \set Staff.instrumentName = #"Oboe 2"
                        % Voice 2
                       r2 d'4 bes a bes d g
                       fis fis, g a g g g fis
                       g d

                  }
                  
\new Staff { \clef "treble_8" 
                     \key g\minor
                     \set Staff.instrumentName = #"Tenor"
                        % Voice 2
                      r2 bes4 g
                      d' g fis g
                      d2 r R1 r2
}

>>
}

