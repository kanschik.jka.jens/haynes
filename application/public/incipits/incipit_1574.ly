\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/4
  bes8 bes32 a bes c  d16 d d d
  \times 2/3 { d16[ es c] } c4 c16 d
}
