
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premiere Sonate "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key c\major
   \tempo "1. moderem.t"
  \time 3/8
\partial 8
g8 f16 e d c b a 
g8 c e 
d16 c d e f g 
e8 d g
f16 e d c b a 
g8 c e

 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Allemande. Legerem.t"
  \time 2/4
\partial 16
c16 c4 r16 g'16 a f
g a f g e f g e
f g e f
d e f d
e f d e

 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. gracieusem.t"
  \time 3/8
\partial 8
c8 b4 c8
g4 e'8
d4 c8
g'4 g,8
c16 b c d c e 
d c d e d f
e8 f16 e d c
d8 g, c
b4
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Rondeau. Legerem.t"
  \time 2/4
e8 f g c,
b4. c8
d[ e f g]
e4 d
g8[ f e d] 
c[ b a c] 
f[ e d c] 
b4 g
 
}
 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxieme Sonate "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Lentem.t"
  \time 3/4
\partial 4
e8 f
g4 \grace f8 e4 \grace d8 c4
d \grace c8 b4 \grace a8 g4
c d e
d2 e8 f
g4
 
 
}

\relative c''' {
  \clef treble
  \key c\major
   \tempo "2. Legerem.t"
  \time 2/4
c4 g
c,4. c16 d
e f e d c d e f 
g8 c, r8 g'8
a16 bes a bes g a g a 
f g f g e f e f 
d e d e 
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Courante"
  \time 3/4
\partial 8
c8 c4. e8 d f
e d c d e f
g4 f8 e d c
b4 g g' a8 b a g f e 
d4 \grace c8 b4
 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Gigue"
  \time 6/8
\partial 4.
c8 e c
d f d e g e 
f a f g f e 
d e f e d c 
d4 g,8
 
}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisieme Sonate "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "1. Lentem.t Et marqué"
  \time 3/4
\partial 8*5
c8 es[ g c, es]
d f b, d g, bes
a c b d c es
d es16 f
es4.

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Rondeau "
  \time 2/4
\partial 4
c8 es
d g d f
es c es g
f[ es d c]
d g, c es
d g d f 
es[ c es g]
f es d8. c16 c4

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. gracieusement"
  \time 3/8
c8 c g'
es8. d16 c8
d es16 d c b 
c8. d16 es f 
g8 g g 
c c,16 b c d 
es f f8 es16 f
g4.


}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "4. gayem.t "
  \time 2/4
\partial 8
c8 g[ as g f]
es4 d8 g
f[ es d es]
c4 g8 c
d[ es f g] 
es[ d c d ]
es4 f
g4. 


}

\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatrieme Sonate "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude"
  \time 3/4
\partial 4
c4 g'4. a16 g f8 g16 f
e4 \grace d8 c4 e
d r16 f16 e d c8. e32 d64 c
b4 g

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Legerem.t"
  \time 2/4
c16 d e f g a b g
c8 c, c g'
f16 e d f e d c e
d8[ g, g c]
b16 a b c d e f d
e d c d

}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondeau. gracieusem.t"
  \time 6/8
\partial 4.
e8 f g
g4. c,8 d e 
d4 c8 e f g
f e d c b a 
g4. e'8 f g 
g4. c,8 d e 
d4 c8


}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. moderem.t"
  \time 2/4
\partial 4
c8 c d e f e16 d
e8 c g' g 
g f16 e d8 c
d4 g8 g 
a b c b16 a
b8 g

}





\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquieme Sonate "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Lentem.t"
  \time 3/4
b8 a g a b c 
a g a b c d 
b a g a b c
d4 g2~ g4 fis2 g4.
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Musette. gayem.t"
  \time 2/2
\partial 2
g8 fis g d
e d c e d c b a 
b4 g b8 a b c
d c b d e d c e
d e fis d
 
}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "3. Courante"
  \time 3/4
\partial 8
g8 g4 f8 es d c 
bes a g a bes c
d4 d g
fis \grace es8 d4
 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Rondeau. gayem.t"
  \time 3/8
b4. a c b
d8 e16 d c b
a8 g a
b c b a4.
 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixieme Sonate "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Allemande"
  \time 2/4
\partial 16
 g16 g4~g16 b a g
fis8. e16 d e c d 
b8. a16 g a b d 
d8 g d8. d16
e c a e' fis d e fis
g8. d16
 
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "2. Rondeau. gayem.t"
  \time 2/4
g8[ b a g]
fis4. d8
e[ d c b]
a4 g
b8[ a b d] 
d[ g, g d']
e4 cis d2
 
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "3. Rondeau"
  \time 2/4
bes8[ c d es]
d4 g
d8[ es d c]
bes4 \grace a8 g4
bes8[ c d es]
d[ es f g]
f4 e
d2
 
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "4. Gigue"
  \time 6/8
\partial 8
g8 b4. a
g~ g4 d8
e f e d4 c8
b a b g4 a8
b g b a d a 
b g b a d a
 
 
}


