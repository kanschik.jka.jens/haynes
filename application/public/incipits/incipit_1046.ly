\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quintetto [2] " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Allegro"
  \time 2/2
  f2 f8 d es f
  f4 g es2 d8 f bes f \grace f4 es2
  d
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Larghetto"
  \time 3/4
   c4 c4. bes8
   a8. bes16 c8 f32 e f16 a8 c
   c,4 c4. bes8
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Allegretto"
  \time 2/4
  d4. es8 d4 c8 r
  bes f'4 g8
  \grace f8 es4 d8 r
}

