\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro ma non troppo [Tutti]"
  \time 4/4
    c,4. e8 g g g g
    g4 g2 f4
    e f g a d,2~ d4 r
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro ma non troppo [Solo]"
  \time 4/4
  \set Score.skipBars = ##t
  R1*103
  c4. e8 g g g g
  g4 b,8 g' c, g' d g
  e g f a g a b c
  g4. f8 e4 r
  
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio  [Tutti]"
  \time 3/8
  es4 d16. b32
  c8 b r
  as g f
  \grace f8 es d r
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Adagio  [Tutti]"
  \time 3/8
  \set Score.skipBars = ##t
  R4.*12
  r4 es16. c32
  g8 r es'16. d32
  d4 f16. d32 g,8
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro ma non Presto"
  \time 2/4
  g'4 e8 c
  b[ d c c]
  d[ d e e]
  g f d
}