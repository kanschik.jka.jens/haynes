\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Alt): Sende, Herr, den Segen ein [BWV 193/5]" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Oboe"
  \key g\major
   \tempo "o. Bez."
   \time 4/4
   b8 a~ a16 c b a g8 d~ d16 fis a c
   b a c b a g fis g e'8 d~ d16 
    
   
  
   
}

\relative c'' {
  \clef treble
  \set Staff.instrumentName = #"Alt"
  \key g\major
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
    R1*12 r2 r4 d8 b
    g r8 r4 e'8 r8 c8 a
    fis d c'16 b c8 c4 r4
}



