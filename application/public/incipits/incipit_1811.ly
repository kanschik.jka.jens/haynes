\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro Spirituoso [Tutti]"
  \time 4/4
  \partial 4
  g4 c r c r
  c c16 b c d e4 e16 d e f
  g4 e f d c4. e16 c g4 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro Spirituoso [Solo]"
  \time 4/4
    c2. \grace d16 c8 b16 c
    e2. \grace d16 c8 b16 c
    g'2. \grace a16 g8 fis16 g
    b2. \grace a16 g8 fis16 g
    c1~ c1~ c1
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Romance poco Adagio"
  \time 4/4
  \partial 4
  c4 c8 f f4. e16 f g f e  f
  e8 d d4 r d
  bes' bes2 \grace a16 g8 f16 e
  g8 f f4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondo. Allegretto"
  \time 3/4
  \partial 4
  g4
  c c c   c d e
  f d d d r
}
