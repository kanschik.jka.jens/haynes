\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro "
  \time 4/4
  f2 c'4. a8
  f2 r8 f16 g a8 f
  e d bes'2 g8 f
  f e c'2 a8 f
  d f e d d c c bes
  bes4 a r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Tutti]"
  \time 3/4
  g8. a16 g8 c c c
  \grace d16 c4 b d16 cis d e
  f8. g16 f8 b, b b
  c8. d32 e f4 r8
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*5
   g'2. g
   g8. e16 c8 c c c
   c4 b \grace e8 d16 cis d e
   f8. d16 b8 b b b
   c8. d32 e f4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegretto"
  \time 2/4
  \partial 8
  c8
  f2 e4 f g4. a8
  f4 r8 f
  bes4 a8 f
  d'4 c8 f,
  c'4 bes8 a
  a g r
}