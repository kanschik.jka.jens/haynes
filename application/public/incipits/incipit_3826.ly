\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro assai [Tutti]"
  \time 3/4
  f2.
  a4. g8 f4 e d c
  cis d r
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro assai [Solo]"
  \time 3/4
  \set Score.skipBars = ##t
  R2.*107
  r4 r c
  f2.
  \grace a,8 a'2.
  \grace c,8 c'2 bes16 a g f
  e g f e  d c bes a g f e d c8 r
  
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio sostenuto [Tutti]"
  \time 4/4
  c2 g c,4 r r g'
  a2 fis g4 r r c
  c b8 r b4 a8 r
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio sostenuto [Solo]"
  \time 4/4
  \set Score.skipBars = ##t
  R1*7
  r2. c8. d16
  \grace c8 e4 d16 c b8 \grace d8 c4. g8
  b a g f g32 f e8. r g
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Allegro"
  \time 6/8
  c'4 a8 \grace g16 f8 e f
  g4 c8
  f,4 a8
  d, d' c bes a g
  fis g f \grace f16 e8 d c
}