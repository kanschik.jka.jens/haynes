 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Ouverture"
  \time 4/4
        d4 r \grace c8 b4. a8 g2 
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Menuet"
  \time 3/4
    g4 d b'
    \grace c8 b2 a4 g d d'
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Menuet"
  \time 3/4
    d4 r e d r r
    d r e c r r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. Sarabande"
  \time 3/2
    g1. a2 b c
    \grace c8 b1 a2
    g1.
}
