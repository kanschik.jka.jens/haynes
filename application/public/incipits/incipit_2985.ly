\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Tempo giusto [Tutti]"
  \time 2/4
    bes8 bes4 \times 2/3 { c16 bes a } 
    bes8 bes4 d32 c bes c
    d8 d4 \times 2/3 { es16 d c } 
    d8 d4 f32 es d es
        
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. Tempo giusto [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*46
   bes2 
   g'32 f16. f8~ f16 e32 f g16. es32
   es d c d es16 d \grace d8 c4
   bes r
  
}



\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Larghetto [Solo]"
  \time 2/4
    \grace {b16 c d} c16~ c64 bes a bes a8 r32 f a c f es d es
    es16~ es64 d c d c8

}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Presto [╚Tutti]"
  \time 2/4
    bes2 bes' f, es' d4. es16 d
    bes' f,4. g16 f
        
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "3. Presto [Solo]"
  \time 2/4
   \set Score.skipBars = ##t
   R2*46
   bes2 f' es d4. f8
   bes,2 c bes4 r
   
  
}

