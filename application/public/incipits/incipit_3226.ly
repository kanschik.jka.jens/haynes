\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Aria (Bass): Es ist euch gut, dass ich hingehe [BWV 108/1]" 
    }
    \vspace #1.5
}

\relative c' {
  \clef treble
  \set Staff.instrumentName = #"Oboe d'amore"
  \key a\major
   \tempo "o. Bez."
   \time 4/4
  r8 e8 a16. cis32 b16. d32 cis8. d16 e16 d32 cis d16. fis32
  e1~ e8 e, a16 b32 cis b16 cis32 d cis16
 
  
  
  
   
   
   
}

\relative c {
  \clef bass
  \set Staff.instrumentName = #"Bass"
  \key a\major
   \tempo "o. Bez."
   \time 4/4
   \set Score.skipBars = ##t
R1*8 r8 e8 a b16[ a32 b] cis8 b16[ cis32 d] cis8 b
a8. gis32 fis e4 r2
   
   
}


