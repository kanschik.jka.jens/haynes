\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Concert" 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "1. Ouverture"
  \time 2/2
  r4 a4 e a
  cis,4. cis8 cis4. d8
  e4 r16 e16 fis gis a4. b8
  gis4. e16 fis gis8 a gis a
  b4. b,8 b4.
}

\relative c'' {
  \clef treble
  \key a\major
   \tempo "2. Air Grave"
  \time 3/4
  e4 a4. cis,8
  d4 d4. e8
  cis b cis4. d8
  b2 a4
  cis8 d e fis g a 
  fis e fis gis a b
  gis fis fis2 e2.

}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "3. Musette en Rondeau. Louré"
  \time 2/2
  \partial 2
  a4 e cis4. b8 cis d e fis
  e d cis d e4 a
  fis4. e8 fis g fis gis 
  a4 e a e
  cis4.

}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "4. Couplet"
  \time 2/2
  \partial 2
  gis8 a gis a
  b a gis fis e d cis b
  cis b a b cis e d cis
  b a b d cis e dis fis e4 b

}

\relative c''' {
  \clef treble
  \key a\major
   \tempo "5. Air Rustique"
  \time 2/2
  a4 e cis a
  e' fis8 gis a4. b8
  gis4 gis8 a b b, cis d
  cis4 d8 e fis d e fis
  e4
}


\relative c'' {
  \clef treble
  \key a\major
   \tempo "6. Chaconne"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*4
  r4 e4. e8
  fis4 fis4. e16 fis gis2 gis8 gis
  a4 b b,
  e4. e8 a cis, d4 d4. d8 d4
}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "II.éme Concert " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "1. Vivement"
  \time 2/2
  \partial 4
  g4 c2. bes4
  a g fis es'
  d2. c4 bes a g g'
  fis2. d4 g2. f4 es d c g' f2.

}

\relative c'' {
  \clef treble
  \key g\minor
   \tempo "2. Lentement"
  \time 3/2
  r2 bes2 bes4. bes8
  b2 r2 d2
  g, g' g g1.~ g2 f as
  d,4 es d2. c4
  c2

}

\relative c''' {
  \clef treble
  \key g\minor
   \tempo "3. Vivement"
  \time 3/8
  g8 g, g a a' a
  bes bes, bes
  c c'4~ c8 bes4 a8 a4
  g8 g16 a bes g
  a4
  d,8 d cis4

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "III.éme Concert " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Tres gay et marqué"
  \time 2/2
  \partial 4
  g4 d' d, d' d d c8 b a g fis e
  d2 d'4 e8 f
  e4 fis8 g fis g a fis g4

}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Tres lentement"
  \time 3/4
  r4 e4 g
  fis4. e8 fis4
  b, b'2~ b8 e, a2~ a4 g fis e4. dis8 e4
  dis2.

}
  
  \relative c'' {
  \clef treble
  \key g\major
   \tempo "3. Vivement"
  \time 3/8
  r8 g16 a b c
  d8 d d 
  e e16 d e fis
  g8 d g~ 
  g fis16 g a b e, fis e fis g a 
 

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "IV.éme Concert " 
    }
    \vspace #1.5
}

\relative c''' {
  \clef treble
  \key c\minor
   \tempo "1. Prelude Grave"
  \time 2/2
  g2 g
  g f4 as
  g f8 es d4 es
  f2 d4 f
  es4. d8 es4. f8
  d2.

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allemande"
  \time 2/2
  \partial 4
  c8 d c4 g g es'
  d c8 d g,4 g'8 as
  g4 f8 es f4 es8 d
  es4 c r4

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "3. Sarabande"
  \time 3/4
  es4 d4. c8
  g'2. as4 d,4. g8 es4 c2
  g'4 f4. es8
  bes'4 b4. b8
  c g as2~ as8 bes g2~ g

}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Caprice"
  \time 4/8
  c8[ es d c]
  g'4 r8 g8
  b,4 r8 b8 c4 r8 c8 
  d[ f es d]
  es[ g f es]
  f[ as g f] g4 r4

}


\pageBreak


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "V.éme Concert " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "1. Prelude"
  \time 3/2
  b2 e g
  fis2. g4 a2~
  a gis b~
  b a4 b c2
  fis, b2. fis4
  g2

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "2. Fugue. Gay"
  \time 2/2
  \partial 4
  b4 g'2 fis 
  e4 d cis e
  dis fis b2~
  b4 a2 g4
  fis e2 dis4
  e8 g, a b c4 b8 a
  b4

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "3. Iére Gavotte"
  \time 2/2
  \partial 2
  e4 fis8 g
  dis4 e fis dis
  e b8 c e4 b
  c b a4. b8 b2

}

\relative c'' {
  \clef treble
  \key e\minor
   \tempo "4. Gigue"
  \time 6/8
  \partial 8
  b8 e,4 c'8 b4 e8
  dis4 fis8 a g fis 
  g e a a8. b16 g8
  fis4 dis8 e4 b8
  c4.~ c8 b a b4

}


\pageBreak

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "VI.éme Concert " 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "1. Prelude"
  \time 3/2
  r2 fis2 fis
  a1.
  r2 a2 a \grace {g16[ a]}
  b1.
  r2 b4. a8 g4. fis8
  e2 e4. fis8 g4. a8
  fis4 d a'1~
  a2

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "2. Allemande. Gay"
  \time 4/4
  a4 a \grace {g16[ a]} b8 b a g
  fis8. g16 g8. fis32 g a8 a g fis
  e4 r16 a16 g a fis8 fis16 e fis8 g
  a8. g16 fis8 

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "3. Reflexion. Lentement"
  \time 2/2
  \set Score.skipBars = ##t
   R1*4
  r2 a4. g8
  fis2. g8 a
  d,2. cis8 d
  cis4 a d2~
  ~d4 e8 d cis2~ 
  cis4 d8 cis b4 cis8 d
  gis,4

}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "4. Ière Gavotte"
  \time 4/8
  a4 fis
  g8.[ a16 fis8. g16]
  e8.[ d16 e8. g16]
  fis4 e fis e8 d
  e4 a8. g16
  fis8. a16
  d,8. cis16 b4 a

}

\relative c'' {
  \clef treble
  \key d\major
   \tempo "5. Sarabande en Rondeau"
  \time 3/4
  fis4 fis4. g8
  a4 a4. a8
  d,4 g4. fis8
  e2 e4 
  fis g a b4. d,8 g4
  fis e4. d8 d2.
  
}

\relative c''' {
  \clef treble
  \key d\major
   \tempo "6. Gigue"
  \time 6/8
  \partial 4.
  a4. d,8 e fis e fis g
  fis d e fis g fis
  e4 a8 a b gis a4.
}


