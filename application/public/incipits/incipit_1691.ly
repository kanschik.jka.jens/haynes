\version "2.16.1"

#(set-global-staff-size 14)

   
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
   \set Score.skipBars = ##t
   R1*48   f2 a4. bes8c4. a8 f4 r8 c8 c4. d8 c8a'  g f f e e4. 
  
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "2. Adagio [Solo]"
  \time 6/8
  \partial 8
  g8 c4 c8 c8. d16 e8 e4 d8 c4 r8 e4 e8 e8. f16 g8 g4 f8 e4 r8
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "3. Rondo [Solo]"
  \time 2/4
  \partial 8
  c8 \grace e8 f4 \grace e8 f4 c4. c8 d[ bes f' d] c4 r8 c8 f[ f a a] c4 bes16 a g f g8[ g f e ] f4
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "4. Tempo di Minuetto [Solo]"
  \time 3/4
  c'4. g8 e c b d g4 f e8 f g e d c b16 g' fis g a g fis g g,4
}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "5. Allemande [Solo]"
  \time 3/8
  \partial 8
  c8 c a' f c a' f bes, g' e f16 a g e f8 c8 a' f c a' f bes, g' e f4 r8
}