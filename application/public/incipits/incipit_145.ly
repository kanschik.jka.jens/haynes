\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. "
  \time 2/4
  \partial 8
    c16 b
    c8 g c[ \grace { c16[ d] } e8]
    d8 g, d'[ \grace { d16[ e] } f8]
    e8 g, e'[ \grace { e16[ f] } g8]
    d4.
}



\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Largo [Ob. II] "
  \time 3/4
  c4 r8 g[ c \grace { c16[ d] } es8] 
  d4 r8 g,[ d' \grace { d16[ es] } f8] 
es4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Adagio [sic] [Ob. I] "
      \set Score.skipBars = ##t
  \time 3/4
  R2.*3
  g'2.~ g2.~ g2.
  g4. f8 es d
  \grace f8 es4 \grace d8 c4 
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro"
  \time 6/8
  c16 g32 g g16 g g g c g c8 c8
  d16 g,32 g g16 g g g d'[ g, d'8 d8]
}


\relative c'' {
  \clef treble
  \key f\major
   \tempo "4. Allegro"
  \time 2/2
  c16[ g a b] c d e f g8 c, c b
  c4. 
}
