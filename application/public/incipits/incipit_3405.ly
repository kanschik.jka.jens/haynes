\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante"
  \time 2/4
      b16 g e' c  b g a fis
      g8 e'16 c b g a fis
      g8 b16 d  d c b a
      b8 g
}
