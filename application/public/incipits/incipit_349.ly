\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante [Tutti]"
  \time 2/2
  d,2 bes es a,4 r8 f'
  f' es d c  bes a g f 
  f e d e g4
}

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "2. Andante [Solo]"
  \time 2/2
   \set Score.skipBars = ##t
   R1*12
  d2 bes es a,4 r8 a
  bes[ c16 d]  es f g a  bes8 c d es
  f4 f,~ f8 g16 f f[ es d es]
  cis4 d8 r

}
