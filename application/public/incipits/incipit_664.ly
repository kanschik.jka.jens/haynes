
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "1e. Suite [15 pieces]"
    }
    \vspace #1.5
}
 
\relative c {
\clef "bass"   
  \key d\major
   \tempo "1. Trio. Ouverture"
  \time 2/2
\partial 8
r8 d4 d,8 d d4 r16 a''16 b cis 
d4 r16 d16 cis b a8 g fis e
d4 d8 d d4 g,4

 
}



\relative c {
  \clef bass
  \key d\major
   \tempo "2. Allegro"
  \time 3/8
a4 r8 r4.
r8 d'8 d 
d cis16 d e cis
fis8 d16 e fis d
e8
e, e


}

