 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. o.Bez."
  \time 2/4
  \partial 8
    a'16. g32
    g8. e16 c8 a'16. g32
    g8. f16 d8 g16. e32
    c8 c d d16 e
    c4 r8
}
