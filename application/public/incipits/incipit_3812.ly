\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante grazioso [Tutti]"
  \time 3/4
  d4 \grace cis8 d8 \grace cis8 d8 \grace cis8 d8 \grace cis8 d8
  \grace e16 d8 c16 b b4 r
  b8. a32 b c8. b32 c d8. e16
  \grace d16 c8 b16 a a4 r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Andante grazioso [Solo]"
  \time 3/4
   \set Score.skipBars = ##t
   R2.*12
   r4 c16 b a b  c b a b
   c4 r r
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. Rondeau. Allegro ma non troppo"
  \time 2/4
  g'8 g8. b16 a g
  g fis fis4 g8
  gis16 a a8. c16[ b a] a g g4
}
