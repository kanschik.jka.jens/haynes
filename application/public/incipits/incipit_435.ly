
\version "2.16.1"
 
#(set-global-staff-size 14)


\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}

\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Premier Suite"
    }
    \vspace #1.5
}
 
\relative c'' {
\clef "treble"   
  \key c\major
   \tempo "1. Allemande. Moderement"
  \time 4/4
\partial 8
r16 c16
c4~ c16 d b d d8 g, d' e16 f
e8 \grace d8 c8 g' f16 e f8 e16 d e8 d16 c 
d8[ g,]


 
} 

 
\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Deuxiéme Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prelude. Gravement"
  \time 3/4
\partial 4
g4 b4. c8 a4
b g d' 
g4. a8 fis4
g d b'
a4. g8 a4 d,2

 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Troisiéme Suite"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Rondeau. Tendrement"
  \time 3/4
\partial 4
g4 c2 d4
e4. f8 d4
c2 d4 e4. f8 d4
c2 d4
e f g 
g d f
e d c


}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Quatriéme Suite "
    }
    \vspace #1.5
}
 
\relative c''' {
  \clef treble
  \key g\major
   \tempo "1. Allemande. Gravement"
  \time 4/4
\partial 8
r16 g16 g4. d8 g,8. fis16 g a b c
b8 \grace a8 g8 b a16 g d'8. c16 b c d e 
d8 g4 fis16 e a8[ d,]

}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Cinquiéme Suite "
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Prelude. Gravement"
  \time 2/2
r4 r8 c8 e4. f8
d4. d8 g4. g8
g4. c,8 f4. f8
f e d e e4. d8 d4. d8 b4 g

 
}


\pageBreak
 
\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "Sixiéme Suite"
    }
    \vspace #1.5
}
 
\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Prelude. Gravement "
  \time 2/2
\partial 4
r8 g8 b4. b8 b4 c8 d
g,4. g8 g b a b 
b4. a8 b4. c8 a4. a8 d4. c8 b4. a8 g4. fis8

 
}


