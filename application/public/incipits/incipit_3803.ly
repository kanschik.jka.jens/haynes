\version "2.16.1"

#(set-global-staff-size 14)
\layout {
   ident = #0
   line-width = #250
   ragged-last = ##t
}

\paper {
   ragged-bottom = ##t        % "Flatterrand" unten
   ragged-right = ##t         % "Flatterrand" rechts
   print-page-number = ##f    % "Keine Seitenzahlen drucken
}


\markuplist {
    \wordwrap-lines {
    \fontsize #3
    "XIIe Suite" 
    }
    \vspace #1.5
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "1. Ouverture "
  \time 2/2
  g4. g8 b b d d 
  g4. g8 fis4 r16 fis16 g a 
  b4. b8 b4. c8
  a4.
 

  
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "2. [1er] Rondeau. gracieusement "
  \time 3/8
  b8 a4
  g16 d' e fis g8
  c, b4
  a16 g' fis g a8
  d,16 a' b8 a
 
 

  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "4. [1er] Vilageoise. gayment "
  \time 2/4
  \partial 4
  g8 b
  a[ d, a' c]
  b[ g b d] 
  c[ b a g]
  d'4 
  

  

  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "6. Gavotte. tres gracieusement "
  \time 2/2
  \partial 2
  b4 d
  g, c b4. c8
  a2 d4 g8 fis
  e4 fis \grace fis8 g4. a8
  fis4 \grace e8 d4
 
  
  

  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "7. [1er] Rondeau. Legerem.t "
  \time 6/8
  \partial 2
  d8 c4 b8
  e4 d8 c4 b8
  a b c d4 g,8
 
 

  
}

\relative c''' {
  \clef treble
  \key g\major
   \tempo "9. [1er] Menuet. "
  \time 3/4
  g8 a b4 a
  g8 fis e d c b
  c d e4 d
  


  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "11. [1er] Rigaudon "
  \time 2/2
  \partial 4
  d4 d b g e'
  d8 c b a g4 d'
  g b e, a
  fis8 g fis g a4 
 
  
  

  
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "13. [1er] Rondeau. moderem.t "
  \time 2/4
  r8 d8[ g b~]
  ~b a16 g fis e d c
  b8 b16 c d8 e
  d 
 
  
  
  
}

\relative c'' {
  \clef treble
  \key g\major
   \tempo "15. Air. gracieusement "
  \time 3/8
  \partial 8
  d8 \grace c8 b8 \grace a8 g8 c
  a4 b8
  c d16 c b a
  b8 \grace a8 g8 
 

 
   
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "16. [1er] Tembourin "
  \time 2/4
  g4 g
  b16 c b a g8 b
  a[ g fis g]
  a16 g fis e d e fis d
  
  
 
  
   
}


\relative c'' {
  \clef treble
  \key g\major
   \tempo "18. Chaconne "
  \time 3/4
  r4 d4 g~
  ~g fis8 e d c
  b4 d b'~
  ~b a8 g fis g
  a4 d,
 
  
 
 
  
   
}

