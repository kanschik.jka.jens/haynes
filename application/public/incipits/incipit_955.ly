\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [tutti]"
  \time 4/4
  c4 c,8. d16 c4 c'8. e16
  d4 g,,8. a16 g4 d''8. f16
  e4.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [solo]"
  \time 4/4
   \override TupletBracket #'stencil = ##f
  \override TupletNumber #'stencil = ##f
  g'4 c,8. d16 c4 e8 d d4. e16 f e4 e8. f16
  \times 2/3 { g8[ f g] }
  \times 2/3 { a8[ g f] }
  \times 2/3 { e8[ d e] }
  \times 2/3 { f8[ e d] }
  c8 g g4.
}
\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante [tutti]"
  \time 6/8
  c8. d32 c a8 r c c
  c8. d32 c g8 r c c
    c8. d32 c b8 f'4  b,8

}

\relative c'' {
  \clef treble
  \key f\major
   \tempo "2. Andante  [solo]"
  \time 6/8
  c2.~ c2.~ c8. d32 c b16[ d] g32 f16. e32 d16. c32 b16.
  c8 c'4~ c4.
}
\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Rondeau"
  \time 3/4
    g'2 \grace f8 e d16 c
    g'8 f d2
    e8 g g c c e,
    \grace e8 d4 c8 b a g
}