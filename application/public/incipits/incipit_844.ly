\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key bes\major
   \tempo "1. o.Bez."
  \time 2/4
  d16 es f es  d8 g
  f es d g
  f[ bes16 a]
}
