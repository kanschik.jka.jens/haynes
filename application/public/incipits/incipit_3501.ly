\version "2.16.1"
   #(set-global-staff-size 14)
   

\relative c'' {
  \clef treble  
  \key g\major
    \time 4/4
    \tempo "1. Moderato"
      \partial 4
      d4
      g2~  g8 d' c b
      \grace a8 g4 g2 g4
      a4. g16 fis
}


