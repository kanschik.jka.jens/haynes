\version "2.16.1"

#(set-global-staff-size 14)


\relative c'' {
  \clef treble
  \key g\major
   \tempo "Aria	"
  \time 2/2
  \partial 4
  d4 b c d e
  d c8 b  a g a4
  b c d8 a a4
}
