 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key a\minor
   \tempo "1. Largo"
  \time 4/4
  a'4. c16 b a4. e'16 c
  a gis a8 r16 e' f e d a b a g d' e d
  c b c b a4.
  
}


\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "2. Allegro"
  \time 4/4
  r8 a c a e'4 a
  r8 g16 f g bes a g  f e f g  f a g f
  e d e f  e g f e  d c d e  d f e d
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "3. Cantabile"
  \time 3/4
    a8. e16 \times 2/3 { a8[ b c]} \times 2/3 { b8[ c a]}
    b8. e,16 \times 2/3 { b'8[ c d]} \times 2/3 { c8[ d b]}
    \times 2/3 { c8[ d e]}  e4 \times 2/3 { d8[ c d]}
}

\relative c'' {
  \clef treble
  \key a\minor	
   \tempo "4. Allegro"
  \time 3/8
    e8 a, b c d8. e32 f
    d8 g, a b c8. d32 e
    c8 f e a gis d
    c16 b b8. a16
    a
}
