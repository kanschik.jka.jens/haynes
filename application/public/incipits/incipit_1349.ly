\version "2.16.1"

#(set-global-staff-size 14)



\relative c'' {
<<
\new Staff { \clef "treble"   
                     \key c\minor
                      \tempo "1. o.Bez."
                      \time 2/2
                      R1 R
                      c~ c8 b r d~ d c r es~
                      es[ d16 es] f8 d b4 r
                      % Voice 1
                  }
\new Staff { \clef "bass" 
                     \key c\minor
                        % Voice 2
                     r8 c, g es c c' as f
                     d d' b g es f g g,
                     c c' g es c c' as f
                     d d' b g es es' c as
                     f4 r g,8 g' d b
                  }
>>
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Allegro"
  \time 2/2
    r8 c d b c es d b
    c es d b c[ g'] f16 es d c
    b4 r r2
}

\relative c'' {
  \clef treble
  \key es\major
   \tempo "3. Larghetto e Cantabile"
  \time 12/8
    bes4. es8. d16 c8 bes4 d8 es8. d16 c8
    bes4. es4 g,8 as8. bes16 as8 f'4 as,8
    as4 g8 r8 r
}
\relative c'' {
  \clef treble
  \key c\minor
   \tempo "4. Allegro"
  \time 3/8
    c8 g es' d g16 f es d
    es8 f16 es d c
    b4. c
}


