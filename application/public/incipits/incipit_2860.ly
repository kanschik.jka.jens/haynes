\version "2.16.1"

#(set-global-staff-size 14)

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Tutti]"
  \time 4/4
  \partial 8
  e8 
  e4. f16 e d4. c16 b
  e8 f e4 d4. e8
  e4. f16 e d4.c16 b
  e8 f e4 d4.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "1. Allegro [Solo]"
  \time 4/4
  \partial 8
  r8
   \set Score.skipBars = ##t
   R1*18
  r2 c16 d e8 d16 e f8
  e16 f g8 f16 g a8 g f16 e d8 c
  \grace { f16[ g]} a4 g
}

\relative c'' {
  \clef treble
  \key c\minor
   \tempo "2. Largo"
  \time 3/4
  c4 g'~ g16 f g as
  f4 f~ f16 es f g
  es4 es~ es16 d es f
  d2 c4
}


\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Tutti]"
  \time 4/4
  r8 c e, e' d4. c16 b
  c8 c e, e' d4. e16 f
  e8 c e, e' d4. c16 b
  c8 c e, e' d4.
}

\relative c'' {
  \clef treble
  \key c\major
   \tempo "3. Allegro [Solo]"
  \time 4/4
    \set Score.skipBars = ##t
   R1*20
   r2 e16 f g8 d16 e f8
   e8 d c4 e16 f g8 d16 e f8
   e8 d c4 e16 f g8 c,16 d e8
   d4 r8
}