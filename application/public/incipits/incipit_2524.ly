 
\version "2.16.1"
 
#(set-global-staff-size 14)
 
 
\relative c'' {
  \clef treble
  \key f\major
   \tempo "1. Sonata adagio"
  \time 4/4
  c8 f16 e f8 e f c r8 bes
  a f'16 e d c bes a a8 g a'4
}


\relative c'' {
  \clef treble
  \key f\major	
   \tempo "2. Allegro"
  \time 2/2
  f8 r f r  f[ e16 d] c f e d
  c[ f e d] c d c bes a[ bes a g] f g a bes
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "3. Largo"
  \time 12/8
  f4 f8 f8. es16 des8 c2.~
  c4 as'8 g4 f8 f4 e8 g4.
}

\relative c'' {
  \clef treble
  \key f\major	
   \tempo "4. Allegro"
  \time 2/4
   \partial 8
   f16 g
   a4 g
   f16 e f g f8 f16 g a4 g
   e16 d e f e8 e16 f

}